﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="CompanySummary" Codebehind="CompanySummary.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <h3>Company Summary </h3>
                    <hr />
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:Label ID="lblCompanyCode" runat="server" CssClass="bold " Text="Company Code"></asp:Label>
                            <asp:TextBox ID="txtCompanyCode" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblCompanyName" runat="server" CssClass="bold " Text="Company Name"></asp:Label>
                            <asp:TextBox ID="txtCompanyName" TabIndex="1" runat="server" AutoCompleteType="None" AutoComplete="Off"
                                CssClass=" form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblStatus" CssClass="bold " runat="server" Text="Status"></asp:Label>
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control  input-sm">                              
                                <asp:ListItem Text="ALL" Value="-32768"></asp:ListItem>
                                <asp:ListItem Text="Approved" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Active" Value="1"></asp:ListItem>                                 
                                <asp:ListItem Text="InActive" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Deleted" Value="-10"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">                        
                            <div class="col-lg-2">
                                <asp:Button runat="server" Text="Search" CssClass="btn btn-sm btn-block btn-primary" ID="Search" OnClick="Search_Click">                                    
                                </asp:Button>
                            </div>
                            <div class="col-lg-2">
                                <asp:Button runat="server" ID="lnkNewCompany" Text="New Company" CssClass="btn  btn-sm btn-block btn-primary" OnClick="lnkNewCompany_Click"> 
                                </asp:Button>
                            </div>                            
                        </div>                     
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-body">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="grdCompanySummary" runat="server"  AutoGenerateColumns="False" Width="100%"  OnRowCommand="grdCompanySummary_RowCommand" OnRowDataBound="grdCompanySummary_RowDataBound" 
                    OnPreRender="grdCompanySummary_PreRender" GridLines="Horizontal" OnRowDeleting="grdCompanySummary_RowDeleting"
                    CssClass="table table-striped table-hover  text-nowrap  dt-responsive nowrap">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>                                
                            </HeaderTemplate>
                            <ItemTemplate>
                                 <asp:LinkButton ID="lbtnApprove" runat="server" CssClass="fa fa-thumbs-o-up fa-2x " CommandName="APPROVE" CommandArgument="<%# Container.DataItemIndex %>" Text="" ToolTip="Approve" ></asp:LinkButton>
                                 <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete " CommandName="DELETE" Text="" ToolTip="Delete" ></asp:LinkButton>
                            </ItemTemplate>                           
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblCompanyCode" Text="Company Code" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink TabIndex="1" CausesValidation="false" ID="lbtnCompanyCode" runat="server"
                                    Text='<%# Eval("Code")%>' ToolTip="Edit" NavigateUrl='<% #"CompanyDetails.aspx?CompanyID=" + Eval("ID")%>' Target="_blank"></asp:HyperLink>
                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrCompanyName" Text="Name" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCompanyName" runat="server" Text='<%# Eval("Name") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrArea" Text="Area" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblArea" runat="server" Text='<%#Eval("DefaultAddress") %>' CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrCity" Text="City" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCityText" runat="server" TabIndex="2" Text='<%#Eval("DefaultCity") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrApproved" Text="Status" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblApproved" runat="server" Text='<%#Eval("StatusName") %>' CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="grdCompanySummary" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdCompanySummary.ClientID%>');
        $(document).ready(function () {
            GridUI(GridID, 50);
        })    
    </script>
</asp:Content>
