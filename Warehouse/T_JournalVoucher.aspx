﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" EnableSessionState="true" Culture="en-GB" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" Inherits="T_JournalVoucher" Codebehind="T_JournalVoucher.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        function validateFloatKeyPress(el, evt) {

            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            if (charCode == 46 && el.value.indexOf(".") !== -1) {
                return false;
            }
            return true;
        }
        function Value(sender) {
            debugger
            var id = sender.id;
            var ArrayIDs = id.split('_');
            var ContentPlaceHolderID = id.replace("_" + ArrayIDs[ArrayIDs.length - 1], '').replace(ArrayIDs[ArrayIDs.length - 2], '')
            var NameOfTextBox = ArrayIDs[ArrayIDs.length - 2].replace('txt', '');
            var RowIndex = ArrayIDs[ArrayIDs.length - 1];
            var txtDebit = $get(ContentPlaceHolderID + 'txtDebit_' + RowIndex);
            var txtCredit = $get(ContentPlaceHolderID + 'txtCredit_' + RowIndex);
            if (NameOfTextBox == "Credit") {
                txtDebit.value = "0.00";
            }
            if (NameOfTextBox == "Debit") {
                txtCredit.value = "0.00";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div id="divButtons" class="pull-right">
                       <div class="btn-group">
                            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="btn btn-sm  btn-primary"   OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnApprove" Text="Approve" CssClass="btn  btn-sm  btn-primary" OnClick="btnApprove_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnCancel" Text="Back" CssClass="btn  btn-sm  btn-primary" OnClick="btnCancel_Click"></asp:Button>
                        </div>
                    </div>
                    <h4>Journal Voucher</h4>
                    <hr />
                    <div class="row">
                        <div class=" col-lg-12">
                            <asp:UpdatePanel ID="up_JournalVoucher" runat="server">
                                <ContentTemplate>
                                    <asp:TabContainer runat="server" CssClass="Bigtab" ActiveTabIndex="0" ID="tabMain">
                                        <asp:TabPanel runat="server" HeaderText="Basic Info" TabIndex="11" ID="tblLineItemDetail">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div id="divVoucherDate" class="col-lg-3">
                                                        <asp:Label ID="lblVoucherDate" runat="server" Text="Voucher Date" CssClass="bold"></asp:Label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtJournalVoucherDate" TabIndex="1" runat="server" placeholder="Voucher Date" CssClass="form-control input-sm" ValidationGroup="MainSave"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <asp:MaskedEditExtender ID="Mee_txtVoucherDate" runat="server"
                                                            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtJournalVoucherDate">
                                                        </asp:MaskedEditExtender>
                                                        <asp:CalendarExtender ID="Ce_JournalVoucherDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtJournalVoucherDate"
                                                            TargetControlID="txtJournalVoucherDate">
                                                        </asp:CalendarExtender>
                                                    </div>
                                                    <div id="divJournalVoucherNo" class="col-lg-3">
                                                        <asp:Label ID="lblJournalVoucherNo" runat="server" CssClass="bold" Text="Journal Voucher No"></asp:Label>
                                                        <asp:TextBox ID="txtJournalVoucherNo" TabIndex="1" placeholder="Journal Voucher No" Enabled="false" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>

                                                    <div id="divRefrenceNo" class="col-lg-3">
                                                        <asp:Label ID="lblRefrenceNo" runat="server" CssClass="bold" Text="Refrence No"></asp:Label>
                                                        <asp:TextBox ID="txtRefrenceNo" TabIndex="1" placeholder="Refrence No" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>

                                                    <div id="divTotalJournalVoucherAmount" class="col-lg-3">
                                                        <asp:Label ID="lblTotalJournalVoucherAmount" runat="server" CssClass="bold" Text="Total Journal Voucher Amount"></asp:Label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtTotalAmount" runat="server" Enabled="false" TabIndex="1" placeholder="0.00" CssClass="form-control amt input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-inr"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:TabPanel>
                                        <asp:TabPanel runat="server" HeaderText="Other Info" TabIndex="15" ID="tblAttachment">
                                            <ContentTemplate>
                                                <div class="row">

                                                    <div id="divStatus" class="col-lg-3">
                                                        <asp:Label ID="lblStatus" runat="server" CssClass="bold" Text="Status"></asp:Label>
                                                        <asp:TextBox ID="txtStatus" placeholder="Transaction Status" TabIndex="1" Enabled="false" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>


                                                </div>
                                                <div id="rowMulticurrency" runat="server">
                                                    <div class="newline"></div>
                                                    <div class="row">
                                                        <div id="divBaseCurrency" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderBaseCurrency" Text="Base Currency" runat="server" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblHeaderBaseCurrencyMsg" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                            <asp:TextBox ID="txtHeaderBaseCurrency" placeholder="Select Base Currency" runat="server" AutoPostBack="True" CssClass="form-control amt input-sm" TabIndex="1" Enabled="false"></asp:TextBox>
                                                            <asp:HiddenField runat="server" ID="hdfHeaderBaseCurrencyRate" Value="" />
                                                            <asp:HiddenField runat="server" ID="hdfHeaderBaseCurrencyID" Value="" />
                                                        </div>
                                                        <div id="divExchangeCurrency" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderExchangeCurrency" CssClass="bold " runat="server" Text="Transaction Currency"> </asp:Label>
                                                            <asp:Label ID="lblHeaderExchangeCurrencyMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                            <asp:RequiredFieldValidator ID="Rfv_txtHeaderExchangeCurrency" runat="server" ControlToValidate="txtHeaderExchangeCurrency" CssClass="text-danger"
                                                                Display="Dynamic" ErrorMessage=" (Select Transaction Currency)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtHeaderExchangeCurrency" TabIndex="1" placeholder="Select Transaction Currency" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfHeaderExchangeCurrencyName" Value="" />
                                                            <asp:HiddenField runat="server" ID="hdfHeaderExchangeCurrencyID" Value="" />
                                                            <asp:AutoCompleteExtender ID="ACEtxtHeaderExchangeCurrency" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CurrencyTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtHeaderExchangeCurrency">
                                                            </asp:AutoCompleteExtender>
                                                        </div>
                                                        <div id="divExchangeCurrencyRate" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderExchangeCurrencyRate" Text="Exchange Currency Rate" runat="server" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblHeaderExchangeCurrencyRateMsg" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtHeaderExchangeCurrencyRate" placeholder="0.00" runat="server" AutoPostBack="True" CssClass="form-control amt input-sm"
                                                                    onkeypress="return validateFloatKeyPress(this, event)" TabIndex="1" ReadOnly="True"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                            </div>
                                                        </div>
                                                        <div id="divCurrencyConvertedAmount" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderCurrencyConvertedAmount" Text="Converted Currency Amount" runat="server" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblHeaderCurrencyConvertedAmountMsg" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                            <asp:TextBox ID="txtHeaderCurrencyConvertedAmount" placeholder="0.00" runat="server" CssClass="form-control amt input-sm" Enabled="False" TabIndex="1"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                        </asp:TabPanel>
                                    </asp:TabContainer>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <asp:UpdatePanel runat="server" ID="JournalVoucher">
                        <ContentTemplate>
                            <asp:GridView ID="grdJournalVoucherDetails" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Both" ShowFooter="true"
                                CssClass="table table-hover table-striped text-nowrap nowrap" OnRowCommand="grdJournalVoucherDetails_RowCommand" OnPreRender="grdJournalVoucherDetails_PreRender">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="250">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderLedger" Text="Ledger" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtLedger" runat="server" ControlToValidate="txtLedger" CssClass="text-danger" Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>

                                            <div class="input-group">
                                                <asp:TextBox ID="txtLedger" TabIndex="1" Text='<%# Eval("Ledger.LedgerName") %>' AutoPostBack="true" OnTextChanged="txtLedger_TextChanged" placeholder="Select Ledger" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                            </div>

                                            <asp:HiddenField runat="server" ID="hdfLedgerName" Value='<%# Eval("Ledger.LedgerName") %>' />
                                            <asp:HiddenField runat="server" ID="hdfLedgerID" Value='<%# Eval("Ledger_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtLedger" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLedger">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="300">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderDescription" Text="Description" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtJournalVoucherDesc" TabIndex="1" Text='<%# Eval("JournalVoucherDesc") %>' AutoPostBack="true" OnTextChanged="txtJournalVoucherDesc_TextChanged" placeholder="Enter Description" runat="server" AutoCompleteType="None" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="200" Visible="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderProfitCenter" Text="Profit Center" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtProfitCenter" runat="server" ControlToValidate="txtProfitCenter" CssClass="text-danger" Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtProfitCenter" TabIndex="1" Text='<%# Eval("ProfitCenter.ProfitCenterName") %>' AutoPostBack="true" OnTextChanged="txtProfitCenter_TextChanged" placeholder="Select Profit Center" runat="server"
                                                AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfProfitCenterName" Value='<%# Eval("ProfitCenter.ProfitCenterName") %>' />
                                            <asp:HiddenField runat="server" ID="hdfProfitCenterID" Value='<%# Eval("ProfitCenter_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtProfitCenter" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="ProfitCenterSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtProfitCenter">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="200" Visible="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCostCenter" Text="Cost Center" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtCostCenter" runat="server" ControlToValidate="txtCostCenter" CssClass="text-danger" Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtCostCenter" TabIndex="1" Text='<%# Eval("CostCenter.CostCenterName") %>' AutoPostBack="true" OnTextChanged="txtCostCenter_TextChanged" placeholder="Select Cost Center" runat="server"
                                                AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfCostCenterName" Value='<%# Eval("CostCenter.CostCenterName") %>' />
                                            <asp:HiddenField runat="server" ID="hdfCostCenterID" Value='<%# Eval("CostCenter_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtCostCenter" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CostCenterSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtCostCenter">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="150">
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderDebitAmount" Text="Debit" runat="server" CssClass="bold"></asp:Label>
                                                ( <span><i class="fa fa-inr"></i></span>)
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDebit" TabIndex="1" OnTextChanged="txtDebit_TextChanged" AutoPostBack="true" Text='<%# Eval("DebitAmount") %>' placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterDebitAmount" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="150">
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderCreditAmount" Text="Credit" runat="server" CssClass="bold"></asp:Label>
                                                ( <span><i class="fa fa-inr"></i></span>)
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtCredit" TabIndex="1" OnTextChanged="txtCredit_TextChanged" AutoPostBack="true" Text='<%# Eval("CreditAmount") %>' placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterCreditAmount" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="15">
                                        <HeaderTemplate>
                                            <asp:Button ID="btnAddLineItem" TabIndex="1" Text="Add Item" AccessKey="A" runat="server" OnClick="btnAddLineItem_Click" CssClass="fa fa -button"></asp:Button>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnRemoveLineItem" CommandName="Remove" TabIndex="1" runat="server" Text="Remove"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="grdJournalVoucherDetails" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
