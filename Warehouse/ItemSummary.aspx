﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="ItemSummary" Codebehind="ItemSummary.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <h3>Item Summary</h3>
                    <hr />
                    <div class="row">
                    <div class="col-lg-3">
                        <asp:Label ID="lblItemCode" runat="server" CssClass="bold" Text="Item Code"></asp:Label>
                        <asp:TextBox ID="txtItemCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                    <div class="col-lg-3">
                        <asp:Label ID="lblItemName" runat="server" CssClass="bold" Text="Item Name"></asp:Label>
                        <asp:TextBox ID="txtItemName" TabIndex="1" runat="server" AutoCompleteType="None" AutoComplete="Off"
                            CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                    <div class="col-lg-3">
                        <asp:Label ID="lblActive" CssClass="bold" runat="server" Text="Status"></asp:Label>
                        <asp:DropDownList ID="ddlActiveStatus" runat="server" CssClass="form-control input-sm">
                            <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                            <asp:ListItem Text="InActive" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="btn-group pull-right">
                                <asp:LinkButton runat="server" Text="Search" CssClass="btn  btn-sm btn-default" ID="lbtnSearch" OnClick="lbtnSearch_Click">                                    
                                </asp:LinkButton>
                                <asp:LinkButton runat="server" ID="lbtnNewItem" Text="New Item" CssClass="btn btn-sm btn-default" OnClick="lbtnNewItem_Click">                                 
                                </asp:LinkButton>                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-body">
                <asp:GridView ID="grdItemSummary" runat="server" AutoGenerateColumns="False" GridLines="Horizontal" 
                    OnPreRender="grdItemSummary_PreRender" OnRowDeleting="grdItemSummary_RowDeleting"
                    CssClass="table table-striped table-hover text-nowrap dt-responsive nowrap">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblSelect" Text="Delete" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                               <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete" CommandName="DELETE" CommandArgument='<%#Eval("ID") %>' Text="" ToolTip="Delete" />
                            </ItemTemplate>                            
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="hdrItemCode" Text="Item Code" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink TabIndex="1" CausesValidation="false" ID="hlItemCode" runat="server"
                                    Text='<%# Eval("Code")%>' ToolTip="History" NavigateUrl='<% #"ItemMaster.aspx?ItemId=" + Eval("ID")%>' Target="_blank"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="HdrItemName" Text="Name" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("Name") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="hdrItemCategory" Text="Item Category" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblItemCategory" runat="server" Text='<%#Eval("CategoryType.Name") %>' CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                       
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="hdrActive" Text="Status" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblActive" runat="server" Text='<%#Eval("Status").ToString()=="1"?"Active":"InActive" %>' CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdItemSummary.ClientID%>');
        $(document).ready(function () {
            GridUI(GridID, 50);
        })
        //var prm = Sys.WebForms.PageRequestManager.getInstance();
        //prm.add_endRequest(function (s, e) {
        //    GridUI(GridID, 50);
        //});
    </script>
</asp:Content>
