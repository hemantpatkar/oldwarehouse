﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="CustomerItem" Codebehind="CustomerItem.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="row">
        <div class="col-lg-12">
            <h3>Item Details</h3>
            <hr />
            <asp:UpdatePanel ID="upItemDetails" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdCustomerItems" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found" 
                        OnPreRender="grdCustomerItems_PreRender" CssClass="table table-striped  table-hover text-nowrap"
                        OnRowCancelingEdit="grdCustomerItems_RowCancelingEdit" OnRowCommand="grdCustomerItems_RowCommand"
                        OnRowEditing="grdCustomerItems_RowEditing" OnRowUpdating="grdCustomerItems_RowUpdating" OnRowDeleting="grdCustomerItems_RowDeleting">
                        <Columns>                                                    
                            <asp:TemplateField>                                
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="ADDNEW" CommandArgument=""
                                        Text="Add New" ToolTip="Add New" />
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="EditItem"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" CssClass="fa fa-ban fa-2x" ToolTip="Cancel"
                                        Text="" />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CssClass="fa fa-edit fa-2x fa-color-Edit " CommandName="Edit"
                                        Text="" ToolTip="Edit" />
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete " CommandName="Delete" Text="" ToolTip="Delete" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x fa-color-Add" ToolTip="Save"
                                        Text="" ValidationGroup="AddItem" UseSubmitBehavior="False" />
                                    <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban fa-2x" Text="" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                             <HeaderTemplate>
                                    <asp:Label ID="lblItem" Text="Item Name" runat="server" CssClass="bold" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblItemType" runat="server" Text='<%# Eval("Item.Name") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtItemType" runat="server" Text='<%# Eval("Item.Name") %>' MaxLength="500" TabIndex="2" ValidationGroup="EditItem" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdfItemTypeName" Value='<%# Eval("Item.Name").ToString() %>' />
                                    <asp:HiddenField runat="server" ID="hdfItemTypeID" Value='<%# Eval("Item_ID") %>' />
                                    <asp:AutoCompleteExtender ID="ACEtxtItemType" runat="server"
                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="ItemSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                        ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtItemType">
                                    </asp:AutoCompleteExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtItemType" CssClass="text-danger"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="EditItem" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewItemType" runat="server" TabIndex="2" onblur="return ClearAutocompleteTextBox(this)" PlaceHolder="Select Item Type" AutoPostBack="false" AutoComplete="Off" AutoCompleteType="None" MaxLength="50" CssClass="form-control input-sm" ValidationGroup="AddItem"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdfNewItemTypeName" />
                                    <asp:HiddenField runat="server" ID="hdfNewItemTypeID" Value="0" />
                                    <asp:AutoCompleteExtender ID="ACEtxtNewItemType" runat="server"
                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="ItemSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                        ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtNewItemType">
                                    </asp:AutoCompleteExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtNewItemType" CssClass="text-danger" ToolTip="Required Field"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="AddItem" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" runat="server" Text='<%#Eval("Status").ToString()=="1"?"Y":"N" %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkActive" TabIndex="9" CssClass="bold" Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsActive" TabIndex="9" CssClass=" bold" Checked="true" />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>                        
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdCustomerItems" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdCustomerItems.ClientID%>');
        $(document).ready(function () {
            debugger;
          //GridUI(GridID, 50);
        })
    </script>
</asp:Content>
