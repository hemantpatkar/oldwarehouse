﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppUtility;
using AppObjects;
public partial class M_UOM : BigSunPage
{
    AppUOMTypeColl obj;
    protected void Page_Load(object sender, EventArgs e)
    {
  
        obj = new AppObjects.AppUOMTypeColl(intRequestingUserID);
        Session[TableConstants.SessionUOM] = obj;
        if (!IsPostBack)
        {
           BindData();
        }
    }
    #region GridViewEvents
   
    protected void grdUOM_PreRender(object sender, EventArgs e)
    {
        if (grdUOM.Rows.Count > 0)
        {
            grdUOM.UseAccessibleHeader = true;
            grdUOM.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdUOM_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if (strCommandName == "INSERT")
        {       
            HiddenField hdfUOMID = grdUOM.FooterRow.FindControl("hdfUOMID") as HiddenField;
            //TextBox txtUOMCode = grdUOM.FooterRow.FindControl("txtNewUOMCode") as TextBox;
            TextBox txtUOMName = grdUOM.FooterRow.FindControl("txtNewUOMName") as TextBox;
            CheckBox chkActiveUOM = grdUOM.FooterRow.FindControl("chkActiveUOM") as CheckBox;
            CheckBox chkDefaultUOM = grdUOM.FooterRow.FindControl("chkDefaultUOM") as CheckBox;
            SaveUOMDetails(strCommandID, "", txtUOMName.Text, chkActiveUOM.Checked, chkDefaultUOM.Checked);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('UOM Successfully Added','1'" + ");", true);
            grdUOM.EditIndex = -1;
            BindData();
        }
        else if (strCommandName == "CANCELADD")
        {
            grdUOM.ShowFooter = false;
            BindData();
        }
        else if (strCommandName == "CANCELEDIT")
        {
            grdUOM.EditIndex = -1;
            BindData();
        }
        else if(strCommandName=="ADDNEW")
        {
            grdUOM.EditIndex = -1;
            grdUOM.ShowFooter = true;
            BindData();
        }
    }
    protected void grdUOM_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdUOM.ShowFooter = false;
        grdUOM.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdUOM_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HiddenField hdfID = grdUOM.Rows[e.RowIndex].FindControl("hdfID") as HiddenField;
        AppUOMType SingleObj = new AppUOMType(intRequestingUserID);
        SingleObj.ID = new AppConvert(hdfID.Value);
        SingleObj.Status = new AppConvert((int)RecordStatus.Deleted);
        SingleObj.Save();
     
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('UOM Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void grdUOM_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strCommandID = new AppConvert(e.RowIndex);
        HiddenField hdfUOMID = grdUOM.Rows[e.RowIndex].FindControl("hdfUOMID") as HiddenField;
       // TextBox txtUOMCode = grdUOM.Rows[e.RowIndex].FindControl("txtUOMCode") as TextBox;
        TextBox txtUOMName = grdUOM.Rows[e.RowIndex].FindControl("txtUOMName") as TextBox;
        CheckBox chkIsActiveUOM = grdUOM.Rows[e.RowIndex].FindControl("chkIsActiveUOM") as CheckBox;
        CheckBox chkIsDefaultUOM = grdUOM.Rows[e.RowIndex].FindControl("chkIsDefaultUOM") as CheckBox;

        SaveUOMDetails(hdfUOMID.Value, "", txtUOMName.Text , chkIsActiveUOM.Checked, chkIsDefaultUOM.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('UOM Successfully Updated','1'" + ");", true);
        grdUOM.EditIndex = -1;
        BindData();
    }
    #endregion
    #region MiscFunctions
    public void SaveUOMDetails(string Id, string UOMCode, string UOMName, bool IsActive, bool IsDefault)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppUOMType SingleObj = new AppUOMType(intRequestingUserID);      
        SingleObj.ID = new AppConvert(Id);
        //SingleObj.Code = new AppConvert(UOMCode);
        SingleObj.Name = new AppConvert(UOMName);
        SingleObj.Status = (IsActive == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        if (IsDefault == true)
        {
           
            obj.SetDefaultUOM();
        }
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;

        Session.Add(TableConstants.SessionUOM, obj);
        SingleObj.Save();
        BindData();
    }
    

    protected void BindData()
    {
        AppObjects.AppUOMTypeColl objColl = new AppUOMTypeColl(intRequestingUserID);
        if (!string.IsNullOrEmpty(txtUOMName.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.UOMType.Name, AppUtility.Operators.Equals, txtUOMName.Text, 0);
        }
       
        if (ddlStatus.SelectedValue != "-32768")
        {
             
            objColl.AddCriteria(AppObjects.UOMType.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.SessionUOM] = objColl;
        if (objColl != null)
        {
            grdUOM.DataSource = objColl;
            grdUOM.DataBind();
            if (objColl.Count <= 0)
            {
                AddEmptyRow();
            }
            
        }
       
 
    }
    public void AddEmptyRow()
    {
        AppUOMTypeColl collOfContacts = new AppUOMTypeColl(intRequestingUserID);
        AppUOMType newContact = new AppUOMType(intRequestingUserID);
        newContact.Name = "Add atleast one UOM";
        collOfContacts.Add(newContact);
        grdUOM.DataSource = collOfContacts;
        grdUOM.DataBind();
        grdUOM.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdUOM.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdUOM.ShowFooter = true;
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}