﻿#region Author
/*
Name    :   Navnath Sonavane
Date    :   04/08/2016
Use     :   This form is used to add Credit Voucher Order details  
*/
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.Services;
using AppUtility;
using Objects;
using AjaxControlToolkit;
#endregion
public partial class T_CreditNote : BigSunPage
{
    #region Page_Load
    AppObjects.AppCreditNote obj = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = new AppObjects.AppCreditNote(intRequestingUserID);
        if (!IsPostBack)
        {
            Session[TableConstants.CreditNote] = obj;
            string ID = Convert.ToString(Request.QueryString["CreditNoteID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int CreditNoteID = new AppUtility.AppConvert(ID);
                obj = new AppObjects.AppCreditNote(intRequestingUserID, CreditNoteID);
                PopulateData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "2")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Credit Note Approved Sucessfully','1'" + ");", true);
                }
            }
            else
            {
                DefaultData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
            }
            Session[TableConstants.CreditNote] = obj;
            BindData();
        }
    }
    #endregion
    #region Button Click Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppCreditNote CreditNote = (AppCreditNote)Session[TableConstants.CreditNote];
        CreditNote.CreditNoteDetailColl.RemoveAll(x => x.Ledger_ID == 0);
        if (CreditNote.CreditNoteDetailColl.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please add atleast one item','2'" + ");", true);
            return;
        }
        Session.Add(TableConstants.CreditNote, CreditNote);
        decimal DebitAmount = CreditNote.CreditNoteDetailColl.Sum(x => x.DebitAmount);
        decimal CreditAmount = CreditNote.CreditNoteDetailColl.Sum(x => x.CreditAmount);
        if (DebitAmount != CreditAmount)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Debit and Credit amount not matching.','2'" + ");", true);
            return;
        }
        SaveCreditNoteHeader();
        Session.Remove(TableConstants.CreditNote);
        Response.Redirect("T_CreditNote.aspx?Msg=1");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("T_CreditNoteSummary.aspx");
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        AppCreditNote CN = (AppCreditNote)Session[TableConstants.CreditNote];
        CN.Status = new AppConvert((int)RecordStatus.Approve);
        CN.Save();
        Response.Redirect("T_CreditNote.aspx?Msg=2&CreditNoteID="+Convert.ToString(Request.QueryString["CreditNoteID"]));
    }


    protected void btnAddLineItem_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            AddEmptyRow(1);
            if (grdCreditNoteDetails.Rows.Count > 0)
            {
                TextBox txtLedger = (TextBox)grdCreditNoteDetails.Rows[0].FindControl("txtLedger");
                AppCreditNote CreditNote = (AppCreditNote)Session[TableConstants.CreditNote];
                if (CreditNote == null)
                {
                    CreditNote = new AppCreditNote(intRequestingUserID);
                }

                txtLedger.Focus();
            }
        }
    }
    #endregion
    #region Functions
    public bool Validation()
    {

        if (grdCreditNoteDetails.Rows.Count > 0)
        {
            TextBox txtLedger = (TextBox)grdCreditNoteDetails.Rows[0].FindControl("txtLedger");
            TextBox txtDebit = (TextBox)grdCreditNoteDetails.Rows[0].FindControl("txtDebit");
            TextBox txtCredit = (TextBox)grdCreditNoteDetails.Rows[0].FindControl("txtCredit");
            if (txtLedger.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please select ledger first.','2'" + ");", true);
                return false;
            }
            if (txtDebit.Text == txtCredit.Text)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter debit or credit value for selected ledger.','2'" + ");", true);
                return false;
            }

            txtLedger.Focus();
        }
        return true;
    }
    public void DefaultData()
    {
        txtCreditNoteDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        //hdfHeaderBaseCurrencyName.Value = txtHeaderBaseCurrency.Text;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrency);
        btnApprove.Visible = false;
    }
    public void PopulateData()
    {
        txtCreditNoteDate.Text = new AppConvert(obj.CreditNoteDate);
        txtCreditNoteNo.Text = new AppConvert(obj.CreditNoteNo);
        txtStatus.Text = obj.StatusName;
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        //hdfHeaderBaseCurrencyName.Value = txtHeaderBaseCurrency.Text;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);
        txtHeaderExchangeCurrency.Text = obj.CurrencyType.Name;
        hdfHeaderExchangeCurrencyName.Value = txtHeaderExchangeCurrency.Text;
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(obj.CurrencyType_ID);
        btnSave.Visible = obj.Status != new AppConvert((int)RecordStatus.Created) ? false : true;
        btnApprove.Visible = obj.Status == new AppConvert((int)RecordStatus.Created) ? true : false;
    }
    public void BindData()
    {
        AppCreditNote CreditNote = (AppCreditNote)Session[TableConstants.CreditNote];
        if (CreditNote == null)
        {
            CreditNote = new AppCreditNote(intRequestingUserID);
        }
        grdCreditNoteDetails.DataSource = CreditNote.CreditNoteDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created));
        grdCreditNoteDetails.DataBind();
        if (CreditNote.CreditNoteDetailColl.Count <= 0)
        {
            AddEmptyRow(1);
        }
        SetValueToGridFooter();
    }
    public void SaveCreditNoteHeader()
    {
        AppCreditNote CN = (AppCreditNote)Session[TableConstants.CreditNote];
        if (CN.ID == 0)
        {
            CN.CreditNoteNo = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "CN", System.DateTime.Now, "CN-", 4, ResetCycle.Yearly));
        }

        CN.VoucherType_ID =20;
        CN.TotalAmount = new AppConvert(txtTotalAmount.Text);
        CN.Company_ID = new AppConvert(LoginCompanyID);
        CN.CreditNoteDate = new AppConvert(txtCreditNoteDate.Text);
        CN.CurrencyType_ID = new AppConvert(hdfHeaderExchangeCurrencyID.Value);
        CN.Status = new AppConvert((int)RecordStatus.Created);
        CN.Type = obj.Type;
        CN.ModifiedBy = intRequestingUserID;
        CN.ModifiedOn = System.DateTime.Now;
        CN.Save();

        if (CN.CreditNoteDetailColl.Count > 0)
        {
            foreach (var item in CN.CreditNoteDetailColl)
            {
                if (item.CreditAmount != 0)
                {
                    LedgerEntry(LoginCompanyID, 100802, 1008, item.Ledger_ID, item.ID, CN.CreditNoteDate, item.CreditAmount, 0, CN.CreditNoteNo, item.ModifiedBy, item.ModifiedOn, item.Status, CN.Status ,item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, CN.VoucherType_ID, item.CreditNoteDesc);
                }
                else
                {
                    LedgerEntry(LoginCompanyID, 100801, 1008, item.Ledger_ID, item.ID, CN.CreditNoteDate, 0, item.DebitAmount, CN.CreditNoteNo, item.ModifiedBy, item.ModifiedOn, item.Status, CN.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, CN.VoucherType_ID, item.CreditNoteDesc);
                }


            }

        }
    }
    public void SetValueToGridFooter()
    {
        AppCreditNote CreditNote = (AppCreditNote)Session[TableConstants.CreditNote];
        Label lblFooterCreditAmount = (Label)grdCreditNoteDetails.FooterRow.FindControl("lblFooterCreditAmount");
        lblFooterCreditAmount.Text = Convert.ToDecimal(CreditNote.CreditNoteDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created)).Sum(x => x.CreditAmount)).ToString("#,##0.00");

        Label lblFooterDebitAmount = (Label)grdCreditNoteDetails.FooterRow.FindControl("lblFooterDebitAmount");
        lblFooterDebitAmount.Text = Convert.ToDecimal(CreditNote.CreditNoteDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created)).Sum(x => x.DebitAmount)).ToString("#,##0.00");
        txtTotalAmount.Text = lblFooterDebitAmount.Text;



    }
    public void AddEmptyRow(int NumberOfRows)
    {
        AppCreditNote CreditNote = (AppCreditNote)Session[TableConstants.CreditNote];
        if (CreditNote == null)
        {
            CreditNote = new AppCreditNote(intRequestingUserID);
        }
        AppCreditNoteDetail CreditNoteItem = new AppCreditNoteDetail(intRequestingUserID);
        for (int i = 0; i < NumberOfRows; i++)
        {
            if (CreditNote.CreditNoteDetailColl.Count == 0)
            {
                CreditNote.CreditNoteDetailColl.Add(CreditNoteItem);
            }
            else
            {
                CreditNote.CreditNoteDetailColl.Insert(0, CreditNoteItem);
            }
            if (CreditNote.CreditNoteDetailColl.Count >= 0)
            {
                decimal DebitAmount = CreditNote.CreditNoteDetailColl.Sum(x => x.DebitAmount);
                decimal CreditAmount = CreditNote.CreditNoteDetailColl.Sum(x => x.CreditAmount);
                if (DebitAmount > CreditAmount)
                {

                    CreditNote.CreditNoteDetailColl[0].CreditAmount = new AppConvert(DebitAmount - CreditAmount);

                }
                else
                {
                    CreditNote.CreditNoteDetailColl[0].DebitAmount = new AppConvert(CreditAmount - DebitAmount);

                }
            }
            Session.Add(TableConstants.CreditNote, CreditNote);
        }
        BindData();
    }
    #endregion
    #region Text Box Event

    protected void txtLedger_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfLedgerID = (HiddenField)grdCreditNoteDetails.Rows[RowIndex].FindControl("hdfLedgerID");
        TextBox txtLedger = (TextBox)grdCreditNoteDetails.Rows[RowIndex].FindControl("txtLedger");
        TextBox txtCreditNoteDesc = (TextBox)grdCreditNoteDetails.Rows[RowIndex].FindControl("txtCreditNoteDesc");
        AppCreditNote CreditNote = (AppCreditNote)Session[TableConstants.CreditNote];
        CreditNote.CreditNoteDetailColl[RowIndex].Ledger_ID = new AppUtility.AppConvert(hdfLedgerID.Value);
        CreditNote.CreditNoteDetailColl[RowIndex].ModifiedBy = intRequestingUserID;
        CreditNote.CreditNoteDetailColl[RowIndex].ModifiedOn = System.DateTime.Now;
        CreditNote.CreditNoteDetailColl[RowIndex].Status = new AppConvert((int)RecordStatus.Created);
        CreditNote.CreditNoteDetailColl[RowIndex].Type = new AppConvert((int)DefaultNormalType.Normal);
        Session.Remove(TableConstants.CreditNote);
        Session.Add(TableConstants.CreditNote, CreditNote);
        BindData();
        txtCreditNoteDesc.Focus();
    }


    protected void txtCreditNoteDesc_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtCreditNoteDesc = (TextBox)grdCreditNoteDetails.Rows[RowIndex].FindControl("txtCreditNoteDesc");
        TextBox txtDebit = (TextBox)grdCreditNoteDetails.Rows[RowIndex].FindControl("txtDebit");
        AppCreditNote CreditNote = (AppCreditNote)Session[TableConstants.CreditNote];
        CreditNote.CreditNoteDetailColl[RowIndex].CreditNoteDesc = new AppUtility.AppConvert(txtCreditNoteDesc.Text);

        Session.Remove(TableConstants.CreditNote);
        Session.Add(TableConstants.CreditNote, CreditNote);
        BindData();
        txtDebit.Focus();
    }
    protected void txtProfitCenter_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtProfitCenter = (TextBox)grdCreditNoteDetails.Rows[RowIndex].FindControl("txtProfitCenter");
        HiddenField hdfProfitCenterID = (HiddenField)grdCreditNoteDetails.Rows[RowIndex].FindControl("hdfProfitCenterID");
        AppCreditNote CreditNote = (AppCreditNote)Session[TableConstants.CreditNote];
        CreditNote.CreditNoteDetailColl[RowIndex].ProfitCenter_ID = new AppUtility.AppConvert(hdfProfitCenterID);
        Session.Remove(TableConstants.CreditNote);
        Session.Add(TableConstants.CreditNote, CreditNote);
        BindData();
        txtProfitCenter.Focus();
    }

    protected void txtCostCenter_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtCostCenter = (TextBox)grdCreditNoteDetails.Rows[RowIndex].FindControl("txtCostCenter");
        HiddenField hdfCostCenterID = (HiddenField)grdCreditNoteDetails.Rows[RowIndex].FindControl("hdfCostCenterID");
        AppCreditNote CreditNote = (AppCreditNote)Session[TableConstants.CreditNote];
        CreditNote.CreditNoteDetailColl[RowIndex].CostCenter_ID = new AppUtility.AppConvert(hdfCostCenterID.Value);
        Session.Remove(TableConstants.CreditNote);
        Session.Add(TableConstants.CreditNote, CreditNote);
        BindData();
        txtCostCenter.Focus();
    }

    protected void txtDebit_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtDebit = (TextBox)grdCreditNoteDetails.Rows[RowIndex].FindControl("txtDebit");
        TextBox txtCredit = (TextBox)grdCreditNoteDetails.Rows[RowIndex].FindControl("txtCredit");
        AppCreditNote CreditNote = (AppCreditNote)Session[TableConstants.CreditNote];
        CreditNote.CreditNoteDetailColl[RowIndex].DebitAmount = new AppUtility.AppConvert(txtDebit.Text);
        CreditNote.CreditNoteDetailColl[RowIndex].CreditAmount = new AppUtility.AppConvert("0.00");

        Session.Remove(TableConstants.CreditNote);
        Session.Add(TableConstants.CreditNote, CreditNote);
        BindData();
        txtCredit.Focus();
    }

    protected void txtCredit_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtCredit = (TextBox)grdCreditNoteDetails.Rows[RowIndex].FindControl("txtCredit");
        Button btnAddLineItem = (Button)grdCreditNoteDetails.HeaderRow.FindControl("btnAddLineItem");
        AppCreditNote CreditNote = (AppCreditNote)Session[TableConstants.CreditNote];
        CreditNote.CreditNoteDetailColl[RowIndex].CreditAmount = new AppUtility.AppConvert(txtCredit.Text);
        CreditNote.CreditNoteDetailColl[RowIndex].DebitAmount = new AppUtility.AppConvert("0.00");
        Session.Remove(TableConstants.CreditNote);
        Session.Add(TableConstants.CreditNote, CreditNote);
        BindData();
        btnAddLineItem.Focus();
    }



    #endregion
    #region Grid View Events
    protected void grdCreditNoteDetails_PreRender(object sender, EventArgs e)
    {
        if (grdCreditNoteDetails.Rows.Count > 0)
        {
            grdCreditNoteDetails.UseAccessibleHeader = true;
            grdCreditNoteDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdCreditNoteDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
            AppCreditNote CreditNote = (AppCreditNote)Session[TableConstants.CreditNote];
            CreditNote.CreditNoteDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created)).ToList()[row.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
            Session.Add(TableConstants.CreditNote, CreditNote);
            BindData();
        }

    }
    #endregion
}