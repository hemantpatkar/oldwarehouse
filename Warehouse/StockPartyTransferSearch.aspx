﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" Culture="en-gb" AutoEventWireup="true" Inherits="StockPartyTransferSearch" Codebehind="StockPartyTransferSearch.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" DisplayAfter="0" AssociatedUpdatePanelID="grdUpdate"
        runat="server">
        <ProgressTemplate>
            <div class="modal fade in" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-keyboard="false" style="display: block">
                <div class="modal-dialog  modal-sm">
                    <h1 style="text-align: center;">
                        <i class="fa fa-truck fa-magic fa-2x faa-passing animated"></i>
                    </h1>
                </div>
            </div>
            <div class="modal-backdrop fade in"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-lg-9">
            <div class="heading">
                <i class="fa fa-truck"></i>
                <asp:Label ID="lblTitleLocationTransferSearch" Text="Party Transfer Search" runat="server"></asp:Label>
            </div>
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSearch_Click" Text="Search" ValidationGroup="Search" />
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnNew" OnClick="btnNew_Click" CssClass="btn btn-primary btn-block btn-sm" runat="server" AccessKey="n" CausesValidation="False" Text="New" />
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnExport" CssClass="btn btn-warning btn-block btn-sm" runat="server" OnClick="btnExportToExcel_Click" AccessKey="n" CausesValidation="False" Text="Export" />
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblTransferNo" Text="Transfer No" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtTransferNo" runat="server" CssClass="form-control input-sm" ValidationGroup="Search">
            </asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblTransferDate" Text="Transfer Date" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtTransferDate" runat="server"></asp:TextBox>
            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder=""
                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtTransferDate">
            </asp:MaskedEditExtender>
            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtTransferDate">
            </asp:CalendarExtender>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblCustomer" Text="Customer" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtCustomer" runat="server" CssClass="form-control input-sm" ValidationGroup="Search">
            </asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblStatus" Text="Status" CssClass="bold"></asp:Label>
            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control input-sm">
                <asp:ListItem Value="-1" Selected="True">All</asp:ListItem>
                <asp:ListItem Value="1">Un Approved</asp:ListItem>
                <asp:ListItem Value="2">Approved</asp:ListItem>
                <asp:ListItem Value="-10">Deleted</asp:ListItem>
            </asp:DropDownList>
            <asp:HiddenField ID="hdfgatereg" runat="server" />
            <asp:HiddenField ID="ShowLastRecords" runat="server" />
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-lg-12" style="overflow: auto">
            <asp:UpdatePanel ID="grdUpdate" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdStockPartyTransferSearch" runat="server" GridLines="None" AutoGenerateColumns="False" PagerStyle-CssClass="pagination-ys"
                        CssClass="table table-hover table-striped text-nowrap table-Full" AllowPaging="true" OnPageIndexChanging="grdStockPartyTransferSearch_PageIndexChanging" PageSize="50"
                        OnPreRender="grdStockPartyTransferSearch_PreRender">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblSrno" runat="server" Text='<%#Container.DataItemIndex+1%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:HyperLink ID="Lnksel" runat="server" CssClass="fa fa-edit fa-2x" CommandArgument='<%#Eval("ID") %>'
                                        NavigateUrl='<%# "StockPartyTransfer.aspx?ID=" + Eval("ID") %>' Target="_blank"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="From Customer">
                                <ItemTemplate>
                                    <asp:Label ID="lblVehicalNo" runat="server" ToolTip='<%#Eval("FromCustomer_Company.Name") %>'
                                        Text='<%#Eval("FromCustomer_Company.Name") %>'></asp:Label><br />
                                    <asp:Label ID="lblVehicalName" runat="server" Font-Bold="true" ForeColor="Maroon"
                                        Text='<%#Eval("FromCustomer_Company.Parent.Name") %>' ToolTip='<%#Eval("FromCustomer_Company.Parent.Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Customer">
                                <ItemTemplate>
                                    <asp:Label ID="lblVehicalNo" runat="server" ToolTip='<%#Eval("ToCustomer_Company.Name") %>'
                                        Text='<%#Eval("ToCustomer_Company.Name") %>'></asp:Label><br />
                                    <asp:Label ID="lblVehicalName" runat="server" Font-Bold="true" ForeColor="Maroon"
                                        Text='<%#Eval("ToCustomer_Company.Parent.Name") %>' ToolTip='<%#Eval("ToCustomer_Company.Parent.Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Transfer Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblpartyname" runat="server" ToolTip='<%# Convert.ToDateTime(Eval("TransferDate")).ToShortDateString() %>' Text='<%#Convert.ToDateTime(Eval("TransferDate")).ToShortDateString()  %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Order By">
                                <ItemTemplate>
                                    <asp:Label ID="lblpartyname" runat="server" ToolTip='<%# Eval("OrderBy")%>' Text='<%# Eval("OrderBy")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Modified By">
                                <ItemTemplate>
                                    <asp:Label ID="lblinunits" runat="server" ToolTip='<%# Eval("ModifiedBy") %>' Text='<%# Eval("ModifiedBy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Modified On">
                                <ItemTemplate>
                                    <asp:Label ID="lbloutunits" runat="server" ToolTip='<%# Convert.ToDateTime(Eval("ModifiedOn")).ToShortDateString() %>' Text='<%# Convert.ToDateTime(Eval("ModifiedOn")).ToShortDateString() %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>

