﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using AppObjects;
using AjaxControlToolkit;
using AppUtility;
using System.Data.Linq;
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class AutoComplete : WebService
{
    string RequestingUserID = "1"; //(string)System.Web.HttpContext.Current.Session["UserIdConst"];
    public AutoComplete() { }
    [WebMethod(EnableSession = true)]
    public List<string> ItemAttributeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);

        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppItemAttributeColl objColl = new AppItemAttributeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.ItemAttribute.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppItemAttribute item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> BankTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);

        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppBankTypeColl objColl = new AppBankTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.BankType.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppBankType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> CurrencyTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);

        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppCurrencyTypeColl objColl = new AppCurrencyTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.CurrencyType.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppCurrencyType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> StatutoryTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppStatutoryTypeColl objColl = new AppStatutoryTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.StatutoryType.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppStatutoryType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> DesignationTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppDesignationTypeColl objColl = new AppDesignationTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.DesignationType.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppDesignationType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> DepartmentTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppDepartmentTypeColl objColl = new AppDepartmentTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.DepartmentType.Code, AppUtility.Operators.Like, prefixText, 0);
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.DepartmentType.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppDepartmentType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Code + " - " + item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> AccountTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppAccountTypeColl objColl = new AppAccountTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.AccountType.AccountTypeName, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppAccountType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.AccountTypeName, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> LedgerTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppLedgerTypeColl objColl = new AppLedgerTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.LedgerType.LedgerTypeName, AppUtility.Operators.Like, prefixText, 0);

            }

            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppLedgerType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.LedgerTypeName, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> LedgerSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        string[] Paramerter = contextKey.Split(',');
        string LedgerType_ID = new AppUtility.AppConvert(Paramerter[0]); // always pass ledger type on 0th position 
        string LedgerFlag = Paramerter.Count() > 1 ? new AppUtility.AppConvert(Paramerter[1]) : ""; // always pass ledger Flag on 1 st position 
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppLedgerColl objColl = new AppLedgerColl(intRequestingUserID);

            if (!string.IsNullOrEmpty(LedgerType_ID))
            {
                int LedgerTypeID = new AppUtility.AppConvert(LedgerType_ID);
                objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Ledger.LedgerType_ID, AppUtility.Operators.Equals, LedgerTypeID, 0);

            }
            if (!string.IsNullOrEmpty(LedgerFlag))
            {
                string[] ParmLedgerFlag = LedgerFlag.Split('~');
                string ControlAccount = new AppUtility.AppConvert(ParmLedgerFlag[0]);
                string LedgerGroup = new AppUtility.AppConvert(ParmLedgerFlag[1]);
                string GeneralLedger = new AppUtility.AppConvert(ParmLedgerFlag[2]);
                if (!string.IsNullOrEmpty(ControlAccount))
                {
                    objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.Ledger.LedgerFlag, AppUtility.Operators.Equals, ControlAccount, 0);
                }
                if (!string.IsNullOrEmpty(LedgerGroup))
                {
                    objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.Ledger.LedgerFlag, AppUtility.Operators.Equals, LedgerGroup, 0);
                }
                if (!string.IsNullOrEmpty(GeneralLedger))
                {
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Ledger.LedgerFlag, AppUtility.Operators.Equals, GeneralLedger, 0);
                }

            }
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Ledger.LedgerName, AppUtility.Operators.Like, prefixText, 0);

            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppLedger item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.LedgerName, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> CategoryTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppCategoryTypeColl objColl = new AppCategoryTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.CategoryType.Code, AppUtility.Operators.Like, prefixText, 0);
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.CategoryType.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppCategoryType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Code + " - " + item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> UOMTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppUOMTypeColl objColl = new AppUOMTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.UOMType.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppUOMType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> PriceGroupSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppPriceGroupColl objColl = new AppPriceGroupColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.PriceGroup.Code, AppUtility.Operators.Like, prefixText, 0);
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.PriceGroup.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppPriceGroup item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Code + " - " + item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> ItemSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppItemColl objColl = new AppItemColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.Item.Code, AppUtility.Operators.Like, prefixText, 0);
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.Item.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppItem item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Code + " - " + item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> TaxTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppTaxTypeColl objColl = new AppTaxTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.TaxType.Code, AppUtility.Operators.Like, prefixText, 0);
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.TaxType.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppTaxType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Code + " - " + item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> OrganizationTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppOrganizationTypeColl objColl = new AppOrganizationTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.OrganizationType.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppOrganizationType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> TaxTypeConfigurationSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppTaxTypeConfigurationColl objColl = new AppTaxTypeConfigurationColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.TaxTypeConfiguration.Code, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppTaxTypeConfiguration item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Code, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> CompanySearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        string[] Paramerter = contextKey.Split(',');
        int Type = new AppConvert(Paramerter[0]);

        int Parent_ID = 0;
        if (Paramerter.Count() > 1)
        {
            Parent_ID = new AppConvert(Paramerter[1]);
        }

        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppCompanyColl objColl = new AppCompanyColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.Company.Code, AppUtility.Operators.Like, prefixText, 0);
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.Company.Name, AppUtility.Operators.Like, prefixText, 0);

            }
            if (Type != 0)
            {
                objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Company.Type, AppUtility.Operators.Equals, Type, 0);
            }
            if (Parent_ID != 0)
            {
                objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Company.Parent_ID, AppUtility.Operators.Equals, Parent_ID, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppCompany item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Code + " - " + item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> AppComponentSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppAppComponentColl objColl = new AppAppComponentColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.AppComponent.Name, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppAppComponent item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> TermsAndConditionsSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);

            AppObjects.AppTermsAndConditionsColl objColl = new AppTermsAndConditionsColl(intRequestingUserID);
            //objColl.AddCriteria(AppUtility.CollOperator.AND, TermsAndConditions.Parent_ID, AppUtility.Operators.Equals, 0, 0);
            //if (!prefixText.Equals("*"))
            //{
            //    objColl.AddCriteria(AppUtility.CollOperator.AND, TermsAndConditions.Statement, AppUtility.Operators.Like, prefixText, 0);
            //}
            objColl.Search(RecordStatus.AllActive, count);

            foreach (AppObjects.AppTermsAndConditions item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Statement, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> CompanyAddressSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        string[] Paramerter = contextKey.Split(',');
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);

            AppObjects.AppCompanyAddressColl objColl = new AppCompanyAddressColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, CompanyAddress.Address1, AppUtility.Operators.Like, prefixText, 0);
                objColl.AddCriteria(AppUtility.CollOperator.OR, CompanyAddress.Address2, AppUtility.Operators.Like, prefixText, 0);
            }
            if (new AppUtility.AppConvert(Paramerter[0]) != 0)
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, CompanyAddress.Company_ID, AppUtility.Operators.Equals, Paramerter[0], 0);
            }
            objColl.Search(RecordStatus.AllActive, count);

            foreach (AppObjects.AppCompanyAddress item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Address1 + " " + item.Address2, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> CompanyUserSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppCompanyUserColl objColl = new AppCompanyUserColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.CompanyUser.LoginName, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppCompanyUser item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.LoginName.ToString(), item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> VendorItemSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppCompany obj = new AppCompany(intRequestingUserID, new AppUtility.AppConvert(contextKey));
            int status = new AppUtility.AppConvert((int)RecordStatus.Active);
            var objResult = obj.VendorItemColl.Where(o => o.Status == status);
            if (prefixText.Equals("*"))
                objResult = obj.VendorItemColl.Take(count);
            else
                objResult = obj.VendorItemColl.Where(o => o.Item.Code.ToUpper().Contains(prefixText.ToUpper()) || o.Item.Name.ToUpper().Contains(prefixText.ToUpper()));

            foreach (AppObjects.AppVendorItem item in objResult)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Item.Code + " - " + item.Item.Name, item.Item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> CustomerItemSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            //AppObjects.AppCompany obj = new AppCompany(intRequestingUserID, new AppUtility.AppConvert(contextKey));  
            AppObjects.AppCompany obj = new AppCompany(intRequestingUserID, 22);
            int status = new AppUtility.AppConvert((int)RecordStatus.Active);
            var objResult = obj.CustomerItemColl.Where(o => o.Status == status);
            if (prefixText.Equals("*"))
                objResult = obj.CustomerItemColl.Take(count);
            else
                objResult = obj.CustomerItemColl.Where(o => o.Item.Code.ToUpper().Contains(prefixText.ToUpper()) || o.Item.Name.ToUpper().Contains(prefixText.ToUpper()));

            foreach (AppObjects.AppCustomerItem item in objResult)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Item.Code + " - " + item.Item.Name, item.Item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> PurchaseOrderSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppPurchaseOrderColl objColl = new AppPurchaseOrderColl(intRequestingUserID);
            if(!string.IsNullOrEmpty(contextKey))
            {
                objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseOrder.Vendor_Company_ID, AppUtility.Operators.Equals, contextKey, 0);
            }
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseOrder.PurchaseOrderNumber, AppUtility.Operators.Like, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppPurchaseOrder item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.PurchaseOrderNumber, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> SaleOrderSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            string[] Paramerter = contextKey.Split(',');
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppSaleOrderColl objColl = new AppSaleOrderColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleOrder.SaleOrderNumber, AppUtility.Operators.Equals, prefixText, 0);
            }
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleOrder.Delivery_Company_ID, AppUtility.Operators.Equals, Paramerter[0], 0);
            //objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleOrder.Status, AppUtility.Operators.GreaterOrEqualTo, new AppUtility.AppConvert((int)RecordStatus.Active),0);
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppSaleOrder item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.SaleOrderNumber, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }


    [WebMethod(EnableSession = true)]
    public List<string> VehicleTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppVechicleTypeColl objColl = new AppVechicleTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.VechicleType.Code, AppUtility.Operators.StartWith, prefixText, 0);
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.VechicleType.Name, AppUtility.Operators.StartWith, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppVechicleType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> GateEntrySearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppGateEntryColl objColl = new AppGateEntryColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppObjects.GateEntry.GateNo, AppUtility.Operators.Like, prefixText);

            }
            //objColl.AddCriteria(AppObjects.GateEntry.Status, AppUtility.Operators.Equals, (int)RecordStatus.Active);
            objColl.Search(RecordStatus.Active, count);
            foreach (AppObjects.AppGateEntry item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.GateNo, item.ID.ToString() + "~" + item.VehicalNo + "~" + item.ChallanNo));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> PreInwardsearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppConvert(RequestingUserID);
            AppPreInwardColl objColl = new AppPreInwardColl(intRequestingUserID);
            objColl.AddCriteria(PreInward.Status, Operators.Equals, (int)RecordStatus.Approve);

            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(PreInward.PreInwardNo, Operators.Like, prefixText);
            }

            objColl.Search(RecordStatus.Active, count);
            foreach (AppPreInward item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.PreInwardNo, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> SchemeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        string[] Parameter = contextKey.Split(',');
        int Item_ID = new AppConvert(Parameter[0]);
        DateTime EffectiveDate = new AppConvert(Parameter[1]);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppConvert(RequestingUserID);
            AppStorageSchemeRateColl objColl = new AppStorageSchemeRateColl(intRequestingUserID);
            objColl.AddCriteria(StorageSchemeRate.Item_ID, Operators.Equals, Item_ID);
            objColl.AddCriteria(StorageSchemeRate.EffectiveDate, Operators.LessOrEqualTo, EffectiveDate);

            //if (!prefixText.Equals("*"))
            //{
            //    objColl.AddCriteria(CollOperator.OR, StorageScheme.Code, Operators.StartWith, prefixText, 0);
            //    objColl.AddCriteria(CollOperator.OR, StorageScheme.Name, Operators.StartWith, prefixText, 0);
            //}

            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppStorageSchemeRate item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.StorageScheme.Code, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> StorageLocationSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppStorageLocationColl objColl = new AppStorageLocationColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.StorageLocation.Code, AppUtility.Operators.StartWith, prefixText, 0);

            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppStorageLocation item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Code, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> MaterialOrderSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppMaterialOrderColl objColl = new AppMaterialOrderColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(MaterialOrder.MaterialOrderNo, Operators.Like, prefixText);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppMaterialOrder item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.MaterialOrderNo, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }


    [WebMethod(EnableSession = true)]
    public List<string> OriginSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppOriginColl objColl = new AppOriginColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.Origin.Code, AppUtility.Operators.StartWith, prefixText, 0);
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.Origin.Name, AppUtility.Operators.StartWith, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppOrigin item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> BrandSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppBrandColl objColl = new AppBrandColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.Brand.Code, AppUtility.Operators.StartWith, prefixText, 0);
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.Brand.Name, AppUtility.Operators.StartWith, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppBrand item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }
    [WebMethod(EnableSession = true)]
    public List<string> VarietySearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppVarietyColl objColl = new AppVarietyColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.Variety.Code, AppUtility.Operators.StartWith, prefixText, 0);
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.Variety.Name, AppUtility.Operators.StartWith, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppVariety item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }

    [WebMethod(EnableSession = true)]
    public List<string> ZipCodeTypeSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppZipCodeTypeColl objColl = new AppZipCodeTypeColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.ZipCodeType.ZipCode, AppUtility.Operators.StartWith, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppZipCodeType item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.ZipCode, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }

    [WebMethod(EnableSession = true)]
    public List<string> TDSSearch(String prefixText, int count, String contextKey)
    {
        List<string> lstResult = new List<string>(count);
        if (!string.IsNullOrEmpty(RequestingUserID))
        {
            int intRequestingUserID = new AppUtility.AppConvert(RequestingUserID);
            AppObjects.AppTDSColl objColl = new AppTDSColl(intRequestingUserID);
            if (!prefixText.Equals("*"))
            {
                objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.TDS.Name, AppUtility.Operators.StartWith, prefixText, 0);
            }
            objColl.Search(RecordStatus.AllActive, count);
            foreach (AppObjects.AppTDS item in objColl)
                lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem(item.Name, item.ID.ToString()));
        }
        if (lstResult.Count == 0)
        {
            lstResult.Add(AutoCompleteExtender.CreateAutoCompleteItem("No Records Found", ""));
        }
        return lstResult;
    }


}
