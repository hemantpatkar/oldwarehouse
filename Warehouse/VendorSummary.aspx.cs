﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to display all vendor lists.
*/
#endregion
#region Using
using System;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.UI;
#endregion
public partial class VendorSummary : BigSunPage
{
    AppCompanyColl objCompany;
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }
    #endregion
   
    #region GridView Events
    protected void grdVendorSummary_PreRender(object sender, EventArgs e)
    {
        if (grdVendorSummary.Rows.Count > 0)
        {
            grdVendorSummary.UseAccessibleHeader = true;
            grdVendorSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdVendorSummary_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        objCompany = (AppCompanyColl)Session[TableConstants.VendorCollSession];
        objCompany[e.RowIndex].Status = new AppUtility.AppConvert((int)RecordStatus.Deleted);
        objCompany[e.RowIndex].Save();
        Session.Add(TableConstants.VendorCollSession, objCompany);
        BindData();
    }
    #endregion   
    #region Button Click Events
    protected void lnkNewVendor_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.VendorSessionObj);
        Response.Redirect("VendorDetails.aspx");
    }

    public void BindData()
    {
        AppCompanyColl objCollection = new AppCompanyColl(intRequestingUserID);
        RecordStatus Status = CommonFunctions.GetStatus(ddlStatus.SelectedValue);
        objCollection.Clear();
        if (!string.IsNullOrEmpty(txtVendorCode.Text))
        {
            objCollection.AddCriteria(AppObjects.Company.Code, AppUtility.Operators.Like, txtVendorCode.Text);
        }

        if (!string.IsNullOrEmpty(txtVendorName.Text))
        {
            objCollection.AddCriteria(AppObjects.Company.Name, AppUtility.Operators.Like, txtVendorName.Text);
        }

        if (ddlStatus.SelectedValue != "-32768")
        {
            objCollection.AddCriteria(AppObjects.Company.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objCollection.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Company.Type, AppUtility.Operators.Equals, (int)CompanyRoleType.Vendor, 1);
        objCollection.Search(RecordStatus.ALL);
        grdVendorSummary.DataSource = objCollection;
        grdVendorSummary.DataBind();
        Session.Add(TableConstants.VendorCollSession, objCollection);
        if (objCollection.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Records Found','2'" + ");", true);
        }
    }
    protected void Search_Click(object sender, EventArgs e)
    {
        BindData();
    }
    #endregion
    protected void grdVendorSummary_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string CommandName = e.CommandName;
        int commandArgument = new AppUtility.AppConvert(e.CommandArgument);
        if (CommandName == "APPROVE")
        {
            objCompany = (AppCompanyColl)Session[TableConstants.VendorCollSession];
            objCompany[commandArgument].Status = new AppUtility.AppConvert((int)RecordStatus.Approve);
            objCompany[commandArgument].Save();
            Session.Add(TableConstants.VendorCollSession, objCompany);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Vendor Successfully Approved','1'" + ");", true);
        }
    }
}