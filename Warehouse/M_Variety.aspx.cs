﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppUtility;
using AppObjects;
public partial class M_Variety : BigSunPage
{
    AppVarietyColl obj;
    protected void Page_Load(object sender, EventArgs e)
    {
  
        obj = new AppObjects.AppVarietyColl(intRequestingUserID);
        Session[TableConstants.SessionVariety] = obj;
        if (!IsPostBack)
        {
           BindData();
        }
    }
    #region GridViewEvents
   
    protected void grdVariety_PreRender(object sender, EventArgs e)
    {
        if (grdVariety.Rows.Count > 0)
        {
            grdVariety.UseAccessibleHeader = true;
            grdVariety.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdVariety_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if (strCommandName == "INSERT")
        {       
            HiddenField hdfVarietyID = grdVariety.FooterRow.FindControl("hdfVarietyID") as HiddenField;
            TextBox txtCode = grdVariety.FooterRow.FindControl("txtNewCode") as TextBox;
            TextBox txtName = grdVariety.FooterRow.FindControl("txtNewName") as TextBox;
            CheckBox chkActiveVariety = grdVariety.FooterRow.FindControl("chkActiveVariety") as CheckBox;
            CheckBox chkDefaultVariety = grdVariety.FooterRow.FindControl("chkDefaultVariety") as CheckBox;
            SaveVarietyDetails(strCommandID, txtCode.Text, txtName.Text, chkActiveVariety.Checked, chkDefaultVariety.Checked);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Successfully Added','1'" + ");", true);
            grdVariety.EditIndex = -1;
            BindData();
        }
        else if (strCommandName == "CANCELADD")
        {
            grdVariety.ShowFooter = false;
            BindData();
        }
        else if (strCommandName == "CANCELEDIT")
        {
            grdVariety.EditIndex = -1;
            BindData();
        }
        else if(strCommandName=="ADDNEW")
        {
            grdVariety.EditIndex = -1;
            grdVariety.ShowFooter = true;
            BindData();
        }
    }
    protected void grdVariety_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdVariety.ShowFooter = false;
        grdVariety.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdVariety_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HiddenField hdfID = grdVariety.Rows[e.RowIndex].FindControl("hdfID") as HiddenField;
        AppVariety SingleObj = new AppVariety(intRequestingUserID);
        SingleObj.ID = new AppConvert(hdfID.Value);
        SingleObj.Status = new AppConvert((int)RecordStatus.Deleted);
        SingleObj.Save();
     
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void grdVariety_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strCommandID = new AppConvert(e.RowIndex);
        HiddenField hdfVarietyID = grdVariety.Rows[e.RowIndex].FindControl("hdfVarietyID") as HiddenField;
        TextBox txtCode = grdVariety.Rows[e.RowIndex].FindControl("txtCode") as TextBox;
        TextBox txtName = grdVariety.Rows[e.RowIndex].FindControl("txtName") as TextBox;
        CheckBox chkIsActiveVariety = grdVariety.Rows[e.RowIndex].FindControl("chkIsActiveVariety") as CheckBox;
        CheckBox chkIsDefaultVariety = grdVariety.Rows[e.RowIndex].FindControl("chkIsDefaultVariety") as CheckBox;

        SaveVarietyDetails(hdfVarietyID.Value, txtCode.Text, txtName.Text , chkIsActiveVariety.Checked, chkIsDefaultVariety.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Successfully Updated','1'" + ");", true);
        grdVariety.EditIndex = -1;
        BindData();
    }
    #endregion
    #region MiscFunctions
    public void SaveVarietyDetails(string Id, string Code, string Name, bool IsActive, bool IsDefault)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppVariety SingleObj = new AppVariety(intRequestingUserID);      
        SingleObj.ID = new AppConvert(Id);
        SingleObj.Code = new AppConvert(Code);
        SingleObj.Name = new AppConvert(Name);
        SingleObj.Status = (IsActive == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));        
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;

        Session.Add(TableConstants.SessionVariety, obj);
        SingleObj.Save();
        BindData();
    }
    

    protected void BindData()
    {
        AppObjects.AppVarietyColl objColl = new AppVarietyColl(intRequestingUserID);
        if (!string.IsNullOrEmpty(txtName.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Variety.Name, AppUtility.Operators.Equals, txtName.Text, 0);
        }
        if (!string.IsNullOrEmpty(txtCode.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Variety.Code, AppUtility.Operators.Equals, txtCode.Text, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
             
            objColl.AddCriteria(AppObjects.Variety.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.SessionVariety] = objColl;
        if (objColl != null)
        {
            grdVariety.DataSource = objColl;
            grdVariety.DataBind();
            if (objColl.Count <= 0)
            {
                AddEmptyRow();
            }
        }
       
       
    }
    public void AddEmptyRow()
    {
        AppVarietyColl collOfContacts = new AppVarietyColl(intRequestingUserID);
        AppVariety newContact = new AppVariety(intRequestingUserID);
        newContact.Code = "Add atleast one Variety";
        collOfContacts.Add(newContact);
        grdVariety.DataSource = collOfContacts;
        grdVariety.DataBind();
        grdVariety.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdVariety.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdVariety.ShowFooter = true;
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}