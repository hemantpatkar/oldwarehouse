﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using System.Web.UI.HtmlControls;

public partial class StockOrder : BigSunPage
{

    #region Variable Declaration

    static int focus;
    public string str;
    public Int16 overide;
    public Int16 outpopflag = 0;
    public Int16 Instpopflag = 0;
    static int flag = 0;
    static int temp = 0;
    private int hisflag = 0;
    public int old;
    List<int> InwardDetail_IDs;
    static int stockflag = 0;

    AppMaterialOrder objAppMaterialOrder = null;
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Material" + " " + "Order";
        dateval();
        objAppMaterialOrder = new AppMaterialOrder(intRequestingUserID);
        InwardDetail_IDs = new List<int>();
        if (!IsPostBack)
        {
            Session.Add(TableConstants.InwardDetailIDSession, InwardDetail_IDs);
            Session.Add(TableConstants.MaterialOrderSession, objAppMaterialOrder);

            DecideLanguage(new AppConvert(Session["LanguageCode"]));

            ViewState["PostBackID"] = Guid.NewGuid().ToString();
            rgvOrderDate.MinimumValue = "30-12-2000";
            BydefaultclickEnter();
            TimeStamp();
            if (new AppConvert(Request.QueryString["ID"]) != "" && Request.QueryString["ID"] != null)
            {
                string[] ids;
                int id;

                if (Request.QueryString["ID"].Contains(","))
                {
                    ids = Request.QueryString["ID"].Split(',');
                    id = Convert.ToInt32(ids[0]);
                }
                else
                {
                    id = Convert.ToInt32(Request.QueryString["ID"]);
                }
                hdfMOID.Value = new AppConvert(Request.QueryString["ID"]);
                FillMaterialOrderDetail(new AppConvert(Request.QueryString["ID"]));
                history();
            }
            else
            {
                txtDeliverDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtOrderDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtDateCompare.Text = DateTime.Now.ToString("dd/MM/yyyy");
                hdfrevised.Value = "";
                txtLotSearch.Focus();
                searchlot.Attributes.Add("style", "display:");
            }
        }

    }
    #endregion

    #region Function

    protected bool ValidationCurrentStockCheck()
    {
        List<Int32> check = new List<Int32>();
        objAppMaterialOrder = (AppMaterialOrder)Session[TableConstants.MaterialOrderSession];
        foreach (var item in objAppMaterialOrder.MaterialOrderDetailColl)
        {
            AppStockSearchVWColl objAppStockSearchVWColl = new AppStockSearchVWColl(intRequestingUserID);
            AppStockSearchVW objAppStockSearchVW = new AppStockSearchVW(intRequestingUserID);
            objAppStockSearchVWColl.Clear();
            objAppStockSearchVWColl.AddCriteria(StockSearchVW.InwardDetail_ID, Operators.Equals, item.InwardDetail_ID);
            objAppStockSearchVWColl.Search();
            objAppStockSearchVW = objAppStockSearchVWColl.FirstOrDefault();
            Decimal NetQuantity = new AppConvert(objAppStockSearchVW.NetQuantity);
            if (item.ID != 0)
            {
                AppMaterialOrderDetail objAppMaterialOrderDetail = new AppMaterialOrderDetail(intRequestingUserID, item.ID);
                NetQuantity = NetQuantity + objAppMaterialOrderDetail.BalOrderQuantity;
            }
            if (item.OrderQuantity > NetQuantity)
            {
                check.Add(1);
            }
        }
        if (check.Count > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public static void OpenNewWindow(System.Web.UI.Page page, string fullUrl, string key)
    {
        string script = "window.open('" + fullUrl + "', '" + key + "', 'status=1,location=1,menubar=1,resizable=1,toolbar=1,scrollbars=1,titlebar=1');";
        page.ClientScript.RegisterClientScriptBlock(page.GetType(), key, script, true);
    }

    #region Save Function
    protected void SaveMaterialOrder()
    {
        #region Validation

        if (ValidateOrder() == false)
        {
            Response.Write("<script LANGUAGE='JavaScript'> window.confirm('Do You Want to Refresh?');document.location='" + ResolveClientUrl("StockOrder.aspx?MOID=" + hdfMOID.Value) + "';</script>");
            return;
        }

        if (ValidationCurrentStockCheck() == false)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Stock has been modified.Please check" + "','2'" + ");", true);
            return;
        }


        #endregion

        #region Code For Save
        if (validatePostback())
        {

            try
            {
                objAppMaterialOrder = (AppMaterialOrder)Session[TableConstants.MaterialOrderSession];
                var DistinctList = objAppMaterialOrder.MaterialOrderDetailColl
                           .GroupBy(a => new
                           {
                               a.DeliveryLocation,
                               a.StorageSchemeRate.StorageScheme.SchemeCategory_ID,
                               a.InwardDetail.Inward.Customer_Company_ID
                           })
                               .Select(x => new { x.Key.DeliveryLocation, x.Key.Customer_Company_ID, x.Key.SchemeCategory_ID }).ToList();

                // DistrinctLIst Select Distinct Company ID, SchemeCategoryID, Location === .Toslist()
                foreach (var obj in DistinctList)
                {
                    var List1 = objAppMaterialOrder.MaterialOrderDetailColl.Where(o => o.InwardDetail.Inward.Customer_Company_ID == obj.Customer_Company_ID &&
                                                                     o.StorageSchemeRate.StorageScheme.SchemeCategory_ID == obj.SchemeCategory_ID &&
                                                                     o.DeliveryLocation == obj.DeliveryLocation && o.OrderQuantity > 0);

                    // save header
                    #region Material Order

                    AppMaterialOrder MO = new AppMaterialOrder(intRequestingUserID);
                    int MOID = Convert.ToInt32("0" + hdfMOID.Value);
                    if (MOID > 0)
                    {
                        MO.ID = MOID;
                        MO.DeliverLocation = txtDeliverTo.Text;
                        MO.MaterialOrderNo = txtMONo.Text;
                    }
                    else
                    {
                        MO.MaterialOrderNo = "MO" + new AppConvert(AppUtility.AutoID.GetNextNumber(1, "MO", System.DateTime.Now, 4, ResetCycle.Yearly));
                        if (obj.DeliveryLocation != "")
                            MO.DeliverLocation = new AppConvert(obj.DeliveryLocation);
                        else
                            MO.DeliverLocation = txtDeliverTo.Text;
                    }

                    MO.ModifiedBy = new AppConvert(Session["UserID"]);
                    MO.ModifiedOn = DateTime.Now;
                    MO.Customer_Company_ID = obj.Customer_Company_ID;
                    MO.MaterialOrderDate = new AppConvert(txtOrderDate.Text);
                    MO.Remarks = new AppConvert(txtRemarks.Text);
                    MO.TotalQuantity = new AppConvert(lblTotalQuantityToShow.Text);
                    MO.TotalPallete = new AppConvert(txtTotalPallete.Text);// As per Instruction given By Nithin Sir - If blank then Set it to 0 on 19/12/2012 10:30 AM
                    MO.VoucherType_ID = 0;    //For Internal Use only. 0 = Normal, 1= Transfer, 2 = Repacking
                    MO.VoucherID = 0;   //For Internal Use only. Transfer/Repacking Ref No
                    MO.Status = ((int)RecordStatus.Approve);    // Always 1                        
                    MO.OrderBy = new AppConvert(txtOrderBy.Text);
                    MO.Transporter = txtTransporter.Text;
                    MO.DeliveryDate = new AppConvert(txtDeliverDate.Text);

                    str = str + MO.MaterialOrderNo;
                    str = str + ",\\n";

                    #endregion

                    foreach (var item in List1)
                    {
                        //save Items
                        AppMaterialOrderDetail objAppMaterialOrderDetail = new AppMaterialOrderDetail(intRequestingUserID);
                        if (item.ID != 0)
                            objAppMaterialOrderDetail.ID = item.ID;
                        objAppMaterialOrderDetail.InwardDetail_ID = item.InwardDetail_ID;
                        objAppMaterialOrderDetail.Item_ID = item.Item_ID;
                        objAppMaterialOrderDetail.LotInternal = item.LotInternal;
                        objAppMaterialOrderDetail.LotVersion = item.LotVersion;
                        objAppMaterialOrderDetail.Status = ((int)RecordStatus.Active);
                        objAppMaterialOrderDetail.StorageSchemeRate_ID = item.StorageSchemeRate_ID;
                        objAppMaterialOrderDetail.ModifiedBy = new AppConvert(Session["UserID"]);
                        objAppMaterialOrderDetail.ModifiedOn = DateTime.Now;
                        objAppMaterialOrderDetail.InwardQuantity = item.InwardQuantity;
                        objAppMaterialOrderDetail.StockQuantity = item.StockQuantity;
                        objAppMaterialOrderDetail.MaterialOrder_ID = MO.ID;
                        objAppMaterialOrderDetail.OrderQuantity = item.OrderQuantity;
                        objAppMaterialOrderDetail.BalOrderQuantity = item.OrderQuantity;
                        MO.MaterialOrderDetailColl.Add(objAppMaterialOrderDetail);
                    }
                    MO.Save();
                }

                StringBuilder sb = new StringBuilder();
                //for (int s = 0; s < modt.Rows.Count; s++)
                //{
                //    str = str + modt.Rows[s]["MaterialOrderNo"].ToString();
                //    str = str + ",\\n";
                //    lstmoid.Add(Convert.ToInt32(modt.Rows[s]["MaterialOrder"]));
                //    sb.Append(modt.Rows[s]["MaterialOrder"]);
                //    sb.Append(",");

                //} 
                #region PDF Report

                string key = "Opener";
                string URL = ""; //"../../Reports/ReportView.aspx?MaterialOrderID=" + MOID + "&RPName=MatreialOrderSubReport&QtyFlag=1";
                                 //var repst = db.AppSettings.FirstOrDefault(a => a.AppSettingID == 115);
                                 //if (repst != null)
                                 //{
                                 //    string repstr = repst.AppSettingValue + "" + "&QtyFlag=1";
                                 //    URL = "../../Reports/ReportView.aspx?MaterialOrderID=" + MOID + "&RPName=" + repstr;
                                 //}
                                 //else
                                 //{
                                 //    URL = "../../Reports/ReportView.aspx?MaterialOrderID=" + MOID + "&RPName=MatreialOrderSubReport&QtyFlag=1";
                                 //}
                                 //OpenNewWindow(this, URL, key);
                #endregion

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "saveJS", @"save();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Saved Sucessfull" + "','1'" + ");", true);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Saved", "window.confirm('Saved Successful \\nMaterial Order Numbers \\n" + str + "');", true);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Open", "window.open('" + URL + "', '" + key + "', 'status=1,location=1,menubar=1,resizable=1,toolbar=1,scrollbars=1,titlebar=1');", true);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "success", "setInterval(function(){location.href='StockOrder.aspx';},1000);", true);

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Transaction Failed" + "','2'" + ");", true);
            }
        }
        else
        {
            Response.Write("<script LANGUAGE='JavaScript' > window.alert('Transaction Failed \\nPlease try again\\n" + str + "');document.location='" + ResolveClientUrl("StockOrder.aspx?str=") + "';</script>");
        }


        #endregion
    }
    #endregion

    #region Refresh

    protected bool ValidateOrder()
    {
        int MOID = Convert.ToInt32("0" + hdfMOID.Value);
        string strrev = "";
        AppMaterialOrder CheckAppMaterialOrder = new AppMaterialOrder(intRequestingUserID, MOID);
        if (MOID != 0)
        {
            strrev = new AppConvert(CheckAppMaterialOrder.ModifiedOn);
        }
        if (hdfrevised.Value == strrev)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion

    #region OrderStatus

    public void orderstaus(Int64 id)
    {
        //int outCount = 0;
        //var Modet = db.MaterialOrderDetails.Where(a => a.MaterialOrderID == id && a.IsActive == 1);
        //foreach (var item in Modet)
        //{
        //    outCount = outCount + item.BalanceOutwardQty;
        //}
        //MaterialOrder mo = new MaterialOrder();
        //mo = db.MaterialOrders.FirstOrDefault(a => a.MaterialOrderID == id);
        //if (outCount == 0)
        //{
        //    mo.OrderStatus = 3;
        //}
        //else
        //{
        //    if (hdfMOID.Value != "")
        //    {
        //        mo.OrderStatus = 2;
        //    }
        //    else
        //    {
        //        mo.OrderStatus = 1;
        //    }
        //}
        //db.SaveChanges();
    }

    #endregion

    #region TimeStamp

    public void TimeStamp()
    {
        int usertype = Convert.ToInt32(Session["UserTypeId"]);
        if (usertype != 1)
        {
            bottem.Visible = false;
        }
    }
    #endregion

    #region date validation


    public void dateval()
    {
        Rangeval.MinimumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangeval.MaximumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangback.MinimumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangback.MaximumValue = DateTime.Now.ToString("dd/MM/yyyy");
    }

    #endregion

    public void FillOverRidden(int PID)
    {
        //var Data = db.MaterialOrders.Where(x => x.PartnerID == PID && x.IsOverRidden == 1 && x.IsActive == 1);
        //GrdOverRidden.DataSource = Data.ToList();
        //GrdOverRidden.DataBind();
    }

    private bool validatePostback()
    {
        // verify duplicate postback
        string postbackid = ViewState["PostBackID"] as string;

        bool isValid = Cache[postbackid] == null;
        if (isValid)
            Cache.Insert(postbackid, true, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(10));
        return isValid;
    }

    public void ShowPartyDescription(List<string> InwardIds)
    {

        List<string> newInwardIds = new List<string>(InwardIds);
        if (InwardIds != null && InwardIds.Count() != 0)
        {
            //string inwdids = InwardIds;
            foreach (string item in InwardIds)
            {
                #region Code
                string[] IDs = item.ToString().Split(',');
                if (item != null && item.ToString() != "")
                {
                    //Int64 inwDetID = Convert.ToInt64(IDs[0]);
                    //InwardDetailQuantity iwd = new InwardDetailQuantity();
                    //iwd = db.InwardDetailQuantities.FirstOrDefault(p => p.InwardDetailQtyID == inwDetID);
                    //DataTable OverRide = GetOverRiddenSchema();
                    //if (ViewState["OverRide"] != "" && ViewState["OverRide"] != null)
                    //    OverRide = (DataTable)ViewState["OverRide"];
                    //bool found = false;
                    //DataRow[] Momatch = OverRide.Select("PartnerID='" + iwd.InwardDetail.PartnerID + "'"); // To check whether There is already Material Order,Outward,Inward Is created for Partner For particular Unit

                    //if (Momatch.Count() > 0)
                    //{
                    //    found = true;
                    //}

                    #region validation For Instruction Popup

                    //DataTable Instpop = GetInstPartner();
                    //if (ViewState["IntPop"] != "" && ViewState["IntPop"] != null)
                    //{
                    //    Instpop = (DataTable)ViewState["IntPop"];
                    //}
                    //bool Intflag = false;
                    //bool outflag = false;
                    //DataRow[] Instmatch = Instpop.Select("PartnerID='" + iwd.InwardDetail.Inward.PartnerID + "'");
                    //if (Instmatch.Count() > 0)
                    //{
                    //    Intflag = true;
                    //    Instpopflag = 1;
                    //}

                    #endregion

                    //if ((found == false) && ((iwd.InwardDetail.Inward.Partner.Instruction != "" && iwd.InwardDetail.Inward.Partner.Instruction != null) || (iwd.InwardDetail.Inward.PartnerGroup.Remarks != "" && iwd.InwardDetail.Inward.PartnerGroup.Remarks != null)) && (Intflag == false))
                    //{
                    //    #region Update outstanding Flag

                    //    DataTable outpop = GetOutschema();
                    //    if (ViewState["OutPop"] != "" && ViewState["OutPop"] != null)
                    //        outpop = (DataTable)ViewState["OutPop"];

                    //    DataRow[] Outmatch = outpop.Select("PartnerGroupID='" + iwd.InwardDetail.Inward.Partner.PartnerGroupID + "'"); // To check whether There is already Material Order,Outward,Inward Is created for Partner For particular Unit

                    //    if (Outmatch.Count() > 0)
                    //    {
                    //        outflag = true;
                    //        outpopflag = 1;
                    //    }

                    //    #endregion

                    //    #region Overriden
                    //    overide = 1;
                    //    PartyInst.Text = iwd.InwardDetail.Inward.Partner.Instruction;
                    //    GroupInst.Text = iwd.InwardDetail.Inward.PartnerGroup.Remarks;
                    //    HDfInwardIDs.Value = item;
                    //    txtOverRiddenRemark.Focus();
                    //    txtOverRiddenRemark.Text = "";
                    //    //GenericUtills.FillSupervisor("UserNamewithCode", "UserID", ddlOverRiddenBy, 2, 0);
                    //    FillOverRidden(iwd.InwardDetail.PartnerID);
                    //    //OutstandingAmt1.Filloutstanding(iwd.InwardDetail.PartnerID);
                    //    //LabourOutstanding1.Filloutstanding(iwd.InwardDetail.PartnerID);
                    //    //mdppinst.Show();
                    //    tempPtID.Value = iwd.InwardDetail.PartnerID.ToString();
                    //    //GenericUtills.popup_Show("modalInstruction", "ctl00_ContentPlaceHolder1_txtOverRiddenRemark", this);
                    //    #endregion
                    //}
                    //else
                    //{
                    //    //DataTable outpop = GetOutschema();
                    //    //if (ViewState["OutPop"] != "" && ViewState["OutPop"] != null)
                    //    //    outpop = (DataTable)ViewState["OutPop"];

                    //    //DataRow[] Outmatch = outpop.Select("PartnerGroupID='" + iwd.InwardDetail.Inward.Partner.PartnerGroupID + "'"); // To check whether There is already Material Order,Outward,Inward Is created for Partner For particular Unit

                    //    //if (Outmatch.Count() > 0)
                    //    //{
                    //    //    outflag = true;
                    //    //    outpopflag = 1;
                    //    //}
                    //    //overide = 0;
                    //    //HDfInwardIDs.Value = item;
                    //    //Outstanding.Filloutstanding(iwd.InwardDetail.PartnerID);
                    //    //LabourOutstanding.Filloutstanding(iwd.InwardDetail.PartnerID);
                    //    //if ((Outstanding.grdoutstnding.Rows.Count > 0 || LabourOutstanding.grdoutstnding.Rows.Count > 0) && outflag == false)
                    //    //{
                    //    //    //mploverride.Show();
                    //    //    tempPtID.Value = iwd.InwardDetail.PartnerID.ToString();
                    //    //    GenericUtills.popup_Show("modalOverride", btnovrOk.ClientID, this);
                    //    //    btnovrOk.Focus();
                    //    //    return;
                    //    //}
                    //    //else
                    //    //{
                    //    //    if (StockAlert(iwd.InwardDetail.PartnerID) == true)
                    //    //    {
                    //    //        stockflag = 0;
                    //    //        GenericUtills.popup_Show("StockAlert", btnovrOk.ClientID, this);
                    //    //        return;
                    //    //    }
                    //    //    else
                    //    //    {
                    //    //        ClickokOut(newInwardIds, 0);
                    //    //    }
                    //    //}
                    //}
                }

                #endregion
            }
            ViewState["InwardIds"] = newInwardIds;
        }
    }

    private void ClickokOut(List<string> InwardIds, int add)
    {
        //string[] IDs = HDfInwardIDs.Value.Split(',');
        //FillDescriptionGrid(IDs[0]);

        //Int64 inwd = Convert.ToInt64(IDs[0]);
        //int pid = db.InwardDetailQuantities.FirstOrDefault(x => x.InwardDetailQtyID == inwd).InwardDetail.PartnerID;

        //#region OutStanding
        //int? pgid = db.InwardDetailQuantities.FirstOrDefault(x => x.InwardDetailQtyID == inwd).InwardDetail.Inward.Partner.PartnerGroupID;
        //if (outpopflag == 0)
        //{
        //    DataTable outpop = new DataTable();
        //    if (ViewState["OutPop"] != "" && ViewState["OutPop"] != null)
        //        outpop = (DataTable)ViewState["OutPop"];
        //    else
        //        outpop = GetOutschema();
        //    DataRow outdr = outpop.NewRow();
        //    outdr["PartnerGroupID"] = pgid;
        //    outpop.Rows.Add(outdr);
        //    ViewState["OutPop"] = outpop;
        //}
        //#endregion

        //#region Instruction Popup

        //if (Instpopflag == 0)
        //{
        //    DataTable instpop = new DataTable();
        //    if (ViewState["IntPop"] != "" && ViewState["IntPop"] != null)
        //    {
        //        instpop = (DataTable)ViewState["IntPop"];
        //    }
        //    else
        //    {
        //        instpop = GetInstPartner();
        //    }
        //    DataRow Indr = instpop.NewRow();
        //    Indr["PartnerID"] = pid;
        //    instpop.Rows.Add(Indr);
        //    ViewState["IntPop"] = instpop;
        //}

        //#endregion

        //gvSearch.Rows[Convert.ToInt32(IDs[1])].Visible = false;
        //gvSearch.Rows[Convert.ToInt32(IDs[1])].Enabled = false;
        //CheckBox CheckBoxSelect = (CheckBox)gvSearch.Rows[Convert.ToInt32(IDs[1])].FindControl("CheckBoxSelect");
        //CheckBoxSelect.Checked = false;
        //InwardIds.Remove(HDfInwardIDs.Value);

        //#region OverRidden
        //if (overide == 1)
        //{
        //    DataTable OverRide = GetOverRiddenSchema();
        //    DataRow OverRider = OverRide.NewRow();
        //    if (ViewState["OverRide"] != "" && ViewState["OverRide"] != null)
        //        OverRide = (DataTable)ViewState["OverRide"];

        //    OverRider["OverRiddenDate"] = txtOrderDate.Text;
        //    OverRider["OverRiddenBy"] = ddlOverRiddenBy.SelectedValue;
        //    OverRider["PartnerID"] = pid;
        //    OverRider["Remarks"] = txtOverRiddenRemark.Text;
        //    OverRide.Rows.Add(OverRider);
        //    ViewState["OverRide"] = OverRide;
        //}
        //#endregion
        //ViewState["InwardIds"] = InwardIds;
        //if (InwardIds.Count() > 0 && InwardIds != null && add == 1)
        //{
        //    ShowPartyDescription(InwardIds);
        //}

    }

    #region F2 PopUp

    public void BindInwardItemDetail(Int32 InwardDetailID)
    {
        AppInwardDetail iwdet = new AppInwardDetail(intRequestingUserID, InwardDetailID);
        txtLotNo.Text = new AppConvert(iwdet.LotInternal);
        txtItemName.Text = new AppConvert(iwdet.Item.Code + "-" + iwdet.Item.Name);
        txtParty.Text = new AppConvert(iwdet.Inward.Customer_Company.Name);
        txtReceivedQuanity.Text = new AppConvert(iwdet.InwardQuantity);
        //txtBalanceQuantity.Text = new AppConvert(iwdet.Quantity);
        txtInwardno.Text = new AppConvert(iwdet.Inward.InwardNo);
        //txtOrderQuanityInDetail.Text = new AppConvert(iwdet.OrderQuantityForSearch);
        txtScheme.Text = iwdet.StorageSchemeRate.StorageScheme.Code + "-" + iwdet.StorageSchemeRate.StorageScheme.Name;
        txtDate.Text = new AppConvert(iwdet.Inward.InwardDate);
        txtPalletNo.Text = new AppConvert(iwdet.PalletNo);
        //txtPacking.Text = new AppConvert((iwdet.Item.);
        txtContainerNo.Text = new AppConvert(iwdet.ContainerNo);
        txtRemarkInPopup.Text = new AppConvert(iwdet.Inward.Remarks);
        txtVehicleNo.Text = new AppConvert(iwdet.Inward.GateEntry.VehicalNo);
    }

    public void BindOutwards(Int32 InwardDetailID)
    {
        AppOutwardDetailColl objAppOutwardDetailColl = new AppOutwardDetailColl(intRequestingUserID);
        objAppOutwardDetailColl.AddCriteria(OutwardDetail.InwardDetail_ID, Operators.Equals, InwardDetailID);
        objAppOutwardDetailColl.Search();

        grdOutwardDetails.DataSource = objAppOutwardDetailColl;
        grdOutwardDetails.DataBind();
        if (objAppOutwardDetailColl.Count() > 0)
        {
            lblTotalOutwardQtyToShow.Text = new AppConvert(objAppOutwardDetailColl.Sum(x => x.Quantity));
        }

    }

    public void BindMO(Int32 InwardDetailID)
    {
        decimal PendingOrderQuantity = 0;

        AppMaterialOrderDetailColl objAppMaterialOrderDetailColl = new AppMaterialOrderDetailColl(intRequestingUserID);
        objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.BalOrderQuantity, Operators.GreaterThan, 0);
        objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.InwardDetail_ID, Operators.Equals, InwardDetailID);
        objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.Status, Operators.GreaterThan, 0);
        objAppMaterialOrderDetailColl.Search();
        grdMaterialOrderDetails.DataSource = objAppMaterialOrderDetailColl;
        grdMaterialOrderDetails.DataBind();

        if (objAppMaterialOrderDetailColl.Count() > 0)
        {
            PendingOrderQuantity = objAppMaterialOrderDetailColl.Sum(x => x.BalOrderQuantity);
        }

        lblTotalPendingQtyToShow.Text = new AppConvert(PendingOrderQuantity);
    }

    public void BindLT(Int32 InwardDetailID)
    {
        AppTransferStockLocationDetailColl objAppTransferStockLocationDetailColl = new AppTransferStockLocationDetailColl(intRequestingUserID);
        objAppTransferStockLocationDetailColl.AddCriteria(TransferStockLocationDetail.InwardDetail_ID, Operators.Equals, InwardDetailID);
        objAppTransferStockLocationDetailColl.Search();

        grdlocationtransfer.DataSource = objAppTransferStockLocationDetailColl;
        grdlocationtransfer.DataBind();
        if (objAppTransferStockLocationDetailColl.Count > 0)
            lblTotalTransferQtyShow.Text = new AppConvert(objAppTransferStockLocationDetailColl.Sum(x => x.MovedQuantity));

    }

    #endregion

    #region Report

    public void ViewReport(string URLPath, EventArgs e)
    {
        StringBuilder strContent = new StringBuilder();
        string lasturl = System.Configuration.ConfigurationManager.AppSettings["ReportServerURL1"].ToString() + URLPath; // Define inside web.config file
        strContent.Append("window.open('" + lasturl + "','',',toolbar= 0,menubar= 0,status=0,location=0,resizable=1,scrollbars=1');");
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "subscribescript", strContent.ToString(), true);
    }

    #endregion

    private void DecideLanguage(string languageCode)
    {
        lblinsthead.Text = "Instructions";
        lblPartyInst.Text = "Customer" + " " + "Instruction";
        lblGroupInst.Text = "Customer" + " " + "Group" + " " + "Instruction";

        lblchkZero.Text = "Zero" + " " + "";
        lblMaterialOrder.Text = "Material" + " " + "Order";
        btnSave.Text = "Save";
        btnNewDetail.Text = "Create New";
        lblchkFyear.Text = "Previous" + " " + "" + " " + "Year";
        lblOrderDate.Text = "Order" + " " + "Date";
        lblOrderBy.Text = "Order" + " " + "By";
        lblDeliverDate.Text = "Delivery" + " " + "Date";
        lblDeliverTo.Text = "Delivery" + " " + "To";
        lblTotalQuantity.Text = "Total" + " " + "Qty";
        lblTotalPallete.Text = "Total" + " " + "Pallete";
        lblRemarks.Text = "Remarks";
        btnAddItems.Text = "Add";
        btnDelete.Text = "Delete";
        lblTransporter.Text = "Transporter";
        btnCancel.Text = "Cancel";
        btnOk.Text = "Ok";

        btnovrCancel.Text = "Cancel";
        btnovrOk.Text = "Ok";
        btnYes.Text = "Yes";
        btnNo.Text = "No";
        lblStockAlertMsg.Text = "ContinueMessage";
        lblheaderstock.Text = "StockAlert";
        lblInwardDetail.Text = "Stock" + " " + "Detail";
        lblLotNo.Text = "Lot" + " " + "No";
        lblItemName.Text = "Item" + " " + "Name";
        lblParty.Text = "Customer";
        lblReceivedQuanity.Text = "Received" + " " + "Quantity";
        lblBalanceQuantity.Text = "Balance" + " " + "Quantity";
        lblInwardno.Text = "Inward" + " " + "No";
        lblOrderedQtyInDetail.Text = "Order" + " " + "Quantity";
        lblScheme.Text = "Scheme";
        lblDate.Text = "Date";
        lblPalletNo.Text = "Pallete" + " " + "No";
        lblContainerNo.Text = "Container" + " " + "No";
        lblPacking.Text = "Packing";
        lblRemark.Text = "Remarks";
        lblVehicleNo.Text = "Vehicle" + " " + "No";
        lblOutwardDetails.Text = "Outward";
        lblTotalOutwardQty.Text = "Total" + " " + "Outward" + " " + "Quantity";
        lblMaterialOrderPending.Text = "Pending" + " " + "Material" + " " + "Orders";
        lblTotalPendingQty.Text = "Total" + " " + "Pending" + " " + "Quantity";
        btnCancelDetailPopup.Text = "Cancel";
        lblLocationTransfer.Text = "Location" + " " + "Transfer";
        lblTotalTransferQty.Text = "Total" + " " + "Transfer" + " " + "Quantity";

        btnhistory.Text = "History";

        lblOverRiddenBy.Text = "Approved" + " " + "By";
        lblOverRiddenRemark.Text = "Remarks";
        btnCancelLoction.Text = "Back";
        lblNetQty.Text = "Net" + " " + "Quantity";
        lblOrderQty.Text = "Order" + " " + "Quantity";
        btnAddDelLoc.Text = "Add";
        lblOutstanding.Text = "Outstanding";

        lblshowshotcut.Text = "Shortcuts";

        colshotcut.CollapsedText = "Show" + " " + "Shortcuts";
        colshotcut.ExpandedText = "Hide" + " " + "Shortcuts";
        lblhistoryremarks.Text = "Comments";
        btnhisCancel.Text = "Cancel";
        btnHisSave.Text = "Save";
        lblHistory.Text = "Material" + " " + "Order" + " " + "History";

    }

    public void BydefaultclickEnter()
    {
        txtOrderDate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtOrderBy.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtDeliverDate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtDeliverTo.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtRemarks.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtTransporter.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");

        UsrtxtPartnerGroupCode.Attributes.Add("onkeypress", "return clickButton(event,'" + btnLotSearch.ClientID + "')");
        UsrtxtPartnerCode.Attributes.Add("onkeypress", "return clickButton(event,'" + btnLotSearch.ClientID + "')");
        txtLotSearch.Attributes.Add("onkeypress", "return clickButton(event,'" + btnLotSearch.ClientID + "')");
        txtReceivedQuantitySearch.Attributes.Add("onkeypress", "return clickButton(event,'" + btnLotSearch.ClientID + "')");
        txtItemGroup.Attributes.Add("onkeypress", "return clickButton(event,'" + btnLotSearch.ClientID + "')");
        txtStorageItemName.Attributes.Add("onkeypress", "return clickButton(event,'" + btnLotSearch.ClientID + "')");
        txtpackingrefSearch.Attributes.Add("onkeypress", "return clickButton(event,'" + btnLotSearch.ClientID + "')");
        txtMarkSearch.Attributes.Add("onkeypress", "return clickButton(event,'" + btnLotSearch.ClientID + "')");
        txtVakkalSearch.Attributes.Add("onkeypress", "return clickButton(event,'" + btnLotSearch.ClientID + "')");
        txtSchemeSearch.Attributes.Add("onkeypress", "return clickButton(event,'" + btnLotSearch.ClientID + "')");
        txtBrand.Attributes.Add("onkeypress", "return clickButton(event,'" + btnLotSearch.ClientID + "')");
        txtOrigin.Attributes.Add("onkeypress", "return clickButton(event,'" + btnLotSearch.ClientID + "')");

        btnAddItems.Attributes.Add("onClick", "javascript:return validateCheckBoxes();");
        txtOverRiddenRemark.Attributes.Add("onkeydown", "return clickButton(event,'" + btnOk.ClientID + "')");
    }

    public void FillMaterialOrderDetail(string MOID)
    {


        #region Print Path checking
        //var strrep = db.AppSettings.FirstOrDefault(a => a.AppSettingID == 115);
        //if (strrep != null)
        //{
        //    string strre = strrep.AppSettingValue;

        //    btnPrint.NavigateUrl = "../../Reports/ReportView.aspx?MaterialOrderID=" + MOID + "&RPName=" + strre;
        //}
        //else
        //{
        //    btnPrint.NavigateUrl = "../../Reports/ReportView.aspx?MaterialOrderID=" + MOID + "&RPName=MatreialOrderSubReport";
        //}

        #endregion

        btnPrint.Visible = true;
        Int32 ID = Convert.ToInt32(MOID);
        hdfMOID.Value = MOID;
        objAppMaterialOrder = new AppMaterialOrder(intRequestingUserID, ID);

        txtMONo.Text = new AppConvert(objAppMaterialOrder.MaterialOrderNo);
        txtMONo.ToolTip = new AppConvert(objAppMaterialOrder.MaterialOrderNo);
        UsrlblPartnerGroup.Text = objAppMaterialOrder.Customer_Company.Parent.Name;
        UsrtxtPartnerGroupCode.Text = UsrlblPartnerGroup.Text;
        UsrhdfPartnerGroup.Value = objAppMaterialOrder.Customer_Company.Parent_ID.ToString();
        UsrhdfPartner.Value = objAppMaterialOrder.Customer_Company_ID.ToString();
        UserhdfPartner1.Value = objAppMaterialOrder.Customer_Company_ID.ToString();
        UsrlblPartner.Text = objAppMaterialOrder.Customer_Company.Name;
        UsrtxtPartnerCode.Text = UsrlblPartner.Text;
        hdfrevised.Value = new AppConvert(objAppMaterialOrder.ModifiedOn);
        txtOrderDate.Text = new AppConvert(objAppMaterialOrder.MaterialOrderDate);
        txtOrderDate.ToolTip = txtOrderDate.Text;
        txtOrderBy.Text = new AppConvert(objAppMaterialOrder.OrderBy);
        txtDeliverDate.Text = new AppConvert(objAppMaterialOrder.DeliveryDate); // To show blank
        txtDeliverDate.ToolTip = txtDeliverDate.Text;
        txtDeliverTo.Text = new AppConvert(objAppMaterialOrder.DeliverLocation);
        txtDeliverTo.ToolTip = txtDeliverTo.Text;
        lblTotalQuantityToShow.Text = new AppConvert(objAppMaterialOrder.TotalQuantity);
        lblTotalQuantityToShow.ToolTip = lblTotalQuantityToShow.Text;
        txtTotalPallete.Text = new AppConvert(objAppMaterialOrder.TotalPallete);
        txtTotalPallete.ToolTip = txtTotalPallete.Text;
        txtRemarks.Text = new AppConvert(objAppMaterialOrder.Remarks);
        txtRemarks.ToolTip = txtRemarks.Text;
        txtTransporter.Text = new AppConvert(objAppMaterialOrder.Transporter);
        txtTransporter.ToolTip = new AppConvert(objAppMaterialOrder.Transporter);

        if (objAppMaterialOrder.Status == 2)
        {
            btnSave.Visible = false;
        }

        #region Editing

        #region change by 

        //if (mo.OrderStatus != 1 || mo.VoucherType_ID == 7 || mo.BillingStatus != 0)
        //{
        //    if (mo.OrderStatus == 3)
        //    {
        //        blockFromEditing();
        //    }

        //    if (mo.OrderStatus == 2)
        //    {
        //        var qr1 = from ot in db.Outwards
        //                  join mor in db.MaterialOrders
        //                   on ot.MaterialOrderID equals mor.MaterialOrderID
        //                  join outd in db.OutwardDetails on ot.OutwardID equals outd.OutwardID
        //                  where mor.MaterialOrderID == mo.MaterialOrderID
        //                  select outd;
        //        int a = qr1.Count();

        //        var qr2 = from ot in db.Outwards
        //                  join mor in db.MaterialOrders
        //                   on ot.MaterialOrderID equals mor.MaterialOrderID
        //                  join outd in db.OutwardDetails on ot.OutwardID equals outd.OutwardID
        //                  where mor.MaterialOrderID == mo.MaterialOrderID && outd.IsActive == 2
        //                  select outd;
        //        int b = qr2.Count();

        //        if (a == b)
        //        {

        //        }
        //        else
        //        {
        //            blockFromEditing();
        //        }
        //    }
        //}

        //if (mo.BillingStatus == 1)
        //{
        //    Editing();
        //}

        #endregion

        #endregion

        #region datevalidation

        // new qty quantity changes date 2014/07/04
        //for (int i = 0; i < gvDescription.Rows.Count; i++)
        //{
        //    #region findid
        //    //HiddenField hdfInwardDetailID = (HiddenField)gvDescription.Rows[i].FindControl("hdfInwardDetailID");
        //    //int id = Convert.ToInt32("0" + hdfInwardDetailID.Value);
        //    HiddenField hdfInwardDetailQtyID = (HiddenField)gvDescription.Rows[i].FindControl("hdfInwardDetailQtyID");
        //    Int64 idqt = Convert.ToInt64("0" + hdfInwardDetailQtyID.Value);
        //    #endregion
        //    if (idqt != 0)
        //    {
        //        int date = compare(idqt);
        //        old = Convert.ToInt32("0" + ViewState["oldDate"]);
        //        if (old < date)
        //        {
        //            txtDateCompare.Text = GenericUtills.ConvertIntToDate(date);
        //            ViewState["oldDate"] = date;
        //        }
        //    }
        //}

        #endregion

        gvDescription.DataSource = objAppMaterialOrder.MaterialOrderDetailColl.Where(x => x.Status == ((int)RecordStatus.Active));
        gvDescription.DataBind();

        Session.Add(TableConstants.MaterialOrderSession, objAppMaterialOrder);
    }

    public void blockFromEditing()
    {
        //btnSave.Visible = false;
        //btnAddItems.Visible = false;
        //btnDelete.Visible = false;
        //btnSearch.Visible = false;
        //ddlPartner.Enabled = false;
        txtOrderDate.Enabled = false;
        //ddlOrderBy.Enabled = false;
        txtOrderBy.Enabled = false;
        txtDeliverDate.Enabled = false;
        txtDeliverTo.Enabled = false;
        txtTotalPallete.Enabled = false;
        txtRemarks.Enabled = false;
        txtTransporter.Enabled = false;


    }

    public void Editing()
    {
        btnSave.Enabled = false;
        btnSave.Visible = false;
        btnAddItems.Visible = false;
        btnAddItems.Enabled = false;
        btnDelete.Visible = false;
        btnDelete.Enabled = false;
        btnLotSearch.Visible = false;
        btnLotSearch.Enabled = false;
    }

    public void SearchOptimised()
    { 
        #region Get Financial Year
        //int YID = Convert.ToInt32(Session["Finyear"]);
        //DateTime startdate = Convert.ToDateTime(GenericUtills.convertIntToDate(db.mFinancialYears.FirstOrDefault(x => x.FYearID == YID).StartDate));
        //DateTime enddate = Convert.ToDateTime(GenericUtills.convertIntToDate(db.mFinancialYears.FirstOrDefault(x => x.FYearID == YID).EndDate));
        #endregion

        #region declare variables
        int PartnerGroup = Convert.ToInt32("0" + UsrhdfPartnerGroup.Value);
        int Customer = Convert.ToInt32("0" + UsrhdfPartner.Value);
        int LotNoInt = new AppConvert(txtLotSearch.Text.Trim());
        string Vakkal = new AppConvert(txtVakkalSearch.Text.Trim());
        string packingref = new AppConvert(txtpackingrefSearch.Text);
        string Mark = new AppConvert(txtMarkSearch.Text);
        string ItemName = new AppConvert(txtStorageItemName.Text);
        string SchemeName = new AppConvert(txtSchemeSearch.Text);
        string itemgroup = new AppConvert(txtItemGroup.Text);
        decimal InwardQty = new AppConvert(txtReceivedQuantitySearch.Text);         
        #endregion

        #region ZeroStock and Fyear Check
        
        if (chkZero.Checked == true && chkFyear.Checked == true)
        {
            if (txtScheme.Text == "" && txtLotNo.Text == "" && txtItemGroup.Text == "" && txtStorageItemName.Text == "" && txtMarkSearch.Text == "" && txtpackingrefSearch.Text == "" && txtVakkalSearch.Text == "" && txtReceivedQuantitySearch.Text == "" && PartnerGroup == 0 && Customer == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Please select atlease one parameter for searching" + "','2'" + ");", true);
                return;
            }
        }
        #endregion

        AppStockSearchVWColl objAppStockSearchVWColl = new AppStockSearchVWColl(intRequestingUserID);
 
        InwardDetail_IDs = (List<int>)Session[TableConstants.InwardDetailIDSession];
        objAppStockSearchVWColl.Search(0, PartnerGroup, Customer, 0, InwardQty, LotNoInt, Vakkal, Mark, packingref, ItemName, InwardDetail_IDs, new AppConvert(chkZero.Checked));

        if (txtLotSearch.Text.ToString() != "")
        {
            if (objAppStockSearchVWColl.Count() <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Lot No Not Present" + "','2'" + ");", true);
                txtLotSearch.Focus();
                return;
            }
        }
        gvSearch.DataSource = objAppStockSearchVWColl;
        gvSearch.DataBind();

    }

    public void FillDescriptionGrid(string InwardIds)
    {
        //if (gvDescription.Rows.Count > 0)
        //{
        //    DataTable dt = AppendNewData((DataTable)GetGridData(0), InwardIds);
        //    gvDescription.DataSource = dt;
        //    gvDescription.DataBind();
        //}
        //else
        //{
        //    lblMsg.Text = "Error";
        //    return;
        //}
    }

    public void TotalQuantityCount()
    {
        int count = 0;
        for (int rowindex = 0; rowindex < gvDescription.Rows.Count; rowindex++)
        {
            TextBox txtOrderQuantity = (TextBox)gvDescription.Rows[rowindex].FindControl("txtOrderQuantity");
            if (txtOrderQuantity.Text.Trim() != "" && txtOrderQuantity.Text != null)
            {
                count = count + Convert.ToInt32(txtOrderQuantity.Text);
            }
            txtOrderQuantity.Focus();
        }
        lblTotalQuantityToShow.Text = new AppConvert(count);
    }

    public void DeleteMODetail(int modID)
    {
        AppMaterialOrderDetail objAppMaterialOrderDetail = new AppMaterialOrderDetail(intRequestingUserID, modID);
        objAppMaterialOrderDetail.Delete();
    }

    public int compare(Int32 id)
    {
        //InwardDetailQuantity idq = new InwardDetailQuantity();
        //idq = db.InwardDetailQuantities.FirstOrDefault(a => a.InwardDetailQtyID == id);
        //Int64 iwdid = Convert.ToInt64("0" + idq.InwardDetailID);
        //InwardDetail iwdd = new InwardDetail();
        //iwdd = db.InwardDetails.FirstOrDefault(a => a.InwardDetailID == iwdid);
        //int iwid = Convert.ToInt32("0" + iwdd.InwardID);
        //Inward iw = new Inward();
        //iw = db.Inwards.FirstOrDefault(a => a.InwardID == iwid);
        int dd = 0;
        //if (iw.VoucherType_ID != 0 && iw.VoucherType_ID != null)
        //{
        //    if (iw.VoucherType_ID == 8)
        //    {
        //        Int32 refid = Convert.ToInt32(iw.ReferenceID);
        //        TransferStockLocation LT = new TransferStockLocation();
        //        LT = db.TransferStockLocations.FirstOrDefault(l => l.TransferStockLocationID == refid);
        //        dd = Convert.ToInt32(LT.TransferDate);
        //    }
        //    else
        //    {
        //        dd = Convert.ToInt32(iw.InwardDate);
        //    }
        //}
        //else
        //{
        //    dd = Convert.ToInt32(iw.InwardDate);
        //}
        //if (txtDateCompare.Text == "")
        //{
        //    txtDateCompare.Text = GenericUtills.ConvertIntToDate(dd);
        //    ViewState["oldDate"] = dd;
        //    //old = Convert.ToInt32("0"+ ViewState["oldDate"]);
        //    //old = dd;
        //}
        return dd;
    }

    public string Crate(string crate)
    {
        StringBuilder sb = new StringBuilder();

        string[] Arr = crate.Split(',');
        for (int i = 0; i < Arr.Count(); i++)
        {
            sb.Append("100");
            if (i < Arr.Count() - 1)
                sb.Append(",");
        }

        crate = new AppConvert(sb);
        return crate;
    }

    public bool StockAlert(int Ptid)
    {
        //var MoAlt = (from idq in db.InwardDetailQuantities where idq.InwardDetail.Inward.PartnerID == Ptid && idq.IsActive == 1 && idq.InwardDetail.IsActive == 1 && idq.InwardDetail.Inward.IsActive == 1 && idq.BillingQuantity > 0 select idq);

        //if (MoAlt.Count() == 1)
        //{
        //    return true;
        //}
        //else
        //{
        return false;
        //}
    }

    #region Transactionlog

    public void InsertTransactionLog(Int16 ReferenceType, Int64 ReferenceID, Int16 TransactionType, int UserID)
    {
        //    using (WMSEntities TDB = new WMSEntities())
        //    {
        //        TransactionLog TLog = new TransactionLog();
        //        TLog.VoucherType_ID = Convert.ToByte(ReferenceType);
        //        TLog.ReferenceID = ReferenceID;
        //        TLog.TransactionType = Convert.ToByte(TransactionType);
        //        TLog.UserID = UserID;
        //        TLog.IsActive = 1;
        //        TLog.CreateDate = DateTime.Now;
        //        TDB.TransactionLogs.AddObject(TLog);
        //        TDB.SaveChanges();
        //    }
    }

    #endregion

    protected void AddItem(AppMaterialOrderDetailColl objAppMaterialOrderDetailColl, int InwardDetailID, decimal OrderQty)
    {
        AppMaterialOrderDetail objAppMaterialOrderDetail = new AppMaterialOrderDetail(intRequestingUserID);
        AppStockSearchVWColl objAppStockSearchVWColl = new AppStockSearchVWColl(intRequestingUserID);
        AppStockSearchVW objAppStockSearchVW = new AppStockSearchVW(intRequestingUserID);
        objAppStockSearchVWColl.Clear();
        objAppStockSearchVWColl.AddCriteria(StockSearchVW.InwardDetail_ID, Operators.Equals, InwardDetailID);
        objAppStockSearchVWColl.Search();
        objAppStockSearchVW = objAppStockSearchVWColl.FirstOrDefault();

        objAppMaterialOrderDetail.InwardDetail_ID = objAppStockSearchVW.InwardDetail_ID;
        objAppMaterialOrderDetail.LotInternal = objAppStockSearchVW.LotInternal;
        objAppMaterialOrderDetail.Item_ID = objAppStockSearchVW.Item_ID;
        objAppMaterialOrderDetail.LotVersion = objAppStockSearchVW.LotVersion;
        objAppMaterialOrderDetail.OrderQuantity = OrderQty;
        objAppMaterialOrderDetail.GroupQuantity = OrderQty;
        objAppMaterialOrderDetail.StorageSchemeRate_ID = objAppStockSearchVW.StorageSchemeRate_ID;
        objAppMaterialOrderDetail.InwardQuantity = objAppStockSearchVW.InwardQuantity;
        objAppMaterialOrderDetail.StockQuantity = new AppConvert(objAppStockSearchVW.NetQuantity);
        objAppMaterialOrderDetail.Status = ((int)RecordStatus.Active);
        objAppMaterialOrderDetailColl.Add(objAppMaterialOrderDetail);
    }
    #endregion

    #region Button Events

    protected void btnNewDetail_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockOrder.aspx");
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        //Response.Redirect(prev);
        Response.Redirect("StockOrderSearch.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        //if (!string.IsNullOrEmpty(hdfMOID.Value) && hisflag != 1 && hdfMOID.Value != "0")
        //{
        //    //ShowMoHistory(Convert.ToInt32(hdfMOID.Value));
        //    //GenericUtills.popup_Show("MatrialOrderhistory", "txthistoryremarks", this);
        //}
        //else
        {
            SaveMaterialOrder();
        }
    }  
    protected void btnAddItems_Click(object sender, EventArgs e)
    {
        objAppMaterialOrder = (AppMaterialOrder)Session[TableConstants.MaterialOrderSession];

        #region To Get IDs of Inward ID from Search Grid
        int ChkFlg = 0;
        List<int> InwardDetail_IDs = (List<int>)Session[TableConstants.InwardDetailIDSession];
        if (InwardDetail_IDs == null)
            InwardDetail_IDs = new List<int>();

        string[] check = Convert.ToString(Request.Form["CheckBoxSelect"]).Split(',');

        for (int i = 0; i < check.Length; i++)
        {
            int InwardDetailID = Convert.ToInt32(check[i]);
            InwardDetail_IDs.Add(InwardDetailID);
            AddItem(objAppMaterialOrder.MaterialOrderDetailColl, InwardDetailID, 0);

            #region datevalidation
            //Int64 id = Convert.ToInt64("0" + hdfInwardDetailID.Value);
            //int date = compare(id);
            //old = Convert.ToInt32("0" + ViewState["oldDate"]);
            //if (old < date)
            //{
            //    txtDateCompare.Text = GenericUtills.ConvertIntToDate(date);
            //    ViewState["oldDate"] = date;
            //}

            #endregion

        }
        if (ChkFlg != 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Duplicate Partner" + "','2'" + ");", true);
            return;
        }

        #endregion

        gvDescription.DataSource = objAppMaterialOrder.MaterialOrderDetailColl.Where(x => x.Status == ((int)RecordStatus.Active));
        gvDescription.DataBind();

        gvSearch.DataSource = null;
        gvSearch.DataBind();

        Session.Add(TableConstants.MaterialOrderSession, objAppMaterialOrder);
        Session.Add(TableConstants.InwardDetailIDSession, InwardDetail_IDs);
        TotalQuantityCount();


        ((TextBox)gvDescription.Rows[0].FindControl("txtOrderQuantity")).Focus();

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

        // mpSearchItems.Hide();
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        Int64 MaterialOrderID = Convert.ToInt64(hdfMOID.Value);
        int UserID = intRequestingUserID;

        //UserInfo userin = new UserInfo();
        //userin = db.UserInfoes.FirstOrDefault(p => p.UserID == UserID);
        //string FirstName = userin.UserFirstName;
        //string LastName = userin.UserLastName;
        //string FullName = FirstName + " " + LastName;

        //ReportManager RP = new ReportManager();

        //RP = db.ReportManagers.FirstOrDefault(p => p.ReportID == 1177);

        //if (RP == null)
        //{
        //    lblMsg.Text = "ValidateReport";
        //    return;
        //}

        //string url = RP.ReportURL; // Fetch Path from Database
        //string url_updated = url.Replace("@MaterialOrderID", new AppConvert(MaterialOrderID)).Replace("@UserID", FullName);
        //ViewReport(url_updated, e);
    }
    protected void btnCancelDetailPopup_Click(object sender, EventArgs e)
    {
        popup_Hide("modalInwardDetail", "", this);
        if (focus == 1)
        {
            //ddlPartner.Focus();
        }
        if (focus == 3)
        {
            int index = Convert.ToInt32(rowsid.Value);
            TextBox txt = (TextBox)gvDescription.Rows[index].FindControl("txtOrderQuantity");
            txt.Focus();
        }
        if (focus == 4)
        {
            int index = Convert.ToInt32(rowsid.Value);
            LinkButton txt = (LinkButton)gvSearch.Rows[index].FindControl("lblLotNo");
            txt.Focus();
        }
    }
    protected void btnLotSearch_Click(object sender, EventArgs e)
    {
        SearchOptimised();
        if (gvDescription.Rows.Count > 0)
        {
            TotalQuantityCount();
        }
        if (gvSearch.Rows.Count > 0)
        {
            TextBox txtOrderQtySearch = (TextBox)gvSearch.Rows[0].FindControl("txtOrderQtySearch");
            txtOrderQtySearch.Focus();
        }
        else
        {
            UsrtxtPartnerGroupCode.Focus();
        }        
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        #region Outward Validation

        int MaterialOrder_ID = Convert.ToInt32("0" + hdfMOID.Value);
        AppOutwardColl AppOutwardColl = new AppOutwardColl(intRequestingUserID);
        AppOutwardColl.AddCriteria(Outward.MaterialOrder_ID, Operators.Equals, MaterialOrder_ID);
        AppOutwardColl.AddCriteria(Outward.Status, Operators.Equals, (int)RecordStatus.Active);
        AppOutwardColl.Search();

        if (AppOutwardColl.Count() > 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Outward has been made. Please delete the outward first" + "','2'" + ");", true);
            return;
        }

        #endregion

        #region modelete validation

        if (ValidateOrder() == false)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Material Order Allready Deleted" + "','2'" + ");", true);
            return;
        }

        #endregion

        try
        {
            AppMaterialOrder objAppMaterialOrder = new AppMaterialOrder(intRequestingUserID, MaterialOrder_ID);
            objAppMaterialOrder.Delete();

            for (int rowindex = gvDescription.Rows.Count - 1; rowindex >= 0; rowindex--)
            {
                HiddenField hdfMaterialOrderDetailID = (HiddenField)gvDescription.Rows[rowindex].FindControl("hdfMaterialOrderDetailID");
                int MODID = Convert.ToInt32("0" + hdfMaterialOrderDetailID.Value);
                if (MODID != 0)
                {
                    DeleteMODetail(MODID);
                }
            }
            //InsertTransactionLog(20, mo.MaterialOrderID, 4, intRequestingUserID);                
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Deleted Successfully" + "','2'" + ");", true);

        }
        catch (Exception ex)
        {
            if (ex.Message.Contains("Foreign"))
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "This Detail Is Used For Outward" + "','2'" + ");", true);
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + ex.Message + "','2'" + ");", true);
        }


        //gvDescription.DataSource = dt;
        //gvDescription.DataBind();
        btnDelete.Visible = false;
        btnSave.Visible = false;

    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        try
        {
            if (StockAlert(Convert.ToInt32("0" + tempPtID.Value)) == true)
            {
                stockflag = 1;
                //GenericUtills.popup_Hide("modalInstruction", "", this);
                //GenericUtills.popup_Show("StockAlert", "", this);
                return;
            }
            else
            {
                List<string> InwardIds = (List<string>)ViewState["InwardIds"];
                ClickokOut(InwardIds, 1);
                //GenericUtills.popup_Hide("modalInstruction", "", this);
            }
        }
        catch (Exception)
        {

        }
    }
    protected void btnovrOk_Click(object sender, EventArgs e)
    {
        try
        {
            if (StockAlert(Convert.ToInt32("0" + tempPtID.Value)) == true)
            {
                stockflag = 1;
                //GenericUtills.popup_Hide("modalOverride", "", this);
                //GenericUtills.popup_Show("StockAlert", "", this);
                return;
            }
            else
            {
                List<string> InwardIds = (List<string>)ViewState["InwardIds"];
                ClickokOut(InwardIds, 1);
                //GenericUtills.popup_Hide("modalOverride", "", this);
            }
        }
        catch (Exception)
        {

        }
    }

    #endregion

    #region GridView

    protected void gvDescription_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Detail")
        {
            int selectedindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int InwardNo = Convert.ToInt32(e.CommandArgument);
            BindInwardItemDetail(InwardNo);
            BindOutwards(InwardNo);
            BindMO(InwardNo);
            BindLT(InwardNo);
            btnCancelDetailPopup.Focus();

            popup_Show("modalInwardDetail", "", this);
            focus = 3;
            rowsid.Value = "" + selectedindex;
        }

        if (e.CommandName == "Location")
        {
            if (hdfMOID.Value == "" || hdfMOID.Value == null || hdfMOID.Value == "0")
            {
                objAppMaterialOrder = (AppMaterialOrder)Session[TableConstants.MaterialOrderSession];

                int selectedindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
                Int32 InwardDetailID = new AppConvert(e.CommandArgument);
                objAppMaterialOrder.MaterialOrderDetailColl.Select(x => x.InwardDetail_ID == InwardDetailID);

                Label lblNetQuantity = (Label)gvDescription.Rows[selectedindex].FindControl("lblNetQuantity");
                TextBox txtOrderQuantity = (TextBox)gvDescription.Rows[selectedindex].FindControl("txtOrderQuantity");

                grdLoc.DataSource = objAppMaterialOrder.MaterialOrderDetailColl.Where(x => x.InwardDetail_ID == InwardDetailID && x.DeliveryLocation != "");
                grdLoc.DataBind();

                txtNetQty.Text = lblNetQuantity.Text;
                txtOrderQty.Text = txtOrderQuantity.Text;

                focus = 3;
                hdfRowId.Value = "" + selectedindex;
                hdfdelinwddetid.Value = new AppConvert(e.CommandArgument);
                popup_Show("modalDeliveryLoc", txtDeliveryLoc.ClientID, this);
                txtDeliveryLoc.Focus();
            }
        }
        if (e.CommandName == "Del")
        {
            try
            {
                int RowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
                objAppMaterialOrder = (AppMaterialOrder)Session[TableConstants.MaterialOrderSession];
                AppMaterialOrderDetail objAppMaterialOrderDetail = new AppMaterialOrderDetail(intRequestingUserID);

                objAppMaterialOrderDetail = objAppMaterialOrder.MaterialOrderDetailColl[RowIndex];

                HiddenField hdfMaterialOrderDetailID = (HiddenField)gvDescription.Rows[RowIndex].FindControl("hdfMaterialOrderDetailID");

                if (hdfMaterialOrderDetailID.Value != "0" && hdfMaterialOrderDetailID.Value != "")
                {
                    int MaterialOrderDetail_ID = Convert.ToInt32(hdfMaterialOrderDetailID.Value);

                    #region Outward Validation

                    int MaterialOrder_ID = Convert.ToInt32("0" + hdfMOID.Value);
                    AppOutwardDetailColl objAppOutwardDetailColl = new AppOutwardDetailColl(intRequestingUserID);
                    objAppOutwardDetailColl.AddCriteria(OutwardDetail.MaterialOrderDetail_ID, Operators.Equals, MaterialOrderDetail_ID);
                    objAppOutwardDetailColl.AddCriteria(OutwardDetail.Status, Operators.Equals, (int)RecordStatus.Active);
                    objAppOutwardDetailColl.Search();

                    if (objAppOutwardDetailColl.Count() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Outward has been made. Please delete the outward first" + "','2'" + ");", true);
                        return;
                    }
                    #endregion

                    DeleteMODetail(MaterialOrderDetail_ID);
                    //InsertTransactionLog(21, MODID, 4, intRequestingUserID);
                }

                objAppMaterialOrder.MaterialOrderDetailColl.Remove(objAppMaterialOrderDetail);

                gvDescription.DataSource = objAppMaterialOrder.MaterialOrderDetailColl.Where(x => x.Status == ((int)RecordStatus.Active));
                gvDescription.DataBind();

                Session.Add(TableConstants.MaterialOrderSession, objAppMaterialOrder);

                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Deleted Sucessfully" + "','1'" + ");", true);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Foreign"))
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "This Detail Is Used For Outward" + "','2'" + ");", true);
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + ex.Message + "','2'" + ");", true);
            }



        }

    }
    protected void gvSearch_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Sel")
        {
            objAppMaterialOrder = (AppMaterialOrder)Session[TableConstants.MaterialOrderSession];

            int rowindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            List<int> InwardDetail_IDs = (List<int>)Session[TableConstants.InwardDetailIDSession];
            if (InwardDetail_IDs == null)
                InwardDetail_IDs = new List<int>();

            HiddenField hdfInwardDetailID = (HiddenField)gvSearch.Rows[rowindex].FindControl("hdfInwardDetailID");
            TextBox txtOrderQtySearch = (TextBox)gvSearch.Rows[rowindex].FindControl("txtOrderQtySearch");

            int InwardDetailID = Convert.ToInt32(hdfInwardDetailID.Value);
            InwardDetail_IDs.Add(InwardDetailID);
            AddItem(objAppMaterialOrder.MaterialOrderDetailColl, InwardDetailID, new AppConvert(txtOrderQtySearch.Text));

            #region datevalidation

            int id = Convert.ToInt32(hdfInwardDetailID.Value);
            int date = compare(id);
            old = Convert.ToInt32("0" + ViewState["oldDate"]);
            if (old < date)
            {
                //txtDateCompare.Text = GenericUtills.ConvertIntToDate(date);
                ViewState["oldDate"] = date;
            }

            #endregion

            gvDescription.DataSource = objAppMaterialOrder.MaterialOrderDetailColl.Where(x => x.Status == ((int)RecordStatus.Active));
            gvDescription.DataBind();

            gvSearch.DataSource = null;
            gvSearch.DataBind();

            //ShowPartyDescription(InwardIDs);

            Session.Add(TableConstants.MaterialOrderSession, objAppMaterialOrder);
            Session.Add(TableConstants.InwardDetailIDSession, InwardDetail_IDs);
            //TotalQuantityCount();
            //btnovrOk.Visible = true;             
            ((TextBox)gvDescription.Rows[gvDescription.Rows.Count - 1].FindControl("txtOrderQuantity")).Focus();
        }

        if (e.CommandName == "Detail")
        {
            int selectedindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            int InwardNo = Convert.ToInt32(e.CommandArgument);
            BindInwardItemDetail(InwardNo);
            BindOutwards(InwardNo);
            BindMO(InwardNo);
            BindLT(InwardNo);
            //string sb = GenericUtills.setfocus(mpInwardDetail.BehaviorID, btnCancelDetailPopup.ClientID);
            //Page.ClientScript.RegisterStartupScript(Page.GetType(), "Startup", sb.ToString());
            //btnCancelDetailPopup.Focus();            
            popup_Show("modalInwardDetail", "", this);
            focus = 4;
            rowsid.Value = "" + selectedindex;
        }

        #region outstanding popup

        if (e.CommandName == "Customer")
        {
            int selindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            Int32 partnerid = Convert.ToInt32(e.CommandArgument);
            //mploverride.Show();
            //GenericUtills.popup_Show("modalOverride", "", this);
            //Outstanding.Filloutstanding(partnerid);
            //LabourOutstanding.Filloutstanding(partnerid);
            btnovrOk.Visible = false;
            //btnovrCancel.Focus();
        }

        #endregion
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSearch.PageIndex = e.NewPageIndex;
        btnLotSearch_Click(sender, e);
    }
    protected void grdMaterialOrderDetails_OnPreRender(object sender, EventArgs e)
    {
        if (grdMaterialOrderDetails.Rows.Count > 0)
        {
            grdMaterialOrderDetails.UseAccessibleHeader = true;
            grdMaterialOrderDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdlocationtransfer_OnPreRender(object sender, EventArgs e)
    {
        if (grdlocationtransfer.Rows.Count > 0)
        {
            grdlocationtransfer.UseAccessibleHeader = true;
            grdlocationtransfer.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdOutwardDetails_OnPreRender(object sender, EventArgs e)
    {
        if (grdOutwardDetails.Rows.Count > 0)
        {
            grdOutwardDetails.UseAccessibleHeader = true;
            grdOutwardDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void GrdOverRidden_PreRender(object sender, EventArgs e)
    {
        if (GrdOverRidden.Rows.Count > 0)
        {
            GrdOverRidden.UseAccessibleHeader = true;
            GrdOverRidden.HeaderRow.TableSection = TableRowSection.TableHeader;

        }
    }
    protected void grdhistory_OnPreRender(object sender, EventArgs e)
    {
        if (grdhistory.Rows.Count > 0)
        {
            grdhistory.UseAccessibleHeader = true;
            grdhistory.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

    #endregion

    #region showing history


    public void history()
    {
        //int refid = Convert.ToInt32(hdfMOID.Value);
        //IQueryable<TransactionLog> Tlog = from tl in db.TransactionLogs where tl.ReferenceID == refid && tl.VoucherType_ID == 20 select tl;
        //var cr = from te in Tlog where te.TransactionType == 1 select te;
        //if (cr.Count() != 0)
        //{
        //    int uid = db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 1 && x.ReferenceID == refid && x.VoucherType_ID == 20).UserID;
        //    lblcruser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == uid).UserName;
        //    lblcrdate.Text = new AppConvert(db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 1 && x.ReferenceID == refid && x.VoucherType_ID == 20).CreateDate);
        //}
        //var edit = (from te in Tlog where te.TransactionType == 2 select te).OrderByDescending(l => l.CreateDate).Take(1);
        //if (edit.Count() != 0)
        //{
        //    int eid = edit.FirstOrDefault().UserID; //db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 2 && x.ReferenceID == refid && x.VoucherType_ID == 20);
        //    lbleduser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == eid).UserName;
        //    DateTime Createstr = edit.FirstOrDefault().CreateDate;
        //    lbleddate.Text = new AppConvert(Createstr);
        //    lnkdetail.Visible = true;
        //}
        //var del = from te in Tlog where te.TransactionType == 4 select te;
        //if (del.Count() != 0)
        //{
        //    int id = db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 4 && x.ReferenceID == refid && x.VoucherType_ID == 20).UserID;
        //    lbldluser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == id).UserName;
        //    lbldldate.Text = new AppConvert(db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 4 && x.ReferenceID == refid && x.VoucherType_ID == 20).CreateDate);
        //}
    }

    protected void btnhistory_Click(object sender, EventArgs e)
    {
        //if (hdfMOID.Value != "")
        //{
        //    int refid = Convert.ToInt32(hdfMOID.Value);
        //    IQueryable<TransactionLog> Tlog = (from tl in db.TransactionLogs where tl.ReferenceID == refid && tl.VoucherType_ID == 20 select tl).OrderByDescending(p => p.CreateDate);
        //    var edit = from te in Tlog where te.TransactionType == 2 select te;
        //    if (edit.Count() != 0)
        //    {
        //        grdedit.DataSource = edit.ToList();
        //        grdedit.DataBind();
        //    }

        //}
        ////Modhistory.Show();
        //GenericUtills.popup_Show("modalhistory", "", this);
    }

    #endregion

    #region Multilocation   
    protected void btnAddDelLoc_Click(object sender, EventArgs e)
    {
        #region Validation  
        decimal QTY = new AppConvert(txtlbqty.Text);
        for (int i = 0; i < grdLoc.Rows.Count; i++)
        {
            Label lblLOc = (Label)grdLoc.Rows[i].FindControl("lblLOc");
            Label lbqty = (Label)grdLoc.Rows[i].FindControl("lbqty");

            if (txtDeliveryLoc.Text == lblLOc.Text)
            {
                popup_Show("modalDeliveryLoc", "", this);
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Duplicate Location Exists" + "','2'" + ");", true);
                return;
            }

            QTY = QTY + new AppConvert(lbqty.Text);
        }

        decimal netqty = 0;
        if (txtNetQty.Text != "" && !string.IsNullOrEmpty(txtNetQty.Text))
        {
            netqty = new AppConvert(txtNetQty.Text);
        }

        if (QTY > netqty)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Quantity Exceeds" + "','2'" + ");", true);
            popup_Show("modalDeliveryLoc", "", this);
            return;
        }


        #endregion

        int rowindex = Convert.ToInt32(hdfRowId.Value);
        TextBox txtOrderQuantity = (TextBox)gvDescription.Rows[rowindex].FindControl("txtOrderQuantity");
        txtOrderQuantity.Text = QTY.ToString();
        int InwardDetailID = new AppConvert(hdfdelinwddetid.Value);

        objAppMaterialOrder = (AppMaterialOrder)Session[TableConstants.MaterialOrderSession];
        AppMaterialOrderDetail objAppMaterialOrderDetail = new AppMaterialOrderDetail(intRequestingUserID);
        var objAppStockSearchVW = objAppMaterialOrder.MaterialOrderDetailColl.FirstOrDefault(o => o.InwardDetail_ID == InwardDetailID && o.Status == ((int)RecordStatus.Active));

        objAppMaterialOrderDetail.InwardDetail_ID = objAppStockSearchVW.InwardDetail_ID;
        objAppMaterialOrderDetail.LotInternal = objAppStockSearchVW.LotInternal;
        objAppMaterialOrderDetail.Item_ID = objAppStockSearchVW.Item_ID;
        objAppMaterialOrderDetail.LotVersion = objAppStockSearchVW.LotVersion;
        objAppMaterialOrderDetail.OrderQuantity = new AppConvert(txtlbqty.Text);
        objAppMaterialOrderDetail.StorageSchemeRate_ID = objAppStockSearchVW.StorageSchemeRate_ID;
        objAppMaterialOrderDetail.InwardQuantity = objAppStockSearchVW.InwardQuantity;
        objAppMaterialOrderDetail.StockQuantity = new AppConvert(objAppStockSearchVW.StockQuantity);

        objAppMaterialOrderDetail.DeliveryLocation = txtDeliveryLoc.Text;

        if (grdLoc.Rows.Count > 0)
        {
            objAppMaterialOrderDetail.Status = ((int)RecordStatus.InActive);
            objAppMaterialOrder.MaterialOrderDetailColl.Add(objAppMaterialOrderDetail);
        }
        else
        {
            objAppMaterialOrderDetail.Status = ((int)RecordStatus.Active);
            objAppMaterialOrder.MaterialOrderDetailColl.Remove(objAppStockSearchVW);
            objAppMaterialOrder.MaterialOrderDetailColl.Add(objAppMaterialOrderDetail);
        }

        grdLoc.DataSource = objAppMaterialOrder.MaterialOrderDetailColl.Where(o => o.InwardDetail_ID == InwardDetailID);
        grdLoc.DataBind();
        Session.Add(TableConstants.MaterialOrderSession, objAppMaterialOrder);

        txtDeliveryLoc.Text = "";
        txtlbqty.Text = "";
        txtDeliveryLoc.Focus();
        popup_Show("modalDeliveryLoc", txtDeliveryLoc.ClientID, this);
    }
    protected void btnCancelLoction_Click(object sender, EventArgs e)
    {
        int rowindex = Convert.ToInt32(hdfRowId.Value);
        TextBox txtOrderQuantity = (TextBox)gvDescription.Rows[rowindex].FindControl("txtOrderQuantity");
        popup_Hide("modalDeliveryLoc", "", this);
        txtOrderQuantity.Focus();
        hdfRowId.Value = "";
        txtDeliveryLoc.Text = "";
        txtlbqty.Text = "";
    }
    protected void btnDeleteLoc_Click(object sender, EventArgs e)
    {
        objAppMaterialOrder = (AppMaterialOrder)Session[TableConstants.MaterialOrderSession];
        for (int rowindex = grdLoc.Rows.Count - 1; rowindex >= 0; rowindex--)
        {
            CheckBox lbchk = (CheckBox)grdLoc.Rows[rowindex].FindControl("lbchk");
            if (lbchk.Checked == true)
            {
                //dt.Rows.RemoveAt(rowindex);
            }
        }

        grdLoc.DataSource = objAppMaterialOrder.MaterialOrderDetailColl;
        grdLoc.DataBind();
        popup_Show("modalDeliveryLoc", "", this);
        lblLocmsg.Text = "";
    }
    #endregion

    #region MaterialOrder History

    public void SaveMoHistory(int MOID, string Remarks)
    {
        //using (db = new WMSEntities())
        //{
        //try
        //{
        //    MaterialorderHistory hismo = new MaterialorderHistory();
        //    hismo.MaterialOrderID = MOID;
        //    hismo.Remarks = Remarks;
        //    hismo.CreatedBy = Convert.ToInt32("0" + Session["UserID"]);
        //    hismo.CreatedOn = DateTime.Now;
        //    hismo.Flag = 1;
        //    db.MaterialorderHistories.AddObject(hismo);
        //    db.SaveChanges();
        //}
        //catch (Exception ex)
        //{
        //    lblMsg.Text = ex.Message;
        //    return;
        //}
        //}
    }

    public void ShowMoHistory(int ID)
    {
        //DataTable dt = new DataTable();
        //dt.Columns.Add("UserCode");
        //dt.Columns.Add("UserFullName");
        //dt.Columns.Add("CreatedDate");
        //dt.Columns.Add("Remark");
        //var Source = db.MaterialorderHistories.Where(a => a.MaterialOrderID == ID);
        //foreach (var item in Source)
        //{
        //    DataRow dr = dt.NewRow();
        //    dr["UserCode"] = db.UserInfoes.FirstOrDefault(a => a.UserID == item.CreatedBy).UserCode;
        //    dr["UserFullName"] = db.UserInfoes.FirstOrDefault(a => a.UserID == item.CreatedBy).UserFullName;
        //    dr["CreatedDate"] = item.CreatedOn;
        //    dr["Remark"] = item.Remarks;
        //    dt.Rows.Add(dr);
        //}
        //if (dt.Rows.Count > 0)
        //{
        //    grdhistory.DataSource = dt;
        //    grdhistory.DataBind();
        //}
        //else
        //{
        //    grdhistory.DataSource = null;
        //    grdhistory.DataBind();
        //}
        //txthistoryremarks.Text = "";
        //txthistoryremarks.Focus();
    }

    protected void btnHisSave_Click(object sender, EventArgs e)
    {
        //if (!string.IsNullOrEmpty(hdfMOID.Value) && hdfMOID.Value != "0")
        //{
        //    GenericUtills.popup_Hide("MatrialOrderhistory", "", this);
        //    hisflag = 1;
        //    SaveMoHistory(Convert.ToInt32(hdfMOID.Value), txthistoryremarks.Text);
        //    SaveMaterialOrder();
        //}
    }

    protected void btnhisCancel_Click(object sender, EventArgs e)
    {
        // GenericUtills.popup_Hide("MatrialOrderhistory", "", this);
    }

    #endregion

    #region Stock Alert

    protected void btnYes_Click(object sender, EventArgs e)
    {
        List<string> InwardIds = (List<string>)ViewState["InwardIds"];
        if (stockflag == 1)
        {
            ClickokOut(InwardIds, 1);
        }
        else
        {
            ClickokOut(InwardIds, 0);
        }
        //GenericUtills.popup_Hide("StockAlert", "", this);
    }

    protected void btnNo_Click(object sender, EventArgs e)
    {
        //GenericUtills.popup_Hide("StockAlert", "", this);
    }

    #endregion

    #region Text Box Events

    protected void txtOrderQuantity_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtOrderQuantity = (TextBox)gvDescription.Rows[RowIndex].FindControl("txtOrderQuantity");
        objAppMaterialOrder = (AppMaterialOrder)Session[TableConstants.MaterialOrderSession];

        AppStockSearchVWColl objAppStockSearchVWColl = new AppStockSearchVWColl(intRequestingUserID);
        AppStockSearchVW objAppStockSearchVW = new AppStockSearchVW(intRequestingUserID);
        objAppStockSearchVWColl.Clear();
        objAppStockSearchVWColl.AddCriteria(StockSearchVW.InwardDetail_ID, Operators.Equals, objAppMaterialOrder.MaterialOrderDetailColl[RowIndex].InwardDetail_ID);
        objAppStockSearchVWColl.Search();
        objAppStockSearchVW = objAppStockSearchVWColl.FirstOrDefault();
        Decimal NetQuantity = new AppConvert(objAppStockSearchVW.NetQuantity);

        if (objAppMaterialOrder.MaterialOrderDetailColl[RowIndex].ID != 0)
        {
            AppMaterialOrderDetail objAppMaterialOrderDetail = new AppMaterialOrderDetail(intRequestingUserID, objAppMaterialOrder.MaterialOrderDetailColl[RowIndex].ID);
            NetQuantity = NetQuantity + objAppMaterialOrderDetail.BalOrderQuantity;
        }

        if (NetQuantity < (new AppConvert(txtOrderQuantity.Text)))
        {
            objAppMaterialOrder.MaterialOrderDetailColl[RowIndex].BalOrderQuantity = NetQuantity;
            objAppMaterialOrder.MaterialOrderDetailColl[RowIndex].OrderQuantity = NetQuantity;
            objAppMaterialOrder.MaterialOrderDetailColl[RowIndex].StockQuantity = NetQuantity;
        }
        else
        {
            objAppMaterialOrder.MaterialOrderDetailColl[RowIndex].BalOrderQuantity = new AppConvert(txtOrderQuantity.Text);
            objAppMaterialOrder.MaterialOrderDetailColl[RowIndex].OrderQuantity = new AppConvert(txtOrderQuantity.Text);
        }

        Session.Add(TableConstants.MaterialOrderSession, objAppMaterialOrder);
        gvDescription.DataSource = objAppMaterialOrder.MaterialOrderDetailColl.Where(x => x.Status == ((int)RecordStatus.Active));
        gvDescription.DataBind();

        if (gvDescription.Rows.Count > RowIndex + 1)
        {
            TextBox txtOrderQuantityNext = (TextBox)gvDescription.Rows[RowIndex + 1].FindControl("txtOrderQuantity");
            if (txtOrderQuantityNext.Text == "0")
                txtOrderQuantityNext.Text = "";
            txtOrderQuantityNext.Focus();
        }
        else
        {
            txtOrderDate.Focus();
        }
    }

    #endregion Text Box Events

}