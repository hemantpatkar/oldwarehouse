﻿using System;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;

public partial class VendorContactDetails : BigSunPage
{
    AppCompany obj;
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = (AppCompany)Session[TableConstants.VendorSessionObj];
        if (!IsPostBack)
        {
            BindData();
        }
        //BindAddress();
    }
    #endregion       

    #region GridView Events
    protected void grdContactDetails_PreRender(object sender, EventArgs e)
    {
        if (grdContactDetails.Rows.Count > 0)
        {
            grdContactDetails.UseAccessibleHeader = true;
            grdContactDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }   
    protected void grdContactDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if (strCommandName == "INSERT")
        {
            DropDownList TitleType = grdContactDetails.FooterRow.FindControl("ddlNewTitleType") as DropDownList;
            TextBox FirstName = grdContactDetails.FooterRow.FindControl("txtFName") as TextBox;
            TextBox LastName = grdContactDetails.FooterRow.FindControl("txtNewLastNName") as TextBox;
            TextBox EmailID = grdContactDetails.FooterRow.FindControl("txtNewEmailID") as TextBox;
            TextBox Phone1 = grdContactDetails.FooterRow.FindControl("txtNewPhoneNo") as TextBox;
            TextBox Phone2 = grdContactDetails.FooterRow.FindControl("txtNewMobileNo") as TextBox;
            CheckBox IsActiveContact = grdContactDetails.FooterRow.FindControl("chkActiveContact") as CheckBox;
            CheckBox IsDefaultContact = grdContactDetails.FooterRow.FindControl("chkDefaultContact") as CheckBox;
            DropDownList CompanyAddress = grdContactDetails.FooterRow.FindControl("ddlNewCompanyAddress") as DropDownList;

            SaveContactDetails(strCommandID, TitleType.SelectedValue, FirstName.Text, LastName.Text, EmailID.Text,
                Phone1.Text, Phone2.Text, CompanyAddress, IsActiveContact.Checked, IsDefaultContact.Checked);
        }
        else if (strCommandName == "CANCELADD")
        {
            grdContactDetails.ShowFooter = false;
            BindData();
        }
        else if (strCommandName == "CANCELEDIT")
        {
            grdContactDetails.EditIndex = -1;
            BindData();
        }       
    }
    protected void grdContactDetails_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdContactDetails.ShowFooter = false;
        grdContactDetails.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdContactDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        obj.CompanyContactColl[e.RowIndex].Status = new AppConvert(RecordStatus.Deleted);
        Session[TableConstants.VendorSessionObj] = obj;
    }
    protected void grdContactDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strCommandID = new AppConvert(e.RowIndex);
        DropDownList TitleType = grdContactDetails.Rows[e.RowIndex].FindControl("ddlTitleType") as DropDownList;
        TextBox FirstName = grdContactDetails.Rows[e.RowIndex].FindControl("txtFirstName") as TextBox;
        TextBox LastName = grdContactDetails.Rows[e.RowIndex].FindControl("txtLastName") as TextBox;
        TextBox EmailID = grdContactDetails.Rows[e.RowIndex].FindControl("txtEmailID") as TextBox;
        TextBox Phone1 = grdContactDetails.Rows[e.RowIndex].FindControl("txtPhoneNo") as TextBox;
        TextBox Phone2 = grdContactDetails.Rows[e.RowIndex].FindControl("txtMobileNo") as TextBox;
        CheckBox IsActiveContact = grdContactDetails.Rows[e.RowIndex].FindControl("chkIsActiveContact") as CheckBox;
        CheckBox IsDefaultContact = grdContactDetails.Rows[e.RowIndex].FindControl("chkIsDefaultContact") as CheckBox;
        DropDownList CompanyAddress = grdContactDetails.Rows[e.RowIndex].FindControl("ddlCompanyAddress") as DropDownList;

        SaveContactDetails(strCommandID, TitleType.SelectedValue, FirstName.Text, LastName.Text, EmailID.Text,
                Phone1.Text, Phone2.Text, CompanyAddress, IsActiveContact.Checked, IsDefaultContact.Checked);
    }
    protected void grdContactDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Footer:
                DropDownList ddlAddresss = (DropDownList)e.Row.FindControl("ddlNewCompanyAddress");
                
                BindAddress("Address1", "ID", ddlAddresss);
                break;
            case DataControlRowType.DataRow:
                DropDownList ddlTitleType = (DropDownList)e.Row.FindControl("ddlTitleType");
                if (ddlTitleType != null)
                {
                    if (obj.CompanyContactColl.Count > 0)
                    {
                        ddlTitleType.SelectedValue = new AppConvert(obj.CompanyContactColl[e.Row.RowIndex].Soluation);
                    }
                }
                DropDownList ddlCompanyAddresss = (DropDownList)e.Row.FindControl("ddlCompanyAddress");
                if (ddlCompanyAddresss != null)
                {                
                    BindAddress("Address1", "ID", ddlCompanyAddresss);
                    ddlCompanyAddresss.SelectedValue = new AppConvert(obj.CompanyContactColl[e.Row.RowIndex].CompanyAddress_ID);
                }
                break;
            default:
                break;
        }
    }
    #endregion

    #region Button Click Events
    protected void lbtnAddNew_Click(object sender, EventArgs e)
    {
        grdContactDetails.EditIndex = -1;
        grdContactDetails.ShowFooter = true;
        BindData();
    }
    #endregion

    #region Bind Data  
    private void BindData()
    {
        if (obj != null)
        {
            grdContactDetails.DataSource = obj.CompanyContactColl;
            grdContactDetails.DataBind();
            if (obj.CompanyContactColl.Count <= 0)
            {
                AddEmptyRow();
            }
        }
    }
    #endregion

    #region Save Contact Details
    public void SaveContactDetails(string Id, string TitleType, string FirstName, string LastName,
        string EmailID, string Phone1, string Phone2, DropDownList ddlCompanyAddress, bool IsActive, bool IsDefault)
    {        
        int intCommandID = new AppUtility.AppConvert(Id);
        AppObjects.AppCompanyContact SingleObj = new AppObjects.AppCompanyContact(intRequestingUserID);
        if (String.IsNullOrEmpty(Id) == false)
        {
            SingleObj = obj.CompanyContactColl[intCommandID];
        }
        SingleObj.Company_ID = obj.ID;
        SingleObj.Soluation = TitleType;
        SingleObj.FirstName = FirstName;
        SingleObj.LastName = LastName;
        SingleObj.Email = EmailID;
        SingleObj.Phone1 = Phone1;
        SingleObj.Phone2 = Phone2;
        SingleObj.CompanyAddress = obj.CompanyAddressColl[ddlCompanyAddress.SelectedIndex];
        SingleObj.Status = new AppUtility.AppConvert(IsActive);
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;
        obj.AddNewCompanyContact(SingleObj);
        Session.Add(TableConstants.VendorSessionObj, obj);
        BindData();
    }
    #endregion

    #region BindAddress
    public void BindAddress(string DataTextField, string DataValueField, DropDownList ddlAddressID)
    {
        ddlAddressID.DataSource = obj.CompanyAddressColl;
        ddlAddressID.DataTextField = DataTextField;
        ddlAddressID.DataValueField = DataValueField;
        ddlAddressID.DataBind();
    }
    #endregion

    #region AddEmptyRow
    public void AddEmptyRow()
    {
        AppCompanyContactColl collOfContacts = new AppCompanyContactColl(intRequestingUserID);
        AppCompanyContact newContact = new AppCompanyContact(intRequestingUserID);
        newContact.Soluation = "Add atleast one Address";
        collOfContacts.Add(newContact);
        grdContactDetails.DataSource = collOfContacts;
        grdContactDetails.DataBind();
        grdContactDetails.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdContactDetails.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdContactDetails.ShowFooter = true;
    }
    #endregion   
}