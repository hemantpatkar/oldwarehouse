﻿<%@ Page Language="C#" AutoEventWireup="true" Culture="en-gb" MasterPageFile="~/Base.master" Inherits="R_ReportPrint" Codebehind="R_ReportPrint.aspx.cs" %>
 
<%@ Register TagPrefix="rsweb" Namespace="Microsoft.Reporting.WebForms" Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div class="row">
            <div class="panel panel-body">
                <rsweb:ReportViewer ID="ReportViewer1" AsyncRendering="false" runat="server" Width="100%">
                </rsweb:ReportViewer>
            </div>
        </div>
    </div>
</asp:Content>
