﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="TermsAndConditions" Codebehind="TermsAndConditions.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        function AutoCompleteSearch(sender, eventArgs) {
            debugger;
            var id = sender._id;
            var autocompleteId = id.split('_');
            var replacetxt = autocompleteId[2].replace('txt', '');
            var txt = $get(autocompleteId[0] + '_txt' + replacetxt);
            var txtName = $get(autocompleteId[0] + '_hdf' + replacetxt + 'Name');
            var txtID = $get(autocompleteId[0] + '_hdf' + replacetxt + 'ID');
            txt.value = eventArgs.get_text();
            txtName.value = eventArgs.get_text();
            txtID.value = eventArgs.get_value();
        }

        function ClearAutocompleteTextBox(sender) {
            debugger;
            var id = sender.id;
            var autocompleteId = id.split('_');
            var replacetxt = autocompleteId[1].replace('txt', '');
            var txt = $get(autocompleteId[0] + '_txt' + replacetxt);
            var txtName = $get(autocompleteId[0] + '_hdf' + replacetxt + 'Name');
            var txtID = $get(autocompleteId[0] + '_hdf' + replacetxt + 'ID');
            if (txt.value == "No Records Found" || txt.value != txtName.value) {
                txt.value = "";
                txtName.value = "";
                txtID.value = "";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="PnlAddAddress" runat="server">
        <ContentTemplate>
            <div class="col-lg-12">
                <div class="panel panel-body">
                    <div class="row">
                        <div class=" col-lg-4">
                            <asp:Label ID="lblTnCGroup" runat="server" Text="Terms & Conditions Group" CssClas="bold"></asp:Label>
                            <asp:Label ID="lblStar" runat="server" CssClass=" bold text-danger" Text="*"></asp:Label>

                            <asp:RequiredFieldValidator ID="Rfv_txtTnCGroup" runat="server" ControlToValidate="txtTnCGroup" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage=" (Enter Group Name)" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>

                            <div class="input-group">
                                <asp:TextBox ID="txtTnCGroup" TabIndex="3" placeholder="Select T&C Group" runat="server" OnTextChanged="txtTnCGroup_TextChanged" AutoPostBack="true"
                                    AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                <asp:HiddenField runat="server" ID="hdfTnCGroupName" />
                                <asp:HiddenField runat="server" ID="hdfTnCGroupID" Value="0" />
                            </div>

                            <asp:AutoCompleteExtender ID="Ace_txtTnCGroup" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TermsAndConditionsSearch" ServicePath="~/Service/AutoComplete.asmx"
                                OnClientItemSelected="AutoCompleteSearch" ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtTnCGroup">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class=" col-lg-7">
                            <asp:Label ID="lblTnC" runat="server" Text="Terms & Conditions" CssClas="bold"></asp:Label>
                            <asp:TextBox ID="txtTnC" runat="server" CssClass="form-control input-sm" MaxLength="100"></asp:TextBox>
                        </div>
                        <div class=" col-lg-1">
                            <br />
                            <asp:Button ID="btnAddTnC" runat="server" Text="Add" OnClick="btnAddTnC_Click" CssClass="btn btn-primary btn-sm" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3" style="float: left">
                        <asp:Button ID="btnAddTnCGroup" runat="server" Text="Add Terms & Conditions Group" CssClass="btn btn-primary btn-sm" OnClick="btnAddTnCGroup_Click" />
                    </div>
                    <div class="col-lg-1" style="float: inherit">
                        <asp:Button ID="btnClose" runat="server" Text="Close" Visible="false" OnClick="btnClose_Click" CssClass="btn btn-primary btn-sm" />
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class=" col-lg-8">
                        <br />
                        <asp:GridView ID="grdTnc" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-hover text-nowrap" OnPreRender="grdTnc_PreRender">
                            <Columns>
                                <asp:TemplateField HeaderText="Terms & Conditions Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTnCGroupName" runat="server" Text='<%# Eval("Statement")%>' CssClas="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div class="row" id="divAddTnCgroup" runat="server">
                    <iframe runat="server" src="TermsAndConditionsGroup.aspx?Master=Simple" id="ifrmTnCGroup" style="height: 300px;" class="col-lg-12 iframe"></iframe>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

