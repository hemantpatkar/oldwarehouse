﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using System.Text;

public partial class Base : System.Web.UI.MasterPage
{

    int UserIDSession = 0;
    int ActiveModuleID = 0;
    string ActiveModuleName = "Select Module";
    protected void Page_Load(object sender, EventArgs e)
    {
        UserIDSession = new AppConvert(Session[TableConstants.UserIDSession]);
        if (!IsPostBack)
        {
            ActiveModuleID = new AppConvert(Session[TableConstants.ActiveModuleID]);
            ActiveModuleName = new AppConvert(Session[TableConstants.ActiveModuleName]);
            BindParentMenu();
            if (ActiveModuleID > 0)
            {
                accordion.InnerHtml = BindUserMenus(new AppConvert(ActiveModuleID)).ToString();
                modulename.InnerText = new AppConvert(ActiveModuleName);
            }

        }
    }

    protected void BindGrid()
    {
        StringBuilder sb = new StringBuilder();
        AppAppComponentColl objAppComponentColl = new AppAppComponentColl(UserIDSession);
        objAppComponentColl.AddCriteria(AppComponent.Parent_ID, Operators.Equals, 0);
        objAppComponentColl.Search();
        sb.Append("<ul>");
        foreach (var item in objAppComponentColl)
        {
            sb.Append("<li>");
            sb.Append("<a class=\"jarvismetro-tile big-cubes btn-primary\">");
            sb.Append("<span class=\"iconbox\"><i class=\"fa " + item.MenuIcon + " fa-4x\"></i><span>" + item.Name + "<span class=\"label pull-right btn-info\">New</span></span>");
            sb.Append("</span>");
            sb.Append("</a>");
            sb.Append("</li>");
        }

        sb.Append("</ul>");
        //shortcut.InnerHtml = sb.ToString();
    }

    protected void BindParentMenu()
    {
        AppAppComponentColl objAppComponentColl = new AppAppComponentColl(UserIDSession);
        objAppComponentColl.AddCriteria(AppComponent.Parent_ID, Operators.Equals, 0);
        objAppComponentColl.Search();

        ParentMenu.DataSource = objAppComponentColl;
        ParentMenu.DataBind();
    }

    public StringBuilder BindUserMenus(int ID)
    {
        StringBuilder sb = new StringBuilder();
        if (UserIDSession == 1)
        {
            AppAppComponentColl objSubMenuList = new AppAppComponentColl(UserIDSession);
            objSubMenuList.AddCriteria(AppComponent.Status, Operators.Equals, 1);
            objSubMenuList.AddCriteria(AppComponent.Parent_ID, Operators.Equals, ID);
            objSubMenuList.Search();
            objSubMenuList.Sort(AppAppComponent.ComparisionDesc);
            int i = 0;
            foreach (var item in objSubMenuList)
            {
                String str = Convert.ToString(BindUserMenus(item.ID));
                if (str != "")
                {
                    sb.Append("<li><a data-toggle=\"collapse\" data-parent=\"#accordion\" href='#collapse" + i + "'><i class=\"fa fa-angle-double-down pull-right\"></i>");
                    sb.Append("<i class='main-icon " + item.MenuIcon + "'></i><span>" + item.Name + "</span></a>");
                    sb.Append("<div id=\"collapse" + i + "\" class=\"collapse\"><ul>");
                    sb.Append(str);
                    sb.Append("</ul></div></li>");
                }
                else
                {
                    sb.Append("<li><a class=\"sub-menu\" href='" + item.NavigationTo + "'>");
                    sb.Append("<i class='main-icon " + item.MenuIcon + "'></i><span>" + "" + item.Name + "</a></li>");
                }
                i++;
            }
        }
        else
        {
            AppAppComponentRightColl objAppAppComponentRightColl = new AppAppComponentRightColl(UserIDSession);
            int departmentID = (int)Session[TableConstants.DepartmentIDConst];
            int designationID = (int)Session[TableConstants.DesignationIDConst];
            objAppAppComponentRightColl.GetUserRights(departmentID, designationID);
            if (objAppAppComponentRightColl.Count > 0)
            {
                foreach (AppAppComponentRight item in objAppAppComponentRightColl)
                {
                    sb.Append("<li><a href='" + item.AppComponent.NavigationTo + "'> <i class='main-icon fa " + item.AppComponent.MenuIcon + "'></i><span>" + item.AppComponent.Name + "</span></a></li>");
                }
            }
        }

        return sb;





        //if (LastMenuAdded != item.AppComponent.Name)
        //{

        //    objMenuList.Append("<li>" +
        //                "<a data-toggle='collapse' data-parent = '#accordion' href ='#collapse" + targetId + "'><i class='fa fa-angle-double-down pull-right'></i>" +
        //                "<i class='main-icon fa fa-bullhorn'></i>" + item.AppComponent.Name + "</a>" +
        //                "<div id ='collapse" + targetId + "' class='collapse'> <ul>");

        //    AppAppComponentColl objSubMenuList = new AppAppComponentColl(UserIDSession);
        //    //objSubMenuList.AddCriteria(AppComponent);
        //    //objSubMenuList.Search();
        //    AppAppComponentColl getAccessedMenus = objAppAppComponentRightColl.GetAccessedMenus(objSubMenuList, objAppAppComponentRightColl);
        //    foreach (AppAppComponent objItem in getAccessedMenus)
        //    {
        //        if ((objItem.Parent_ID == item.ID))
        //        {
        //            objMenuList.Append("<li><a href='" + objItem.NavigationTo + "'>  " + objItem.Name + " </a></li> ");
        //        }
        //    }
        //    LastMenuAdded = item.AppComponent.Name;
        //    objMenuList.Append("</ul></div></li>");
        //}



    }

    protected void ParentMenu_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "ParentMenu")
        {
            Session.Add(TableConstants.ActiveModuleID, e.CommandArgument);
            accordion.InnerHtml = BindUserMenus(new AppConvert(e.CommandArgument)).ToString();

            LinkButton selectButton = (LinkButton)e.CommandSource;
            Session.Add(TableConstants.ActiveModuleName, selectButton.ToolTip);
            modulename.InnerText = selectButton.ToolTip;
        }


    }
}



