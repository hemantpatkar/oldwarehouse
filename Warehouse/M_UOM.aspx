﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="M_UOM" Codebehind="~/M_UOM.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class=" col-lg-12 ">
            <div id="divButtons" class="pull-right">
                <br />
                <div class="btn-group">
                    <div class=" col-lg-3">
                        <asp:TextBox ID="txtUOMName" runat="server" placeholder="UOM Name" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                    </div>
                    <div class=" col-lg-3">
                        <asp:TextBox ID="txtUOMCode" runat="server" placeholder="UOM Code" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                    </div>
                    <div class=" col-lg-3">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control  input-sm">
                            <asp:ListItem Text="ALL" Value="-32768"></asp:ListItem>
                            <asp:ListItem Text="Approved" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Created" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Deleted" Value="-10"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class=" col-lg-3">
                        <asp:Button runat="server" Text="Search" CssClass="btn btn-sm  btn-primary" ID="btnSearch" OnClick="btnSearch_Click"></asp:Button>
                    </div>

                </div>
            </div>
            <h3>UOM Details</h3>
            <hr />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdUOM" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Horizontal"
                        CssClass="table table-hover table-striped text-nowrap nowrap"
                        OnPreRender="grdUOM_PreRender"
                        OnRowCommand="grdUOM_RowCommand" OnRowEditing="grdUOM_RowEditing" OnRowDeleting="grdUOM_RowDeleting"
                        OnRowUpdating="grdUOM_RowUpdating">
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="ADDNEW"
                                        Text="Add New" ToolTip="Add New" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CssClass=" fa fa-edit fa-2x" CommandName="Edit"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnDelete" Visible="false" runat="server" CssClass="fa fa-trash fa-2x" CommandName="Delete"
                                        Text="" ToolTip="Delete" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                     <asp:HiddenField runat="server" ID="hdfUOMID" Value='<%# Eval("ID") %>' />
                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="EditUOM"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="CANCELEDIT" CssClass="fa fa-ban fa-2x" toot="Cancel"
                                        Text="" />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x" ToolTip="Save" ValidationGroup="AddUOM"
                                        Text="" UseSubmitBehavior="False" />
                                    <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban  fa-2x" Text="" />
                                </FooterTemplate>
                            </asp:TemplateField>

                         <%--   <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrUOMCode" Text="Code" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblUOMCode" runat="server" TabIndex="1" Text='<%#Eval("Code") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                      <asp:HiddenField runat="server" ID="hdfID" Value='<%# Eval("ID") %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfUOMID" Value='<%# Eval("ID") %>' />
                                    <asp:TextBox ID="txtUOMCode" runat="server" Text='<%# Eval("Code") %>' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtUOMCode" runat="server" ControlToValidate="txtUOMCode"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditUOM"></asp:RequiredFieldValidator>

                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewUOMCode" runat="server" Text='' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtNewUOMCode" runat="server" ControlToValidate="txtNewUOMCode"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddUOM"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>--%>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrUOMName" Text="Name" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblUOMName" runat="server" TabIndex="1" Text='<%#Eval("Name") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtUOMName" runat="server" Text='<%# Eval("Name") %>' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtUOMName" runat="server" ControlToValidate="txtUOMName"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditUOM"></asp:RequiredFieldValidator>

                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewUOMName" runat="server" Text='' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtNewUOMName" runat="server" ControlToValidate="txtNewUOMName"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddUOM"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" runat="server" Text='<%#(Eval("StatusName")) %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsActiveUOM" CssClass="form-control" TabIndex="8" Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkActiveUOM" CssClass="form-control" TabIndex="8" Checked="true" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Default" Text="Default" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDefault" runat="server" Text='<%#(Eval("Type").ToString()=="1"?"Y":"N") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsDefaultUOM" TabIndex="9" Checked='<%#Eval("Type").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkDefaultUOM" TabIndex="9" />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdUOM" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
