﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" Culture="en-gb" AutoEventWireup="true" Inherits="StockLocTransferSearch" Codebehind="StockLocTransferSearch.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" DisplayAfter="0" AssociatedUpdatePanelID="grdUpdate"
        runat="server">
        <ProgressTemplate>
            <div class="modal fade in" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-keyboard="false" style="display: block">
                <div class="modal-dialog  modal-sm">
                    <h1 style="text-align: center;">
                        <i class="fa fa-truck fa-magic fa-2x faa-passing animated"></i>
                    </h1>
                </div>
            </div>
            <div class="modal-backdrop fade in"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-lg-9">
            <div class="heading">
                <i class="fa fa-truck"></i>
                <asp:Label ID="lblTitleLocationTransferSearch" Text="Location Transfer Search" runat="server"></asp:Label>
            </div>
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSearch_Click" Text="Search" ValidationGroup="Search" />
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnNew" OnClick="btnNew_Click" CssClass="btn btn-primary btn-block btn-sm" runat="server" AccessKey="n" CausesValidation="False" Text="New" />
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnExport" CssClass="btn btn-warning btn-block btn-sm" runat="server" OnClick="btnExportToExcel_Click" AccessKey="n" CausesValidation="False" Text="Export" />
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblLotNo" Text="Lot No" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtLotNo" runat="server" CssClass="form-control input-sm" ValidationGroup="Search">
            </asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblTransferDate" Text="Transfer Date" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtTransferDate" runat="server"></asp:TextBox>
            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder=""
                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtTransferDate">
            </asp:MaskedEditExtender>
            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtTransferDate">
            </asp:CalendarExtender>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblCustomer" Text="Customer" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtCustomer" runat="server" CssClass="form-control input-sm" ValidationGroup="Search">
            </asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblMovedQuantity" Text="Moved Quantity" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtMovedQuantity" runat="server" CssClass="form-control input-sm" ValidationGroup="Search">
            </asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblItemName" Text="Item Name" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtItemName" runat="server" CssClass="form-control input-sm" ValidationGroup="Search">
            </asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblStatus" Text="Status" CssClass="bold"></asp:Label>
            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control input-sm">
                <asp:ListItem Value="-1" Selected="True">All</asp:ListItem>
                <asp:ListItem Value="1">Un Approved</asp:ListItem>
                <asp:ListItem Value="2">Approved</asp:ListItem>
                <asp:ListItem Value="-10">Deleted</asp:ListItem>
            </asp:DropDownList>
            <asp:HiddenField ID="hdfgatereg" runat="server" />
            <asp:HiddenField ID="ShowLastRecords" runat="server" />
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-lg-12" style="overflow: auto">
            <asp:UpdatePanel ID="grdUpdate" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdStockLocTransferSearch" runat="server" OnRowCommand="grdStockLocTransferSearch_RowCommand" GridLines="None" AutoGenerateColumns="False" PagerStyle-CssClass="pagination-ys"
                        CssClass="table table-hover table-striped text-nowrap table-Full" AllowPaging="true" OnPageIndexChanging="grdStockLocTransferSearch_PageIndexChanging" PageSize="50">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblSrno" runat="server" Text='<%#Container.DataItemIndex+1%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="Lnksel" runat="server" CausesValidation="false" ForeColor="Maroon" CssClass="fa fa-close fa-2x" CommandArgument='<%#Eval("ID") %>'
                                        CommandName="Del"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblLotNo" Text="Lot No" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lblLotNo" Text='<%#Eval("LotInternal") %>' CommandArgument='<%# Eval("InwardDetail_ID")%>'
                                        CommandName="Detail" ToolTip='<%#Eval("LotInternal")%>' runat="server"></asp:LinkButton>
                                    <asp:LinkButton ID="lblLotVersion" Text='<%#Eval("LotVersion") %>' CommandArgument='<%# Eval("InwardDetail_ID")%>'
                                        CommandName="Detail" ToolTip='<%#Eval("LotVersion")%>' runat="server"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer Info">
                                <ItemTemplate>
                                    <asp:Label ID="lblVehicalNo" runat="server" ToolTip='<%#Eval("InwardDetail.Inward.Customer_Company.Name") %>'
                                        Text='<%#Eval("InwardDetail.Inward.Customer_Company.Name") %>'></asp:Label><br />
                                    <asp:Label ID="lblVehicalName" runat="server" Font-Bold="true" ForeColor="Maroon"
                                        Text='<%#Eval("InwardDetail.Inward.Customer_Company.Parent.Name") %>' ToolTip='<%#Eval("InwardDetail.Inward.Customer_Company.Parent.Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Moved Quantity">
                                <ItemTemplate>
                                    <asp:Label ID="lblStockQuantity" runat="server" ToolTip='<%#Eval("MovedQuantity") %>'
                                        Text='<%#Eval("MovedQuantity") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Transfer Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblpartyname" runat="server" ToolTip='<%#Eval("TransferDate") %>'
                                        Text='<%#Eval("TransferDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblItem_ID" runat="server" ToolTip='<%#Eval("Item.Name") %>'
                                        Text='<%#Eval("Item.Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblScheme" Text="Scheme" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblScheme" runat="server" ForeColor="#3399ff" Text='<%#Eval("InwardDetail.StorageSchemeRate.StorageScheme.Code")  %>' ToolTip='<%#Eval("InwardDetail.StorageSchemeRate.StorageScheme.Code") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="From Location">
                                <ItemTemplate>
                                    <asp:Label ID="lblLocationNo" runat="server" ForeColor="Maroon" Text='<%#Eval("InwardDetail.StorageLocation.Name") %>' ToolTip='<%#Eval("InwardDetail.StorageLocation.Name")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Location">
                                <ItemTemplate>
                                    <asp:Label ID="lblTo_StorageLocation" runat="server" ToolTip='<%# Eval("To_StorageLocation.Name") %>' Text='<%# Eval("To_StorageLocation.Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Created Info">
                                <ItemTemplate>
                                    <asp:Label ID="lblModifiedBy" runat="server" ToolTip='<%# Eval("ModifiedBy") %>' Text='<%# Eval("ModifiedBy") %>'></asp:Label>
                                    <br />
                                    <asp:Label ID="lblModifiedOn" runat="server" ToolTip='<%# Eval("ModifiedOn") %>' Text='<%# Eval("ModifiedOn") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>

