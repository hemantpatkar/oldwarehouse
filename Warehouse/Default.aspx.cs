﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : BigSunPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AppObjects.AppCompany x;
        if (Request.QueryString[0] == null)
            x = new AppObjects.AppCompany(this.intRequestingUserID);
        else
            x = new AppObjects.AppCompany(this.intRequestingUserID);


        x.Name = "";
        x.Type = 1;

        AppObjects.AppCompanyAddress y = new AppObjects.AppCompanyAddress(this.intRequestingUserID);
        y.Address1 = "";
        y.Address2 = "";
        y.ZipCodeType_ID = 1;

        x.AddNewCompanyAddress(y);
        x.Save();

      
    }
}