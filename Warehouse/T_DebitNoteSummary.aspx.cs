﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to display all Company lists.
*/
#endregion
#region Using
using System;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.UI;
using AppUtility;
#endregion
public partial class T_DebitNoteSummary : BigSunPage
{
    #region PageLoad
    AppDebitNoteColl objDN;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtVoucherDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            BindData();
        }
    }
    #endregion
    #region Function
    public void CancelDebitNote(AppDebitNote ObjDebitNote)
    {
        AppDebitNote ObjDuplicateDebitNote = ObjDebitNote;
        ObjDuplicateDebitNote.ID = 0;
        ObjDuplicateDebitNote.VoucherType_ID = 32; //Debit Voucher Cancel
        ObjDuplicateDebitNote.ModifiedBy = intRequestingUserID;
        ObjDuplicateDebitNote.ModifiedOn = System.DateTime.Now;
        ObjDuplicateDebitNote.Status = new AppConvert((int)RecordStatus.Cancelled);

        if (ObjDuplicateDebitNote.DebitNoteDetailColl.Count > 0)
        {
            int TotalCount = ObjDuplicateDebitNote.DebitNoteDetailColl.Count;
            for (int i = 0; i < TotalCount; i++)
            {
                ObjDuplicateDebitNote.DebitNoteDetailColl[i].ID = 0;
                if (ObjDuplicateDebitNote.DebitNoteDetailColl[i].DebitAmount > 0)
                {
                    ObjDuplicateDebitNote.DebitNoteDetailColl[i].CreditAmount = ObjDuplicateDebitNote.DebitNoteDetailColl[i].DebitAmount;
                    ObjDuplicateDebitNote.DebitNoteDetailColl[i].DebitAmount = 0;
                }
                else
                {
                    ObjDuplicateDebitNote.DebitNoteDetailColl[i].DebitAmount = ObjDuplicateDebitNote.DebitNoteDetailColl[i].CreditAmount;
                    ObjDuplicateDebitNote.DebitNoteDetailColl[i].CreditAmount = 0;
                }
                ObjDuplicateDebitNote.DebitNoteDetailColl[i].ModifiedBy = intRequestingUserID;
                ObjDuplicateDebitNote.DebitNoteDetailColl[i].ModifiedOn = System.DateTime.Now;
                ObjDuplicateDebitNote.DebitNoteDetailColl[i].Status = new AppConvert((int)RecordStatus.Cancelled);
            }
        }
        ObjDuplicateDebitNote.Save();

        #region Add Ledger Entry


        if (ObjDuplicateDebitNote.DebitNoteDetailColl.Count > 0)
        {
            foreach (var item in ObjDuplicateDebitNote.DebitNoteDetailColl)
            {
                if (item.CreditAmount != 0)
                {
                    LedgerEntry(LoginCompanyID, 101002, 1010, item.Ledger_ID, item.ID, ObjDuplicateDebitNote.DebitNoteDate, item.CreditAmount, 0, ObjDuplicateDebitNote.DebitNoteNo, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicateDebitNote.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, ObjDuplicateDebitNote.VoucherType_ID, item.DebitNoteDesc);
                }
                else
                {
                    LedgerEntry(LoginCompanyID, 101001, 1010, item.Ledger_ID, item.ID, ObjDuplicateDebitNote.DebitNoteDate, 0, item.DebitAmount, ObjDuplicateDebitNote.DebitNoteNo, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicateDebitNote.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, ObjDuplicateDebitNote.VoucherType_ID, item.DebitNoteDesc);
                }


            }

        }


        #endregion
    }
    protected void BindData()
    {
        AppObjects.AppDebitNoteColl objColl = new AppDebitNoteColl(intRequestingUserID);
        if (txtDebitNoteNo.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.DebitNote.DebitNoteNo, AppUtility.Operators.Equals, txtDebitNoteNo.Text, 0);
        }
        if (txtDebitNoteAmount.Text != "")
        {
            switch (ddlOperators.SelectedValue)
            {
                case "1":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.DebitNote.ExchangeRate, AppUtility.Operators.Equals, txtDebitNoteAmount.Text, 0);
                    break;
                case "2":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.DebitNote.ExchangeRate, AppUtility.Operators.GreaterOrEqualTo, txtDebitNoteAmount.Text, 0);
                    break;
                case "3":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.DebitNote.ExchangeRate, AppUtility.Operators.LessOrEqualTo, txtDebitNoteAmount.Text, 0);
                    break;
                case "4":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.DebitNote.ExchangeRate, AppUtility.Operators.GreaterThan, txtDebitNoteAmount.Text, 0);
                    break;
                case "5":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.DebitNote.ExchangeRate, AppUtility.Operators.LessThan, txtDebitNoteAmount.Text, 0);
                    break;
            }
        }
        if (!string.IsNullOrEmpty(txtVoucherFromDate.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.DebitNote.DebitNoteDate, AppUtility.Operators.Equals,new AppUtility.AppConvert(txtVoucherFromDate.Text), 0);
        }
        if (!string.IsNullOrEmpty(txtVoucherToDate.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.DebitNote.DebitNoteDate, AppUtility.Operators.Equals, new AppUtility.AppConvert(txtVoucherToDate.Text), 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
            objColl.AddCriteria(AppObjects.DebitNote.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.DebitNoteSummary] = objColl;
        grdDebitNoteSummary.DataSource = objColl;
        grdDebitNoteSummary.DataBind();
    }
    #endregion
    #region GridView Events
    protected void grdDebitNoteSummary_PreRender(object sender, EventArgs e)
    {
        if (grdDebitNoteSummary.Rows.Count > 0)
        {
            grdDebitNoteSummary.UseAccessibleHeader = true;
            grdDebitNoteSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region Button Click Events
    protected void btnNewDebitNote_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.ObjCompanySession);
        Response.Redirect("T_DebitNote.aspx");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {

        if (Update(grdDebitNoteSummary, new AppConvert((int)RecordStatus.Deleted), 1010, 1010) == 1)
        {
            BindData();
        }

    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        if (Update(grdDebitNoteSummary, new AppConvert((int)RecordStatus.Approve), 1010, 1010) == 1)
        {
            BindData();
        }
    }
    protected void btnCanceled_Click(object sender, EventArgs e)
    {
        AppObjects.AppDebitNoteColl objColl = new AppDebitNoteColl(intRequestingUserID);
        if (txtCancelDNNo.Text != "" && txtVoucherDate.Text != "")
        {
            objColl.AddCriteria(AppObjects.DebitNote.DebitNoteNo, AppUtility.Operators.Equals, txtCancelDNNo.Text);
            objColl.Search(RecordStatus.ALL);

            if (objColl.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Record not found','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                return;
            }
            else
            {
                AppDebitNote objSingleDN = objColl[0];
                if (objColl[0].Status == new AppUtility.AppConvert((int)RecordStatus.Cancelled))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelDNNo.Text + " Transaction alerady cancelled','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelDNNo.Focus();
                    return;
                }
                else if (objColl[0].Status == new AppUtility.AppConvert((int)RecordStatus.Deleted))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelDNNo.Text + " Transaction is deleted','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelDNNo.Focus();
                    return;
                }
                else if (objColl[0].Status == new AppUtility.AppConvert((int)RecordStatus.Created))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelDNNo.Text + " Transaction is not approved','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelDNNo.Focus();
                    return;
                }
                else if (objColl[0].DebitNoteDate > new AppUtility.AppConvert(Convert.ToDateTime(txtVoucherDate.Text)))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelDNNo.Text + " Cancel voucher date should be greater than or equal to voucher date.','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelDNNo.Focus();
                    return;
                }
                objSingleDN.Status = new AppUtility.AppConvert((int)RecordStatus.Cancelled);
                objSingleDN.Save();

                if (objSingleDN.DebitNoteDetailColl.Count > 0)
                {
                    foreach (var item in objSingleDN.DebitNoteDetailColl)
                    {
                        if (item.CreditAmount != 0)
                        {
                            UpdateLedgerEntryStatus(101002, 1010, item.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                        }
                        else
                        {
                            UpdateLedgerEntryStatus(101001, 1010, item.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                        }
                    }
                }
                CancelDebitNote(objSingleDN);
            }
            Session[TableConstants.DebitNoteSummary] = objColl;
            grdDebitNoteSummary.DataSource = objColl;
            grdDebitNoteSummary.DataBind();
            txtCancelDNNo.Text = "";
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Debit voucher cancelled successfully','1'" + ");", true);


        }
        else
        {
            if (txtCancelDNNo.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Debit voucher no.','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                txtCancelDNNo.Focus();
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter voucher date.','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                txtVoucherDate.Focus();
                return;
            }
        }
    }
    #endregion
}