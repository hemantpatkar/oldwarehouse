﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="T_SaleOrderSummary" Codebehind="T_SaleOrderSummary.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<script type="text/javascript">
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <div id="divButtons" class="pull-right">
                        <br />
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm  btn-primary" data-toggle="collapse" data-target="#demo">
                                <span class="glyphicon glyphicon-search"></span>Search Tools
                            </button>
                            <asp:Button runat="server" Text="Search" CssClass="btn btn-sm  btn-primary" ID="btnSearch" OnClick="btnSearch_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="btn  btn-sm  btn-primary" OnClick="btnDelete_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnApproved" Text="Approve" CssClass="btn  btn-sm  btn-primary" OnClick="btnApproved_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnCanceled" Text="Cancel" CssClass="btn  btn-sm  btn-primary" OnClick="btnCanceled_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnNewSaleOrder" Text="New Sale Order" CssClass="btn  btn-sm  btn-primary" OnClick="btnNewSaleOrder_Click"></asp:Button>
                        </div>
                    </div>
                    <h3>Sale Order Summary </h3>
                    <hr />
                    <div id="demo" class="collapse">
                        <div class="row" runat="server">
                            <div class="col-lg-3">
                                <asp:TextBox ID="txtPurchaeOrderNo" runat="server" placeholder="Enter Sale Order No" CssClass=" form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <asp:TextBox ID="txtCustomer" TabIndex="1" AutoPostBack="true" placeholder="Select Customer" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)"
                                        CssClass="form-control input-sm"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                </div>
                                <asp:HiddenField runat="server" ID="hdfCustomerName" Value="" />
                                <asp:HiddenField runat="server" ID="hdfCustomerID" Value="" />
                                <asp:AutoCompleteExtender ID="ACEtxtCustomer" runat="server"
                                    CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                    CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                    CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                    FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                    ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtCustomer">
                                </asp:AutoCompleteExtender>
                            </div>
                            <div class="col-lg-2">
                                <asp:TextBox ID="txtSaleOrderAmount" runat="server" placeholder="Sale Order Amount" CssClass=" form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-lg-1">
                                <asp:DropDownList ID="ddlOperators" runat="server" CssClass="form-control  input-sm">
                                    <asp:ListItem Text="=" Value="1"></asp:ListItem>
                                    <asp:ListItem Text=">=" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="<=" Value="3"></asp:ListItem>
                                    <asp:ListItem Text=">" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="<" Value="5"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-3">
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control  input-sm">
                                    <asp:ListItem Text="ALL" Value="-32768"></asp:ListItem>
                                    <asp:ListItem Text="Approved" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Created" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Deleted" Value="-10"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-body">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="grdSaleOrderSummary" runat="server" AutoGenerateColumns="False" Width="100%" 
                            OnPreRender="grdSaleOrderSummary_PreRender" GridLines="Horizontal"
                            CssClass="table table-striped table-hover  text-nowrap  dt-responsive nowrap">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <input id="SelectAll" onclick="SelectAll(this);" runat="server" type="checkbox" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                       <asp:CheckBox runat="server" ID="ChkSelect" Enabled='<%#Convert.ToBoolean(Eval("Status").ToString()=="0"?  1 : 0)%>' />
                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID")%>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblSaleOrderNo" Text="Sale Order No" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HyperLink TabIndex="1" CausesValidation="false" ID="lbtnCompanyCode" runat="server"
                                            Text='<%# Eval("SaleOrderNumber")%>' ToolTip="Edit" NavigateUrl='<% #"T_SaleOrder.aspx?SaleOrderID=" + Eval("ID")%>' Target="_blank"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderCustomerName" Text="Customer Name" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCustomerName" runat="server" Text='<%# Eval("Delivery_Company.Name") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderSaleOrderBaseAmount" Text="Base Amount" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblSaleOrderBaseAmount" runat="server" TabIndex="2" Text='<%#Eval("SaleOrderBaseAmount") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderDiscount" Text="Discount" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDiscount" runat="server" Text='<%#Eval("DiscountAmount") %>' CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderTaxAmount" Text="Tax Amount" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTaxAmount" runat="server" Text='<%#Eval("TaxAmount") %>' CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderStatusOf" Text="Staus" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatusOf" runat="server" Text='<%#Eval("StatusName") %>' CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderCurrency" Text="Currency" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCurrencyType" runat="server" Text='<%#Eval("CurrencyType.Name") %>' CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="grdSaleOrderSummary" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdSaleOrderSummary.ClientID%>');
        $(document).ready(function () {
            GridUI(GridID, 50);
        })
    </script>
</asp:Content>
