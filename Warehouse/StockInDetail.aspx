﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" Culture="en-gb" AutoEventWireup="true" Inherits="StockInDetail" Codebehind="StockInDetail.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function validateForm() {
            var errmsg = '';
            var lotno = document.getElementById('<%=txtLotNoInternal.ClientID%>').value;
            if (lotno.enable == true) {
                if (lotno == null || lotno == "") {
                    errmsg += "You must enter a 'LotNumber'";
                }
            }
            var ItemQty = document.getElementById('<%=txtItemQuantity.ClientID%>').value;
            var cvitm = document.getElementById('<%=cvitemqty.ClientID%>').valuetocompare;
            if (ItemQty == null || ItemQty == "") {
                errmsg += "<br />You must enter 'Quantity'";
            } else {

                if (isNaN(ItemQty)) {
                    errmsg += "<br />'Please enter proper quantity";
                }
                else if (parseInt(ItemQty) < parseInt(cvitm)) {
                    errmsg += "<br /> Quantity Entered Should Be Greater Than Material Order OR Location Transfered Quantity.";
                }
            }

            var sche = document.getElementById('<%=hdfStorageSchemeRateID.ClientID%>').value;
            if (sche == null || sche == "") {
                errmsg += "<br />You must Select a 'Scheme'";
            } else {

                if (isNaN(sche)) {
                    errmsg += "<br />'Please Select proper Scheme";
                }
            }

            var Item = document.getElementById('<%=UsrhdfItem.ClientID%>').value;
            if (Item == null || Item == "") {
                errmsg += "<br />You must Select an 'Item'";
            } else {

                if (isNaN(Item)) {
                    errmsg += "<br />'Please Select Proper Item";
                }
            }
            var loca = document.getElementById('<%=UsrhdfLocation.ClientID%>').value;
            if (loca == null || loca == "") {
                errmsg += "<br />You must Select a 'Location'";

            } else {
                if (isNaN(loca)) {
                    errmsg += "<br />'Please Select Proper Location";
                }
            }



            if (errmsg == '') {
                return true;
            } else {
                ShowAnimatedMessage(errmsg, '2')
                return false;
            }
        }

        function ReceivedBy(sender, eventArgs) {
            var hdSchID = $get('<%= hdfReceivedBy.ClientID %>');
            var lblSchname = $get('<%= lblReceived.ClientID %>');
            var Schcode = $get('<%= txtReceivedBy.ClientID %>');

            hdSchID.value = eventArgs.get_value();
            if (hdSchID.value != "") {
                Schcode.value = eventArgs.get_text();
                lblSchname.value = eventArgs.get_text();
            }
        }

        function ClearReceivedBy() {
            var hdpartID = $get('<%= hdfReceivedBy.ClientID %>');
            var PatyGroup = $get('<%=txtReceivedBy.ClientID %>');
            var party = $get('<%=lblReceived.ClientID %>');
            if ((PatyGroup.value != party.value) || PatyGroup.value == "") {
                $get('<%=txtReceivedBy.ClientID %>').value = "";
                $get('<%=lblReceived.ClientID %>').value = "";
                $get('<%=hdfReceivedBy.ClientID %>').value = "";
            }
        }

        function GateRegister(sender, eventArgs) {
            debugger
            var hdSchID = $get('<%= hdfGateRegis.ClientID %>');
            var Schcode = $get('<%= txtGateRegis.ClientID %>');
            var txtcode = $get('<%= txtVehName.ClientID %>');
            var lblSchname = $get('<%= txtVehicalNo.ClientID %>');
            var detail = eventArgs.get_value().split('~');
            hdSchID.value = detail[0];
            Schcode.value = eventArgs.get_text();
            txtcode.value = eventArgs.get_text();
            txtcode.innerHTML = eventArgs.get_text();
            lblSchname.value = detail[1];
            lblSchname.innerHTML = detail[1];
        }

        function ClearGateReg() {
            var hdpartID = $get('<%= hdfGateRegis.ClientID %>');
            var PatyGroup = $get('<%=txtGateRegis.ClientID %>');
            var party = $get('<%=txtVehicalNo.ClientID %>');
            var txtcode = $get('<%= txtVehName.ClientID %>');
            if ((PatyGroup.value != txtcode.value) || PatyGroup.value == "") {
                $get('<%=txtGateRegis.ClientID %>').value = "";
                $get('<%=txtVehicalNo.ClientID %>').value = "";
                $get('<%=hdfGateRegis.ClientID %>').value = "";
                $get('<%=txtVehName.ClientID %>').value = "";
            }
        }

        function SchemeCodeSearch(sender, eventArgs) {

            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());
            var hdSchID = $get('<%= hdfStorageSchemeRateID.ClientID %>');
            var lblSchname = $get('<%= hdfStorageSchemeRateName.ClientID %>');
            var Schcode = $get('<%= txtStorageSchemeRate.ClientID %>');
         <%--   var schemedetailid = $get('<%= UsrhdfschemeDetailid.ClientID %>');--%>
            var detail = eventArgs.get_value().split('~');
            hdSchID.value = detail[0];
            //schemedetailid.value = detail[1];
            if (hdSchID.value != "") {
                Schcode.value = eventArgs.get_text();
                lblSchname.innerHTML = eventArgs.get_text();
                lblSchname.value = eventArgs.get_text();
            }
        }

        function ClearScheme() {
            var hdpartID = $get('<%= hdfStorageSchemeRateID.ClientID %>');
            var PatyGroup = $get('<%=hdfStorageSchemeRateName.ClientID %>');
            var party = $get('<%=txtStorageSchemeRate.ClientID %>');
            if ((PatyGroup.value != party.value) || PatyGroup.value == "") {
                $get('<%=hdfStorageSchemeRateID.ClientID %>').value = "";
                $get('<%=hdfStorageSchemeRateName.ClientID %>').value = "";
                $get('<%=txtStorageSchemeRate.ClientID %>').value = "";
            }
        }

        function ItemSearch(sender, eventArgs) {

            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());

            var hdschID = $get('<%= hdfStorageSchemeRateID.ClientID %>');
            var lblschname = $get('<%= hdfStorageSchemeRateName.ClientID %>');
            var schcode = $get('<%= txtStorageSchemeRate.ClientID %>');
            hdschID.value = "";
            lblschname.innerHTML = "";
            schcode.value = "";
            var hdItemID = $get('<%= UsrhdfItem.ClientID %>');
            var lblItemname = $get('<%= UsrlblItem.ClientID %>');
            var Itemcode = $get('<%= UsrtxtItemN.ClientID %>');
            var today = $get('<%= txtInwardDate.ClientID %>');          
           
            hdItemID.value = eventArgs.get_value();
            if (hdItemID.value != "") {
                Itemcode.value = eventArgs.get_text();
                lblItemname.innerHTML = eventArgs.get_text();
                lblItemname.value = eventArgs.get_text();
                lblItemname.textContent = eventArgs.get_text();
                $find('<%=ACE_txtStorageSchemeRate.ClientID %>').set_contextKey(eventArgs.get_value() + ',' + today.value);
            }
        }

        function ClearItem() {
            var hdpartID = $get('<%= UsrhdfItem.ClientID %>');
            var PatyGroup = $get('<%=UsrtxtItemN.ClientID %>');
            var party = $get('<%=UsrlblItem.ClientID %>');
            if ((PatyGroup.value != party.value) || PatyGroup.value == "") {
                $get('<%=UsrtxtItemN.ClientID %>').value = "";
                $get('<%=UsrlblItem.ClientID %>').value = "";
                $get('<%=UsrhdfItem.ClientID %>').value = "";
            }
        }

        function LocationCodeSearch(sender, eventArgs) {
            var hdLocID = $get('<%= UsrhdfLocation.ClientID %>');
            var lblLocname = $get('<%= UsrlblLocation.ClientID %>');
            var Loccode = $get('<%= UsrtxtLocation.ClientID %>');

            hdLocID.value = eventArgs.get_value();
            if (hdLocID.value != "") {
                Loccode.value = eventArgs.get_text();
                lblLocname.innerHTML = eventArgs.get_text();
                lblLocname.value = eventArgs.get_text();

            }
        }

        function ClearLocation() {
            var hdpartID = $get('<%= UsrhdfLocation.ClientID %>');
            var PatyGroup = $get('<%=UsrtxtLocation.ClientID %>');
            var party = $get('<%=UsrlblLocation.ClientID %>');
            if ((PatyGroup.value != party.value) || PatyGroup.value == "") {
                $get('<%=UsrtxtLocation.ClientID %>').value = "";
                $get('<%=UsrlblLocation.ClientID %>').value = "";
                $get('<%=UsrhdfLocation.ClientID %>').value = "";
            }
        }


        function PartnerSearch(sender, eventArgs) {

            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());
            var hdpartID = $get('<%= UsrhdfPartner.ClientID %>');
            var lblPartname = $get('<%= UsrlblPartner.ClientID %>');
            var UsrlblPartner1 = $get('<%= UsrlblPartner1.ClientID %>');
            var part = document.getElementById('<%=UsrlblPartner.ClientID  %>')
            var partycode = $get('<%= UsrtxtPartnerCode.ClientID %>');

            hdpartID.value = eventArgs.get_value();
            if (hdpartID.value != "") {
                partycode.value = eventArgs.get_text();
                UsrlblPartner1.value = eventArgs.get_text();
                lblPartname.value = eventArgs.get_text();

                $find('<%=UsrtxtPartnerGroupCode_AutoCompleteExtender.ClientID %>').set_contextKey(eventArgs.get_value());

            }
        }

        function ClearPartner() {
            var hdpartID = $get('<%= UsrhdfPartner.ClientID %>');
            var PatyGroup = $get('<%=UsrtxtPartnerCode.ClientID %>');
            var party = $get('<%=UsrlblPartner.ClientID %>');
            if ((PatyGroup.value != party.value) || PatyGroup.value == "") {
                $get('<%=UsrtxtPartnerCode.ClientID %>').value = "";
                $get('<%=UsrlblPartner.ClientID %>').value = "";
                $get('<%=UsrhdfPartner.ClientID %>').value = "";
                $find('<%=UsrtxtPartnerGroupCode_AutoCompleteExtender.ClientID %>').set_contextKey("0");
            }
        }

        var i = 1;
        function clickButton(e, buttonid) {

            var evt = (e || window.event);
            var keyCode = (evt.keyCode || evt.which);
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (keyCode == 13) {
                    if (bt == "ctl00_ContentPlaceHolder1_btnDetailSave") {
                        validateForm();
                    }
                    bt.click();
                    //if (bt.onclick() == true) {
                    //    bt.disabled = true;
                    //}
                    return false;
                }
            }
        }

        function clickItem(e, buttonid) {
            var evt = (e || window.event);
            var keyCode = (evt.keyCode || evt.which);
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (keyCode == 40) {
                    bt.click();
                    return false;
                }
            }
        }


        function LabourClick(e, buttonid, osdbuttonid) {
            var evt = (e || window.event);
            var keyCode = (evt.keyCode || evt.which);
            var shift = 0;
            var alt = 0;
            var ctrl = 0;
            shift = evt.shiftKey;
            alt = evt.altKey;
            ctrl = evt.ctrlKey;
            var bt = document.getElementById(buttonid);
            var btosd = document.getElementById(osdbuttonid);
            if (bt) {

                if (keyCode == 76 && alt == 1) {
                    bt.click();
                    return false;
                }
            }
            if (btosd) {

                if (keyCode == 75 && alt == 1) {
                    btosd.click();
                    return false;
                }
            }
        }


        function PreInwardSearch(sender, eventArgs) {
            var hdfpreId = $get('<%= UsrhdfPre.ClientID %>');
            var lblPre = $get('<%= HidePre.ClientID %>');
            var preno = $get('<%= UsrtxtParNo.ClientID %>');
            preno.value = eventArgs.get_text();
            hdfpreId.value = eventArgs.get_value();
            lblPre.value = eventArgs.get_text();
            lblPre.innerHTML = eventArgs.get_text();
        }

        function ClearPreInwardSearch() {
            var OrignalProfitCenter = $get('<%=UsrtxtParNo.ClientID %>');
            var HidenProfitCenter = $get('<%= HidePre.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.value) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=HidePre.ClientID %>').value = "";
                $get('<%=UsrtxtParNo.ClientID %>').value = "";
                $get('<%= UsrhdfPre.ClientID %>').value = "";
            }
        }

        function PartySearch(sender, eventArgs) {
            var hdpartID = $get('<%= hdfParty.ClientID %>');
            var lblPartname = $get('<%= HideParty.ClientID %>');
            var partycode = $get('<%= txtparty.ClientID %>');
            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.value = eventArgs.get_text();
            lblPartname.innerHTML = eventArgs.get_text();

        }

        function ClearParty() {
            var OrignalProfitCenter = $get('<%=txtparty.ClientID %>');
            var HidenProfitCenter = $get('<%=HideParty.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.value) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=hdfParty.ClientID %>').value = "";
                $get('<%=txtparty.ClientID %>').value = "";
            }
        }
        function confirmalert(ask) {
            return confirm(ask);
        }


        function OriginSearch(sender, eventArgs) {
            var hdfID = $get('<%= hdfOriginID.ClientID %>');
            var Orignal = $get('<%=txtOrigin.ClientID %>');
            var Hiden = $get('<%= HideOrigin.ClientID %>');

            hdfID.value = eventArgs.get_value();
            if (hdfID.value != "") {
                Orignal.value = eventArgs.get_text();
                Hiden.value = eventArgs.get_text();
            }
        }

        function ClearOrigin() {
            var hdfID = $get('<%= hdfOriginID.ClientID %>');
            var Orignal = $get('<%=txtOrigin.ClientID %>');
            var Hiden = $get('<%= HideOrigin.ClientID %>');
            if ((Orignal.value != Hiden.value) || Orignal.value == "No Records Found") {
                $get('<%=hdfOriginID.ClientID %>').value = "";
                $get('<%=txtOrigin.ClientID %>').value = "";
                $get('<%=HideOrigin.ClientID %>').value = "";
            }
        }


        function BrandSearch(sender, eventArgs) {
            var hdfID = $get('<%=hdfBrandID.ClientID %>');
            var Orignal = $get('<%=txtBrand.ClientID %>');
            var Hiden = $get('<%=HideBrand.ClientID %>');

            hdfID.value = eventArgs.get_value();
            if (hdfID.value != "") {
                Orignal.value = eventArgs.get_text();
                Hiden.value = eventArgs.get_text();
            }
        }

        function ClearBrand() {
            var hdfID = $get('<%=hdfBrandID.ClientID %>');
            var Orignal = $get('<%=txtBrand.ClientID %>');
            var Hiden = $get('<%=HideBrand.ClientID %>');
            if ((Orignal.value != Hiden.value) || Orignal.value == "No Records Found") {
                $get('<%=hdfBrandID.ClientID %>').value = "";
                $get('<%=txtBrand.ClientID %>').value = "";
                $get('<%=HideBrand.ClientID %>').value = "";
            }
        }


        function VarietySearch(sender, eventArgs) {
            var hdfID = $get('<%=hdfVarietyID.ClientID %>');
            var Orignal = $get('<%=txtVariety.ClientID %>');
            var Hiden = $get('<%=HideVariety.ClientID %>');

            hdfID.value = eventArgs.get_value();
            if (hdfID.value != "") {
                Orignal.value = eventArgs.get_text();
                Hiden.value = eventArgs.get_text();
            }
        }

        function ClearVariety() {
            var hdfID = $get('<%=hdfVarietyID.ClientID %>');
            var Orignal = $get('<%=txtVariety.ClientID %>');
            var Hiden = $get('<%=HideVariety.ClientID %>');
            if ((Orignal.value != Hiden.value) || Orignal.value == "No Records Found") {
                $get('<%=hdfVarietyID.ClientID %>').value = "";
                $get('<%=txtVariety.ClientID %>').value = "";
                $get('<%=HideVariety.ClientID %>').value = "";
            }
        }

        function PartnerGroupSearch(sender, eventArgs) {

            var hdpartID = $get('<%= UsrhdfPartnerGroup.ClientID %>');
            var lblPartname = $get('<%= UsrlblPartnerGroup.ClientID %>');
            var partycode = $get('<%= UsrtxtPartnerGroupCode.ClientID %>');

            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=UsrtxtPartnerCode_AutoCompleteExtender.ClientID %>').set_contextKey("3," + eventArgs.get_value());
            }
        }


        function ClearPartnerGroupSearch() {
            var OrignalProfitCenter = $get('<%=UsrtxtPartnerGroupCode.ClientID %>');
            var HidenProfitCenter = $get('<%= UsrlblPartnerGroup.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=UsrhdfPartnerGroup.ClientID %>').value = "";
                $get('<%=UsrlblPartnerGroup.ClientID %>').value = "";
                $get('<%=UsrtxtPartnerGroupCode.ClientID %>').value = "";
                $find('<%=UsrtxtPartnerCode_AutoCompleteExtender.ClientID %>').set_contextKey("4");
            }
        }


    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
        <ProgressTemplate>
            <div class="modal fade in" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-keyboard="false" style="display: block">
                <div class="modal-dialog  modal-sm">
                    <h1 style="text-align: center;">
                        <i class="fa fa-truck fa-magic fa-2x faa-passing animated"></i>
                    </h1>
                </div>
            </div>
            <div class="modal-backdrop fade in"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>

            <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <i class="fa fa-sign-in"></i>
                        <asp:Label ID="lblInward" runat="server"></asp:Label>&nbsp;/&nbsp;
                          <asp:Label ID="lblInwardNoValue" ForeColor="Maroon" runat="server"></asp:Label>
                        <div class="pull-right">
                            <asp:LinkButton ID="btnDeleteInwardDetail" runat="server" Text="" CausesValidation="False" Visible="false"
                                TabIndex="1" OnClick="btnDeleteInwardDetail_Click" OnClientClick="return confirmalert('Do you want to Delete ?' )"> <i class="fa fa-trash text-danger"></i> </asp:LinkButton>
                            <asp:ConfirmButtonExtender ID="btnDeleteInwardDetail_ConfirmButtonExtender" runat="server"
                                Enabled="True" TargetControlID="btnDeleteInwardDetail">
                            </asp:ConfirmButtonExtender>
                            <asp:LinkButton ID="btnhistory" runat="server" CausesValidation="False" Visible="false"
                                TabIndex="1" OnClick="btnhistory_Click">
                                  <i class="fa fa-history"></i> 
                            </asp:LinkButton>
                            <asp:HyperLink ID="btnPrint" runat="server" Target="_blank" Visible="false"
                                TabIndex="1" NavigateUrl=""><i class="fa fa-print"></i> </asp:HyperLink>

                            <asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click" CausesValidation="False"
                                TabIndex="1">
                            <i class="fa fa-search"></i> 
                            </asp:LinkButton>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row btnrow">
                <div class="col-lg-6">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <asp:LinkButton ID="btnCopy" runat="server" Visible="false" />
                        </li>
                        <li>
                            <asp:LinkButton ID="btnApproved" runat="server" Text="" TabIndex="1" Visible="false" AccessKey="a" OnClick="btnApproved_Click" ValidationGroup="Inward" />
                        </li>
                        <li>
                            <asp:LinkButton ValidationGroup="Inward" ID="btnSave" CausesValidation="true" runat="server" AccessKey="s" OnClick="btnSave_Click" TabIndex="1" />
                        </li>
                        <li>
                            <asp:LinkButton ID="btnNew" runat="server" AccessKey="n" OnClick="btnNew_Click" CausesValidation="False" TabIndex="1"></asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="btnAddItem" runat="server" Text="Add Item" AccessKey="i" OnClick="btnAddItem_Click" CausesValidation="False" TabIndex="1"></asp:LinkButton>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <div class="pull-right">
                        <ul class="nav nav-pills nav-wizard">
                            <li class="active"><a href="#" data-toggle="tab">New</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Pending Approval</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Available</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Partially Available</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Done</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <hr />
            <div class="panel" id="prin" runat="server">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-3">

                            <asp:HiddenField ID="hdfapproved" runat="server" />
                            <asp:HiddenField ID="hdfrevised" runat="server" />
                            <asp:HiddenField ID="hdfprerevised" runat="server" />

                            <asp:Label ID="lblPreinwardNo" runat="server" CssClass="bold"></asp:Label>
                            <asp:LinkButton ID="Lnkpreinward" runat="server" AccessKey="P" OnClick="Lnkpreinward_Click"></asp:LinkButton>
                            <%-- <asp:TextBox ID="txtpreinward" runat="server"></asp:TextBox>--%>
                            <asp:TextBox ID="UsrtxtParNo" runat="server" onblur="ClearPreInwardSearch()"
                                CssClass="form-control input-sm" autocomplete="off" spellcheck="false" TabIndex="1"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="UsrtxtParNo_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionSetCount="10"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="PreInwardSearch" ServiceMethod="PreInwardsearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                UseContextKey="true" ContextKey="0" TargetControlID="UsrtxtParNo">
                            </asp:AutoCompleteExtender>
                            <div style="display: none">
                                <asp:HiddenField ID="UsrhdfPre" runat="server" />
                                <asp:TextBox ID="HidePre" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-9 text-align-left">
                            <br />
                            <asp:Button ID="btnsearch" runat="server" OnClick="btnsearch_Click" CssClass="btn btn-primary btn-sm" TabIndex="1" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label ID="lblGateRegisterID" runat="server" CssClass="bold"></asp:Label>
                            <asp:Label ID="Label12" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtGateRegis"
                                SetFocusOnError="true" CssClass="text-danger" Display="Dynamic" ErrorMessage="" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                            <div class="input-group">
                                <asp:TextBox ID="txtGateRegis" runat="server" TabIndex="1" onblur="ClearGateReg()" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                            </div>
                            <asp:AutoCompleteExtender ID="txtGateRegis_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="15" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="GateRegister" ServiceMethod="GateEntrySearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtGateRegis">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:TextBox ID="txtVehName" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <asp:TextBox ID="hdfGateRegis" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>

                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblVeh" runat="server"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtVehicalNo" TabIndex="1" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div style="display: none;">
                                <asp:TextBox ID="UsrlblPartner" runat="server" BorderWidth="0px" CssClass="form-control input-sm" Width="322px"></asp:TextBox>
                            </div>
                            <asp:Label ID="lblInwardDate" runat="server" CssClass="bold"></asp:Label>
                            <asp:Label ID="Label6" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RangeValidator ID="Rangeval" runat="server" Type="Date" ErrorMessage="Invalid" CssClass="text-danger"
                                Display="Dynamic" ControlToValidate="txtInwardDate" ValidationGroup="Inward"></asp:RangeValidator>
                            <asp:RangeValidator ID="Rangback" runat="server" Type="Date" ErrorMessage="Invalid" CssClass="text-danger"
                                Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtInwardDate" ValidationGroup="Inward"></asp:RangeValidator>
                            <div class="input-group">
                                <asp:TextBox ID="txtInwardDate" runat="server" CssClass="form-control input-sm" TabIndex="1"
                                    ValidationGroup="Inward"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="txtInwardDate_MaskedEditExtender" runat="server" Enabled="True"
                                Mask="99/99/9999" MaskType="Date" TargetControlID="txtInwardDate">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="caltxtInwardDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtInwardDate"
                                TargetControlID="txtInwardDate">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RFVFromdate0" runat="server" ControlToValidate="txtInwardDate"
                                CssClass="text-danger" Display="Dynamic" ErrorMessage="" SetFocusOnError="true"
                                ValidationGroup="Inward"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtDateCompare"
                                ControlToValidate="txtInwardDate" Display="Dynamic" ErrorMessage="" SetFocusOnError="true"
                                Type="Date" ValidationGroup="Inward"></asp:CompareValidator>

                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPartnerGroupCode" runat="server" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="UsrtxtPartnerGroupCode" onblur="ClearPartnerGroupSearch()" TabIndex="1" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                            </div>
                            <asp:AutoCompleteExtender ID="UsrtxtPartnerGroupCode_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerGroupSearch"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" ContextKey="4" UseContextKey="true"
                                TargetControlID="UsrtxtPartnerGroupCode">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:HiddenField ID="UsrhdfPartnerGroup" runat="server" />
                                <asp:Label ID="UsrlblPartnerGroup" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPartnerCode" runat="server" CssClass="bold"></asp:Label>
                            <asp:Label ID="Label4" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="RFVFromdate1" runat="server" ControlToValidate="UsrtxtPartnerCode" SetFocusOnError="true"
                                CssClass="text-danger" Display="Dynamic" ErrorMessage="" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                            <div class="input-group">
                                <asp:TextBox ID="UsrtxtPartnerCode" TabIndex="1" runat="server" onblur="ClearPartner()" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div>
                            <asp:AutoCompleteExtender ID="UsrtxtPartnerCode_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerSearch"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                UseContextKey="true" ContextKey="3" TargetControlID="UsrtxtPartnerCode">
                            </asp:AutoCompleteExtender>

                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblReceivedBy" runat="server" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtReceivedBy" runat="server" TabIndex="1" onblur="ClearReceivedBy()" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtReceivedBy_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListItemCssClass="AutoExtenderList"
                                CompletionSetCount="15" ContextKey="0" DelimiterCharacters="" EnableCaching="false"
                                Enabled="True" FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="ReceivedBy"
                                ServiceMethod="CompanyUserSearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtReceivedBy">
                            </asp:AutoCompleteExtender>
                            <asp:RequiredFieldValidator ID="reqrecivedby" runat="server" ControlToValidate="txtReceivedBy"
                                CssClass="messages" Display="Dynamic" ErrorMessage="" SetFocusOnError="true" ForeColor="" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-2">
                            <div style="display: none">
                                <asp:HiddenField ID="UsrhdfPartner" runat="server" />
                                <asp:TextBox ID="lblReceived" runat="server"></asp:TextBox>
                            </div>
                            <div id="hh" style="display: none">
                                <asp:TextBox ID="UsrlblPartner1" runat="server" BorderWidth="0px" Width="200px" CssClass="form-control input-sm"></asp:TextBox>
                                <asp:TextBox ID="hdfReceivedBy" runat="server" BorderWidth="0px" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <asp:Label ID="lblReceivedDate" runat="server" CssClass="bold"></asp:Label>
                            <div style="display: none">
                                <asp:TextBox ID="txtDateCompare" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="input-group">
                                <asp:TextBox ID="txtReceivedDate" runat="server" CssClass="form-control input-sm" TabIndex="1"
                                    ValidationGroup="Inward"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" AutoComplete="False"
                                ClearTextOnInvalid="True" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtReceivedDate"
                                UserDateFormat="DayMonthYear">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtReceivedDate"
                                TargetControlID="txtReceivedDate">
                            </asp:CalendarExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblInwardStartedOn" runat="server" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtInwardStartedOn" ValidationGroup="Inward" runat="server" CssClass="form-control input-sm"
                                    TabIndex="1" AutoPostBack="True" OnTextChanged="txtInwardStartedOn_TextChanged"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99/99/9999"
                                MaskType="Date" Enabled="True" TargetControlID="txtInwardStartedOn">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CalendarExtender2" TargetControlID="txtInwardStartedOn"
                                Format="dd/MM/yyyy" PopupButtonID="txtInwardStartedOn" runat="server">
                            </asp:CalendarExtender>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblStartedOnHH" runat="server" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtStartedOnHH" runat="server" CssClass="form-control input-sm" MaxLength="2"
                                TabIndex="1" ValidationGroup="Inward"></asp:TextBox>
                            <asp:MaskedEditExtender ID="metxtStartedOnHH" runat="server" ClearMaskOnLostFocus="true"
                                Mask="99" MaskType="Number" TargetControlID="txtStartedOnHH">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevtxtStartedOnHH" SetFocusOnError="true" runat="server"
                                ControlExtender="metxtStartedOnHH" ControlToValidate="txtStartedOnHH" CssClass="text-danger"
                                Enabled="false" InvalidValueBlurredMessage="Please Enter Valid Hour" InvalidValueMessage="Please Enter Valid Hour"
                                MaximumValue="23" MaximumValueMessage="Please Enter Valid Hour" MinimumValue="0"
                                ValidationGroup="Inward" Display="Dynamic"></asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ID="rfvtxtStartedOnHH" runat="server" ControlToValidate="txtStartedOnHH" Display="Dynamic"
                                CssClass="text-danger" Enabled="false" ErrorMessage="" SetFocusOnError="true" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblStartedOnMM" runat="server" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtStartedOnMM" runat="server" CssClass="form-control input-sm" TabIndex="1"
                                ValidationGroup="Inward"></asp:TextBox>
                            <asp:MaskedEditExtender ID="metxtStartedOnMM" runat="server" ClearMaskOnLostFocus="true"
                                Mask="99" MaskType="Number" TargetControlID="txtStartedOnMM">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevtxtStartedOnMM" SetFocusOnError="true" runat="server"
                                ControlExtender="metxtStartedOnMM" ControlToValidate="txtStartedOnMM" CssClass="text-danger"
                                Enabled="false" InvalidValueBlurredMessage="Please Enter Valid Minutes" InvalidValueMessage="Please Enter Valid Minutes"
                                MaximumValue="59" MaximumValueMessage="Please Enter Valid Minutes" MinimumValue="0"
                                ValidationGroup="Inward" Display="Dynamic"></asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ID="rfvtxtStartedOnMM" SetFocusOnError="true" runat="server" Display="Dynamic"
                                ControlToValidate="txtStartedOnMM" CssClass="text-danger" Enabled="false" ErrorMessage=""
                                ValidationGroup="Inward"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblInwardEndedOn" runat="server" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtInwardEndedOn" ValidationGroup="Inward" runat="server" CssClass="form-control input-sm"
                                    TabIndex="1" AutoPostBack="True" OnTextChanged="txtInwardEndedOn_TextChanged"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99/99/9999"
                                MaskType="Date" Enabled="True" TargetControlID="txtInwardEndedOn" AutoComplete="False"
                                ClearTextOnInvalid="True" UserDateFormat="DayMonthYear">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CalendarExtender3" TargetControlID="txtInwardEndedOn" Format="dd/MM/yyyy"
                                PopupButtonID="txtInwardEndedOn" runat="server">
                            </asp:CalendarExtender>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblEndedOnHH" runat="server" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtEndedOnHR" runat="server" CssClass="form-control input-sm" MaxLength="2"
                                TabIndex="1" ValidationGroup="Inward"></asp:TextBox>
                            <asp:MaskedEditExtender ID="metxtEndedOnHR" runat="server" ClearMaskOnLostFocus="false"
                                Enabled="false" Mask="99" MaskType="Number" TargetControlID="txtEndedOnHR">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevtxtEndedOnHR" runat="server" ControlExtender="metxtEndedOnHR"
                                ControlToValidate="txtEndedOnHR" CssClass="text-danger" Enabled="false" InvalidValueBlurredMessage="Please Enter Valid Hour"
                                InvalidValueMessage="Please Enter Valid Hour" MaximumValue="23" MaximumValueMessage="Please Enter Valid Hour"
                                MinimumValue="0" ValidationGroup="Inward" Display="Dynamic"></asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ID="rfvmetxtEndedOnHR" runat="server" ControlToValidate="txtEndedOnHR" Display="Dynamic"
                                SetFocusOnError="true"
                                CssClass="text-danger" Enabled="False" ErrorMessage="" ValidationGroup="Inward"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblEndedonMM" runat="server" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtEndedOnMM" runat="server" CssClass="form-control input-sm" TabIndex="1"></asp:TextBox>
                            <asp:MaskedEditExtender ID="metxtEndedOnMM" runat="server" ClearMaskOnLostFocus="false"
                                Enabled="false" Mask="99" MaskType="Number" TargetControlID="txtEndedOnMM">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevtxtEndedOnMM" runat="server" SetFocusOnError="true"
                                ControlExtender="metxtEndedOnMM" ControlToValidate="txtEndedOnMM" CssClass="text-danger"
                                Enabled="false" InvalidValueBlurredMessage="Please Enter Valid Minutes" InvalidValueMessage="Please Enter Valid Minutes"
                                MaximumValue="59" MaximumValueMessage="Please Enter Valid Minutes" MinimumValue="0"
                                ValidationGroup="Inward" Display="Dynamic"></asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ID="rfvtxtEndedOnMM" runat="server" SetFocusOnError="true"
                                ControlToValidate="txtEndedOnMM" CssClass="text-danger" Enabled="False" ErrorMessage="" Display="Dynamic"
                                ValidationGroup="Inward"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:Label ID="lblRemarks" runat="server" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtRemarks" runat="server" MaxLength="500" TabIndex="1" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-1" style="display: none;">
                            <br />
                            <asp:HiddenField ID="hdfInward" runat="server" />
                            <asp:HiddenField ID="hdfPageType" runat="server" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12" style="overflow: auto; max-height: 350px;">
                    <asp:GridView ID="grdInward" runat="server" AutoGenerateColumns="False" OnRowCommand="grdInward_RowCommand"
                        ShowFooter="True" CssClass="table table-striped table-hover table-Full text-nowrap">
                        <Columns>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblSrNo" Text="No" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" Text='<%#Container.DataItemIndex + 1 %>' runat="server" />
                                    <asp:Label ID="lblRowIndex" Text='<%#Container.DataItemIndex %>' Visible="false"
                                        runat="server" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblLotNo" Text="Lot No" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblLotNoInternal" Text='<%# Eval("LotInternal") %>' ToolTip='<%# Eval("LotInternal") %>' runat="server" />
                                    <asp:Label ID="lblLotVersion" Text='<%# Eval("LotVersion") %>' ToolTip='<%# Eval("LotVersion") %>' runat="server" Visible="false" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblItem" Text="Item" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblInwardDetID" Visible="false" Text='<%#Eval("ID") %>' runat="server" />
                                    <asp:Label ID="lblReferenceID" Visible="false" Text='<%#Eval("VoucherDetailID") %>' runat="server" />
                                    <asp:LinkButton ID="lnkSelect" Text='<%#Eval("Item.Name") %>' ToolTip='<%#Eval("Item.Name") %>' ForeColor="Maroon"
                                        runat="server" CommandName="Sel" CausesValidation="false" CommandArgument='<%#Eval("Item_ID") %>'
                                        CssClass="lnk" TabIndex="1" />
                                    <br />
                                    <asp:Label ID="Label3" Text='<%#Eval("Item.Sales_TaxType.Name") %>' ForeColor="#3399ff" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPackRef" Text="Packing Ref" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPackingReference" Text='<%# Eval("PackingReference") %>' ToolTip='<%# Eval("PackingReference") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblItemMark" Text="Item Mark" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblItemMark" Text='<%# Eval("ItemMark") %>' ToolTip='<%# Eval("ItemMark") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblVakkal" Text="Vakkal" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblLotNoCustomer" Text='<%# Eval("LotCustomer") %>' ToolTip='<%# Eval("LotCustomer") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdVariety" Text="Variety/Count" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblVariety" Text='<%# Eval("Variety.Name") %>' ToolTip='<%# Eval("Variety.Name") %>' runat="server" />
                                    <asp:HiddenField ID="hdfVarietyID" Value='<%# Eval("Variety_ID") %>' runat="server" />
                                    <br />
                                    <asp:Label ID="lblCount" Text='<%# Eval("Count") %>' ForeColor="#3399ff" ToolTip='<%# Eval("Count") %>' runat="server" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblItemTotal" runat="server" Font-Bold="true" Text="Total"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblApprovedQty" Text="Quantity" runat="server" />
                                </HeaderTemplate>

                                <ItemTemplate>
                                    <div class="input-group">
                                        <asp:TextBox ID="txtApprovedQuantity" Width="70px" runat="server" Visible="true" Style="text-align: right !important"
                                            Text='<%# Eval("InwardQuantity") %>' ToolTip='<%# Eval("InwardQuantity")%>'
                                            ValidationGroup="Inward" CausesValidation="true" TabIndex="1" MaxLength="10" CssClass="form-control input-sm" />
                                        <asp:RequiredFieldValidator ID="rfvApprovedQuantity" SetFocusOnError="true" Display="Dynamic"
                                            ValidationGroup="Inward" runat="server" ErrorMessage="" ControlToValidate="txtApprovedQuantity"></asp:RequiredFieldValidator>
                                        <asp:CompareValidator ID="CPVWeighQty" SetFocusOnError="true" Display="Dynamic" runat="server"
                                            ErrorMessage="Incorect Qty" ValidationGroup="Inward" ControlToValidate="txtApprovedQuantity"
                                            Type="Double" ValueToCompare='<%# Eval("InwardQuantity") %>' Operator="LessThanEqual"></asp:CompareValidator>
                                        <asp:CompareValidator ID="CPVAppQtyMO" runat="server" ErrorMessage="Incorect Qty"
                                            ValidationGroup="Inward" ControlToValidate="txtApprovedQuantity" Type="Double"
                                            Display="Dynamic" ValueToCompare='<%# Eval("InwardQuantity") %>' Operator="GreaterThanEqual"
                                            SetFocusOnError="True"></asp:CompareValidator>
                                        <asp:FilteredTextBoxExtender ID="FTApprovedQuantity" TargetControlID="txtApprovedQuantity"
                                            FilterType="Numbers,Custom" ValidChars="." runat="server">
                                        </asp:FilteredTextBoxExtender>
                                        <asp:Label ID="lblItemQuantity" Text='<%# Eval("InwardQuantity") %>' Visible="false" ToolTip='<%# Eval("InwardQuantity") %>'
                                            runat="server" />
                                    </div>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lbltotal" ForeColor="Maroon" Font-Bold="true" runat="server"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                                <FooterStyle CssClass="text-align-right" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblLocation" Text="Location" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <asp:Label ID="lblStorageLocationIDInitial" Visible="false" Text='<%# Eval("StorageLocation_ID") %>'
                                                runat="server" />
                                            <asp:Label ID="lblStorageLocationNameInitial" Text='<%# Eval("StorageLocation.Name") %>'
                                                ToolTip='<%# Eval("StorageLocation.Name") %>' runat="server" />
                                        </div>
                                        <div class="col-lg-6 control-label">
                                            <a><i class="fa fa-arrow-down heading"></i></a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblScheme" Text="Scheme" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <asp:Label ID="lblScheme" Visible="false" Text='<%# Eval("StorageSchemeRate_ID") %>' runat="server" />
                                            <asp:Label ID="lblSchemeName" ForeColor="Maroon" Text='<%# Eval("StorageSchemeRate.StorageScheme.Code") %>' ToolTip='<%# Eval("StorageSchemeRate.StorageScheme.Code") %>' runat="server" />
                                            <br />
                                            <asp:Label ID="lblSchemeRate" Text='<%# Eval("StorageSchemeRate.Rate") %>' ForeColor="#3399ff" runat="server" />
                                        </div>
                                        <div class="col-lg-6 control-label">
                                            <a><i class="fa fa-arrow-down heading"></i></a>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdBrand" Text="Brand/Origin" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblBrand" Text='<%# Eval("Brand.Name") %>' ToolTip='<%# Eval("Brand.Name") %>' runat="server" />
                                    <asp:HiddenField ID="hdfBrandID" Value='<%# Eval("Brand_ID") %>' runat="server" />
                                    <br />
                                    <asp:Label ID="lblorigin" Text='<%# Eval("Origin.Name") %>' ToolTip='<%# Eval("Origin.Name") %>' runat="server" />
                                    <asp:HiddenField ID="hdfOriginID" Value='<%# Eval("Origin_ID") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPalletNo" Text="Pallet/Container No" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPalletNo" Text='<%# Eval("PalletNo") %>' ToolTip='<%# Eval("PalletNo") %>' runat="server" />
                                    <br />
                                    <asp:Label ID="lblContainerNo" Text='<%# Eval("ContainerNo") %>' ToolTip='<%# Eval("ContainerNo") %>'
                                        runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblExpiryDate" Text="Expiry Date" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblExpiryDate" Visible="false" Text='<%# Eval("ExpiryDate") %>'
                                        runat="server" />
                                    <asp:Label ID="lblAlertFlag" Visible="false" Text='<%# Eval("ExpiryDate") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkDelete" Visible="false" runat="server" TabIndex="18" />
                                    <asp:LinkButton ID="btnDeleteItem" ForeColor="Maroon" runat="server" CommandName="Del" CommandArgument='<%# Eval("ID") %>' CausesValidation="False" TabIndex="1">
                                                <i class="fa fa-close fa-2x"></i>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>

            <div class="panel" id="pnlAddItem" style="display: none" runat="server">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="heading">Add Item</div>
                        </div>
                        <div class="col-lg-8">
                        </div>
                        <div class="col-lg-1">
                            <asp:LinkButton CausesValidation="true" ID="btnDetailSave" runat="server" OnClick="btnDetailSave_Click" CssClass="btn btn-primary btn-sm"
                                TabIndex="1" OnClientClick="return validateForm()" ValidationGroup="InwardDetail" />
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-lg-2">
                            <div style="display: none;">
                                <asp:TextBox ID="UsrlblItem" runat="server" CssClass="bold"></asp:TextBox>
                                <asp:TextBox ID="UsrlblLocation" runat="server" CssClass="bold"></asp:TextBox>

                            </div>
                            <asp:Label ID="lblLotNoInternal" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtLotNoInternal" runat="server" CssClass="form-control input-sm" MaxLength="20"
                                TabIndex="1" ValidationGroup="InwardDetail" />
                            <asp:FilteredTextBoxExtender ID="txtLotNoInternal_FilteredTextBoxExtender" FilterType="Numbers"
                                runat="server" Enabled="True" TargetControlID="txtLotNoInternal">
                            </asp:FilteredTextBoxExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblItem" runat="server" CssClass="bold" />
                            <asp:Label ID="Label7" runat="server" CssClass="text-danger" Text="*" />
                            <asp:TextBox ID="UsrtxtItemN" runat="server" TabIndex="1" onblur="ClearItem()" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="UsrtxtItemN_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="ItemSearch" ServiceMethod="ItemSearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="UsrtxtItemN">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblItemMark" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtItemMark" runat="server" CssClass="form-control input-sm" MaxLength="15"
                                TabIndex="1" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPackingRefer" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtPackingReference" runat="server" CssClass="form-control input-sm" MaxLength="20"
                                TabIndex="1" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblStorageSchemeRate" runat="server" CssClass="bold" />
                            <asp:Label ID="Label9" runat="server" CssClass="text-danger" Text="*" />
                            <asp:TextBox ID="txtStorageSchemeRate" runat="server" TabIndex="1" onblur="ClearScheme()" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACE_txtStorageSchemeRate" runat="server" CompletionInterval="1"
                                CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionSetCount="10"
                                ContextKey="0" DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="SchemeCodeSearch" ServiceMethod="SchemeSearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtStorageSchemeRate" UseContextKey="true">
                            </asp:AutoCompleteExtender>
                            <div style="display: none">
                                <asp:HiddenField ID="hdfStorageSchemeRateID" runat="server" />
                                <asp:HiddenField ID="hdfStorageSchemeRateName" runat="server" />
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblItemQuantity" runat="server" CssClass="bold" />
                            <asp:Label ID="Label10" runat="server" CssClass="text-danger" Text="*" />
                            <asp:TextBox ID="txtItemQuantity" runat="server" CssClass="form-control input-sm" MaxLength="10"
                                TabIndex="1" ValidationGroup="InwardDetail" />
                            <asp:CompareValidator ID="cvitemqty" ValidationGroup="InwardDetail" runat="server"
                                ControlToValidate="txtItemQuantity" Display="Dynamic" ErrorMessage="" Operator="GreaterThanEqual"
                                Type="Double" ValueToCompare="0"></asp:CompareValidator>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtItemQuantity"
                                FilterType="Numbers,Custom" ValidChars="." runat="server">
                            </asp:FilteredTextBoxExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server"
                                ControlToValidate="txtItemQuantity" CssClass="text-danger" ErrorMessage="" SetFocusOnError="true"
                                ValidationGroup="InwardDetail"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblorigin" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtOrigin" runat="server" CssClass="form-control input-sm" TabIndex="1" onblur="ClearOrigin()"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtOrigin_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="OriginSearch" ServiceMethod="OriginSearch"
                                ServicePath="~/Services/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtOrigin">
                            </asp:AutoCompleteExtender>
                            <div style="display: none">
                                <asp:HiddenField ID="hdfOriginID" runat="server" />
                                <asp:TextBox ID="HideOrigin" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblBrand" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control input-sm" TabIndex="1" onblur="ClearBrand()"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtBrand_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="BrandSearch" ServiceMethod="BrandSearch"
                                ServicePath="~/Services/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtBrand">
                            </asp:AutoCompleteExtender>
                            <div style="display: none">
                                <asp:HiddenField ID="hdfBrandID" runat="server" />
                                <asp:TextBox ID="HideBrand" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblVariety" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtVariety" runat="server" CssClass="form-control input-sm" TabIndex="1" onblur="ClearVariety()"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtVariety_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="VarietySearch" ServiceMethod="VarietySearch"
                                ServicePath="~/Services/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtVariety">
                            </asp:AutoCompleteExtender>
                            <div style="display: none">
                                <asp:HiddenField ID="hdfVarietyID" runat="server" />
                                <asp:TextBox ID="HideVariety" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblcount" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtcount" runat="server" CssClass="form-control input-sm" MaxLength="50" TabIndex="1" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblStorageLocationIDInitial" runat="server" CssClass="bold" />
                            <asp:Label ID="Label13" runat="server" CssClass="text-danger" Text="*" />
                            <asp:TextBox ID="UsrtxtLocation" runat="server" TabIndex="1" onblur="ClearLocation()" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="UsrtxtLocation_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="10" ContextKey="0" DelimiterCharacters="" EnableCaching="false"
                                Enabled="True" FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="LocationCodeSearch"
                                ServiceMethod="StorageLocationSearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="UsrtxtLocation"
                                UseContextKey="true">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblLotNoCustomer" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtLotNoCustomer" runat="server" CssClass="form-control input-sm" MaxLength="20"
                                TabIndex="1" ValidationGroup="InwardDetail" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPalletNo" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtPalletNo" runat="server" CssClass="form-control input-sm" MaxLength="50"
                                TabIndex="1" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblContainerNo" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtContainerNo" runat="server" CssClass="form-control input-sm" MaxLength="50"
                                TabIndex="1" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblExpiryYearMonth" runat="server" CssClass="bold" />
                            <div class="input-group">
                                <asp:TextBox ID="UsrtxtYearMonth" runat="server" TabIndex="1" onblur="ClearYear()" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:CalendarExtender ID="CalendarExtender5" TargetControlID="UsrtxtYearMonth" Format="dd/MM/yyyy"
                                PopupButtonID="UsrtxtYearMonth" runat="server">
                            </asp:CalendarExtender>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-1">

                    <asp:HiddenField ID="UsrhdfItem" runat="server" />
                    <asp:HiddenField ID="hdfInwdDetailRowIndex" runat="server" />
                    <asp:HiddenField ID="hdfInwardDetail" runat="server" />
                    <asp:HiddenField ID="hdflotversion" runat="server" />
                    <asp:HiddenField ID="hdfversion" runat="server" />
                    <asp:HiddenField ID="hdfItemQuantity" runat="server" />
                    <asp:HiddenField ID="UsrhdfLocation" runat="server" />


                    <asp:HiddenField ID="hdfApprovedQty" runat="server" />

                    <asp:HiddenField ID="btnDummy" runat="server" />
                    <asp:HiddenField ID="hdfReferenceID" runat="server" />
                    <asp:HiddenField ID="hdfReferenceType" runat="server" />
                </div>

            </div>

        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grdInward" EventName="RowCommand" />
            <%-- <asp:AsyncPostBackTrigger ControlID="Lnkpreinward" EventName="Click" />--%>
            <%-- <asp:AsyncPostBackTrigger ControlID="btnpopsearch" EventName="Click" />--%>
            <asp:AsyncPostBackTrigger ControlID="btnPreInwardAddItem" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>

    <div runat="server" id="bottem" class="row" style="display: none">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-3">

            <asp:Label ID="lbleduser" Text="" runat="server" />
            <asp:Label ID="lbledbar" runat="server" Text=" |"></asp:Label>
            <asp:Label ID="lbleddate" Text="" runat="server" />

        </div>
        <div class="col-lg-3">

            <asp:Label ID="lblshowshotcut" runat="server" CssClass="text-danger" Visible="false" />
            <asp:CollapsiblePanelExtender ID="colshotcut" runat="server"
                CollapseControlID="lblshowshotcut" ExpandControlID="lblshowshotcut"
                ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" Collapsed="true"
                TargetControlID="Pnlcol">
            </asp:CollapsiblePanelExtender>
        </div>
        <div class="col-lg-3">
        </div>

    </div>

    <asp:Panel ID="Pnlcol" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <asp:Label ID="Lblsh" runat="server" Text="Alt+S : Save"></asp:Label>
                    <asp:Label ID="lblb" runat="server" Text=" |"></asp:Label>
                    <asp:Label ID="lblal" runat="server" Text="Alt+N : cursor goes directly to lot no textbox"></asp:Label>
                    <asp:Label ID="Lblbr" runat="server" Text=" |"></asp:Label>
                    <asp:Label ID="lblosd" runat="server" Text="Alt+K(On Approved qty) : Open OSD Popup"></asp:Label>
                </div>
            </div>
        </div>
    </asp:Panel>

    <%--All Modal--%>
    <div class="modal fade" id="modalprein" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                <div class="modal-header btn-primary">
                    <div class="heading">
                        <asp:Label ID="lblpophead" runat="server"></asp:Label>
                    </div>
                </div>

                <asp:UpdatePanel ID="upprein" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <asp:Label ID="lblparty" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtparty" TabIndex="1" runat="server" onblur="ClearParty()" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="txtparty_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                        CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                        CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionSetCount="10"
                                        DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                        MinimumPrefixLength="1" OnClientItemSelected="PartySearch" ServiceMethod="CompanySearch"
                                        ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                        UseContextKey="true" ContextKey="0" TargetControlID="txtparty">
                                    </asp:AutoCompleteExtender>
                                    <asp:RequiredFieldValidator ID="reqpreparty" runat="server" ControlToValidate="txtparty" SetFocusOnError="true"
                                        CssClass="text-danger" Display="Dynamic" ErrorMessage="" ValidationGroup="PreinwardSearch"></asp:RequiredFieldValidator>
                                    <div style="display: none">
                                        <asp:HiddenField ID="hdfParty" runat="server" />
                                        <asp:TextBox ID="HideParty" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <asp:Label ID="lblpredate" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtDate" runat="server" AutoCompleteType="None" TabIndex="1" AutoComplete="Off"
                                        CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:MaskedEditExtender ID="txtDate_MaskedEditExtender" runat="server" Enabled="True"
                                        Mask="99/99/9999" MaskType="Date" TargetControlID="txtDate">
                                    </asp:MaskedEditExtender>
                                    <asp:MaskedEditValidator ID="MaskedEditValidator1" ErrorMessage="" MaximumValue="31/12/3000"
                                        MinimumValue="01/01/2000" ControlExtender="txtDate_MaskedEditExtender" ControlToValidate="txtDate"
                                        runat="server"></asp:MaskedEditValidator>
                                    <asp:CalendarExtender ID="CalendarExtender4" TargetControlID="txtDate" Format="dd/MM/yyyy"
                                        PopupButtonID="txtDate" runat="server">
                                    </asp:CalendarExtender>
                                    <%--<asp:RequiredFieldValidator ID="reqtdate" runat="server" ControlToValidate="txtDate"
                            CssClass="text-danger" Display="Dynamic" ErrorMessage="" ValidationGroup="PreinwardSearch"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-12" style="overflow: auto;">
                                    <asp:GridView ID="grdPreinward" runat="server" AutoGenerateColumns="false" CssClass="table table-hover text-nowrap"
                                        OnPreRender="grdPreinward_PreRender">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                    <asp:HiddenField ID="hdfPreid" Value='<%#Eval("ID")%>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblPreinwardNo" Text="PreInward No" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpreinNo" Text='<%# Eval("PreInwardNo") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblDate" Text="Date" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpredate" Text='<%# Eval("PreInwardDate") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblGateNo" Text="Gate No" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgateno" Text='<%# Eval("GateEntry.GateNo") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblVehicleName" Text="Vehical No" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblvechcalnm" Text='<%# Eval("GateEntry.VehicalNo") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblRemarks" Text="Remarks" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpreremarks" Text='<%# Eval("Remarks") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Lnkpreinward" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnpopsearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="btnPreInwardAddItem" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <div class="modal-footer">
                    <asp:Button ID="btnPreInwardAddItem" runat="server" CssClass="btn btn-primary btn-sm" Text="" OnClick="btnPreInwardAddItem_Click" />
                    <asp:Button ID="btnpopsearch" runat="server" CssClass="btn btn-primary btn-sm" Text="" ValidationGroup="PreinwardSearch" OnClick="btnpopsearch_Click" />
                    <asp:Button ID="btnpopcancel" runat="server" CssClass="btn btn-primary btn-sm" data-dismiss="modal" />
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.div modalprein Modal -->

    <div class="modal fade" id="modalhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                <div class="modal-header">
                    <h1 class="modal-title">
                        <asp:Label ID="lbledit" runat="server" Text=""></asp:Label>
                    </h1>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:Label ID="lblcron" runat="server" CssClass="bold"></asp:Label>
                                    <asp:Label ID="lblcruser" Text="" runat="server" />
                                    <asp:Label ID="lblcrbar" runat="server" Text=" | " CssClass="bold"></asp:Label>
                                    <asp:Label ID="lblcrdate" Text="" runat="server" />
                                </div>
                            </div>
                            <div class="newline"></div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:Label ID="lblappon" runat="server" CssClass="bold"></asp:Label>
                                    <asp:Label ID="lalapuser" Text="" runat="server" />
                                    <asp:Label ID="lblapbar" runat="server" Text=" | " CssClass="bold"></asp:Label>
                                    <asp:Label ID="lblapdate" Text="" runat="server" />
                                </div>

                            </div>
                            <div class="newline"></div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:Label ID="lbldelon" runat="server" CssClass="bold"></asp:Label>
                                    <asp:Label ID="lbldluser" Text="" runat="server" />
                                    <asp:Label ID="lbldelbar" runat="server" Text=" | " CssClass="bold"></asp:Label>
                                    <asp:Label ID="lbldldate" Text="" runat="server" />
                                </div>
                            </div>
                            <div class="newline"></div>
                            <div class="row">
                                <div class="col-lg-12" style="text-align: center">
                                    <asp:Label ID="lbledon" runat="server" CssClass="bold"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:GridView ID="grdedit" runat="server" AutoGenerateColumns="false" OnPreRender="grdedit_OnPreRender"
                                        CssClass="table table-hover text-nowrap">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblUser" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbleduser" Text='<%# Eval("UserInfo.UserCode") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblUserFullName" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUserFullName" Text='<%# Eval("UserInfo.UserFullName") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblDate" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbleddate" Text='<%# Eval("CreateDate") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>

                            <asp:AsyncPostBackTrigger ControlID="btnhistory" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.div modalhistory Modal -->

</asp:Content>

