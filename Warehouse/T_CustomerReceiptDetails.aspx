﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="T_CustomerReceiptDetails" Codebehind="T_CustomerReceiptDetails.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainDiv">
        <asp:UpdatePanel ID="up_ReceiptVoucher" runat="server">
            <ContentTemplate>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <div id="divButtons" class="pull-right">
                                    <asp:LinkButton ID="btnSave" runat="server" OnClick="btnSave_Click" ValidationGroup="MainSave"><i class="fa fa-save  fa-2x text-success"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnCancel" OnClick="btnCancel_Click" TabIndex="1" runat="server"><i class="fa fa-close fa-2x"></i></asp:LinkButton>
                                </div>
                                <h3>Customer Receipt Details </h3>
                                <hr />
                                <div class="row">
                                    <div class=" col-lg-12">
                                        <asp:TabContainer runat="server" CssClass="Bigtab" ActiveTabIndex="0" ID="tabMain">
                                            <asp:TabPanel runat="server" HeaderText="Basic Info" TabIndex="0" ID="tblBasicInfo">
                                                <ContentTemplate>
                                                    <div class="row" runat="server" id="divBasicInfo">
                                                        <div id="divReceiptDate" class="col-lg-2">
                                                            <asp:Label ID="lblReceiptDate" runat="server" Text="Receipt Date" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblMsgReceiptDt" runat="server" Text="*" CssClass="bold text-danger"></asp:Label>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtReceiptDate" TabIndex="1" runat="server" placeholder="Receipt Date" CssClass="form-control input-sm" ValidationGroup="MainSave"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RFV_txtReceiptDate" runat="server" ErrorMessage="Select Date" ValidationGroup="MainSave" ControlToValidate="txtReceiptDate"></asp:RequiredFieldValidator>
                                                            <asp:MaskedEditExtender ID="Mee_txtReceiptDate" runat="server"
                                                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtReceiptDate">
                                                            </asp:MaskedEditExtender>
                                                            <asp:CalendarExtender ID="Ce_ReceiptDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtReceiptDate"
                                                                TargetControlID="txtReceiptDate">
                                                            </asp:CalendarExtender>
                                                        </div>
                                                        <div id="divPaymentType" class="col-lg-2">
                                                            <asp:Label ID="lblPaymentType" CssClass="bold " runat="server" Text="Receipt Type"></asp:Label>
                                                            <asp:Label ID="lblMsgPaymentType" runat="server" Text="*" CssClass="bold text-danger"></asp:Label>
                                                            <asp:DropDownList ID="ddlPaymentType" AutoPostBack="true" runat="server" CssClass="form-control input-sm" TabIndex="0" OnSelectedIndexChanged="ddlPaymentType_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RFV_ddlPaymentType" runat="server" ErrorMessage="" ValidationGroup="MainSave" ControlToValidate="ddlPaymentType" InitialValue="0"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div id="divCustomerName" class="col-lg-4">
                                                            <asp:Label ID="lblCustomerName" runat="server" Text="Customer" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblMsgCustomerName" runat="server" Text="*" CssClass="bold text-danger"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="RFV_txtCustomer" runat="server" CssClass="text-danger" ValidationGroup="MainSave" ControlToValidate="txtCustomer"></asp:RequiredFieldValidator>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtCustomer" TabIndex="1" AutoPostBack="true" placeholder="Select Customer Name" runat="server" AutoCompleteType="None" AutoComplete="Off" onblur="return ClearAutocompleteTextBox(this)" OnTextChanged="txtCustomerName_TextChanged"
                                                                    CssClass="form-control input-sm"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfCustomerName" Value="" />
                                                            <asp:HiddenField runat="server" ID="hdfCustomerID" Value="" />
                                                            <asp:AutoCompleteExtender ID="ACEtxtCustomer" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="3" UseContextKey="true"
                                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtCustomer">
                                                            </asp:AutoCompleteExtender>
                                                        </div>
                                                        <div id="divNoOfDueDays" class="col-lg-2">
                                                            <asp:Label ID="lblNoDueDays" runat="server" Text="No Of Due Days" CssClass="bold"></asp:Label>
                                                            <asp:TextBox ID="txtNoOfDueDays" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                        </div>
                                                        <div id="divCollectedAmount" class="col-lg-2">
                                                            <asp:Label ID="lblCollectedAmount" runat="server" Text="Collected Amount" CssClass="bold"></asp:Label>
                                                            <asp:TextBox ID="txtCollectedAmount" AutoPostBack="true" runat="server" CssClass="form-control input-sm" OnTextChanged="txtCollectedAmount_TextChanged"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                            <asp:TabPanel runat="server" HeaderText="Additional Info" TabIndex="0" ID="tblAddiInfo">
                                                <ContentTemplate>
                                                    <div class="row" id="divPayeeNameDetails">
                                                        <div id="divPayeeName" class="col-lg-3">
                                                            <asp:Label ID="lblPayeeName" runat="server" Text="Payee Name" CssClass="bold"></asp:Label>
                                                            <asp:TextBox ID="txtPayee" TabIndex="0" placeholder="Enter Payee Name" runat="server" AutoCompleteType="None" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <asp:Label ID="lblLedger" runat="server" Text="Ledger" CssClass="bold"></asp:Label>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtLedger" placeholder="Select Ledger" runat="server" MaxLength="500" AutoPostBack="false" TabIndex="2" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfLedgerName" />
                                                            <asp:HiddenField runat="server" ID="hdfLedgerID" />
                                                            <asp:AutoCompleteExtender ID="ACEtxtLedger" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLedger">
                                                            </asp:AutoCompleteExtender>
                                                        </div>
                                                        <div id="divDescription" class="col-lg-6">
                                                            <asp:Label ID="lblDescription" runat="server" Text="Description" CssClass="bold"></asp:Label>
                                                            <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row" id="divVoucherDetails">
                                                        <div id="divVoucherNo" class="col-lg-3">
                                                            <asp:Label ID="lblVoucherNo" runat="server" Text="Voucher No" CssClass="bold"></asp:Label>
                                                            <asp:TextBox ID="txtVoucherNo" TabIndex="0" Enabled="false" runat="server" AutoCompleteType="None" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <asp:RequiredFieldValidator ID="Rfv_txtCurrency" runat="server" ControlToValidate="txtCurrency" CssClass="text-danger"
                                                                Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                            <asp:Label ID="lblCurrency" CssClass="bold " runat="server" Text="Currency"> </asp:Label>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtCurrency" Text='<%# Eval("CurrencyType.Name") %>' TabIndex="1" placeholder="Select Transaction Currency" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfCurrencyName" Value='<%# Eval("CurrencyType.Name") %>' />
                                                            <asp:HiddenField runat="server" ID="hdfCurrencyID" Value='<%# Eval("CurrencyType_ID") %>' />
                                                            <asp:AutoCompleteExtender ID="ACEtxtCurrency" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CurrencyTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtCurrency">
                                                            </asp:AutoCompleteExtender>

                                                        </div>

                                                        <div id="divExchangeCurrency" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderExchangeCurrency" CssClass="bold " runat="server" Text="Transaction Currency"> </asp:Label>
                                                            <asp:Label ID="lblHeaderExchangeCurrencyMsg" runat="server" CssClass=" bold text-danger" Text=" "></asp:Label>
                                                            <asp:RequiredFieldValidator ID="Rfv_txtHeaderExchangeCurrency" runat="server" ControlToValidate="txtHeaderExchangeCurrency" CssClass="text-danger"
                                                                Display="Dynamic" ErrorMessage=" (Select Currency)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtHeaderExchangeCurrency" TabIndex="1" placeholder="Select Transaction Currency" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfHeaderExchangeCurrencyName" Value="" />
                                                            <asp:HiddenField runat="server" ID="hdfHeaderExchangeCurrencyID" Value="" />
                                                            <asp:AutoCompleteExtender ID="ACEtxtHeaderExchangeCurrency" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CurrencyTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtHeaderExchangeCurrency">
                                                            </asp:AutoCompleteExtender>
                                                        </div>
                                                        <div id="divExchangeCurrencyRate" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderExchangeCurrencyRate" Text="Exchange Currency Rate" runat="server" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblHeaderExchangeCurrencyRateMsg" runat="server" CssClass="text-danger" Text=""></asp:Label>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtHeaderExchangeCurrencyRate" placeholder="0.00" runat="server" AutoPostBack="True" CssClass="form-control amt input-sm"
                                                                    onkeypress="return validateFloatKeyPress(this, event)" TabIndex="1" ReadOnly="True"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                        </asp:TabContainer>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="divBankDetails" runat="server" visible="false">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body" style="padding-top: 10px;">
                                <div class="row" id="CompanyBankDetails">
                                    <div id="divCompanyAccountNo" class="col-lg-3">
                                        <asp:Label ID="lblCompanyAccountNo" CssClass="bold " runat="server" Text="Company Account Number"></asp:Label>
                                        <asp:DropDownList ID="ddlCompanyAccountNo" AutoPostBack="true" runat="server" CssClass="form-control input-sm" TabIndex="0" OnSelectedIndexChanged="ddlCompanyAccountNo_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divCompanyBankBranch" class="col-lg-3">
                                        <asp:Label ID="lblCompanyBankBranch" CssClass="bold " runat="server" Text="Company Bank Branch"></asp:Label>
                                        <asp:DropDownList ID="ddlCompanyBankBranch" Enabled="false" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divCompanyBank" class="col-lg-2">
                                        <asp:Label ID="lblCompanyBank" CssClass="bold " runat="server" Text="Company Bank"></asp:Label>
                                        <asp:DropDownList ID="ddlCompanyBank" Enabled="false" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divChequeNo" class="col-lg-2" runat="server">
                                        <asp:Label ID="lblChequeNo" runat="server" Text="Cheque / DD No" CssClass="bold"></asp:Label>
                                        <asp:TextBox ID="txtChequeNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <div id="divChequeDate" class="col-lg-2" runat="server">
                                        <asp:Label ID="lblChequeDate" runat="server" Text="Cheque Date" CssClass="bold"></asp:Label>
                                        <div class="input-group">
                                            <asp:TextBox ID="txtChequeDate" TabIndex="1" runat="server" placeholder="Cheque Date" CssClass="form-control input-sm"></asp:TextBox>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <asp:MaskedEditExtender ID="MEE_ChequeDate" runat="server"
                                            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtChequeDate">
                                        </asp:MaskedEditExtender>
                                        <asp:CalendarExtender ID="CE_ChequeDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtChequeDate"
                                            TargetControlID="txtChequeDate">
                                        </asp:CalendarExtender>
                                    </div>
                                </div>
                                <div class="row hidden" id="CustomerBankDetails">
                                    <div id="divCustomerAccountNo" class="col-lg-3">
                                        <asp:Label ID="lblCustomerAccountNo" CssClass="bold " runat="server" Text="Customer Account Number"></asp:Label>
                                        <asp:DropDownList ID="ddlCustomerAccountNo" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerAccountNo_SelectedIndexChanged" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divCustomerBankBranch" class="col-lg-3">
                                        <asp:Label ID="lblCustomerBankBranch" CssClass="bold " runat="server" Text="Customer Bank Branch"></asp:Label>
                                        <asp:DropDownList ID="ddlCustomerBankBranch" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divCustomerBank" class="col-lg-2">
                                        <asp:Label ID="lblCustomerBank" CssClass="bold " runat="server" Text="Customer Bank"></asp:Label>
                                        <asp:DropDownList ID="ddlCustomerBank" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div class="row" id="divBillDetails" runat="server">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <asp:UpdatePanel ID="up_ReceiptVoucherDetails" runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="grdSaleBillDetails" runat="server" AutoGenerateColumns="False" OnPreRender="grdSaleBillDetails_PreRender"
                                GridLines="Horizontal"
                                CssClass="table table-striped table-hover  text-nowrap  dt-responsive nowrap">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <input id="SelectAll" onclick="SelectAll(this);" runat="server" type="checkbox" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="ChkSelect" Checked='<%# Eval("IsReceiptChecked") %>' AutoPostBack="true" Enabled="true" OnCheckedChanged="ChkSelect_CheckedChanged" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdrSaleBillNo" Text="Bill No" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemSaleBillNo" Text='<%# Eval("BillNo") %>' runat="server" CssClass="bold"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdrBillDate" Text="Bill Date" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBilldate" runat="server" Text='<%# Convert.ToDateTime(Eval("BillDate")).ToString("dd/MM/yyyy") %>' TabIndex="1" AutoComplete="Off" CssClass="bold"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdrGrossAmount" Text="Bill Amount" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBillAmount" runat="server" TabIndex="1" Text='<%# Eval("BillAmount") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdrReceiptAmt" Text="Receipt Amount" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotalReceiptAmount" runat="server" TabIndex="1" Text='<%# Eval("ReceiptAmount") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                            <asp:Label ID="lblReceiptAmount" runat="server" TabIndex="1" AutoComplete="Off" Visible="false" CssClass="bold hidden"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdrBalanceAmt" Text="Balance Amount" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBalanceAmount" runat="server" TabIndex="1" Text='<%# Eval("BalanceAmount") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdrReceiptNewAmt" Text="Amount" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%-- Text='<%# Eval("FinalAmount") %>'--%>
                                            <asp:TextBox ID="txtReceiptAmt" Enabled='<%# Eval("IsReceiptChecked") %>' TabIndex="1" Text='<%# Eval("Amount") %>' AutoPostBack="true" OnTextChanged="txtReceiptAmt_TextChanged" placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField ID="hdfPBVoucherType" Value='<%# Eval("SaleBillVoucherType") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdrDiscount" Text="Rebate" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtDiscount" Enabled="true" TabIndex="1" Text='<%# Eval("TDSAmount") %>' AutoPostBack="true" OnTextChanged="txtDiscount_TextChanged" placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdrTDSAmount" Text="TDS Amount" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtTDSAmount" Enabled="true" TabIndex="1" Text='<%# Eval("TDSAmount") %>' AutoPostBack="true" OnTextChanged="txtTDSAmount_TextChanged" placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdrTDSLedger" Text="TDS Ledger" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtTDSLedger" placeholder="Select Ledger" runat="server" MaxLength="500" AutoPostBack="false" TabIndex="2" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>                                                     
                                        <asp:HiddenField runat="server" ID="hdfTDSLedgerName" />
                                        <asp:HiddenField runat="server" ID="hdfTDSLedgerID" />
                                        <asp:AutoCompleteExtender ID="ACEtxtTDSLedger" runat="server"
                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtTDSLedger">
                                        </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>


                            <div class="row">
                                <div id="divTotalReceipt" class="col-lg-3">
                                    <asp:Label ID="lblTotalReceipt" runat="server" Text="Total Receipt" CssClass="bold"></asp:Label>
                                    <asp:TextBox ID="txtTotalReceipt" Enabled="false" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div id="divAdvanceAmount" class="col-lg-3">
                                    <asp:Label ID="lblAdvanceAmount" CssClass="bold " runat="server" Text="Advance Amount"></asp:Label>
                                    <asp:TextBox ID="txtAdvanceAmount" Enabled="false" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div id="divSetOffAmt" class="col-lg-3">
                                    <asp:Label ID="lblSetOffAmt" CssClass="bold " runat="server" Text="Set Off Amount"></asp:Label>
                                    <asp:TextBox ID="txtSetOffAmt" AutoPostBack="true" Enabled="true" OnTextChanged="txtSetOffAmt_TextChanged" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div id="divTotalSettledAmount" class="col-lg-3">
                                    <asp:Label ID="lblTotalSettledAmount" CssClass="bold " runat="server" Text="Total Settled Amount"></asp:Label>
                                    <asp:TextBox ID="txtTotalSettledAmount" Enabled="false" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                            </div>


                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="grdSaleBillDetails" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>



</asp:Content>
