﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using AjaxControlToolkit;

public partial class T_PaymentVoucherDetails : BigSunPage
{

    #region PageLoad
    AppObjects.AppPaymentVoucher objPaymentVoucher = null;
    CommonFunctions CF = new CommonFunctions();
    protected void Page_Load(object sender, EventArgs e)
    {
        objPaymentVoucher = new AppPaymentVoucher(intRequestingUserID);
        if (!IsPostBack)
        {
            ACEtxtVendor.ContextKey = new AppConvert((int)AppObjects.CompanyRoleType.Vendor);
            Session[TableConstants.SessionPaymentVoucherDetail] = objPaymentVoucher;
            BindToDropdown();
            string ID = Convert.ToString(Request.QueryString["PaymentVoucherID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int PaymentVoucherID = new AppUtility.AppConvert(ID);
                objPaymentVoucher = new AppObjects.AppPaymentVoucher(intRequestingUserID, PaymentVoucherID);
                Session[TableConstants.SessionPaymentVoucherDetail] = objPaymentVoucher;
                PopulateData();
                SetPaymentFor();
                if (Convert.ToString(Request.QueryString["Msg"]) == "2")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Contra Approved Sucessfully','1'" + ");", true);
                }
              
            }
            else
            {
                DefaultData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
                Session[TableConstants.SessionPaymentVoucherDetail] = objPaymentVoucher;
            }
            BindData();
        }
    }
    #endregion
    #region Text Events
    protected void txtVendorName_TextChanged(object sender, EventArgs e)
    {
        objPaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        int VendorCompanyID = new AppConvert(hdfVendorID.Value);
        objPaymentVoucher.Vendor_Company_ID = VendorCompanyID;
        objPaymentVoucher.PaymentFor = new AppConvert(ddlPaymentFor.SelectedValue);
        BindVendorDetails(VendorCompanyID);
        BindData();
    }
    protected void txtLedger_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfLedgerID = (HiddenField)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("hdfLedgerID");
        TextBox txtPaymentVoucherDesc = (TextBox)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("txtPaymentVoucherDesc");
        AppPaymentVoucher PaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        PaymentVoucher.PaymentVoucherDetailColl[RowIndex].Ledger_ID = new AppUtility.AppConvert(hdfLedgerID.Value);
        Session.Add(TableConstants.SessionPaymentVoucherDetail, PaymentVoucher);
        BindData();
        txtPaymentVoucherDesc.Focus();
    }
    protected void txtPaymentVoucherDesc_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtAmount = (TextBox)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("txtAmount");
        TextBox txtPaymentVoucherDesc = (TextBox)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("txtPaymentVoucherDesc");
        AppPaymentVoucher PaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        PaymentVoucher.PaymentVoucherDetailColl[RowIndex].PaymentVoucherDesc = new AppUtility.AppConvert(txtPaymentVoucherDesc.Text);
        Session.Add(TableConstants.SessionPaymentVoucherDetail, PaymentVoucher);
        BindData();
        txtAmount.Focus();
    }
    protected void txtAmount_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfTDSRate = (HiddenField)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("hdfTDSRate");
        TextBox txtTDS = (TextBox)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("txtTDS");
        TextBox txtAmount = (TextBox)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("txtAmount");
        AppPaymentVoucher PaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        PaymentVoucher.PaymentVoucherDetailColl[RowIndex].Amount = new AppUtility.AppConvert(txtAmount.Text);
        PaymentVoucher.PaymentVoucherDetailColl[RowIndex].ModifiedBy = intRequestingUserID;
        PaymentVoucher.PaymentVoucherDetailColl[RowIndex].ModifiedOn = System.DateTime.Now;
        PaymentVoucher.PaymentVoucherDetailColl[RowIndex].Status = new AppConvert((int)RecordStatus.Created);
        PaymentVoucher.PaymentVoucherDetailColl[RowIndex].TDSAmount = TDSAmountCal(new AppConvert(hdfTDSRate.Value), new AppConvert(txtAmount.Text));
        Session.Add(TableConstants.SessionPaymentVoucherDetail, PaymentVoucher);
        BindData();
        txtTDS.Focus();
    }
    protected void txtTDS_TextChanged(object sender, EventArgs e)
    {

        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfTDSRate = (HiddenField)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("hdfTDSRate");
        HiddenField hdfTDSID = (HiddenField)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("hdfTDSID");
        TextBox txtTDS = (TextBox)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("txtTDS");
        TextBox txtAmount = (TextBox)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("txtAmount");

        AppPaymentVoucher PaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        PaymentVoucher.PaymentVoucherDetailColl[RowIndex].TDS_ID = new AppUtility.AppConvert(hdfTDSID.Value);
        PaymentVoucher.PaymentVoucherDetailColl[RowIndex].TDS_Ledger_ID = new AppUtility.AppConvert(PaymentVoucher.PaymentVoucherDetailColl[RowIndex].TDS.Ledger_ID);
        decimal TDSRate = CF.GetTDSRate(hdfTDSID.Value, txtPaymentDate.Text);
        if (TDSRate == -99)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('TDS rate is not defined.','1'" + ");", true);
        }
        else
        {
            PaymentVoucher.PaymentVoucherDetailColl[RowIndex].TDSRate = TDSRate;
            PaymentVoucher.PaymentVoucherDetailColl[RowIndex].TDSAmount = TDSAmountCal(TDSRate, new AppConvert(txtAmount.Text));
        }
        Session.Add(TableConstants.SessionPaymentVoucherDetail, PaymentVoucher);
        BindData();
        txtTDS.Focus();


    }
    protected void txtProfitCenter_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfProfitCenterID = (HiddenField)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("hdfProfitCenterID");
        TextBox txtCostCenter = (TextBox)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("txtCostCenter");
        AppPaymentVoucher PaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        PaymentVoucher.PaymentVoucherDetailColl[RowIndex].ProfitCenter_ID = new AppUtility.AppConvert(hdfProfitCenterID.Value);
        Session.Add(TableConstants.SessionPaymentVoucherDetail, PaymentVoucher);
        BindData();
        txtCostCenter.Focus();
    }
    protected void txtCostCenter_TextChanged(object sender, EventArgs e)
    {

        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfCostCenterID = (HiddenField)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("hdfCostCenterID");
        Button btnAddLineItem = (Button)grdPaymentVoucherDetails.HeaderRow.FindControl("btnAddLineItem");
        AppPaymentVoucher PaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        PaymentVoucher.PaymentVoucherDetailColl[RowIndex].CostCenter_ID = new AppUtility.AppConvert(hdfCostCenterID.Value);
        Session.Add(TableConstants.SessionPaymentVoucherDetail, PaymentVoucher);
        BindData();
        btnAddLineItem.Focus();

    }
    protected void txtTDSAmount_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtTDSAmount = (TextBox)grdPaymentVoucherDetails.Rows[RowIndex].FindControl("txtTDSAmount");
        Button btnAddLineItem = (Button)grdPaymentVoucherDetails.HeaderRow.FindControl("btnAddLineItem");
        AppPaymentVoucher PaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        PaymentVoucher.PaymentVoucherDetailColl[RowIndex].TDSAmount = new AppUtility.AppConvert(txtTDSAmount.Text);
        Session.Add(TableConstants.SessionPaymentVoucherDetail, PaymentVoucher);
        BindData();
        btnAddLineItem.Focus();

    }
    #endregion
    #region Functions
    public void DefaultData()
    {
        txtPaymentDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtCurrency.Text = this.DefaultBaseCurrency;
        hdfCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);
        hdfCurrencyName.Value = txtCurrency.Text;
        ACEtxtLedger.ContextKey = Convert.ToString(((ddlPaymentType.SelectedValue == "1") ? "4" : "3") + (",~" + "~" + new AppUtility.AppConvert((int)AppObjects.LedgerFlag.GeneralLedger)));
    }
    public void BindVendorDetails(int VendorCompanyID)
    {
        if (VendorCompanyID > 0)
        {
            AppCompany objVendorDetails = new AppCompany(intRequestingUserID, VendorCompanyID);
            CommonFunctions.BindDropDown(intRequestingUserID, Components.CompanyBank, ddlVendorAccountNo, "AccountNo", "ID", "", false, VendorCompanyID);
            if (ddlVendorAccountNo.Items.Count > 0) { ddlVendorAccountNo.SelectedValue = new AppConvert(objVendorDetails.DefaultCompanyBank.ID); }
            CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlVendorBankBranch, "Name", "ID", "", true, VendorCompanyID);
            if (ddlVendorBankBranch.Items.Count > 0) { ddlVendorBankBranch.SelectedValue = new AppConvert(objVendorDetails.DefaultCompanyBank.BankType_ID); }
            CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlVendorBank, "BankBranch", "ID", "", true, VendorCompanyID);
            if (ddlVendorBank.Items.Count > 0) { ddlVendorBank.SelectedValue = new AppConvert(objVendorDetails.DefaultCompanyBank.BankType_ID); }
            txtPayee.Text = objVendorDetails.Name;
        }
        else
        {
            ddlVendorAccountNo.Items.Clear();
            ddlVendorBankBranch.Items.Clear();
            ddlVendorBank.Items.Clear();

        }
    }
    public decimal TDSAmountCal(decimal TDSRate, decimal VoucherAmount)
    {
        decimal TDSAmount = 0;
        if (TDSRate > 0)
        {
            TDSAmount = new AppConvert(Math.Round(((Convert.ToDouble(VoucherAmount) * 0.01) * Convert.ToDouble(TDSRate)), 2));
            return TDSAmount;
        }
        else
        {
            return 0;
        }

    }
    public void SetValueToGridFooter()
    {
        AppPaymentVoucher PaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        Label lblFooterAmount = (Label)grdPaymentVoucherDetails.FooterRow.FindControl("lblFooterAmount");
        lblFooterAmount.Text = Convert.ToDecimal(PaymentVoucher.PaymentVoucherDetailColl.Where(x => x.Status == 0).Sum(x => x.Amount)).ToString("#,##0.00");
        Label lblFooterTDSAmount = (Label)grdPaymentVoucherDetails.FooterRow.FindControl("lblFooterTDSAmount");
        lblFooterTDSAmount.Text = Convert.ToDecimal(PaymentVoucher.PaymentVoucherDetailColl.Where(x => x.Status == 0).Sum(x => x.TDSAmount)).ToString("#,##0.00");
        txtNetPayable.Text = lblFooterAmount.Text;

    }
    #region BindData   
    protected void BindData()
    {
        AppPaymentVoucher objPaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        if (objPaymentVoucher == null)
        {
            objPaymentVoucher = new AppPaymentVoucher(intRequestingUserID);
        }

        if (hdfVendorID.Value != "")
        {
            objPaymentVoucher.Vendor_Company_ID = new AppConvert(hdfVendorID.Value);
        }
        if (objPaymentVoucher.PaymentVoucherDetailColl.Count <= 0)
        {
            AddEmptyRow(1);
        }
        else
        {
            if (ddlPaymentFor.SelectedValue == "1" || ddlPaymentFor.SelectedValue == "3")
            {
                objPaymentVoucher.PaymentVoucherDetailColl[0].Ledger_ID = objPaymentVoucher.Vendor_Company.Vendor_Ledger_ID;
            }
            
        }
        grdPaymentVoucherDetails.DataSource = objPaymentVoucher.PaymentVoucherDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created));
        grdPaymentVoucherDetails.DataBind();
        divBillDetails.Visible = true;
        Session.Add(TableConstants.SessionPaymentVoucherDetail, objPaymentVoucher);
        SetValueToGridFooter();
    }

    public void AddEmptyRow(int NumberOfRows)
    {
        AppPaymentVoucher objPaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        if (objPaymentVoucher == null)
        {
            objPaymentVoucher = new AppPaymentVoucher(intRequestingUserID);
        }
        AppPaymentVoucherDetail PVDC = new AppPaymentVoucherDetail(intRequestingUserID);
        if (ddlPaymentFor.SelectedValue == "1" || ddlPaymentFor.SelectedValue == "3")
        {
            PVDC.Ledger_ID = objPaymentVoucher.Vendor_Company.Vendor_Ledger_ID;

        }
        else
        {
            objPaymentVoucher.PaymentVoucherDetailColl[0].Ledger_ID = 0;
        }
        for (int i = 0; i < NumberOfRows; i++)
        {
            if (objPaymentVoucher.PaymentVoucherDetailColl.Count == 0)
            {
                objPaymentVoucher.PaymentVoucherDetailColl.Add(PVDC);
            }
            else
            {
                objPaymentVoucher.PaymentVoucherDetailColl.Insert(0, PVDC);
            }
            Session.Add(TableConstants.SessionPaymentVoucherDetail, objPaymentVoucher);
        }
        BindData();
    }
    #endregion
    public void BindToDropdown()
    {
        CommonFunctions.BindDropDown(intRequestingUserID, Components.PaymentType, ddlPaymentType, "PaymentTypeName", "ID", "", false);
        CommonFunctions.BindDropDown(intRequestingUserID, Components.CompanyBank, ddlCompanyAccountNo, "AccountNo", "ID", "", false, LoginCompanyID);
        ddlCompanyAccountNo.SelectedValue = new AppConvert(DefaultCompanyBank.ID);
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCompanyBankBranch, "BankBranch", "ID", "", true, LoginCompanyID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(DefaultCompanyBank.BankType_ID);
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCompanyBank, "Name", "ID", "", true, LoginCompanyID);
        ddlCompanyBank.SelectedValue = new AppConvert(DefaultCompanyBank.BankType_ID);
        SetBankDetails();
    }
    public void SetPaymentFor()
    {
        int PaymentFor = new AppConvert(ddlPaymentFor.SelectedValue);
        switch (PaymentFor)
        {
            case 1:// Payment For Vendor
                divVendorName.Visible = true;
                grdPaymentVoucherDetails.Columns[3].Visible = true;
                grdPaymentVoucherDetails.Columns[4].Visible = true;
                break;
            case 2://Payment For TDS
                divVendorName.Visible = false;
                grdPaymentVoucherDetails.Columns[3].Visible = false;
                grdPaymentVoucherDetails.Columns[4].Visible = false;
                break;
            case 3: //Payment For Refund
                divVendorName.Visible = true;
                grdPaymentVoucherDetails.Columns[3].Visible = true;
                grdPaymentVoucherDetails.Columns[4].Visible = true;
                break;
            case 4: //Payment For Other
                divVendorName.Visible = false;
                grdPaymentVoucherDetails.Columns[3].Visible = true;
                grdPaymentVoucherDetails.Columns[4].Visible = true;
                break;

            default: //Payment For Other
                divVendorName.Visible = true;
                grdPaymentVoucherDetails.Columns[3].Visible = true;
                grdPaymentVoucherDetails.Columns[4].Visible = true;
                break;
        }
    }
    public void SetBankDetails()
    {
        int PaymentType = new AppConvert(ddlPaymentType.SelectedValue);
        switch (PaymentType)
        {
            case 1:
                divBankDetails.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            case 2:
                divBankDetails.Visible = true;
                divChequeNo.Visible = true;
                divChequeDate.Visible = true;
                break;
            case 3:
                divBankDetails.Visible = true;
                divChequeNo.Visible = true;
                divChequeDate.Visible = true;
                break;
            case 4:
                divBankDetails.Visible = true;
                divChequeNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            case 5:
                divBankDetails.Visible = true;
                divChequeNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            default:
                divBankDetails.Visible = false;
                divChequeNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
        }
        SetPaymentFor();
    }
    
    public void PopulateData()
    {
        AppPaymentVoucher objPaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        txtPaymentDate.Text = new AppConvert(objPaymentVoucher.PaymentVoucherDate);
        ddlPaymentType.SelectedValue = new AppConvert(objPaymentVoucher.PaymentType_ID);
        ddlCompanyAccountNo.SelectedValue = new AppConvert(objPaymentVoucher.From_CompanyBank_ID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objPaymentVoucher.From_CompanyBank.BankType.Name);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objPaymentVoucher.From_CompanyBank.BankType.BankBranch);
        if (objPaymentVoucher.PaymentType_ID > 1)
        {
            BindVendorDetails(objPaymentVoucher.Vendor_Company_ID);
            txtChequeNo.Text = new AppConvert(objPaymentVoucher.ChequeNo);
            txtChequeDate.Text = new AppConvert(objPaymentVoucher.ChequeDate);
            divChequeNo.Visible = true;
            divChequeDate.Visible = true;
            ddlVendorAccountNo.SelectedValue = new AppConvert(objPaymentVoucher.To_CompanyBank_ID);
            ddlVendorBankBranch.SelectedValue = new AppConvert(objPaymentVoucher.To_CompanyBank.BankType.BankBranch);
            ddlVendorBank.SelectedValue = new AppConvert(objPaymentVoucher.To_CompanyBank.BankType.Name);
            divBankDetails.Visible = true;
        }
        else
        {
            divBankDetails.Visible = false;
        }
        txtVendor.Text = new AppConvert(objPaymentVoucher.Vendor_Company.Name);
        hdfVendorName.Value = new AppConvert(objPaymentVoucher.Vendor_Company.Name);
        hdfVendorID.Value = new AppConvert(objPaymentVoucher.Vendor_Company_ID);
        txtNetPayable.Text = new AppConvert(objPaymentVoucher.Amount);
        txtPayee.Text = new AppConvert(objPaymentVoucher.Vendor_Company.Name);
        txtLedger.Text = new AppConvert(objPaymentVoucher.Ledger.LedgerName);
        hdfLedgerName.Value = new AppConvert(objPaymentVoucher.Ledger.LedgerName);
        hdfLedgerID.Value = new AppConvert(objPaymentVoucher.Ledger_ID);
        txtVoucherNo.Text = new AppConvert(objPaymentVoucher.PaymentVoucherNo);
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(objPaymentVoucher.CurrencyExchID);
        ddlPaymentFor.SelectedValue = new AppConvert(objPaymentVoucher.PaymentFor);
        ddlPaymentFor.Enabled = false;
        ddlPaymentType.Enabled = false;
        txtVendor.Enabled = false;
        //hdfHeaderExchangeCurrencyName.Value = new AppConvert(objPaymentVoucher.cu);
        txtHeaderExchangeCurrency.Text = "";

        txtCurrency.Text = new AppConvert(objPaymentVoucher.CurrencyType.Name);
        hdfCurrencyID.Value = new AppConvert(objPaymentVoucher.CurrencyType_ID);
        hdfCurrencyName.Value = new AppConvert(objPaymentVoucher.CurrencyType.Name);


        objPaymentVoucher.GetAllPaymentOrderDetails();
        grdPaymentVoucherDetails.DataSource = objPaymentVoucher.PaymentVoucherDetailColl;
        grdPaymentVoucherDetails.DataBind();
        Session[TableConstants.SessionPaymentVoucherDetail] = objPaymentVoucher;
        ACEtxtLedger.ContextKey = Convert.ToString(((ddlPaymentType.SelectedValue == "1") ? "4" : "3") + (",~" + "~" + new AppUtility.AppConvert((int)AppObjects.LedgerFlag.GeneralLedger)));
    }
    #endregion
    #region Dropdown Events
    protected void ddlPaymentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetBankDetails();
        int Payment_Type = new AppConvert(ddlPaymentType.SelectedValue);
        if (Payment_Type > 1)
        {
            AppCompanyBank ObjCompanyBank = new AppCompanyBank(intRequestingUserID, Payment_Type);
            txtLedger.Text = ObjCompanyBank.Ledger.LedgerName;
            hdfLedgerName.Value = ObjCompanyBank.Ledger.LedgerName;
            hdfLedgerID.Value = new AppConvert(ObjCompanyBank.Ledger_ID);
            ACEtxtLedger.ContextKey = Convert.ToString(((ddlPaymentType.SelectedValue == "1") ? "4" : "3") + (",~" + "~" + new AppUtility.AppConvert((int)AppObjects.LedgerFlag.GeneralLedger)));
        }
        else
        {
            txtLedger.Text = "";
            hdfLedgerName.Value = "";
            hdfLedgerID.Value = "";
        }
    }
    protected void ddlCompanyAccountNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Company_Bank_Id = new AppConvert(ddlCompanyAccountNo.SelectedValue);
        AppCompanyBank objCOmpanyBank = new AppCompanyBank(intRequestingUserID, Company_Bank_Id);
        ddlCompanyBank.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
    }
    protected void ddlVendorAccountNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Company_Bank_Id = new AppConvert(ddlVendorAccountNo.SelectedValue);
        AppCompanyBank objCOmpanyBank = new AppCompanyBank(intRequestingUserID, Company_Bank_Id);
        ddlCompanyBank.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
    }
    protected void ddlPaymentFor_SelectedIndexChanged(object sender, EventArgs e)
    {
        int VendorCompanyID = new AppConvert(hdfVendorID.Value);
        SetPaymentFor();
        objPaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        if (ddlPaymentFor.SelectedValue == "1" || ddlPaymentFor.SelectedValue == "3")
        {
            objPaymentVoucher.Vendor_Company_ID = VendorCompanyID;
        }
        else
        {
            txtVendor.Text = "";
            hdfVendorName.Value = "";
            hdfVendorID.Value = "0";
            objPaymentVoucher.Vendor_Company_ID = 0;

        }
        BindData();
    }
    #endregion
    #region Button Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppPaymentVoucher PV = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        string Message = "Record successfully Saved";
        if (PV.ID == 0)
        {
            PV.PaymentVoucherNo = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "PV", System.DateTime.Now, "PV-",4, ResetCycle.Yearly));
        }
        else
        {
            Message = "Record successfully Updated";
            PV.IsRecordModified = true;
        }
        tabMain.ActiveTabIndex = 2;
        int Payment_Type = new AppConvert(ddlPaymentType.SelectedValue);
        if ((Payment_Type == 2 || Payment_Type == 3))
        {
            if (string.IsNullOrEmpty(txtChequeNo.Text) == true)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter cheque no.','2'" + ");", true);
                return;
            }
            if (string.IsNullOrEmpty(txtChequeDate.Text) == true)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Cheque Date.','2'" + ");", true);
                return;
            }
        }

        PV.Company_ID = new AppConvert(LoginCompanyID);
        PV.VoucherType_ID = 9;
        PV.Vendor_Company_ID = new AppConvert(hdfVendorID.Value);
        PV.PaymentVoucherDate = new AppConvert(txtPaymentDate.Text);
        PV.PaymentType_ID = new AppConvert(ddlPaymentType.SelectedValue);
        PV.Amount = new AppConvert(txtNetPayable.Text);
        PV.Description = txtDescription.Text;
        PV.Vendor_Company_ID = new AppConvert(hdfVendorID.Value);
        PV.Ledger_ID = new AppConvert(hdfLedgerID.Value);// PV.Vendor_Company.Vendor_Ledger_ID; 
        PV.ChequeNo = new AppConvert(txtChequeNo.Text);
        PV.ChequeDate = new AppConvert(txtChequeDate.Text);
        PV.CurrencyType_ID = new AppConvert(hdfCurrencyID.Value);
        PV.CurrencyExchID = new AppConvert(hdfHeaderExchangeCurrencyID.Value);
        PV.ExchangeRate = new AppConvert(txtHeaderExchangeCurrencyRate.Text);
        PV.From_CompanyBank_ID = new AppConvert(ddlCompanyAccountNo.SelectedValue);
        PV.To_CompanyBank_ID = new AppConvert(ddlVendorAccountNo.SelectedValue);
        PV.PaymentFor = new AppConvert(ddlPaymentFor.SelectedValue);
        PV.Advance = 0;
        PV.ModifiedBy = intRequestingUserID;
        PV.ModifiedOn = System.DateTime.Now;
        PV.Status = new AppConvert((int)RecordStatus.Created);
        PV.Type = 0;
        PV.Save();
      
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + Message + "','1'" + ");", true);


        #region Add JournalEntries
        if (ddlPaymentFor.SelectedValue == "3")
        {
            decimal TdsAmount = PV.PaymentVoucherDetailColl.Sum(x => x.TDSAmount);
            LedgerEntry(LoginCompanyID, 100301, 1003, PV.Ledger_ID, PV.ID, PV.PaymentVoucherDate, 0, PV.Amount - TdsAmount, PV.PaymentVoucherNo, PV.ModifiedBy, PV.ModifiedOn, 0,PV.Status, PV.Type,
                                 0, 0, PV.VoucherType_ID, PV.Description, "", PV.PaymentType_ID);
            if (PV.PaymentVoucherDetailColl.Count > 0)
            {
                foreach (var item in PV.PaymentVoucherDetailColl)
                {
                    if (item.Amount != 0)
                    {
                        LedgerEntry(LoginCompanyID, 100401, 1004, item.Ledger_ID, item.ID, PV.PaymentVoucherDate, item.Amount, 0, PV.PaymentVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, PV.Status, item.Type,
                                     item.ProfitCenter_ID, item.CostCenter_ID, PV.VoucherType_ID, item.PaymentVoucherDesc, "", PV.PaymentType_ID);
                        LedgerEntry(LoginCompanyID, 100402, 1004, item.TDS_Ledger_ID, item.ID, PV.PaymentVoucherDate, 0, item.TDSAmount, PV.PaymentVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, PV.Status, item.Type,
                                     item.ProfitCenter_ID, item.CostCenter_ID, PV.VoucherType_ID, item.PaymentVoucherDesc, "", PV.PaymentType_ID);
                    }
                }

            }

        }
        else
        {
            decimal TdsAmount = PV.PaymentVoucherDetailColl.Sum(x => x.TDSAmount);
            LedgerEntry(LoginCompanyID, 100301, 1003, PV.Ledger_ID, PV.ID, PV.PaymentVoucherDate, PV.Amount - TdsAmount, 0, PV.PaymentVoucherNo, PV.ModifiedBy, PV.ModifiedOn,0, PV.Status, PV.Type,
                                  0, 0, PV.VoucherType_ID, PV.Description, "", PV.PaymentType_ID);
            if (PV.PaymentVoucherDetailColl.Count > 0)
            {
                foreach (var item in PV.PaymentVoucherDetailColl)
                {
                    if (item.Amount != 0)
                    {
                        LedgerEntry(LoginCompanyID, 100401, 1004, item.Ledger_ID, item.ID, PV.PaymentVoucherDate, 0, item.Amount, PV.PaymentVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, PV.Status, item.Type,
                                     item.ProfitCenter_ID, item.CostCenter_ID, PV.VoucherType_ID, item.PaymentVoucherDesc, "", PV.PaymentType_ID);
                        LedgerEntry(LoginCompanyID, 100402, 1004, item.TDS_Ledger_ID, item.ID, PV.PaymentVoucherDate, item.TDSAmount, 0, PV.PaymentVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, PV.Status, item.Type,
                                     item.ProfitCenter_ID, item.CostCenter_ID, PV.VoucherType_ID, item.PaymentVoucherDesc, "", PV.PaymentType_ID);
                    }
                }

            }
        }
        #endregion

        Session.Remove(TableConstants.SessionPaymentVoucherDetail);
        Response.Redirect("T_PaymentVoucherSummary.aspx?Msg=1&PaymentVoucherID=" + PV.ID);

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.SessionPaymentVoucherDetail);
        Response.Redirect("T_PaymentVoucherSummary.aspx");
    }
    protected void btnAddLineItem_Click(object sender, EventArgs e)
    {
        AddEmptyRow(1);
        if (grdPaymentVoucherDetails.Rows.Count >= 0)
        {
            TextBox txtLedger = (TextBox)grdPaymentVoucherDetails.Rows[0].FindControl("txtLedger");
            AutoCompleteExtender ACEtxtLedger = (AutoCompleteExtender)grdPaymentVoucherDetails.Rows[0].FindControl("ACEtxtLedger");
            ACEtxtLedger.ContextKey = hdfVendorID.Value;
            txtLedger.Focus();
        }
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {

    }
    protected void btnApprove_Click1(object sender, EventArgs e)
    {
        AppPaymentVoucher PV = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
        PV.Status = new AppUtility.AppConvert((int)RecordStatus.Approve);
        PV.Save();
        Response.Redirect("T_PaymentVoucherDetails.aspx?Msg=2");
    }
    #endregion
    #region GridView Events
    protected void grdPaymentVoucherDetails_PreRender(object sender, EventArgs e)
    {
        if (grdPaymentVoucherDetails.Rows.Count > 0)
        {
            grdPaymentVoucherDetails.UseAccessibleHeader = true;
            grdPaymentVoucherDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdPaymentVoucherDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
            AppPaymentVoucher PaymentVoucher = (AppPaymentVoucher)Session[TableConstants.SessionPaymentVoucherDetail];
            PaymentVoucher.PaymentVoucherDetailColl.Where(x => x.Status == 0).ToList()[row.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
            Session.Add(TableConstants.SessionPaymentVoucherDetail, PaymentVoucher);
            BindData();

        }
    }
    #endregion
    #region CheckBox Events
    public static bool CheckItemSelectedinGrid(GridView Grid, string CheckBoxName)
    {
        if (Grid.Rows.Count == 0)
        {
            return false;
        }
        for (int i = 0; i < Grid.Rows.Count; i++)
        {
            CheckBox chkSelect = (CheckBox)Grid.Rows[i].FindControl(CheckBoxName);
            if (chkSelect.Checked == true)
            {
                return true;
            }
        }
        return false;
    }

    #endregion
}