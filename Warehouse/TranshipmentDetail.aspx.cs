﻿using AppObjects;
using AppUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TranshipmentDetail : BigSunPage
{

    #region Variables
    AppTranshipment objAppTranshipment;
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        DecideLanguage(Convert.ToString(Session["LanguageCode"]));
        Page.Title = "Transhipment Detail";
        objAppTranshipment = new AppTranshipment(intRequestingUserID);
        if (!IsPostBack)
        {
            Session.Add(TableConstants.TranshipmentSession,objAppTranshipment);
            string ID = Convert.ToString(Request.QueryString["TranshipmentID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                hdfTranshipmentID.Value = ID;
                int TranshipmentID = new AppUtility.AppConvert(ID);
                PopulateData(TranshipmentID);
            }
            else
            {
                btnAddRow_Click(sender, e);
                ByDefaultValueOnPageLoad();
            }
        }
    }
    #endregion

    #region Function
    private void DecideLanguage(string languageCode)
    {
        try
        {
            btnSave.Text = "Save";
            btnNewDetail.Text = "New";
        }
        catch (Exception)
        {

        }
    }
    public void PopulateData(int ID)
    {
        objAppTranshipment = new AppObjects.AppTranshipment(intRequestingUserID, ID);

        lblTranshipmentNo.Text = new AppConvert(objAppTranshipment.TranshipmentNo);

        txtGetRegFr.Text = new AppConvert(objAppTranshipment.From_GateEntry.GateNo);
        txtGetRegTo.Text = new AppConvert(objAppTranshipment.To_GateEntry.GateNo);


        UsrhdfPartner.Value = new AppConvert(objAppTranshipment.Customer_Company_ID);
        UsrlblPartner1.Text = objAppTranshipment.Customer_Company.Code + "--" + objAppTranshipment.Customer_Company.Name;
        UsrtxtPartnerCode.Text = objAppTranshipment.Customer_Company.Code + "--" + objAppTranshipment.Customer_Company.Name;

        UsrtxtPartnerCode_AutoCompleteExtender.ContextKey = new AppConvert("3," + objAppTranshipment.Customer_Company.Parent_ID);
        UsrhdfPartnerGroup.Value = new AppConvert(objAppTranshipment.Customer_Company.Parent_ID);
        UsrtxtPartnerGroupCode.Text = objAppTranshipment.Customer_Company.Parent.Code + "--" + objAppTranshipment.Customer_Company.Parent.Name;

        txtReceivedBy.Text = new AppConvert(objAppTranshipment.ReceivedBy_CompanyContact.FirstName);
        hdfReceivedByName.Value = txtReceivedBy.Text;
        hdfReceivedByID.Value = new AppConvert(objAppTranshipment.ReceivedBy_CompanyContact.ID);

        txtTranshipmentDate.Text = new AppConvert(objAppTranshipment.TranshipmentDate);
        txtReceivedDate.Text = new AppConvert(objAppTranshipment.ReceivedDate);
        txtTranshipmentStartedOn.Text = new AppConvert(objAppTranshipment.UnloadingStartedOn);
        txtStartedOnHH.Text = new AppConvert(objAppTranshipment.UnloadingStartedOn.Hour);
        txtStartedOnMM.Text = new AppConvert(objAppTranshipment.UnloadingStartedOn.Minute);
        txtTranshipmentEndedOn.Text = new AppConvert(objAppTranshipment.UnloadingEndedOn);
        txtEndedOnHR.Text = new AppConvert(objAppTranshipment.UnloadingEndedOn.Hour);
        txtEndedOnMM.Text = new AppConvert(objAppTranshipment.UnloadingEndedOn.Minute);
        txtLStartedOn.Text = new AppConvert(objAppTranshipment.LoadingStartedOn);
        txtLStartedOnHH.Text = new AppConvert(objAppTranshipment.LoadingStartedOn.Hour);
        txtLStartedOnMM.Text = new AppConvert(objAppTranshipment.LoadingStartedOn.Minute);
        txtLEndedOn.Text = new AppConvert(objAppTranshipment.LoadingEndedOn);
        txtLEndedOnHR.Text = new AppConvert(objAppTranshipment.LoadingEndedOn.Hour);
        txtLEndedOnMM.Text = new AppConvert(objAppTranshipment.LoadingEndedOn.Minute);
        txtRemarks.Text = objAppTranshipment.Remarks;

        grdItems.DataSource = objAppTranshipment.TranshipmentDetailColl.Where(x => x.Status == 1);
        grdItems.DataBind();

        btnDelete.Visible = true;

        Session.Add(TableConstants.TranshipmentSession, objAppTranshipment);
    }
    public void DeleteTDDetail(int TranshipmentDetailID)
    {
        AppTranshipmentDetail objAppTransferStockCustomerTo = new AppTranshipmentDetail(intRequestingUserID, TranshipmentDetailID);
        objAppTransferStockCustomerTo.Delete();
    }

    private void ByDefaultValueOnPageLoad()
    {
        txtTranshipmentDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtTranshipmentStartedOn.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtTranshipmentEndedOn.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtTranshipmentDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtReceivedDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtLEndedOn.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtLStartedOn.Text = DateTime.Now.ToString("dd/MM/yyyy");


        txtLStartedOnHH.Text = DateTime.Now.ToString("hh");
        txtLStartedOnMM.Text = DateTime.Now.ToString("mm");
        txtLEndedOnHR.Text = DateTime.Now.ToString("hh");
        txtLEndedOnMM.Text = DateTime.Now.ToString("mm");

        txtStartedOnHH.Text = DateTime.Now.ToString("hh");
        txtStartedOnMM.Text = DateTime.Now.ToString("mm");
        txtEndedOnHR.Text = DateTime.Now.ToString("hh");
        txtEndedOnMM.Text = DateTime.Now.ToString("mm");
    }
    #endregion

    #region Grid
    protected void grdItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void grdItems_PreRender(object sender, EventArgs e)
    {

    }
    #endregion

    #region Button Event
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (hdfTranshipmentID.Value != "" && hdfTranshipmentID.Value != null)
        {
            int TranshipmentID = Convert.ToInt32(hdfTranshipmentID.Value);
            AppTranshipment objAppTranshipment = new AppTranshipment(intRequestingUserID, TranshipmentID);
            objAppTranshipment.Delete();
        }
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("TranshipmentSearch.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppTranshipment objAppTranshipment = (AppTranshipment)Session[TableConstants.TranshipmentSession];

        #region Transhipment
        if (objAppTranshipment.ID == 0)
        {
            objAppTranshipment.TranshipmentNo = "TS" + new AppConvert(AppUtility.AutoID.GetNextNumber(1, "TS", System.DateTime.Now, 4, ResetCycle.Yearly));
        }
        objAppTranshipment.Company_ID = new AppConvert(UsrhdfPartner.Value);
        string[] GetRegFr = hdfGetRegFrID.Value.Split('~');
        objAppTranshipment.From_GateEntry_ID = new AppConvert(GetRegFr[0]);
        string[] GetRegTo = hdfGetRegToID.Value.Split('~');
        objAppTranshipment.To_GateEntry_ID = new AppConvert(GetRegTo[0]);
        objAppTranshipment.ReceivedBy_CompanyContact_ID = new AppConvert(hdfReceivedByID.Value);
        objAppTranshipment.TranshipmentDate = new AppConvert(txtTranshipmentDate.Text);
        objAppTranshipment.ReceivedDate = new AppConvert(txtReceivedDate.Text);
        objAppTranshipment.UnloadingStartedOn = new AppConvert(txtTranshipmentStartedOn.Text);
        objAppTranshipment.UnloadingEndedOn = new AppConvert(txtTranshipmentEndedOn.Text);
        objAppTranshipment.UnloadingStartedOn = new AppConvert(txtLStartedOn.Text);
        objAppTranshipment.UnloadingEndedOn = new AppConvert(txtLEndedOn.Text);
        objAppTranshipment.Status = new AppConvert((int)RecordStatus.Active);
        objAppTranshipment.Remarks = txtRemarks.Text;
        objAppTranshipment.Type = objAppTranshipment.Type;
        objAppTranshipment.ModifiedBy = intRequestingUserID;
        objAppTranshipment.ModifiedOn = System.DateTime.Now;
        #endregion

        #region Transhipment Item
        int i = 0;
        foreach (var AppTranshipmentDetail in objAppTranshipment.TranshipmentDetailColl)
        {
            AppTranshipmentDetail objAppTranshipmentDetail = new AppTranshipmentDetail(intRequestingUserID);

            HiddenField hdfStorageSchemeRate_ID = (HiddenField)grdItems.Rows[i].FindControl("hdfStorageSchemeRate_ID");
            HiddenField hdfStorageItemID = (HiddenField)grdItems.Rows[i].FindControl("hdfStorageItemID");
            TextBox ItemMark = (TextBox)grdItems.Rows[i].FindControl("ItemMark");
            TextBox txtPackingReference = (TextBox)grdItems.Rows[i].FindControl("txtPackingReference");
            TextBox Quantity = (TextBox)grdItems.Rows[i].FindControl("Quantity");
            TextBox lblLotNoInternal = (TextBox)grdItems.Rows[i].FindControl("lblLotNoInternal");
            TextBox lblLotNoCustomer = (TextBox)grdItems.Rows[i].FindControl("lblLotNoCustomer");

            AppTranshipmentDetail.Item_ID = Convert.ToInt32(hdfStorageItemID.Value);
            AppTranshipmentDetail.StorageSchemeRate_ID = Convert.ToInt32(hdfStorageSchemeRate_ID.Value);
            AppTranshipmentDetail.ItemMark = ItemMark.Text;
            AppTranshipmentDetail.PackingReference = txtPackingReference.Text;
            AppTranshipmentDetail.Quantity = Convert.ToInt32(Quantity.Text);
            AppTranshipmentDetail.LotInternal = Convert.ToInt32(lblLotNoInternal.Text);
            AppTranshipmentDetail.LotCustomer = lblLotNoCustomer.Text;
            AppTranshipmentDetail.Status = new AppConvert((int)RecordStatus.Active);
            i++;
        }

        objAppTranshipment.Save();
        grdItems.DataSource = objAppTranshipment.TranshipmentDetailColl;
        grdItems.DataBind();
        Session[TableConstants.TranshipmentSession] = objAppTranshipment;
        #endregion

        Session.Remove(TableConstants.TranshipmentSession);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Saved Successfully','1'" + ");", true);
    }
    protected void btnNewDetail_Click(object sender, EventArgs e)
    {
        Response.Redirect("TranshipmentDetail.aspx");
    }
    #endregion

    #region Item
    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        objAppTranshipment = (AppTranshipment)Session[TableConstants.TranshipmentSession];
        AppTranshipmentDetail objAppTranshipmentDetail = new AppTranshipmentDetail(intRequestingUserID);
        objAppTranshipment.TranshipmentDetailColl.Add(objAppTranshipmentDetail);
        grdItems.DataSource = objAppTranshipment.TranshipmentDetailColl;
        grdItems.DataBind();
        Session[TableConstants.TranshipmentSession] = objAppTranshipment;
        //AddEmptyRow(1);
    }
    protected void grdItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Del")
        {
            try
            {
                int selectedindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
                HiddenField hdfTranshipmentID = (HiddenField)grdItems.Rows[selectedindex].FindControl("hdfTranshipmentID");
                if (hdfTranshipmentID.Value != "0" && hdfTranshipmentID.Value != "")
                {
                    int Transhipment_ID = Convert.ToInt32(hdfTranshipmentID.Value);

                    #region Outward Validation
                    int MaterialOrder_ID = Convert.ToInt32("0" + hdfTranshipmentDetailID.Value);
                    AppOutwardDetailColl objAppOutwardDetailColl = new AppOutwardDetailColl(intRequestingUserID);
                    objAppOutwardDetailColl.AddCriteria(OutwardDetail.MaterialOrderDetail_ID, Operators.Equals, Transhipment_ID);
                    objAppOutwardDetailColl.AddCriteria(OutwardDetail.Status, Operators.Equals, (int)RecordStatus.Active);
                    objAppOutwardDetailColl.Search();

                    if (objAppOutwardDetailColl.Count() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Outward has been made. Please delete the outward first" + "','2'" + ");", true);
                        return;
                    }
                    #endregion

                    DeleteTDDetail(Transhipment_ID);
                    //InsertTransactionLog(21, MODID, 4, intRequestingUserID);
                }

                #region SMS Log 
                if (!string.IsNullOrEmpty(hdfTranshipmentDetailID.Value))
                {
                    //int moid = Convert.ToInt32(hdfTranshipmentDetailID.Value);
                    //var MoSMS = db.MaterialOrders.FirstOrDefault(a => a.MaterialOrderID == moid);
                    ////if (!db.SMSTransLogs.Any(a => a.UnitID == MoSMS.UnitID && a.TransRefID == MoSMS.MaterialOrderID && a.SMSTemplateID == 5))
                    ////{
                    //string strOB = "";
                    //if (MoSMS.OrderedBy != null)
                    //{
                    //    strOB = db.ParterContacts.FirstOrDefault(a => a.PartnerContID == MoSMS.OrderedBy).FirstName + " " + db.ParterContacts.FirstOrDefault(a => a.PartnerContID == MoSMS.OrderedBy).LastName;
                    //}
                    //else
                    //{
                    //    strOB = MoSMS.OrderName;
                    //}

                    //InsertSMSLog(6, MoSMS.MaterialOrderID, MoSMS.UnitID, MoSMS.PartnerID, TMDqty, "", MoSMS.MaterialOrderNo, MoSMS.DeliverTo, strOB); //Convert.ToInt32("0" + iwd.Totalqty)
                    // }
                }
                #endregion

                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Deleted Sucessfully" + "','2'" + ");", true);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Foreign"))
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "This Detail Is Used For Outward" + "','2'" + ");", true);
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + ex.Message + "','2'" + ");", true);
            }

            //gvDescription.DataSource = bindsearch(dt);
            //gvDescription.DataBind();

        }
    }
    #endregion

}