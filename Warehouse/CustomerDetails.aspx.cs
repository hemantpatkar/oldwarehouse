﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to add details of Customers
*/
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
#endregion
public partial class CustomerDetails : BigSunPage
{
    AppObjects.AppCustomer obj = null;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = new AppObjects.AppCustomer(this.intRequestingUserID);
        if (!IsPostBack)
        {
            Session.Add(TableConstants.CustomerSessionObj, obj);
            CommonFunctions.BindDropDown(intRequestingUserID, Components.OrganizationType, ddlOrganizationType, "Name", "ID", "", false);
            CommonFunctions.BindDropDown(intRequestingUserID, Components.CurrencyType, ddlCurrency, "Name", "ID", "", false);
            CommonFunctions.BindDropDown(intRequestingUserID, Components.PriceGroup, ddlPriceGroup, "Name", "ID", "", true);
            string ID = Convert.ToString(Request.QueryString["CustomerID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int CustomerID = new AppUtility.AppConvert(ID);
                AppObjects.AppCustomer objCustomer = new AppObjects.AppCustomer(intRequestingUserID, CustomerID);
                txtCustomerCode.Text = objCustomer.Code;
                txtCustomerName.Text = objCustomer.Name;
                txtDescription.Text = objCustomer.Description;
                ddlCurrency.SelectedValue = new AppConvert(objCustomer.CurrencyType_ID);
                ddlOrganizationType.SelectedValue = new AppUtility.AppConvert(objCustomer.OrganizationType_ID);
                hdfTermsAndConditionsID.Value = new AppConvert(objCustomer.TermsAndConditions_ID);
                hdfTermsAndConditionsName.Value = new AppConvert(objCustomer.TermsAndConditions.Statement);
                txtTermsAndConditions.Text = new AppConvert(objCustomer.TermsAndConditions.Statement);
                ddlPriceGroup.SelectedValue = new AppConvert(objCustomer.GetCustomerPriceGroup);
                Session.Add(TableConstants.CustomerSessionObj, objCustomer);
                btnApprove.Visible = true;
                hdfCustomerLedgerID.Value = new AppConvert(objCustomer.Customer_Ledger_ID);
                txtCustomerLedger.Text = objCustomer.Customer_Ledger.LedgerName;
                hdfCustomerLedgerName.Value = txtCustomerLedger.Text;
                chkIsNewCustomerLedger.Visible = false;
                chkIsNewCustomerLedger.Checked = false;



                LedgerType(CustomerID);
            }
            else
            {
                chkIsNewCustomerLedger.Visible = true;
                chkIsNewCustomerLedger.Checked = true;
                LedgerType(0);
                btnApprove.Visible = false;
            }
        }
    }
    #endregion    
    #region btnSave_Click
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string Message = "Customer Updated Successfully";
        AppObjects.AppCustomer objCustomer = (AppObjects.AppCustomer)Session[TableConstants.CustomerSessionObj];
        if (objCustomer == null)
        {
            objCustomer = new AppObjects.AppCustomer(this.intRequestingUserID);
            Message = "Customer Added Successfully";
        }
        objCustomer.Code = txtCustomerCode.Text;
        objCustomer.Name = txtCustomerName.Text;
        objCustomer.Description = txtDescription.Text;
        objCustomer.OrganizationType_ID = new AppConvert(ddlOrganizationType.SelectedValue);
        objCustomer.CurrencyType_ID = new AppConvert(ddlCurrency.SelectedValue);
        objCustomer.Status = (chkActive.Checked == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.Created));
        objCustomer.Type = new AppConvert((int)CompanyRoleType.Customer);
        objCustomer.ModifiedBy = this.intRequestingUserID;
        objCustomer.ModifiedOn = System.DateTime.Now;
        objCustomer.TermsAndConditions_ID = new AppConvert(hdfTermsAndConditionsID.Value);
        int PriceGroupValue = new AppConvert(ddlPriceGroup.SelectedValue);

        AppCustomerPriceGroup objCustomerPriceGroup = new AppCustomerPriceGroup(intRequestingUserID);
        objCustomerPriceGroup.Company_ID = objCustomer.ID;
        objCustomerPriceGroup.PriceGroup_ID = new AppConvert(ddlPriceGroup.SelectedValue);
        objCustomerPriceGroup.Status = (chkActive.Checked == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.Created));
        objCustomerPriceGroup.ModifiedBy = intRequestingUserID;
        objCustomerPriceGroup.ModifiedOn = System.DateTime.Now;
        objCustomer.AddNewCustomerPriceGroup(objCustomerPriceGroup);
        if (chkIsNewCustomerLedger.Checked)
        {
            objCustomer.Customer_Ledger_ID = CreateLedger(new AppConvert(hdfCustomerLedgerID.Value));
        }
        else
        {
            objCustomer.Customer_Ledger_ID = (new AppConvert(hdfCustomerLedgerID.Value));
        }

        objCustomer.Save();
        Session.Remove(TableConstants.CustomerSessionObj);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + Message + "','1'" + ");", true);
        AppCustomer objNewCustomer = new AppCustomer(intRequestingUserID);
        Session.Add(TableConstants.CustomerSessionObj, objNewCustomer);
    }
    #endregion
    #region btnCancel_Click
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.CustomerSessionObj);
        Response.Redirect("CustomerSummary.aspx");
    }
    #endregion
    #region btnApprove_Click
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        obj = (AppObjects.AppCustomer)Session[TableConstants.CustomerSessionObj];
        obj.Status = new AppConvert((int)RecordStatus.Approve);
        obj.Save();
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Customer Approved','1'" + ");", true);
    }
    #endregion

    protected void ddlPriceGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        int SelectedValue = new AppConvert(ddlPriceGroup.SelectedValue);
        if (SelectedValue > 0)
        {
            tabMain.Tabs[4].Visible = false;
        }
        else
        {
            tabMain.Tabs[4].Visible = true;
        }
    }
    public int CreateLedger(int Parent_ID)
    {
        AppLedgerColl objLedgerColl = (AppLedgerColl)HttpContext.Current.Session[TableConstants.LedgerTreeSessionObj];
        AppLedger objLedger = new AppLedger(intRequestingUserID);
        objLedger.ID = 0;
        objLedger.LedgerName = txtCustomerName.Text;
        objLedger.AccountNo = "1000000002";
        objLedger.LedgerCode = txtCustomerCode.Text;
        objLedger.Parent_ID = new AppConvert(Parent_ID);
        objLedger.AccountType_ID = AppLedgerColl.GetAccountType(Parent_ID);
        objLedger.LedgerType_ID = 2;
        objLedger.Status = (chkActive.Checked == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        objLedger.LedgerFlag = new AppConvert((int)LedgerFlag.GeneralLedger);
        if (objLedgerColl != null)
        {
            objLedger.SortOrder = new AppConvert(objLedgerColl.FindLastIndex(o => o.Parent_ID == objLedger.Parent_ID));
        }
        objLedger.ModifiedBy = intRequestingUserID;
        objLedger.ModifiedOn = System.DateTime.Now;
        objLedger.Save();
        return objLedger.ID;
    }
    public void LedgerType(int ID = 0)
    {

        if (chkIsNewCustomerLedger.Checked)
        {

            lblCustomerLedger.Text = "Parent Ledger";
            ACEtxtCustomerLedger.ContextKey = "," + (new AppUtility.AppConvert((int)LedgerFlag.ControlAccount) + "~" + new AppUtility.AppConvert((int)LedgerFlag.LedgerGroup) + "~");
        }
        else
        {
            lblCustomerLedger.Text = "Customer Ledger";
            ACEtxtCustomerLedger.ContextKey = "2," + ("~" + "~" + new AppUtility.AppConvert((int)LedgerFlag.GeneralLedger));
        }
    }

    protected void chkIsNewCustomerLedger_CheckedChanged(object sender, EventArgs e)
    {
        LedgerType();
    }
}