﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;

public partial class StockOutDetail : BigSunPage
{

    #region Variables

    int total = 0;
    private int totalflag = 0;
    static int focus;
    static int rowsid;
    private string scheme = "";
    private string outwardno = "";
    static int outid;
    public int flag = 0;
    static int temp = 0;
    AppOutward objAppOutward = null;

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        hdfOutward.Value = Request.QueryString["ID"];
        dateval();
        objAppOutward = new AppOutward(intRequestingUserID);
        if (!IsPostBack)
        {
            Session.Add(TableConstants.OutWardSession, objAppOutward);
            BydefaultclickEnter();
            DecideLanguage(new AppConvert(Session["LanguageCode"]));
            ViewState["PostBackID"] = Guid.NewGuid().ToString();

            if (hdfOutward.Value != "" && hdfOutward.Value != "0")
            {
                btnPrint.Focus();
                history();
                PopulateOutward(hdfOutward.Value);
            }
            else
            {
                hdfrevised.Value = "";
                hdfMorev.Value = "";
                ByDefaultValueOnPageLoad();
                UsrtxtMONO.Focus();

                #region Showing Last Outward Number No
                AppOutwardColl objAppOutwardColl = new AppOutwardColl(intRequestingUserID);
                objAppOutwardColl.Search();
                if (objAppOutwardColl.Count() > 0)
                {
                    lblChNo.Text = objAppOutwardColl.LastOrDefault().OutwardNo;
                }
                #endregion
            }
        }
    }
    #endregion

    #region Function

    #region Refresh

    protected bool ValidateOutward()
    {
        int ID = Convert.ToInt32("0" + hdfOutward.Value);
        string strrev = "";
        if (ID != 0)
        {
            AppOutward objAppOutward = new AppOutward(intRequestingUserID, ID);
            strrev = new AppConvert(objAppOutward.ModifiedOn);
        }
        if (hdfrevised.Value == strrev)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion

    #region Refreshing Material order


    protected bool ValidateMo()
    {
        int ID = Convert.ToInt32("0" + hdfMono.Value);
        string strrevmo = "";
        if (ID != 0)
        {
            AppMaterialOrder objAppMaterialOrder = new AppMaterialOrder(intRequestingUserID, ID);
            strrevmo = new AppConvert(objAppMaterialOrder.ModifiedOn);
        }
        if (hdfMorev.Value == strrevmo)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion

    private bool validatePostback()
    {
        // verify duplicate postback
        string postbackid = ViewState["PostBackID"] as string;

        bool isValid = Cache[postbackid] == null;
        if (isValid)
            Cache.Insert(postbackid, true, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(10));
        return isValid;
    }

    #region date validation

    public void dateval()
    {
        //mFinancialYear fy = new mFinancialYear();
        //Int32 fyid = Convert.ToInt32(Session["FYearID"]);
        //fy = db.mFinancialYears.FirstOrDefault(a => a.FYearID == fyid);
        Rangeval.MinimumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangeval.MaximumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangback.MinimumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangback.MaximumValue = DateTime.Now.ToString("dd/MM/yyyy");
    }

    #endregion



    private void ByDefaultValueOnPageLoad()
    {
        txtOutwardDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtOutwardStartedOn.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtOutwardEndedOn.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtActualOutwardDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtStartedOnHH.Text = DateTime.Now.ToString("hh");
        txtStartedOnMM.Text = DateTime.Now.ToString("mm");
        txtEndedOnHR.Text = DateTime.Now.ToString("hh");
        txtEndedOnMM.Text = DateTime.Now.ToString("mm");
    }

    public void BydefaultclickEnter() // Method used to hit save buton on ENTER key press
    {
        UsrtxtMONO.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        UsrtxtGate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtOutwardDate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtActualOutwardDate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtOutwardStartedOn.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtStartedOnHH.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtStartedOnMM.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtOutwardEndedOn.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtEndedOnHR.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtEndedOnMM.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtRemarks.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtDateCompare.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");

    }

    private void DecideLanguage(string languageCode)  // Method used to fill Screen Language on lables & Buttons text property
    {

        Page.Title = "Outward" + " " + "Detail";
        lblVehicalNo.Text = "Vehicle" + " " + "No";
        btnPrint.ToolTip = "View" + " " + "Report";
        btnDelete.Text = "Delete";
        btnApproved.Text = "Approve";
        lblOutward.Text = "Outward";
        btnSave.Text = "Save";
        btnNew.Text = "Create New";
        lblMaterialOrderID.Text = "Material" + " " + "Order" + " " + "No";
        lblPartnerGroupIDFrom.Text = "Customer Group";
        lblPartnerIDFrom.Text = "Customer";
        lblRemarks.Text = "Remarks";
        lblGateRegisterID.Text = "Gate" + " " + "Register" + " " + "No";
        lblOutwardDate.Text = "Outward Date";
        lblActualOutwardDate.Text = "Actual" + " " + "Date";
        lblDeliveredTo.Text = "Delivered" + " " + "To";
        lblDeliveredOn.Text = "Delivered" + " " + "On";
        lblOutwardStartedOn.Text = "Started" + " " + "On";
        lblStartedOnHH.Text = "HH";
        lblStartedOnMM.Text = "MM";
        lblOutwardEndedOn.Text = "Ended" + " " + "On";
        lblEndedOnHH.Text = "HH";
        lblEndedonMM.Text = "MM";
        btnAdd.Text = "Add";
        btncancelpop.Text = "Cancel";
        lblCnm.Text = "Last" + " " + "Outward" + " " + "No";
        btnDelete_ConfirmButtonExtender.ConfirmText = "DeleteMsg";
        lblcron.Text = "CreatedOn" + " : ";
        lbledon.Text = "EditedOn" + " : ";
        lbldelon.Text = "DeletedOn" + " : ";
        lblshowshotcut.Text = "Shortcuts";
        lbledit.Text = "Transaction" + " " + "History";
        colshotcut.CollapsedText = "Show" + " " + "Shortcuts";
        colshotcut.ExpandedText = "Hide" + " " + "Shortcuts";

        lblorderby.Text = "Order" + " " + "By";
        lblChallanNo.Text = "Challan" + " " + "No.";
    }



    public void PopulateOutward(string ID)
    {

        //if (db.AppSettings.Any(a => a.AppSettingID == 111))
        //{
        //    string str = db.AppSettings.FirstOrDefault(a => a.AppSettingID == 111).AppSettingValue;
        //    btnPrint.NavigateUrl = "../../Reports/ReportView.aspx?OutwardID=" + ID + "&RPName=" + str;

        //}
        //else
        //{
        //    btnPrint.NavigateUrl = "../../Reports/ReportView.aspx?OutwardID=" + ID + "&RPName=OutwardSubReport";
        //}

        btnPrintSalesBill.NavigateUrl = "../../Reports/ReportView.aspx?OutwardID=" + ID + "&RPName=" + "Print_Outward";
        UsrtxtMONO.Enabled = false;
        btnPrint.Visible = true;

        int OutwardID = Convert.ToInt32(ID);

        AppOutward objAppOutward = new AppOutward(intRequestingUserID, OutwardID);
        hdfrevised.Value = new AppConvert(objAppOutward.ModifiedOn);

        Int32 ReferenceType = objAppOutward.VoucherType_ID;
        Int32 active = objAppOutward.Status;
        Int32 MaterialOrderID = Convert.ToInt32(objAppOutward.MaterialOrder_ID);
        hdfMono.Value = new AppConvert(objAppOutward.MaterialOrder_ID);

        if (hdfMono.Value != "" && hdfMono.Value != null)
        {
            UsrtxtMONO.Text = objAppOutward.MaterialOrder.MaterialOrderNo;
            Usrhidetxt.Text = UsrtxtMONO.Text;
            UsrtxtMONO.ToolTip = UsrtxtMONO.Text;
            hdfMorev.Value = new AppConvert(objAppOutward.MaterialOrder.ModifiedOn);
            lblorderbyName.Text = objAppOutward.MaterialOrder.OrderBy;

        }
        txtDateCompare.Text = new AppConvert(objAppOutward.MaterialOrder.MaterialOrderDate);
        lblPartnerGroupIDFromID.Text = new AppConvert(objAppOutward.Customer_Company.Parent_ID);
        lblPartnerGroupIDFromvalue.Text = objAppOutward.Customer_Company.Parent.Name;
        lblPartnerGroupIDFromvalue.ToolTip = lblPartnerGroupIDFromvalue.Text;

        lblPartnerIDFromValue.Text = new AppConvert(objAppOutward.Customer_Company.Name);
        lblPartnerIDFromValue.ToolTip = lblPartnerIDFromValue.Text;
        lblPartnerIDFromID.Text = new AppConvert(objAppOutward.Customer_Company_ID);
        lblOutwardNoValue.Text = objAppOutward.OutwardNo;
        if (objAppOutward.GateEntry_ID != 0)
        {
            UsrhdfGate.Value = new AppConvert(objAppOutward.GateEntry_ID);
            int id = Convert.ToInt32(objAppOutward.GateEntry_ID);
            UsrtxtGate.Text = objAppOutward.GateEntry.GateNo;
            HideGate.Text = UsrtxtGate.Text;
            txtVehicalNo.Text = objAppOutward.GateEntry.VehicalNo;

        }
        txtChallanNo.Text = objAppOutward.GateEntry.ChallanNo;
        txtOutwardDate.Text = new AppConvert(objAppOutward.OutwardDate);
        txtOutwardDate.ToolTip = txtOutwardDate.Text;
        txtActualOutwardDate.Text = new AppConvert(objAppOutward.ActualOutwardDate);
        txtActualOutwardDate.ToolTip = txtActualOutwardDate.Text;
        lblDeliveredToValue.Text = objAppOutward.DeliveredTo;
        lblDeliveredToValue.ToolTip = lblDeliveredToValue.Text;
        lblDeliveredOnValue.Text = new AppConvert(objAppOutward.DeliverdOn);
        lblDeliveredOnValue.ToolTip = lblDeliveredOnValue.Text;

        if (objAppOutward.OutwardStartedOn != null || new AppConvert(objAppOutward.OutwardStartedOn) != "")
        {
            DateTime EntryOn = Convert.ToDateTime(objAppOutward.OutwardStartedOn);
            txtOutwardStartedOn.Text = new AppConvert(EntryOn.ToString("dd/MM/yyyy"));
            txtStartedOnHH.Text = new AppConvert(EntryOn.Hour);
            if (txtStartedOnHH.Text.Length == 1)
            {
                txtStartedOnHH.Text = "0" + txtStartedOnHH.Text;
            }
            txtStartedOnMM.Text = new AppConvert(EntryOn.Minute);
            if (txtStartedOnMM.Text.Length == 1)
            {
                txtStartedOnMM.Text = "0" + txtStartedOnMM.Text;
            }
        }
        else
        {
            mevtxtStartedOnHH.Enabled = false;
            mevtxtStartedOnMM.Enabled = false;
        }
        if (objAppOutward.OutwardEndedOn != null || new AppConvert(objAppOutward.OutwardEndedOn) != "")
        {
            DateTime EntryOn = Convert.ToDateTime(objAppOutward.OutwardEndedOn);
            txtOutwardEndedOn.Text = new AppConvert(EntryOn.ToString("dd/MM/yyyy"));

            txtEndedOnHR.Text = new AppConvert(EntryOn.Hour);
            if (txtEndedOnHR.Text.Length == 1)
            {
                txtEndedOnHR.Text = "0" + txtEndedOnHR.Text;
            }
            txtEndedOnMM.Text = new AppConvert(EntryOn.Minute);
            if (txtEndedOnMM.Text.Length == 1)
            {
                txtEndedOnMM.Text = "0" + txtEndedOnMM.Text;
            }
        }


        txtRemarks.Text = objAppOutward.Remarks;
        txtRemarks.ToolTip = txtRemarks.Text;
        ddlOutwardType.SelectedValue = new AppConvert(objAppOutward.Type);

        totalflag = 1;

        grdOutwardDetail.DataSource = objAppOutward.OutwardDetailColl;
        grdOutwardDetail.DataBind();
        Session.Add(TableConstants.OutWardSession, objAppOutward);
        if (objAppOutward.Status < 2)
        {
            btnApproved.Visible = true;
        }
        else if (objAppOutward.Status < 1)
        {

        }
        else if (objAppOutward.Status < 0)
        {
            btnSave.Visible = false;
        }
        else
        {
            btnSave.Visible = false;
        }

        //#endregion

    }

    #endregion

    #region TxtChanged

    protected void UsrtxtMONO_TextChanged(object sender, EventArgs e)
    {
        grdOutwardDetail.DataSource = null;
        grdOutwardDetail.DataBind();
        ViewState["grdOutward"] = null;
        if (hdfMono.Value != "" && hdfMono.Value != null)
        {
            #region Material Order


            int MaterialOrderID = Convert.ToInt32(hdfMono.Value);
            AppMaterialOrder objAppMaterialOrder = new AppMaterialOrder(intRequestingUserID, MaterialOrderID);
            txtDateCompare.Text = new AppConvert(objAppMaterialOrder.MaterialOrderDate);
            lblPartnerGroupIDFromID.Text = new AppConvert(objAppMaterialOrder.Customer_Company.Parent_ID);
            lblPartnerGroupIDFromvalue.Text = objAppMaterialOrder.Customer_Company.Parent.Name;
            hdfMorev.Value = new AppConvert(objAppMaterialOrder.ModifiedOn);
            lblPartnerIDFromID.Text = new AppConvert(objAppMaterialOrder.Customer_Company_ID);
            lblPartnerIDFromValue.Text = objAppMaterialOrder.Customer_Company.Name;
            lblDeliveredToValue.Text = objAppMaterialOrder.DeliverLocation;
            lblorderbyName.Text = objAppMaterialOrder.OrderBy;
            lblDeliveredOnValue.Text = new AppConvert(objAppMaterialOrder.DeliveryDate);
            #endregion

            #region Outward
            AppOutward objAppOutward = new AppOutward(intRequestingUserID);
            objAppOutward.OutwardDate = objAppMaterialOrder.DeliveryDate;
            objAppOutward.MaterialOrder_ID = objAppMaterialOrder.ID;
            objAppOutward.DeliveredTo = objAppMaterialOrder.DeliverLocation;
            objAppOutward.DeliverdOn = objAppMaterialOrder.DeliveryDate;

            if (objAppMaterialOrder.MaterialOrderDetailColl.Count > 0)
            {
                objAppOutward.OutwardDetailColl.AddRange(objAppMaterialOrder.MaterialOrderDetailColl.Where(x => x.BalOrderQuantity > 0).
                    Select(x => new AppOutwardDetail(intRequestingUserID)
                    {
                        InwardDetail_ID = x.InwardDetail_ID,
                        MaterialOrderDetail_ID = x.ID,
                        Type = x.Type,
                        Quantity = 0,
                        Status = x.Status
                    })
                );
            }

            #endregion

            grdOutwardDetail.DataSource = objAppOutward.OutwardDetailColl.Where(x => x.Status == (int)RecordStatus.Active);
            grdOutwardDetail.DataBind();

            //btnAdd_Click(sender, e);
            Session.Add(TableConstants.OutWardSession, objAppOutward);
            UsrtxtGate.Focus();
        }
        else
        {
            lblPartnerGroupIDFromvalue.Text = "";
            lblPartnerGroupIDFromID.Text = "";
            lblPartnerIDFromValue.Text = "";
            lblPartnerIDFromID.Text = "";
            lblDeliveredToValue.Text = "";
            lblDeliveredToValue.Text = "";
            lblDeliveredOnValue.Text = "";
            hdfMorev.Value = "";
            UsrtxtMONO.Focus();

        }

    }
    protected void txtOutwardStartedOn_TextChanged1(object sender, EventArgs e)
    {

        if (txtOutwardStartedOn.Text.Trim() != "" && txtOutwardStartedOn.Text != "__/__/____")
        {
            //int sdate = new AppConvert(txtOutwardStartedOn.Text);
            //int otdate = 0;
            //if (txtOutwardDate.Text.Trim() != "" && txtOutwardDate.Text != "__/__/____")
            //{
            //    otdate = new AppConvert(txtOutwardDate.Text);
            //}
            //if (sdate < otdate)
            //{
            //    lblMsg.Text = "Invalid Started On";
            //    txtOutwardStartedOn.Focus();
            //    return;
            //}
            rfvtxtStartedOnHH.Enabled = true;
            metxtStartedOnHH.Enabled = true;
            mevtxtStartedOnHH.Enabled = true;

            rfvtxtStartedOnMM.Enabled = true;
            metxtStartedOnMM.Enabled = true;
            mevtxtStartedOnMM.Enabled = true;
        }
        else
        {
            rfvtxtStartedOnHH.Enabled = false;
            metxtStartedOnHH.Enabled = false;
            mevtxtStartedOnHH.Enabled = false;

            rfvtxtStartedOnMM.Enabled = false;
            metxtStartedOnMM.Enabled = false;
            mevtxtStartedOnMM.Enabled = false;
        }



        txtStartedOnHH.Focus();
    }
    protected void txtOutwardEndedOn_TextChanged1(object sender, EventArgs e)
    {

        if (txtOutwardEndedOn.Text.Trim() != "" && txtOutwardEndedOn.Text != "__/__/____")
        {
            //int sdate = new AppConvert(txtOutwardEndedOn.Text);
            //int otdate = 0;
            //if (txtOutwardDate.Text.Trim() != "" && txtOutwardDate.Text != "__/__/____")
            //{
            //    otdate = new AppConvert(txtOutwardDate.Text);
            //}
            //if (sdate < otdate)
            //{
            //    lblMsg.Text = "Invalid Ended On";
            //    txtOutwardEndedOn.Focus();
            //    return;
            //}
            rfvmetxtEndedOnHR.Enabled = true;
            metxtEndedOnHR.Enabled = true;
            mevtxtEndedOnHR.Enabled = true;

            rfvtxtEndedOnMM.Enabled = true;
            metxtEndedOnMM.Enabled = true;
            mevtxtEndedOnMM.Enabled = true;
        }
        else
        {
            rfvmetxtEndedOnHR.Enabled = false;
            metxtEndedOnHR.Enabled = false;
            mevtxtEndedOnHR.Enabled = false;

            rfvtxtEndedOnMM.Enabled = false;
            metxtEndedOnMM.Enabled = false;
            mevtxtEndedOnMM.Enabled = false;
        }
        txtEndedOnHR.Focus();
    }

    #endregion

    #region Button Event
    protected void btnSave_Click(object sender, EventArgs e)
    {
        #region Validation

        bool val = ValidateOutward();

        if (val == false)
        {
            Response.Write("<script LANGUAGE='JavaScript'> window.confirm('Do You Want to Refresh?');document.location='" + ResolveClientUrl("StockOutDetail.aspx?ID=" + hdfOutward.Value) + "';</script>");
            return;
        }

        #region Zero Quantity Validation   // This validation is for checking Zero quantity entered in line item for saving data.

        if (grdOutwardDetail.Rows.Count > 0)
        {
            int chqty = 0;
            for (int i = 0; i < grdOutwardDetail.Rows.Count; i++)
            {
                #region Finding  gridview labels                   
                TextBox txtOutwardQty = (TextBox)grdOutwardDetail.Rows[i].FindControl("txtOutwardQty");
                #endregion
                if (txtOutwardQty.Text == "0" || string.IsNullOrEmpty(txtOutwardQty.Text))
                {
                    chqty = chqty + 1;
                }
            }
            if (grdOutwardDetail.Rows.Count == chqty)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid Quantity" + "','2'" + ");", true);
                return;
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Select Item" + "','2'" + ");", true);
            return;
        }

        #endregion


        #endregion

        #region MaterialOrder Validation

        bool moval = ValidateMo();
        if (moval == false)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Order has been modified" + "','2'" + ");", true);
            return;
        }

        #endregion

        #region startdate enddate validation

        if (txtOutwardStartedOn.Text.Trim() != "" && txtOutwardStartedOn.Text != "__/__/____")
        {
            if ((txtStartedOnHH.Text.Trim() == "") || (txtStartedOnMM.Text == "__"))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please Enter Valid Started Time" + "','2'" + ");", true);
                return;
            }
        }

        if (txtOutwardEndedOn.Text.Trim() != "" && txtOutwardEndedOn.Text != "__/__/____")
        {
            if ((txtEndedOnHR.Text.Trim() == "") || (txtEndedOnMM.Text == "__"))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please Enter Valid Started Time" + "','2'" + ");", true);
                return;
            }
        }

        #endregion

        if (validatePostback())
        {

            try
            {
                #region Outward Save 
                objAppOutward = (AppOutward)Session[TableConstants.OutWardSession];
                objAppOutward.ModifiedBy = intRequestingUserID;
                objAppOutward.ModifiedOn = DateTime.Now;
                Int32 MaterialOrderID = Convert.ToInt32(hdfMono.Value);
                objAppOutward.MaterialOrder_ID = MaterialOrderID;
                objAppOutward.Customer_Company_ID = Convert.ToInt32(lblPartnerIDFromID.Text);
                objAppOutward.GateEntry_ID = Convert.ToInt32(UsrhdfGate.Value);
                objAppOutward.OutwardDate = new AppConvert(txtOutwardDate.Text);
                objAppOutward.ActualOutwardDate = new AppConvert(txtActualOutwardDate.Text);
                objAppOutward.DeliveredTo = lblDeliveredToValue.Text;
                objAppOutward.DeliverdOn = new AppConvert(lblDeliveredOnValue.Text);
                objAppOutward.Remarks = txtRemarks.Text.Trim();
                objAppOutward.Status = (int)RecordStatus.Active;
                objAppOutward.Type = new AppConvert(ddlOutwardType.SelectedValue);

                #region Code for Validating  Started Date and Inward Ended Date
                if (txtOutwardStartedOn.Text.Trim() != "" && txtOutwardEndedOn.Text.Trim() != "" && txtOutwardStartedOn.Text != "__/__/____" && txtOutwardEndedOn.Text != "__/__/____")
                {
                    int OutwardStartedOn = 0, OutwardEndedOn = 0;
                    OutwardStartedOn = new AppConvert(txtOutwardStartedOn.Text);
                    OutwardEndedOn = new AppConvert(txtOutwardEndedOn.Text);
                    if (OutwardStartedOn > OutwardEndedOn)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid Date" + "','2'" + ");", true);
                        return;
                    }
                    #region Code for Validating  Started On and Ended On Hour and Minute
                    if (OutwardStartedOn == OutwardEndedOn)
                    {
                        int Shh = 0, Smm = 0, Ehh = 0, Emm = 0;
                        Shh = Convert.ToInt32(txtStartedOnHH.Text.Trim());
                        Smm = Convert.ToInt32(txtStartedOnMM.Text.Trim());
                        Ehh = Convert.ToInt32(txtEndedOnHR.Text.Trim());
                        Emm = Convert.ToInt32(txtEndedOnMM.Text.Trim());
                        if (Shh > Ehh)
                        {

                            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid StartTime EndTime" + "','2'" + ");", true);
                            return;
                        }

                        if (Shh == Ehh)
                        {
                            if (Smm > Emm)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid StartTime EndTime" + "','2'" + ");", true);
                                return;
                            }
                        }
                    }
                    #endregion
                }
                #endregion

                if (txtOutwardStartedOn.Text.Trim() != "" && txtStartedOnHH.Text.Trim() != "" && txtStartedOnMM.Text.Trim() != "" && txtOutwardStartedOn.Text != "__/__/____" && txtStartedOnMM.Text != "__" && txtStartedOnHH.Text != "__")
                {
                    DateTime startedon = Convert.ToDateTime(txtOutwardStartedOn.Text + " " + txtStartedOnHH.Text + ":" + txtStartedOnMM.Text);
                    objAppOutward.OutwardStartedOn = startedon;
                }
                if (txtOutwardEndedOn.Text.Trim() != "" && txtEndedOnHR.Text.Trim() != "" && txtEndedOnMM.Text.Trim() != "" && txtOutwardEndedOn.Text != "__/__/____" && txtEndedOnHR.Text != "__" && txtEndedOnMM.Text != "__")
                {
                    DateTime endedon = Convert.ToDateTime(txtOutwardEndedOn.Text + " " + txtEndedOnHR.Text + ":" + txtEndedOnMM.Text);
                    objAppOutward.OutwardEndedOn = endedon;
                }




                if (txtChallanNo.Text.Trim() != objAppOutward.GateEntry.ChallanNo)
                {
                    AppGateEntry objAppGateEntry = new AppGateEntry(intRequestingUserID, objAppOutward.GateEntry_ID);
                    objAppGateEntry.ChallanNo = txtChallanNo.Text;
                    objAppGateEntry.Save();
                }

                int Rows = grdOutwardDetail.Rows.Count;

                for (int i = 0; i < Rows; i++)
                {
                    TextBox txtOutwardQty = (TextBox)grdOutwardDetail.Rows[i].FindControl("txtOutwardQty");
                    if (txtOutwardQty.Text != "" && txtOutwardQty.Text != "0")
                    {
                        #region updating Quantity in MaterialOrderDetail

                        AppMaterialOrderDetail objAppMaterialOrderDetail = new AppMaterialOrderDetail(intRequestingUserID, objAppOutward.OutwardDetailColl[i].MaterialOrderDetail_ID);

                        decimal oldMODetailQty = objAppMaterialOrderDetail.BalOrderQuantity;
                        decimal NewQuantity, DiffQuantity, OldQuantity;
                        OldQuantity = objAppOutward.OutwardDetailColl[i].Quantity;
                        NewQuantity = new AppConvert(txtOutwardQty.Text);
                        DiffQuantity = NewQuantity - OldQuantity;

                        objAppMaterialOrderDetail.ModifiedBy = intRequestingUserID;
                        objAppMaterialOrderDetail.ModifiedOn = DateTime.Now;
                        objAppMaterialOrderDetail.BalOrderQuantity = oldMODetailQty - DiffQuantity;
                        objAppMaterialOrderDetail.Save();

                        #endregion

                        objAppOutward.OutwardDetailColl[i].Quantity = new AppConvert(txtOutwardQty.Text);
                    }
                    else
                    {
                        objAppOutward.OutwardDetailColl[i].Status = -10;
                    }
                }

                objAppOutward.OutwardDetailColl.RemoveAll(x => x.Status == -10);
                objAppOutward.Save();

                #endregion

                #region Auto ID

                //session value coming from login               
                if (hdfOutward.Value == "")
                {
                    lblOutwardNoValue.Text = "OUT" + new AppConvert(AppUtility.AutoID.GetNextNumber(1, "OUT", System.DateTime.Now, 4, ResetCycle.Yearly));
                    objAppOutward.OutwardNo = lblOutwardNoValue.Text.Trim();
                }
                objAppOutward.Save();
                #endregion

                #region code to open report in new window

                //string key = "Opener";
                //string URL = "";
                ////string URL = "../../Reports/ReportView.aspx?OutwardID=" + otwd.OutwardID + "&RPName=OutwardSubReport";
                //if (db.AppSettings.Any(a => a.AppSettingID == 111))
                //{
                //    string str = db.AppSettings.FirstOrDefault(a => a.AppSettingID == 111).AppSettingValue;
                //    URL = "../../Reports/ReportView.aspx?OutwardID=" + otwd.OutwardID + "&RPName=" + str;
                //}
                //else
                //{
                //    URL = "../../Reports/ReportView.aspx?OutwardID=" + otwd.OutwardID + "&RPName=OutwardSubReport";
                //}
                //OpenNewWindow(this, URL, key);

                #endregion

                Response.Write("<script LANGUAGE='JavaScript' > window.alert('Saved Successful \\nOutward Number:-" + objAppOutward.OutwardNo + " \\nFor Scheme:-" + scheme + "');</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "success", "setInterval(function(){location.href='StockOutDetail.aspx';},1000);", true);

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Transaction Failed" + "','2'" + ");", true);
            }
        }
        else
        {
            Response.Write("<script LANGUAGE='JavaScript'> window.alert('Saved Successful \\nOutward Number:-" + outwardno + " \\nFor Scheme:-" + scheme + "');document.location='" + ResolveClientUrl("StockOutDetail.aspx?ID=" + outid) + "';</script>");
            return;
        }

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockOutSearch.aspx");
    }
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockOutDetail.aspx");
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(hdfMono.Value))
        {
            if (ViewState["grdOutward"] == null)
            {
                grdOutwardDetail.DataSource = null;
                grdOutwardDetail.DataBind();
            }
            else
            {
                //totalflag = 0;
                //grdOutwardDetail.DataSource = (DataTable)ViewState["grdOutward"];
                //grdOutwardDetail.DataBind();
            }

            int MaterialOrderID = Convert.ToInt32(hdfMono.Value);
            List<Int64> InwardIDs = new List<Int64>();

            if (grdOutwardDetail.Rows.Count > 0)
            {
                for (int rowindex = 0; rowindex < grdOutwardDetail.Rows.Count; rowindex++)
                {
                    Label lblMaterialOrderDetailID = (Label)grdOutwardDetail.Rows[rowindex].FindControl("lblMaterialOrderDetailID");
                    if (new AppConvert(lblMaterialOrderDetailID.Text) != "")
                    {
                        InwardIDs.Add(Convert.ToInt64(lblMaterialOrderDetailID.Text));
                    }
                }
            }
            List<Int64> currentInwardIDs = InwardIDs;
            //grdOutwardDetail.DataSource = (from mo in db.MaterialOrderDetails where !currentInwardIDs.Contains(mo.MaterialOrderDetailID) && mo.MaterialOrderID == MaterialOrderID && mo.BalanceOutwardQty > 0 && mo.IsActive == 1 select mo).ToList();
            //grdOutwardDetail.DataBind();

            #region Lot Version Validation

            int flog = 0;

            //if (db.AppSettings.Any(a => a.AppSettingID == 24003 && a.AppSettingValue == "Enable"))
            //{
            //    flog = 1;
            //}

            for (int i = 0; i < grdOutwardDetail.Rows.Count; i++)
            {
                Label lblLotNoInternal = (Label)grdOutwardDetail.Rows[i].FindControl("lblLotNoInternal");
                Label lblLotVersionWise = (Label)grdOutwardDetail.Rows[i].FindControl("lblLotVersionWise");
                if (flog == 1)
                {
                    lblLotNoInternal.Visible = false;
                    lblLotVersionWise.Visible = true;
                }
                else
                {
                    lblLotNoInternal.Visible = true;
                    lblLotVersionWise.Visible = false;
                }
            }
            #endregion
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Select Material Order" + "','2'" + ");", true);
        }
        //string sbOutwardDetail = GenericUtills.setfocus(ModalPopupExtender1.BehaviorID, btnSaveDetail.ClientID);
        //Page.ClientScript.RegisterStartupScript(Page.GetType(), "Startup", sbOutwardDetail.ToString());
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        //int CompID = Convert.ToInt32(Session["CompanyID"]);

        //Int64 OutwardID = Convert.ToInt64(hdfOutward.Value);

        //ReportManager RP = new ReportManager();

        //RP = db.ReportManagers.FirstOrDefault(p => p.ReportID == 1130);
        //if (RP == null)
        //{
        //    lblMsg.Text = "ValidateReport";
        //    return;
        //}

        //string url = RP.ReportURL; // Fetch Path from Database
        //string url_updated = url.Replace("@OutwardID", new AppConvert(OutwardID));
        //url_updated = url_updated + "&CompanyLocID=" + CompID;

        //ViewReport(url_updated, e);
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {

        #region MaterialOrder Validation

        bool moval = ValidateMo();
        if (moval == false)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Outward Allready Deleted" + "','2'" + ");", true);
            return;
        }

        #endregion

        {//Transaction begind here
            try
            {
                #region outward

                objAppOutward = (AppOutward)Session[TableConstants.OutWardSession];

                objAppOutward.Status = (int)RecordStatus.Deleted;
                objAppOutward.ModifiedOn = DateTime.Now;
                objAppOutward.ModifiedBy = intRequestingUserID;
                objAppOutward.Save();

                #endregion

                for (int i = grdOutwardDetail.Rows.Count - 1; i >= 0; i--)
                {

                    AppOutwardDetail objAppOutwardDetail = new AppOutwardDetail(intRequestingUserID);
                    objAppOutwardDetail = objAppOutward.OutwardDetailColl[i];

                    if (objAppOutwardDetail != null)
                    {
                        objAppOutwardDetail.ModifiedBy = intRequestingUserID;
                        objAppOutwardDetail.ModifiedOn = DateTime.Now;
                        objAppOutwardDetail.Status = (int)RecordStatus.Deleted;
                        objAppOutwardDetail.Save();

                        decimal OutwardDetQty = objAppOutwardDetail.Quantity;

                        AppMaterialOrderDetail objAppMaterialOrderDetail = new AppMaterialOrderDetail(intRequestingUserID, objAppOutwardDetail.MaterialOrderDetail_ID);

                        if (objAppMaterialOrderDetail != null)
                        {
                            #region Updating MaterialOrderDetail
                            Int32 InwardDetailID = (Int32)(objAppMaterialOrderDetail.InwardDetail_ID);
                            Int32 MaterialOrder_ID = objAppMaterialOrderDetail.MaterialOrder_ID;
                            decimal BalanceOutwardQty = objAppMaterialOrderDetail.BalOrderQuantity;

                            objAppMaterialOrderDetail.ModifiedBy = intRequestingUserID;
                            objAppMaterialOrderDetail.ModifiedOn = DateTime.Now;

                            objAppMaterialOrderDetail.BalOrderQuantity = BalanceOutwardQty + OutwardDetQty;
                            objAppMaterialOrderDetail.Save();
                            #endregion

                            #region Updating MaterialOrder status

                            AppMaterialOrderDetailColl objAppMaterialOrderDetailColl = new AppMaterialOrderDetailColl(intRequestingUserID);
                            objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.BalOrderQuantity, Operators.GreaterThan, 0);
                            objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.MaterialOrder_ID, Operators.GreaterThan, MaterialOrder_ID);
                            objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.Status, Operators.GreaterThan, (int)RecordStatus.Approve);

                            if (objAppMaterialOrderDetailColl.Count > 0)
                            {
                                AppMaterialOrder objAppMaterialOrder = new AppMaterialOrder(intRequestingUserID, MaterialOrder_ID);
                                objAppMaterialOrder.Status = 2; //2 indicates Quantity of all lineitem of specific MaterialOrder is not Zero ==>2= Partial Outward Created,
                                objAppMaterialOrder.ModifiedBy = intRequestingUserID;
                                objAppMaterialOrder.ModifiedOn = DateTime.Now;
                                objAppMaterialOrder.Save();
                            }
                            #endregion

                        }
                        //GenericUtills.InsertTransactionLog(31, owtDet.OutwardDetailID, 4, intRequestingUserID);
                    }
                }

                //GenericUtills.InsertTransactionLog(30, ot.OutwardID, 4, intRequestingUserID);

                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Deletee Successfully" + "','1'" + ");", true);
                btnDelete.Visible = false;
                btnSave.Visible = false;
                PopulateOutward(hdfOutward.Value);
            }
            catch (Exception)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Transaction Failed" + "','2'" + ");", true);
            }
        }
    }
    protected void btnCopy_Click(object sender, EventArgs e)
    {

        //int outword = Convert.ToInt32(hdfOutward.Value);
        //if (db.Outwards.Any(a => a.IsActive != 3 && a.OutwardID == outword))
        //{
        //    hdfOutward.Value = "";
        //    btnSave.Visible = true;
        //    txtOutwardNoValue.Visible = false;
        //    UsrtxtMONO.Enabled = true;
        //    flag = 3;
        //    temp = outword;
        //    for (int i = 0; i < grdOutwardDetail.Rows.Count; i++)
        //    {
        //        Label lblOutwardDetailID = (Label)grdOutwardDetail.Rows[i].FindControl("lblOutwardDetailID");
        //        Label lblIsActive = (Label)grdOutwardDetail.Rows[i].FindControl("lblIsActive");
        //        lblOutwardDetailID.Text = "0";
        //        lblIsActive.Text = "Y";
        //    }
        //    lblMsg.Text = "Copy";
        //}
        //else
        //{
        //    lblMsg.Text = "Sorry Record Allready Copied..!";
        //    btnSave.Visible = false;
        //}
    }

    #endregion

    #region Grid View

    protected void grdOutwardDetail_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Del")
        {
            try
            {
                int RowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
                objAppOutward = (AppOutward)Session[TableConstants.OutWardSession];
                AppOutwardDetail objAppOutwardDetail = new AppOutwardDetail(intRequestingUserID);
                objAppOutwardDetail = objAppOutward.OutwardDetailColl[RowIndex];

                if (objAppOutwardDetail != null)
                {
                    objAppOutwardDetail.ModifiedBy = intRequestingUserID;
                    objAppOutwardDetail.ModifiedOn = DateTime.Now;
                    objAppOutwardDetail.Status = (int)RecordStatus.Deleted;
                    objAppOutwardDetail.Save();

                    decimal OutwardDetQty = objAppOutwardDetail.Quantity;

                    AppMaterialOrderDetail objAppMaterialOrderDetail = new AppMaterialOrderDetail(intRequestingUserID, objAppOutwardDetail.MaterialOrderDetail_ID);

                    if (objAppMaterialOrderDetail != null)
                    {
                        #region Updating MaterialOrderDetail
                        Int32 InwardDetailID = (Int32)(objAppMaterialOrderDetail.InwardDetail_ID);
                        Int32 MaterialOrder_ID = objAppMaterialOrderDetail.MaterialOrder_ID;
                        decimal BalanceOutwardQty = objAppMaterialOrderDetail.BalOrderQuantity;

                        objAppMaterialOrderDetail.ModifiedBy = intRequestingUserID;
                        objAppMaterialOrderDetail.ModifiedOn = DateTime.Now;

                        objAppMaterialOrderDetail.BalOrderQuantity = BalanceOutwardQty + OutwardDetQty;
                        objAppMaterialOrderDetail.Save();
                        #endregion

                        #region Updating MaterialOrder status

                        AppMaterialOrderDetailColl objAppMaterialOrderDetailColl = new AppMaterialOrderDetailColl(intRequestingUserID);
                        objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.BalOrderQuantity, Operators.GreaterThan, 0);
                        objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.MaterialOrder_ID, Operators.GreaterThan, MaterialOrder_ID);
                        objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.Status, Operators.GreaterThan, (int)RecordStatus.Approve);

                        if (objAppMaterialOrderDetailColl.Count > 0)
                        {
                            AppMaterialOrder objAppMaterialOrder = new AppMaterialOrder(intRequestingUserID, MaterialOrder_ID);
                            objAppMaterialOrder.Status = 2; //2 indicates Quantity of all lineitem of specific MaterialOrder is not Zero ==>2= Partial Outward Created,
                            objAppMaterialOrder.ModifiedBy = intRequestingUserID;
                            objAppMaterialOrder.ModifiedOn = DateTime.Now;
                            objAppMaterialOrder.Save();
                        }
                        #endregion

                    }
                    //GenericUtills.InsertTransactionLog(31, owtDet.OutwardDetailID, 4, intRequestingUserID);
                }

                objAppOutward.OutwardDetailColl.Remove(objAppOutwardDetail);

                grdOutwardDetail.DataSource = objAppOutward.OutwardDetailColl.Where(x => x.Status == ((int)RecordStatus.Active));
                grdOutwardDetail.DataBind();

                Session.Add(TableConstants.OutWardSession, objAppOutward);

                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Deleted Successfully" + "','1'" + ");", true);
                PopulateOutward(hdfOutward.Value);

            }
            catch (Exception)
            {

                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Transaction Failed" + "','2'" + ");", true);

            }
        }

    }
    protected void grdOutwardDetail_PreRender(object sender, EventArgs e)
    {
        if (grdOutwardDetail.Rows.Count > 0)
        {
            grdOutwardDetail.UseAccessibleHeader = true;
            grdOutwardDetail.HeaderRow.TableSection = TableRowSection.TableHeader;

            //Footer
            Label lblTotal = (Label)grdOutwardDetail.FooterRow.FindControl("lblTotal");
            lblTotal.Text = "Total";
        }
    }
    protected void grdedit_OnPreRender(object sender, EventArgs e)
    {
        if (grdedit.Rows.Count > 0)
        {
            grdedit.UseAccessibleHeader = true;
            grdedit.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

    #endregion

    #region Report


    public void ViewReport(string URLPath, EventArgs e)
    {
        StringBuilder strContent = new StringBuilder();
        string lasturl = System.Configuration.ConfigurationManager.AppSettings["ReportServerURL1"].ToString() + URLPath; // Define inside web.config file
        strContent.Append("window.open('" + lasturl + "','',',toolbar= 0,menubar= 0,status=0,location=0,resizable=1,scrollbars=1');");
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "subscribescript", strContent.ToString(), true);
    }


    public static void OpenNewWindow(System.Web.UI.Page page, string fullUrl, string key)
    {
        string script = "window.open('" + fullUrl + "', '" + key + "', 'status=1,location=1,menubar=1,resizable=1,toolbar=1,scrollbars=1,titlebar=1');";
        page.ClientScript.RegisterClientScriptBlock(page.GetType(), key, script, true);
    }

    #endregion

    #region showing history

    public void history()
    {
        //int refid = Convert.ToInt32(hdfOutward.Value);
        //IQueryable<TransactionLog> Tlog = from tl in db.TransactionLogs where tl.ReferenceID == refid && tl.ReferenceType == 30 select tl;
        //var cr = from te in Tlog where te.TransactionType == 1 select te;
        //if (cr.Count() != 0)
        //{
        //    int uid = db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 1 && x.ReferenceID == refid && x.ReferenceType == 30).UserID;
        //    lblcruser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == uid).UserFullName;
        //    lblcrdate.Text = new AppConvert(db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 1 && x.ReferenceID == refid && x.ReferenceType == 30).CreateDate);
        //}
        //var edit = (from te in Tlog where te.TransactionType == 2 select te).OrderByDescending(l => l.CreateDate).Take(1);
        //if (edit.Count() != 0)
        //{
        //    int eid = edit.FirstOrDefault().UserID; //db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 2 && x.ReferenceID == refid && x.ReferenceType == 30);
        //    lbleduser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == eid).UserFullName;
        //    DateTime str = edit.FirstOrDefault().CreateDate;
        //    lbleddate.Text = new AppConvert(str);
        //    lnkdetail.Visible = true;
        //}
        //var del = from te in Tlog where te.TransactionType == 4 select te;
        //if (del.Count() != 0)
        //{
        //    int id = db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 4 && x.ReferenceID == refid && x.ReferenceType == 30).UserID;
        //    lbldluser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == id).UserFullName;
        //    lbldldate.Text = new AppConvert(db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 4 && x.ReferenceID == refid && x.ReferenceType == 30).CreateDate);
        //}
    }

    protected void btnhistory_Click(object sender, EventArgs e)
    {
        //if (hdfOutward.Value != "")
        //{
        //    int refid = Convert.ToInt32(hdfOutward.Value);
        //    IQueryable<TransactionLog> Tlog = (from tl in db.TransactionLogs where tl.ReferenceID == refid && tl.ReferenceType == 30 select tl).OrderByDescending(p => p.CreateDate);
        //    var edit = from te in Tlog where te.TransactionType == 2 select te;
        //    if (edit.Count() != 0)
        //    {
        //        grdedit.DataSource = edit.ToList();
        //        grdedit.DataBind();
        //    }

        //}
        //GenericUtills.popup_Show("modalhistory", "", this);
    }

    #endregion

    #region btnApprove click
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        if (hdfOutward.Value != null && hdfOutward.Value != "")
        {
            try
            {
                #region Validation

                #region Double Click Validation
                bool val = ValidateOutward();
                if (val == false)
                {
                    Response.Write("<script LANGUAGE='JavaScript'> window.confirm('Do You Want to Refresh?');document.location='" + ResolveClientUrl("StockOutDetail.aspx?ID=" + hdfOutward.Value) + "';</script>");
                    //Duplicate Save validation 
                    return;
                }

                #endregion

                #region Item Validation

                if (grdOutwardDetail.Rows.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Select Item" + "','2'" + ");", true);
                    return;
                }

                #endregion

                #region Code for Validating AppInward Started Date and AppInward Ended Date

                if (txtOutwardStartedOn.Text.Trim() != "" && txtOutwardEndedOn.Text.Trim() != "" && txtOutwardStartedOn.Text.Trim() != "__/__/____" && txtOutwardEndedOn.Text.Trim() != "__/__/____")
                {
                    DateTime Inwardstartedon, Inwardendedon;
                    Inwardstartedon = new AppConvert(txtOutwardStartedOn.Text);
                    Inwardendedon = new AppConvert(txtOutwardEndedOn.Text);
                    if (Inwardstartedon > Inwardendedon)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid Inward Date" + "','2'" + ");", true);
                        return;
                    }
                    #region Code for Validating  Started On and Ended On Hour and Minute
                    if (Inwardstartedon == Inwardendedon)
                    {
                        int Shh = 0, Smm = 0, Ehh = 0, Emm = 0;
                        Shh = new AppConvert(txtStartedOnHH.Text.Trim());
                        Smm = new AppConvert(txtStartedOnMM.Text.Trim());
                        Ehh = new AppConvert(txtEndedOnHR.Text.Trim());
                        Emm = new AppConvert(txtEndedOnMM.Text.Trim());
                        if (Shh > Ehh)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid start time and end time" + "','2'" + ");", true);
                            return;
                        }

                        if (Shh == Ehh)
                        {
                            if (Smm > Emm)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid start time and end time" + "','2'" + ");", true);
                                return;
                            }
                        }
                    }
                    #endregion
                }
                #endregion

                #endregion

                int OutwardID = Convert.ToInt32(hdfOutward.Value);
                AppOutward objAppOutward = new AppOutward(intRequestingUserID, OutwardID);
                if (objAppOutward != null)
                {
                    objAppOutward.Status = (int)RecordStatus.Approve;
                    objAppOutward.ModifiedBy = intRequestingUserID;
                    objAppOutward.ModifiedOn = DateTime.Now;
                    objAppOutward.Save();

                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Approved Successfully" + "','1'" + ");", true);
                    btnApproved.Visible = false;
                    btnSave.Visible = false;
                }
            }
            catch (Exception ex)
            {

            }

        }
    }

    #endregion

}