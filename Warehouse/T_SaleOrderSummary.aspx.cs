﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to display all Company lists.
*/
#endregion
#region Using
using System;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.UI;
using AppUtility;
#endregion
public partial class T_SaleOrderSummary : BigSunPage
{
    #region PageLoad
    AppSaleOrderColl objPO;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }
    #endregion
    #region BindData   
    protected void BindData()
    {
        AppObjects.AppSaleOrderColl objColl = new AppSaleOrderColl(intRequestingUserID);
        if (txtPurchaeOrderNo.Text!="")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleOrder.SaleOrderNumber, AppUtility.Operators.Equals, txtPurchaeOrderNo.Text, 0);
        }
        if (txtSaleOrderAmount.Text != "")
        {
            switch (ddlOperators.SelectedValue)
            {
                case "1":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleOrder.SaleOrderBaseAmount, AppUtility.Operators.Equals, txtSaleOrderAmount.Text, 0);
                    break;
                case "2":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleOrder.SaleOrderBaseAmount, AppUtility.Operators.GreaterOrEqualTo, txtSaleOrderAmount.Text, 0);
                    break;
                case "3":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleOrder.SaleOrderBaseAmount, AppUtility.Operators.LessOrEqualTo, txtSaleOrderAmount.Text, 0);
                    break;
                case "4":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleOrder.SaleOrderBaseAmount, AppUtility.Operators.GreaterThan, txtSaleOrderAmount.Text, 0);
                    break;
                case "5":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleOrder.SaleOrderBaseAmount, AppUtility.Operators.LessThan, txtSaleOrderAmount.Text, 0);
                    break;
            }
        }
        if (hdfCustomerID.Value != "")
        {
           objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleOrder.Delivery_Company_ID, AppUtility.Operators.Equals, hdfCustomerID.Value, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleOrder.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue, 0);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.SaleOrderSummary] = objColl;
        grdSaleOrderSummary.DataSource = objColl;
        grdSaleOrderSummary.DataBind();
    }
    #endregion
    #region GridView Events
    protected void grdSaleOrderSummary_PreRender(object sender, EventArgs e)
    {
        if (grdSaleOrderSummary.Rows.Count > 0)
        {
            grdSaleOrderSummary.UseAccessibleHeader = true;
            grdSaleOrderSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion   
    #region Button Click Events
    protected void btnNewSaleOrder_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.ObjCompanySession);
        Response.Redirect("T_SaleOrder.aspx");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (Update(grdSaleOrderSummary, new AppConvert((int)RecordStatus.Deleted), 1017, 1018) == 1)
        {
            BindData();
        }

    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        if (Update(grdSaleOrderSummary, new AppConvert((int)RecordStatus.Approve), 1017, 1018) == 1)
        {
            BindData();
        }
    }
    protected void btnCanceled_Click(object sender, EventArgs e)
    {
    }
    #endregion
}