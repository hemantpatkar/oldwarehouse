﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Simple.master" AutoEventWireup="true" Inherits="TermsAndConditionsGroup" Codebehind="TermsAndConditionsGroup.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row" id="divAddTnCgroup" runat="server">
        <div class="col-lg-12">
        <div class="col-lg-8">
            <asp:Label ID="lblTnCGroupName" runat="server" Text="Terms & Condition Group Name" CssClas="bold"></asp:Label>
            <asp:TextBox ID="txtTnCGroupName" runat="server" CssClass="form-control input-sm" MaxLength="50"></asp:TextBox>
        </div>
        <div class="col-lg-4">
            <br />
            <asp:Button ID="btnAddGroup" runat="server" Text="Add Group" OnClick="btnAddGroup_Click" CssClass="btn btn-primary btn-sm" />
        </div>
            </div>
    </div>
</asp:Content>

