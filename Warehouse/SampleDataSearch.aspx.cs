﻿using AppObjects;
using AppUtility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SampleDataSearch : BigSunPage
{
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Sample Search";
        if (!IsPostBack)
        {
        }
    }
    #endregion


    #region Button Event
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("SampleDataDetail.aspx");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        AppSamplingColl objSamplingColl = new AppSamplingColl(intRequestingUserID);

        DateTime fromdate;
        if (txtSampleDate.Text.Trim() != "" && txtSampleDate.Text.Trim() != "__/__/____")
        {
            fromdate = Convert.ToDateTime(txtSampleDate.Text);
            objSamplingColl.AddCriteria(Sampling.SampleDate, Operators.GreaterOrEqualTo, fromdate);
        }
        if (txtCustomer.Text.Trim() != "")
        {
            objSamplingColl.AddCriteria(Sampling.Customer_Company_ID, Operators.StartWith, txtCustomer.Text);
        }
        if (txtSampleNo.Text.Trim() != "")
        {
            objSamplingColl.AddCriteria(Sampling.SampleNo, Operators.Equals, txtSampleNo.Text);
        }
        if (txtReceivedBy.Text.Trim() != "")
        {
            objSamplingColl.AddCriteria(Sampling.ReceivedBy_CompanyContact_ID, Operators.Equals, txtReceivedBy.Text);
        }
        if (ddlStatus.SelectedValue != "-1")
        {
            int status = Convert.ToInt32(ddlStatus.SelectedValue);
            objSamplingColl.AddCriteria(Sampling.Status, Operators.Equals, status); //
        }

        objSamplingColl.Search();
        //objAppGateEntryColl.Sort(AppPreInward.ComparisionDesc);
        grdTranshipment.DataSource = objSamplingColl;
        grdTranshipment.DataBind();
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (grdTranshipment.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Doesnot contain data" + "','2'" + ");", true);
            return;
        }

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grdTranshipment.AllowPaging = false;
            grdTranshipment.HeaderRow.BackColor = Color.White;
            foreach (TableCell cell in grdTranshipment.HeaderRow.Cells)
            {
                cell.BackColor = grdTranshipment.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in grdTranshipment.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = grdTranshipment.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = grdTranshipment.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            grdTranshipment.RenderControl(hw);
            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    #endregion

    #region Grid
    protected void grdTranshipment_PreRender(object sender, EventArgs e)
    {

    }
    #endregion
}