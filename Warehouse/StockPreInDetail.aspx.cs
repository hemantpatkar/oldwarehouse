﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using System.Text;
using System.Drawing;

public partial class StockPreInDetail : BigSunPage
{

    #region Variables     
    AppPreInward objAppPreInward;
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        string languageCode = Convert.ToString(Session["LanguageCode"]);
        Page.Title = "Pre" + " " + "Inward";
        dateval();
        objAppPreInward = new AppPreInward(intRequestingUserID);

        #region if (!IsPostBack)
        if (!IsPostBack)
        {
            Session.Add(TableConstants.PreInwardSession, objAppPreInward);
            ViewState["PostBackID"] = Guid.NewGuid().ToString();
            DecideLanguage(languageCode);
            BydefaultclickEnter();
            fillDropdownforInward();
            hdfInward.Value = Request.QueryString["ID"];
            if (hdfInward.Value != "")
            {
                if (new AppConvert(Request.QueryString["Msg"]) != "")
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Saved Successfully" + "','1'" + ");", true);

                txtDateCompare.Text = "";
                PopulateInward(hdfInward.Value);
                history();
            }
            else
            {
                txtInwardDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtReceivedDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                hdfrevised.Value = "";
                txtDateCompare.Text = "11/12/2000";
                CompareValidator2.Operator = ValidationCompareOperator.GreaterThanEqual;
            }
        }
        #endregion

        txtGateRegis.Focus();
    }
    #endregion

    #region Button Event

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        #region validation
        bool val = ValidatePreInward();
        if (val == false)
        {
            Response.Write("<script LANGUAGE='JavaScript'> window.confirm('Do You Want to Refresh?');document.location='" + ResolveClientUrl("StockPreInDetail.aspx?PreInwardID=" + hdfInward.Value) + "';</script>");

            return;
        }
        #endregion

        if (validatePostback())
        {
            try
            {
                if (grdInward.Rows.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please Select Item..!" + "','2'" + ");", true);
                    return;
                }

                if (hdfInward.Value != "")
                {
                    int PreInwardID = new AppConvert(hdfInward.Value);
                    objAppPreInward = new AppPreInward(intRequestingUserID, PreInwardID);
                    objAppPreInward.Status = new AppConvert((int)RecordStatus.Approve);
                    objAppPreInward.ModifiedBy = intRequestingUserID;
                    objAppPreInward.ModifiedOn = DateTime.Now;
                    objAppPreInward.PreInwardDetailColl.ForEach(x => x.Status = new AppConvert((int)RecordStatus.Approve));
                    objAppPreInward.Save();
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        pnladditem.Attributes.Add("style", "display:");
        hdfInwdDetailRowIndex.Value = "";
        hdfInwardDetail.Value = "";
        txtPackingReference.Text = "";
        clear();
        txtLotNoInternal.Focus();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        #region validation
        bool val = ValidatePreInward();
        if (val == false)
        {
            Response.Write("<script LANGUAGE='JavaScript'> window.confirm('Do You Want to Refresh?');document.location='" + ResolveClientUrl("StockPreInDetail.aspx?PreInwardID=" + hdfInward.Value) + "';</script>");

            return;
        }
        #endregion
        
        if (validatePostback())
        {
            try
            {
                if (grdInward.Rows.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please Select Item..!" + "','2'" + ");", true);
                    return;
                }

                #region Pre Inward Save

                objAppPreInward = (AppPreInward)Session[TableConstants.PreInwardSession];

                if (hdfInward.Value != "")
                {
                    int inwardID = new AppConvert(hdfInward.Value);
                    objAppPreInward.ID = inwardID;
                    objAppPreInward.PreInwardNo = lblInwardNoValue.Text;
                    // GenericUtills.InsertTransactionLog(120, inwardID, 2, intRequestingUserID);
                }
                else
                {
                    objAppPreInward.PreInwardNo = "PREIN" + new AppConvert(AppUtility.AutoID.GetNextNumber(1, "PREIN", System.DateTime.Now, 4, ResetCycle.Yearly));
                }

                objAppPreInward.ModifiedBy = intRequestingUserID;
                objAppPreInward.ModifiedOn = DateTime.Now;
                objAppPreInward.GateEntry_ID = new AppConvert(hdfGateRegis.Text);
                objAppPreInward.PreInwardDate = new AppConvert(txtInwardDate.Text);
                objAppPreInward.ReceivedBy_CompanyContact_ID = new AppConvert(hdfReceivedByID.Value);
                objAppPreInward.Customer_Company_ID = new AppConvert(hdfCustomerID.Value);
                objAppPreInward.Remarks = txtRemarks.Text.Trim();
                objAppPreInward.Status = new AppConvert((int)RecordStatus.Active);
                objAppPreInward.VoucherType_ID = 10;//Type 10=Preinward always 
                objAppPreInward.VoucherID = 0;
                objAppPreInward.ReceivedDate = new AppConvert(txtReceivedDate.Text);

                if (txtChallanNo.Text.Trim() != objAppPreInward.GateEntry.ChallanNo)
                {
                    AppGateEntry objAppGateEntry = new AppGateEntry(intRequestingUserID, objAppPreInward.GateEntry_ID);
                    objAppGateEntry.ChallanNo = txtChallanNo.Text;
                    objAppGateEntry.Save();
                }

                objAppPreInward.Save();


                // Finish => Code to save Inward
                //if (hdfInward.Value == "")
                //{
                //    GenericUtills.InsertTransactionLog(120, inward.PreInwardID, 1, intRequestingUserID);

                //}
                #endregion

                Response.Redirect("StockPreInDetail.aspx?ID=" + objAppPreInward.ID + "&Msg=1");

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Transaction Failed..!" + "','2'" + ");", true);
            }
        }
        else
        {
            Response.Write("<script LANGUAGE='JavaScript' > window.alert('Transaction Failed  \\n" + " " + "');document.location='" + ResolveClientUrl("StockPreInDetail.aspx") + "';</script>");
        }

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockPreInSearch.aspx");

    }
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockPreInDetail.aspx");
    }
    protected void btnDeleteInwardDetail_Click(object sender, EventArgs e)
    {
        try
        {
            #region Inward Validation

            for (int chek = grdInward.Rows.Count - 1; chek >= 0; chek--)
            {
                Label lblInwardDetID = (Label)grdInward.Rows[chek].FindControl("lblInwardDetID");
                int inwarddetid = new AppConvert(lblInwardDetID.Text);

                AppInwardDetailColl objAppInwardDetailColl = new AppInwardDetailColl(intRequestingUserID);
                objAppInwardDetailColl.AddCriteria(InwardDetail.VoucherDetailID, Operators.Equals, inwarddetid);
                objAppInwardDetailColl.AddCriteria(InwardDetail.VoucherType_ID, Operators.Equals, 40);
                objAppInwardDetailColl.Search();

                if (objAppInwardDetailColl.Count() > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Inward has been made from this Pre-Inward. \n Please Cancel the Inward First..!" + "','2'" + ");", true);
                    return;
                }
            }

            #endregion

            #region PreInward
            AppPreInward objAppPreInward = (AppPreInward)Session[TableConstants.PreInwardSession];
            objAppPreInward.Delete();

            #endregion

            //GenericUtills.InsertTransactionLog(120, inid, 4, intRequestingUserID);

            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Deleted Successfully..!" + "','1'" + ");", true);
            grdInward.DataSource = objAppPreInward.PreInwardDetailColl;
            grdInward.DataBind();
            btnSave.Visible = false;
            txtCustomer.Enabled = true;
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + ex.Message + "','2'" + ");", true);
        }


    }

    protected void btnDetailSave_Click(object sender, EventArgs e)
    {
        AppPreInward objAppPreInward = (AppPreInward)Session[TableConstants.PreInwardSession];
        AppPreInwardDetail objAppPreInwardDetail = new AppPreInwardDetail(intRequestingUserID);
        if (hdfInwdDetailRowIndex.Value.ToString().Trim() != "")
        {
            int rowindex = new AppConvert(hdfInwdDetailRowIndex.Value);
            objAppPreInwardDetail = objAppPreInward.PreInwardDetailColl[rowindex];
        }
        else
        {
            objAppPreInward.AddNewPreInwardDetail(objAppPreInwardDetail);
        }

        if (hdfInwardDetail.Value != "" && hdfInwardDetail.Value != "0")
        {
            objAppPreInwardDetail.ID = new AppConvert(hdfInwardDetail.Value);
        }

        objAppPreInwardDetail.StorageSchemeRate_ID = new AppConvert(hdfStorageSchemeRateID.Value);
        objAppPreInwardDetail.Item_ID = new AppConvert(UsrhdfItem.Value);
        objAppPreInwardDetail.ItemMark = txtItemMark.Text.Trim();

        if (txtLotNoInternal.Text != "")
            objAppPreInwardDetail.LotInternal = new AppConvert(txtLotNoInternal.Text);
        else
        {
            AppLotVersion objAppLotVersion = new AppLotVersion(intRequestingUserID, 1);
            objAppPreInwardDetail.LotInternal = objAppLotVersion.LotNumber + 1;
            objAppPreInwardDetail.LotVersion = new AppConvert(objAppLotVersion.ID);
            objAppLotVersion.LotNumber = objAppLotVersion.LotNumber + 1;
            objAppLotVersion.Save();
        }

        objAppPreInwardDetail.LotCustomer = txtLotNoCustomer.Text.Trim();
        objAppPreInwardDetail.Quantity = new AppConvert(txtItemQuantity.Text.Trim());
        objAppPreInwardDetail.PackingReference = txtPackingReference.Text.Trim();
        objAppPreInwardDetail.Status = new AppConvert((int)RecordStatus.Active);
        objAppPreInwardDetail.Billable = 1;
        objAppPreInwardDetail.PalletNo = txtPalletNo.Text.Trim();
        objAppPreInwardDetail.ContainerNo = txtContainerNo.Text.Trim();
        objAppPreInwardDetail.ExpiryDate = new AppConvert(UsrtxtYearMonth.Text);
        objAppPreInwardDetail.StorageLocation_ID = new AppConvert(hdfStorageLocationID.Value);
        objAppPreInwardDetail.Origin_ID = new AppConvert(hdfOriginID.Value);
        objAppPreInwardDetail.Brand_ID = new AppConvert(hdfBrandID.Value);
        objAppPreInwardDetail.Variety_ID = new AppConvert(hdfVarietyID.Value);
        objAppPreInwardDetail.Count = txtcount.Text;
        grdInward.DataSource = objAppPreInward.PreInwardDetailColl;
        grdInward.DataBind();

        Session.Add(TableConstants.PreInwardSession, objAppPreInward);

        if (grdInward.Rows.Count > 0)
        {
            Label lbltotal = (Label)grdInward.FooterRow.FindControl("lbltotal");
            lbltotal.Text = new AppConvert(objAppPreInward.PreInwardDetailColl.Sum(x => x.Quantity));
        }

        txtLotNoInternal.Enabled = true;
        hdfInwardDetail.Value = "";
        UsrtxtItemN.Text = "";
        txtGateRegis.Focus();
        clear();

        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Item added successfully" + "','1'" + ");", true);
        pnladditem.Attributes.Add("style", "display:none");
    }

    #endregion

    #region Function

    public static void OpenNewWindow(System.Web.UI.Page page, string fullUrl, string key)
    {
        string script = "window.open('" + fullUrl + "', '" + key + "', 'status=1,location=1,menubar=1,resizable=1,toolbar=1,scrollbars=1,titlebar=1');";
        page.ClientScript.RegisterClientScriptBlock(page.GetType(), key, script, true);
    }

    #region Validation

    protected bool ValidatePreInward()
    {
        int PreInwardID = new AppConvert("0" + hdfInward.Value);
        string strrev = "";
        AppPreInward CheckAppPreInward = new AppPreInward(intRequestingUserID, PreInwardID);
        if (PreInwardID != 0)
        {
            strrev = new AppConvert(CheckAppPreInward.ModifiedOn);
        }
        if (hdfrevised.Value == strrev)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private bool validatePostback()
    {
        // verify duplicate postback
        string postbackid = ViewState["PostBackID"] as string;

        bool isValid = Cache[postbackid] == null;
        if (isValid)
            Cache.Insert(postbackid, true, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(10));
        return isValid;
    }
    public void dateval()
    {
        //mFinancialYear fy = new mFinancialYear();
        //Int32 fyid =  new AppConvert(Session["FYearID"]);
        //fy = db.mFinancialYears.FirstOrDefault(a => a.FYearID == fyid);
        Rangeval.MinimumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangeval.MaximumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangback.MinimumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangback.MaximumValue = DateTime.Now.ToString("dd/MM/yyyy");
    }

    #endregion

    public void PopulateInward(string ID) // function used to fill all fileds while updating data(populate)
    {
        btnPrint.Visible = true;
        btnhistory.Visible = true;

        int inwardID = new AppConvert(ID);
        objAppPreInward = new AppPreInward(intRequestingUserID, inwardID);
        hdfrevised.Value = new AppConvert(objAppPreInward.ModifiedOn);
        //lblUnitIDvalue.Text = GenericUtills.GetUnitName(iwd.UnitID);

        if (objAppPreInward.Status < 2)
        {
            btnDeleteInwardDetail.Visible = true;
            btnApprove.Visible = true;
        }
        else if (objAppPreInward.Status < 1)
        {

        }
        else if (objAppPreInward.Status < 0)
        {
            btnAddItem.Visible = false;
            btnSave.Visible = false;
        }
        else
        {
            btnSave.Visible = false;
            btnAddItem.Visible = false;
        }

        int partnerid = new AppConvert("0" + objAppPreInward.Customer_Company_ID);

        if (partnerid > 0)
        {
            hdfCustomerID.Value = new AppConvert(objAppPreInward.Customer_Company_ID);
            txtCustomer.Text = objAppPreInward.Customer_Company.Code + "--" + objAppPreInward.Customer_Company.Name;
            hdfCustomerName.Value = txtCustomer.Text;
            txtCustomer.ToolTip = txtCustomer.Text;

            UsrhdfPartnerGroup.Value = new AppConvert(objAppPreInward.Customer_Company.Parent_ID);
            UsrlblPartnerGroup.Text = objAppPreInward.Customer_Company.Parent.Code + "--" + objAppPreInward.Customer_Company.Parent.Name;
            UsrtxtPartnerGroupCode.Text = UsrlblPartnerGroup.Text;
        }

        lblInwardNoValue.Text = new AppConvert(objAppPreInward.PreInwardNo);
        txtGateRegis_AutoCompleteExtender.ContextKey = new AppConvert(objAppPreInward.GateEntry_ID);

        if (new AppConvert(objAppPreInward.GateEntry_ID) > 0)
        {
            hdfGateRegis.Text = new AppConvert(objAppPreInward.GateEntry_ID);
            txtVehicalNo.Text = objAppPreInward.GateEntry.VehicalNo;
            txtGateRegis.Text = objAppPreInward.GateEntry.GateNo;
            txtVehName.Text = objAppPreInward.GateEntry.GateNo;
            txtChallanNo.Text = objAppPreInward.GateEntry.ChallanNo;
        }

        txtInwardDate.Text = new AppConvert(objAppPreInward.PreInwardDate);
        ACEtxtReceivedBy.ContextKey = objAppPreInward.ReceivedBy_CompanyContact_ID.ToString();

        hdfReceivedByID.Value = new AppConvert(objAppPreInward.ReceivedBy_CompanyContact_ID);
        txtReceivedBy.Text = new AppConvert(objAppPreInward.ReceivedBy_CompanyContact.FirstName + " " + objAppPreInward.ReceivedBy_CompanyContact.LastName);
        hdfReceivedByName.Value = txtReceivedBy.Text;

        txtRemarks.Text = objAppPreInward.Remarks;
        txtReceivedDate.Text = new AppConvert(objAppPreInward.ReceivedDate);

        grdInward.DataSource = objAppPreInward.PreInwardDetailColl;
        grdInward.DataBind();

        if (grdInward.Rows.Count > 0)
        {
            Label lbltotal = (Label)grdInward.FooterRow.FindControl("lbltotal");
            lbltotal.Text = new AppConvert(objAppPreInward.PreInwardDetailColl.Sum(x => x.Quantity));
        }

        Session.Add(TableConstants.PreInwardSession, objAppPreInward);
    }

    public void BydefaultclickEnter() // Method used to hit save buton on ENTER key press
    {
        //Inward
        txtGateRegis.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtInwardDate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtReceivedDate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtRemarks.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtReceivedBy.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtCustomer.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtStorageLocation.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtStorageSchemeRate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        UsrtxtYearMonth.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtItemQuantity.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtItemMark.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtLotNoInternal.Attributes.Add("onkeypress", "return  clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtLotNoCustomer.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtPalletNo.Attributes.Add("onkeypress", "return  clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtContainerNo.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtPackingReference.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtLotNoInternal.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        UsrtxtItemN.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtBrand.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtOrigin.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtVariety.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtcount.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");

    }

    private void DecideLanguage(string languageCode)  // Method used to fill Screen Language on lables & Buttons text property
    {
        lblVehical.Text = "Vehicle" + " " + "Name";
        lblPartnerCode.Text = "Customer";// +" " + "Code";
        lblInward.Text = "Pre" + " " + "Inward";
        lblPackingRefer.Text = "Packing" + " " + "Reference";
        btnSave.Text = "Save";
        btnNew.Text = "Create New";
        lblGateRegisterID.Text = "Gate" + " " + "Entry" + " " + "No";
        lblInwardDate.Text = "Pre" + " " + "Inward" + " " + "Date";
        lblReceivedBy.Text = "Received" + " " + "By";
        lblRemarks.Text = "Remarks";

        lblItem.Text = "Item";
        lblItemMark.Text = "Item" + " " + "Mark";
        lblSchemeID.Text = "Scheme";
        lblLotNoInternal.Text = "Lot" + " " + "No";
        lblLotNoCustomer.Text = "Vakkal";
        lblItemQuantity.Text = "Item" + " " + "Quantity";
        lblPalletNo.Text = "Pallet" + " " + "No";
        lblContainerNo.Text = "Container" + " " + "No";
        lblStorageLocationIDInitial.Text = "Storage" + " " + "Location";
        lblReceivedDate.Text = "Received" + " " + "Date";
        lblExpiryYearMonth.Text = "ExpiryYearMonth";
        lblcron.Text = "CreatedOn" + " : ";
        lbledon.Text = "EditedOn";
        lbldelon.Text = "DeletedOn" + " : ";
        lblshowshotcut.Text = "Shortcuts";
        lbledit.Text = "Transaction" + " " + "History";
        lblorigin.Text = "Origin";
        lblBrand.Text = "Brand";
        lblVariety.Text = "Variety";
        lblcount.Text = "Count";
        lblChallanNo.Text = "Challan" + " " + "No.";
        lblPartnerGroupCode.Text = "Customer Group";

    }

    public void clear()
    {
        //txtItem.Text = "";  
        UsrhdfItem.Value = "";
        hdfStorageLocationID.Value = "";
        hdfCustomerID.Value = "";
        hdfCustomerName.Value = "";
        hdfStorageSchemeRateID.Value = "";
        txtCustomer.Text = "";
        UsrlblItem.Text = "";
        hdfStorageLocationName.Value = "";
        hdfStorageSchemeRateName.Value = "";
        //UsrtxtItem.Text = "";
        txtStorageLocation.Text = "";
        txtStorageSchemeRate.Text = "";
        UsrtxtYearMonth.Text = "";
        txtPackingReference.Text = "";
        hdfInwdDetailRowIndex.Value = "";
        txtItemMark.Text = "";
        txtLotNoInternal.Text = "";
        txtLotNoCustomer.Text = "";
        txtItemQuantity.Text = "";
        txtPalletNo.Text = "";
        txtContainerNo.Text = "";
        txtOrigin.Text = "";
        hdfOriginName.Value = "";
        hdfOriginID.Value = "";
        txtBrand.Text = "";
        hdfBrandName.Value = "";
        hdfBrandID.Value = "";
        txtVariety.Text = "";
        hdfVarietyName.Value = "";
        hdfVarietyID.Value = "";
        txtcount.Text = "";
        UsrhdfPartnerGroup.Value = "";
        UsrlblPartnerGroup.Text = "";
        UsrtxtPartnerGroupCode.Text = "";
        txtChallanNo.Text = "";
    }

    public void fillDropdownforInward()
    {
        txtGateRegis_AutoCompleteExtender.ContextKey = "0";
        ACEtxtReceivedBy.ContextKey = "0";
        ACEtxtStorageLocation.ContextKey = Convert.ToString(Session["UnitID"]);
    }

    #endregion

    #region Gried Events    
    protected void grdInward_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Sel")
        {

            #region FindControls
            AppPreInward objAppPreInward = (AppPreInward)Session[TableConstants.PreInwardSession];
            AppPreInwardDetail objAppPreInwardDetail = new AppPreInwardDetail(intRequestingUserID);
            int row = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            objAppPreInwardDetail = objAppPreInward.PreInwardDetailColl[row];
            hdfInwdDetailRowIndex.Value = new AppConvert(row);
            #endregion

            #region validation Preinward Editing


            //Label lblRowIndex = (Label)grdInward.Rows[row].FindControl("lblRowIndex");

            //int Edit;
            //Int64 inid = Convert.ToInt64("0" + lblInwardDetID.Text);

            //if (db.InwardDetails.Any(a => a.ReferenceType == 10 && a.ReferenceID == inid && a.IsActive == 1))
            //{
            //    Edit = 2;
            //}
            //else
            //{
            //    Edit = 1;
            //}

            //if (Edit == 1)
            //{

            //    int unitID =  new AppConvert(Session["UnitID"]);
            //    clear();

            //    row =  new AppConvert(lblRowIndex.Text);
            //    hdfInwdDetailRowIndex.Value = Convert.ToString(row); //use to set hdf value while editing item from gridview

            //    //grdInward.AllowPaging = false;
            //    if (hdfInward.Value == null)
            //    {
            //        grdInward.DataSource = ViewState["grdview"];
            //        grdInward.DataBind();
            //    }
            #endregion

            #region MyRegion


            hdfInwardDetail.Value = new AppConvert(objAppPreInwardDetail.ID);
            UsrhdfItem.Value = new AppConvert(objAppPreInwardDetail.Item_ID);
            txtItemMark.Text = new AppConvert(objAppPreInwardDetail.ItemMark);
            txtLotNoInternal.Text = new AppConvert(objAppPreInwardDetail.LotInternal);
            txtLotNoCustomer.Text = new AppConvert(objAppPreInwardDetail.LotCustomer);
            txtItemQuantity.Text = new AppConvert(objAppPreInwardDetail.Quantity);
            txtPalletNo.Text = new AppConvert(objAppPreInwardDetail.PalletNo);
            txtContainerNo.Text = new AppConvert(objAppPreInwardDetail.ContainerNo);
            UsrtxtYearMonth.Text = new AppConvert(objAppPreInwardDetail.ExpiryDate);
            txtPackingReference.Text = objAppPreInwardDetail.PackingReference;
            hdfStorageSchemeRateID.Value = new AppConvert(objAppPreInwardDetail.StorageSchemeRate_ID);
            txtStorageSchemeRate.Text = objAppPreInwardDetail.StorageSchemeRate.StorageScheme.Code;
            hdfStorageSchemeRateName.Value = txtStorageSchemeRate.Text;
            hdfStorageLocationID.Value = new AppConvert(objAppPreInwardDetail.StorageLocation_ID);
            txtStorageLocation.Text = objAppPreInwardDetail.StorageLocation.Name;
            hdfStorageLocationName.Value = txtStorageLocation.Text;

            UsrlblItem.Text = new AppConvert(objAppPreInwardDetail.Item.Name); //not is use bcoz textbox converted to combobox
            UsrtxtItemN.Text = UsrlblItem.Text; //not is use bcoz textbox converted to combobox           
            UsrlblItem.ToolTip = UsrlblItem.Text;

            if (hdfInwardDetail.Value != "0" && !string.IsNullOrEmpty(hdfInwardDetail.Value))
            {
                ACEtxtStorageSchemeRate.ContextKey = new AppConvert(objAppPreInwardDetail.Item_ID + ',' + objAppPreInward.PreInwardDate.ToString("dd/MM/yyyy"));
            }
            else
            {
                ACEtxtStorageSchemeRate.ContextKey = objAppPreInwardDetail.Item_ID + ',' + DateTime.Now.ToString("dd/MM/yyyy");
            }

            #endregion

            #region LotNo Validation textbox showing

            if (objAppPreInwardDetail.ID != 0)
            {
                if (objAppPreInwardDetail.LotVersion != "")
                {
                    txtLotNoInternal.Enabled = false;
                    UsrtxtItemN.Focus();
                }
                else
                {
                    txtLotNoInternal.Enabled = true;
                    txtLotNoInternal.Focus();
                }
            }
            else
            {
                txtLotNoInternal.Focus();
            }
            #endregion

            #region Origin Brand Variety
            hdfOriginID.Value = new AppConvert(objAppPreInwardDetail.Origin_ID);
            txtOrigin.Text = new AppConvert(objAppPreInwardDetail.Origin.Name);
            hdfOriginName.Value = txtOrigin.Text;
            ACEtxtOrigin.ContextKey = UsrhdfItem.Value;

            hdfBrandID.Value = new AppConvert(objAppPreInwardDetail.Brand_ID);
            txtBrand.Text = new AppConvert(objAppPreInwardDetail.Brand.Name);
            hdfBrandName.Value = txtBrand.Text;
            ACEtxtBrand.ContextKey = UsrhdfItem.Value;

            hdfVarietyID.Value = new AppConvert(objAppPreInwardDetail.Variety_ID);
            txtVariety.Text = new AppConvert(objAppPreInwardDetail.Variety.Name);
            hdfVarietyName.Value = txtVariety.Text;
            ACEtxtVariety.ContextKey = UsrhdfItem.Value;
            txtcount.Text = new AppConvert(objAppPreInwardDetail.Count);

            #endregion

            pnladditem.Attributes.Add("style", "display:");

        }

        if (e.CommandName == "Del")
        {
            AppPreInward objAppPreInward = (AppPreInward)Session[TableConstants.PreInwardSession];
            AppPreInwardDetail objAppPreInwardDetail = new AppPreInwardDetail(intRequestingUserID);
            int row = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            objAppPreInwardDetail = objAppPreInward.PreInwardDetailColl[row];

            //Label lblRowIndex = (Label)grdInward.Rows[row].FindControl("lblRowIndex");
            //Label lblInwardDetID = (Label)grdInward.Rows[row].FindControl("lblInwardDetID");
            //int rowindex =  new AppConvert("0" + lblRowIndex.Text);

            if (objAppPreInwardDetail.ID != 0)
            {
                #region Validation
                AppInwardDetailColl objAppInwardDetailColl = new AppInwardDetailColl(intRequestingUserID);
                objAppInwardDetailColl.AddCriteria(InwardDetail.VoucherDetailID, Operators.Equals, objAppPreInwardDetail.ID);
                objAppInwardDetailColl.AddCriteria(InwardDetail.VoucherType_ID, Operators.Equals, 40);
                objAppInwardDetailColl.Search();

                if (objAppInwardDetailColl.Count() > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Inward has been made from this Pre-Inward. \n Please Cancel the Inward First..!" + "','2'" + ");", true);
                    return;
                }
                else
                {
                    objAppPreInward.PreInwardDetailColl[row].Delete();
                }
                #endregion

                //GenericUtills.InsertTransactionLog(121, indid, 4, intRequestingUserID);

            }

            objAppPreInward.PreInwardDetailColl.Remove(objAppPreInwardDetail);
            grdInward.DataSource = objAppPreInward.PreInwardDetailColl;
            grdInward.DataBind();
            Session.Add(TableConstants.PreInwardSession, objAppPreInward);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Item deleted successfully" + "','1'" + ");", true);
        }
        if (e.CommandName == "Split")
        {
            AppPreInward objAppPreInward = (AppPreInward)Session[TableConstants.PreInwardSession];
            int row = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
            TextBox txtNo = (TextBox)grdInward.Rows[row].FindControl("txtNo");
            TextBox txtApprovedQuantity = (TextBox)grdInward.Rows[row].FindControl("txtApprovedQuantity");
            int no = new AppConvert(txtNo.Text);
            decimal qty = Convert.ToDecimal(txtApprovedQuantity.Text) / no;
            for (int j = 0; j < no; j++)
            {
                AppPreInwardDetail objAppPreInwardDetail = new AppPreInwardDetail(intRequestingUserID);
                objAppPreInwardDetail.Count = objAppPreInward.PreInwardDetailColl[row].Count;
                objAppPreInwardDetail.Variety_ID = objAppPreInward.PreInwardDetailColl[row].Variety_ID;
                objAppPreInwardDetail.Origin_ID = objAppPreInward.PreInwardDetailColl[row].Origin_ID;
                objAppPreInwardDetail.PackingReference = objAppPreInward.PreInwardDetailColl[row].PackingReference;
                objAppPreInwardDetail.PalletNo = objAppPreInward.PreInwardDetailColl[row].PalletNo;
                objAppPreInwardDetail.Brand_ID = objAppPreInward.PreInwardDetailColl[row].Brand_ID;
                objAppPreInwardDetail.Billable = objAppPreInward.PreInwardDetailColl[row].Billable;
                objAppPreInwardDetail.StorageSchemeRate_ID = objAppPreInward.PreInwardDetailColl[row].StorageSchemeRate_ID;
                objAppPreInwardDetail.Item_ID = objAppPreInward.PreInwardDetailColl[row].Item_ID;
                objAppPreInwardDetail.StorageLocation_ID = objAppPreInward.PreInwardDetailColl[row].StorageLocation_ID;
                objAppPreInwardDetail.LotInternal = objAppPreInward.PreInwardDetailColl[row].LotInternal;
                objAppPreInwardDetail.LotVersion = objAppPreInward.PreInwardDetailColl[row].LotVersion;
                objAppPreInwardDetail.LotCustomer = objAppPreInward.PreInwardDetailColl[row].LotCustomer;
                objAppPreInwardDetail.ItemMark = objAppPreInward.PreInwardDetailColl[row].ItemMark;
                objAppPreInwardDetail.ContainerNo = objAppPreInward.PreInwardDetailColl[row].ContainerNo;
                objAppPreInwardDetail.TaxType_ID = objAppPreInward.PreInwardDetailColl[row].TaxType_ID;
                objAppPreInwardDetail.ExpiryDate = objAppPreInward.PreInwardDetailColl[row].ExpiryDate;
                objAppPreInwardDetail.Status = objAppPreInward.PreInwardDetailColl[row].Status;
                objAppPreInwardDetail.Quantity = Math.Round(qty, 2);

                objAppPreInward.AddNewPreInwardDetail(objAppPreInwardDetail);

            }

            objAppPreInward.PreInwardDetailColl.Remove(objAppPreInward.PreInwardDetailColl[row]);
            grdInward.DataSource = objAppPreInward.PreInwardDetailColl;
            grdInward.DataBind();
            Session.Add(TableConstants.PreInwardSession, objAppPreInward);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Item splited successfully" + "','1'" + ");", true);
        }
    }
    protected void grdInward_PreRender(object sender, EventArgs e)
    {
        try
        {
            if (grdInward.Rows.Count > 0)
            {
                grdInward.UseAccessibleHeader = true;
                grdInward.FooterRow.TableSection = TableRowSection.TableFooter;
                grdInward.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        catch (Exception)
        {

        }
    }
    protected void grdedit_OnPreRender(object sender, EventArgs e)
    {
        if (grdedit.Rows.Count > 0)
        {
            grdedit.UseAccessibleHeader = true;
            grdedit.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion

    #region Report


    public void ViewReport(string URLPath, EventArgs e)
    {
        StringBuilder strContent = new StringBuilder();
        string lasturl = System.Configuration.ConfigurationManager.AppSettings["ReportServerURL1"].ToString() + URLPath; // Define inside web.config file
        strContent.Append("window.open('" + lasturl + "','',',toolbar= 0,menubar= 0,status=0,location=0,resizable=1,scrollbars=1');");
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "subscribescript", strContent.ToString(), true);
    }

    #endregion

    #region showing history

    public void history()
    {
        //int refid =  new AppConvert(hdfInward.Value);
        //IQueryable<TransactionLog> Tlog = from tl in db.TransactionLogs where tl.ReferenceID == refid && tl.ReferenceType == 120 select tl;
        //var cr = from te in Tlog where te.TransactionType == 1 select te;
        //if (cr.Count() != 0)
        //{
        //    int uid = db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 1 && x.ReferenceID == refid && x.ReferenceType == 120).UserID;
        //    lblcruser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == uid).UserFullName;
        //    lblcrdate.Text = Convert.ToString(db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 1 && x.ReferenceID == refid && x.ReferenceType == 120).CreateDate);
        //}
        //var edit = (from te in Tlog where te.TransactionType == 2 select te).OrderByDescending(l => l.CreateDate).Take(1);
        //if (edit.Count() != 0)
        //{
        //    int eid = edit.FirstOrDefault().UserID; //db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 2 && x.ReferenceID == refid && x.ReferenceType == 10);
        //    lbleduser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == eid).UserFullName;
        //    DateTime str = edit.FirstOrDefault().CreateDate;
        //    lbleddate.Text = Convert.ToString(str);
        //    lnkdetail.Visible = true;
        //}
        //var del = from te in Tlog where te.TransactionType == 4 select te;
        //if (del.Count() != 0)
        //{
        //    int id = db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 4 && x.ReferenceID == refid && x.ReferenceType == 120).UserID;
        //    lbldluser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == id).UserFullName;
        //    lbldldate.Text = Convert.ToString(db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 4 && x.ReferenceID == refid && x.ReferenceType == 120).CreateDate);
        //}
    }

    protected void btnhistory_Click(object sender, EventArgs e)
    {
        //if (hdfInward.Value != "")
        //{
        //    int refid =  new AppConvert(hdfInward.Value);
        //    IQueryable<TransactionLog> Tlog = from tl in db.TransactionLogs where tl.ReferenceID == refid && tl.ReferenceType == 120 select tl;
        //    var edit = from te in Tlog where te.TransactionType == 2 select te;
        //    if (edit.Count() != 0)
        //    {
        //        grdedit.DataSource = edit.ToList();
        //        grdedit.DataBind();
        //    }

        //}
        ////Modhistory.Show();
        //GenericUtills.popup_Show("modalhistory", "", this);
    }

    #endregion


}