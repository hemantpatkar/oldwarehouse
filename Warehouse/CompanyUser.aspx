﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="CompanyUser" Codebehind="CompanyUser.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <div class="row">
        <div class=" col-lg-12 ">
             <h3>Users Details</h3>
            <hr />
    <asp:UpdatePanel ID="UpUsers" runat="server">
        <ContentTemplate>
            <asp:GridView ID="grdUsers" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Horizontal"
                CssClass="table table-hover table-striped text-nowrap nowrap" OnRowDataBound="grdUsers_RowDataBound"
                OnPreRender="grdUsers_PreRender"
                OnRowCommand="grdUsers_RowCommand" OnRowEditing="grdUsers_RowEditing" OnRowDeleting="grdUsers_RowDeleting"
                OnRowUpdating="grdUsers_RowUpdating">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <HeaderTemplate>
                            <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="ADDNEW" OnClick="lbtnAddNew_Click"
                                Text="Add New" ToolTip="Add New" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtnEdit" runat="server" CssClass=" fa fa-edit fa-2x" CommandName="Edit"
                                Text="" />
                            <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x" CommandName="Delete"
                                Text="" ToolTip="Delete" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="Editcontact"
                                Text="" />
                            <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="CANCELEDIT" CssClass="fa fa-ban fa-2x" toot="Cancel"
                                Text="" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x" ToolTip="Save" ValidationGroup="Addcontact"
                                Text="" UseSubmitBehavior="False" />
                            <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban  fa-2x" Text="" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblLoginName" Text="Login Name" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblLogin" runat="server" CssClass="bold" MaxLength="50" Text='<%#Eval("LoginName") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLoginName" runat="server" Text='<%# Eval("LoginName") %>' MaxLength="50" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>                          
                                <asp:RequiredFieldValidator ID="RFV_txtLoginName" runat="server" ControlToValidate="txtLoginName"
                                    Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditUser"></asp:RequiredFieldValidator>                          
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNewLoginName" runat="server" MaxLength="50" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                            <asp:Label ID="MsgNewLoginName" runat="server" CssClass="text-danger">
                                <asp:RequiredFieldValidator ID="RFV_txtNewLoginName" runat="server" ControlToValidate="txtNewLoginName"
                                    Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                            </asp:Label>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblPassword" Text="Password" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblPassKey" runat="server" Text='<%#Eval("Password") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPassword" runat="server" Text='<%# Eval("Password") %>' MaxLength="50" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RFV_txtPassword" runat="server" ControlToValidate="txtPassword"
                                    Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditUser"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNewPassword" runat="server" Text='' MaxLength="50" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFV_txtNewPassword" runat="server" ControlToValidate="txtNewPassword"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblReportingTo" Text="Reporting To" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblReportingMgr" runat="server" AutoComplete="Off" Text='<%#Eval("Parent.LoginName") %>' CssClass="bold"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtReportingTo" placeholder="Select Reporting Person"  onblur="ClearAutocompleteTextBox(this)" runat="server" MaxLength="50" Text='<%#Eval("Parent.LoginName") %>' AutoComplete="Off" CssClass="input-sm form-control" ValidationGroup="EditUser"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hdfReportingToName" value='<%#Eval("Parent.LoginName") %>' />
                            <asp:HiddenField runat="server" ID="hdfReportingToID" value='<%#Eval("Parent_ID") %>' />
                            <asp:RequiredFieldValidator ID="RFV_txtReportingTo" runat="server" ControlToValidate="txtReportingTo"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditUser"></asp:RequiredFieldValidator>
                            <asp:AutoCompleteExtender ID="ACEtxtReportingTo" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanyUserSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtReportingTo">
                            </asp:AutoCompleteExtender>
                        </EditItemTemplate>
                        <FooterTemplate>                            
                            <asp:TextBox ID="txtNewReportingTo"  placeholder="Select Reporting Person"  onblur="ClearAutocompleteTextBox(this)" runat="server" MaxLength="50" Text="" AutoComplete="Off" CssClass="input-sm form-control" ValidationGroup="AddUser"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hdfNewReportingToName" />
                            <asp:HiddenField runat="server" ID="hdfNewReportingToID" Value="" />
                            <asp:RequiredFieldValidator ID="RFV_txtNewReportingTo" runat="server" ControlToValidate="txtNewReportingTo"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditUser"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewReportingTo"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditUser"></asp:RequiredFieldValidator>
                            <asp:AutoCompleteExtender ID="ACEtxtNewReportingTo" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanyUserSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtNewReportingTo">
                            </asp:AutoCompleteExtender>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblLocation" Text="User Location" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblContactLocation" runat="server" Text='<%# string.Concat(Eval("CompanyAddress.Address1")," ",Eval("CompanyAddress.Address2")) %>'  CssClass="bold"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlCompanyAddress" runat="server" CssClass="form-control input-sm" ValidationGroup="EditUser">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlNewCompanyAddress" runat="server" CssClass="form-control input-sm" ValidationGroup="AddUser">
                            </asp:DropDownList>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblContactDetails" Text="User Contact" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblContact" runat="server" Text='<%# string.Concat(Eval("CompanyContact.FirstName")," ",Eval("CompanyContact.LastName")) %>'  CssClass="bold"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlUserContact" runat="server" CssClass="form-control input-sm" ValidationGroup="EditUser">
                            </asp:DropDownList>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:DropDownList ID="ddlNewUserContact" runat="server" CssClass="form-control input-sm" ValidationGroup="AddUser">
                            </asp:DropDownList>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblDesignation" Text="Designation" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDesig" runat="server" CssClass="bold" Text='<%#Eval("DesignationType.Name") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDesignation" placeholder="Select Designation Type" Text='<%#Eval("DesignationType.Name") %>' runat="server" MaxLength="50" onblur="ClearAutocompleteTextBox(this)" AutoComplete="Off" CssClass="input-sm form-control" ValidationGroup="EditUser"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hdfDesignationName" value='<%#Eval("DesignationType.Name") %>' />
                            <asp:HiddenField runat="server" ID="hdfDesignationID" Value='<%#Eval("DesignationType_ID") %>' />
                            <asp:RequiredFieldValidator ID="RFV_txtDesignation" runat="server" ControlToValidate="txtDesignation"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditUser"></asp:RequiredFieldValidator>
                            <asp:AutoCompleteExtender ID="ACEtxtDesignation" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="DesignationTypeSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtDesignation">
                            </asp:AutoCompleteExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNewDesignation" placeholder="Select Designation Type" runat="server" MaxLength="50" onblur="ClearAutocompleteTextBox(this)" AutoComplete="Off" CssClass="input-sm form-control" ValidationGroup="AddUser"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hdfNewDesignationName" />
                            <asp:HiddenField runat="server" ID="hdfNewDesignationID" Value="" />
                            <asp:RequiredFieldValidator ID="RFV_txtNewDesignation" runat="server" ControlToValidate="txtNewDesignation"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddUser"></asp:RequiredFieldValidator>
                            <asp:AutoCompleteExtender ID="ACEtxtNewDesignation" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="DesignationTypeSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtNewDesignation">
                            </asp:AutoCompleteExtender>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblDepartment" Text="Department" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDepart" runat="server" CssClass="bold" Text='<%#Eval("DepartmentType.Name") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDepartment" placeholder="Select Department Type" Text='<%#Eval("DepartmentType.Name") %>' runat="server" MaxLength="50" AutoComplete="Off" onblur="ClearAutocompleteTextBox()" CssClass="input-sm form-control" ValidationGroup="EditUser"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hdfDepartmentName" value='<%#Eval("DepartmentType.Name") %>' />
                            <asp:HiddenField runat="server" ID="hdfDepartmentID" Value='<%#Eval("DepartmentType_ID") %>' />
                            <asp:RequiredFieldValidator ID="RFV_txtDepartment" runat="server" ControlToValidate="txtDepartment"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditUser"></asp:RequiredFieldValidator>
                            <asp:AutoCompleteExtender ID="ACEtxtDepartment" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="DepartmentTypeSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtDepartment">
                            </asp:AutoCompleteExtender>
                        </EditItemTemplate>
                        <FooterTemplate>                            
                            <asp:TextBox ID="txtNewDepartment" placeholder="Select Department Type" runat="server" MaxLength="50" AutoComplete="Off" CssClass="input-sm form-control" ValidationGroup="AddUser" onblur="ClearAutocompleteTextBox()"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hdfNewDepartmentName" />
                            <asp:HiddenField runat="server" ID="hdfNewDepartmentID" Value="" />
                            <asp:RequiredFieldValidator ID="RFV_txtNewDepartment" runat="server" ControlToValidate="txtNewDepartment"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditUser"></asp:RequiredFieldValidator>
                            <asp:AutoCompleteExtender ID="ACEtxtNewDepartment" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="DepartmentTypeSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtNewDepartment">
                            </asp:AutoCompleteExtender>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblActive" runat="server" Text='<%#Eval("Status").ToString()=="1"?"Y":"N" %>' CssClass="bold"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox runat="server" ID="chkActive" CssClass="bold" Checked='<%#Eval("Status").ToString()=="2"?true:false %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox runat="server" ID="chkIsActive" CssClass=" bold" Checked="true" />
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="grdUsers" />
        </Triggers>
    </asp:UpdatePanel>
            </div>
          </div>
</asp:Content>
