﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="M_Variety" Codebehind="M_Variety.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class=" col-lg-12 ">
            <div id="divButtons" class="pull-right">
                <br />
                <div class="btn-group">
                    <div class=" col-lg-3">
                        <asp:TextBox ID="txtName" runat="server" placeholder="Variety Name" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                    </div>
                    <div class=" col-lg-3">
                        <asp:TextBox ID="txtCode" runat="server" placeholder="Variety Code" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                    </div>
                    <div class=" col-lg-3">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control  input-sm">
                            <asp:ListItem Text="ALL" Value="-32768"></asp:ListItem>
                            <asp:ListItem Text="Approved" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Created" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Deleted" Value="-10"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class=" col-lg-3">
                        <asp:Button runat="server" Text="Search" CssClass="btn btn-sm  btn-primary" ID="btnSearch" OnClick="btnSearch_Click"></asp:Button>
                    </div>

                </div>
            </div>
            <h3>Variety Master</h3>
            <hr />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdVariety" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Horizontal"
                        CssClass="table table-hover table-striped text-nowrap nowrap"
                        OnPreRender="grdVariety_PreRender"
                        OnRowCommand="grdVariety_RowCommand" OnRowEditing="grdVariety_RowEditing" OnRowDeleting="grdVariety_RowDeleting"
                        OnRowUpdating="grdVariety_RowUpdating">
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="ADDNEW"
                                        Text="Add New" ToolTip="Add New" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CssClass=" fa fa-edit fa-2x" CommandName="Edit"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnDelete" Visible="false" runat="server" CssClass="fa fa-trash fa-2x" CommandName="Delete"
                                        Text="" ToolTip="Delete" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="EditVariety"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="CANCELEDIT" CssClass="fa fa-ban fa-2x" toot="Cancel"
                                        Text="" />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x" ToolTip="Save" ValidationGroup="AddVariety"
                                        Text="" UseSubmitBehavior="False" />
                                    <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban  fa-2x" Text="" />
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrCode" Text="Code" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" TabIndex="1" Text='<%#Eval("Code") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                      <asp:HiddenField runat="server" ID="hdfID" Value='<%# Eval("ID") %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfVarietyID" Value='<%# Eval("ID") %>' />
                                    <asp:TextBox ID="txtCode" runat="server" Text='<%# Eval("Code") %>' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtCode" runat="server" ControlToValidate="txtCode"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditVariety"></asp:RequiredFieldValidator>

                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewCode" runat="server" Text='' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtNewCode" runat="server" ControlToValidate="txtNewCode"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddVariety"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrName" Text="Name" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" TabIndex="1" Text='<%#Eval("Name") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("Name") %>' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtName" runat="server" ControlToValidate="txtName"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditVariety"></asp:RequiredFieldValidator>

                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewName" runat="server" Text='' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtNewName" runat="server" ControlToValidate="txtNewName"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddVariety"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" runat="server" Text='<%#(Eval("StatusName")) %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsActiveVariety" CssClass="form-control" TabIndex="8" Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkActiveVariety" CssClass="form-control" TabIndex="8" Checked="true" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Default" Text="Default" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDefault" runat="server" Text='<%#(Eval("Type").ToString()=="1"?"Y":"N") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsDefaultVariety" TabIndex="9" Checked='<%#Eval("Type").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkDefaultVariety" TabIndex="9" />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdVariety" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
