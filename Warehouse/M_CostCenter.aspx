﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="M_CostCenter" Codebehind="M_CostCenter.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class=" col-lg-12 ">
            <div id="divButtons" class="pull-right">
                <br />
                <div class="btn-group">
                    <div class=" col-lg-3">
                        <asp:TextBox ID="txtCostCenterName" runat="server" placeholder="Cost Center Name" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                    </div>
                    <div class=" col-lg-3">
                        <asp:TextBox ID="txtCostCenterCode" runat="server" placeholder="Cost Center Code" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                    </div>
                    <div class=" col-lg-3">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control  input-sm">
                            <asp:ListItem Text="ALL" Value="-32768"></asp:ListItem>
                            <asp:ListItem Text="Approved" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Created" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Deleted" Value="-10"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class=" col-lg-3">
                        <asp:Button runat="server" Text="Search" CssClass="btn btn-sm  btn-primary" ID="btnSearch" OnClick="btnSearch_Click"></asp:Button>
                    </div>

                </div>
            </div>
            <h3>Cost Center Details</h3>
            <hr />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdCostCenter" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Horizontal"
                        CssClass="table table-hover table-striped text-nowrap nowrap"
                        OnPreRender="grdCostCenter_PreRender"
                        OnRowCommand="grdCostCenter_RowCommand" OnRowEditing="grdCostCenter_RowEditing" OnRowDeleting="grdCostCenter_RowDeleting"
                        OnRowUpdating="grdCostCenter_RowUpdating">
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="ADDNEW"
                                        Text="Add New" ToolTip="Add New" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CssClass=" fa fa-edit fa-2x" CommandName="Edit"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnDelete" Visible="false" runat="server" CssClass="fa fa-trash fa-2x" CommandName="Delete"
                                        Text="" ToolTip="Delete" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="EditCostCenter"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="CANCELEDIT" CssClass="fa fa-ban fa-2x" toot="Cancel"
                                        Text="" />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x" ToolTip="Save" ValidationGroup="AddCostCenter"
                                        Text="" UseSubmitBehavior="False" />
                                    <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban  fa-2x" Text="" />
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrCostCenterCode" Text="Code" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCostCenterCode" runat="server" TabIndex="1" Text='<%#Eval("CostCenterCode") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                      <asp:HiddenField runat="server" ID="hdfID" Value='<%# Eval("ID") %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfCostCenterID" Value='<%# Eval("ID") %>' />
                                    <asp:TextBox ID="txtCostCenterCode" runat="server" Text='<%# Eval("CostCenterCode") %>' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtCostCenterCode" runat="server" ControlToValidate="txtCostCenterCode"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditCostCenter"></asp:RequiredFieldValidator>

                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewCostCenterCode" runat="server" Text='' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtNewCostCenterCode" runat="server" ControlToValidate="txtNewCostCenterCode"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddCostCenter"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrCostCenterName" Text="Name" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCostCenterName" runat="server" TabIndex="1" Text='<%#Eval("CostCenterName") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtCostCenterName" runat="server" Text='<%# Eval("CostCenterName") %>' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtCostCenterName" runat="server" ControlToValidate="txtCostCenterName"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditCostCenter"></asp:RequiredFieldValidator>

                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewCostCenterName" runat="server" Text='' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtNewCostCenterName" runat="server" ControlToValidate="txtNewCostCenterName"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddCostCenter"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" runat="server" Text='<%#(Eval("StatusName")) %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsActiveCostCenter" CssClass="form-control" TabIndex="8" Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkActiveCostCenter" CssClass="form-control" TabIndex="8" Checked="true" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Default" Text="Default" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDefault" runat="server" Text='<%#(Eval("Type").ToString()=="1"?"Y":"N") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsDefaultCostCenter" TabIndex="9" Checked='<%#Eval("Type").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkDefaultCostCenter" TabIndex="9" />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdCostCenter" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
