﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using AjaxControlToolkit;

public partial class T_ReceiptVoucherDetails : BigSunPage
{
    #region PageLoad
    AppReceiptVoucher objReceiptVoucher = null;
    CommonFunctions CF = new CommonFunctions();
    protected void Page_Load(object sender, EventArgs e)
    {
        objReceiptVoucher = new AppReceiptVoucher(intRequestingUserID);
        if (!IsPostBack)
        {

            ACEtxtCustomer.ContextKey =new AppConvert((int)AppObjects.CompanyRoleType.Customer);
            Session[TableConstants.SessionReceiptVoucherDetail] = objReceiptVoucher;
            BindToDropdown();
            string ID = Convert.ToString(Request.QueryString["ReceiptVoucherID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int ReceiptVoucherID = new AppUtility.AppConvert(ID);
                objReceiptVoucher = new AppObjects.AppReceiptVoucher(intRequestingUserID, ReceiptVoucherID);
                Session[TableConstants.SessionReceiptVoucherDetail] = objReceiptVoucher;
                PopulateData();
                SetPaymentFor();
                if (Convert.ToString(Request.QueryString["Msg"]) == "2")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Contra Approved Sucessfully','1'" + ");", true);
                }
              
            }
            else
            {
                DefaultData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
                Session[TableConstants.SessionReceiptVoucherDetail] = objReceiptVoucher;
            }
            BindData();
        }
    }
    #endregion
    #region Text Events
    protected void txtCustomerName_TextChanged(object sender, EventArgs e)
    {
        objReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        int CustomerCompanyID = new AppConvert(hdfCustomerID.Value);
        objReceiptVoucher.Customer_Company_ID = CustomerCompanyID;
        objReceiptVoucher.ReceiptFor = new AppConvert(ddlPaymentFor.SelectedValue);
        BindCustomerDetails(CustomerCompanyID);
        BindData();
    }
    protected void txtLedger_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfLedgerID = (HiddenField)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("hdfLedgerID");
        TextBox txtReceiptVoucherDesc = (TextBox)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("txtReceiptVoucherDesc");
        AppReceiptVoucher ReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].Ledger_ID = new AppUtility.AppConvert(hdfLedgerID.Value);
        Session.Add(TableConstants.SessionReceiptVoucherDetail, ReceiptVoucher);
        BindData();
        txtReceiptVoucherDesc.Focus();
    }
    protected void txtReceiptVoucherDesc_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtAmount = (TextBox)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("txtAmount");
        TextBox txtReceiptVoucherDesc = (TextBox)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("txtReceiptVoucherDesc");
        AppReceiptVoucher ReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].ReceiptVoucherDesc = new AppUtility.AppConvert(txtReceiptVoucherDesc.Text);
        Session.Add(TableConstants.SessionReceiptVoucherDetail, ReceiptVoucher);
        BindData();
        txtAmount.Focus();
    }
    protected void txtAmount_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfTDSRate = (HiddenField)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("hdfTDSRate");
        TextBox txtTDS = (TextBox)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("txtTDS");
        TextBox txtAmount = (TextBox)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("txtAmount");
        AppReceiptVoucher ReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].Amount = new AppUtility.AppConvert(txtAmount.Text);
        ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].ModifiedBy = intRequestingUserID;
        ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].ModifiedOn = System.DateTime.Now;
        ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].Status = new AppConvert((int)RecordStatus.Created);
        ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].TDSAmount = TDSAmountCal(new AppConvert(hdfTDSRate.Value), new AppConvert(txtAmount.Text));
        Session.Add(TableConstants.SessionReceiptVoucherDetail, ReceiptVoucher);
        BindData();
        txtTDS.Focus();
    }
    protected void txtTDS_TextChanged(object sender, EventArgs e)
    {

        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfTDSRate = (HiddenField)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("hdfTDSRate");
        HiddenField hdfTDSID = (HiddenField)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("hdfTDSID");
        TextBox txtTDS = (TextBox)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("txtTDS");
        TextBox txtAmount = (TextBox)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("txtAmount");

        AppReceiptVoucher ReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].TDS_ID = new AppUtility.AppConvert(hdfTDSID.Value);
        ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].TDS_Ledger_ID = new AppUtility.AppConvert(ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].TDS.Ledger_ID);
        decimal TDSRate = CF.GetTDSRate(hdfTDSID.Value, txtPaymentDate.Text);
        if (TDSRate == -99)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('TDS rate is not defined.','1'" + ");", true);
        }
        else
        {
            ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].TDSRate = TDSRate;
            ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].TDSAmount = TDSAmountCal(TDSRate, new AppConvert(txtAmount.Text));
        }
        Session.Add(TableConstants.SessionReceiptVoucherDetail, ReceiptVoucher);
        BindData();
        txtTDS.Focus();


    }
    protected void txtProfitCenter_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfProfitCenterID = (HiddenField)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("hdfProfitCenterID");
        TextBox txtCostCenter = (TextBox)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("txtCostCenter");
        AppReceiptVoucher ReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].ProfitCenter_ID = new AppUtility.AppConvert(hdfProfitCenterID.Value);
        Session.Add(TableConstants.SessionReceiptVoucherDetail, ReceiptVoucher);
        BindData();
        txtCostCenter.Focus();
    }
    protected void txtCostCenter_TextChanged(object sender, EventArgs e)
    {

        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfCostCenterID = (HiddenField)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("hdfCostCenterID");
        Button btnAddLineItem = (Button)grdReceiptVoucherDetails.HeaderRow.FindControl("btnAddLineItem");
        AppReceiptVoucher ReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].CostCenter_ID = new AppUtility.AppConvert(hdfCostCenterID.Value);
        Session.Add(TableConstants.SessionReceiptVoucherDetail, ReceiptVoucher);
        BindData();
        btnAddLineItem.Focus();

    }
    protected void txtTDSAmount_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtTDSAmount = (TextBox)grdReceiptVoucherDetails.Rows[RowIndex].FindControl("txtTDSAmount");
        Button btnAddLineItem = (Button)grdReceiptVoucherDetails.HeaderRow.FindControl("btnAddLineItem");
        AppReceiptVoucher ReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        ReceiptVoucher.ReceiptVoucherDetailColl[RowIndex].TDSAmount = new AppUtility.AppConvert(txtTDSAmount.Text);
        Session.Add(TableConstants.SessionReceiptVoucherDetail, ReceiptVoucher);
        BindData();
        btnAddLineItem.Focus();

    }
    #endregion
    #region Functions
    public void DefaultData()
    {
        txtPaymentDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtCurrency.Text = this.DefaultBaseCurrency;
        hdfCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);
        hdfCurrencyName.Value = txtCurrency.Text;
        ACEtxtLedger.ContextKey = Convert.ToString(((ddlPaymentType.SelectedValue == "1") ? "4" : "3") + (",~" + "~" + new AppUtility.AppConvert((int)AppObjects.LedgerFlag.GeneralLedger)));
    }
    public void BindCustomerDetails(int CustomerCompanyID)
    {
        if (CustomerCompanyID > 0)
        {
            AppCompany objCustomerDetails = new AppCompany(intRequestingUserID, CustomerCompanyID);
            CommonFunctions.BindDropDown(intRequestingUserID, Components.CompanyBank, ddlCustomerAccountNo, "AccountNo", "ID", "", false, CustomerCompanyID);
            if (ddlCustomerAccountNo.Items.Count > 0) { ddlCustomerAccountNo.SelectedValue = new AppConvert(objCustomerDetails.DefaultCompanyBank.ID); }
            CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCustomerBankBranch, "Name", "ID", "", true, CustomerCompanyID);
            if (ddlCustomerBankBranch.Items.Count > 0) { ddlCustomerBankBranch.SelectedValue = new AppConvert(objCustomerDetails.DefaultCompanyBank.BankType_ID); }
            CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCustomerBank, "BankBranch", "ID", "", true, CustomerCompanyID);
            if (ddlCustomerBank.Items.Count > 0) { ddlCustomerBank.SelectedValue = new AppConvert(objCustomerDetails.DefaultCompanyBank.BankType_ID); }
            txtPayee.Text = objCustomerDetails.Name;
        }
        else
        {
            ddlCustomerAccountNo.Items.Clear();
            ddlCustomerBankBranch.Items.Clear();
            ddlCustomerBank.Items.Clear();

        }
    }
    public decimal TDSAmountCal(decimal TDSRate, decimal VoucherAmount)
    {
        decimal TDSAmount = 0;
        if (TDSRate > 0)
        {
            TDSAmount = new AppConvert(Math.Round(((Convert.ToDouble(VoucherAmount) * 0.01) * Convert.ToDouble(TDSRate)), 2));
            return TDSAmount;
        }
        else
        {
            return 0;
        }

    }
    private void SetValueToGridFooter()
    {
        AppReceiptVoucher ReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        Label lblFooterAmount = (Label)grdReceiptVoucherDetails.FooterRow.FindControl("lblFooterAmount");
        lblFooterAmount.Text = Convert.ToDecimal(ReceiptVoucher.ReceiptVoucherDetailColl.Where(x => x.Status == 0).Sum(x => x.Amount)).ToString("#,##0.00");
        Label lblFooterTDSAmount = (Label)grdReceiptVoucherDetails.FooterRow.FindControl("lblFooterTDSAmount");
        lblFooterTDSAmount.Text = Convert.ToDecimal(ReceiptVoucher.ReceiptVoucherDetailColl.Where(x => x.Status == 0).Sum(x => x.TDSAmount)).ToString("#,##0.00");
        txtNetPayable.Text = lblFooterAmount.Text;

    }
    #region BindData   
    protected void BindData()
    {
        AppReceiptVoucher objReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        if (objReceiptVoucher == null)
        {
            objReceiptVoucher = new AppReceiptVoucher(intRequestingUserID);
        }

        if (hdfCustomerID.Value != "")
        {
            objReceiptVoucher.Customer_Company_ID = new AppConvert(hdfCustomerID.Value);
        }
        if (objReceiptVoucher.ReceiptVoucherDetailColl.Count <= 0)
        {
            AddEmptyRow(1);
        }
        else
        {
            if (ddlPaymentFor.SelectedValue == "1" || ddlPaymentFor.SelectedValue == "3")
            {
                objReceiptVoucher.ReceiptVoucherDetailColl[0].Ledger_ID = objReceiptVoucher.Customer_Company.Customer_Ledger_ID;
            }
            
        }
        grdReceiptVoucherDetails.DataSource = objReceiptVoucher.ReceiptVoucherDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created));
        grdReceiptVoucherDetails.DataBind();
        divBillDetails.Visible = true;
        Session.Add(TableConstants.SessionReceiptVoucherDetail, objReceiptVoucher);
        SetValueToGridFooter();
    }
    public void AddEmptyRow(int NumberOfRows)
    {
        AppReceiptVoucher objReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        if (objReceiptVoucher == null)
        {
            objReceiptVoucher = new AppReceiptVoucher(intRequestingUserID);
        }
        AppReceiptVoucherDetail RVDC = new AppReceiptVoucherDetail(intRequestingUserID);
        if (ddlPaymentFor.SelectedValue == "1" || ddlPaymentFor.SelectedValue == "3")
        {
            RVDC.Ledger_ID = objReceiptVoucher.Customer_Company.Customer_Ledger_ID;

        }
        else
        {
            objReceiptVoucher.ReceiptVoucherDetailColl[0].Ledger_ID = 0;
        }
        for (int i = 0; i < NumberOfRows; i++)
        {
            if (objReceiptVoucher.ReceiptVoucherDetailColl.Count == 0)
            {
                objReceiptVoucher.ReceiptVoucherDetailColl.Add(RVDC);
            }
            else
            {
                objReceiptVoucher.ReceiptVoucherDetailColl.Insert(0, RVDC);
            }
            Session.Add(TableConstants.SessionReceiptVoucherDetail, objReceiptVoucher);
        }
        BindData();
    }
    #endregion
    public void BindToDropdown()
    {
        CommonFunctions.BindDropDown(intRequestingUserID, Components.PaymentType, ddlPaymentType, "PaymentTypeName", "ID", "", false);
        CommonFunctions.BindDropDown(intRequestingUserID, Components.CompanyBank, ddlCompanyAccountNo, "AccountNo", "ID", "", false, LoginCompanyID);
        ddlCompanyAccountNo.SelectedValue = new AppConvert(DefaultCompanyBank.ID);
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCompanyBankBranch, "BankBranch", "ID", "", true, LoginCompanyID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(DefaultCompanyBank.BankType_ID);
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCompanyBank, "Name", "ID", "", true, LoginCompanyID);
        ddlCompanyBank.SelectedValue = new AppConvert(DefaultCompanyBank.BankType_ID);
        SetBankDetails();
    }
    public void SetPaymentFor()
    {
        int PaymentFor = new AppConvert(ddlPaymentFor.SelectedValue);
        switch (PaymentFor)
        {
            case 1:// Payment For Customer
                divCustomerName.Visible = true;
                grdReceiptVoucherDetails.Columns[3].Visible = true;
                grdReceiptVoucherDetails.Columns[4].Visible = true;
                break;
            case 2://Payment For TDS
                divCustomerName.Visible = false;
                grdReceiptVoucherDetails.Columns[3].Visible = false;
                grdReceiptVoucherDetails.Columns[4].Visible = false;
                break;
            case 3: //Payment For Refund
                divCustomerName.Visible = true;
                grdReceiptVoucherDetails.Columns[3].Visible = true;
                grdReceiptVoucherDetails.Columns[4].Visible = true;
                break;
            case 4: //Payment For Other
                divCustomerName.Visible = false;
                grdReceiptVoucherDetails.Columns[3].Visible = true;
                grdReceiptVoucherDetails.Columns[4].Visible = true;
                break;

            default: //Payment For Other
                divCustomerName.Visible = true;
                grdReceiptVoucherDetails.Columns[3].Visible = true;
                grdReceiptVoucherDetails.Columns[4].Visible = true;
                break;
        }
    }
    public void SetBankDetails()
    {
        int PaymentType = new AppConvert(ddlPaymentType.SelectedValue);
        switch (PaymentType)
        {
            case 1:
                divBankDetails.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            case 2:
                divBankDetails.Visible = true;
                divChequeNo.Visible = true;
                divChequeDate.Visible = true;
                break;
            case 3:
                divBankDetails.Visible = true;
                divChequeNo.Visible = true;
                divChequeDate.Visible = true;
                break;
            case 4:
                divBankDetails.Visible = true;
                divChequeNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            case 5:
                divBankDetails.Visible = true;
                divChequeNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            default:
                divBankDetails.Visible = false;
                divChequeNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
        }
        SetPaymentFor();
    }
    public void PopulateData()
    {
        AppReceiptVoucher objReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        txtPaymentDate.Text = new AppConvert(objReceiptVoucher.ReceiptVoucherDate);
        ddlPaymentType.SelectedValue = new AppConvert(objReceiptVoucher.PaymentType_ID);
        ddlCompanyAccountNo.SelectedValue = new AppConvert(objReceiptVoucher.From_CompanyBank_ID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objReceiptVoucher.From_CompanyBank.BankType.Name);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objReceiptVoucher.From_CompanyBank.BankType.BankBranch);
        if (objReceiptVoucher.PaymentType_ID > 1)
        {
            BindCustomerDetails(objReceiptVoucher.Customer_Company_ID);
            txtChequeNo.Text = new AppConvert(objReceiptVoucher.ChequeNo);
            txtChequeDate.Text = new AppConvert(objReceiptVoucher.ChequeDate);
            divChequeNo.Visible = true;
            divChequeDate.Visible = true;
            ddlCustomerAccountNo.SelectedValue = new AppConvert(objReceiptVoucher.To_CompanyBank_ID);
            ddlCustomerBankBranch.SelectedValue = new AppConvert(objReceiptVoucher.To_CompanyBank.BankType.BankBranch);
            ddlCustomerBank.SelectedValue = new AppConvert(objReceiptVoucher.To_CompanyBank.BankType.Name);
            divBankDetails.Visible = true;
        }
        else
        {
            divBankDetails.Visible = false;
        }
        txtCustomer.Text = new AppConvert(objReceiptVoucher.Customer_Company.Name);
        hdfCustomerName.Value = new AppConvert(objReceiptVoucher.Customer_Company.Name);
        hdfCustomerID.Value = new AppConvert(objReceiptVoucher.Customer_Company_ID);
        txtNetPayable.Text = new AppConvert(objReceiptVoucher.Amount);
        txtPayee.Text = new AppConvert(objReceiptVoucher.Customer_Company.Name);
        txtLedger.Text = new AppConvert(objReceiptVoucher.Ledger.LedgerName);
        hdfLedgerName.Value = new AppConvert(objReceiptVoucher.Ledger.LedgerName);
        hdfLedgerID.Value = new AppConvert(objReceiptVoucher.Ledger_ID);
        txtVoucherNo.Text = new AppConvert(objReceiptVoucher.ReceiptVoucherNo);
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(objReceiptVoucher.CurrencyExchID);
        ddlPaymentFor.SelectedValue = new AppConvert(objReceiptVoucher.ReceiptFor);
        ddlPaymentFor.Enabled = false;
        ddlPaymentType.Enabled = false;
        txtCustomer.Enabled = false;
        //hdfHeaderExchangeCurrencyName.Value = new AppConvert(objReceiptVoucher.cu);
        txtHeaderExchangeCurrency.Text = "";

        txtCurrency.Text = new AppConvert(objReceiptVoucher.CurrencyType.Name);
        hdfCurrencyID.Value = new AppConvert(objReceiptVoucher.CurrencyType_ID);
        hdfCurrencyName.Value = new AppConvert(objReceiptVoucher.CurrencyType.Name);


        objReceiptVoucher.GetAllReceiptOrderDetails();
        grdReceiptVoucherDetails.DataSource = objReceiptVoucher.ReceiptVoucherDetailColl;
        grdReceiptVoucherDetails.DataBind();
        Session[TableConstants.SessionReceiptVoucherDetail] = objReceiptVoucher;
        ACEtxtLedger.ContextKey = Convert.ToString(((ddlPaymentType.SelectedValue == "1") ? "4" : "3") + (",~" + "~" + new AppUtility.AppConvert((int)AppObjects.LedgerFlag.GeneralLedger)));
    }
    #endregion
    #region Dropdown Events
    protected void ddlPaymentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetBankDetails();
        int Payment_Type = new AppConvert(ddlPaymentType.SelectedValue);
        if (Payment_Type > 1)
        {
            AppCompanyBank ObjCompanyBank = new AppCompanyBank(intRequestingUserID, Payment_Type);
            txtLedger.Text = ObjCompanyBank.Ledger.LedgerName;
            hdfLedgerName.Value = ObjCompanyBank.Ledger.LedgerName;
            hdfLedgerID.Value = new AppConvert(ObjCompanyBank.Ledger_ID);
            ACEtxtLedger.ContextKey = Convert.ToString(((ddlPaymentType.SelectedValue == "1") ? "4" : "3") + (",~" + "~" + new AppUtility.AppConvert((int)AppObjects.LedgerFlag.GeneralLedger)));
        }
        else
        {
            txtLedger.Text = "";
            hdfLedgerName.Value = "";
            hdfLedgerID.Value = "";
        }
    }
    protected void ddlCompanyAccountNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Company_Bank_Id = new AppConvert(ddlCompanyAccountNo.SelectedValue);
        AppCompanyBank objCOmpanyBank = new AppCompanyBank(intRequestingUserID, Company_Bank_Id);
        ddlCompanyBank.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
    }
    protected void ddlCustomerAccountNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Company_Bank_Id = new AppConvert(ddlCustomerAccountNo.SelectedValue);
        AppCompanyBank objCOmpanyBank = new AppCompanyBank(intRequestingUserID, Company_Bank_Id);
        ddlCompanyBank.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
    }
    #endregion
    #region GridView Events
    protected void grdReceiptVoucherDetails_PreRender(object sender, EventArgs e)
    {
        if (grdReceiptVoucherDetails.Rows.Count > 0)
        {
            grdReceiptVoucherDetails.UseAccessibleHeader = true;
            grdReceiptVoucherDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdReceiptVoucherDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
            AppReceiptVoucher ReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
            ReceiptVoucher.ReceiptVoucherDetailColl.Where(x => x.Status == 0).ToList()[row.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
            Session.Add(TableConstants.SessionReceiptVoucherDetail, ReceiptVoucher);
            BindData();

        }
    }
    #endregion
    #region CheckBox Events
    public static bool CheckItemSelectedinGrid(GridView Grid, string CheckBoxName)
    {
        if (Grid.Rows.Count == 0)
        {
            return false;
        }
        for (int i = 0; i < Grid.Rows.Count; i++)
        {
            CheckBox chkSelect = (CheckBox)Grid.Rows[i].FindControl(CheckBoxName);
            if (chkSelect.Checked == true)
            {
                return true;
            }
        }
        return false;
    }
    #endregion
    #region Button Click Event
    protected void btnAddLineItem_Click(object sender, EventArgs e)
    {
        AddEmptyRow(1);
        if (grdReceiptVoucherDetails.Rows.Count >= 0)
        {
            TextBox txtLedger = (TextBox)grdReceiptVoucherDetails.Rows[0].FindControl("txtLedger");
            AutoCompleteExtender ACEtxtLedger = (AutoCompleteExtender)grdReceiptVoucherDetails.Rows[0].FindControl("ACEtxtLedger");
            ACEtxtLedger.ContextKey = hdfCustomerID.Value;
            txtLedger.Focus();
        }
    }
    protected void ddlPaymentFor_SelectedIndexChanged(object sender, EventArgs e)
    {
        int CustomerCompanyID = new AppConvert(hdfCustomerID.Value);
        SetPaymentFor();
        objReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        if (ddlPaymentFor.SelectedValue == "1" || ddlPaymentFor.SelectedValue == "3")
        {
            objReceiptVoucher.Customer_Company_ID = CustomerCompanyID;
        }
        else
        {
            txtCustomer.Text = "";
            hdfCustomerName.Value = "";
            hdfCustomerID.Value = "0";
            objReceiptVoucher.Customer_Company_ID = 0;

        }
        BindData();
    }
    protected void btnApprove_Click1(object sender, EventArgs e)
    {
        AppReceiptVoucher RV = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        RV.Status = new AppUtility.AppConvert((int)RecordStatus.Approve);
        RV.Save();
        Response.Redirect("T_ReceiptVoucherDetails.aspx?Msg=2");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppReceiptVoucher RV = (AppReceiptVoucher)Session[TableConstants.SessionReceiptVoucherDetail];
        string Message = "Record successfully Saved";
        if (RV.ID == 0)
        {
            RV.ReceiptVoucherNo = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "RV", System.DateTime.Now, "RV-", 4, ResetCycle.Yearly));
        }
        else
        {
            Message = "Record successfully Updated";
            RV.IsRecordModified = true;
        }
        tabMain.ActiveTabIndex = 2;
        int Payment_Type = new AppConvert(ddlPaymentType.SelectedValue);
        if ((Payment_Type == 2 || Payment_Type == 3))
        {
            if (string.IsNullOrEmpty(txtChequeNo.Text) == true)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter cheque no.','2'" + ");", true);
                return;
            }
            if (string.IsNullOrEmpty(txtChequeDate.Text) == true)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Cheque Date.','2'" + ");", true);
                return;
            }
        }

        RV.Company_ID = new AppConvert(LoginCompanyID);
        RV.VoucherType_ID = 9;
        RV.Customer_Company_ID = new AppConvert(hdfCustomerID.Value);
        RV.ReceiptVoucherDate = new AppConvert(txtPaymentDate.Text);
        RV.PaymentType_ID = new AppConvert(ddlPaymentType.SelectedValue);
        RV.Amount = new AppConvert(txtNetPayable.Text);
        RV.Description = txtDescription.Text;
        RV.Customer_Company_ID = new AppConvert(hdfCustomerID.Value);
        RV.Ledger_ID = new AppConvert(hdfLedgerID.Value);// RV.Customer_Company.Customer_Ledger_ID; 
        RV.ChequeNo = new AppConvert(txtChequeNo.Text);
        RV.ChequeDate = new AppConvert(txtChequeDate.Text);
        RV.CurrencyType_ID = new AppConvert(hdfCurrencyID.Value);
        RV.CurrencyExchID = new AppConvert(hdfHeaderExchangeCurrencyID.Value);
        RV.ExchangeRate = new AppConvert(txtHeaderExchangeCurrencyRate.Text);
        RV.From_CompanyBank_ID = new AppConvert(ddlCompanyAccountNo.SelectedValue);
        RV.To_CompanyBank_ID = new AppConvert(ddlCustomerAccountNo.SelectedValue);
        RV.ReceiptFor = new AppConvert(ddlPaymentFor.SelectedValue);
        RV.Advance = 0;
        RV.ModifiedBy = intRequestingUserID;
        RV.ModifiedOn = System.DateTime.Now;
        RV.Status = new AppConvert((int)RecordStatus.Created);
        RV.Type = 0;
        RV.Save();
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + Message + "','1'" + ");", true);


        #region Add JournalEntries
        if (ddlPaymentFor.SelectedValue == "3")
        {
            decimal TdsAmount = RV.ReceiptVoucherDetailColl.Sum(x => x.TDSAmount);
            LedgerEntry(LoginCompanyID, 101301, 1013, RV.Ledger_ID, RV.ID, RV.ReceiptVoucherDate, RV.Amount - TdsAmount, 0, RV.ReceiptVoucherNo, RV.ModifiedBy, RV.ModifiedOn, 0, RV.Status, RV.Type,
                                  0, 0, RV.VoucherType_ID, RV.Description, "", RV.PaymentType_ID);
            if (RV.ReceiptVoucherDetailColl.Count > 0)
            {
                foreach (var item in RV.ReceiptVoucherDetailColl)
                {
                    if (item.Amount != 0)
                    {
                        LedgerEntry(LoginCompanyID, 101401, 1014, item.Ledger_ID, item.ID, RV.ReceiptVoucherDate, 0, item.Amount, RV.ReceiptVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, RV.Status, item.Type,
                                     item.ProfitCenter_ID, item.CostCenter_ID, RV.VoucherType_ID, item.ReceiptVoucherDesc, "", RV.PaymentType_ID);
                        LedgerEntry(LoginCompanyID, 101402, 1014, item.TDS_Ledger_ID, item.ID, RV.ReceiptVoucherDate, item.TDSAmount, 0, RV.ReceiptVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, RV.Status, item.Type,
                                     item.ProfitCenter_ID, item.CostCenter_ID, RV.VoucherType_ID, item.ReceiptVoucherDesc, "", RV.PaymentType_ID);
                    }
                }

            }

        }
        else
        {

            decimal TdsAmount = RV.ReceiptVoucherDetailColl.Sum(x => x.TDSAmount);
            LedgerEntry(LoginCompanyID, 101301, 1013, RV.Ledger_ID, RV.ID, RV.ReceiptVoucherDate, 0, RV.Amount - TdsAmount, RV.ReceiptVoucherNo, RV.ModifiedBy, RV.ModifiedOn, 0, RV.Status, RV.Type,
                                 0, 0, RV.VoucherType_ID, RV.Description, "", RV.PaymentType_ID);
            if (RV.ReceiptVoucherDetailColl.Count > 0)
            {
                foreach (var item in RV.ReceiptVoucherDetailColl)
                {
                    if (item.Amount != 0)
                    {
                        LedgerEntry(LoginCompanyID, 101401, 1014, item.Ledger_ID, item.ID, RV.ReceiptVoucherDate, item.Amount, 0, RV.ReceiptVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, RV.Status, item.Type,
                                     item.ProfitCenter_ID, item.CostCenter_ID, RV.VoucherType_ID, item.ReceiptVoucherDesc, "", RV.PaymentType_ID);
                        LedgerEntry(LoginCompanyID, 101402, 1014, item.TDS_Ledger_ID, item.ID, RV.ReceiptVoucherDate, 0, item.TDSAmount, RV.ReceiptVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, RV.Status, item.Type,
                                     item.ProfitCenter_ID, item.CostCenter_ID, RV.VoucherType_ID, item.ReceiptVoucherDesc, "", RV.PaymentType_ID);
                    }
                }

            }


        }
        #endregion

        Session.Remove(TableConstants.SessionReceiptVoucherDetail);
        Response.Redirect("T_ReceiptVoucherSummary.aspx?Msg=1&ReceiptVoucherID=" + RV.ID);

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.SessionReceiptVoucherDetail);
        Response.Redirect("T_ReceiptVoucherSummary.aspx");
    }
    #endregion
}