﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="UserDesigSummary" Codebehind="UserDesigSummary.aspx.cs" %>

<script runat="server">


</script>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <asp:UpdatePanel ID="update" runat="server">
                <ContentTemplate>
                    <div class="panel">
                        <div class="panel-body">
                            <h3>User Designation Summary </h3>
                            <hr />
                            <div class="row" runat="server" id="pnlDeptEntry">
                                <div class="col-lg-2">
                                    <asp:Label ID="lblDesigCode" runat="server" CssClass="bold " Text="Designation Code"></asp:Label>
                                    <asp:Label ID="Label18" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                    <asp:TextBox ID="txtDesigCode" runat="server" CssClass=" form-control input-sm" MaxLength="30" TabIndex="1"></asp:TextBox>
                                    <%-- <asp:RequiredFieldValidator ID="RFV_txtDesigCode" runat="server" ControlToValidate="txtDesigCode"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*"
                                        ValidationGroup="Save"></asp:RequiredFieldValidator>--%>
                                </div>

                                <div class="col-lg-2">
                                    <asp:Label ID="lblDesigName" runat="server" CssClass="bold " Text="Designation Name"></asp:Label>
                                    <asp:Label ID="Label1" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                    <asp:TextBox ID="txtDesigName" TabIndex="1" runat="server" AutoCompleteType="None" AutoComplete="Off"
                                        CssClass=" form-control input-sm" MaxLength="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtDesigName" runat="server" ControlToValidate="txtDesigName"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*"
                                        ValidationGroup="Save"></asp:RequiredFieldValidator>
                                </div>

                                <div class=" col-lg-2">
                                    <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClas="bold"></asp:Label>
                                    <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control input-sm"></asp:DropDownList>
                                </div>
                                <div class=" col-lg-2">
                                    <asp:Label ID="lblReportingDesignation" runat="server" Text="Reporting To" CssClas="bold"></asp:Label>
                                    <asp:DropDownList ID="ddlReportDesign" runat="server" CssClass="form-control input-sm"></asp:DropDownList>
                                </div>

                                <div class="col-lg-1">
                                    <asp:Label ID="lblActive" runat="server" Text="Active/InActive" CssClass="bold"></asp:Label>
                                    <div class="material-switch pull-right" tabindex="1">
                                        <asp:CheckBox runat="server" ID="chkActive" Checked="true" TabIndex="1" />
                                        <label for='<%=chkActive.ClientID %>' class="label-primary"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="row" runat="server" id="pnlSearch">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <asp:Label ID="Label3" runat="server" CssClass="bold" Text="Designation Code"></asp:Label>
                                            <asp:TextBox ID="txtCode" runat="server" TabIndex="1" MaxLength="50" CssClass="form-control" Enabled="true"></asp:TextBox>
                                        </div>

                                        <div class="col-lg-3">
                                            <asp:Label ID="Label4" runat="server" CssClass="bold" Text="Designation Name"></asp:Label>
                                            <asp:TextBox ID="txtName" runat="server" TabIndex="1" MaxLength="50" CssClass="form-control" Enabled="true"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="btn-group pull-right">
                                        <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="btn btn-primary" OnClick="btnSearch_Click" />
                                        <asp:HiddenField ID="hdfDesigId" runat="server" Value='<%#Eval("ID") %>' />
                                        <asp:Button runat="server" ID="btnSaveDesignation" Text="Save" CssClass="btn btn-primary" TabIndex="1" OnClick="btnSaveDesignation_Click" ValidationGroup="Save" />

                                        <asp:Button runat="server" ID="btnReset" TabIndex="1" Text="Reset" CssClass="btn btn-default" OnClick="btnReset_Click" />

                                        <asp:Button runat="server" ID="btnCancel" TabIndex="1" Text="Cancel" CssClass="btn btn-default" OnClick="btnCancel_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

    <div class="row" runat="server" id="pnlGridViewDisplay">
        <div class="col-lg-12">
            <asp:UpdatePanel ID="pnlGridView" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdDesignations" runat="server" AutoGenerateColumns="False" OnPreRender="grdDesignations_PreRender"
                        OnRowEditing="grdDesignations_RowEditing" CssClass="table table-striped dt-responsive table-hover" OnRowDeleting="grdDesignations_RowDeleting">
                        <Columns>
                            <asp:TemplateField HeaderText="Name">
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="Insert" OnClick="btnAddNew_Click"
                                        Text="Add New" ToolTip="Add New" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEditDesig" runat="server" CssClass="fa fa-edit fa-2x fa-color-Edit space" CommandName="EDIT" CommandArgument='<%#Eval("ID") %>'
                                        Text="" ToolTip="Edit" />
                                    <asp:LinkButton ID="lbtnDeleteDesig" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete space" CommandName="DELETE" CommandArgument='<%#Eval("ID") %>' Text="" ToolTip="Delete" />
                                </ItemTemplate>
                                <ItemStyle Width="15%" />
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label10" runat="server" Text="Code"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCode" runat="server" TabIndex="2" AutoComplete="Off" 
                                        CssClass="bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="Name"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%#Eval("Name") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label8" runat="server" Text="Active"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" runat="server" Text='<%#Eval("Status").ToString()=="1"?"Y":"N" %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdDesignations" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

