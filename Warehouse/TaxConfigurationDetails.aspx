﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" Culture="en-gb" AutoEventWireup="true" Inherits="TaxConfigurationDetails" Codebehind="TaxConfigurationDetails.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="pull-right">
                        <br />
                        <asp:LinkButton ID="lbtnSave" runat="server" ValidationGroup="FinalSave" OnClick="lbtnSave_Click"><i class="fa fa-check  fa-2x text-success"></i></asp:LinkButton>
                        <asp:LinkButton ID="lbtnCancel" CausesValidation="false" runat="server" OnClick="lbtnCancel_Click"><i class="fa fa-close fa-2x"></i></asp:LinkButton>
                    </div>
                    <h3>Tax Type  
                    </h3>
                    <hr />
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label ID="lblTaxCode" runat="server" Text="Tax Code" CssClass="bold"></asp:Label>
                            <asp:Label ID="lblTaxCodeStar" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="Rfv_txtTaxCode" runat="server" ControlToValidate="txtTaxCode"
                                Display="Dynamic" ErrorMessage="(Required Field)" ValidationGroup="SaveTax" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <div class="input-group">
                                <asp:TextBox ID="txtTaxCode" TabIndex="1" placeholder="" runat="server" AutoCompleteType="None" autocomplete="Off" ValidationGroup="SaveTax"
                                    CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblTaxName" runat="server" Text="Tax Name" CssClass="bold"></asp:Label>
                            <asp:Label ID="lblTaxNameStar" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="Rfv_txtTaxName" runat="server" ControlToValidate="txtTaxName"
                                Display="Dynamic" ErrorMessage="(Required Field)" ValidationGroup="SaveTax" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <div class="input-group">
                                <asp:TextBox ID="txtTaxName" TabIndex="2" runat="server" AutoCompleteType="None" autocomplete="Off" ValidationGroup="SaveTax"
                                    CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblType" runat="server" Text="Input/Output" CssClass="bold"></asp:Label>
                            <div class="material-switch pull-right">
                                <asp:CheckBox runat="server" ID="chkTaxType" TabIndex="3" />
                                <label for='<%=chkTaxType.ClientID %>' class="label-primary"></label>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblFromDt" runat="server" Text="From Date" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtFromDate" TabIndex="4" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="txtFromDt_MaskedEditExtender" runat="server"
                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFromDate">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CE_txtFromDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtFromDt"
                                TargetControlID="txtFromDate">
                            </asp:CalendarExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtToDate" TabIndex="5" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="txtToDt_MaskedEditExtender" runat="server"
                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtToDate">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CE_txtToDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtToDt"
                                TargetControlID="txtToDate">
                            </asp:CalendarExtender>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="Label10" runat="server" Text="Active/InActive" CssClass="bold"></asp:Label>
                            <div class="material-switch pull-right">
                                <asp:CheckBox runat="server" ID="chkActive" Checked="true" />
                                <label for='<%=chkActive.ClientID %>' class="label-primary "></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <%-- <asp:UpdatePanel ID="UpdatePnlTaxConfigDetails" runat="server">
                <ContentTemplate>--%>
            <div class="panel">
                <div class="panel-body">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6" style="padding-left:0px">
                                <div class="pull-left">
                                    <div class="input-group" style="padding:0px 250px 10px 0px">
                                        <span class="input-group-addon"> Tax Configuration Base Price</span>
                                        <asp:TextBox ID="txtBasePrice" Text="100" CssClass="form-control input-sm" runat="server" AutoPostBack="true" OnTextChanged="txtBasePrice_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3" >
                            </div>
                            <div class="col-lg-3" style="padding-right:0px">
                                <div class="pull-right" >
                                    <span class="input-group-addon"> <%--["FinalValue" code is mandtory for final calculation]--%> Example:- CST = (BasePrice) * 5.5 * 0.2 </span>
                                </div>
                            </div>
                        </div>
                        <%--  <hr />--%>
                        <div class="row">
                            <asp:GridView ID="grdTaxConfigDetails" runat="server" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found" OnRowDataBound="grdTaxConfigDetails_RowDataBound"
                                AutoGenerateColumns="False" OnPreRender="grdTaxConfigDetails_PreRender" OnRowCommand="grdTaxConfigDetails_RowCommand" GridLines="Horizontal" OnRowDeleting="grdTaxConfigDetails_RowDeleting"
                                CssClass="table table-striped table-hover dt-responsive text-nowrap">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="New" OnClick="lbtnAddNew_Click"
                                                Text="Add New" ToolTip="Add" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hdfID" Value="" />
                                            <asp:LinkButton ID="lbtnDelete" CausesValidation="false" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete" CommandName="DELETE" TabIndex="16"
                                                Text="" ToolTip="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="Label2" Text="Tax Code" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtTaxCode" runat="server" ControlToValidate="txtTaxType"
                                                Display="Dynamic" ErrorMessage="" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtTaxType" Text='<%#Eval("Code") %>' TabIndex="0" MaxLength="20" runat="server" OnTextChanged="txtTaxType_TextChanged" AutoCompleteType="None" AutoComplete="Off" AutoPostBack="true" ValidationGroup="FinalSave" CssClass="form-control input-sm">
                                            </asp:TextBox>
                                            <asp:RegularExpressionValidator ID="REV_txtTaxType" runat="server" ControlToValidate="txtTaxType"
                                                ErrorMessage="(Only Characters,Numbers and .)" Display="Dynamic" SetFocusOnError="true" ValidationGroup="FinalSave" ValidationExpression="^[a-zA-Z0-9]+(?:\.[a-zA-Z0-9]+)?$"></asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="Label15" Text="=" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="Label13" Text="Expression" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtTaxTypeCodeExpression" runat="server" ControlToValidate="txtExpression" ValidationGroup="FinalSave"
                                                Display="Dynamic" ErrorMessage="" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtExpression" TabIndex="0" runat="server" MaxLength="200" AutoCompleteType="None" AutoComplete="Off" AutoPostBack="true" Text='<%#Eval("Expression") %>' ValidationGroup="FinalSave"
                                                OnTextChanged="txtExpression_TextChanged"
                                                CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="hdrlblTaxValue" Text="Tax Value" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxValue" CssClass="bold" Text='<%#Eval("CurrentValue") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblRoundType" Text="Round Type" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlRoundType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRoundType_SelectedIndexChanged" CssClass="form-control input-sm">
                                                <asp:ListItem Text="No Round" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Round" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Round Up" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Round Down" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblPercentage" Text="Set Off %" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtSetOfPercent" runat="server" AutoPostBack="true" ControlToValidate="txtSetOfPercent" ValidationGroup="FinalSave"
                                                Display="Dynamic" ErrorMessage="" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtSetOfPercent" OnTextChanged="txtSetOfPercent_TextChanged" TabIndex="0" runat="server" AutoCompleteType="None" AutoComplete="Off" Text='<%#Eval("SetOffPercentage") %>' MaxLength="14" ValidationGroup="FinalSave"
                                                CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblLegder" Text="Ledger Name" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                              <asp:RequiredFieldValidator ID="Rfv_txtLedger" runat="server" Enabled='<%#Convert.ToBoolean(Eval("Code").ToString()!="FinalValue"?  1 : 0)%>' AutoPostBack="true" ControlToValidate="txtLedger" ValidationGroup="FinalSave"
                                                Display="Dynamic" ErrorMessage="" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtLedger" Text='<%#Eval("Ledger.LedgerName") %>' Enabled='<%#Convert.ToBoolean(Eval("Code").ToString()!="FinalValue"?  1 : 0)%>' runat="server" MaxLength="500" OnTextChanged="txtLedger_TextChanged" AutoPostBack="true" TabIndex="2" ValidationGroup="Item" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfLedgerName" />
                                            <asp:HiddenField runat="server" ID="hdfLedgerID" />
                                            <asp:AutoCompleteExtender ID="ACEtxtLedger" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLedger">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblDocumentRequired" Text="Document Required" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" AutoPostBack="true" ID="chkDocumentReq" TabIndex="15" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
            <%-- </ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdTaxConfigDetails.ClientID%>');
        $(document).ready(function () {
            // GridUI(GridID, 50);
        })
    </script>
</asp:Content>
