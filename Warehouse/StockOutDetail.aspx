﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" Culture="en-gb" AutoEventWireup="true" Inherits="StockOutDetail" Codebehind="StockOutDetail.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = (e || window.event);
            var keyCode = (evt.keyCode || evt.which);
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (keyCode == 13) {
                    bt.click();
                    bt.disabled = true;
                    return false;
                }
            }
        }
        function labour(e, buttonLabour, buttonid, btnosd) {

            var evt = (e || window.event);
            var keyCode = (evt.keyCode || evt.which);
            var shift = 0;
            var alt = 0;
            var ctrl = 0;
            shift = evt.shiftKey;
            alt = evt.altKey;
            ctrl = evt.ctrlKey;
            var btLabour = document.getElementById(buttonLabour);
            if (btLabour) {
                if (keyCode == 76 && alt == 1) {
                    btLabour.click();
                    return false;
                }
            }
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (keyCode == 13) {
                    bt.click();
                    bt.disabled = true;
                    return false;
                }
            }
            var btosd = document.getElementById(btnosd);
            if (btosd) {
                if (keyCode == 75 && alt == 1) {
                    btosd.click();
                    return false;
                }
            }
        }



        function MSearch(sender, eventArgs) {
            var hdfID = $get('<%= hdfMono.ClientID %>');
            var MONO = $get('<%= UsrtxtMONO.ClientID %>');
            var MoNoHide = $get('<%= Usrhidetxt.ClientID %>');
            MONO.value = eventArgs.get_text();
            hdfID.value = eventArgs.get_value();
            MoNoHide.value = eventArgs.get_text();
        }

        function ClearMo() {
            var OrignalProfitCenter = $get('<%=UsrtxtMONO.ClientID %>');
            var HidenProfitCenter = $get('<%=Usrhidetxt.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.value) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=Usrhidetxt.ClientID %>').value = "";
                $get('<%=UsrtxtMONO.ClientID %>').value = "";
            }
        }

        //** GateReg

        function GateReg(sender, eventArgs) {
            var hdpartID = $get('<%= UsrhdfGate.ClientID %>');
            var lblPartname = $get('<%= HideGate.ClientID %>');
            var partycode = $get('<%= UsrtxtGate.ClientID %>');
            var lblvechical = $get('<%=txtVehicalNo.ClientID %>');
            var detail = eventArgs.get_value().split('~');
            hdpartID.value = detail[0];
            partycode.value = eventArgs.get_text();
            lblPartname.value = eventArgs.get_text();
            lblPartname.innerHTML = eventArgs.get_text();
            lblvechical.value = detail[1];
            lblvechical.innerHTML = detail[1];
        }
        function ClearGateReg() {
            var OrignalProfitCenter = $get('<%=UsrtxtGate.ClientID %>');
            var HidenProfitCenter = $get('<%=HideGate.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.value) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=UsrhdfGate.ClientID %>').value = "";
                $get('<%=UsrtxtGate.ClientID %>').value = "";

            }
        }

        function confirmalert(ask) {
            return confirm(ask);
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
        <ProgressTemplate>
            <div class="modal fade in" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-keyboard="false" style="display: block">
                <div class="modal-dialog  modal-sm">
                    <h1 style="text-align: center;">
                        <i class="fa fa-truck fa-magic fa-2x faa-passing animated"></i>
                    </h1>
                </div>
            </div>
            <div class="modal-backdrop fade in"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div class="row">
        <div class="col-lg-12">
            <div class="heading">
                <i class="fa fa-sign-out"></i>
                <asp:Label ID="lblOutward" runat="server"></asp:Label>
                &nbsp;/&nbsp;
                 <asp:Label ID="lblOutwardNoValue" runat="server" ForeColor="Maroon"></asp:Label>
                <div class="pull-right">
                    <asp:HyperLink ID="btnPrint" runat="server" ToolTip="View Report" Target="_blank" Visible="False" TabIndex="1" NavigateUrl=""><i class="fa fa-print"></i></asp:HyperLink>
                    <asp:LinkButton ID="btnhistory" runat="server" CausesValidation="False" TabIndex="1" OnClick="btnhistory_Click">
                        <i class="fa fa-history"></i>
                    </asp:LinkButton>
                    <asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click" CausesValidation="False" TabIndex="1"> <i class="fa fa-search"></i></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>

    <div class="row btnrow">
        <div class="col-lg-6">

            <ul class="nav navbar-nav navbar-left">

                <li>

                    <asp:HiddenField ID="hdfMorev" runat="server" />
                    <asp:HiddenField ID="hdfrevised" runat="server" />
                    <asp:LinkButton ID="btnDelete" runat="server" Text="Delete" CausesValidation="False" TabIndex="1" OnClick="btnDelete_Click" Visible="False" OnClientClick="return confirmalert('Do you want to Delete ?' )" />
                    <asp:ConfirmButtonExtender ID="btnDelete_ConfirmButtonExtender" runat="server" Enabled="True" TargetControlID="btnDelete">
                    </asp:ConfirmButtonExtender>
                </li>
                <li>
                    <asp:HyperLink ID="btnPrintSalesBill" runat="server" ToolTip="View Report" Target="_blank" Visible="False" TabIndex="1" NavigateUrl="" Text="Print SaleBill">            </asp:HyperLink>
                </li>
                <li></li>
                <li>
                    <asp:LinkButton ValidationGroup="Outward" AccessKey="w" CausesValidation="true" ID="btnSave" runat="server" OnClick="btnSave_Click" TabIndex="1" />
                </li>
                <li>
                    <asp:LinkButton ID="btnApproved" runat="server" Text="Approve" Visible="false" OnClick="btnApproved_Click" TabIndex="1"></asp:LinkButton>
                </li>
                <li></li>
                <li>
                    <asp:LinkButton ID="btnNew" runat="server" AccessKey="n" Text="Create New" OnClick="btnNew_Click" CausesValidation="False" TabIndex="1" />
                </li>
                <li></li>
            </ul>
        </div>
        <div class="col-lg-6">
            <div class="pull-right">
                <ul class="nav nav-pills nav-wizard">
                    <li class="active"><a href="#" data-toggle="tab">New</a><div class="nav-arrow"></div>
                    </li>
                    <li>
                        <div class="nav-wedge"></div>
                        <a href="#" data-toggle="tab">Pending Approval</a><div class="nav-arrow"></div>
                    </li>
                    <li>
                        <div class="nav-wedge"></div>
                        <a href="#" data-toggle="tab">Available</a><div class="nav-arrow"></div>
                    </li>
                    <li>
                        <div class="nav-wedge"></div>
                        <a href="#" data-toggle="tab">Partially Available</a><div class="nav-arrow"></div>
                    </li>
                    <li>
                        <div class="nav-wedge"></div>
                        <a href="#" data-toggle="tab">Done</a></li>

                </ul>
            </div>
        </div>

    </div>

    <hr />
    <div class="panel">
        <div class="panel-body">
            <div class="row">


                <div class="col-lg-2">
                    <asp:Label ID="lblMaterialOrderID" runat="server" CssClass="bold"></asp:Label>
                    <asp:Label ID="Label2" runat="server" Text="*" CssClass="text-danger"></asp:Label>
                    <asp:TextBox ID="UsrtxtMONO" runat="server" onblur="ClearMo()" CssClass="form-control input-sm" TabIndex="1"
                        OnTextChanged="UsrtxtMONO_TextChanged" AutoPostBack="True">
                    </asp:TextBox>
                    <asp:AutoCompleteExtender ID="UsrtxtMONO_AutoCompleteExtender" runat="server" CompletionInterval="1"
                        CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                        CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionSetCount="10"
                        ContextKey="0" DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                        MinimumPrefixLength="1" ServiceMethod="MaterialOrderSearch" OnClientItemSelected="MSearch"
                        ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                        TargetControlID="UsrtxtMONO" UseContextKey="true">
                    </asp:AutoCompleteExtender>
                    <div style="display: none">
                        <asp:HiddenField ID="hdfMono" runat="server" />
                        <asp:TextBox ID="Usrhidetxt" runat="server"></asp:TextBox>
                    </div>
                    <asp:RequiredFieldValidator ValidationGroup="Outward" ID="RequiredFieldValidator3" Display="Dynamic" SetFocusOnError="true"
                        runat="server" ControlToValidate="UsrtxtMONO" ErrorMessage="" CssClass="bold text-danger">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="col-lg-2">
                    <asp:Label ID="lblGateRegisterID" runat="server" CssClass="bold"></asp:Label>
                    <asp:Label ID="Label1" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                    <asp:RequiredFieldValidator ID="RFVvchicalno" runat="server" ControlToValidate="txtVehicalNo"
                        Display="Dynamic" SetFocusOnError="true" CssClass="text-danger bold"
                        ErrorMessage="" ValidationGroup="Outward"></asp:RequiredFieldValidator>
                    <asp:TextBox ID="UsrtxtGate" TabIndex="1" CssClass="form-control input-sm" runat="server" onblur="ClearGateReg()"></asp:TextBox>
                    <asp:AutoCompleteExtender ID="UsrtxtGate_AutoCompleteExtender" runat="server" CompletionInterval="1"
                        CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                        CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionSetCount="10"
                        DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                        MinimumPrefixLength="1" OnClientItemSelected="GateReg" ServiceMethod="GateEntrySearch"
                        ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                        UseContextKey="true" ContextKey="0" TargetControlID="UsrtxtGate">
                    </asp:AutoCompleteExtender>
                    <div style="display: none">
                        <asp:HiddenField ID="UsrhdfGate" runat="server" />
                        <asp:TextBox ID="HideGate" runat="server"></asp:TextBox>
                    </div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" CssClass="text-danger bold" SetFocusOnError="true"
                        ControlToValidate="UsrtxtGate" ErrorMessage="" ValidationGroup="Outward">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="col-lg-2">
                    <asp:Label ID="lblVehicalNo" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="txtVehicalNo" TabIndex="1" CssClass="form-control input-sm" runat="server" MaxLength="100">
                    </asp:TextBox>
                </div>
                <div class="col-lg-2">
                    <asp:Label ID="lblCnm" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="lblChNo" runat="server" CssClass="form-control input-sm" Enabled="false" TabIndex="1"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    <asp:Label ID="lblPartnerGroupIDFrom" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="lblPartnerGroupIDFromvalue" runat="server" CssClass="form-control input-sm" TabIndex="1"
                        Enabled="false">
                    </asp:TextBox>
                    <asp:Label ID="lblPartnerGroupIDFromID" Visible="false" runat="server" CssClass="bold"
                        Font-Bold="True"></asp:Label>
                </div>
                <div class="col-lg-2">
                    <asp:Label ID="lblPartnerIDFrom" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="lblPartnerIDFromValue" runat="server" Enabled="false" CssClass="form-control input-sm" TabIndex="1"></asp:TextBox>
                    <asp:Label ID="lblPartnerIDFromID" Visible="false" runat="server" CssClass="bold"
                        Font-Bold="True"></asp:Label>
                </div>
                <div class="col-lg-2">
                    <asp:Label ID="lblorderby" runat="server" CssClass="bold"></asp:Label>
                    <br />
                    <asp:Label ID="lblorderbyName" CssClass="form-control input-sm" runat="server"></asp:Label>
                </div>

                <div class="col-lg-2">
                    <asp:Label ID="lblOutwardDate" runat="server" CssClass="bold"></asp:Label>
                    <asp:Label ID="Label6" runat="server" Text="*" CssClass="text-danger"></asp:Label>
                    <asp:TextBox ID="txtOutwardDate" ValidationGroup="Outward" runat="server" CssClass="form-control input-sm"
                        TabIndex="1">
                    </asp:TextBox>
                    <asp:MaskedEditExtender ID="txtOutwardDate_MaskedEditExtender" runat="server" Mask="99/99/9999"
                        MaskType="Date" Enabled="True" ClearMaskOnLostFocus="true" TargetControlID="txtOutwardDate">
                    </asp:MaskedEditExtender>
                    <asp:CalendarExtender ID="caltxtOutwardDate" TargetControlID="txtOutwardDate" Format="dd/MM/yyyy"
                        PopupButtonID="txtOutwardDate" runat="server">
                    </asp:CalendarExtender>
                    <asp:RequiredFieldValidator ValidationGroup="Outward" ID="RFVFromdate" runat="server"
                        ControlToValidate="txtOutwardDate" Display="Dynamic" ErrorMessage="Invaild Date" CssClass="text-danger">
                    </asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="Rangeval" runat="server" Type="Date" ErrorMessage="Invalid Date"
                        ControlToValidate="txtOutwardDate" ValidationGroup="Outward" Display="Dynamic" CssClass="text-danger">
                    </asp:RangeValidator>
                    <asp:RangeValidator ID="Rangback" runat="server" Type="Date" ErrorMessage="Invalid Date"
                        SetFocusOnError="true" ControlToValidate="txtOutwardDate" ValidationGroup="Outward" CssClass="text-danger"
                        Display="Dynamic">
                    </asp:RangeValidator>
                </div>
                <div class="col-lg-2">
                    <asp:Label ID="lblActualOutwardDate" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="txtActualOutwardDate" ValidationGroup="Outward" runat="server" CssClass="form-control input-sm"
                        TabIndex="1">
                    </asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender2" TargetControlID="txtActualOutwardDate"
                        Format="dd/MM/yyyy" PopupButtonID="txtActualOutwardDate" runat="server">
                    </asp:CalendarExtender>
                    <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99/99/9999"
                        MaskType="Date" Enabled="True" ClearMaskOnLostFocus="true" TargetControlID="txtActualOutwardDate">
                    </asp:MaskedEditExtender>

                    <asp:CompareValidator ID="cmpdeliverdate" runat="server" ErrorMessage="Invalid Date" SetFocusOnError="true"
                        ControlToValidate="txtActualOutwardDate" Type="Date" Operator="GreaterThanEqual" CssClass="text-danger"
                        ControlToCompare="txtOutwardDate" Display="Dynamic">
                    </asp:CompareValidator>
                </div>
                <div class="col-lg-2">
                    <asp:Label ID="lblDeliveredTo" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="lblDeliveredToValue" runat="server" Enabled="false" CssClass="form-control input-sm"></asp:TextBox>
                </div>
                <div class="col-lg-2">
                    <asp:Label ID="lblDeliveredOn" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="lblDeliveredOnValue" runat="server" CssClass="form-control input-sm" TabIndex="1" Enabled="false"></asp:TextBox>
                </div>

                <div class="col-lg-2">
                    <asp:Label ID="lbloutwardtype" runat="server" CssClass="bold" Text="OutwardType"></asp:Label>
                    <asp:DropDownList ID="ddlOutwardType" runat="server" CssClass="form-control input-sm">
                        <asp:ListItem Text="Normal" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Consignee" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Not Found" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </div>

                <div class="col-lg-2">
                    <asp:Label ID="lblOutwardStartedOn" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="txtOutwardStartedOn" ValidationGroup="Outward" runat="server" CssClass="form-control input-sm"
                        TabIndex="1" AutoPostBack="True" OnTextChanged="txtOutwardStartedOn_TextChanged1">
                    </asp:TextBox>
                    <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99/99/9999"
                        MaskType="Date" Enabled="True" ClearMaskOnLostFocus="true" TargetControlID="txtOutwardStartedOn">
                    </asp:MaskedEditExtender>
                    <%--<asp:CompareValidator ID="cmpstartedon" runat="server" ErrorMessage="Invalid Date" SetFocusOnError="true" ValidationGroup="Outward"
                                ControlToValidate="txtOutwardStartedOn" Type="Date" Display="Dynamic" Operator="GreaterThanEqual"
                                ControlToCompare="txtOutwardDate">
                            </asp:CompareValidator>--%>
                    <asp:CalendarExtender ID="CalendarExtender3" TargetControlID="txtOutwardStartedOn"
                        Format="dd/MM/yyyy" PopupButtonID="txtOutwardStartedOn" runat="server">
                    </asp:CalendarExtender>
                </div>
                <div class="col-lg-1">
                    <asp:Label ID="lblStartedOnHH" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="txtStartedOnHH" ValidationGroup="Outward" MaxLength="2" runat="server"
                        CssClass="form-control input-sm" TabIndex="1">
                    </asp:TextBox>
                    <asp:MaskedEditExtender ID="metxtStartedOnHH" Mask="99" MaskType="Number" ClearMaskOnLostFocus="true"
                        TargetControlID="txtStartedOnHH" runat="server">
                    </asp:MaskedEditExtender>
                    <asp:MaskedEditValidator ID="mevtxtStartedOnHH" ControlToValidate="txtStartedOnHH"
                        ValidationGroup="Outward" ControlExtender="metxtStartedOnHH" MaximumValue="23"
                        MinimumValue="0" InvalidValueMessage="Please Enter Valid Hour" MaximumValueMessage="Please Enter Valid Hour"
                        InvalidValueBlurredMessage="Please Enter Valid Hour" runat="server" CssClass="text-danger"
                        Enabled="false" SetFocusOnError="true" Display="Dynamic">
                    </asp:MaskedEditValidator>
                    <asp:RequiredFieldValidator ValidationGroup="Outward" ID="rfvtxtStartedOnHH" runat="server" Display="Dynamic"
                        ControlToValidate="txtStartedOnHH" CssClass="text-danger" ErrorMessage="*" Enabled="false"
                        SetFocusOnError="true">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="col-lg-1">
                    <asp:Label ID="lblStartedOnMM" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="txtStartedOnMM" ValidationGroup="Outward" runat="server"
                        CssClass="form-control input-sm" TabIndex="1">
                    </asp:TextBox>
                    <asp:MaskedEditExtender ID="metxtStartedOnMM" Mask="99" MaskType="Number" ClearMaskOnLostFocus="true"
                        TargetControlID="txtStartedOnMM" runat="server">
                    </asp:MaskedEditExtender>
                    <asp:MaskedEditValidator ValidationGroup="Outward" ID="mevtxtStartedOnMM" ControlToValidate="txtStartedOnMM"
                        ControlExtender="metxtStartedOnMM" MaximumValue="59" MinimumValue="0" InvalidValueMessage="Please Enter Valid Minutes"
                        MaximumValueMessage="Please Enter Valid Minutes" InvalidValueBlurredMessage="Please Enter Valid Minutes"
                        runat="server" CssClass="text-danger" Enabled="false" Display="Dynamic">
                    </asp:MaskedEditValidator>
                    <asp:RequiredFieldValidator ValidationGroup="Outward" ID="rfvtxtStartedOnMM" runat="server" Display="Dynamic"
                        ControlToValidate="txtStartedOnMM" CssClass="text-danger" ErrorMessage="*" Enabled="false">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="col-lg-2">
                    <asp:Label ID="lblOutwardEndedOn" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="txtOutwardEndedOn" ValidationGroup="Outward" runat="server" CssClass="form-control input-sm"
                        TabIndex="1" AutoPostBack="True" OnTextChanged="txtOutwardEndedOn_TextChanged1">
                    </asp:TextBox>
                    <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Mask="99/99/9999"
                        MaskType="Date" Enabled="True" ClearMaskOnLostFocus="true" TargetControlID="txtOutwardEndedOn">
                    </asp:MaskedEditExtender>
                    <%-- <asp:CompareValidator ID="cmpendedon" runat="server" ErrorMessage="Invalid Date" SetFocusOnError="true"
                                ControlToValidate="txtOutwardEndedOn" Type="Date" Display="Dynamic" Operator="GreaterThanEqual" ValidationGroup="Outward"
                                ControlToCompare="txtOutwardDate">
                            </asp:CompareValidator>--%>
                    <asp:CalendarExtender ID="CalendarExtender4" TargetControlID="txtOutwardEndedOn"
                        Format="dd/MM/yyyy" PopupButtonID="txtOutwardEndedOn" runat="server">
                    </asp:CalendarExtender>
                </div>
                <div class="col-lg-1">
                    <asp:Label ID="lblEndedOnHH" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="txtEndedOnHR" MaxLength="2" runat="server" CssClass="form-control input-sm" TabIndex="1" ValidationGroup="Outward"></asp:TextBox>
                    <asp:MaskedEditExtender ID="metxtEndedOnHR" Mask="99" MaskType="Number" ClearMaskOnLostFocus="true" TargetControlID="txtEndedOnHR" runat="server">
                    </asp:MaskedEditExtender>
                    <asp:MaskedEditValidator ID="mevtxtEndedOnHR" ControlToValidate="txtEndedOnHR"
                        ValidationGroup="Outward" ControlExtender="metxtEndedOnHR" MaximumValue="59"
                        MinimumValue="0" InvalidValueMessage="Please Enter Valid Minutes " MaximumValueMessage="Please Enter Valid Hour"
                        InvalidValueBlurredMessage="Please Enter Valid Hour" runat="server" CssClass="text-danger"
                        Enabled="false" SetFocusOnError="true" Display="Dynamic">
                    </asp:MaskedEditValidator>
                    <asp:RequiredFieldValidator ValidationGroup="Outward" ID="rfvmetxtEndedOnHR" runat="server" Display="Dynamic"
                        ControlToValidate="txtEndedOnHR" CssClass="text-danger" ErrorMessage="*" Enabled="False"
                        SetFocusOnError="true">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="col-lg-1">
                    <asp:Label ID="lblEndedonMM" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="txtEndedOnMM" runat="server" CssClass="form-control input-sm" TabIndex="1" MaxLength="2"></asp:TextBox>
                    <asp:MaskedEditExtender ID="metxtEndedOnMM" Mask="99" MaskType="Number" ClearMaskOnLostFocus="true" TargetControlID="txtEndedOnMM" runat="server">
                    </asp:MaskedEditExtender>
                    <asp:MaskedEditValidator ID="mevtxtEndedOnMM" ControlToValidate="txtEndedOnMM"
                        ValidationGroup="Outward" ControlExtender="metxtEndedOnMM" MaximumValue="59"
                        MinimumValue="0" InvalidValueMessage="Please Enter Valid Minutes " MaximumValueMessage="Please Enter Valid Minutes"
                        InvalidValueBlurredMessage="Please Enter Valid Minutes" runat="server" CssClass="text-danger"
                        Enabled="false" SetFocusOnError="true" Display="Dynamic">
                    </asp:MaskedEditValidator>
                    <asp:RequiredFieldValidator ID="rfvtxtEndedOnMM" runat="server" ValidationGroup="Outward" Display="Dynamic"
                        ControlToValidate="txtEndedOnMM" CssClass="text-danger" ErrorMessage="*" Enabled="False">
                    </asp:RequiredFieldValidator>
                </div>
                <div class="col-lg-2">
                    <asp:Label ID="lblChallanNo" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="txtChallanNo" ValidationGroup="Inward" runat="server" CssClass="form-control input-sm" TabIndex="1"></asp:TextBox>
                </div>
                <div class="col-lg-12">
                    <asp:Label ID="lblRemarks" runat="server" CssClass="bold"></asp:Label>
                    <asp:TextBox ID="txtRemarks" CssClass="form-control input-sm" runat="server" MaxLength="500"
                        TabIndex="1">
                    </asp:TextBox>
                </div>
                <div class="col-lg-4">
                    <asp:HiddenField ID="hdfrowid" runat="server" />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Outward date cannot be less than Order date"
                        ControlToCompare="txtDateCompare" ControlToValidate="txtOutwardDate" Type="Date" SetFocusOnError="true" CssClass="bold text-danger"
                        Display="Dynamic" Operator="GreaterThanEqual" ValidationGroup="Outward">
                    </asp:CompareValidator>
                    <div style="height: 0px; width: 0px; visibility: hidden">
                        <asp:TextBox ID="txtDateCompare" runat="server"></asp:TextBox>
                    </div>
                    <asp:HiddenField ID="hdfOutward" runat="server" />
                </div>

            </div>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="col-lg-12" style="overflow: auto">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="row">
                        <div class="col-lg-12" style="overflow: auto; max-height: 300px;">
                            <asp:GridView ID="grdOutwardDetail" runat="server" AutoGenerateColumns="False" EmptyDataText="No Record Found"
                                HorizontalAlign="left" OnRowCommand="grdOutwardDetail_RowCommand"
                                CssClass="table table-hover text-nowrap table-striped"
                                ShowFooter="True">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDeleteItem" ForeColor="Maroon" OnClientClick="return confirmalert('Do you want to Delete ?' )" runat="server" CommandName="Del"
                                                Visible='<%# Convert.ToString(Eval("Outward.Status"))=="1"?true:false %>' CommandArgument='<%# Eval("ID") %>' CausesValidation="False" TabIndex="1">
                                                <i class="fa fa-close fa-2x"></i>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label runat="server" ID="lblgrdsrno" Text="No."></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblouno" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblLotNo" Text="Lot No" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblLotNoInternal" runat="server" Text='<%# Eval("InwardDetail.LotInternal") %>' ToolTip='<%# Eval("InwardDetail.LotInternal") %>' />
                                            <asp:Label ID="lblLotVersionWise" Text='<%# Eval("InwardDetail.LotVersion") %>' ToolTip='<%# Eval("InwardDetail.LotVersion") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblItem" Text="Item Name" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRowIndex" runat="server" Text="<%#Container.DataItemIndex %>"
                                                Visible="false" />
                                            <asp:Label ID="lblMaterialOrderDetailID" runat="server" Text='<%#Eval("ID") %>'
                                                Visible="false" />
                                            <asp:Label ID="lblOutwardDetailID" runat="server"
                                                Visible="false" />
                                            <asp:Label ID="lblStorageItemID" Text='<%#Eval("InwardDetail.Item.Name") %>'
                                                ToolTip='<%#Eval("InwardDetail.Item.Name") %>' runat="server" />
                                            <asp:Label ID="lblInwardDetailID" runat="server" Text='<%#Eval("InwardDetail_ID") %>'
                                                Visible="false" />
                                            <asp:Label ID="lblStorageItem" runat="server" Text='<%#Eval("InwardDetail.Item_ID") %>'
                                                Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblItemMark" Text="Item Mark" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStorageItemMark" runat="server" Text='<%# Eval("InwardDetail.ItemMark") %>'
                                                ToolTip='<%# Eval("InwardDetail.ItemMark") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblVakkal" Text="Vakkal" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblLotNoCustomer" runat="server" Text='<%# Eval("InwardDetail.LotCustomer") %>'
                                                ToolTip='<%# Eval("InwardDetail.LotCustomer") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblhdCount" Text="Variety/Count" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVariety" Text='<%# Eval("InwardDetail.Variety.Name") %>' ToolTip='<%# Eval("InwardDetail.Variety.Name") %>' runat="server" />
                                            <asp:HiddenField ID="hdfVarietyID" Value='<%# Eval("InwardDetail.Variety_ID") %>' runat="server" />
                                            <br />
                                            <asp:Label ID="lblCount" ForeColor="Maroon" Text='<%# Eval("InwardDetail.Count") %>' ToolTip='<%# Eval("InwardDetail.Count") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblhdBrand" Text="Brand/Origin" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBrand" Text='<%# Eval("InwardDetail.Brand.Name") %>' ToolTip='<%# Eval("InwardDetail.Brand.Name") %>' runat="server" />
                                            <asp:HiddenField ID="hdfBrandID" Value='<%# Eval("InwardDetail.Brand_ID") %>' runat="server" />
                                            <br />
                                            <asp:Label ID="lblorigin" ForeColor="Maroon" Text='<%# Eval("InwardDetail.Origin.Name") %>' ToolTip='<%# Eval("InwardDetail.Origin.Name") %>' runat="server" />
                                            <asp:HiddenField ID="hdfOriginID" Value='<%# Eval("InwardDetail.Origin_ID") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblInwardInfo" Text="Inward Info" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblRDate" runat="server" Text='<%# Convert.ToDateTime(Eval("InwardDetail.Inward.InwardDate")).ToString("dd/MM/yyyy") %>' />
                                            <br />
                                            <asp:Label ID="lblRQty" runat="server" ForeColor="Maroon" Text='<%# Eval("InwardDetail.InwardQuantity") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdNetqty" Text="Stock Qty" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetQty" runat="server" Text='<%# Eval("MaterialOrderDetail.StockQuantity") %>' ToolTip='<%# Eval("MaterialOrderDetail.StockQuantity") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblItemQty" Text="Order Qty" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStorageItemQuantityMO" runat="server" Text='<%# Eval("MaterialOrderDetail.OrderQuantity") %>' ToolTip='<%# Eval("MaterialOrderDetail.OrderQuantity") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblBalanceOutQty" Text="Balance Qty" runat="server" />
                                        </HeaderTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBalanceOutwardQty" runat="server" Text='<%# Convert.ToInt32( Eval("MaterialOrderDetail.BalOrderQuantity")) + Convert.ToInt32(  Eval("Quantity")) %>' ToolTip='<%# Convert.ToInt32( Eval("MaterialOrderDetail.BalOrderQuantity"))+Convert.ToInt32(  Eval("Quantity")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblOutwardQty" Text="Out Quantity" runat="server" />
                                        </HeaderTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalQty" runat="server"></asp:Label>
                                        </FooterTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtOutwardQty" TabIndex="1" runat="server" CssClass="form-control input-sm"
                                                Text='<%# Eval("Quantity") %>' ToolTip='<%# Eval("Quantity") %>' />
                                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtOutwardQty"
                                                FilterType="Numbers,Custom" ValidChars="." runat="server">
                                            </asp:FilteredTextBoxExtender>
                                            <asp:CompareValidator SetFocusOnError="true" ID="CPVOutwardQty" Operator="LessThanEqual"
                                                runat="server" ErrorMessage="" ControlToValidate="txtOutwardQty" Type="Double"
                                                ValidationGroup="Outward" ValueToCompare='<%# Convert.ToInt32( Eval("MaterialOrderDetail.BalOrderQuantity")) + Convert.ToInt32(Eval("Quantity"))%>'
                                                Display="Dynamic"></asp:CompareValidator>
                                            <asp:CompareValidator SetFocusOnError="true" ID="CompareValidator2" Operator="LessThanEqual"
                                                runat="server" ErrorMessage="" ControlToValidate="txtOutwardQty" Type="Double"
                                                ValidationGroup="Outward" ValueToCompare='<%# Convert.ToInt32( Eval("MaterialOrderDetail.BalOrderQuantity")) + Convert.ToInt32(Eval("Quantity")) %>'
                                                Display="Dynamic"></asp:CompareValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:Label ID="lblItemRate" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtItemRate" TabIndex="1" Width="50px" runat="server"  CssClass="form-control input-sm" 
                                                    Text='<%# Eval("ItemRate") %>' ToolTip='<%# Eval("ItemRate") %>' Visible="true" />
                                                <asp:FilteredTextBoxExtender ID="txtItemRateFilterTextbox" TargetControlID="txtItemRate"
                                                    FilterType="Numbers" runat="server">
                                                </asp:FilteredTextBoxExtender>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblScheme" Text="Scheme/Location" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSchemeID" runat="server" Text='<%# Eval("InwardDetail.StorageSchemeRate_ID") %>' Visible="false" />
                                            <asp:Label ID="lblSchemeCode" Text='<%# Eval("InwardDetail.StorageSchemeRate.StorageScheme.Code") %>' ToolTip='<%# Eval("InwardDetail.StorageSchemeRate.StorageScheme.Code") %>'
                                                runat="server" /><br />
                                            <asp:Label ID="lblStorageLocationID" Visible="false" runat="server" Text='<%# Eval("InwardDetail.StorageLocation_ID") %>' />
                                            <asp:Label ID="lblStorageLocationName" ForeColor="Maroon" runat="server" Text='<%# Eval("InwardDetail.StorageLocation.Name") %>' ToolTip='<%# Eval("InwardDetail.StorageLocation.Name") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="pull-right">
                <div class="btn-group">
                    <asp:LinkButton ID="btnAdd" runat="server" Text="" CssClass="btn btn-primary btn-sm" Visible="false" CausesValidation="False" TabIndex="1" OnClick="btnAdd_Click" ValidationGroup="Outward" />
                </div>
            </div>
        </div>
    </div>
    <div runat="server" id="bottem" class="row" style="display: none">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-3">

            <asp:Label ID="lbleduser" Text="" runat="server" />
            <asp:Label ID="lbledbar" runat="server" Text=" |"></asp:Label>
            <asp:Label ID="lbleddate" Text="" runat="server" />

        </div>
        <div class="col-lg-3">
        </div>
        <div class="col-lg-3">
            <asp:Label ID="lblshowshotcut" runat="server" CssClass="text-danger" />
            <asp:CollapsiblePanelExtender ID="colshotcut" runat="server"
                CollapseControlID="lblshowshotcut" ExpandControlID="lblshowshotcut"
                ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" Collapsed="true"
                TargetControlID="Pnlcol">
            </asp:CollapsiblePanelExtender>
        </div>
    </div>

    <asp:Panel ID="Pnlcol" runat="server" Visible="false">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <asp:Label ID="Lblsh" runat="server" Text="Alt+W : Save"></asp:Label>
                    <asp:Label ID="lblb" runat="server" Text=" |"></asp:Label>
                    <asp:Label ID="lblal" runat="server" Text="Enter : Save "></asp:Label>
                    <asp:Label ID="lblosdbr" runat="server" Text=" |"></asp:Label>
                    <asp:Label ID="lblosd" runat="server" Text="Alt+K(On Outward qty) : Open OSD Popup"></asp:Label>
                </div>
            </div>
        </div>
    </asp:Panel>

    <div class="modal fade" id="modalhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                <div class="modal-header">
                    <h1 class="modal-title">
                        <asp:Label ID="lbledit" runat="server"></asp:Label>
                    </h1>
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>--%>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Pnlhistory" runat="server">
                                <div class="row">
                                    <div class="col-lg-1">
                                        <asp:HiddenField ID="hdfhistory" runat="server" />
                                        <asp:Button ID="btncancelpop" runat="server" CssClass="btn btn-primary" data-dismiss="modal" Visible="false" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <asp:Label ID="lblcron" runat="server" CssClass="bold"></asp:Label>
                                        <asp:Label ID="lblcruser" Text="" runat="server" />
                                        <asp:Label ID="lblcrbar" runat="server" Text=" |"></asp:Label>
                                        <asp:Label ID="lblcrdate" Text="" runat="server" />
                                    </div>
                                </div>
                                <div class="newline"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <asp:Label ID="lbldelon" runat="server" CssClass="bold"></asp:Label>
                                        <asp:Label ID="lbldluser" Text="" runat="server" />
                                        <asp:Label ID="lbldelbar" runat="server" Text=" |"></asp:Label>
                                        <asp:Label ID="lbldldate" Text="" runat="server" />
                                    </div>
                                </div>
                                <div class="newline"></div>
                                <div class="row">
                                    <div class="col-lg-12" style="text-align: center">
                                        <asp:Label ID="lbledon" runat="server" CssClass="bold"></asp:Label>
                                    </div>
                                </div>
                                <div class="newline"></div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <asp:GridView ID="grdedit" runat="server" AutoGenerateColumns="false"
                                            OnPreRender="grdedit_OnPreRender"
                                            CssClass="table table-hover text-nowrap">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblUser" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbleduser" Text='<%# Eval("UserInfo.UserCode") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblUserFullName" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserFullName" Text='<%# Eval("UserInfo.UserFullName") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblDate" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbleddate" Text='<%# Eval("CreateDate") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>

                            <asp:AsyncPostBackTrigger ControlID="btnhistory" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



</asp:Content>
