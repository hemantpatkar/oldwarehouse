﻿using AppObjects;
using AppUtility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SampleDataDetail : BigSunPage
{
    #region Variables
    static int focus;
    public string str;
    public Int16 overide;
    public Int16 outpopflag = 0;
    public Int16 Instpopflag = 0;
    static int flag = 0;
    static int temp = 0;
    private int hisflag = 0;
    public int old;
    List<int> InwardDetail_IDs;
    static int stockflag = 0;
    AppSamplingDetailColl objAppSamplingDetailColl;
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        DecideLanguage(Convert.ToString(Session["LanguageCode"]));
        Page.Title = "Sample Detail";

        objAppSamplingDetailColl = new AppSamplingDetailColl(intRequestingUserID);
        if (!IsPostBack)
        {
            Session.Add(TableConstants.SampleSession, objAppSamplingDetailColl);

            string ID = Convert.ToString(Request.QueryString["SampleID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                hdfSamplingID.Value = ID;
                int TranshipmentID = new AppUtility.AppConvert(ID);
                PopulateData(TranshipmentID);
            }
            else
            {
                txtLotSearch.Focus();
                searchlot.Attributes.Add("style", "display:");
            }
        }
    }
    #endregion

    #region Button Event
    protected void btnDeleteInwardDetail_Click(object sender, EventArgs e)
    {

    }
    protected void btnhistory_Click(object sender, EventArgs e)
    {

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("SampleDataSearch.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        objAppSamplingDetailColl = (AppSamplingDetailColl)Session[TableConstants.SampleSession];
        int i = 0;


        AppSampling objAppSampling = new AppSampling(intRequestingUserID);
        if (objAppSampling.ID == 0)
        {
            objAppSampling.SampleNo = new AppConvert("SD" + AppUtility.AutoID.GetNextNumber(1, "Invoice", System.DateTime.Now, 4, ResetCycle.Yearly));
        }
        objAppSampling.ModifiedBy = intRequestingUserID;
        objAppSampling.ModifiedOn = DateTime.Now;
        objAppSampling.Customer_Company_ID = Convert.ToInt32(hdfPartnerCodeID.Value);
        objAppSampling.Billable = 0;
        objAppSampling.SampleDate = new AppConvert(txtSampleDate.Text);
        objAppSampling.Remarks = new AppConvert(txtRemarks.Text);
        objAppSampling.VoucherType_ID = 0;
        objAppSampling.VoucherID = 0;
        objAppSampling.Status = 1;
        objAppSampling.Save();

        foreach (var AppSamplingDetail in objAppSamplingDetailColl)
        {
            TextBox StorageItemIndQuantityOpened = (TextBox)grdSamplingDetails.Rows[i].FindControl("StorageItemIndQuantityOpened");
            TextBox StorageItemIndQuantityTaken = (TextBox)grdSamplingDetails.Rows[i].FindControl("StorageItemIndQuantityTaken");
            DropDownList ddlStorageItemIndQuantityTakenUOM = (DropDownList)grdSamplingDetails.Rows[i].FindControl("ddlStorageItemIndQuantityTakenUOM");

            AppSamplingDetail.QuantityOpened = Convert.ToInt32(StorageItemIndQuantityOpened.Text);
            AppSamplingDetail.QuantityTaken = Convert.ToInt32(StorageItemIndQuantityTaken.Text);
            if (ddlStorageItemIndQuantityTakenUOM.SelectedValue != null && ddlStorageItemIndQuantityTakenUOM.SelectedValue != "" && ddlStorageItemIndQuantityTakenUOM.SelectedValue != "0")
                AppSamplingDetail.QuantityUOM = Convert.ToInt32(ddlStorageItemIndQuantityTakenUOM.SelectedValue);
            AppSamplingDetail.Sampling_ID = objAppSampling.ID;
            AppSamplingDetail.Save();
            i++;
        }
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Saved Successfully','1'" + ");", true);
    }
    protected void btnNewDetail_Click(object sender, EventArgs e)
    {
        Response.Redirect("SampleDataDetail.aspx");
    }
    #endregion

    #region Function
    public void PopulateData(int ID)
    {
        AppSampling objAppSampling = new AppObjects.AppSampling(intRequestingUserID, ID);

        lblSampleNo.Text = new AppConvert(objAppSampling.SampleNo);

        txtPartnerCode.Text = new AppConvert(objAppSampling.Customer_Company.Code + "--" + objAppSampling.Customer_Company.Name);
        hdfPartnerCodeName.Value = txtPartnerCode.Text;
        hdfPartnerCodeID.Value = new AppConvert(objAppSampling.Customer_Company_ID);

        ACEtxtPartnerCode.ContextKey = new AppConvert("3," + objAppSampling.Customer_Company.Parent_ID);
        hdfPartnerGroupCodeID.Value = new AppConvert(objAppSampling.Customer_Company.Parent_ID);
        txtPartnerGroupCode.Text = objAppSampling.Customer_Company.Parent.Code + "--" + objAppSampling.Customer_Company.Parent.Name;
        hdfPartnerGroupCodeName.Value = txtPartnerGroupCode.Text;

        txtGetRegFr.Text = new AppConvert(objAppSampling.ReceivedBy_CompanyContact.FirstName);
        hdfGetRegFrName.Value = txtGetRegFr.Text;
        hdfGetRegFrID.Value = new AppConvert(objAppSampling.ReceivedBy_CompanyContact.ID);
        txtSampleDate.Text = new AppConvert(objAppSampling.SampleDate);
        txtRemarks.Text = objAppSampling.Remarks;

        grdSamplingDetails.DataSource = objAppSampling.SamplingDetailColl.Where(x => x.Status == 1);
        grdSamplingDetails.DataBind();

        btnDeleteSample.Visible = true;
    }
    private void DecideLanguage(string languageCode)  // Method used to fill Screen Language on lables & Buttons text property
    {
        try
        {
            lblSampleDataTitle.Text = "Sample Detail";
            btnSave.Text = "Save";
            btnDeleteItem.Text = "Delete" + " " + "Item";
            btnNewDetail.Text = "New";
            btnLotSearch.Text = "Search";
            btnAddItems.Text = "Add";
            lblchkZero.Text = "Zero" + " " + "";
            lblchkFyear.Text = "Previous" + " " + "" + " " + "Year";
        }
        catch (Exception)
        {

        }
    }
    public void SearchOptimised()
    {
        int pID = 0;

        #region Get Financial Year
        //int YID = Convert.ToInt32(Session["Finyear"]);
        //DateTime startdate = Convert.ToDateTime(GenericUtills.convertIntToDate(db.mFinancialYears.FirstOrDefault(x => x.FYearID == YID).StartDate));
        //DateTime enddate = Convert.ToDateTime(GenericUtills.convertIntToDate(db.mFinancialYears.FirstOrDefault(x => x.FYearID == YID).EndDate));
        #endregion

        #region declare variables
        int PartnerGroup = Convert.ToInt32("0" + UsrhdfPartnerGroup.Value);
        int Customer = Convert.ToInt32("0" + UsrhdfPartner.Value);
        int LotNoInt = new AppConvert(txtLotSearch.Text.Trim());
        string Vakkal = new AppConvert(txtVakkalSearch.Text.Trim());
        string packingref = new AppConvert(txtpackingrefSearch.Text);
        string Mark = new AppConvert(txtMarkSearch.Text);
        string ItemName = new AppConvert(txtStorageItemName.Text);
        string SchemeName = new AppConvert(txtSchemeSearch.Text);
        string itemgroup = new AppConvert(txtItemGroup.Text);
        decimal InwardQty = new AppConvert(txtReceivedQuantitySearch.Text);
        int UTID = Convert.ToInt32(Session["UnitID"]);
        #endregion


        AppStockSearchVWColl objAppStockSearchVWColl = new AppStockSearchVWColl(intRequestingUserID);

        InwardDetail_IDs = (List<int>)Session[TableConstants.InwardDetailIDSession];
        objAppStockSearchVWColl.Search(0, Customer, 0, 0, InwardQty, LotNoInt, Vakkal, Mark, packingref, ItemName, InwardDetail_IDs);

        if (txtLotSearch.Text.ToString() != "")
        {
            if (objAppStockSearchVWColl.Count() <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Lot No Not Present" + "','2'" + ");", true);
                txtLotSearch.Focus();
                return;
            }

        }
        gvSearch.DataSource = objAppStockSearchVWColl;
        gvSearch.DataBind();

    }
    public List<Int32> GetInwardDetailIds()
    {

        List<Int32> InwardIDs = new List<Int32>();
        if (grdSamplingDetails.Rows.Count > 0)
        {
            for (int rowindex = 0; rowindex < grdSamplingDetails.Rows.Count; rowindex++)
            {
                HiddenField hdfInwardDetailID = (HiddenField)grdSamplingDetails.Rows[rowindex].FindControl("hdfInwardDetailID");
                if (new AppConvert(hdfInwardDetailID.Value) != "")
                {
                    InwardIDs.Add(Convert.ToInt32(hdfInwardDetailID.Value));
                }
            }
        }
        return InwardIDs;
    }
    public void TotalQuantityCount()
    {

        decimal count = 0;
        // for (int rowindex = 0; rowindex < grdSamplingDetails.Rows.Count; rowindex++)
        //{
        //    TextBox txtOrderQuantity = (TextBox)grdSamplingDetails.Rows[rowindex].FindControl("txtOrderQuantity");
        //    if (txtOrderQuantity.Text.Trim() != "" && txtOrderQuantity.Text != null)
        //    {
        //        count = count + Convert.ToDecimal(txtOrderQuantity.Text);
        //    }
        //    txtOrderQuantity.Focus();
        //}
        //lblTotalQuantityToShow.Text = new AppConvert(count);
    }
    #endregion

    #region Grid
    protected void grdSamplingDetails_PreRender(object sender, EventArgs e)
    {

    }
    #endregion

    protected void btnLotSearch_Click(object sender, EventArgs e)
    {

        SearchOptimised();

        if (grdSamplingDetails.Rows.Count > 0)
        {
            TotalQuantityCount();
        }
        if (gvSearch.Rows.Count > 0)
        {
            //CheckBox CheckBoxSelect = (CheckBox)gvSearch.Rows[0].FindControl("CheckBoxSelect");
            //CheckBoxSelect.Focus();
        }
        else
        {
            UsrtxtPartnerGroupCode.Focus();
        }

        if (UsrhdfPartnerGroup.Value != "0" && UsrhdfPartnerGroup.Value != "")
        {
            //int partnergrp = Convert.ToInt32(hdfUsrtxtPartnerGroupCodeID.Value);
            //UsrlblPartnerGroup.Text = db.PartnerGroups.FirstOrDefault(a => a.PartnerGroupID == partnergrp).PartnerGroupName;
            //UsrtxtPartnerGroupCode.Text = db.PartnerGroups.FirstOrDefault(a => a.PartnerGroupID == partnergrp).PartnerGroupName;
        }

        if (UsrhdfPartner.Value != "0" && UsrhdfPartner.Value != "")
        {
            //int partner = Convert.ToInt32(hdfUsrtxtPartnerCodeID.Value);
            //UsrlblPartner.Text = db.Partners.FirstOrDefault(x => x.PartnerID == partner).PartnerName;
            //UsrtxtPartnerCode.Text = db.Partners.FirstOrDefault(x => x.PartnerID == partner).PartnerName;
        }
    }

    protected void gvSearch_PreRender(object sender, EventArgs e)
    {

    }

    protected void btnAddItems_Click(object sender, EventArgs e)
    {
        AppSamplingDetailColl objAppSamplingDetailColl = (AppSamplingDetailColl)Session[TableConstants.SampleSession];

        #region To Get IDs of Inward ID from Search Grid
        int ChkFlg = 0;
        List<int> InwardDetail_IDs = (List<int>)Session[TableConstants.InwardDetailIDSession];
        if (InwardDetail_IDs == null)
            InwardDetail_IDs = new List<int>();

        string[] check = Convert.ToString(Request.Form["CheckBoxSelect"]).Split(',');
        AppStockSearchVWColl objAppStockSearchVWColl = new AppStockSearchVWColl(intRequestingUserID);
        for (int i = 0; i < check.Length; i++)
        {
            int InwardDetailID = Convert.ToInt32(check[i]);
            InwardDetail_IDs.Add(InwardDetailID);
            AppSamplingDetail objAppSamplingDetail = new AppSamplingDetail(intRequestingUserID);
            AppStockSearchVW objAppStockSearchVW = new AppStockSearchVW(intRequestingUserID);
            objAppStockSearchVWColl.Clear();
            objAppStockSearchVWColl.AddCriteria(StockSearchVW.InwardDetail_ID, Operators.Equals, InwardDetailID);
            objAppStockSearchVWColl.Search();
            objAppStockSearchVW = objAppStockSearchVWColl.FirstOrDefault();

            objAppSamplingDetail.InwardDetail_ID = objAppStockSearchVW.InwardDetail_ID;
            objAppSamplingDetail.LotInternal = objAppStockSearchVW.LotInternal;
            objAppSamplingDetail.Item_ID = objAppStockSearchVW.Item_ID;
            objAppSamplingDetail.LotVersion = objAppStockSearchVW.LotVersion;
            objAppSamplingDetail.ModifiedBy = intRequestingUserID;
            objAppSamplingDetail.ModifiedOn = System.DateTime.Now;
            //objAppSamplingDetail.OrderQuantity = 0;
            //objAppSamplingDetail.StorageSchemeRate_ID = objAppStockSearchVW.StorageSchemeRate_ID;
            //objAppSamplingDetail.InwardQuantity = objAppStockSearchVW.InwardQuantity;
            //objAppSamplingDetail.StockQuantity = new AppConvert(objAppStockSearchVW.NetQuantity);
            objAppSamplingDetail.Status = 1;
            objAppSamplingDetailColl.Add(objAppSamplingDetail);

            //int Partner = Convert.ToInt32(db.InwardDetailQuantities.FirstOrDefault(x => x.InwardDetailQtyID == InwardDetailQTYID).InwardDetail.Inward.PartnerID);
            //if (Partner != PartnerID && PartnerID != 0)
            //{
            //    lblMsg.Text = "Can not add item Different Partner";
            //    return;
            //}
            //var detail = db.InwardDetailQuantities.Where(p => p.IsActive == 1 && IDs.Contains(p.InwardDetailQtyID));
            //foreach (var item in detail)
            //{
            //    Int64 ID = Convert.ToInt64(item.InwardDetailID);
            //    // InwardDetailIDs.Add(item.InwardDetailID);
            //    if (grdSamplingDetails.Rows.Count > 1)
            //    {
            //        var dt = db.InwardDetails.FirstOrDefault(x => x.InwardDetailID == ID);
            //        if (dt != null && dt.PartnerID != PartnerID)
            //        {
            //            ChkFlg = ChkFlg + 1;
            //        }
            //    }
            //    else
            //    {
            //        PartnerID = Convert.ToInt32(db.InwardDetails.FirstOrDefault(x => x.InwardDetailID == ID).PartnerID);
            //        UserhdfPartner1.Value = new AppConvert(PartnerID);
            //    }
            //}

            #region datevalidation
            //Int64 id = Convert.ToInt64("0" + hdfInwardDetailID.Value);
            //int date = compare(id);
            //old = Convert.ToInt32("0" + ViewState["oldDate"]);
            //if (old < date)
            //{
            //    txtDateCompare.Text = GenericUtills.ConvertIntToDate(date);
            //    ViewState["oldDate"] = date;
            //}

            #endregion

        }
        if (ChkFlg != 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Duplicate Partner" + "','2'" + ");", true);
            return;
        }

        #endregion
        //ViewState["InwardIds"] = InwardDetail_IDs;
        //ShowPartyDescription(InwardDetail_IDs);

        grdSamplingDetails.DataSource = objAppSamplingDetailColl.Where(x => x.Status == 1);
        grdSamplingDetails.DataBind();

        gvSearch.DataSource = null;
        gvSearch.DataBind();

        if (grdSamplingDetails.Rows.Count > 0)
        {
            AppStockSearchVW objAppStockSearchVW = new AppStockSearchVW(intRequestingUserID);
            hdfPartnerCodeID.Value = Convert.ToString(objAppStockSearchVWColl.FirstOrDefault().Customer_Company_ID);
            txtPartnerCode.Text = objAppStockSearchVWColl.FirstOrDefault().CustomerName;
            hdfPartnerCodeName.Value = objAppStockSearchVWColl.FirstOrDefault().CustomerName;
        }

        Session.Add(TableConstants.SampleSession, objAppSamplingDetailColl);
        Session.Add(TableConstants.InwardDetailIDSession, InwardDetail_IDs);
        TotalQuantityCount();
    }

    protected void btnDeleteItem_Click(object sender, EventArgs e)
    {

    }

    protected void grdSamplingDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList ddlStorageItemIndQuantityTakenUOM = (DropDownList)e.Row.FindControl("ddlStorageItemIndQuantityTakenUOM");
            CommonFunctions.BindDropDown(intRequestingUserID, Components.UOMType, ddlStorageItemIndQuantityTakenUOM, "Name", "ID");
        }
    }

    protected void grdSamplingDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Del")
        {
            try
            {
                int selectedindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;

                HiddenField hdfSamplingDetailID = (HiddenField)grdSamplingDetails.Rows[selectedindex].FindControl("hdfSamplingDetailID");

                if (hdfSamplingDetailID.Value != "0" && hdfSamplingDetailID.Value != "")
                {
                    int SamplingDetail_ID = Convert.ToInt32(hdfSamplingDetailID.Value);

                    #region Outward Validation

                    int MaterialOrder_ID = Convert.ToInt32("0" + hdfSDID.Value);
                    AppOutwardDetailColl objAppOutwardDetailColl = new AppOutwardDetailColl(intRequestingUserID);
                    objAppOutwardDetailColl.AddCriteria(OutwardDetail.MaterialOrderDetail_ID, Operators.Equals, SamplingDetail_ID);
                    objAppOutwardDetailColl.AddCriteria(OutwardDetail.Status, Operators.Equals, (int)RecordStatus.Active);
                    objAppOutwardDetailColl.Search();

                    if (objAppOutwardDetailColl.Count() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Outward has been made. Please delete the outward first" + "','2'" + ");", true);
                        return;
                    }


                    #endregion

                    DeleteSDDetail(SamplingDetail_ID);
                    //InsertTransactionLog(21, MODID, 4, intRequestingUserID);
                }

                #region SMS Log 


                if (!string.IsNullOrEmpty(hdfSDID.Value))
                {
                    //int moid = Convert.ToInt32(hdfSDID.Value);
                    //var MoSMS = db.MaterialOrders.FirstOrDefault(a => a.MaterialOrderID == moid);
                    ////if (!db.SMSTransLogs.Any(a => a.UnitID == MoSMS.UnitID && a.TransRefID == MoSMS.MaterialOrderID && a.SMSTemplateID == 5))
                    ////{
                    //string strOB = "";
                    //if (MoSMS.OrderedBy != null)
                    //{
                    //    strOB = db.ParterContacts.FirstOrDefault(a => a.PartnerContID == MoSMS.OrderedBy).FirstName + " " + db.ParterContacts.FirstOrDefault(a => a.PartnerContID == MoSMS.OrderedBy).LastName;
                    //}
                    //else
                    //{
                    //    strOB = MoSMS.OrderName;
                    //}

                    //InsertSMSLog(6, MoSMS.MaterialOrderID, MoSMS.UnitID, MoSMS.PartnerID, TMDqty, "", MoSMS.MaterialOrderNo, MoSMS.DeliverTo, strOB); //Convert.ToInt32("0" + iwd.Totalqty)
                    // }
                }

                #endregion

                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Deleted Sucessfully" + "','2'" + ");", true);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Foreign"))
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "This Detail Is Used For Outward" + "','2'" + ");", true);
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + ex.Message + "','2'" + ");", true);
            }

            //gvDescription.DataSource = bindsearch(dt);
            //gvDescription.DataBind();

        }
    }
    public void DeleteSDDetail(int SamplingDetailID)
    {
        AppSamplingDetail objAppSamplingDetail = new AppSamplingDetail(intRequestingUserID, SamplingDetailID);
        objAppSamplingDetail.Delete();
    }

    protected void btnDeleteSample_Click(object sender, EventArgs e)
    {
        if (hdfSamplingID.Value != "" && hdfSamplingID.Value != null)
        {
            int TranshipmentID = Convert.ToInt32(hdfSamplingID.Value);
            AppSampling objAppSampling = new AppSampling(intRequestingUserID, TranshipmentID);
            objAppSampling.Delete();
        }
    }
}