﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppUtility;
using AppObjects;
public partial class M_ItemAttribute : BigSunPage
{
    AppItemAttributeColl obj;
    protected void Page_Load(object sender, EventArgs e)
    {

        obj = new AppObjects.AppItemAttributeColl(intRequestingUserID);
        Session[TableConstants.SessionItemAttribute] = obj;
        if (!IsPostBack)
        {
            BindData();
        }
    }
    #region GridViewEvents

    protected void grdItemAttribute_PreRender(object sender, EventArgs e)
    {
        if (grdItemAttribute.Rows.Count > 0)
        {
            grdItemAttribute.UseAccessibleHeader = true;
            grdItemAttribute.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdItemAttribute_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if (strCommandName == "INSERT")
        {
            HiddenField hdfItemAttributeID = grdItemAttribute.FooterRow.FindControl("hdfItemAttributeID") as HiddenField;
            TextBox txtName = grdItemAttribute.FooterRow.FindControl("txtNewName") as TextBox;
            CheckBox chkActiveItemAttribute = grdItemAttribute.FooterRow.FindControl("chkActiveItemAttribute") as CheckBox;
            CheckBox chkDefaultItemAttribute = grdItemAttribute.FooterRow.FindControl("chkDefaultItemAttribute") as CheckBox;
            SaveItemAttributeDetails(strCommandID, txtName.Text, chkActiveItemAttribute.Checked, chkDefaultItemAttribute.Checked);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Successfully Added','1'" + ");", true);
            grdItemAttribute.EditIndex = -1;
            BindData();
        }
        else if (strCommandName == "CANCELADD")
        {
            grdItemAttribute.ShowFooter = false;
            BindData();
        }
        else if (strCommandName == "CANCELEDIT")
        {
            grdItemAttribute.EditIndex = -1;
            BindData();
        }
        else if (strCommandName == "ADDNEW")
        {
            grdItemAttribute.EditIndex = -1;
            grdItemAttribute.ShowFooter = true;
            BindData();
        }
    }
    protected void grdItemAttribute_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdItemAttribute.ShowFooter = false;
        grdItemAttribute.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdItemAttribute_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HiddenField hdfID = grdItemAttribute.Rows[e.RowIndex].FindControl("hdfID") as HiddenField;
        AppItemAttribute SingleObj = new AppItemAttribute(intRequestingUserID);
        SingleObj.ID = new AppConvert(hdfID.Value);
        SingleObj.Status = new AppConvert((int)RecordStatus.Deleted);
        SingleObj.Save();

        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void grdItemAttribute_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strCommandID = new AppConvert(e.RowIndex);
        HiddenField hdfItemAttributeID = grdItemAttribute.Rows[e.RowIndex].FindControl("hdfItemAttributeID") as HiddenField;

        TextBox txtName = grdItemAttribute.Rows[e.RowIndex].FindControl("txtName") as TextBox;
        CheckBox chkIsActiveItemAttribute = grdItemAttribute.Rows[e.RowIndex].FindControl("chkIsActiveItemAttribute") as CheckBox;
        CheckBox chkIsDefaultItemAttribute = grdItemAttribute.Rows[e.RowIndex].FindControl("chkIsDefaultItemAttribute") as CheckBox;

        SaveItemAttributeDetails(hdfItemAttributeID.Value, txtName.Text, chkIsActiveItemAttribute.Checked, chkIsDefaultItemAttribute.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Successfully Updated','1'" + ");", true);
        grdItemAttribute.EditIndex = -1;
        BindData();
    }
    #endregion
    #region MiscFunctions
    public void SaveItemAttributeDetails(string Id, string Name, bool IsActive, bool IsDefault)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppItemAttribute SingleObj = new AppItemAttribute(intRequestingUserID);
        SingleObj.ID = new AppConvert(Id);
        SingleObj.Name = new AppConvert(Name);
        SingleObj.Status = (IsActive == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;

        Session.Add(TableConstants.SessionItemAttribute, obj);
        SingleObj.Save();
        BindData();
    }


    protected void BindData()
    {
        AppObjects.AppItemAttributeColl objColl = new AppItemAttributeColl(intRequestingUserID);
        if (!string.IsNullOrEmpty(txtName.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ItemAttribute.Name, AppUtility.Operators.Equals, txtName.Text, 0);
        }

        if (ddlStatus.SelectedValue != "-32768")
        {

            objColl.AddCriteria(AppObjects.ItemAttribute.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.SessionItemAttribute] = objColl;
        if (objColl != null)
        {
            grdItemAttribute.DataSource = objColl;
            grdItemAttribute.DataBind();
            if (objColl.Count <= 0)
            {
                AddEmptyRow();
            }
        }


    }
    public void AddEmptyRow()
    {
        AppItemAttributeColl collOfContacts = new AppItemAttributeColl(intRequestingUserID);
        AppItemAttribute newContact = new AppItemAttribute(intRequestingUserID);
        newContact.Name = "Add atleast one ItemAttribute";
        collOfContacts.Add(newContact);
        grdItemAttribute.DataSource = collOfContacts;
        grdItemAttribute.DataBind();
        grdItemAttribute.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdItemAttribute.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdItemAttribute.ShowFooter = true;
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}