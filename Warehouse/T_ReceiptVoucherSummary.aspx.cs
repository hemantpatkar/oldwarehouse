﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class T_ReceiptVoucherSummary : BigSunPage
{
    #region PageLoad
    AppReceiptVoucherColl objPO;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtVoucherDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            BindData();
        }
    }
    #endregion
    #region BindData   
    protected void BindData()
    {
        AppObjects.AppReceiptVoucherColl objColl = new AppReceiptVoucherColl(intRequestingUserID);
        objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ReceiptVoucher.VoucherType_ID, AppUtility.Operators.Equals, "9", 0);
        if (txtReceiptVoucherNo.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ReceiptVoucher.ReceiptVoucherNo, AppUtility.Operators.Equals, txtReceiptVoucherNo.Text, 0);
        }
        if (txtReceiptVoucherAmount.Text != "")
        {
            switch (ddlOperators.SelectedValue)
            {
                case "1":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ReceiptVoucher.Amount, AppUtility.Operators.Equals, txtReceiptVoucherAmount.Text, 0);
                    break;
                case "2":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ReceiptVoucher.Amount, AppUtility.Operators.GreaterOrEqualTo, txtReceiptVoucherAmount.Text, 0);
                    break;
                case "3":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ReceiptVoucher.Amount, AppUtility.Operators.LessOrEqualTo, txtReceiptVoucherAmount.Text, 0);
                    break;
                case "4":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ReceiptVoucher.Amount, AppUtility.Operators.GreaterThan, txtReceiptVoucherAmount.Text, 0);
                    break;
                case "5":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ReceiptVoucher.Amount, AppUtility.Operators.LessThan, txtReceiptVoucherAmount.Text, 0);
                    break;
            }
        }
        if (hdfCustomerID.Value != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ReceiptVoucher.Customer_Company_ID, AppUtility.Operators.Equals, hdfCustomerID.Value, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ReceiptVoucher.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue, 0);
        }
        objColl.Search();
        Session[TableConstants.ReceiptVoucherSummary] = objColl;
        grdReceiptVoucherSummary.DataSource = objColl;
        grdReceiptVoucherSummary.DataBind();
        if (grdReceiptVoucherSummary.Rows.Count <= 0)
        {
            pnlDisplaySummary.Visible = false;
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Records Found','2'" + ");", true);
        }
        else
        {
            pnlDisplaySummary.Visible = true;
        }
    }
    #endregion
    #region GridView Events
    public void CancelReceiptVoucher(AppReceiptVoucher ObjReceiptVoucher)
    {
        AppReceiptVoucher ObjDuplicateReceiptVoucher = ObjReceiptVoucher;
        ObjDuplicateReceiptVoucher.ID = 0;
        if (ObjDuplicateReceiptVoucher.PaymentType_ID == 2)
        {
            ObjDuplicateReceiptVoucher.VoucherType_ID = 27; // Cheque Void
        }
        else
        {
            ObjDuplicateReceiptVoucher.VoucherType_ID = 26; //Customer Receipt Cancel
        }
        ObjDuplicateReceiptVoucher.ModifiedBy = intRequestingUserID;
        ObjDuplicateReceiptVoucher.ModifiedOn = System.DateTime.Now;
        ObjDuplicateReceiptVoucher.Status = new AppConvert((int)RecordStatus.Cancelled);
        if (ObjDuplicateReceiptVoucher.ReceiptVoucherDetailColl.Count > 0)
        {
            int TotalCount = ObjDuplicateReceiptVoucher.ReceiptVoucherDetailColl.Count;
            for (int i = 0; i < TotalCount; i++)
            {
                ObjDuplicateReceiptVoucher.ReceiptVoucherDetailColl[i].ID = 0;
                ObjDuplicateReceiptVoucher.ReceiptVoucherDetailColl[i].ModifiedBy = intRequestingUserID;
                ObjDuplicateReceiptVoucher.ReceiptVoucherDetailColl[i].ModifiedOn = System.DateTime.Now;
                ObjDuplicateReceiptVoucher.ReceiptVoucherDetailColl[i].Status = new AppConvert((int)RecordStatus.Cancelled);
            }
        }
        ObjDuplicateReceiptVoucher.Save();
        #region Add Ledger Entry
        if (ObjDuplicateReceiptVoucher.ReceiptFor != 3)
        {
            decimal TdsAmount = ObjDuplicateReceiptVoucher.ReceiptVoucherDetailColl.Sum(x => x.TDSAmount);
            LedgerEntry(LoginCompanyID, 101301, 1013, ObjDuplicateReceiptVoucher.Ledger_ID, ObjDuplicateReceiptVoucher.ID, ObjDuplicateReceiptVoucher.ReceiptVoucherDate, 0, ObjDuplicateReceiptVoucher.Amount - TdsAmount, ObjDuplicateReceiptVoucher.ReceiptVoucherNo, ObjDuplicateReceiptVoucher.ModifiedBy, ObjDuplicateReceiptVoucher.ModifiedOn, 0, ObjDuplicateReceiptVoucher.Status, ObjDuplicateReceiptVoucher.Type,
                                  0, 0, ObjDuplicateReceiptVoucher.VoucherType_ID, ObjDuplicateReceiptVoucher.Description, "", ObjDuplicateReceiptVoucher.PaymentType_ID);
            if (ObjDuplicateReceiptVoucher.ReceiptVoucherDetailColl.Count > 0)
            {
                foreach (var item in ObjDuplicateReceiptVoucher.ReceiptVoucherDetailColl)
                {
                    if (item.Amount != 0)
                    {
                        LedgerEntry(LoginCompanyID, 101401, 1014, item.Ledger_ID, item.ID, ObjDuplicateReceiptVoucher.ReceiptVoucherDate, item.Amount, 0, ObjDuplicateReceiptVoucher.ReceiptVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicateReceiptVoucher.Status, item.Type,
                                     item.ProfitCenter_ID, item.CostCenter_ID, ObjDuplicateReceiptVoucher.VoucherType_ID, item.ReceiptVoucherDesc, "", ObjDuplicateReceiptVoucher.PaymentType_ID);
                        LedgerEntry(LoginCompanyID, 101402, 1014, item.TDS_Ledger_ID, item.ID, ObjDuplicateReceiptVoucher.ReceiptVoucherDate, 0, item.TDSAmount, ObjDuplicateReceiptVoucher.ReceiptVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicateReceiptVoucher.Status, item.Type,
                                    item.ProfitCenter_ID, item.CostCenter_ID, ObjDuplicateReceiptVoucher.VoucherType_ID, item.ReceiptVoucherDesc, "", ObjDuplicateReceiptVoucher.PaymentType_ID);
                    }
                }
            }
        }
        else
        {
            decimal TdsAmount = ObjDuplicateReceiptVoucher.ReceiptVoucherDetailColl.Sum(x => x.TDSAmount);
            LedgerEntry(LoginCompanyID, 101301, 1013, ObjDuplicateReceiptVoucher.Ledger_ID, ObjDuplicateReceiptVoucher.ID, ObjDuplicateReceiptVoucher.ReceiptVoucherDate, ObjDuplicateReceiptVoucher.Amount - TdsAmount, 0, ObjDuplicateReceiptVoucher.ReceiptVoucherNo, ObjDuplicateReceiptVoucher.ModifiedBy, ObjDuplicateReceiptVoucher.ModifiedOn, 0, ObjDuplicateReceiptVoucher.Status, ObjDuplicateReceiptVoucher.Type,
                                 0, 0, ObjDuplicateReceiptVoucher.VoucherType_ID, ObjDuplicateReceiptVoucher.Description, "", ObjDuplicateReceiptVoucher.PaymentType_ID);
            if (ObjDuplicateReceiptVoucher.ReceiptVoucherDetailColl.Count > 0)
            {
                foreach (var item in ObjDuplicateReceiptVoucher.ReceiptVoucherDetailColl)
                {
                    if (item.Amount != 0)
                    {
                        LedgerEntry(LoginCompanyID, 101401, 1014, item.Ledger_ID, item.ID, ObjDuplicateReceiptVoucher.ReceiptVoucherDate, 0, item.Amount, ObjDuplicateReceiptVoucher.ReceiptVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicateReceiptVoucher.Status, item.Type,
                                     item.ProfitCenter_ID, item.CostCenter_ID, ObjDuplicateReceiptVoucher.VoucherType_ID, item.ReceiptVoucherDesc, "", ObjDuplicateReceiptVoucher.PaymentType_ID);
                        LedgerEntry(LoginCompanyID, 101402, 1014, item.TDS_Ledger_ID, item.ID, ObjDuplicateReceiptVoucher.ReceiptVoucherDate, item.TDSAmount, 0, ObjDuplicateReceiptVoucher.ReceiptVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicateReceiptVoucher.Status, item.Type,
                                    item.ProfitCenter_ID, item.CostCenter_ID, ObjDuplicateReceiptVoucher.VoucherType_ID, item.ReceiptVoucherDesc, "", ObjDuplicateReceiptVoucher.PaymentType_ID);
                    }
                }
            }
        }
        #endregion
    }
    protected void grdReceiptVoucherSummary_PreRender(object sender, EventArgs e)
    {
        if (grdReceiptVoucherSummary.Rows.Count > 0)
        {
            grdReceiptVoucherSummary.UseAccessibleHeader = true;
            grdReceiptVoucherSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region Button Click Events
    protected void btnNewReceiptVoucher_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.ObjCompanySession);
        Response.Redirect("T_ReceiptVoucherDetails.aspx");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
        if (grdReceiptVoucherSummary.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Records Found','2'" + ");", true);
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (Update(grdReceiptVoucherSummary, new AppConvert((int)RecordStatus.Deleted), 1013, 1014) == 1)
        {
            BindData();
        }
    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        if (Update(grdReceiptVoucherSummary, new AppConvert((int)RecordStatus.Approve), 1013, 1014) == 1)
        {
            BindData();
        }
    }
    protected void btnCanceled_Click1(object sender, EventArgs e)
    {
        AppObjects.AppReceiptVoucherColl objColl = new AppReceiptVoucherColl(intRequestingUserID);
        if (txtCancelPVNo.Text != "" && txtVoucherDate.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ReceiptVoucher.ReceiptVoucherNo, AppUtility.Operators.Equals, txtCancelPVNo.Text, 0);
            objColl.Search(RecordStatus.ALL);
            if (objColl.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Record not found','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                return;
            }
            else
            {
                AppReceiptVoucher objSinglePV = objColl[0];
                if (objSinglePV.Status == new AppConvert((int)RecordStatus.Cancelled))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelPVNo.Text + " Transaction alerady cancelled','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    return;
                }
                else if (objSinglePV.Status == new AppConvert((int)RecordStatus.Deleted))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelPVNo.Text + " Transaction is deleted','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    return;
                }
                else if (objSinglePV.Status == new AppConvert((int)RecordStatus.Created))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelPVNo.Text + " Transaction is not approved','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    return;
                }
                else if (objColl[0].ReceiptVoucherDate <= new AppUtility.AppConvert(Convert.ToDateTime(txtVoucherDate.Text)))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelPVNo.Text + " Cancel voucher date should be greater than or equal to voucher date.','1'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelPVNo.Focus();
                    return;
                }
                objSinglePV.Status = new AppConvert((int)RecordStatus.Cancelled);
                objSinglePV.Save();
                #region Update Ledger Entries
                UpdateLedgerEntryStatus(0, 1013, objSinglePV.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                var PVItem = objSinglePV.ReceiptVoucherDetailColl.ToList();
                foreach (var item in PVItem)
                {
                    UpdateLedgerEntryStatus(0, 1014, item.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                }
                #endregion
                CancelReceiptVoucher(objSinglePV);
            }
            BindData();
            txtCancelPVNo.Text = "";
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Receipt voucher cancelled successfully','1'" + ");", true);
        }
        else
        {
            if (string.IsNullOrEmpty(txtCancelPVNo.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Voucher No.','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                txtCancelPVNo.Focus();
            }
            if (string.IsNullOrEmpty(txtVoucherDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Voucher Date.','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                txtVoucherDate.Focus();
            }
        }
    }
    #endregion
}