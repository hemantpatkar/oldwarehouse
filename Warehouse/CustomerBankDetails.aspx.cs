﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppUtility;
using AppObjects;
public partial class CustomerBankDetails : BigSunPage
{
    AppCompany obj;
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = (AppCompany)Session[TableConstants.CustomerSessionObj];
        if (!IsPostBack)
        {
           BindData();
        }
    }
    #region GridViewEvents
    protected void grdBankDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Footer:
                DropDownList ddlNewCurrencyType = (DropDownList)e.Row.FindControl("ddlNewCurrencyType");                
                CommonFunctions.BindDropDown(intRequestingUserID, Components.CurrencyType, ddlNewCurrencyType, "Name", "ID","",false);
                break;
            case DataControlRowType.DataRow:               
                DropDownList ddlCurrencyType = (DropDownList)e.Row.FindControl("ddlCurrencyType");
                if (ddlCurrencyType != null)
                {
                    CommonFunctions.BindDropDown(intRequestingUserID, Components.CurrencyType, ddlCurrencyType, "Name", "ID", "", false);
                   // ddlCurrencyType.SelectedValue = new AppConvert(obj.CompanyBankColl[e.Row.RowIndex].CurrencyType_ID);
                }
                break;
            default:
                break;
        }
    }
    protected void grdBankDetails_PreRender(object sender, EventArgs e)
    {
        if (grdBankDetails.Rows.Count > 0)
        {
            grdBankDetails.UseAccessibleHeader = true;
            grdBankDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdBankDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if (strCommandName == "INSERT")
        {       
            HiddenField hdfNewBankID = grdBankDetails.FooterRow.FindControl("hdfNewBankID") as HiddenField;
            HiddenField hdfNewLedgerID = grdBankDetails.FooterRow.FindControl("hdfNewLedgerID") as HiddenField;
            TextBox txtNewAccountNo = grdBankDetails.FooterRow.FindControl("txtNewAccountNo") as TextBox;
            DropDownList ddlNewCurrencyType = grdBankDetails.FooterRow.FindControl("ddlNewCurrencyType") as DropDownList;
            CheckBox chkActiveBank = grdBankDetails.FooterRow.FindControl("chkActiveBank") as CheckBox;
            CheckBox chkDefaultBank = grdBankDetails.FooterRow.FindControl("chkDefaultBank") as CheckBox;
            SaveBankDetails(strCommandID, hdfNewBankID.Value, txtNewAccountNo.Text, hdfNewLedgerID.Value, ddlNewCurrencyType.SelectedValue, chkActiveBank.Checked, chkDefaultBank.Checked);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Bank Successfully Added','1'" + ");", true);
            grdBankDetails.EditIndex = -1;
            BindData();
        }
        else if (strCommandName == "CANCELADD")
        {
            grdBankDetails.ShowFooter = false;
            BindData();
        }
        else if (strCommandName == "CANCELEDIT")
        {
            grdBankDetails.EditIndex = -1;
            BindData();
        }
        else if(strCommandName=="ADDNEW")
        {
            grdBankDetails.EditIndex = -1;
            grdBankDetails.ShowFooter = true;
            BindData();
        }
    }
    protected void grdBankDetails_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdBankDetails.ShowFooter = false;
        grdBankDetails.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdBankDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        obj = (AppCompany)Session[TableConstants.CustomerSessionObj];
        obj.CompanyBankColl[e.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
        Session[TableConstants.CustomerSessionObj] = obj;
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Bank Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void grdBankDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strCommandID = new AppConvert(e.RowIndex);
        HiddenField hdfBankTypeID = grdBankDetails.Rows[e.RowIndex].FindControl("hdfBankTypeID") as HiddenField;
        HiddenField hdfLedgerID = grdBankDetails.Rows[e.RowIndex].FindControl("hdfLedgerID") as HiddenField;
        TextBox txtAccountNo = grdBankDetails.Rows[e.RowIndex].FindControl("txtAccountNo") as TextBox;
        DropDownList ddlCurrencyType = grdBankDetails.Rows[e.RowIndex].FindControl("ddlCurrencyType") as DropDownList;       
        CheckBox chkIsActiveBank = grdBankDetails.Rows[e.RowIndex].FindControl("chkIsActiveBank") as CheckBox;
        CheckBox chkIsDefaultBank = grdBankDetails.Rows[e.RowIndex].FindControl("chkIsDefaultBank") as CheckBox;
        SaveBankDetails(strCommandID, hdfBankTypeID.Value, txtAccountNo.Text, hdfLedgerID.Value, ddlCurrencyType.SelectedValue, chkIsActiveBank.Checked, chkIsDefaultBank.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Bank Successfully Updated','1'" + ");", true);
        grdBankDetails.EditIndex = -1;
        BindData();
    }
    #endregion
    #region MiscFunctions
    public void SaveBankDetails(string Id, string BankType, string AccountNumber, string Ledger_ID, string CurrencyType, bool IsActive, bool IsDefault)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppCompanyBank SingleObj = new AppCompanyBank(intRequestingUserID);
        if (String.IsNullOrEmpty(Id) == false)
        {
            SingleObj = obj.CompanyBankColl[intCommandID];
        }
        else
        {
            obj.AddNewCompanyBank(SingleObj);
        }
        SingleObj.Company_ID = obj.ID;
        SingleObj.BankType_ID = new AppConvert(BankType);
        SingleObj.Ledger_ID = new AppConvert(Ledger_ID);
        SingleObj.CurrencyType_ID = new AppConvert(CurrencyType);

        SingleObj.AccountNo = AccountNumber;
        SingleObj.Status = (IsActive == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        if (IsDefault == true)
        {
            obj.SetDefaultBank();
        }
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;
        Session.Add(TableConstants.CustomerSessionObj, obj);
        SingleObj.Save();
        BindData();
    }
    private void BindData()
    {
        if (obj != null)
        {
            grdBankDetails.DataSource = obj.CompanyBankColl;
            grdBankDetails.DataBind();
            if (obj.CompanyBankColl.Count <= 0)
            {
                AddEmptyRow();
            }
        }
    }
    public void AddEmptyRow()
    {
        AppCompanyBankColl collOfContacts = new AppCompanyBankColl(intRequestingUserID);
        AppCompanyBank newContact = new AppCompanyBank(intRequestingUserID);
        newContact.BankType.Name = "Add atleast one Bank";
        collOfContacts.Add(newContact);
        grdBankDetails.DataSource = collOfContacts;
        grdBankDetails.DataBind();
        grdBankDetails.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdBankDetails.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdBankDetails.ShowFooter = true;
    }
    #endregion
}