﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Login" Codebehind="Login.aspx.cs" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Enterprise Resource Planning</title>

    <link href="UI/css/bootstrap.min.css" rel="stylesheet" />
    <link href="UI/css/font-awesome.min.css" rel="stylesheet" />
    <link href="UI/css/LoginPageCss/form-elements.css" rel="stylesheet" />
    <link href="UI/css/LoginPageCss/style.css" rel="stylesheet" />

     <!-- Javascript -->
    <script src="UI/js/LoginPageJs/jquery-1.11.1.min.js"></script>
    <script src="UI/js/LoginPageJs/jquery.backstretch.min.js"></script>
    <script src="UI/js/LoginPageJs/jquery.backstretch.js"></script>
    <script src="UI/js/LoginPageJs/scripts.js"></script>
    <!--[endif]-->


</head>

<body>

    <!-- Top content -->
    <div class="top-content">
        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text">
                        <h1><strong>Enterprise Resource Planning</strong></h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Login to application</h3>
                                <p>Enter your username and password to log on:</p>
                            </div>
                            <div class="form-top-right">
                                <img alt="" src="UI/img/CompanyLogo.jpg" style="height: 80px; width: 80px;" />
                            </div>
                        </div>

                        <div class="form-bottom">
                            <form role="form" action="" method="post" class="login-form" runat="server">
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Username</label>
                                    <asp:TextBox placeholder="Username..." CssClass="form-control form-username" ID="txtUsername" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="form-password">Password</label>
                                    <asp:TextBox placeholder="Password..." CssClass="form-control form-password" TextMode="Password" ID="txtpassword" runat="server"></asp:TextBox>
                                </div>

                                <div class=" ">
                                    <asp:Button ID="btnlogin" OnClick="btnlogin_Click" CssClass="btn btn-danger btn-lg btn-block" Text="Sign In" runat="server" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 

</body>

</html>
