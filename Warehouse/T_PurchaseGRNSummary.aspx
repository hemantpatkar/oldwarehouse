﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="T_PurchaseGRNSummary" Codebehind="T_PurchaseGRNSummary.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <div id="divButtons" class="pull-right">
                        <br />
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm  btn-primary" data-toggle="collapse" onclick="checkClass(2)" data-target="#divSearch">
                                <span class="glyphicon glyphicon-search"></span>Search Tools
                            </button>
                            <button type="button" class="btn btn-sm  btn-primary" onclick="checkClass(1)" data-toggle="collapse" data-target="#divCancel">
                                Cancel
                            </button>
                            <asp:Button runat="server" Text="Search" CssClass="btn btn-sm  btn-primary" ID="btnSearch" OnClick="btnSearch_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="btn  btn-sm  btn-primary" OnClick="btnDelete_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnApproved" Text="Approve" CssClass="btn  btn-sm  btn-primary" OnClick="btnApproved_Click"></asp:Button>                            
                            <asp:Button runat="server" ID="btnNewPurchaseGRN" Text="New Purchase GRN" CssClass="btn  btn-sm  btn-primary" OnClick="btnNewPurchaseGRN_Click"></asp:Button>
                        </div>
                    </div>
                    <h3>Purchase GRN Summary </h3>
                    <hr />
                    <div id="divSearch" class="collapse">
                        <div class="row" runat="server">
                            <div class="col-lg-3">
                                <asp:TextBox ID="txtPurchaeGRNNo" runat="server" placeholder="Enter Purchase GRN No" CssClass=" form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <asp:TextBox ID="txtVendor" TabIndex="1" AutoPostBack="true" placeholder="Select Vendor" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)"
                                        CssClass="form-control input-sm"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                </div>
                                <asp:HiddenField runat="server" ID="hdfVendorName" Value="" />
                                <asp:HiddenField runat="server" ID="hdfVendorID" Value="" />
                                <asp:AutoCompleteExtender ID="ACEtxtVendor" runat="server"
                                    CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                    CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                    CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                    FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                    ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtVendor">
                                </asp:AutoCompleteExtender>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <asp:TextBox ID="txtPurchaseGRNAmount" runat="server" placeholder="Purchase GRN Amount" CssClass=" form-control input-sm"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-inr"></i></span>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <asp:DropDownList ID="ddlOperators" runat="server" CssClass="form-control  input-sm">
                                    <asp:ListItem Text="=" Value="1"></asp:ListItem>
                                    <asp:ListItem Text=">=" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="<=" Value="3"></asp:ListItem>
                                    <asp:ListItem Text=">" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="<" Value="5"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-2">
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control  input-sm">
                                    <asp:ListItem Text="ALL" Value="-32768"></asp:ListItem>
                                    <asp:ListItem Text="Approved" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Created" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Deleted" Value="-10"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div id="divCancel" class="collapse">
                        <div class="row">
                            <div class="col-lg-3">
                                <asp:TextBox ID="txtCancelNo" runat="server" placeholder="Enter Payment Voucher No" CssClass=" form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <asp:TextBox ID="txtVoucherDate" TabIndex="1" runat="server" placeholder="Voucher Date" CssClass="form-control input-sm" ValidationGroup="MainSave"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <asp:MaskedEditExtender ID="MEE_txtVoucherDate" runat="server"
                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtVoucherDate">
                                </asp:MaskedEditExtender>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtVoucherDate"
                                    TargetControlID="txtVoucherDate">
                                </asp:CalendarExtender>
                            </div>
                            <div class="col-lg-2">
                                <asp:Button runat="server" ID="btnCanceled" Text="Cancel" CssClass="btn  btn-sm  btn-primary" OnClick="btnCanceled_Click"></asp:Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="pnlDisplaySummary" runat="server">
        <div class="col-lg-12">
            <div class="panel panel-body">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="grdPurchaseGRNSummary" runat="server" AutoGenerateColumns="False" Width="100%"
                            OnPreRender="grdPurchaseGRNSummary_PreRender" GridLines="Horizontal"
                            CssClass="table table-striped table-hover  text-nowrap  dt-responsive nowrap">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <input id="SelectAll" onclick="SelectAll(this);" runat="server" type="checkbox" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="ChkSelect" Enabled='<%#Convert.ToBoolean(Eval("Status").ToString()=="0"?  1 : 0)%>' />
                                        <asp:Label ID="lblPurchaseGRNID" runat="server" Text='<%# Eval("ID")%>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblPurchaseGRNNo" Text="Purchase GRN No" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HyperLink TabIndex="1" CausesValidation="false" ID="lbtnCompanyCode" runat="server"
                                            Text='<%# Eval("GRNNumber")%>' ToolTip="Edit" NavigateUrl='<% #"T_PurchaseGRN.aspx?PurchaseGRNID=" + Eval("ID")%>' Target="_blank"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderVendorName" Text="Vendor Name" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("Vendor_Company.Name") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderPurchaseGRNBaseAmount" Text="Base Amount" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblPurchaseGRNBaseAmount" runat="server" TabIndex="2" Text='<%# Convert.ToDecimal(Eval("InvoiceBaseAmount")).ToString("#,##0.00") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderDiscount" Text="Discount" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDiscount" runat="server" Text='<%# Convert.ToDecimal(Eval("DiscountAmount")).ToString("#,##0.00") %>' CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderTaxAmount" Text="Tax Amount" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblTaxAmount" runat="server" Text='<%# Convert.ToDecimal(Eval("TaxAmount")).ToString("#,##0.00") %>' CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderCurrency" Text="Currency" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblCurrencyType" runat="server" Text='<%#Eval("CurrencyType.Name") %>' CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderStatusOf" Text="Staus" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatusOf" runat="server" Text='<%#Eval("StatusName") %>' CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="grdPurchaseGRNSummary" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdPurchaseGRNSummary.ClientID%>');
        $(document).ready(function () {
            GridUI(GridID, 50);
        })
    </script>
</asp:Content>
