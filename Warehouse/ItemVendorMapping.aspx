﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="ItemVendorMapping" Codebehind="ItemVendorMapping.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            if (div.style.display == "none") {
                div.style.display = "block";
            } else {
                div.style.display = "none";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <h3>Item Mapping</h3>
            <hr />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdItemMapping" runat="server" AutoGenerateColumns="False" AllowPaging="true" OnPageIndexChanging="grdItemMapping_PageIndexChanging" PageSize="50" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found" OnRowDataBound="grdItemMapping_RowDataBound"
                        OnPreRender="grdItemMapping_PreRender" OnRowCommand="grdItemMapping_RowCommand" OnRowEditing="grdItemMapping_RowEditing" OnRowUpdating="grdItemMapping_RowUpdating"
                        OnRowCancelingEdit="grdItemMapping_RowCancelingEdit"
                        OnRowDeleting="grdItemMapping_RowDeleting"
                        CssClass="table table-striped  dt-responsive table-hover text-nowrap">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbtnAddNew" visible="false" runat="server" CssClass="btn btn-primary btn-xs" CommandName="Insert" OnClick="lbtnAddNew_Click"
                                        Text="Add New" ToolTip="Add New" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                     <asp:LinkButton ID="lbtnEdit" runat="server" CssClass="fa fa-floppy-o fa-2x" CommandName="SAVE" CommandArgument='<%# Eval("ID") %>'
                                        Text="" ToolTip="Edit" />
                                    <%--<asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT"  CommandArgument='<%# Eval("ID") %>' CssClass="fa fa-floppy-o fa-2x" ToolTip="Save" ValidationGroup="EditItem" 
                                        Text="" UseSubmitBehavior="False" />--%>
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete " CommandName="Delete" Text="" ToolTip="Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblItem" Text="Item Name" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <a href="JavaScript:divexpandcollapse('div<%# Eval("ID") %>');" id="aItemName"><%# Eval("Item.Name") %> </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblUOM" Text="UOM" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblUnitOfMeasu" runat="server" Text='<%#Eval("UOMType.Name") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblTaxType" Text="Tax Type" runat="server" CssClass=""></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITTax" runat="server" Text='<%#Eval("TaxType.Name") %>' AutoComplete="Off" CssClass=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrSupplyRate" Text="Supply Rate" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITItSupplyRate" runat="server" Text='<%#Eval("Price").ToString()=="0"?" ":Eval("Price").ToString() %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrPrice" Text="MRP" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITItPrice" runat="server" Text='<%#Eval("MRP").ToString()=="0"?" ":Eval("MRP").ToString() %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrMOQ" Text="MOQ" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITItMinOrderQty" runat="server" Text='<%#Eval("MinmumOrderQuantity").ToString()=="0"?" ":Eval("MinmumOrderQuantity").ToString() %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrLeadDays" Text="Supply Lead Days" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITItLeadDays" runat="server" Text='<%#Eval("SupplyLeadDays").ToString()=="0"?" ":Eval("SupplyLeadDays").ToString() %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrDispatchAdd" Text="Dispatch Add" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITItDispatchAdd" runat="server" Text='<%#Eval("CompanyAddress.Address1") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrCurrency" Text="Currency" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="ITItCurrency" runat="server" Text='<%#Eval("CurrencyType.Name") %>' CssClass="bold"></asp:Label>
                                    <tr>
                                        <td colspan="100%">
                                            <div class="row" style="display: none;" id="div<%# Eval("ID") %>">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <asp:Label ID="lblItemType" runat="server" CssClass="bold" Text="Item Type"></asp:Label>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtItemType" runat="server" Text='<%# Eval("Item.Name") %>' MaxLength="500" ValidationGroup="EditItem" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfItemTypeName" Value='<%# Eval("Item.Name").ToString() %>' />
                                                            <asp:HiddenField runat="server" ID="hdfItemTypeID" Value='<%# Eval("Item_ID") %>' />
                                                            <asp:AutoCompleteExtender ID="ACEtxtItemType" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="ItemSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtItemType">
                                                            </asp:AutoCompleteExtender>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtItemType" CssClass="text-danger"
                                                                Display="Dynamic" ErrorMessage="" ValidationGroup="EditItem" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <asp:Label ID="lblUOM" runat="server" CssClass="bold" Text="UOM"></asp:Label>
                                                            <asp:DropDownList ID="ddlUOM" runat="server" CssClass="form-control input-sm">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RFV_ddlUOM" runat="server" ControlToValidate="ddlUOM"
                                                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditItem" InitialValue="0"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <asp:Label ID="lblTaxType" runat="server" CssClass="bold" Text="Tax Type"></asp:Label>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtTaxType" runat="server" Text='<%#Eval("TaxType.Name") %>' AutoPostBack="false" ValidationGroup="EditItem" AutoComplete="Off" onblur="return ClearAutocompleteTextBox(this)" AutoCompleteType="None" CssClass="form-control input-sm"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfTaxTypeName" Value='<%#Eval("TaxType.Name") %>' />
                                                            <asp:HiddenField runat="server" ID="hdfTaxTypeID" Value='<%#Eval("TaxType_ID") %>' />
                                                            <asp:AutoCompleteExtender ID="ACEtxtTaxType" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TaxTypeSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtTaxType">
                                                            </asp:AutoCompleteExtender>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtTaxType" CssClass="text-danger" ToolTip="Required Field"
                                                                Display="Dynamic" ErrorMessage="" ValidationGroup="EditItem" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <asp:Label ID="lblSupplyRate" runat="server" CssClass="bold" Text="Supply Rate"></asp:Label>
                                                            <asp:TextBox ID="txtSupplyRate" runat="server" Text='<%#Eval("Price").ToString() %>' AutoComplete="Off" CssClass="form-control input-sm" onkeypress="return validateFloatKeyPress(this, event)" ValidationGroup="EditItem"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <asp:Label ID="lblProductPrice" runat="server" CssClass="bold" Text="Product Price"></asp:Label>
                                                            <asp:TextBox ID="txtProductPrice" runat="server" onkeypress="return validateFloatKeyPress(this, event)" Text='<%#Eval("MRP").ToString() %>' CssClass="form-control input-sm"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <asp:Label ID="Label1" runat="server" CssClass="bold" Text="Min Order Qty"></asp:Label>
                                                            <asp:TextBox ID="txtMinOrderQty" runat="server" Text='<%#Eval("MinmumOrderQuantity") %>' onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm" ValidationGroup="EditItem" AutoPostBack="false"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <asp:Label ID="Label2" runat="server" CssClass="bold" Text="Supply Lead Days"></asp:Label>
                                                            <asp:TextBox ID="txtLeadDays" onkeypress="return checkIfNumber(event);" runat="server" Text='<%#Eval("SupplyLeadDays") %>' CssClass="form-control input-sm" ValidationGroup="EditItem"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <asp:Label ID="lblddlNewDispatchAdd" runat="server" CssClass="bold" Text="Dispatch Address"></asp:Label>
                                                            <asp:DropDownList ID="ddlNewDispatchAdd" runat="server" CssClass="form-control input-sm">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RFV_ddlNewDispatchAdd" runat="server" ControlToValidate="ddlNewDispatchAdd" CssClass="text-danger" ToolTip="Required Field"
                                                                Display="Dynamic" ErrorMessage="" ValidationGroup="AddItem" SetFocusOnError="true" InitialValue="0"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <asp:Label ID="Label4" runat="server" CssClass="bold" Text="Currency Type"></asp:Label>
                                                            <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="form-control input-sm">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <asp:Label ID="Label5" runat="server" CssClass="bold" Text="Rate Variance"></asp:Label>
                                                            <asp:TextBox ID="txtRateVariance" runat="server" onkeypress="return validateFloatKeyPress(this, event)" Text='<%#Eval("PriceVariance") %>' CssClass="form-control input-sm" ValidationGroup="EditItem"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-3">
                                                            <asp:Label ID="Label6" runat="server" CssClass="bold" Text="Qty Variance"></asp:Label>
                                                            <asp:TextBox ID="txtQtyVariance" runat="server" Text='<%#Eval("QtyVariance") %>' CssClass="form-control input-sm" ValidationGroup="EditItem" onkeypress="return validateFloatKeyPress(this, event)"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-1 col-md-1 col-sm-3">
                                                            <asp:Label ID="Label7" runat="server" CssClass="bold" Text="Active/InActive"></asp:Label>
                                                            <asp:CheckBox runat="server" ID="chkActive" CssClass="bold" Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdItemMapping" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
  
</asp:Content>
