﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Culture="en-gb" Inherits="StockPartyTransfer" Codebehind="StockPartyTransfer.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function checkedAllSearch() {
            var but = document.getElementById("SelectAll");
            var TargetBaseControl = document.getElementById('<%= this.gvSearch.ClientID %>');
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var icount = 0; icount < Inputs.length; ++icount) {
                if (Inputs[icount].type == 'checkbox') {
                    if (Inputs[icount].disabled == false) {
                        if (but.checked == true) {
                            Inputs[icount].checked = true;
                        }
                        else {

                            Inputs[icount].checked = false;
                        }
                    }
                    else {
                        Inputs[icount].checked = false;
                    }
                }
            }
        }



        function PartnerSearchFrom(sender, eventArgs) {
            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());
            var hdpartID = $get('<%= hdfPartnerID.ClientID %>');
            var lblPartname = $get('<%= hdfPartnerName.ClientID %>');
            var partycode = $get('<%= txtPartner.ClientID %>');
            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            lblPartname.defaultvalue = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=ACEtxtPartnerGroupCode.ClientID %>').set_contextKey(eventArgs.get_value());
            }

        }

        function ClearPartnerSearchFrom() {
            var OrignalProfitCenter = $get('<%=txtPartner.ClientID %>');
            var HidenProfitCenter = $get('<%= hdfPartnerName.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=hdfPartnerName.ClientID %>').value = "";
                $get('<%=txtPartner.ClientID %>').value = "";
                $get('<%=hdfPartnerID.ClientID %>').value = "";
            }
        }


        function PartnerSearchTo(sender, eventArgs) {
            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());
            var hdpartID = $get('<%= hdfToPartnerID.ClientID %>');
            var lblPartname = $get('<%= hdfToPartnerName.ClientID %>');
            var partycode = $get('<%= txtToPartner.ClientID %>');
            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            lblPartname.defaultvalue = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=ACEtxtPartnerGroupCodeTo.ClientID %>').set_contextKey(eventArgs.get_value());
            }

        }

        function ClearPartnerSearchTo() {
            var OrignalProfitCenter = $get('<%=txtToPartner.ClientID %>');
            var HidenProfitCenter = $get('<%= hdfToPartnerName.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=hdfToPartnerName.ClientID %>').value = "";
                $get('<%=txtToPartner.ClientID %>').value = "";
                $get('<%=hdfToPartnerID.ClientID %>').value = "";
            }
        }



        function PartnerGroupSearchFrom(sender, eventArgs) {
            var hdpartID = $get('<%= hdfPartnerGroupCodeID.ClientID %>');
            var lblPartname = $get('<%= hdfPartnerGroupCodeName.ClientID %>');
            var partycode = $get('<%= txtPartnerGroupCode.ClientID %>');

            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=ACEtxtPartner.ClientID %>').set_contextKey("3," + eventArgs.get_value());
            }
        }


        function ClearPartnerGroupSearchFrom() {
            var OrignalProfitCenter = $get('<%=txtPartnerGroupCode.ClientID %>');
            var HidenProfitCenter = $get('<%= hdfPartnerGroupCodeName.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=hdfPartnerGroupCodeID.ClientID %>').value = "";
                $get('<%=hdfPartnerGroupCodeName.ClientID %>').value = "";
                $get('<%=txtPartnerGroupCode.ClientID %>').value = "";
                $find('<%=UsrtxtPartnerCode_AutoCompleteExtender.ClientID %>').set_contextKey("0");
            }
        }

        function PartnerGroupSearchTo(sender, eventArgs) {

            var hdpartID = $get('<%= hdfPartnerGroupCodeToID.ClientID %>');
            var lblPartname = $get('<%= hdfPartnerGroupCodeToName.ClientID %>');
            var partycode = $get('<%= txtPartnerGroupCodeTo.ClientID %>');

            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=ACEtxtToPartner.ClientID %>').set_contextKey("3," + eventArgs.get_value());
            }
        }


        function ClearPartnerGroupSearchTo() {
            var OrignalProfitCenter = $get('<%=txtPartnerGroupCodeTo.ClientID %>');
            var HidenProfitCenter = $get('<%= hdfPartnerGroupCodeToName.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=hdfPartnerGroupCodeToID.ClientID %>').value = "";
                $get('<%=hdfPartnerGroupCodeToName.ClientID %>').value = "";
                $get('<%=txtPartnerGroupCodeTo.ClientID %>').value = "";
                $find('<%=UsrtxtPartnerCode_AutoCompleteExtender.ClientID %>').set_contextKey("0");
            }
        }






        function PartnerGroupSearch(sender, eventArgs) {

            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());
            var hdpartID = $get('<%= UsrhdfPartnerGroup.ClientID %>');
            var lblPartname = $get('<%= UsrlblPartnerGroup.ClientID %>');
            var partycode = $get('<%= UsrtxtPartnerGroupCode.ClientID %>');


            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=UsrtxtPartnerCode_AutoCompleteExtender.ClientID %>').set_contextKey(eventArgs.get_value());
            }
        }


        function ClearPartnerGroupSearch() {
            var OrignalProfitCenter = $get('<%=UsrtxtPartnerGroupCode.ClientID %>');
            var HidenProfitCenter = $get('<%= UsrlblPartnerGroup.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=UsrhdfPartnerGroup.ClientID %>').value = "";
                $get('<%=UsrlblPartnerGroup.ClientID %>').value = "";
                $get('<%=UsrtxtPartnerGroupCode.ClientID %>').value = "";
            }
        }

        function PartnerSearch(sender, eventArgs) {
            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());
            var hdpartID = $get('<%= UsrhdfPartner.ClientID %>');
            var lblPartname = $get('<%= UsrlblPartner.ClientID %>');
            var partycode = $get('<%= UsrtxtPartnerCode.ClientID %>');
            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            lblPartname.defaultvalue = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=UsrtxtPartnerGroupCode_AutoCompleteExtender.ClientID %>').set_contextKey(eventArgs.get_value());
            }

        }

        function ClearPartnerSearch() {
            var OrignalProfitCenter = $get('<%=UsrtxtPartnerCode.ClientID %>');
            var HidenProfitCenter = $get('<%= UsrlblPartner.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=UsrlblPartner.ClientID %>').value = "";
                $get('<%=UsrtxtPartnerCode.ClientID %>').value = "";
                $get('<%=UsrhdfPartner.ClientID %>').value = "";
            }
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" DisplayAfter="0" AssociatedUpdatePanelID="UpdatePanel1"
        runat="server">
        <ProgressTemplate>
            <div class="modal fade in" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-keyboard="false" style="display: block">
                <div class="modal-dialog  modal-sm">
                    <h1 style="text-align: center;">
                        <i class="fa fa-truck fa-magic fa-2x faa-passing animated"></i>
                    </h1>
                </div>
            </div>
            <div class="modal-backdrop fade in"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <%--Header Section--%>
            <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <i class="fa fa-users"></i>
                        <asp:Label ID="lblPartyTransferTitle" runat="server"></asp:Label>
                        &nbsp;/&nbsp;
                   <asp:Label ID="txtTSPNo" ForeColor="Maroon" runat="server"></asp:Label>
                        <asp:HiddenField ID="hdfPartyID" runat="server" />
                        <asp:HiddenField ID="hdfPartyDetailID" runat="server" />
                        <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                        <div class="pull-right">
                            <asp:LinkButton ID="btnDeletePartyTransfer" runat="server" ForeColor="Maroon" CausesValidation="False" OnClick="btnDeletePartyTransfer_Click" TabIndex="12" Visible="false">
                                  <i class="fa fa-trash"></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnBack" runat="server" CausesValidation="False" OnClick="btnBack_Click"
                                TabIndex="12" Text="">
                                  <i class="fa fa-search"></i>
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row btnrow">
                <div class="col-lg-6">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <asp:LinkButton ID="lnkdetail" runat="server"></asp:LinkButton>

                            <asp:HiddenField ID="hdfrevised" runat="server" />
                            <asp:ConfirmButtonExtender ID="cbeDelet" runat="server" ConfirmText="Are you sure?" TargetControlID="btnDelete">
                            </asp:ConfirmButtonExtender>
                        </li>
                        <li>
                            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank"
                                Visible="False" TabIndex="12"></asp:HyperLink>
                        </li>
                        <li>
                            <asp:LinkButton ID="btnSave" runat="server" OnClick="btnSave_Click" TabIndex="11" Text=""
                                ValidationGroup="PartyTransfer" AccessKey="w"></asp:LinkButton><span></span>

                        </li>
                        <li>
                            <asp:LinkButton ID="btnNewDetail" runat="server" Text="" CausesValidation="false" TabIndex="13"
                                OnClick="btnNewDetail_Click" AccessKey="n" />
                        </li>
                        <li>

                            <asp:LinkButton ID="btnDelete" runat="server" TabIndex="19" CssClass="btn btn-primary btn-sm"
                                Visible="False" OnClientClick="return confirmalert('Do you want to Delete ?' )" />
                        </li>
                        <li>
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CssClass="btn btn-primary btn-sm"
                                TabIndex="160" OnClick="btnhistory_Click" Visible="false" />
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <div class="pull-right">
                        <ul class="nav nav-pills nav-wizard">
                            <li class="active"><a href="#" data-toggle="tab">New</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Pending Approval</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Available</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Partially Available</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Done</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <hr />


            <%--Header Info--%>
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label ID="lblPartnerGroupCode" runat="server" CssClass="bold" Text="From Customer Group"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtPartnerGroupCode" TabIndex="2" runat="server" onblur="ClearPartnerGroupSearchFrom()" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                            </div>
                            <asp:AutoCompleteExtender ID="ACEtxtPartnerGroupCode" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="10" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerGroupSearchFrom"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" ContextKey="4" UseContextKey="true"
                                TargetControlID="txtPartnerGroupCode">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfPartnerGroupCodeID" runat="server" />
                            <asp:HiddenField ID="hdfPartnerGroupCodeName" runat="server"></asp:HiddenField>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPartner" runat="server" CssClass="bold" Text="From Customer"></asp:Label>
                            <asp:Label ID="lblpatman" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="reqpartern" runat="server" Display="Dynamic" ValidationGroup="PartyTransfer" ControlToValidate="txtPartner"></asp:RequiredFieldValidator>
                            <div class="input-group">
                                <asp:TextBox ID="txtPartner" TabIndex="2" runat="server" onblur="ClearPartnerSearchFrom()" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div>
                            <asp:AutoCompleteExtender ID="ACEtxtPartner" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="10" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerSearchFrom"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" ContextKey="3" UseContextKey="true"
                                TargetControlID="txtPartner">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfPartnerID" runat="server" />
                            <asp:HiddenField ID="hdfPartnerName" runat="server" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPartnerGroupCodeTo" runat="server" CssClass="bold" Text="To Customer Group"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtPartnerGroupCodeTo" TabIndex="2" runat="server" onblur="ClearPartnerGroupSearchTo()" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                            </div>
                            <asp:AutoCompleteExtender ID="ACEtxtPartnerGroupCodeTo" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="10" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerGroupSearchTo"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" ContextKey="4" UseContextKey="true"
                                TargetControlID="txtPartnerGroupCodeTo">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfPartnerGroupCodeToID" runat="server" />
                            <asp:HiddenField ID="hdfPartnerGroupCodeToName" runat="server"></asp:HiddenField>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblToPartner" runat="server" CssClass="bold" Text=" To Customer"></asp:Label>
                            <asp:Label ID="Label3" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ValidationGroup="PartyTransfer" ControlToValidate="txtToPartner"></asp:RequiredFieldValidator>
                            <div class="input-group">
                                <asp:TextBox ID="txtToPartner" TabIndex="2" runat="server" onblur="ClearPartnerSearchTo()" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div>
                            <asp:AutoCompleteExtender ID="ACEtxtToPartner" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="10" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerSearchTo"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" ContextKey="3" UseContextKey="true"
                                TargetControlID="txtToPartner">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfToPartnerID" runat="server" />
                            <asp:HiddenField ID="hdfToPartnerName" runat="server" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblOrderBy" runat="server" CssClass="bold" Text="Order By"></asp:Label>
                            <asp:Label ID="lblmanorder" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="rfvOrderedBy" runat="server" ControlToValidate="txtOrderBy" CssClass="text-danger" SetFocusOnError="true"
                                Display="Dynamic" ValidationGroup="PartyTransfer"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtOrderBy" runat="server" TabIndex="2" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblTransferPartnerDate" runat="server" CssClass="bold" Text="Transfer Order Date"></asp:Label>
                            <asp:Label ID="lblmantpda" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtTransferDate"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic" ValidationGroup="PartyTransfer"></asp:RequiredFieldValidator>
                            <div class="input-group">
                                <asp:TextBox ID="txtTransferDate" runat="server" MaxLength="50" ValidationGroup="PartyTransfer" TabIndex="2" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="txtTransferDate_MaskedEditExtender" runat="server" CultureAMPMPlaceholder=""
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtTransferDate">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="txtTransferDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtTransferDate"></asp:CalendarExtender>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label ID="lblRecvdBy" runat="server" CssClass="bold" Text="Received By"></asp:Label>
                            <asp:TextBox ID="txtReceivedBy" runat="server" TabIndex="2" onblur="ClearRevisedby()" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtReceivedBy" runat="server"
                                CompletionInterval="1" CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListItemCssClass="AutoExtenderList"
                                CompletionSetCount="10" ContextKey="0" DelimiterCharacters="" EnableCaching="false"
                                Enabled="True" FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch"
                                ServiceMethod="CompanyUserSearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtReceivedBy">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfReceivedByName" runat="server" />
                            <asp:HiddenField ID="hdfReceivedByID" runat="server" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblRecvdDate" runat="server" CssClass="bold" Text="Received Date"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtRecvdDate" ValidationGroup="PartyTransfer" runat="server" TabIndex="2" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="txtRecvdDate_MaskedEditExtender" runat="server" Mask="99/99/9999"
                                MaskType="Date" Enabled="True" TargetControlID="txtRecvdDate">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="caltxtRecvdDate" TargetControlID="txtRecvdDate" Format="dd/MM/yyyy"
                                runat="server">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ControlToValidate="txtRecvdDate" Display="Dynamic" CssClass="text-danger"
                                ErrorMessage="*" ValidationGroup="Approved" SetFocusOnError="True">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblTansferStartedOn" runat="server" CssClass="bold" Text="Transfer Started On"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtTansferPartyStartedOn" ValidationGroup="PartyTransfer" runat="server" CssClass="form-control input-sm" TabIndex="2" onblur="VisibleFalseTrue();"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="txtTansferPartyStartedOn_MaskedEditExtender" runat="server"
                                Mask="99/99/9999" MaskType="Date" Enabled="True" TargetControlID="txtTansferPartyStartedOn">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="caltxtTansferPartyStartedOn" TargetControlID="txtTansferPartyStartedOn"
                                Format="dd/MM/yyyy" runat="server">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="rfvStartedOn" runat="server" ControlToValidate="txtTansferPartyStartedOn"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ErrorMessage="*" ValidationGroup="Approved">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblStartedOnHH" runat="server" CssClass="bold" Text="HH"></asp:Label>
                            <asp:TextBox ID="txtTansferStartedOnHH" ValidationGroup="Approved" MaxLength="2"
                                runat="server" CssClass="form-control input-sm" TabIndex="2">
                            </asp:TextBox>
                            <asp:MaskedEditExtender ID="txtTansferStartedOnHH_MaskedEditExtender" Mask="99" MaskType="Number"
                                ClearMaskOnLostFocus="false" TargetControlID="txtTansferStartedOnHH" runat="server">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="MEVtxtTansferStartedOnHH" ControlToValidate="txtTansferStartedOnHH"
                                ValidationGroup="Approved" ControlExtender="txtTansferStartedOnHH_MaskedEditExtender"
                                MaximumValue="23" MinimumValue="0" runat="server" CssClass="text-danger" SetFocusOnError="True">
                            </asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ValidationGroup="Approved" ID="RFVTSH" runat="server" ControlToValidate="txtTansferStartedOnHH"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ErrorMessage="*">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblStartedOnMM" runat="server" CssClass="bold" Text="MM"></asp:Label>
                            <asp:TextBox ID="txtTansferStartedOnMM" ValidationGroup="Approved" runat="server"
                                CssClass="form-control input-sm" TabIndex="2">
                            </asp:TextBox>
                            <asp:MaskedEditExtender ID="txtTansferStartedOnMM_MaskedEditExtender" Mask="99" MaskType="Number"
                                ClearMaskOnLostFocus="false" TargetControlID="txtTansferStartedOnMM" runat="server">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ValidationGroup="Approved" ID="MEVtxtTansferStartedOnMM" ControlToValidate="txtTansferStartedOnMM"
                                ControlExtender="txtTansferStartedOnMM_MaskedEditExtender" MaximumValue="59"
                                MinimumValue="0" runat="server" CssClass="text-danger" SetFocusOnError="True">
                            </asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ValidationGroup="Approved" ID="RFVTSM" runat="server" ControlToValidate="txtTansferStartedOnMM"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ErrorMessage="*">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblTansferPartyEndedOn" runat="server" CssClass="bold" Text="Transfer Ended On"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtTansferPartyEndedOn" ValidationGroup="Approved" runat="server" CssClass="form-control input-sm"
                                    TabIndex="2" onblur="VisibleFalseTrue();" CausesValidation="True"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99/99/9999"
                                MaskType="Date" Enabled="True" TargetControlID="txtTansferPartyEndedOn">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CalendarExtender3" TargetControlID="txtTansferPartyEndedOn"
                                Format="dd/MM/yyyy" runat="server">
                            </asp:CalendarExtender>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblEndedOnHH" runat="server" CssClass="bold" Text="HH"></asp:Label>
                            <asp:TextBox ID="txtEndedOnHR" MaxLength="2" runat="server" CssClass="form-control input-sm"
                                TabIndex="2" ValidationGroup="Approved">
                            </asp:TextBox>
                            <asp:MaskedEditExtender ID="metxtEndedOnHR" Mask="99" MaskType="Number" ClearMaskOnLostFocus="false"
                                TargetControlID="txtEndedOnHR" runat="server" Enabled="false">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevtxtEndedOnHR" ControlToValidate="txtEndedOnHR" ValidationGroup="Approved"
                                ControlExtender="metxtEndedOnHR" MaximumValue="23" MinimumValue="0" InvalidValueMessage="Please Enter Valid Hour"
                                MaximumValueMessage="Please Enter Valid Hour" InvalidValueBlurredMessage="Please Enter Valid Hour"
                                runat="server" CssClass="text-danger" Enabled="false" SetFocusOnError="True">
                            </asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ValidationGroup="Approved" ID="rfvmetxtEndedOnHR" runat="server"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ControlToValidate="txtEndedOnHR" ErrorMessage="*" Enabled="False">
                            </asp:RequiredFieldValidator>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                Enabled="True" FilterType="Numbers" TargetControlID="txtEndedOnHR">
                            </asp:FilteredTextBoxExtender>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblEndedonMM" runat="server" CssClass="bold" Text="MM"></asp:Label>
                            <asp:TextBox ID="txtEndedOnMM" runat="server" CssClass="form-control input-sm"
                                TabIndex="2">
                            </asp:TextBox>
                            <asp:MaskedEditExtender ID="metxtEndedOnMM" Mask="99" MaskType="Number" ClearMaskOnLostFocus="false"
                                TargetControlID="txtEndedOnMM" runat="server" Enabled="false">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevtxtEndedOnMM" ControlToValidate="txtEndedOnMM" ValidationGroup="Approved"
                                ControlExtender="metxtEndedOnMM" MaximumValue="59" MinimumValue="0" InvalidValueMessage="Please Enter Valid Minutes"
                                MaximumValueMessage="Please Enter Valid Minutes" InvalidValueBlurredMessage="Please Enter Valid Minutes"
                                runat="server" CssClass="text-danger" Enabled="false" SetFocusOnError="True">
                            </asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ID="rfvtxtEndedOnMM" runat="server" ValidationGroup="Approved"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ControlToValidate="txtEndedOnMM" ErrorMessage="*" Enabled="False">
                            </asp:RequiredFieldValidator>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                Enabled="True" FilterType="Numbers" TargetControlID="txtEndedOnMM">
                            </asp:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:Label ID="lblRemarks" runat="server" CssClass="bold" Text="Remarks"></asp:Label>
                            <asp:TextBox ID="txtRemarks" runat="server" MaxLength="400" CssClass="form-control input-sm" TabIndex="2"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-lg-12" style="overflow: auto; max-height: 300px;">
                    <asp:GridView ID="gvDescription" ShowHeaderWhenEmpty="true" ShowHeader="true" runat="server" AutoGenerateColumns="False"
                        OnRowCommand="gvDescription_RowCommand" CssClass="table table-striped table-hover text-nowrap">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDeleteItem" ForeColor="Maroon" OnClientClick="return confirmalert('Do you want to Delete ?' )" runat="server" CommandName="Del" CommandArgument='<%# Eval("ID") %>' CausesValidation="False" TabIndex="1">
                                                <i class="fa fa-close fa-2x"></i>
                                    </asp:LinkButton>
                                    <asp:HiddenField ID="hdfPartyTrasferID" runat="server" Value='<%#Eval("ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblSrno" runat="server" Text="<%# Convert.ToInt32(Container.DataItemIndex)+1%>" Visible="true"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblLotNo" Text="LotNo" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblLotNo" runat="server" Text='<%#Eval("InwardDetail.LotInternal") %>' ToolTip='<%#Eval("InwardDetail.LotInternal")%>'></asp:Label>
                                    <asp:Label ID="lblLotVersion" runat="server" Text='<%#Eval("InwardDetail.LotVersion") %>' ToolTip='<%#Eval("InwardDetail.LotVersion")%>'></asp:Label>
                                    <asp:HiddenField ID="hdfInwardDetailID" runat="server" Value='<%#Eval("InwardDetail_ID") %>' />
                                    <asp:HiddenField ID="hdfMaterialOrderDetailID" runat="server" Value='<%#Eval("ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblOrderQty" Text="Transfer Qty" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtOrderQuantity" CssClass="form-control input-sm" Text='<%#Eval("Quantity") %>'
                                        ValidationGroup="PartyTransfer" CausesValidation="True" MaxLength="7" TabIndex="3" />
                                    <asp:CompareValidator ID="cmpOrderQuantity" runat="server" ControlToValidate="txtOrderQuantity"
                                        ErrorMessage="Invalid" ControlToCompare="txtNetQuan" ValidationGroup="PartyTransfer" CssClass="text-danger"
                                        Type="Integer" Operator="LessThanEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                    <asp:CompareValidator ID="cmpLessThan1" runat="server" ValidationGroup="PartyTransfer"
                                        ErrorMessage="" ControlToValidate="txtOrderQuantity" ValueToCompare="0" Type="Integer" CssClass="text-danger"
                                        Operator="NotEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                    <asp:CompareValidator ID="cmpgrthan" runat="server" ValidationGroup="PartyTransfer"
                                        ErrorMessage="" ControlToValidate="txtOrderQuantity" ValueToCompare="0" Type="Integer" CssClass="text-danger"
                                        Operator="GreaterThanEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                    <asp:RequiredFieldValidator ID="rfvOrderQuant" runat="server" Display="Dynamic" ControlToValidate="txtOrderQuantity"
                                        ValidationGroup="PartyTransfer" SetFocusOnError="true" ErrorMessage=""></asp:RequiredFieldValidator>
                                    <div style="height: 0px; width: 0px;">
                                        <asp:TextBox ID="txtNetQuan" runat="server" Text='<%#Eval("Quantity") %>' Height="0px"
                                            Width="0px" BorderColor="White" BackColor="White" BorderStyle="None" CausesValidation="True"
                                            BorderWidth="0px" ReadOnly="True"></asp:TextBox>
                                    </div>
                                    <asp:FilteredTextBoxExtender ID="ftbOrderQuantity" runat="server" FilterType="Numbers,Custom" ValidChars="."
                                        TargetControlID="txtOrderQuantity">
                                    </asp:FilteredTextBoxExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblStockQuantity" Text="Net Qty" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblStockQuantity" runat="server" Text='<%#Eval("Quantity") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblItemCode" Text="Item Name" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblStorageItemName" runat="server" Text='<%#Eval("Item.Name") %>'
                                        ToolTip='<%#Eval("Item.Name")%>'></asp:Label>
                                    <asp:HiddenField runat="server" ID="hdfStorageItemID" Value='<%#Eval("Item_ID")%>' />
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPackRef" Text="Packing Ref" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPackingReference" Text='<%# Eval("InwardDetail.PackingReference") %>' ToolTip='<%# Eval("InwardDetail.PackingReference") %>' runat="server" />
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblMark" Text="Item Mark" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblMark" runat="server" Text='<%# Eval("InwardDetail.ItemMark") %>' ToolTip='<%# Eval("InwardDetail.ItemMark") %>'></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblVakkal" Text="Vakkal" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblVakkal" runat="server" Text='<%#Eval("InwardDetail.LotCustomer")  %>' ToolTip='<%#Eval("InwardDetail.LotCustomer") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPalletNo" Text="Pallete/Container No" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPallete" runat="server" ForeColor="#3399ff" Text='<%# Eval("InwardDetail.PalletNo") %>' ToolTip='<%#Eval("InwardDetail.PalletNo")%>'></asp:Label><br />
                                    <asp:Label ID="lblContainerNo" runat="server" ForeColor="Maroon" Text='<%# Eval("InwardDetail.ContainerNo") %>' ToolTip='<%#Eval("InwardDetail.ContainerNo")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdVariety" Text="Variety/Count" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblVariety" ForeColor="#3399ff" Text='<%# Eval("InwardDetail.Variety.Name") %>' ToolTip='<%# Eval("InwardDetail.Variety.Name") %>' runat="server" />
                                    <asp:HiddenField ID="hdfVarietyID" Value='<%# Eval("InwardDetail.Variety_ID") %>' runat="server" />
                                    <br />
                                    <asp:Label ID="lblCount" ForeColor="Maroon" Text='<%# Eval("InwardDetail.Count") %>' ToolTip='<%# Eval("InwardDetail.Count") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdBrand" Text="Brand/Origin" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblBrand" ForeColor="#3399ff" Text='<%# Eval("InwardDetail.Brand.Name") %>' ToolTip='<%# Eval("InwardDetail.Brand.Name") %>' runat="server" />
                                    <asp:HiddenField ID="hdfBrandID" Value='<%# Eval("InwardDetail.Brand_ID") %>' runat="server" />
                                    <br />
                                    <asp:Label ID="lblorigin" ForeColor="Maroon" Text='<%# Eval("InwardDetail.Origin.Name") %>' ToolTip='<%# Eval("InwardDetail.Origin.Name") %>' runat="server" />
                                    <asp:HiddenField ID="hdfOriginID" Value='<%# Eval("InwardDetail.Origin_ID") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblScheme" Text="Scheme/Location" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblScheme" runat="server" ForeColor="#3399ff" Text='<%#Eval("InwardDetail.StorageSchemeRate.StorageScheme.Code")  %>' ToolTip='<%#Eval("InwardDetail.StorageSchemeRate.StorageScheme.Code") %>'></asp:Label>
                                    <asp:HiddenField runat="server" ID="hdfSchemeID" Value='<%#Eval("InwardDetail.StorageSchemeRate_ID")  %>' />
                                    <br />
                                    <asp:Label ID="lblLocationNo" runat="server" ForeColor="Maroon" Text='<%#Eval("InwardDetail.StorageLocation.Name") %>' ToolTip='<%#Eval("InwardDetail.StorageLocation.Name")%>'></asp:Label>
                                    <asp:HiddenField runat="server" ID="hdfStorageLocationID" Value='<%#Eval("InwardDetail.StorageLocation_ID")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>                            
                        </Columns>
                    </asp:GridView>
                </div>
            </div>



            <%--Search parameter panel--%>
            <div class="panel" runat="server" id="searchlot" style="display: none">
                <div class="panel-heading btn-primary">
                    <div class="row">
                    </div>

                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">

                            <asp:TextBox ID="UsrtxtPartnerGroupCode" onblur="ClearPartnerGroupSearch()" TabIndex="1" placeholder="Customer Group"
                                runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="UsrtxtPartnerGroupCode_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerGroupSearch"
                                ServiceMethod="PartnerGroupSearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" ContextKey="0" UseContextKey="true"
                                TargetControlID="UsrtxtPartnerGroupCode">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:HiddenField ID="UsrhdfPartnerGroup" runat="server" />
                                <asp:Label ID="UsrlblPartnerGroup" runat="server"></asp:Label>

                                <asp:TextBox ID="txthideMo" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="hdfmono" runat="server" />
                            </div>
                        </div>
                        <div class="col-lg-2">

                            <asp:TextBox ID="UsrtxtPartnerCode" onblur="ClearPartnerSearch()" TabIndex="2" runat="server" placeholder="Customer"
                                AccessKey="p" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="UsrtxtPartnerCode_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerSearch"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                UseContextKey="true" ContextKey="0" TargetControlID="UsrtxtPartnerCode">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:HiddenField ID="UsrhdfPartner" runat="server" />
                                <asp:HiddenField ID="UserhdfPartner1" runat="server" />
                                <asp:Label ID="UsrlblPartner" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtLotSearch" placeholder="Lot No" CssClass="form-control input-sm" MaxLength="10" runat="server"
                                TabIndex="4"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftblot" runat="server" FilterType="Numbers" TargetControlID="txtLotSearch">
                            </asp:FilteredTextBoxExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtReceivedQuantitySearch" TabIndex="4" MaxLength="20" placeholder="Received Quantity" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtItemGroup" TabIndex="4" MaxLength="20" placeholder="Item Group"
                                runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtItemGroup_AutoCompleteExtender" runat="server" EnableCaching="false"
                                TargetControlID="txtItemGroup" CompletionInterval="1" CompletionSetCount="10"
                                UseContextKey="true" ContextKey="0" DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1"
                                ServiceMethod="ItemCategory" FirstRowSelected="false" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" CompletionListElementID="divwidth"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListCssClass="AutoExtender">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtStorageItemName" TabIndex="4" MaxLength="20" placeholder="Item Name"
                                runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtLocSearch_AutoCompleteExtender" runat="server" EnableCaching="false"
                                TargetControlID="txtStorageItemName" CompletionInterval="1" CompletionSetCount="10" ContextKey="0" UseContextKey="true"
                                DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="ItemSearch"
                                FirstRowSelected="false" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListCssClass="AutoExtender">
                            </asp:AutoCompleteExtender>
                        </div>

                        <div class="col-lg-2">
                            <asp:TextBox ID="txtpackingrefSearch" placeholder="Packing Reference" TabIndex="4" MaxLength="20" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtMarkSearch" placeholder="Item Mark" TabIndex="4" MaxLength="20" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtVakkalSearch" placeholder="Vakkal" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtSchemeSearch" placeholder="Scheme" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtBrand" placeholder="Brand" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtOrigin" placeholder="Origin" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <asp:Button ID="btnLotSearch" runat="server" OnClick="btnLotSearch_Click" TabIndex="19" CssClass="btn btn-primary btn-sm btn-block" Text="Search" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Button ID="btnAddItems" runat="server" CausesValidation="False" CssClass="btn btn-primary btn-sm btn-block"
                                OnClick="btnAddItems_Click" TabIndex="19" Text="" />
                        </div>
                        <div class="col-lg-2">

                            <label class="btn btn-primary btn-sm  btn-block" id="divIssue" runat="server">
                                <asp:CheckBox ID="chkZero" runat="server" TabIndex="2" />
                                <asp:Label ID="lblchkZero" runat="server" CssClass="bold" />
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <label class="btn btn-primary btn-sm  btn-block" id="divNewRequirement" runat="server">
                                <asp:CheckBox ID="chkFyear" runat="server" TabIndex="2" />
                                <asp:Label ID="lblchkFyear" runat="server" CssClass="bold" />
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <asp:Button ID="btnDeleteItem" runat="server" OnClick="btnDeleteItem_Click" TabIndex="19" Visible="False"
                                OnClientClick="return confirmalert('Do you want to Delete ?' )" CssClass="btn btn-danger btn-sm" />
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-lg-12" style="overflow: auto">
                            <asp:GridView ID="gvSearch" runat="server" AutoGenerateColumns="False" PagerStyle-CssClass="pagination-ys"
                                OnRowCommand="gvSearch_RowCommand" AllowPaging="True" OnPageIndexChanging="gvSearch_PageIndexChanging"
                                CssClass="table table-hover  table-striped text-nowrap" PageSize="30">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <input id="SelectAll" onclick="checkedAllSearch();" type="checkbox" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%--disabled='<%# Convert.ToInt32(Eval("NetQuantity")) > 0 ? false : true  %>'--%>
                                            <input id="CheckBoxSelect" name="CheckBoxSelect" onkeypress="clickButton(event,'<%# ((GridViewRow)Container).FindControl("lnkAdditem").ClientID %>')" type="checkbox" tabindex="4" value='<%#Eval("InwardDetail_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblorderQty" Text="Order Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtOrderQtySearch" ValidationGroup="OrderQty" CssClass="form-control input-sm" Text="" onkeypress="return GridclickButton(event,'lnkAdditem',this)" runat="server"></asp:TextBox>
                                            <asp:CompareValidator ID="cmpOrderQuantity" runat="server" ControlToValidate="txtOrderQtySearch"
                                                ErrorMessage="Invalid" ControlToCompare="txtNetQuantity" ValidationGroup="OrderQty" CssClass="text-danger"
                                                Type="Double" Operator="LessThanEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                            <asp:CompareValidator ID="cmpLessThan1" runat="server" ValidationGroup="OrderQty"
                                                ErrorMessage="" ControlToValidate="txtOrderQtySearch" ValueToCompare="0" Type="Double" CssClass="text-danger"
                                                Operator="NotEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                            <asp:CompareValidator ID="cmpgrthan" runat="server" ValidationGroup="OrderQty"
                                                ErrorMessage="" ControlToValidate="txtOrderQtySearch" ValueToCompare="0" Type="Double" CssClass="text-danger"
                                                Operator="GreaterThanEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblLotNo" Text="LotNo" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="display: none;">
                                                <asp:LinkButton ID="lnkAdditem" ValidationGroup="OrderQty" runat="server" CommandName="Sel" CommandArgument='<%#Eval("InwardDetail_ID") %>' />
                                                <asp:HiddenField ID="hdfInwardDetailID" runat="server" Value='<%#Eval("InwardDetail_ID") %>' />
                                                <asp:HiddenField ID="hdfMaterialOrderID" runat="server" Value="" />
                                                <asp:TextBox ID="txtNetQuantity" runat="server" Text='<% #Eval("NetQuantity")%>'></asp:TextBox>
                                            </div>
                                            <asp:LinkButton ID="lblLotNo" Text='<%#Eval("LotInternal") %>' CommandArgument='<%# Eval("InwardDetail_ID")%>'
                                                CommandName="Detail" ToolTip='<%#Eval("LotInternal")%>' runat="server"></asp:LinkButton>
                                            <asp:LinkButton ID="lblLotVersion" Text='<%#Eval("LotVersion") %>' CommandArgument='<%# Eval("InwardDetail_ID")%>'
                                                CommandName="Detail" ToolTip='<%#Eval("LotVersion")%>' runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblParty" Text="Customer Info" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkpartner" Text='<%#Eval("CustomerName") %>' CommandArgument='<%# Eval("Customer_Company_ID")%>'
                                                CommandName="Customer" ToolTip='<%#Eval("CustomerName")%>' runat="server"></asp:LinkButton>
                                            <asp:HiddenField ID="hdfPartnerID" runat="server" Value='<%#Eval("Customer_Company_ID") %>' />
                                            <br />
                                            <asp:Label ID="lblCustomer" Text='<%#Eval("CustomerGroupName")%>' ForeColor="Maroon" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblItem" Text="Item Info" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItem" runat="server" Text='<%#Eval("ItemName") %>'></asp:Label>
                                            <asp:HiddenField runat="server" ID="hdfStorageItemID" Value='<%#Eval("Item_ID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblMark" Text="Item Mark" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMark" runat="server" Text='<%# Eval("ItemMark") %>'
                                                ToolTip='<%# Eval("ItemMark") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblPackingRef" Text="Packing Ref" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPackingReference" Text='<%# Eval("PackingReference") %>' ToolTip='<%# Eval("PackingReference") %>'
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblVakkal" Text="Vakkal" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVakkal" runat="server" Text='<%#Eval("LotCustomer")  %>'
                                                ToolTip='<%#Eval("LotCustomer") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblPalleteNo" Text="Pallete No" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPallete" runat="server" Text='<%# Eval("PalletNo") %>'
                                                ToolTip='<%#Eval("PalletNo")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblhdCount" Text="Variety/Count" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVariety" Text='<%# Eval("VarietyName") %>' ToolTip='<%# Eval("VarietyName") %>' runat="server" />
                                            <asp:HiddenField ID="hdfVarietyID" Value='<%# Eval("Variety_ID") %>' runat="server" />
                                            <br />
                                            <asp:Label ID="lblCount" Text='<%# Eval("Count") %>' ToolTip='<%# Eval("Count") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblhdBrand" Text="Brand/Origin" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBrand" Text='<%# Eval("BrandName") %>' ToolTip='<%# Eval("BrandName") %>' runat="server" />
                                            <asp:HiddenField ID="hdfBrandID" Value='<%# Eval("Brand_ID") %>' runat="server" />
                                            <br />
                                            <asp:Label ID="lblorigin" Text='<%# Eval("OriginName") %>' ToolTip='<%# Eval("OriginName") %>' runat="server" />
                                            <asp:HiddenField ID="hdfOriginID" Value='<%# Eval("Origin_ID") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblStockQty" Text="Stock Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStockQuantity" runat="server" Text='<%#Eval("StockQuantity") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblOrderQty" Text="Order Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrderQuantity" runat="server" Text='<%#Eval("OrderQuantity") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblNetQty" Text="Net Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetQuantity" runat="server" Text='<% #Eval("NetQuantity")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblScheme" Text="Scheme/Location" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblScheme" runat="server" Text='<%#Eval("SchemeCode")  %>' ForeColor="#3399ff"></asp:Label>
                                            <asp:HiddenField runat="server" ID="hdfSchemeID" Value='<%#Eval("StorageScheme_ID")  %>' />
                                            <br />
                                            <asp:Label ID="lblLocationNo" runat="server" Text='<%#Eval("LocationName") %>' ForeColor="Maroon"
                                                ToolTip='<%#Eval("LocationName")%>'></asp:Label>
                                            <asp:HiddenField runat="server" ID="hdfStorageLocationID" Value='<%#Eval("StorageLocation_ID")%>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

