﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class VendorItemMapping : BigSunPage
{
    AppCompany obj;
    #region Using
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = (AppCompany)Session[TableConstants.VendorSessionObj];
        if(!IsPostBack)
        {
            BindData();
        }
    }
    #endregion
    
    #region Button Click Events
    protected void lbtnAddNew_Click(object sender, EventArgs e)
    {
        BindData();
        grdItemMapping.ShowFooter = true;
    }
    #endregion
    #region GridView Events
    protected void grdItemMapping_PreRender(object sender, EventArgs e)
    {
        if (grdItemMapping.Rows.Count > 0)
        {
            grdItemMapping.UseAccessibleHeader = true;
            grdItemMapping.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdItemMapping_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if (strCommandName == "INSERT")
        {
            HiddenField hdfNewItemTypeID = grdItemMapping.FooterRow.FindControl("hdfNewItemTypeID") as HiddenField;
            DropDownList ddlNewUOM = grdItemMapping.FooterRow.FindControl("ddlNewUOM") as DropDownList;
            HiddenField hdfNewTaxTypeID = grdItemMapping.FooterRow.FindControl("hdfNewTaxTypeID") as HiddenField;
            TextBox txtNewSupplyRate = grdItemMapping.FooterRow.FindControl("txtNewSupplyRate") as TextBox;
            TextBox txtNewProductPrice = grdItemMapping.FooterRow.FindControl("txtNewProductPrice") as TextBox;
            TextBox txtNewMinOrderQty = grdItemMapping.FooterRow.FindControl("txtNewMinOrderQty") as TextBox;
            TextBox txtNewLeadDays = grdItemMapping.FooterRow.FindControl("txtNewLeadDays") as TextBox;
            DropDownList ddlNewDispatchAdd = grdItemMapping.FooterRow.FindControl("ddlNewDispatchAdd") as DropDownList;
            DropDownList ddlNewCurrency = grdItemMapping.FooterRow.FindControl("ddlNewCurrency") as DropDownList;
            TextBox txtNewRateVariance = grdItemMapping.FooterRow.FindControl("txtNewRateVariance") as TextBox;
            TextBox txtNewQtyVariance = grdItemMapping.FooterRow.FindControl("txtNewQtyVariance") as TextBox;
            CheckBox chkActive = grdItemMapping.FooterRow.FindControl("chkIsVendorActive") as CheckBox;
            SaveItemMappings(strCommandID, hdfNewItemTypeID.Value, ddlNewUOM.SelectedValue, hdfNewTaxTypeID.Value, txtNewProductPrice.Text, txtNewMinOrderQty.Text,
                txtNewLeadDays.Text, ddlNewDispatchAdd.SelectedValue, ddlNewCurrency.SelectedValue, txtNewRateVariance.Text, txtNewQtyVariance.Text,
                txtNewSupplyRate.Text, chkActive.Checked);
        }
        else if (strCommandName == "CANCELADD")
        {
            grdItemMapping.ShowFooter = false;
            BindData();
        }
    }
    protected void grdItemMapping_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdItemMapping.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdItemMapping_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grdItemMapping.EditIndex = -1;
        BindData();
    }
    protected void grdItemMapping_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        obj.VendorItemColl[e.RowIndex].Status = -1;
        Session[TableConstants.VendorSessionObj] = obj;
    }
    protected void grdItemMapping_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Footer:
                DropDownList ddlAddresss = (DropDownList)e.Row.FindControl("ddlNewDispatchAdd");
                BindAddress("Address1", "ID", ddlAddresss);
                DropDownList ddlNewCurrency = (DropDownList)e.Row.FindControl("ddlNewCurrency");
                CommonFunctions.BindCurrencyTypes(ddlNewCurrency, "Name", "ID");
                DropDownList ddlNewUOM = (DropDownList)e.Row.FindControl("ddlNewUOM");
                CommonFunctions.BindDropDown(intRequestingUserID, Components.UOMType, ddlNewUOM, "Name", "ID");
                break;
            case DataControlRowType.DataRow:
                DropDownList ddlDispatchAdd = (DropDownList)e.Row.FindControl("ddlDispatchAdd");
                if (ddlDispatchAdd != null)
                {
                    BindAddress("Address1", "ID", ddlDispatchAdd);
                    ddlDispatchAdd.SelectedValue = new AppConvert(obj.VendorItemColl[e.Row.RowIndex].CompanyAddress_ID);
                }
                DropDownList ddlCurrency = (DropDownList)e.Row.FindControl("ddlCurrency");
                if (ddlCurrency != null)
                {
                    CommonFunctions.BindCurrencyTypes(ddlCurrency, "Name", "ID");
                    ddlCurrency.SelectedValue = new AppConvert(obj.VendorItemColl[e.Row.RowIndex].CurrencyType_ID);
                }
                DropDownList ddlUOM = (DropDownList)e.Row.FindControl("ddlUOM");
                if (ddlUOM != null)
                {
                    CommonFunctions.BindDropDown(intRequestingUserID, Components.UOMType, ddlUOM, "Name", "ID");
                    ddlUOM.SelectedValue = new AppConvert(obj.VendorItemColl[e.Row.RowIndex].UOMType_ID);
                }
                break;
            default:
                break;
        }
    }
    protected void grdItemMapping_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strCommandID = new AppConvert(e.RowIndex);
        HiddenField hdfItemTypeID = grdItemMapping.Rows[e.RowIndex].FindControl("hdfItemTypeID") as HiddenField;
        DropDownList ddlUOM = grdItemMapping.Rows[e.RowIndex].FindControl("ddlUOM") as DropDownList;
        HiddenField hdfTaxTypeID = grdItemMapping.Rows[e.RowIndex].FindControl("hdfTaxTypeID") as HiddenField;
        TextBox txtSupplyRate = grdItemMapping.Rows[e.RowIndex].FindControl("txtSupplyRate") as TextBox;
        TextBox txtProductPrice = grdItemMapping.Rows[e.RowIndex].FindControl("txtProductPrice") as TextBox;
        TextBox txtMinOrderQty = grdItemMapping.Rows[e.RowIndex].FindControl("txtMinOrderQty") as TextBox;
        TextBox txtLeadDays = grdItemMapping.Rows[e.RowIndex].FindControl("txtLeadDays") as TextBox;
        DropDownList ddlDispatchAdd = grdItemMapping.Rows[e.RowIndex].FindControl("ddlDispatchAdd") as DropDownList;
        DropDownList ddlCurrency = grdItemMapping.Rows[e.RowIndex].FindControl("ddlCurrency") as DropDownList;
        TextBox txtRateVariance = grdItemMapping.Rows[e.RowIndex].FindControl("txtRateVariance") as TextBox;
        TextBox txtQtyVariance = grdItemMapping.Rows[e.RowIndex].FindControl("txtQtyVariance") as TextBox;
        CheckBox IsActive = grdItemMapping.Rows[e.RowIndex].FindControl("chkActive") as CheckBox;
        SaveItemMappings(strCommandID, hdfItemTypeID.Value, ddlUOM.SelectedValue, hdfTaxTypeID.Value, txtProductPrice.Text, txtMinOrderQty.Text, txtLeadDays.Text,
            ddlDispatchAdd.SelectedValue, ddlCurrency.SelectedValue, txtRateVariance.Text, txtQtyVariance.Text, txtSupplyRate.Text, IsActive.Checked);
        grdItemMapping.EditIndex = -1;
        BindData();
    }
    #endregion    
    #region BindData    
    private void BindData()
    {
        AppVendorItemColl appVendorItemColl = obj.VendorItemColl;
        grdItemMapping.DataSource = appVendorItemColl;
        grdItemMapping.DataBind();
        if (appVendorItemColl.Count <= 0)
        {
            AddEmptyRow();
        }     
    }
    #endregion    
    #region AddEmptyRow
    public void AddEmptyRow()
    {
        AppVendorItemColl vendorItemCollection = new AppVendorItemColl(intRequestingUserID);
        AppVendorItem newVendorItem = new AppVendorItem(intRequestingUserID);
        newVendorItem.Item.Name = "No Records Found";
        newVendorItem.UOMType.Name = "";
        newVendorItem.TaxType.Name = "";        
        vendorItemCollection.Add(newVendorItem);
        grdItemMapping.DataSource = vendorItemCollection;
        grdItemMapping.DataBind();
        grdItemMapping.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdItemMapping.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdItemMapping.ShowFooter = true;
    }
    #endregion    
    #region BindAddress
    public void BindAddress(string DataTextField, string DataValueField, DropDownList ddlAddressID)
    {
        if (obj.CompanyAddressColl.Count > 0 )
        {
            ddlAddressID.DataSource = obj.CompanyAddressColl;
            ddlAddressID.DataTextField = DataTextField;
            ddlAddressID.DataValueField = DataValueField;
            ddlAddressID.DataBind();
        }
        ddlAddressID.Items.Insert(0, "Select Address");
    }
    #endregion
    #region SaveItemMappings
    public void SaveItemMappings(string Id, string hdfNewItemTypeID, string ddlNewUOM, string hdfNewTaxTypeID, string txtNewProductPrice,string txtNewMinOrderQty,
        string txtNewLeadDays, string ddlNewDispatchAdd, string ddlNewCurrency, string txtNewRateVariance, string txtNewQtyVariance,string txtNewSupplyRate, bool IsActive)
    {
        string strCommandID = Id;
        int intCommandID = new AppUtility.AppConvert(strCommandID);
        AppVendorItem SingleObj = new AppVendorItem(intRequestingUserID);
        if (String.IsNullOrEmpty(strCommandID) == false)
        {
            SingleObj = obj.VendorItemColl[intCommandID];
        }
        //create one single object of Company address.        
        SingleObj.Company_ID = obj.ID;
        SingleObj.Item_ID = new AppConvert(hdfNewItemTypeID);
        SingleObj.UOMType_ID = new AppConvert(ddlNewUOM);
        SingleObj.TaxType_ID = new AppUtility.AppConvert(hdfNewTaxTypeID);
        SingleObj.Price = new AppUtility.AppConvert(txtNewSupplyRate);
        SingleObj.PriceVariance = new AppUtility.AppConvert(txtNewRateVariance);
        SingleObj.QtyVariance = new AppUtility.AppConvert(txtNewQtyVariance);
        SingleObj.MRP = new AppUtility.AppConvert(txtNewProductPrice);
        SingleObj.CompanyAddress_ID = new AppUtility.AppConvert(ddlNewDispatchAdd);
        SingleObj.MinmumOrderQuantity = new AppUtility.AppConvert(txtNewMinOrderQty);
        SingleObj.SupplyLeadDays = new AppUtility.AppConvert(txtNewLeadDays);
        SingleObj.CurrencyType_ID = new AppUtility.AppConvert(ddlNewCurrency);        
        SingleObj.Status = new AppUtility.AppConvert(IsActive);
        //SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = 1;//TableConstants.UserId;
        SingleObj.ModifiedOn = System.DateTime.Now;
        obj.AddNewVendorItem(SingleObj);
        //add new object in collection.           
        Session.Add(TableConstants.VendorSessionObj, obj);
        BindData();
        grdItemMapping.EditIndex = -1;
    }
    #endregion    
}