﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class CompanyAddressDetails : BigSunPage
{
    AppCompany obj;
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = (AppCompany)Session[TableConstants.ObjCompanySession];
        if (!IsPostBack)
        {
            BindData();
        }
    }
    protected void grdCompanyAddress_PreRender(object sender, EventArgs e)
    {
        if (grdCompanyAddress.Rows.Count > 0)
        {
            grdCompanyAddress.UseAccessibleHeader = true;
            grdCompanyAddress.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdCompanyAddress_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = "";
        if (strCommandName == "INSERT")
        {
            TextBox Address1 = grdCompanyAddress.FooterRow.FindControl("txtNewAddress") as TextBox;
            TextBox Area = grdCompanyAddress.FooterRow.FindControl("txtNewVendorArea") as TextBox;
            HiddenField hdfCountryID = grdCompanyAddress.FooterRow.FindControl("hdfNewCountryId") as HiddenField;
            HiddenField hdfZipCodeID = grdCompanyAddress.FooterRow.FindControl("hdfNewZipCodeID") as HiddenField;
            CheckBox IsActiveAddress = grdCompanyAddress.FooterRow.FindControl("chkIsVendorActive") as CheckBox;
            CheckBox IsDefaultAddress = grdCompanyAddress.FooterRow.FindControl("chkIsVendorDefault") as CheckBox;
            SaveAddress(strCommandID, Address1.Text, Area.Text, hdfCountryID.Value, hdfZipCodeID.Value, IsActiveAddress.Checked, IsDefaultAddress.Checked);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Address Added Successfully','1'" + ");", true);
            BindData();
            grdCompanyAddress.EditIndex = -1;
        }
        else if (strCommandName == "CANCELADD")
        {
            grdCompanyAddress.ShowFooter = false;
            BindData();
        }
    }
    protected void grdCompanyAddress_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        obj = (AppCompany)Session[TableConstants.ObjCompanySession];
        obj.CompanyAddressColl[e.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
        Session[TableConstants.ObjCompanySession] = obj;
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Address Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void lbtnAddNew_Click(object sender, EventArgs e)
    {
        BindData();
        grdCompanyAddress.ShowFooter = true;
    }
    protected void grdCompanyAddress_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grdCompanyAddress.EditIndex = -1;
        BindData();
    }
    protected void grdCompanyAddress_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdCompanyAddress.ShowFooter = false;
        grdCompanyAddress.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdCompanyAddress_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string Index = new AppConvert(e.RowIndex);
        TextBox Address1 = grdCompanyAddress.Rows[e.RowIndex].FindControl("txtAddress") as TextBox;
        TextBox Area = grdCompanyAddress.Rows[e.RowIndex].FindControl("txtVendorArea") as TextBox;
        HiddenField hdfCountryID = grdCompanyAddress.Rows[e.RowIndex].FindControl("hdCountryID") as HiddenField;
        HiddenField hdfZipCodeID = grdCompanyAddress.Rows[e.RowIndex].FindControl("hdfVendorPincodeID") as HiddenField;
        CheckBox IsActiveAddress = grdCompanyAddress.Rows[e.RowIndex].FindControl("chkActive") as CheckBox;
        CheckBox IsDefaultAddress = grdCompanyAddress.Rows[e.RowIndex].FindControl("chkDefault") as CheckBox;
        SaveAddress(Index, Address1.Text, Area.Text, hdfCountryID.Value, hdfZipCodeID.Value, IsActiveAddress.Checked, IsDefaultAddress.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Address Updated Successfully','1'" + ");", true);
        grdCompanyAddress.EditIndex = -1;
        BindData();
    }
    protected void txtNewVendorPinCode_TextChanged(object sender, EventArgs e)
    {
        GridViewRow gvr = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox City = ((TextBox)(grdCompanyAddress.FooterRow.FindControl("txtNewCity")));
        TextBox State = ((TextBox)(grdCompanyAddress.FooterRow.FindControl("txtNewState")));
        TextBox Country = ((TextBox)(grdCompanyAddress.FooterRow.FindControl("txtNewCountry")));
        HiddenField CountryID = ((HiddenField)(grdCompanyAddress.FooterRow.FindControl("hdfNewCountryId")));
        HiddenField ZipCodeID = ((HiddenField)(grdCompanyAddress.FooterRow.FindControl("hdfNewZipCodeID")));
        TextBox txtPinCode = ((TextBox)(grdCompanyAddress.FooterRow.FindControl("txtNewVendorPinCode")));
        string PinCode = txtPinCode.Text;
        AppZipCodeTypeColl appZCTColl = new AppZipCodeTypeColl(intRequestingUserID);
        appZCTColl.Search(PinCode);
        if (appZCTColl.Count > 0)
        {
            AppZipCodeType objZip = appZCTColl[0];
            City.Text = objZip.CountryType.City;
            State.Text = objZip.CountryType.State;
            Country.Text = objZip.CountryType.Country;
            CountryID.Value = new AppUtility.AppConvert(objZip.CountryType.ID);
            ZipCodeID.Value = new AppUtility.AppConvert(objZip.CountryType.ID);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Pincode Found','2'" + ");", true);
            City.Text = "";
            State.Text = "";
            Country.Text = "";
            CountryID.Value = "";
            ZipCodeID.Value = "";
            txtPinCode.Focus();
        }
    }
    protected void txtVendorPinCode_TextChanged(object sender, EventArgs e)
    {
        GridViewRow gvr = (GridViewRow)((TextBox)sender).Parent.Parent;
        int RowIndex = new AppConvert(gvr.RowIndex);
        TextBox City = (TextBox)(grdCompanyAddress.Rows[RowIndex].FindControl("txtCity"));
        TextBox State = (TextBox)(grdCompanyAddress.Rows[RowIndex].FindControl("txtState"));
        TextBox Country = (TextBox)(grdCompanyAddress.Rows[RowIndex].FindControl("txtCountry"));
        HiddenField CountryID = (HiddenField)(grdCompanyAddress.Rows[RowIndex].FindControl("hdCountryID"));
        HiddenField PincodeId = (HiddenField)(grdCompanyAddress.Rows[RowIndex].FindControl("hdfVendorPincodeID"));
        TextBox Pincode = (TextBox)(grdCompanyAddress.Rows[RowIndex].FindControl("txtVendorPinCode"));
        string PinCode = Pincode.Text;
        AppZipCodeTypeColl appZCTColl = new AppZipCodeTypeColl(intRequestingUserID);
        appZCTColl.Search(PinCode);
        if (appZCTColl.Count > 0)
        {
            AppZipCodeType objZip = appZCTColl[0];
            City.Text = objZip.CountryType.City;
            State.Text = objZip.CountryType.State;
            Country.Text = objZip.CountryType.Country;
            CountryID.Value = new AppUtility.AppConvert(objZip.CountryType.ID);
            PincodeId.Value = new AppUtility.AppConvert(objZip.CountryType.ID);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Pincode Found','2'" + ");", true);
            City.Text = "";
            State.Text = "";
            Country.Text = "";
            CountryID.Value = "";
            PincodeId.Value = "";
            Pincode.Focus();
        }
    }
    #region BindData    
    private void BindData()
    {
        AppCompanyAddressColl appCompanyAddressColl = obj.CompanyAddressColl;
        grdCompanyAddress.DataSource = appCompanyAddressColl;
        grdCompanyAddress.DataBind();
        if (appCompanyAddressColl.Count <= 0)
        {
            AddEmptyRow();
        }
    }
    #endregion
    #region AddEmptyRow
    public void AddEmptyRow()
    {
        AppCompanyAddressColl CollOfAddress = new AppCompanyAddressColl(intRequestingUserID);
        AppCompanyAddress newCompany = new AppCompanyAddress(intRequestingUserID);
        newCompany.Address1 = "Add atleast one Address";
        CollOfAddress.Add(newCompany);
        grdCompanyAddress.DataSource = CollOfAddress;
        grdCompanyAddress.DataBind();
        grdCompanyAddress.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdCompanyAddress.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdCompanyAddress.ShowFooter = true;
    }
    #endregion
    #region Save Address
    public void SaveAddress(string Id, string Address1, string Area, string CountryID,
        string ZipCodeID, bool IsActive, bool IsDefault)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppCompanyAddress SingleObj = new AppCompanyAddress(intRequestingUserID);
        if (String.IsNullOrEmpty(Id) == false)
        {
            SingleObj = obj.CompanyAddressColl[intCommandID];
        }
        else
        {
            obj.AddNewCompanyAddress(SingleObj);
        }
        SingleObj.Company_ID = obj.ID;
        SingleObj.Address1 = Address1;
        SingleObj.Address2 = Area;
        SingleObj.ZipCodeType_ID = new AppUtility.AppConvert(ZipCodeID);
        SingleObj.CountryType_ID = new AppUtility.AppConvert(CountryID);
        SingleObj.Status = (IsActive==true?new AppConvert((int)RecordStatus.Active):new AppConvert((int)RecordStatus.InActive));        
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;
        if (IsDefault == true)
        {
            obj.SetDefaultAddress();
        }
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        Session.Add(TableConstants.ObjCompanySession, obj);
        BindData();
        grdCompanyAddress.EditIndex = -1;
    }
    #endregion
}