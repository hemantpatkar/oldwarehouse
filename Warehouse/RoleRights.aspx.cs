﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using AppObjects;
using AppUtility;
using System.Web.UI.WebControls;

public partial class RoleRights : BigSunPage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            CommonFunctions.BindDropDown(this.intRequestingUserID, Components.DepartmentType, ddlDepartment, "Name", "ID", "", true);
        }
    }

    

    #region Bind DesignationTypes
    public void BindDesignationTypes(DropDownList ddlDesignation)
    {
        AppDesignationTypeColl objDesignationTypeColl = new AppDesignationTypeColl(intRequestingUserID);
        objDesignationTypeColl.Search();
        ddlDesignation.DataSource = objDesignationTypeColl;
        ddlDesignation.DataTextField = "Name";
        ddlDesignation.DataValueField = "ID";
        ddlDesignation.DataBind();
        ddlDesignation.Items.Insert(0, "Select");
    }
    #endregion
    #region Bind grdRoles
    protected void BindgrdRoles()
    {
        AppAppComponentRightColl objComponent = new AppAppComponentRightColl(this.intRequestingUserID);
        Int16 DepartmentID = 0, DesignationID = 0;
        if (ddlDepartment.SelectedIndex != 0 && ddlDesignation.SelectedIndex != 0)
        {
            DepartmentID = new AppConvert(ddlDepartment.SelectedValue);
            DesignationID = new AppConvert(ddlDesignation.SelectedValue);
        }
        objComponent.Search(DepartmentID, DesignationID);
        grdRoles.DataSource = objComponent;
        grdRoles.DataBind();
    }
    #endregion

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlDesignation.Items.Clear();
        grdRoles.DataSource = null;
        grdRoles.DataBind();
        if (ddlDepartment.SelectedIndex > 0)
        {
            BindDesignationTypes(ddlDesignation);
        }
    }

    protected void ddlDesignation_SelectedIndexChanged(object sender, EventArgs e)
    {
        grdRoles.DataSource = null;
        grdRoles.DataBind();
        if (ddlDesignation.SelectedIndex > 0)
        {
            BindgrdRoles();
        }
    }

    protected void grdRoles_PreRender(object sender, EventArgs e)
    {
        if (grdRoles.Rows.Count > 0)
        {
            grdRoles.UseAccessibleHeader = true;
            grdRoles.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        AppAppComponentRightColl objComponent = new AppAppComponentRightColl(this.intRequestingUserID);
        Int16 DepartmentID = 0, DesignationID = 0;
        if (ddlDepartment.SelectedIndex != 0 && ddlDesignation.SelectedIndex != 0)
        {
            DepartmentID = new AppConvert(ddlDepartment.SelectedValue);
            DesignationID = new AppConvert(ddlDesignation.SelectedValue);
        }
        objComponent.Search(DepartmentID, DesignationID);
        for (int i = 0; i < grdRoles.Rows.Count; i++)
        {
            DropDownList ddlRightsgrd = (DropDownList)grdRoles.Rows[i].FindControl("ddlRightsgrd");
            ddlRightsgrd.SelectedValue = objComponent[i].Status.ToString();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppAppComponentRightColl objComponent = new AppAppComponentRightColl(this.intRequestingUserID);
        Int16 DepartmentID = 0, DesignationID = 0;
        if (ddlDepartment.SelectedIndex != 0 && ddlDesignation.SelectedIndex != 0)
        {
            DepartmentID = new AppConvert(ddlDepartment.SelectedValue);
            DesignationID = new AppConvert(ddlDesignation.SelectedValue);
        }
        objComponent.Search(DepartmentID, DesignationID);

        for (int i = 0; i < grdRoles.Rows.Count; i++)
        {
            DropDownList ddlRightsgrd = (DropDownList)grdRoles.Rows[i].FindControl("ddlRightsgrd");
            objComponent[i].DepartmentType_ID = DepartmentID;
            objComponent[i].DesignationType_ID = DesignationID;
            objComponent[i].Status = new AppConvert(ddlRightsgrd.SelectedValue);
            objComponent[i].Save();
        }
    }
}