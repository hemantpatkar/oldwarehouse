﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="VendorDetails" Codebehind="VendorDetails.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="pull-right">
                          <br />
                            <asp:LinkButton ID="btnSave" runat="server" ValidationGroup="FinalSave" OnClick="btnSave_Click"><i class="fa fa-check  fa-2x text-success"></i></asp:LinkButton>                       
                            <asp:LinkButton ID="btnCancel" runat="server" OnClick="btnCancel_Click"><i class="fa fa-close fa-2x"></i></asp:LinkButton>                      
                            <asp:LinkButton ID="btnApprove" runat="server" OnClick="btnApprove_Click"><i class="fa fa-thumb fa-2x"></i></asp:LinkButton>                         
                    </div>
                    <h3>Vendor Details
                    </h3>
                    <hr />
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:Label ID="lblVendorCode" runat="server" CssClass="bold" Text="Vendor Code"></asp:Label>
                            <asp:Label ID="Label8" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ControlToValidate="txtVendorCode" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage="Select" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtVendorCode" runat="server" MaxLength="50" ValidationGroup="FinalSave" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblVendorName" runat="server" CssClass="bold " Text="Vendor Name"></asp:Label>
                            <asp:Label ID="lblVnameStar" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtVendorName" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage=" (Enter Vendor Name)" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtVendorName" runat="server" MaxLength="100" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="FinalSave"
                                CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblCurrency" CssClass="bold " runat="server" Text="Currency"></asp:Label>
                            <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="form-control input-sm" >                              
                            </asp:DropDownList>
                        </div>                      
                        <div class="col-lg-3">
                            <asp:Label ID="lblCoType" CssClass="bold " runat="server" Text="Organization Type"></asp:Label>
                            <asp:Label ID="Label1" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlOrganizationType" CssClass="text-danger" InitialValue="0"
                                Display="Dynamic" ErrorMessage="" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlOrganizationType" runat="server" CssClass="form-control input-sm" >                                
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-lg-3">
                            <asp:Label ID="lblDescription" runat="server" CssClass="bold" Text="Description"></asp:Label>
                            <asp:TextBox ID="txtDescription" runat="server" MaxLength="50" ValidationGroup="FinalSave" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblTermsandConditions" CssClass="bold " runat="server" Text="Terms & Conditions"> </asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtTermsAndConditions" TabIndex="1" placeholder="Select Terms And Conditions" runat="server"
                                    AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>
                            <asp:HiddenField runat="server" ID="hdfTermsAndConditionsName" Value="" />
                            <asp:HiddenField runat="server" ID="hdfTermsAndConditionsID" Value="" />
                            <asp:AutoCompleteExtender ID="ACEtxtTermsAndConditions" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TermsAndConditionsSearch" ServicePath="~/Service/AutoComplete.asmx"
                                OnClientItemSelected="AutoCompleteSearch" ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtTermsAndConditions">
                            </asp:AutoCompleteExtender>
                        </div>
                         <div class="col-lg-3" runat="server" id="divVendorLedger">
                            <asp:Label ID="lblVendorLedger" CssClass="bold" runat="server" Text="Vendor Ledger"> </asp:Label>
                            <asp:CheckBox Text="New Ledger" ID="chkIsNewVendorLedger" CssClass="checkboxHeader" AutoPostBack="true" OnCheckedChanged="chkIsNewVendorLedger_CheckedChanged" runat="server" Checked="true" />
                            <div class="input-group">
                                <asp:RequiredFieldValidator ID="RFV_txtVendorLedger" runat="server" ControlToValidate="txtVendorLedger" CssClass="text-danger"
                                    Display="Dynamic" ErrorMessage="" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtVendorLedger" TabIndex="1" placeholder="Select Sales Ledger" runat="server"
                                    AutoCompleteType="None" autocomplete="Off" ValidationGroup="save" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                <asp:HiddenField runat="server" ID="hdfVendorLedgerName" />
                                <asp:HiddenField runat="server" ID="hdfVendorLedgerID" Value="0" />
                            </div>
                            <asp:AutoCompleteExtender ID="ACEtxtVendorLedger" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx"
                                OnClientItemSelected="AutoCompleteSearch" ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtVendorLedger">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblActive" runat="server" Text="Active/InActive" CssClass="bold "></asp:Label>
                            <div class="material-switch pull-right">
                                <asp:CheckBox runat="server" ID="chkActive" TabIndex="1" Checked="true" />
                                <label for='<%=chkActive.ClientID %>' class="label-primary"></label>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class=" col-lg-12">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:TabContainer runat="server" CssClass="Bigtab" ActiveTabIndex="0" ID="tabMain" OnClientActiveTabChanged="ActiveTabChanged" >
                        <asp:TabPanel runat="server" HeaderText="Address"  ID="tblAddress">
                            <ContentTemplate>
                                <div class="row" id="divAddress">
                                    <iframe runat="server" src="VendorAddressDetails.aspx?Master=Simple" id="ifrmSearch" style="height: 600px;" class="col-lg-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>
                        <asp:TabPanel runat="server" HeaderText="Contacts"  ID="tblContacts">
                            <ContentTemplate>
                                <div class="row" id="divContact">
                                    <iframe runat="server" src="VendorContactDetails.aspx?Master=Simple" id="IfrmSample" style="height: 600px;" class="col-lg-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>
                        <asp:TabPanel runat="server" HeaderText="Statutory Info"  ID="tblStatutory">
                            <ContentTemplate>
                                <div class="row" id="divStatutoryInfo">
                                    <iframe runat="server" src="VendorStatutoryInfo.aspx?Master=Simple" id="IframeInfoStatuInfo" style="height: 600px;" class=" col-lg-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>

                          <asp:TabPanel runat="server" HeaderText="Bank Details" ID="tblItemMap">
                            <ContentTemplate>
                                <div class="row" id="divBankDetails">
                                    <iframe runat="server" src="VendorBankDetails.aspx?Master=Simple" id="iframeItemMap" style="height: 600px;" class=" col-lg-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>

                         <asp:TabPanel runat="server" HeaderText="Item Mapping" ID="TabPanel2">
                            <ContentTemplate>
                                <div class="row" id="divMappingVendorItem">
                                    <iframe runat="server" src="ItemVendorMapping.aspx?Master=Simple" id="iframe2" style="height: 600px;" class=" col-lg-12 col-md-12 col-sm-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>
                    </asp:TabContainer>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        function ActiveTabChanged(sender, e) {
            var activeTab = sender.get_activeTabIndex();
            if (activeTab == 1) {
                $("#divContact").html("<iframe  src='VendorContactDetails.aspx?Master=Simple'  id='frmSample' style='height: 600px;' class='col-lg-12 iframe'></iframe>");
            }
            if(activeTab==3)
            {
                $("#divItemMapping").html("<iframe  src='ItemVendorMapping.aspx?Master=Simple'  id='frmItemSample' style='height: 600px;' class='col-lg-12 iframe'></iframe>");
            }
        }
    </script>
</asp:Content>
