﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="T_DebitNoteSummary" Codebehind="T_DebitNoteSummary.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function checkClass(no) {
            debugger;
            if (no == 1) {
                $("#divSearch").removeClass("in");
            }
            if (no == 2) {
                $("#divCancel").removeClass("in");
            }
            if (no == 3) {
                $("#divCancel").addClass("in");
            }


        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <div id="divButtons" class="pull-right">
                                <br />
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm  btn-primary" onclick="checkClass(2)" data-toggle="collapse" data-target="#divSearch">
                                        <span class="glyphicon glyphicon-search"></span>Search Tools
                                    </button>
                                    <button type="button" class="btn btn-sm  btn-primary" onclick="checkClass(1)" data-toggle="collapse" data-target="#divCancel">
                                        Cancel
                                    </button>
                                    <asp:Button runat="server" Text="Search" CssClass="btn btn-sm  btn-primary" ID="btnSearch" OnClick="btnSearch_Click"></asp:Button>
                                    <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="btn  btn-sm  btn-primary" OnClick="btnDelete_Click"></asp:Button>

                                    <asp:Button runat="server" ID="btnApproved" Text="Approve" CssClass="btn  btn-sm  btn-primary" OnClick="btnApproved_Click"></asp:Button>
                                    <asp:Button runat="server" ID="btnNewDebitNote" Text="New Debit Voucher" CssClass="btn  btn-sm  btn-primary" OnClick="btnNewDebitNote_Click"></asp:Button>
                                </div>
                            </div>
                            <h3>Debit Voucher Summary </h3>
                            <hr />
                            <div id="divSearch" class="collapse">
                                <div class="row" runat="server">
                                    <div class="col-lg-2">
                                        <asp:TextBox ID="txtDebitNoteNo" runat="server" placeholder="Enter Debit Voucher No" CssClass=" form-control input-sm"></asp:TextBox>
                                    </div>
                                    <div id="divVoucherFromDate" class="col-lg-2">
                                        <div class="input-group">
                                            <asp:TextBox ID="txtVoucherFromDate" TabIndex="1" runat="server" placeholder="Voucher Date" CssClass="form-control input-sm" ValidationGroup="MainSave"></asp:TextBox>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <asp:MaskedEditExtender ID="Mee_txtVoucherFromDate" runat="server"
                                            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtVoucherFromDate">
                                        </asp:MaskedEditExtender>
                                        <asp:CalendarExtender ID="Ce_txtVoucherFromDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtVoucherFromDate"
                                            TargetControlID="txtVoucherFromDate">
                                        </asp:CalendarExtender>
                                    </div>
                                    <div id="divVoucherToDate" class="col-lg-2">
                                        <div class="input-group">
                                            <asp:TextBox ID="txtVoucherToDate" TabIndex="1" runat="server" placeholder="Voucher Date" CssClass="form-control input-sm" ValidationGroup="MainSave"></asp:TextBox>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <asp:MaskedEditExtender ID="Mee_txtVoucherToDate" runat="server"
                                            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtVoucherToDate">
                                        </asp:MaskedEditExtender>
                                        <asp:CalendarExtender ID="Ce_txtVoucherToDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtVoucherToDate"
                                            TargetControlID="txtVoucherToDate">
                                        </asp:CalendarExtender>
                                    </div>

                                    <div class="col-lg-2">
                                        <asp:TextBox ID="txtDebitNoteAmount" runat="server" placeholder="Debit Voucher Amount" CssClass=" form-control input-sm"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-1">
                                        <asp:DropDownList ID="ddlOperators" runat="server" CssClass="form-control  input-sm">
                                            <asp:ListItem Text="=" Value="1"></asp:ListItem>
                                            <asp:ListItem Text=">=" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="<=" Value="3"></asp:ListItem>
                                            <asp:ListItem Text=">" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="<" Value="5"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-3">
                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control  input-sm">
                                            <asp:ListItem Text="ALL" Value="-32768"></asp:ListItem>
                                            <asp:ListItem Text="Approved" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Created" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Deleted" Value="-10"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div id="divCancel" class="collapse">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <asp:TextBox ID="txtCancelDNNo" runat="server" placeholder="Enter Debit Voucher No" CssClass=" form-control input-sm"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <asp:TextBox ID="txtVoucherDate" TabIndex="1" runat="server" placeholder="Voucher Date" CssClass="form-control input-sm" ValidationGroup="MainSave"></asp:TextBox>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <asp:MaskedEditExtender ID="MEE_txtVoucherDate" runat="server"
                                            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtVoucherDate">
                                        </asp:MaskedEditExtender>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtVoucherDate"
                                            TargetControlID="txtVoucherDate">
                                        </asp:CalendarExtender>
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:Button runat="server" ID="btnCanceled" Text="Cancel" CssClass="btn  btn-sm  btn-primary" OnClick="btnCanceled_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
         <Triggers>
             <asp:PostBackTrigger ControlID="btnCanceled" />
         </Triggers>
    </asp:UpdatePanel>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-body">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="grdDebitNoteSummary" runat="server" AutoGenerateColumns="False" Width="100%"
                            OnPreRender="grdDebitNoteSummary_PreRender" GridLines="Horizontal"
                            CssClass="table table-striped table-hover  text-nowrap  dt-responsive nowrap">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <input id="SelectAll" onclick="SelectAll(this);" runat="server" type="checkbox" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="ChkSelect" Enabled='<%#Convert.ToBoolean(Eval("Status").ToString()=="0"?  1 : 0)%>' />
                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("ID")%>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblDebitNoteNo" Text="Debit Voucher No" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HyperLink TabIndex="1" CausesValidation="false" ID="lbtnCompanyCode" runat="server"
                                            Text='<%# Eval("DebitNoteNo")%>' ToolTip="Edit" NavigateUrl='<% #"T_DebitNote.aspx?DebitNoteID=" + Eval("ID")%>' Target="_blank"></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderDebitNoteDate" Text="DN Date" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDebitNoteDate" runat="server" TabIndex="2" Text='<%# Convert.ToDateTime(Eval("DebitNoteDate")).ToString("dd/MM/yyyy") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderDebitNoteBaseAmount" Text="Total Amount" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDebitNoteBaseAmount" runat="server" TabIndex="2" Text='<%#Eval("TotalAmount") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID="lblHeaderStatusOf" Text="Staus" runat="server" CssClass="bold"></asp:Label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatusOf" runat="server" Text='<%#Eval("StatusName") %>' CssClass="bold"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="grdDebitNoteSummary" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var GridID = $get('<%=grdDebitNoteSummary.ClientID%>');
        $(document).ready(function () {
            GridUI(GridID, 50);
        })
    </script>
</asp:Content>
