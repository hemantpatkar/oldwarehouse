﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class T_VendorPaymentDetails : BigSunPage
{

    #region PageLoad
    AppObjects.AppPaymentVoucher objPaymentVoucher = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        objPaymentVoucher = new AppPaymentVoucher(intRequestingUserID);
        if (!IsPostBack)
        {
            Session[TableConstants.VendorPaymentDetails] = objPaymentVoucher;
            ACEtxtVendor.ContextKey = new AppConvert((int)AppObjects.CompanyRoleType.Vendor);
            Ce_PaymentDate.SelectedDate = DateTime.Today;
            BindToDropdown();
            string ID = Convert.ToString(Request.QueryString["PaymentVoucherID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int PaymentVoucherID = new AppUtility.AppConvert(ID);
                objPaymentVoucher = new AppObjects.AppPaymentVoucher(intRequestingUserID, PaymentVoucherID);
                Session[TableConstants.VendorPaymentDetails] = objPaymentVoucher;
                PopulateData();
            }
            else
            {
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
                Session[TableConstants.VendorPaymentDetails] = objPaymentVoucher;
            }
            BindData();
        }
    }
    #endregion
    #region Text Events
    protected void txtVendorName_TextChanged(object sender, EventArgs e)
    {
        objPaymentVoucher = (AppPaymentVoucher)Session[TableConstants.VendorPaymentDetails];
        int VendorCompanyID = new AppConvert(hdfVendorID.Value);
        objPaymentVoucher.Vendor_Company_ID = VendorCompanyID;
        BindVendorDetails(VendorCompanyID);
        BindData();
    }
    protected void txtPaidAmt_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        CalculateNetAmount(RowIndex);
    }
    #endregion
    #region Functions
    public void BindVendorDetails(int VendorCompanyID)
    {
        AppCompany objVendorDetails = new AppCompany(intRequestingUserID, VendorCompanyID);
        CommonFunctions.BindDropDown(intRequestingUserID, Components.CompanyBank, ddlVendorAccountNo, "AccountNo", "ID", "", false, VendorCompanyID);
        if (ddlVendorAccountNo.Items.Count > 0) { ddlVendorAccountNo.SelectedValue = new AppConvert(objVendorDetails.DefaultCompanyBank.ID); }
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlVendorBankBranch, "Name", "ID", "", true, VendorCompanyID);
        if (ddlVendorBankBranch.Items.Count > 0) { ddlVendorBankBranch.SelectedValue = new AppConvert(objVendorDetails.DefaultCompanyBank.BankType_ID); }
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlVendorBank, "BankBranch", "ID", "", true, VendorCompanyID);
        if (ddlVendorBank.Items.Count > 0) { ddlVendorBank.SelectedValue = new AppConvert(objVendorDetails.DefaultCompanyBank.BankType_ID); }
        txtPayee.Text = objVendorDetails.Name;
    }
    #region BindData   
    protected void BindData()
    {
        objPaymentVoucher = (AppPaymentVoucher)Session[TableConstants.VendorPaymentDetails];
        if (txtNoOfDueDays.Text != "")
        {
            //objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseOrder.PurchaseOrderNumber, AppUtility.Operators.Equals, txtNoOfDueDays.Text, 0);
        }
        if (hdfVendorID.Value != "")
        {
            objPaymentVoucher.Vendor_Company_ID = new AppConvert(hdfVendorID.Value);
        }
        objPaymentVoucher.GetAllPaymentOrderDetails();
        grdPurchaseBillDetails.DataSource = objPaymentVoucher.PaymentVoucherDetailColl;
        grdPurchaseBillDetails.DataBind();
        divBillDetails.Visible = true;
        Session.Add(TableConstants.VendorPaymentDetails, objPaymentVoucher);
    }
    #endregion
    public void BindToDropdown()
    {
        CommonFunctions.BindDropDown(intRequestingUserID, Components.PaymentType, ddlPaymentType, "PaymentTypeName", "ID");
        CommonFunctions.BindDropDown(intRequestingUserID, Components.CompanyBank, ddlCompanyAccountNo, "AccountNo", "ID", "", false, LoginCompanyID);
        ddlCompanyAccountNo.SelectedValue = new AppConvert(DefaultCompanyBank.ID);
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCompanyBankBranch, "BankBranch", "ID", "", true, LoginCompanyID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(DefaultCompanyBank.BankType_ID);
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCompanyBank, "Name", "ID", "", true, LoginCompanyID);
        ddlCompanyBank.SelectedValue = new AppConvert(DefaultCompanyBank.BankType_ID);
        SetBankDetails();
    }
    public void SetBankDetails()
    {
        int PaymentType = new AppConvert(ddlPaymentType.SelectedValue);
        switch (PaymentType)
        {
            case 1:
                divBankDetails.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            case 2:
                divBankDetails.Visible = true;
                divChequeNo.Visible = true;
                divChequeDate.Visible = true;
                break;
            case 3:
                divBankDetails.Visible = true;
                divChequeNo.Visible = true;
                divChequeDate.Visible = true;
                break;
            case 4:
                divBankDetails.Visible = true;
                divChequeNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            case 5:
                divBankDetails.Visible = true;
                divChequeNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            default:
                divBankDetails.Visible = false;
                divChequeNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
        }
    }
    public void CalculateNetAmount(int RowIndex)
    {
        AppPaymentVoucher objPV = (AppPaymentVoucher)Session[TableConstants.VendorPaymentDetails];
        TextBox txtPaidAmt = (TextBox)grdPurchaseBillDetails.Rows[RowIndex].FindControl("txtPaidAmt");
        Label lblBillAmount = (Label)grdPurchaseBillDetails.Rows[RowIndex].FindControl("lblBillAmount");
        Label lblPaidAmount = (Label)grdPurchaseBillDetails.Rows[RowIndex].FindControl("lblPaidAmount");
        Label lblBalanceAmount = (Label)grdPurchaseBillDetails.Rows[RowIndex].FindControl("lblBalanceAmount");
        decimal NetPayableAmount = 0;
        decimal CurrentBillPaidAmount = 0;
        decimal AlreadyCurrentBillPaid = 0;
        decimal BillAmount = 0;
        decimal TotalPaidAmount = 0;
        decimal BalanceAmount = 0;
        NetPayableAmount = new AppConvert(txtNetPayable.Text);
        CurrentBillPaidAmount = new AppConvert(txtPaidAmt.Text);
        AlreadyCurrentBillPaid = objPV.PaymentVoucherDetailColl[RowIndex].Amount;
        BillAmount = objPV.PaymentVoucherDetailColl[RowIndex].BillAmount;
        TotalPaidAmount = objPV.PaymentVoucherDetailColl[RowIndex].PaidAmount;
        BalanceAmount = objPV.PaymentVoucherDetailColl[RowIndex].BalanceAmount;
        if (CurrentBillPaidAmount > 0)
        {
            if (CurrentBillPaidAmount > BalanceAmount)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Amount should not be greater than Balance Amount','2'" + ");", true);
                CurrentBillPaidAmount = 0;
            }
            else
            {
                TotalPaidAmount = TotalPaidAmount + CurrentBillPaidAmount;
                BalanceAmount = BillAmount - TotalPaidAmount;
            }
            txtPaidAmt.Enabled = true;
        }
        else
        {
            txtPaidAmt.Enabled = false;
        }
        NetPayableAmount = (NetPayableAmount - AlreadyCurrentBillPaid) + CurrentBillPaidAmount;
        txtNetPayable.Text = new AppConvert(NetPayableAmount);
        lblPaidAmount.Text = new AppConvert(TotalPaidAmount);
        lblBalanceAmount.Text = new AppConvert(BalanceAmount);
        txtPaidAmt.Text = new AppConvert(CurrentBillPaidAmount);
        objPV.PaymentVoucherDetailColl[RowIndex].Amount = new AppConvert(CurrentBillPaidAmount);
        objPV.PaymentVoucherDetailColl[RowIndex].Ledger_ID = objPV.Vendor_Company.Vendor_Ledger_ID;
        objPV.PaymentVoucherDetailColl[RowIndex].Advance = 0;
        objPV.PaymentVoucherDetailColl[RowIndex].ModifiedOn = DateTime.Now;
        objPV.PaymentVoucherDetailColl[RowIndex].ModifiedBy = intRequestingUserID;
        objPV.PaymentVoucherDetailColl[RowIndex].Status = new AppConvert((int)RecordStatus.Created);
        objPV.PaymentVoucherDetailColl[RowIndex].Type = 0;
        Session[TableConstants.VendorPaymentDetails] = objPV;
    }
    public void PopulateData()
    {
        AppPaymentVoucher objPaymentVoucher = (AppPaymentVoucher)Session[TableConstants.VendorPaymentDetails];
        txtPaymentDate.Text = new AppConvert(objPaymentVoucher.PaymentVoucherDate);
        ddlPaymentType.SelectedValue = new AppConvert(objPaymentVoucher.PaymentType_ID);
        ddlCompanyAccountNo.SelectedValue = new AppConvert(objPaymentVoucher.From_CompanyBank_ID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objPaymentVoucher.From_CompanyBank.BankType.Name);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objPaymentVoucher.From_CompanyBank.BankType.BankBranch);
        if (objPaymentVoucher.PaymentType_ID > 1)
        {
            BindVendorDetails(objPaymentVoucher.Vendor_Company_ID);
            txtChequeNo.Text = new AppConvert(objPaymentVoucher.ChequeNo);
            txtChequeDate.Text = new AppConvert(objPaymentVoucher.ChequeDate);
            divChequeNo.Visible = true;
            divChequeDate.Visible = true;
            ddlVendorAccountNo.SelectedValue = new AppConvert(objPaymentVoucher.To_CompanyBank_ID);
            ddlVendorBankBranch.SelectedValue = new AppConvert(objPaymentVoucher.To_CompanyBank.BankType.BankBranch);
            ddlVendorBank.SelectedValue = new AppConvert(objPaymentVoucher.To_CompanyBank.BankType.Name);
            divBankDetails.Visible = true;
        }
        else
        {
            divBankDetails.Visible = false;
        }
        txtVendor.Text = new AppConvert(objPaymentVoucher.Vendor_Company.Name);
        hdfVendorName.Value = new AppConvert(objPaymentVoucher.Vendor_Company.Name);
        hdfVendorID.Value = new AppConvert(objPaymentVoucher.Vendor_Company_ID);
        txtNetPayable.Text = new AppConvert(objPaymentVoucher.Amount);
        txtPayee.Text = new AppConvert(objPaymentVoucher.Vendor_Company.Name);
        txtLedger.Text = new AppConvert(objPaymentVoucher.Ledger.LedgerName);
        hdfLedgerName.Value = new AppConvert(objPaymentVoucher.Ledger.LedgerName);
        hdfLedgerID.Value = new AppConvert(objPaymentVoucher.Ledger_ID);
        txtVoucherNo.Text = new AppConvert(objPaymentVoucher.PaymentVoucherNo);
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(objPaymentVoucher.CurrencyExchID);
        //hdfHeaderExchangeCurrencyName.Value = new AppConvert(objPaymentVoucher.cu);
        txtHeaderExchangeCurrency.Text = "";
        
        txtCurrency.Text = new AppConvert(objPaymentVoucher.CurrencyType.Name);
        hdfCurrencyID.Value = new AppConvert(objPaymentVoucher.CurrencyType_ID);
        hdfCurrencyName.Value = new AppConvert(objPaymentVoucher.CurrencyType.Name);


        txtPaymentDate.Text = new AppConvert(objPaymentVoucher.PaymentVoucherDate);
        objPaymentVoucher.GetAllPaymentOrderDetails();
        grdPurchaseBillDetails.DataSource = objPaymentVoucher.PaymentVoucherDetailColl;
        grdPurchaseBillDetails.DataBind();
        Session[TableConstants.VendorPaymentDetails] = objPaymentVoucher;
    }
    #endregion
    #region Dropdown Events
    protected void ddlPaymentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetBankDetails();
        int Payment_Type = new AppConvert(ddlPaymentType.SelectedValue);
        if (Payment_Type > 1)
        {
            AppCompanyBank ObjCompanyBank = new AppCompanyBank(intRequestingUserID, Payment_Type);
            txtLedger.Text = ObjCompanyBank.Ledger.LedgerName;
            hdfLedgerName.Value = ObjCompanyBank.Ledger.LedgerName;
            hdfLedgerID.Value = new AppConvert(ObjCompanyBank.Ledger_ID);
        }
        else
        {
            txtLedger.Text = "";
            hdfLedgerName.Value = "";
            hdfLedgerID.Value = "";
        }
    }
    protected void ddlCompanyAccountNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Company_Bank_Id = new AppConvert(ddlCompanyAccountNo.SelectedValue);
        AppCompanyBank objCOmpanyBank = new AppCompanyBank(intRequestingUserID, Company_Bank_Id);
        ddlCompanyBank.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
    }
    protected void ddlVendorAccountNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Company_Bank_Id = new AppConvert(ddlVendorAccountNo.SelectedValue);
        AppCompanyBank objCOmpanyBank = new AppCompanyBank(intRequestingUserID, Company_Bank_Id);
        ddlCompanyBank.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
    }
    #endregion
    #region Button Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppPaymentVoucher PV = (AppPaymentVoucher)Session[TableConstants.VendorPaymentDetails];
        string Message = "Record successfully Saved";
        if (PV.ID == 0)
        {
            PV.PaymentVoucherNo = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "VP", System.DateTime.Now, 4, ResetCycle.Yearly));
        }
        else
        {
            Message = "Record successfully Updated";
            PV.IsRecordModified = true;
        }
        int Payment_Type = new AppConvert(ddlPaymentType.SelectedValue);
        if ((Payment_Type==2 || Payment_Type==3))
        {
            if(string.IsNullOrEmpty(txtChequeNo.Text) == true)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter cheque no','2'" + ");", true);
                return;
            }
            if (string.IsNullOrEmpty(txtChequeDate.Text) == true)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Cheque Date','2'" + ");", true);
                return;
            }
        }

        PV.Company_ID = new AppConvert(LoginCompanyID);
        PV.VoucherType_ID = 10;
        PV.Vendor_Company_ID = new AppConvert(hdfVendorID.Value);
        PV.PaymentVoucherDate = new AppConvert(txtPaymentDate.Text);
        PV.PaymentType_ID = new AppConvert(ddlPaymentType.SelectedValue);
        PV.Amount = new AppConvert(txtNetPayable.Text);
        PV.Description = txtDescription.Text;
        PV.Vendor_Company_ID = new AppConvert(hdfVendorID.Value);
        PV.Ledger_ID = new AppConvert(hdfLedgerID.Value);// PV.Vendor_Company.Vendor_Ledger_ID; 
        PV.ChequeNo = new AppConvert(txtChequeNo.Text);
        PV.ChequeDate = new AppConvert(txtChequeDate.Text);
        PV.CurrencyType_ID = new AppConvert(hdfCurrencyID.Value);
        PV.CurrencyExchID = new AppConvert(hdfHeaderExchangeCurrencyID.Value);
        PV.ExchangeRate = new AppConvert(txtHeaderExchangeCurrencyRate.Text);
        PV.From_CompanyBank_ID = new AppConvert(ddlCompanyAccountNo.SelectedValue);
        PV.To_CompanyBank_ID = new AppConvert(ddlVendorAccountNo.SelectedValue);
        PV.Advance = 0;
        PV.ModifiedBy = intRequestingUserID;
        PV.ModifiedOn = System.DateTime.Now;
        PV.Status = new AppConvert((int)RecordStatus.Created);
        PV.Type = 0;
        PV.Save();
        PV.AddNewPaymentAllocationDetails();
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + Message + "','1'" + ");", true);


        #region Add JournalEntries
        LedgerEntry(LoginCompanyID, 100301, 1003, PV.Ledger_ID, PV.ID, PV.PaymentVoucherDate, PV.Amount, 0, PV.PaymentVoucherNo, PV.ModifiedBy, PV.ModifiedOn,0, PV.Status, PV.Type,
                              0, 0, PV.VoucherType_ID, PV.Description, "", PV.PaymentType_ID);
        if (PV.PaymentVoucherDetailColl.Count > 0)
        {
            foreach (var item in PV.PaymentVoucherDetailColl)
            {
                if (item.Amount != 0)
                {
                    LedgerEntry(LoginCompanyID, 100401, 1004, item.Ledger_ID, item.ID, PV.PaymentVoucherDate, 0, item.Amount, PV.PaymentVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, PV.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, PV.VoucherType_ID, item.PaymentVoucherDesc, "", PV.PaymentType_ID);
                }
            }

        }
        #endregion

        Session.Remove(TableConstants.VendorPaymentDetails);
        Response.Redirect("T_VendorPaymentSummary.aspx");

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.VendorPaymentDetails);
        Response.Redirect("T_VendorPaymentSummary.aspx");
    }
    #endregion
    #region GridView Events
    protected void grdPurchaseBillDetails_PreRender(object sender, EventArgs e)
    {
        if (grdPurchaseBillDetails.Rows.Count > 0)
        {
            grdPurchaseBillDetails.UseAccessibleHeader = true;
            grdPurchaseBillDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region CheckBox Events
    public static bool CheckItemSelectedinGrid(GridView Grid, string CheckBoxName)
    {
        if (Grid.Rows.Count == 0)
        {
            return false;
        }
        for (int i = 0; i < Grid.Rows.Count; i++)
        {
            CheckBox chkSelect = (CheckBox)Grid.Rows[i].FindControl(CheckBoxName);
            if (chkSelect.Checked == true)
            {
                return true;
            }
        }
        return false;
    }
    protected void ChkSelect_CheckedChanged(object sender, EventArgs e)
    {
       
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        AppPaymentVoucher objPV = (AppPaymentVoucher)Session[TableConstants.VendorPaymentDetails];
        CheckBox ChkSelect = (CheckBox)grdPurchaseBillDetails.Rows[RowIndex].FindControl("ChkSelect");
        #region Ubwanted COde
        //TextBox txtPaidAmt = (TextBox)grdPurchaseBillDetails.Rows[RowIndex].FindControl("txtPaidAmt");
        //Label lblBillAmount = (Label)grdPurchaseBillDetails.Rows[RowIndex].FindControl("lblBillAmount");
        //Label lblPaidAmount = (Label)grdPurchaseBillDetails.Rows[RowIndex].FindControl("lblPaidAmount");
        //Label lblBalanceAmount = (Label)grdPurchaseBillDetails.Rows[RowIndex].FindControl("lblBalanceAmount");
        //decimal currentPaidAmount = new AppConvert(txtPaidAmt.Text);
        //decimal NetPayable = new AppConvert(txtNetPayable.Text);
        //decimal AlreadyPaidAmount = objPV.PaymentVoucherDetailColl[RowIndex].Amount;

        //if (ChkSelect.Checked == true)
        //{
        //    txtPaidAmt.Text = lblBalanceAmount.Text;
        //    txtPaidAmt.Enabled = true;
        //    txtPaidAmt.Focus();
        //    NetPayable = (NetPayable - currentPaidAmount) + new AppConvert(lblBalanceAmount.Text);
        //}
        //else
        //{
        //    txtPaidAmt.Text = new AppConvert(objPV.Amount);
        //    txtPaidAmt.Enabled = false;
        //    NetPayable = (NetPayable - currentPaidAmount) + AlreadyPaidAmount;

        //}
        //txtNetPayable.Text = new AppConvert(NetPayable);
        //objPV.PaymentVoucherDetailColl[RowIndex].Amount = new AppConvert(txtPaidAmt.Text);
        //objPV.PaymentVoucherDetailColl[RowIndex].Ledger_ID = objPV.Vendor_Company.Vendor_Ledger_ID;
        //objPV.PaymentVoucherDetailColl[RowIndex].Advance = 0;
        //objPV.PaymentVoucherDetailColl[RowIndex].ModifiedOn = DateTime.Now;
        //objPV.PaymentVoucherDetailColl[RowIndex].ModifiedBy = intRequestingUserID;
        //objPV.PaymentVoucherDetailColl[RowIndex].Status = new AppConvert((int)RecordStatus.Active);
        //objPV.PaymentVoucherDetailColl[RowIndex].Type = 0;
        //Session.Add(TableConstants.VendorPaymentDetails, objPV);
        #endregion




        TextBox txtPaidAmt = (TextBox)grdPurchaseBillDetails.Rows[RowIndex].FindControl("txtPaidAmt");
        Label lblBillAmount = (Label)grdPurchaseBillDetails.Rows[RowIndex].FindControl("lblBillAmount");
        Label lblPaidAmount = (Label)grdPurchaseBillDetails.Rows[RowIndex].FindControl("lblPaidAmount");
        Label lblBalanceAmount = (Label)grdPurchaseBillDetails.Rows[RowIndex].FindControl("lblBalanceAmount");
        decimal NetPayableAmount = 0;
        decimal CurrentBillPaidAmount = 0;
        decimal AlreadyCurrentBillPaid = 0;
        decimal BillAmount = 0;
        decimal TotalPaidAmount = 0;
        decimal BalanceAmount = 0;
        NetPayableAmount = new AppConvert(txtNetPayable.Text);
        CurrentBillPaidAmount = new AppConvert(txtPaidAmt.Text);
        AlreadyCurrentBillPaid = objPV.PaymentVoucherDetailColl[RowIndex].Amount;
        BillAmount = objPV.PaymentVoucherDetailColl[RowIndex].BillAmount;
        TotalPaidAmount = objPV.PaymentVoucherDetailColl[RowIndex].PaidAmount;
        BalanceAmount = objPV.PaymentVoucherDetailColl[RowIndex].BalanceAmount;


        if(ChkSelect.Checked==true)
        {
            txtPaidAmt.Enabled = true;
            if (CurrentBillPaidAmount > 0)
            {
                if (CurrentBillPaidAmount > BalanceAmount)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Amount should not be greater than Balance Amount','2'" + ");", true);
                    CurrentBillPaidAmount = 0;
                }
                else
                {
                    TotalPaidAmount = TotalPaidAmount + CurrentBillPaidAmount;
                    BalanceAmount = BillAmount - TotalPaidAmount;
                }

            }          
        }
        else
        {
            CurrentBillPaidAmount = 0;
            txtPaidAmt.Enabled = false;
        }
        if (objPV.ID == 0)
        {
            lblPaidAmount.Text = new AppConvert(TotalPaidAmount);
            lblBalanceAmount.Text = new AppConvert(BalanceAmount);
        }
        else
        {

        }

        NetPayableAmount = (NetPayableAmount - AlreadyCurrentBillPaid) + CurrentBillPaidAmount;
        txtNetPayable.Text = new AppConvert(NetPayableAmount);
      

        txtPaidAmt.Text = new AppConvert(CurrentBillPaidAmount);
        objPV.PaymentVoucherDetailColl[RowIndex].Amount = new AppConvert(CurrentBillPaidAmount);
        objPV.PaymentVoucherDetailColl[RowIndex].Ledger_ID = objPV.Vendor_Company.Vendor_Ledger_ID;
        objPV.PaymentVoucherDetailColl[RowIndex].Advance = 0;
        objPV.PaymentVoucherDetailColl[RowIndex].ModifiedOn = DateTime.Now;
        objPV.PaymentVoucherDetailColl[RowIndex].ModifiedBy = intRequestingUserID;
        objPV.PaymentVoucherDetailColl[RowIndex].Status = new AppConvert((int)RecordStatus.Created);
        objPV.PaymentVoucherDetailColl[RowIndex].Type = 0;
        Session[TableConstants.VendorPaymentDetails] = objPV;        
    }
    #endregion     
}