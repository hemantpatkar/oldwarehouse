﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class CustomerItem : BigSunPage
{
    AppCustomer obj;
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = (AppCustomer)Session[TableConstants.CustomerSessionObj];
        if (!IsPostBack)
        {
            BindData();
        }
    }
    protected void grdCustomerItems_PreRender(object sender, EventArgs e)
    {
        if (grdCustomerItems.Rows.Count > 0)
        {
            grdCustomerItems.UseAccessibleHeader = true;
            grdCustomerItems.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdCustomerItems_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grdCustomerItems.EditIndex = -1;
        BindData();
    }
    protected void grdCustomerItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string CommandName = e.CommandName;
        string CommandID = "";
        switch (CommandName)
        {
            case "INSERT":                
                HiddenField hdfItemTypeID = grdCustomerItems.FooterRow.FindControl("hdfNewItemTypeID") as HiddenField;
                CheckBox IsActive = grdCustomerItems.FooterRow.FindControl("chkIsActive") as CheckBox;
                SaveItemDetails(CommandID, hdfItemTypeID.Value, IsActive.Checked);
                BindData();
                break;
            case "ADDNEW":
                AddEmptyRow();
                BindData();
                break;
        }
    }
    protected void grdCustomerItems_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdCustomerItems.ShowFooter = false;
        grdCustomerItems.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdCustomerItems_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string Index = new AppConvert(e.RowIndex);
        HiddenField hdfItemTypeID = (HiddenField)grdCustomerItems.Rows[e.RowIndex].FindControl("hdfItemTypeID");      
        CheckBox chkActive = grdCustomerItems.Rows[e.RowIndex].FindControl("chkActive") as CheckBox;       
        SaveItemDetails(Index, hdfItemTypeID.Value, chkActive.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Item Successfully Updated','1'" + ");", true);
        grdCustomerItems.EditIndex = -1;
        BindData();
    }
    protected void grdCustomerItems_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        obj = (AppCustomer)Session[TableConstants.CustomerSessionObj];
        obj.CustomerItemColl[e.RowIndex].Status = (int)RecordStatus.Deleted;
        Session[TableConstants.CustomerSessionObj] = obj;
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Item Successfully Deleted','1'" + ");", true);
        BindData();
    }
    #region Functions    
    private void BindData()
    {
        if (obj != null)
        {
            AppCustomerItemColl appCustomerItemColl = obj.CustomerItemColl;
            grdCustomerItems.DataSource = appCustomerItemColl;
            grdCustomerItems.DataBind();
            if (appCustomerItemColl.Count <= 0)
            {
                AddEmptyRow();
            }

        }
    }
    public void AddEmptyRow()
    {
        AppCustomerItemColl CustomerItemCollection = new AppCustomerItemColl(intRequestingUserID);
        AppCustomerItem newVendorItem = new AppCustomerItem(intRequestingUserID);
        CustomerItemCollection.Add(newVendorItem);
        newVendorItem.Item.Name = "No Records Found";
        grdCustomerItems.DataSource = CustomerItemCollection;
        grdCustomerItems.DataBind();
        grdCustomerItems.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdCustomerItems.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdCustomerItems.ShowFooter = true;
    }
    #endregion
    #region Save Address
    public void SaveItemDetails(string Id, string ItemID, bool Active)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppCustomerItem SingleObj = new AppCustomerItem(intRequestingUserID);
        if (String.IsNullOrEmpty(Id) == false)
        {
            SingleObj = obj.CustomerItemColl[intCommandID];
        }
        else
        {
            obj.AddNewCustomerItem(SingleObj);
        }
        SingleObj.Company_ID = obj.ID;
        SingleObj.Item_ID = new AppConvert(ItemID);      
        SingleObj.Status = (Active==true? new AppUtility.AppConvert((int)RecordStatus.Active):new AppConvert((int)RecordStatus.InActive));
        //SingleObj.Type = 
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;        
        Session.Add(TableConstants.CustomerSessionObj, obj);
        BindData();
        grdCustomerItems.EditIndex = -1;
    }
    #endregion
}