﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to display all Company lists.
*/
#endregion
#region Using
using System;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.UI;
using AppUtility;
#endregion
public partial class T_PurchaseOrderSummary : BigSunPage
{
    #region PageLoad
    AppPurchaseOrderColl objPO;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }
    #endregion
    #region BindData   
    protected void BindData()
    {
        AppObjects.AppPurchaseOrderColl objColl = new AppPurchaseOrderColl(intRequestingUserID);
        if (txtPurchaeOrderNo.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseOrder.PurchaseOrderNumber, AppUtility.Operators.Equals, txtPurchaeOrderNo.Text, 0);
        }
        if (txtPurchaseOrderAmount.Text != "")
        {
            switch (ddlOperators.SelectedValue)
            {
                case "1":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseOrder.PurchaseOrderBaseAmount, AppUtility.Operators.Equals, txtPurchaseOrderAmount.Text, 0);
                    break;
                case "2":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseOrder.PurchaseOrderBaseAmount, AppUtility.Operators.GreaterOrEqualTo, txtPurchaseOrderAmount.Text, 0);
                    break;
                case "3":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseOrder.PurchaseOrderBaseAmount, AppUtility.Operators.LessOrEqualTo, txtPurchaseOrderAmount.Text, 0);
                    break;
                case "4":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseOrder.PurchaseOrderBaseAmount, AppUtility.Operators.GreaterThan, txtPurchaseOrderAmount.Text, 0);
                    break;
                case "5":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseOrder.PurchaseOrderBaseAmount, AppUtility.Operators.LessThan, txtPurchaseOrderAmount.Text, 0);
                    break;
            }
        }
        if (hdfVendorID.Value != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseOrder.Vendor_Company_ID, AppUtility.Operators.Equals, hdfVendorID.Value, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
            //objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseOrder.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue, 0);
            objColl.AddCriteria(AppObjects.PurchaseOrder.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.PurchaseOrderSummary] = objColl;
        grdPurchaseOrderSummary.DataSource = objColl;
        grdPurchaseOrderSummary.DataBind();
    }
    #endregion
    #region GridView Events
    protected void grdPurchaseOrderSummary_PreRender(object sender, EventArgs e)
    {
        if (grdPurchaseOrderSummary.Rows.Count > 0)
        {
            grdPurchaseOrderSummary.UseAccessibleHeader = true;
            grdPurchaseOrderSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region Button Click Events
    protected void btnNewPurchaseOrder_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.ObjCompanySession);
        Response.Redirect("T_PurchaseOrder.aspx");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (Update(grdPurchaseOrderSummary, new AppConvert((int)RecordStatus.Deleted), 1019, 1020) == 1)
        {
            BindData();
        }
    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        if (Update(grdPurchaseOrderSummary, new AppConvert((int)RecordStatus.Approve), 1019, 1020) == 1)
        {
            BindData();
        }
    }
    protected void btnCanceled_Click(object sender, EventArgs e)
    {
    }
    #endregion
}