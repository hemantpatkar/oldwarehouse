﻿using System;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using System.Globalization;
public partial class CurrencyExchangeRate : BigSunPage
{
    AppExchangeRateDailyColl objDailyExchange;
    AppExchangeRateMonthlyColl objMonthlyExchange;    
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        objDailyExchange = (AppExchangeRateDailyColl)Session[TableConstants.DailyExchangeRateColl];
        objMonthlyExchange = (AppExchangeRateMonthlyColl)Session[TableConstants.MonthlyExchangeRateColl];
        if (!IsPostBack)
        {
            //CommonFunctions.BindCurrencyTypes(ddlMonthlyFrmCurrency, "Name", "ID");
            //CommonFunctions.BindDropDown(intRequestingUserID, Components.CurrencyType, ddlMonthlyFrmCurrency, "Name", "ID");
            CommonFunctions.BindDropDown(intRequestingUserID,Components.CurrencyType,ddlMonthlyFrmCurrency, "Name", "ID");
            CommonFunctions.BindDropDown(intRequestingUserID,Components.CurrencyType,ddlFromCurrency, "Name", "ID");
            CommonFunctions.BindDropDown(intRequestingUserID,Components.CurrencyType,ddlMonthlyToCurrency, "Name", "ID");
            CommonFunctions.BindDropDown(intRequestingUserID,Components.CurrencyType,ddlToCurrency, "Name", "ID");
            BindData();
            BindMonthlyData();
        }
    }
    #endregion
    #region Button Click Events
    protected void btnSaveRate_Click(object sender, EventArgs e)
    {
        AppExchangeRateDailyColl objExchangeDaily = new AppExchangeRateDailyColl(1);
        int fromCurrency = new AppConvert(ddlFromCurrency.SelectedValue);
        int toCurrency = new AppConvert(ddlToCurrency.SelectedValue);
        decimal Rate = new AppConvert(txtExchangeRate.Text);
        DateTime frmDate = new AppConvert(txtFromDate.Text);
        DateTime toDate = new AppConvert(txtEffToDate.Text);
        int ModifiedBy = 1;
        objExchangeDaily.AddExchangeRate(fromCurrency, toCurrency, Rate, frmDate, toDate, ModifiedBy);
        objExchangeDaily.Search();
        grdDailyExchangeRates.DataSource = objExchangeDaily;
        grdDailyExchangeRates.DataBind();
        CommonFunctions.BindCurrencyTypes(ddlToCurrency, "Name", "ID");
        CommonFunctions.BindCurrencyTypes(ddlFromCurrency, "Name", "ID");
        ClearExchangeFields(1);
        Session.Add(TableConstants.DailyExchangeRateColl, objExchangeDaily);
    }
    protected void lbtnAddNew_Click(object sender, EventArgs e)
    {
        divDailyEx.Visible = true;
    }
    protected void lbtnAddMonthlyRate_Click(object sender, EventArgs e)
    {
        //ClearMonthlyExhangeFields();
        divMonthlyEx.Visible = true;
    }
    protected void btnSaveMonthlyRate_Click(object sender, EventArgs e)
    {
        AppExchangeRateMonthly objMonthlyExchangeRate = new AppExchangeRateMonthly(1);
        string strCommandID = hdfMonthlyExId.Value;
        int intCommandID = new AppUtility.AppConvert(strCommandID);
        if (String.IsNullOrEmpty(strCommandID) == false)
        {
            objMonthlyExchangeRate.ID = intCommandID;
        }
        objMonthlyExchangeRate.From_CurrencyType_ID = new AppConvert(ddlMonthlyFrmCurrency.SelectedValue);
        objMonthlyExchangeRate.To_CurrencyType_ID = new AppConvert(ddlMonthlyToCurrency.SelectedValue);
        objMonthlyExchangeRate.Rate = new AppConvert(txtRate.Text);
        DateTime EffDate = new AppConvert(txtEffectiveToDate.Text);
        objMonthlyExchangeRate.Month = new AppConvert(EffDate.Month);
        objMonthlyExchangeRate.Year = new AppConvert(EffDate.Year);
        objMonthlyExchangeRate.ModifiedBy = 1;
        objMonthlyExchangeRate.Save();
        BindMonthlyData();
        ClearExchangeFields(2);
    }
    protected void btnResetMonthly_Click(object sender, EventArgs e)
    {
        ClearExchangeFields(2);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearExchangeFields(1);
    }
    #endregion
    #region Functions   
    protected void BindData()
    {
        AppObjects.AppExchangeRateDailyColl objExchangeDaily = new AppExchangeRateDailyColl(intRequestingUserID);
        objExchangeDaily.AddCriteria(ExchangeRateDaily.Status, Operators.Equals, 0);
        objExchangeDaily.AddCriteria(ExchangeRateDaily.ModifiedBy, Operators.Equals, 0);
        objExchangeDaily.AddCriteria(ExchangeRateDaily.ID, Operators.Equals, 0);
        objExchangeDaily.Search(RecordStatus.ALL);
        grdDailyExchangeRates.DataSource = objExchangeDaily;
        grdDailyExchangeRates.DataBind();
        Session.Add(TableConstants.DailyExchangeRateColl, objExchangeDaily);
        if (objExchangeDaily.Count > 0)
        {
            divDailyEx.Visible = false;
        }
    }
    protected void SetDataForEdit(int index)
    {
        AppObjects.AppExchangeRateDaily objEdit = null;
        if (index >= 0)
        {
            objEdit = objDailyExchange[index];
        }
        ddlFromCurrency.SelectedValue = new AppConvert(objEdit.From_CurrencyType_ID);
        ddlToCurrency.SelectedValue = new AppConvert(objEdit.To_CurrencyType_ID);
        txtExchangeRate.Text = new AppConvert(objEdit.Rate);
        txtFromDate.Text = new AppConvert(objEdit.Date);
        txtEffToDate.Text = new AppConvert(objEdit.Date);
        hdfDailyExId.Value = new AppUtility.AppConvert(index);
        divDailyEx.Visible = true;
    }
    public void BindMonthlyData()
    {
        AppExchangeRateMonthlyColl appMonthlyColl = new AppExchangeRateMonthlyColl(1);
        appMonthlyColl.Search();
        grdMonthlyRates.DataSource = appMonthlyColl;
        grdMonthlyRates.DataBind();
        Session.Add(TableConstants.MonthlyExchangeRateColl, appMonthlyColl);
        if (appMonthlyColl.Count > 0)
        {
            divMonthlyEx.Visible = false;
        }
    }
    protected void SetMonthlyRateForEdit(int index)
    {
        AppExchangeRateMonthly objEdit = null;
        if (index >= 0)
        {
            objEdit = objMonthlyExchange[index];
        }
        ddlMonthlyFrmCurrency.SelectedValue = new AppConvert(objEdit.From_CurrencyType_ID);
        ddlMonthlyToCurrency.SelectedValue = new AppConvert(objEdit.To_CurrencyType_ID);
        txtRate.Text = new AppConvert(objEdit.Rate);
        string Date = objEdit.Year + " / " + objEdit.Month;
        string strMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(objEdit.Month) + "-" + objEdit.Year;
        txtEffectiveToDate.Text = strMonthName;
        hdfMonthlyExId.Value = new AppConvert(index);
        divMonthlyEx.Visible = true;
    }
    public void ClearExchangeFields(int Type)
    {
        if (Type == 1)
        {
            //ddlFromCurrency.SelectedValue = "0";
            //ddlToCurrency.SelectedValue = "0";
            txtExchangeRate.Text = "";
            txtFromDate.Text = "";
            txtEffToDate.Text = "";
            hdfDailyExId.Value = "";
        }
        else
        {
            //ddlMonthlyFrmCurrency.SelectedValue = "0";
            //ddlMonthlyToCurrency.SelectedValue = "0";
            txtRate.Text = "";
            txtEffectiveToDate.Text = "";
            hdfMonthlyExId.Value = "";
        }
    }
    #endregion
    #region GridView Events   
    protected void grdDailyExchangeRates_RowEditing(object sender, GridViewEditEventArgs e)
    {
        SetDataForEdit(e.NewEditIndex);
    }
    protected void grdDailyExchangeRates_PreRender(object sender, EventArgs e)
    {
        if (grdDailyExchangeRates.Rows.Count > 0)
        {
            grdDailyExchangeRates.UseAccessibleHeader = true;
            grdDailyExchangeRates.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdDailyExchangeRates_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string CommandName = e.CommandName.ToString();
        string CommandArgument = e.CommandArgument.ToString();
        int CommandId = new AppConvert(CommandArgument);
        if (CommandName == "DELETE")
        {
            objDailyExchange[CommandId].Status = 0;
            objDailyExchange[CommandId].Save();
            Session.Add(TableConstants.DailyExchangeRateColl, objDailyExchange);
        }
    }
    protected void grdMonthlyRates_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string CommandName = e.CommandName.ToString();
        string CommandArgument = e.CommandArgument.ToString();
        int CommandId = new AppConvert(CommandArgument);
        if (CommandName == "DELETE")
        {
            objMonthlyExchange[CommandId].Status = 0;
            objMonthlyExchange[CommandId].Save();
            Session.Add(TableConstants.MonthlyExchangeRateColl, objMonthlyExchange);
        }
    }
    protected void grdMonthlyRates_PreRender(object sender, EventArgs e)
    {
        if (grdMonthlyRates.Rows.Count > 0)
        {
            grdMonthlyRates.UseAccessibleHeader = true;
            grdMonthlyRates.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdMonthlyRates_RowEditing(object sender, GridViewEditEventArgs e)
    {
        SetMonthlyRateForEdit(e.NewEditIndex);
    }
    #endregion    
}