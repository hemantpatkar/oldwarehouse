﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppUtility;
using AppObjects;
public partial class M_ProfitCenter : BigSunPage
{
    AppProfitCenterColl obj;
    protected void Page_Load(object sender, EventArgs e)
    {
  
        obj = new AppObjects.AppProfitCenterColl(intRequestingUserID);
        Session[TableConstants.SessionProfitCenter] = obj;
        if (!IsPostBack)
        {
           BindData();
        }
    }
    #region GridViewEvents
   
    protected void grdProfitCenter_PreRender(object sender, EventArgs e)
    {
        if (grdProfitCenter.Rows.Count > 0)
        {
            grdProfitCenter.UseAccessibleHeader = true;
            grdProfitCenter.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdProfitCenter_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if (strCommandName == "INSERT")
        {       
            HiddenField hdfProfitCenterID = grdProfitCenter.FooterRow.FindControl("hdfProfitCenterID") as HiddenField;
            TextBox txtProfitCenterCode = grdProfitCenter.FooterRow.FindControl("txtNewProfitCenterCode") as TextBox;
            TextBox txtProfitCenterName = grdProfitCenter.FooterRow.FindControl("txtNewProfitCenterName") as TextBox;
            CheckBox chkActiveProfitCenter = grdProfitCenter.FooterRow.FindControl("chkActiveProfitCenter") as CheckBox;
            CheckBox chkDefaultProfitCenter = grdProfitCenter.FooterRow.FindControl("chkDefaultProfitCenter") as CheckBox;
            SaveProfitCenterDetails(strCommandID, txtProfitCenterCode.Text, txtProfitCenterName.Text, chkActiveProfitCenter.Checked, chkDefaultProfitCenter.Checked);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Profit Center Successfully Added','1'" + ");", true);
            grdProfitCenter.EditIndex = -1;
            BindData();
        }
        else if (strCommandName == "CANCELADD")
        {
            grdProfitCenter.ShowFooter = false;
            BindData();
        }
        else if (strCommandName == "CANCELEDIT")
        {
            grdProfitCenter.EditIndex = -1;
            BindData();
        }
        else if(strCommandName=="ADDNEW")
        {
            grdProfitCenter.EditIndex = -1;
            grdProfitCenter.ShowFooter = true;
            BindData();
        }
    }
    protected void grdProfitCenter_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdProfitCenter.ShowFooter = false;
        grdProfitCenter.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdProfitCenter_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HiddenField hdfID = grdProfitCenter.Rows[e.RowIndex].FindControl("hdfID") as HiddenField;
        AppProfitCenter SingleObj = new AppProfitCenter(intRequestingUserID);
        SingleObj.ID = new AppConvert(hdfID.Value);
        SingleObj.Status = new AppConvert((int)RecordStatus.Deleted);
        SingleObj.Save();
     
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Profit Center Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void grdProfitCenter_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strCommandID = new AppConvert(e.RowIndex);
        HiddenField hdfProfitCenterID = grdProfitCenter.Rows[e.RowIndex].FindControl("hdfProfitCenterID") as HiddenField;
        TextBox txtProfitCenterCode = grdProfitCenter.Rows[e.RowIndex].FindControl("txtProfitCenterCode") as TextBox;
        TextBox txtProfitCenterName = grdProfitCenter.Rows[e.RowIndex].FindControl("txtProfitCenterName") as TextBox;
        CheckBox chkIsActiveProfitCenter = grdProfitCenter.Rows[e.RowIndex].FindControl("chkIsActiveProfitCenter") as CheckBox;
        CheckBox chkIsDefaultProfitCenter = grdProfitCenter.Rows[e.RowIndex].FindControl("chkIsDefaultProfitCenter") as CheckBox;

        SaveProfitCenterDetails(hdfProfitCenterID.Value, txtProfitCenterCode.Text, txtProfitCenterName.Text , chkIsActiveProfitCenter.Checked, chkIsDefaultProfitCenter.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Profit Center Successfully Updated','1'" + ");", true);
        grdProfitCenter.EditIndex = -1;
        BindData();
    }
    #endregion
    #region MiscFunctions
    public void SaveProfitCenterDetails(string Id, string ProfitCenterCode, string ProfitCenterName, bool IsActive, bool IsDefault)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppProfitCenter SingleObj = new AppProfitCenter(intRequestingUserID);      
        SingleObj.ID = new AppConvert(Id);
        SingleObj.ProfitCenterCode = new AppConvert(ProfitCenterCode);
        SingleObj.ProfitCenterName = new AppConvert(ProfitCenterName);
        SingleObj.Status = (IsActive == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        if (IsDefault == true)
        {
           
            obj.SetDefaultProfitCenter();
        }
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;

        Session.Add(TableConstants.SessionProfitCenter, obj);
        SingleObj.Save();
        BindData();
    }
    

    protected void BindData()
    {
        AppObjects.AppProfitCenterColl objColl = new AppProfitCenterColl(intRequestingUserID);
        if (!string.IsNullOrEmpty(txtProfitCenterName.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ProfitCenter.ProfitCenterName, AppUtility.Operators.Equals, txtProfitCenterName.Text, 0);
        }
        if (!string.IsNullOrEmpty(txtProfitCenterCode.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.ProfitCenter.ProfitCenterCode, AppUtility.Operators.Equals, txtProfitCenterCode.Text, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
             
            objColl.AddCriteria(AppObjects.ProfitCenter.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.SessionProfitCenter] = objColl;
        if (objColl != null)
        {
            grdProfitCenter.DataSource = objColl;
            grdProfitCenter.DataBind();
            if (objColl.Count <= 0)
            {
                AddEmptyRow();
            }
        }
       
       
    }
    public void AddEmptyRow()
    {
        AppProfitCenterColl collOfContacts = new AppProfitCenterColl(intRequestingUserID);
        AppProfitCenter newContact = new AppProfitCenter(intRequestingUserID);
        newContact.ProfitCenterCode = "Add atleast one profit center";
        collOfContacts.Add(newContact);
        grdProfitCenter.DataSource = collOfContacts;
        grdProfitCenter.DataBind();
        grdProfitCenter.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdProfitCenter.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdProfitCenter.ShowFooter = true;
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}