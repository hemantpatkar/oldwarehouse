﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class T_VendorPaymentSummary : BigSunPage
{
    #region PageLoad
    AppPaymentVoucherColl objPO;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtVoucherDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            BindData();
        }
    }
    #endregion
    #region Functions
    public void CancelPaymentVoucher(AppPaymentVoucher ObjPaymentVoucher)
    {
        AppPaymentVoucher ObjDuplicatePaymentVoucher = ObjPaymentVoucher;
        ObjDuplicatePaymentVoucher.ID = 0;
        if (ObjDuplicatePaymentVoucher.PaymentType_ID == 2)
        {
            ObjDuplicatePaymentVoucher.VoucherType_ID = 27; // Cheque Void
        }
        else
        {
            ObjDuplicatePaymentVoucher.VoucherType_ID = 26; //Vendor Payment Cancel
        }
        ObjDuplicatePaymentVoucher.ModifiedBy = intRequestingUserID;
        ObjDuplicatePaymentVoucher.ModifiedOn = System.DateTime.Now;
        ObjDuplicatePaymentVoucher.Status = new AppConvert((int)RecordStatus.Cancelled);
        if (ObjDuplicatePaymentVoucher.PaymentVoucherDetailColl.Count > 0)
        {
            int TotalCount = ObjDuplicatePaymentVoucher.PaymentVoucherDetailColl.Count;
            for (int i = 0; i < TotalCount; i++)
            {
                ObjDuplicatePaymentVoucher.PaymentVoucherDetailColl[i].ID = 0;
                ObjDuplicatePaymentVoucher.PaymentVoucherDetailColl[i].ModifiedBy = intRequestingUserID;
                ObjDuplicatePaymentVoucher.PaymentVoucherDetailColl[i].ModifiedOn = System.DateTime.Now;
                ObjDuplicatePaymentVoucher.PaymentVoucherDetailColl[i].Status = new AppConvert((int)RecordStatus.Cancelled);
            }
        }
        ObjDuplicatePaymentVoucher.Save();
        #region Add Ledger Entry
        LedgerEntry(LoginCompanyID, 100301, 1003, ObjDuplicatePaymentVoucher.Ledger_ID, ObjDuplicatePaymentVoucher.ID, ObjDuplicatePaymentVoucher.PaymentVoucherDate, 0, ObjDuplicatePaymentVoucher.Amount, ObjDuplicatePaymentVoucher.PaymentVoucherNo, ObjDuplicatePaymentVoucher.ModifiedBy, ObjDuplicatePaymentVoucher.ModifiedOn, 0, ObjDuplicatePaymentVoucher.Status, ObjDuplicatePaymentVoucher.Type,
                              0, 0, ObjDuplicatePaymentVoucher.VoucherType_ID, ObjDuplicatePaymentVoucher.Description, "", ObjDuplicatePaymentVoucher.PaymentType_ID);
        if (ObjDuplicatePaymentVoucher.PaymentVoucherDetailColl.Count > 0)
        {
            foreach (var item in ObjDuplicatePaymentVoucher.PaymentVoucherDetailColl)
            {
                if (item.Amount != 0)
                {
                    LedgerEntry(LoginCompanyID, 100401, 1004, item.Ledger_ID, item.ID, ObjDuplicatePaymentVoucher.PaymentVoucherDate, item.Amount, 0, ObjDuplicatePaymentVoucher.PaymentVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicatePaymentVoucher.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, ObjDuplicatePaymentVoucher.VoucherType_ID, item.PaymentVoucherDesc, "", ObjDuplicatePaymentVoucher.PaymentType_ID);
                }
            }
        }
        #endregion
    }
    protected void BindData()
    {
        AppObjects.AppPaymentVoucherColl objColl = new AppPaymentVoucherColl(intRequestingUserID);
        objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PaymentVoucher.VoucherType_ID, AppUtility.Operators.Equals, "10", 0);
        if (txtPaymentVoucherNo.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PaymentVoucher.PaymentVoucherNo, AppUtility.Operators.Equals, txtPaymentVoucherNo.Text, 0);
        }
        if (txtPaymentVoucherAmount.Text != "")
        {
            switch (ddlOperators.SelectedValue)
            {
                case "1":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PaymentVoucher.Amount, AppUtility.Operators.Equals, txtPaymentVoucherAmount.Text, 0);
                    break;
                case "2":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PaymentVoucher.Amount, AppUtility.Operators.GreaterOrEqualTo, txtPaymentVoucherAmount.Text, 0);
                    break;
                case "3":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PaymentVoucher.Amount, AppUtility.Operators.LessOrEqualTo, txtPaymentVoucherAmount.Text, 0);
                    break;
                case "4":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PaymentVoucher.Amount, AppUtility.Operators.GreaterThan, txtPaymentVoucherAmount.Text, 0);
                    break;
                case "5":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PaymentVoucher.Amount, AppUtility.Operators.LessThan, txtPaymentVoucherAmount.Text, 0);
                    break;
            }
        }
        if (hdfVendorID.Value != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PaymentVoucher.Vendor_Company_ID, AppUtility.Operators.Equals, hdfVendorID.Value, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PaymentVoucher.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue, 0);
        }
        objColl.Search();
        Session[TableConstants.PaymentVoucherSummary] = objColl;
        grdPaymentVoucherSummary.DataSource = objColl;
        grdPaymentVoucherSummary.DataBind();
        if (grdPaymentVoucherSummary.Rows.Count <= 0)
        {
            pnlDisplaySummary.Visible = false;
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Records Found','2'" + ");", true);
        }
        else
        {
            pnlDisplaySummary.Visible = true;
        }
    }
    #endregion
    #region GridView Events
    protected void grdPaymentVoucherSummary_PreRender(object sender, EventArgs e)
    {
        if (grdPaymentVoucherSummary.Rows.Count > 0)
        {
            grdPaymentVoucherSummary.UseAccessibleHeader = true;
            grdPaymentVoucherSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region Button Click Events
    protected void btnNewPaymentVoucher_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.ObjCompanySession);
        Response.Redirect("T_VendorPaymentDetails.aspx");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
        if (grdPaymentVoucherSummary.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Records Found','2'" + ");", true);
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        int count = 0;
        AppPaymentVoucherColl PaymentVoucherColl = (AppPaymentVoucherColl)Session[TableConstants.PaymentVoucherSummary];
        if (CheckItemSelectedinGrid(grdPaymentVoucherSummary, "ChkSelect") == false)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please select at least one GRN','2'" + ");", true);
            return;
        }
        for (int i = 0; i < grdPaymentVoucherSummary.Rows.Count; i++)
        {
            CheckBox ChkSelect = (CheckBox)grdPaymentVoucherSummary.Rows[i].FindControl("ChkSelect");
            Label lblPaymentVoucherID = (Label)grdPaymentVoucherSummary.Rows[i].FindControl("lblPaymentVoucherID");
            Int32 PaymentVoucherID = Convert.ToInt32(lblPaymentVoucherID.Text);
            if (ChkSelect.Checked == true && ChkSelect.Enabled == true)
            {
                count = 1;
                PaymentVoucherColl[i].Status = new AppUtility.AppConvert((int)RecordStatus.Deleted);
                PaymentVoucherColl[i].Save();
            }
        }
        if (count == 1)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('GRN Deleted Sucessfully','1'" + ");", true);
            BindData();
        }
    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        int count = 0;
        AppPaymentVoucherColl PaymentVoucherColl = (AppPaymentVoucherColl)Session[TableConstants.PaymentVoucherSummary];
        if (CheckItemSelectedinGrid(grdPaymentVoucherSummary, "ChkSelect") == false)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please select at least one GRN','1'" + ");", true);
            return;
        }
        for (int i = 0; i < grdPaymentVoucherSummary.Rows.Count; i++)
        {
            CheckBox ChkSelect = (CheckBox)grdPaymentVoucherSummary.Rows[i].FindControl("ChkSelect");
            Label lblPaymentVoucherID = (Label)grdPaymentVoucherSummary.Rows[i].FindControl("lblPaymentVoucherID");
            Int32 PaymentVoucherID = Convert.ToInt32(lblPaymentVoucherID.Text);
            if (ChkSelect.Checked == true && ChkSelect.Enabled == true)
            {
                count = 1;
                PaymentVoucherColl[i].Status = new AppUtility.AppConvert((int)RecordStatus.Approve);
                PaymentVoucherColl[i].Save();
            }
        }
        if (count == 1)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Vendor Payment Approved Sucessfully','1'" + ");", true);
            BindData();
        }
    }
    protected void btnCanceled_Click1(object sender, EventArgs e)
    {
        AppObjects.AppPaymentVoucherColl objColl = new AppPaymentVoucherColl(intRequestingUserID);
        if (txtCancelPVNo.Text != "" && txtVoucherDate.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PaymentVoucher.PaymentVoucherNo, AppUtility.Operators.Equals, txtCancelPVNo.Text, 0);
            objColl.Search(RecordStatus.ALL);
            if (objColl.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Record not found','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                return;
            }
            else
            {
                AppPaymentVoucher objSinglePV = objColl[0];
                if (objSinglePV.Status == new AppConvert((int)RecordStatus.Cancelled))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelPVNo.Text + " Transaction alerady cancelled','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    return;
                }
                else if (objSinglePV.Status == new AppConvert((int)RecordStatus.Deleted))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelPVNo.Text + " Transaction is deleted','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    return;
                }
                else if (objSinglePV.Status == new AppConvert((int)RecordStatus.Created))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelPVNo.Text + " Transaction is not approved','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    return;
                }
                else if (objColl[0].PaymentVoucherDate <= new AppUtility.AppConvert(Convert.ToDateTime(txtVoucherDate.Text)))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelPVNo.Text + " Cancel voucher date should be greater than or equal to voucher date.','1'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelPVNo.Focus();
                    return;
                }
                objSinglePV.Status = new AppConvert((int)RecordStatus.Cancelled);
                objSinglePV.Save();
                #region Update Ledger Entries
                UpdateLedgerEntryStatus(100301, 1003, objSinglePV.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                var PVItem = objSinglePV.PaymentVoucherDetailColl.ToList();
                foreach (var item in PVItem)
                {
                    UpdateLedgerEntryStatus(0, 1004, item.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                }
                #endregion
                objSinglePV.CancelPaymentAllocationDetails();
                CancelPaymentVoucher(objSinglePV);
            }
            BindData();
            txtCancelPVNo.Text = "";
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Payment voucher cancelled successfully','1'" + ");", true);
        }
        else
        {
            if (string.IsNullOrEmpty(txtCancelPVNo.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Voucher No.','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                txtCancelPVNo.Focus();
            }
            if (string.IsNullOrEmpty(txtVoucherDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Voucher Date.','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                txtVoucherDate.Focus();
            }
        }
    }
    #endregion
}