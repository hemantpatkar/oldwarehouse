﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="M_TDS" Codebehind="M_TDS.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function ShowPopupn() {
            $('#ModelTDSRate').modal('show');
            $("#<%=txtEffectiveDate.ClientID%>").focus();
        }
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class=" col-lg-12 ">
            <div id="divButtons" class="pull-right">
                <br />
                <div class="btn-group">
                    <div class=" col-lg-3">
                        <asp:TextBox ID="txtTDSName" runat="server" placeholder="TDS Name" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                    </div>
                    <div class=" col-lg-3">
                        <asp:TextBox ID="txtTDSLedger" runat="server" placeholder="TDS Ledger" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                    </div>
                    <div class=" col-lg-3">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control  input-sm">
                            <asp:ListItem Text="ALL" Value="-32768"></asp:ListItem>
                            <asp:ListItem Text="Approved" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Created" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Deleted" Value="-10"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class=" col-lg-3">
                        <asp:Button runat="server" Text="Search" CssClass="btn btn-sm  btn-primary" ID="btnSearch" OnClick="btnSearch_Click"></asp:Button>
                    </div>

                </div>
            </div>
            <h3>TDS Details</h3>
            <hr />

            <asp:GridView ID="grdTDS" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Horizontal"
                CssClass="table table-hover table-striped text-nowrap nowrap"
                OnPreRender="grdTDS_PreRender"
                OnRowCommand="grdTDS_RowCommand" OnRowEditing="grdTDS_RowEditing" OnRowDeleting="grdTDS_RowDeleting"
                OnRowUpdating="grdTDS_RowUpdating">
                <Columns>
                    <asp:TemplateField HeaderText="">
                        <HeaderTemplate>
                            <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="ADDNEW"
                                Text="Add New" ToolTip="Add New" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtnEdit" CausesValidation="false" runat="server" CssClass=" fa fa-edit fa-2x" CommandName="Edit"
                                Text="" />
                            <asp:LinkButton ID="lbtnDelete" Visible="false" runat="server" CssClass="fa fa-trash fa-2x" CommandName="Delete"
                                Text="" ToolTip="Delete" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="EditTDS"
                                Text="" />
                            <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="CANCELEDIT" CssClass="fa fa-ban fa-2x" toot="Cancel"
                                Text="" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x" ToolTip="Save" ValidationGroup="AddTDS"
                                Text="" UseSubmitBehavior="False" />
                            <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban  fa-2x" Text="" />
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblHdrCode" Text="Code" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblCode" runat="server" TabIndex="1" Text='<%#Eval("Code") %>' AutoComplete="Off" CssClass="bold"></asp:Label>

                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:TextBox ID="txtCode" runat="server" Text='<%# Eval("Code") %>' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFV_txtCode" runat="server" ControlToValidate="txtCode"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditTDS"></asp:RequiredFieldValidator>

                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNewCode" runat="server" Text='' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFV_txtNewCode" runat="server" ControlToValidate="txtNewCode"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddTDS"></asp:RequiredFieldValidator>
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblHdrTDSName" Text="TDS Name" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTDSName" runat="server" TabIndex="1" Text='<%#Eval("Name") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:HiddenField runat="server" ID="hdfTDSID" Value='<%# Eval("ID") %>' />
                            <asp:TextBox ID="txtTDSName" runat="server" Text='<%# Eval("Name") %>' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFV_txtTDSName" runat="server" ControlToValidate="txtTDSName"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditTDS"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNewTDSName" runat="server" Text='' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFV_txtNewTDSName" runat="server" ControlToValidate="txtNewTDSName"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddTDS"></asp:RequiredFieldValidator>
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblLedger" Text="Zip Code" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTDS" runat="server" CssClass="bold" MaxLength="50" Text='<%#Eval("Ledger.LedgerName") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLedger" runat="server" Text='<%# Eval("Ledger.LedgerName") %>' MaxLength="500" TabIndex="1" ValidationGroup="EditTDS" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hdfLedgerName" Value='<%# Eval("Ledger.LedgerName").ToString() %>' />
                            <asp:HiddenField runat="server" ID="hdfLedgerID" Value='<%# Eval("Ledger_ID") %>' />
                            <asp:RequiredFieldValidator ID="RFV_txtLedger" runat="server" ControlToValidate="txtLedger" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage="" ValidationGroup="EditTDS" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:AutoCompleteExtender ID="ACEtxtLedger" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLedger">
                            </asp:AutoCompleteExtender>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNewLedger" runat="server" TabIndex="1" onblur="return ClearAutocompleteTextBox(this)" PlaceHolder="Select TDS" AutoPostBack="false" AutoComplete="Off" AutoCompleteType="None" MaxLength="50" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="hdfNewLedgerName" />
                            <asp:HiddenField runat="server" ID="hdfNewLedgerID" Value="0" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtNewLedger" CssClass="text-danger" ToolTip="Required Field"
                                Display="Dynamic" ErrorMessage="" ValidationGroup="AddTDS" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:AutoCompleteExtender ID="ACEtxtNewLedger" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtNewLedger">
                            </asp:AutoCompleteExtender>
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblActive" runat="server" Text='<%#(Eval("StatusName")) %>' CssClass="bold"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox runat="server" ID="chkIsActiveTDS" CssClass="form-control" TabIndex="8" Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox runat="server" ID="chkActiveTDS" CssClass="form-control" TabIndex="8" Checked="true" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="Default" Text="Default" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDefault" runat="server" Text='<%#(Eval("Type").ToString()=="1"?"Y":"N") %>' CssClass="bold"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox runat="server" ID="chkIsDefaultTDS" TabIndex="9" Checked='<%#Eval("Type").ToString()=="1"?true:false %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox runat="server" ID="chkDefaultTDS" TabIndex="9" />
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>

                        <ItemTemplate>
                            <asp:LinkButton ID="lbtnTdsRate" runat="server" CommandName="TDSRate" CommandArgument='<%# Eval("ID") %>' CssClass="fa-1x" ToolTip="Save" ValidationGroup="AddTDS"
                                Text="TDS Rate" UseSubmitBehavior="False" />

                        </ItemTemplate>

                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="ModelTDSRate" role="dialog">
        <div class="modal-dialog">

            <!-- Modal TDS RATE-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">TDS Rate</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class=" col-lg-15 ">

                            <div class=" col-lg-6">
                                <asp:HiddenField runat="server" ID="hdfTDSID" />
                                <asp:Label ID="lblTds" Text="TDS" runat="server" CssClass="bold"></asp:Label>
                                <asp:TextBox ID="txtTDS" runat="server" placeholder="TDS" Enabled="false" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                            </div>
                            <div class=" col-lg-3">
                                <asp:Label ID="lblEffectiveDate" Text="Effective Date" runat="server" CssClass="bold"></asp:Label>
                                <asp:TextBox ID="txtEffectiveDate" runat="server" placeholder="Effective Date" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                <asp:MaskedEditExtender ID="MEE_txtEffectiveDate" runat="server"
                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtEffectiveDate">
                                </asp:MaskedEditExtender>
                                <asp:CalendarExtender ID="CE_txtEffectiveDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtEffectiveDate"
                                    TargetControlID="txtEffectiveDate">
                                </asp:CalendarExtender>
                            </div>
                            <div class=" col-lg-3">
                                <asp:Label ID="lblTdsRate" Text="Tds Rate" runat="server" CssClass="bold"></asp:Label>
                                <asp:TextBox ID="txtTdsRate" runat="server" placeholder="TDS Rate" TabIndex="1" onkeypress="return validateFloatKeyPress(this, event)" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                            </div>

                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class=" col-lg-12 ">
                            <asp:GridView ID="grdTdsRate" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Horizontal"
                                CssClass="table table-hover table-striped text-nowrap nowrap" OnPreRender="grdTdsRate_PreRender">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdrTDSRate" Text="TDS Rate" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTDSRate" runat="server" TabIndex="1" Text='<%#Eval("Rate") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdrTDSEffectiveDate" Text="Effective Date" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblTDSEffectiveDate" runat="server" TabIndex="1" Text='<%#Convert.ToDateTime(Eval("EffectiveFrom")).ToString("dd/MM/yyyy") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm  btn-primary" data-dismiss="modal">Close</button>
                    <asp:Button runat="server" Text="Save" CssClass="btn btn-sm  btn-primary" ID="btnSaveTdsRate" OnClick="btnSaveTdsRate_Click"></asp:Button>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
