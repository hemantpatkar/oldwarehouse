﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using System.IO;
using System.Drawing;

public partial class StockOutSearch : BigSunPage
{
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion

    #region Button Event
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockOutDetail.aspx");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Search();
    }

    protected void Search()
    {
        AppOutwardColl objAppGateEntryColl = new AppOutwardColl(intRequestingUserID);

        #region DateValidation
        DateTime fromdate, to;
        if (!string.IsNullOrEmpty(txtfromdate.Text.Trim()) && !string.IsNullOrEmpty(txttodate.Text.Trim()))
        {
            fromdate = new AppConvert(txtfromdate.Text);
            to = new AppConvert(txttodate.Text);
            if (fromdate > to)
            {
                grdOrderSearch.DataSource = null;
                grdOrderSearch.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please Select Validate Date range..!" + "','2'" + ");", true);
                return;
            }
        }

        #endregion

        #region Code By Hemant On 24-12-2012

        if (ddlStatus.SelectedValue == "0")
        {
            if (txtfromdate.Text.Trim() != "" && txttodate.Text.Trim() != "" && txtfromdate.Text.Trim() != "__/__/____" && txttodate.Text.Trim() != "__/__/____")
            {
                fromdate = Convert.ToDateTime(txtfromdate.Text);
                to = Convert.ToDateTime(txttodate.Text + " 23:59:59");
                objAppGateEntryColl.AddCriteria(Outward.OutwardDate, Operators.GreaterOrEqualTo, fromdate);
                objAppGateEntryColl.AddCriteria(Outward.OutwardDate, Operators.LessOrEqualTo, to);
            }
            else if (txtfromdate.Text.Trim() != "" && txtfromdate.Text.Trim() != "__/__/____" && (txttodate.Text.Trim() == "" || txttodate.Text.Trim() == "__/__/____"))
            {
                fromdate = Convert.ToDateTime(txtfromdate.Text);

                objAppGateEntryColl.AddCriteria(Outward.OutwardDate, Operators.GreaterOrEqualTo, fromdate);
            }
            else if (txttodate.Text.Trim() != "" && txttodate.Text.Trim() != "__/__/____" && (txtfromdate.Text.Trim() == "" || txtfromdate.Text.Trim() == "__/__/____"))
            {
                to = Convert.ToDateTime(txttodate.Text + " 23:59:59");

                objAppGateEntryColl.AddCriteria(Outward.OutwardDate, Operators.LessOrEqualTo, to);
            }
        }
        else if (ddlStatus.SelectedValue == "1")
        {

        }
        else
        {

        }

        if (txtOrderNo.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(Outward.OutwardNo, Operators.Like, txtOrderNo.Text);
        }

        if (txtCustomer.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(Outward.Customer_Company_ID, Operators.Like, txtCustomer.Text);
        }


        if (txtDeliveryTo.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(Outward.DeliveredTo, Operators.Like, txtDeliveryTo.Text);
        }

        //if (txtChallanNo.Text.Trim() != "")
        //{
        //    objAppGateEntryColl.AddCriteria(PreInward.DriverName, Operators.Like, txtChallanNo.Text);
        //}        
        if (ddlStatus.SelectedValue != "-1")
        {
            int status = Convert.ToInt32(ddlStatus.SelectedValue);
            objAppGateEntryColl.AddCriteria(Outward.Status, Operators.Equals, status); //
        }

        objAppGateEntryColl.Search();
        //objAppGateEntryColl.Sort(AppPreInward.ComparisionDesc);
        grdOrderSearch.DataSource = objAppGateEntryColl;
        grdOrderSearch.DataBind();


        #endregion
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (grdOrderSearch.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Doesnot contain data" + "','2'" + ");", true);
            return;
        }

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grdOrderSearch.AllowPaging = false;
            grdOrderSearch.HeaderRow.BackColor = Color.White;
            foreach (TableCell cell in grdOrderSearch.HeaderRow.Cells)
            {
                cell.BackColor = grdOrderSearch.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in grdOrderSearch.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = grdOrderSearch.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = grdOrderSearch.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            grdOrderSearch.RenderControl(hw);
            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    #endregion

    #region Grid
    protected void grdOrderSearch_PreRender(object sender, EventArgs e)
    {

    }
    #endregion

    protected void grdOrderSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdOrderSearch.PageIndex = e.NewPageIndex;
        Search();
    }
}