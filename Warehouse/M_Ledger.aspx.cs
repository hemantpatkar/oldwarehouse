﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using System.Web.Services;
using System.Text;
public partial class M_Ledger : BigSunPage
{
    AppLedgerColl objLedgerTypeColl = null;
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        objLedgerTypeColl = new AppLedgerColl(intRequestingUserID);
        if (!IsPostBack)
        {
            CommonFunctions.BindDropDown(intRequestingUserID, Components.AccountType, ddlAccountType, "AccountTypeName", "ID");
            CommonFunctions.BindDropDown(intRequestingUserID, Components.LedgerType, ddlLedgerType, "LedgerTypeName", "ID");
            PopulateData(Convert.ToString(Request.QueryString["LedgerId"]));
        }
    }
    #endregion
    #region Button Click Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppLedgerColl objLedgerColl = (AppLedgerColl)HttpContext.Current.Session[TableConstants.LedgerTreeSessionObj];
        AppLedger objLedger = new AppLedger(intRequestingUserID);
        string strCommandID = hdfID.Value;
        int intCommandID = new AppConvert(strCommandID);
        if (String.IsNullOrEmpty(strCommandID) == false)
        {
            objLedger.ID = intCommandID;
        }
        objLedger.LedgerName = txtLedgerName.Text;
        objLedger.AccountNo = txtAccountNo.Text;
        objLedger.LedgerCode = txtLedgerCode.Text;
        objLedger.AccountType_ID = new AppConvert(ddlAccountType.SelectedValue);
        objLedger.LedgerType_ID = new AppConvert(ddlLedgerType.SelectedValue);
        objLedger.Parent_ID = new AppConvert(hdfParentLedgerGrpID.Value);
        objLedger.Status = (chkActive.Checked == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        objLedger.LedgerFlag = (rdbTypeOfLedger.SelectedValue == "1" ? new AppConvert((int)LedgerFlag.ControlAccount) : rdbTypeOfLedger.SelectedValue == "2" ? new AppConvert((int)LedgerFlag.LedgerGroup): new AppConvert((int)LedgerFlag.GeneralLedger));

        if (objLedgerColl != null)
        {
            objLedger.SortOrder = new AppConvert(objLedgerColl.FindLastIndex(o => o.Parent_ID == objLedger.Parent_ID));
        }
        objLedger.ModifiedBy = intRequestingUserID;
        objLedger.ModifiedOn = System.DateTime.Now;
        objLedger.Save();
        ClearFields();
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Ledger Saved','1'" + ");", true);
        Response.Redirect("M_LedgerSummary.aspx");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearFields();
        Session.Remove(TableConstants.LedgerTreeSessionObj);
        Session.Remove(TableConstants.LedgerTypeSessionObj);
        Response.Redirect("M_LedgerSummary.aspx");
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        AppLedger objLedger = (AppLedger)Session[TableConstants.LedgerTypeSessionObj];
        objLedger.Status = new AppConvert((int)RecordStatus.Approve);
        objLedger.Save();
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Ledger Approved','1'" + ");", true);
    }
    #endregion
    #region Web Method
    [WebMethod(EnableSession = true)]
    public static string getData(string FDID)
    {
        int UserId = new AppUtility.AppConvert(HttpContext.Current.Session[TableConstants.UserIDSession]);
        AppLedgerColl objLedgerCollection = new AppLedgerColl(UserId);
        StringBuilder builder = new StringBuilder();
        try
        {
            objLedgerCollection.Search();
            builder.Append("[");
            foreach (AppLedger item in objLedgerCollection)
            {
                builder.Append("{ id:" + item.ID + ", FID:" + item.ID + ", IsFile:" + 0 + ", Lock:" + 0 + ", New:1" + ", Update:0" + ", pId:" + (int)(item.Parent_ID) + ", name:" + '"' + item.LedgerName + '"' + ", t:" + '"' + item.LedgerName + '"' + ", open: false, font:" + ("{ 'color': 'Green' }") + "},");
            }
            builder.Append("]");
            HttpContext.Current.Session.Add(TableConstants.LedgerTreeSessionObj, objLedgerCollection);
            return builder.ToString();
        }
        catch
        {
            return builder.ToString();
        }
    }
    public static string BindrightClickMenu()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<li id=\"m_cut\" onclick=\"return cut();\">Cut</li>");
        sb.Append("<li id=\"m_paste\" onclick=\"return paste();\">Paste</li>");
        sb.Append("<div style=\"border-bottom: 1px solid #808080\"></div>");
        return sb.ToString();
    }
    [WebMethod(EnableSession = true)]
    public static string BingMenu(string FDID, string Type, string Lock, string File)
    {
        return BindrightClickMenu();
    }
    [WebMethod(EnableSession = true)]
    public static string Copy(string FDID, string PID)
    {
        return "0";
    }
    [WebMethod(EnableSession = true)]
    public static string Move(string FolderID, string DestinationID)
    {
        int UserId = new AppUtility.AppConvert(HttpContext.Current.Session["UserIdConst"]);
        int FolderId = new AppConvert(FolderID);
        int DestinationId = new AppConvert(DestinationID);
        AppLedgerColl objLedgerTypeColl = new AppLedgerColl(UserId);
        objLedgerTypeColl.MoveLedgerType(FolderId, DestinationId);
        return "";
    }
    #endregion
    #region Misc Methods
    public void PopulateData(string ID)
    {
        if (!string.IsNullOrEmpty(ID))
        {
            int LedgerId = new AppConvert(ID);
            AppLedger objLedger = new AppLedger(intRequestingUserID, LedgerId);
            txtLedgerCode.Text = objLedger.LedgerCode;
            txtLedgerName.Text = objLedger.LedgerName;
            txtAccountNo.Text = objLedger.AccountNo;
            ddlAccountType.SelectedValue = new AppConvert(objLedger.AccountType_ID);
            ddlLedgerType.SelectedValue = new AppConvert(objLedger.LedgerType_ID);
            rdbTypeOfLedger.SelectedValue = new AppConvert(objLedger.LedgerFlag);
            txtParentLedgerGrp.Text = objLedger.Parent.LedgerCode + " - " + objLedger.Parent.LedgerName;
            hdfParentLedgerGrpName.Value = objLedger.Parent.LedgerCode + " - " + objLedger.Parent.LedgerName;
            hdfParentLedgerGrpID.Value = new AppConvert(objLedger.Parent_ID);
            chkActive.Checked = new AppConvert(objLedger.Status);
            hdfID.Value = new AppConvert(objLedger.ID);
            btnApprove.Visible = true;
            if (rdbTypeOfLedger.SelectedValue == "1")
            {
                divParentLedger.Attributes.Add("style", "display:none");
            }
            else
            {
                divParentLedger.Attributes.Add("style", "display:block");
            }

            Session.Add(TableConstants.LedgerTypeSessionObj, objLedger);
        }
        else
        {
            btnApprove.Visible = false;
        }
    }
    public void ClearFields()
    {
        txtLedgerCode.Text = "";
        txtLedgerName.Text = "";
        hdfID.Value = "";
        hdfParentLedgerGrpID.Value = "";
        chkActive.Checked = true;
    }
    #endregion
}