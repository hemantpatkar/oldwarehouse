﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
/// <summary>
/// Summary description for TableConstants
/// </summary>
public class TableConstants
{
    public TableConstants()
    {
        //
        // TODO: Add constructor logic here
        //      
    }
    public const int StatusActive = 1;

    #region SessionConstants
    public const string LoginCompanyID = "LoginCompanyID";
    public const string UserIDSession = "UserIDSession";
    public const string VendorSessionObj = "CompanySessionObj";
    public const string CustomerSessionObj = "CustomerSessionObj";
    public const string VendorCollSession = "VendorCollSession";
    public const string CustomerCollSession = "CustomerCollSession";
    public const string DailyExchangeRateColl = "DailyExchangeRateColl";
    public const string MonthlyExchangeRateColl = "MonthlyExchangeRateColl";
    public const string DepartmentData = "DepartmentData";
    public const string VendorIdConst = "VendorIdConst";
    public const string PriceGroupConst = "PriceGroupConst";
    public const string PriceGroupCollection = "PriceGroupCollection";
    public const string ItemsCollection = "ItemsCollection";
    public const string DesignationData = "DesignationData";
    public const string TaxSessionObj = "TaxSessionObj";
    public const string TaxSessionCollObj = "TaxSessionCollObj";
    public const string LedgerTypeCollObj = "LedgerTypeCollObj";
    public const string LedgerCollObj = "LedgerCollObj";
    public const string LedgerTypeSessionObj = "LedgerTypeSessionObj";
    public const string LedgerTreeSessionObj = "LedgerTreeSessionObj";    
    public const string ObjCompanySession = "ObjCompanySession";
    public const string ObjCompanySessionColl = "ObjCompanySessionColl";
    public const string PurchaseOrder = "PurchaseOrder";
    public const string PurchaseOrderSummary = "PurchaseOrderSummary";
    public const string PurchaseGRN = "PurchaseGRN";
    public const string PurchaseGRNSummary = "PurchaseGRNSummary";    
    public const string SaleOrderSummary = "SaleOrderSummary";
    public const string SaleOrderSession = "SaleOrderSession";
    public const string BaseCurrencyID = "BaseCurrencyID";
    
    public const string CompanyIdConst = "CompanyIdConst";
    public const string DepartmentIDConst = "DepartmentIDConst";
    public const string DesignationIDConst = "DesignationIDConst";
    public const string LoginUser = "LoginUser";
    public const string SaleBillSession = "SaleBillSession";
    public const string JournalVoucher = "JournalVoucher";
    public const string JournalVoucherSummary = "JournalVoucherSummary";
    public const string SaleBillSummary = "SaleBillSummary";
    public const string VendorPaymentDetails = "VendorPaymentDetails";
    public const string PaymentVoucherSummary = "PaymentVoucherSummary";
    public const string CreditNote = "CreditNote";
    public const string CreditNoteSummary = "CreditNoteSummary";
    public const string DebitNote = "DebitNote";
    public const string DebitNoteSummary = "DebitNoteSummary";
    public const string CustomerReceiptDetails = "CustomerReceiptDetails";
    public const string ReceiptVoucherSummary = "ReceiptVoucherSummary";
    public const string Contra = "Contra";
    public const string ContraSummary = "ContraSummary";
    public const string SessionProfitCenter = "SessionProfitCenter";
    public const string SessionCostCenter = "SessionCostCenter";
    public const string SessionBankType = "SessionBankType";
    public const string SessionUOM = "SessionUOM";
    public const string SessionTDS = "SessionTDS";
    public const string SessionTDSRate = "SessionTDSRate";
    public const string SessionPaymentVoucherDetail = "SessionPaymentVoucherDetail";
    public const string SessionReceiptVoucherDetail = "SessionReceiptVoucherDetail";
    public const string SessionItemAttribute = "SessionItemAttribute";
        

    #region Warehouse
    public const string InwardSession = "InwardSession";
    public const string InwardDetailIDSession = "InwardDetailIDSession";
    public const string MaterialOrderSession = "MaterialOrderSession";
    public const string OutWardSession = "OutWardSession";
    public const string PreInwardSession = "PreInwardSession";
    public const string TransferStockLocationSession = "TransferStockLocationSession";
    public const string TransferStockCustomerSession = "TransferStockCustomerSession";
    public const string TranshipmentSession = "TranshipmentSession";
    public const string SampleSession = "SampleSession";
    public const string RepackingSession = "RepackingSession";
    public const string GradingSession = "GradingSession";
    public const string SessionBrand = "SessionBrand";
    public const string SessionOrigin = "SessionOrigin";
    public const string SessionVariety = "SessionVariety";
    public const string SessionLocation = "SessionLocation";
    public const string SessionScheme = "SessionScheme";    
    #endregion

    public const string ActiveModuleName = "ActiveModuleName";
    public const string ActiveModuleID = "ActiveModuleID";


    #endregion



    public const int VendorConst = 2;
    public const int CustomerConst = 3;

    #region UserId
    /// <summary>
    /// Pradip Bobhate 18/07/2016 6.40 PM.
    /// This property is used to get User ID from session.
    /// </summary>
    public static int UserId
    {
        get
        {
            int returnValue = 1;
            object getObject = HttpContext.Current.Session[UserIDSession];
            if (getObject != null)
            {
                returnValue = Convert.ToInt32(getObject);
            }
            return returnValue;
        }
    }
    #endregion
}