﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class BigSunPage : System.Web.UI.Page
{
    public int intRequestingUserID = 0;
    public int LoginCompanyID = 0;
    public int intDepartmentID = 0;
    public int intDesignationID = 0;
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (Session[TableConstants.UserIDSession] == null)
        {
            Response.Redirect("Login.aspx");
            return;
        }
        intRequestingUserID = (int)Session[TableConstants.UserIDSession];
        LoginCompanyID = (int)Session[TableConstants.CompanyIdConst];
        intDepartmentID = (int)Session[TableConstants.DepartmentIDConst];
        intDesignationID = (int)Session[TableConstants.DesignationIDConst];

    }
    #region BindMaster
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        if (Convert.ToString(Request.QueryString["Master"]) == "Simple")
            MasterPageFile = "~/Simple.Master";
        else
            MasterPageFile = "~/Base.Master";
    }
    #endregion



    public string DefaultCompanyAddress
    {
        get
        {
            AppObjects.AppCompany Company = new AppObjects.AppCompany(intRequestingUserID, LoginCompanyID);
            AppCompanyAddress CAddress = Company.CompanyAddressColl.FirstOrDefault(x => x.Company_ID == LoginCompanyID && x.Status == new AppUtility.AppConvert((int)AppObjects.RecordStatus.Active) && x.Type == 1);
            if (CAddress != null)
            {

                return CAddress.Address1 + "" + CAddress.Address2;

            }
            return "";
        }
    }
    public int DefaultCompanyAddressID
    {
        get
        {
            AppObjects.AppCompany Company = new AppObjects.AppCompany(intRequestingUserID, LoginCompanyID);
            AppCompanyAddress CAddress = Company.CompanyAddressColl.FirstOrDefault(x => x.Company_ID == LoginCompanyID && x.Status == new AppUtility.AppConvert((int)AppObjects.RecordStatus.Active) && x.Type == 1);
            if (CAddress != null)
            {

                return CAddress.ID;

            }
            return 0;
        }
    }
    public string DefaultDeliveryAddress
    {
        get
        {
            AppObjects.AppCompany Company = new AppObjects.AppCompany(intRequestingUserID, LoginCompanyID);
            AppCompanyAddress CAddresss = Company.CompanyAddressColl.FirstOrDefault(x => x.Company_ID == LoginCompanyID && x.Status == (int)AppObjects.RecordStatus.Active && x.Type == 1);
            if (CAddresss != null)
            {
                return CAddresss.Address1 + "" + CAddresss.Address2;
            }
            return "";
        }
    }
    public int DefaultDeliveryAddressID
    {
        get
        {
            AppObjects.AppCompany Company = new AppObjects.AppCompany(intRequestingUserID, LoginCompanyID);
            AppCompanyAddress CAddresss = Company.CompanyAddressColl.FirstOrDefault(x => x.Company_ID == LoginCompanyID && x.Status == (int)AppObjects.RecordStatus.Active && x.Type == 1);
            if (CAddresss != null)
            {
                return CAddresss.ID;
            }
            return 0;
        }
    }
    public string DefaultBaseCurrency
    {
        get
        {
            AppObjects.AppCompany Company = new AppObjects.AppCompany(intRequestingUserID, LoginCompanyID);
            if (Company != null)
            {
                if (Company.CurrencyType_ID != 0)
                    return Company.CurrencyType.Name;
            }
            return "";
        }
    }
    public int DefaultBaseCurrencyID
    {
        get
        {
            AppObjects.AppCompany Company = new AppObjects.AppCompany(intRequestingUserID, LoginCompanyID);
            if (Company != null)
            {

                return Company.CurrencyType_ID;
            }
            return 0;
        }
    }
    public void UpdateLedgerEntryStatus(int LedgerEntryFields_ID, int TableID, int TransactionID, int TransStatus, int LedgerID = 0)
    {
        AppObjects.AppLedgerEntryColl objLEColl = new AppObjects.AppLedgerEntryColl(intRequestingUserID);
        objLEColl.Clear();
        if (LedgerEntryFields_ID > 0)
        {
            objLEColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.LedgerEntry.LedgerEntryFields_ID, AppUtility.Operators.Equals, LedgerEntryFields_ID, 1);
        }
        objLEColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.LedgerEntry.TableID, AppUtility.Operators.Equals, TableID, 1);
        objLEColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.LedgerEntry.TransactionID, AppUtility.Operators.Equals, TransactionID, 1);
        if (LedgerID > 0)
        {
            objLEColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.LedgerEntry.Ledger_ID, AppUtility.Operators.Equals, LedgerID, 1);
        }
        objLEColl.Search();
        if (objLEColl.Count > 0)
        {
            foreach (var item in objLEColl)
            {
                item.TransStatus = TransStatus;
                item.Save();
            }
        }
    }
    public void LedgerEntry(int Company_ID, int LedgerEntryFields_ID, int TableID, int Ledger_ID, int TransactionID, DateTime TransactionDate,
                            decimal CreditAmount, decimal DebitAmount, string TransactionNo, int ModifiedBy, DateTime ModifiedOn, int Status, int TransStatus, int Type
                            , int ProfitCenter_ID = 0, int CostCenter_ID = 0, int VoucherType_ID = 0, string Description = "", string PaymentTypeNo = "", int PaymentType_ID = 0, DateTime? PaymentTypeDate = null, string InvoiceNo = "", int IsOpeningBal = 0)
    {
        AppObjects.AppLedgerEntryColl objLEColl = new AppObjects.AppLedgerEntryColl(intRequestingUserID);
        objLEColl.Clear();
        objLEColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.LedgerEntry.LedgerEntryFields_ID, AppUtility.Operators.Equals, LedgerEntryFields_ID, 1);
        objLEColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.LedgerEntry.TableID, AppUtility.Operators.Equals, TableID, 1);
        objLEColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.LedgerEntry.TransactionID, AppUtility.Operators.Equals, TransactionID, 1);
        objLEColl.Search();
        AppLedgerEntry objDLE = objLEColl.FirstOrDefault();
        AppObjects.AppLedgerEntry objLE = new AppObjects.AppLedgerEntry(intRequestingUserID);
        AppObjects.AppLedgerEntryDetail objLED = new AppObjects.AppLedgerEntryDetail(intRequestingUserID);
        if (objDLE != null)
        {
            objLE = new AppObjects.AppLedgerEntry(intRequestingUserID, objDLE.ID);
        }
        if (objLE.LedgerEntryDetailColl.Count > 0)
        {
            objLED = objLE.LedgerEntryDetailColl[0];
        }
        else
        {
            objLE.AddNewLedgerEntryDetail(objLED);
        }
        objLED.TransactionNo = TransactionNo;
        objLED.Description = Description;
        objLED.ProfitCenter_ID = ProfitCenter_ID;
        objLED.CostCenter_ID = CostCenter_ID;
        objLED.PaymentType_ID = PaymentType_ID;
        objLED.PaymentTypeNo = PaymentTypeNo;
        if (PaymentTypeDate != null)
        {
            objLED.PaymentTypeDate = new AppUtility.AppConvert(PaymentTypeDate);
        }
        objLED.InvoiceNo = InvoiceNo;
        objLED.ModifiedBy = ModifiedBy;
        objLED.ModifiedOn = ModifiedOn;
        objLED.Status = new AppUtility.AppConvert(Status);
        objLED.Type = new AppUtility.AppConvert(Type);
        objLE.Company_ID = Company_ID;
        objLE.LedgerEntryFields_ID = LedgerEntryFields_ID;
        objLE.TableID = TableID;
        objLE.Ledger_ID = Ledger_ID;
        objLE.TransactionID = TransactionID;
        objLE.TransactionDate = TransactionDate;
        objLE.CreditAmount = CreditAmount;
        objLE.VoucherType_ID = VoucherType_ID;
        objLE.TransStatus = TransStatus;
        objLE.DebitAmount = DebitAmount;
        objLE.IsOpeningBal = new AppUtility.AppConvert(IsOpeningBal);
        objLE.ModifiedBy = ModifiedBy;
        objLE.ModifiedOn = ModifiedOn;
        objLE.Status = new AppUtility.AppConvert(Status);
        objLE.Type = new AppUtility.AppConvert(Type);
        objLE.Save();
    }
    public static void popup_Show(string Popupname, string FocusControlName, Control PageName)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(@"<script type='text/javascript'>");
        sb.Append("$('#" + Popupname + "').modal('show');");
        sb.Append(" $('#" + Popupname + "').on('shown.bs.modal', function () {$('#" + FocusControlName + "').focus();});");
        sb.Append(@"</script>");
        ScriptManager.RegisterClientScriptBlock(PageName, PageName.GetType(), "DetailModalScriptShow", sb.ToString(), false);
    }
    public static void popup_Hide(string Popupname, string FocusControlName, Control PageName)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(@"<script type='text/javascript'>");
        sb.Append("$('#" + Popupname + "').modal('hide');");
        sb.Append(" $('#" + Popupname + "').on('shown.bs.modal', function () {$('#" + FocusControlName + "').focus();});");
        sb.Append(@"</script>");
        ScriptManager.RegisterClientScriptBlock(PageName, PageName.GetType(), "DetailModalScriptHide", sb.ToString(), false);
    }
    public static void Div_Show(string Divname, Control PageName)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(@"<script type='text/javascript'>");
        sb.Append("$('#" + Divname + "').show();");
        sb.Append(@"</script>");
        ScriptManager.RegisterClientScriptBlock(PageName, PageName.GetType(), "DetailModalScriptShow", sb.ToString(), false);
    }
    public static void Div_Hide(string Divname, Control PageName)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(@"<script type='text/javascript'>");
        sb.Append("$('#" + Divname + "').hide();");
        sb.Append(@"</script>");
        ScriptManager.RegisterClientScriptBlock(PageName, PageName.GetType(), "DetailModalScriptHide", sb.ToString(), false);
    }
    public AppCompanyBank DefaultCompanyBank
    {
        get
        {
            AppObjects.AppCompany Company = new AppObjects.AppCompany(intRequestingUserID, LoginCompanyID);
            AppCompanyBank CompanyBank = Company.CompanyBankColl.FirstOrDefault(x => x.Company_ID == LoginCompanyID && x.Status == new AppUtility.AppConvert((int)AppObjects.RecordStatus.Active));
            // && x.Type == 1
            if (CompanyBank == null)
            {
                CompanyBank = new AppCompanyBank(intRequestingUserID);
            }
            return CompanyBank;
        }
    }
    public int DiscountLedgerID
    {
        get
        {
            int DLedgerID = 0;
            AppObjects.AppLedgerColl objNew = new AppLedgerColl(this.intRequestingUserID);
            //objNew.AddCriteria(AppObjects.Ledger.Type, AppUtility.Operators.Equals, (Int16)DefaultNormalType.Default);
            objNew.AddCriteria(AppObjects.Ledger.LedgerType_ID, AppUtility.Operators.Equals, 8);
            objNew.Search(RecordStatus.ALL, 0);
            if (objNew != null)
            {
                if (objNew.Count > 0)
                {
                    return DLedgerID = (objNew.FirstOrDefault().ID);
                }
            }
            return DLedgerID;
        }
    }
    public int RoundingOFFLedgerID
    {
        get
        {
            int RLedgerID = 0;
            AppObjects.AppLedgerColl objNew = new AppLedgerColl(this.intRequestingUserID);
            //objNew.AddCriteria(AppObjects.Ledger.Type, AppUtility.Operators.Equals, (Int16)DefaultNormalType.Default);
            objNew.AddCriteria(AppObjects.Ledger.LedgerType_ID, AppUtility.Operators.Equals, 9);
            objNew.Search(RecordStatus.ALL, 0);
            if (objNew != null)
            {
                if (objNew.Count > 0)
                {
                    return RLedgerID = (objNew.FirstOrDefault().ID);
                }
            }
            return RLedgerID;
        }
    }
    public static bool CheckItemSelectedinGrid(GridView Grid, string CheckBoxName)
    {
        if (Grid.Rows.Count == 0)
        {
            return false;
        }
        for (int i = 0; i < Grid.Rows.Count; i++)
        {
            CheckBox chkSelect = (CheckBox)Grid.Rows[i].FindControl(CheckBoxName);
            if (chkSelect.Checked == true)
            {
                return true;
            }
        }
        return false;
    }
    private void UpdateDetail(int TransactionID, Int16 TransStatus, int HeaderTableID, int DetailTableID)
    {
        switch (HeaderTableID)
        {
            case 1001:
                #region Update Traasaction Entries
                AppPurchaseGRNColl PurchaseGRNColl = (AppPurchaseGRNColl)Session[TableConstants.PurchaseGRNSummary];
                AppPurchaseGRN objPorchaseGrn = PurchaseGRNColl.FirstOrDefault(x => x.ID == TransactionID);
                objPorchaseGrn.Status = TransStatus;
                objPorchaseGrn.Save();
                #endregion
                #region Update Ledger Entries
                UpdateLedgerEntryStatus(0, HeaderTableID, TransactionID, TransStatus);
                var PurchaseItem = objPorchaseGrn.PurchaseGRNItemColl.ToList();
                foreach (var item in PurchaseItem)
                {
                    UpdateLedgerEntryStatus(0, DetailTableID, item.ID, TransStatus);
                }
                #endregion
                break;
            case 1015:
                #region Update Traasaction Entries
                AppSaleBillColl SaleBillColl = (AppSaleBillColl)Session[TableConstants.SaleBillSummary];
                AppSaleBill objSalesBill = SaleBillColl.FirstOrDefault(x => x.ID == TransactionID);
                objSalesBill.Status = TransStatus;
                objSalesBill.Save();
                #endregion
                #region Update Ledger Entries
                UpdateLedgerEntryStatus(0, HeaderTableID, TransactionID, TransStatus);
                var SalesItem = objSalesBill.SaleBillItemColl.ToList();
                foreach (var item in SalesItem)
                {
                    UpdateLedgerEntryStatus(0, DetailTableID, item.ID, TransStatus);
                }
                #endregion
                break;
            case 1017:
                #region Update Traasaction Entries
                AppSaleOrderColl SaleOrderColl = (AppSaleOrderColl)Session[TableConstants.SaleOrderSummary];
                AppSaleOrder objSaleOrder = SaleOrderColl.FirstOrDefault(x => x.ID == TransactionID);
                objSaleOrder.Status = TransStatus;
                objSaleOrder.Save();
                #endregion
                break;
            case 1019:
                #region Update Traasaction Entries
                AppPurchaseOrderColl PurchaseOrderColl = (AppPurchaseOrderColl)Session[TableConstants.PurchaseOrderSummary];
                AppPurchaseOrder objPurchaseOrder = PurchaseOrderColl.FirstOrDefault(x => x.ID == TransactionID);
                objPurchaseOrder.Status = TransStatus;
                objPurchaseOrder.Save();
                #endregion
                break;
            case 1006:
                #region Update Traasaction Entries
                AppJournalVoucherColl JournalVoucherColl = (AppJournalVoucherColl)Session[TableConstants.JournalVoucherSummary];
                AppJournalVoucher objJournalVoucherColl = JournalVoucherColl.FirstOrDefault(x => x.ID == TransactionID);
                objJournalVoucherColl.Status = TransStatus;
                objJournalVoucherColl.Save();
                #endregion
                #region Update Ledger Entries
                UpdateLedgerEntryStatus(0, HeaderTableID, TransactionID, TransStatus);
                var JVItem = objJournalVoucherColl.JournalVoucherDetailColl.ToList();
                foreach (var item in JVItem)
                {
                    UpdateLedgerEntryStatus(0, DetailTableID, item.ID, TransStatus);
                }
                #endregion
                break;
            case 1012:
                #region Update Traasaction Entries
                AppContraColl ContraColl = (AppContraColl)Session[TableConstants.ContraSummary];
                AppContra objJContraColl = ContraColl.FirstOrDefault(x => x.ID == TransactionID);
                objJContraColl.Status = TransStatus;
                objJContraColl.Save();
                #endregion
                #region Update Ledger Entries
                UpdateLedgerEntryStatus(0, HeaderTableID, TransactionID, TransStatus);
                #endregion
                break;
            case 1008:
                #region Update Traasaction Entries
                AppCreditNoteColl CreditNoteColl = (AppCreditNoteColl)Session[TableConstants.CreditNoteSummary];
                AppCreditNote objCreditNoteColl = CreditNoteColl.FirstOrDefault(x => x.ID == TransactionID);
                objCreditNoteColl.Status = TransStatus;
                objCreditNoteColl.Save();
                #endregion
                #region Update Ledger Entries
                UpdateLedgerEntryStatus(0, HeaderTableID, TransactionID, TransStatus);
                #endregion
                break;
            case 1010:
                #region Update Traasaction Entries
                AppDebitNoteColl DebitNoteColl = (AppDebitNoteColl)Session[TableConstants.DebitNoteSummary];
                AppDebitNote objDebitNoteColl = DebitNoteColl.FirstOrDefault(x => x.ID == TransactionID);
                objDebitNoteColl.Status = TransStatus;
                objDebitNoteColl.Save();
                #endregion
                #region Update Ledger Entries
                UpdateLedgerEntryStatus(0, HeaderTableID, TransactionID, TransStatus);
                #endregion
                break;
            case 1003:
                #region Update Traasaction Entries
                AppPaymentVoucherColl PaymentVoucherColl = (AppPaymentVoucherColl)Session[TableConstants.PaymentVoucherSummary];
                AppPaymentVoucher objPaymentVoucher = PaymentVoucherColl.FirstOrDefault(x => x.ID == TransactionID);
                objPaymentVoucher.Status = TransStatus;
                objPaymentVoucher.Save();
                #endregion
                #region Update Ledger Entries
                UpdateLedgerEntryStatus(0, HeaderTableID, TransactionID, TransStatus);
                var PVItem = objPaymentVoucher.PaymentVoucherDetailColl.ToList();
                foreach (var item in PVItem)
                {
                    UpdateLedgerEntryStatus(0, DetailTableID, item.ID, TransStatus);
                }
                #endregion
                break;
            case 1013:
                #region Update Traasaction Entries
                AppReceiptVoucherColl ReceiptVoucherColl = (AppReceiptVoucherColl)Session[TableConstants.ReceiptVoucherSummary];
                AppReceiptVoucher objReceiptVoucher = ReceiptVoucherColl.FirstOrDefault(x => x.ID == TransactionID);
                objReceiptVoucher.Status = TransStatus;
                objReceiptVoucher.Save();
                #endregion
                #region Update Ledger Entries
                UpdateLedgerEntryStatus(0, HeaderTableID, TransactionID, TransStatus);
                var RVItem = objReceiptVoucher.ReceiptVoucherDetailColl.ToList();
                foreach (var item in RVItem)
                {
                    UpdateLedgerEntryStatus(0, DetailTableID, item.ID, TransStatus);
                }
                #endregion
                break;
            default:
                break;
        }
    }
    public byte Update(GridView Grid, Int16 TransStatus, int HeaderTableID, int DetailTableID)
    {
        byte count = 0;
        if (CheckItemSelectedinGrid(Grid, "ChkSelect") == false)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please select at least one record.','2'" + ");", true);
            return count;
        }
        for (int i = 0; i < Grid.Rows.Count; i++)
        {
            CheckBox ChkSelect = (CheckBox)Grid.Rows[i].FindControl("ChkSelect");
            Label lblID = (Label)Grid.Rows[i].FindControl("lblID");
            Int32 TransactionID = new AppConvert(lblID.Text);
            if (ChkSelect.Checked == true && ChkSelect.Enabled == true)
            {
                count = 1;
                UpdateDetail(TransactionID, TransStatus, HeaderTableID, DetailTableID);
            }
        }
        if (count == 1)
        {

            if (new AppConvert((int)RecordStatus.Approve) == TransStatus)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Approved Sucessfully','1'" + ");", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Deleted Sucessfully','1'" + ");", true);
            }

        }
        return count;
    }
}