﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
/// <summary>
/// Summary description for CommpnFunctions
/// </summary>
public class CommonFunctions : BigSunPage
{
    //public CommonFunctions()
    //{
    //    //
    //    // TODO: Add constructor logic here
    //    //
    //}
    //#region BindCurrencyTypes
    public static void BindCurrencyTypes(DropDownList ddlControlName, string DataTextField, string DataValueField)
    {
        AppCurrencyTypeColl objCurrencyTypeColl = new AppCurrencyTypeColl(1);
        objCurrencyTypeColl.Search(RecordStatus.AllActive);
        ddlControlName.DataSource = objCurrencyTypeColl;
        ddlControlName.DataTextField = DataTextField;
        ddlControlName.DataValueField = DataValueField;
        ddlControlName.DataBind();
        ListItem defaultItem = new ListItem();
        defaultItem.Text = "Select Type";
        defaultItem.Value = "0";
        ddlControlName.Items.Insert(0, defaultItem);
    }
    //#endregion    
    //#region BindDropDown
    //public static void BindUnitOfMeasurement(DropDownList ddlUOM, string DataTextField, string DataValueField)
    //{
    //    AppUOMTypeColl objUOMColl = new AppUOMTypeColl();
    //    objUOMColl.GetAll();
    //    ddlUOM.DataSource = objUOMColl;
    //    ddlUOM.DataTextField = DataTextField;
    //    ddlUOM.DataValueField = DataValueField;
    //    ddlUOM.DataBind();
    //    ListItem defaultItem = new ListItem();
    //    defaultItem.Text = "Select Type";
    //    defaultItem.Value = "0";
    //    ddlUOM.Items.Insert(0, defaultItem);
    //}
    //#endregion
    //#region BindDepartmentTypes
    //public static void BindDepartmentTypes(DropDownList ddlDepartment)
    //{
    //    AppDepartmentTypeColl objDepartmentTypeColl = new AppDepartmentTypeColl();
    //    objDepartmentTypeColl.GetAll();
    //    ddlDepartment.DataSource = objDepartmentTypeColl;
    //    ddlDepartment.DataTextField = "Name";
    //    ddlDepartment.DataValueField = "ID";
    //    ddlDepartment.DataBind();
    //    ListItem defaultItem = new ListItem();
    //    defaultItem.Text = "Select Type";
    //    defaultItem.Value = "0";
    //    ddlDepartment.Items.Insert(0, defaultItem);
    //}
    //#endregion
    //#region Bind DesignationTypes
    //public static void BindDesignationTypes(DropDownList ddlDesignation)
    //{
    //    AppDesignationTypeColl objDesignationTypeColl = new AppDesignationTypeColl();
    //    objDesignationTypeColl.GetAll();
    //    ddlDesignation.DataSource = objDesignationTypeColl;
    //    ddlDesignation.DataTextField = "Name";
    //    ddlDesignation.DataValueField = "ID";
    //    ddlDesignation.DataBind();
    //    ListItem defaultItem = new ListItem();
    //    defaultItem.Text = "Select Type";
    //    defaultItem.Value = "0";
    //    ddlDesignation.Items.Insert(0, defaultItem);
    //}
    //#endregion
    //#region Bind BindPandLType
    //public static void BindPandLType(DropDownList ddlPandLType)
    //{
    //    AppPandLTypeColl objPandLTypeColl = new AppPandLTypeColl();
    //    objPandLTypeColl.GetAll();
    //    ddlPandLType.DataSource = objPandLTypeColl;
    //    ddlPandLType.DataTextField = "Name";
    //    ddlPandLType.DataValueField = "ID";
    //    ddlPandLType.DataBind();
    //    ListItem defaultItem = new ListItem();
    //    defaultItem.Text = "Select Type";
    //    defaultItem.Value = "0";
    //    ddlPandLType.Items.Insert(0, defaultItem);
    //}
    //#endregion
    //#region BindBalanceType
    //public static void BindBalanceType(DropDownList ddlBalanceType)
    //{
    //    AppBalanceSheetTypeColl objBalanceTypeColl = new AppBalanceSheetTypeColl();
    //    objBalanceTypeColl.GetAll();
    //    ddlBalanceType.DataSource = objBalanceTypeColl;
    //    ddlBalanceType.DataTextField = "Name";
    //    ddlBalanceType.DataValueField = "ID";
    //    ddlBalanceType.DataBind();
    //    ListItem defaultItem = new ListItem();
    //    defaultItem.Text = "Select Type";
    //    defaultItem.Value = "0";
    //    ddlBalanceType.Items.Insert(0, defaultItem);
    //}
    //#endregion
    /// <summary>
    /// Testing yet to be done for the folloing function
    /// </summary>
    /// <param name="RequestingUserID"></param>
    /// <param name="SourceType"></param>
    /// <param name="ddlControl"></param>
    /// <param name="DataTextField"></param>
    /// <param name="DataValueField"></param>
    /// <param name="SelectedValue"></param>
    /// <param name="AddSelect"></param>
    public static void BindDropDown(int RequestingUserID, AppObjects.Components SourceType, DropDownList ddlControl, string DataTextField, string DataValueField, string SelectedValue = null, bool AddSelect = true, int Param = 0)
    {
        IList objColl = null;
        switch (SourceType)
        {
            case AppObjects.Components.AppComponent: objColl = new AppAppComponentColl(RequestingUserID); ((AppAppComponentColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.AppComponentRight: objColl = new AppAppComponentRightColl(RequestingUserID); ((AppAppComponentRightColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.AccountType: objColl = new AppAccountTypeColl(RequestingUserID); ((AppAccountTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.LedgerType: objColl = new AppLedgerTypeColl(RequestingUserID); ((AppLedgerTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.BankType: objColl = new AppBankTypeColl(RequestingUserID); ((AppBankTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.CategoryType: objColl = new AppCategoryTypeColl(RequestingUserID); ((AppCategoryTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.Company: objColl = new AppCompanyColl(RequestingUserID); ((AppCompanyColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.CompanyAddress: objColl = new AppCompanyAddressColl(RequestingUserID); ((AppCompanyAddressColl)objColl).Search(RecordStatus.AllActive); break;

            case AppObjects.Components.CompanyBank: objColl = new AppCompanyBankColl(RequestingUserID); ((AppCompanyBankColl)objColl).Search(Param); break;

            case AppObjects.Components.CompanyContact: objColl = new AppCompanyContactColl(RequestingUserID); ((AppCompanyContactColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.CompanyDocument: objColl = new AppCompanyDocumentColl(RequestingUserID); ((AppCompanyDocumentColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.CompanyStatutory: objColl = new AppCompanyStatutoryColl(RequestingUserID); ((AppCompanyStatutoryColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.CompanyUser: objColl = new AppCompanyUserColl(RequestingUserID); ((AppCompanyUserColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.CountryType: objColl = new AppCountryTypeColl(RequestingUserID); ((AppCountryTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.CurrencyType: objColl = new AppCurrencyTypeColl(RequestingUserID); ((AppCurrencyTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.CustomerItem: objColl = new AppCustomerItemColl(RequestingUserID); ((AppCustomerItemColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.CustomerItemPrice: objColl = new AppCustomerItemPriceColl(RequestingUserID); ((AppCustomerItemPriceColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.CustomerPriceGroup: objColl = new AppCustomerPriceGroupColl(RequestingUserID); ((AppCustomerPriceGroupColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.DepartmentType: objColl = new AppDepartmentTypeColl(RequestingUserID); ((AppDepartmentTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.DesignationType: objColl = new AppDesignationTypeColl(RequestingUserID); ((AppDesignationTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.ExchangeRateDaily: objColl = new AppExchangeRateDailyColl(RequestingUserID); ((AppExchangeRateDailyColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.ExchangeRateMonthly: objColl = new AppExchangeRateMonthlyColl(RequestingUserID); ((AppExchangeRateMonthlyColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.FinancialYearType: objColl = new AppFinancialYearTypeColl(RequestingUserID); ((AppFinancialYearTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.Item: objColl = new AppItemColl(RequestingUserID); ((AppItemColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.ItemAllocation: objColl = new AppItemAllocationColl(RequestingUserID); ((AppItemAllocationColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.ItemAttribute: objColl = new AppItemAttributeColl(RequestingUserID); ((AppItemAttributeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.ItemAttributeValue: objColl = new AppItemAttributeValueColl(RequestingUserID); ((AppItemAttributeValueColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.LedgerEntry: objColl = new AppLedgerEntryColl(RequestingUserID); ((AppLedgerEntryColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.OrganizationType: objColl = new AppOrganizationTypeColl(RequestingUserID); ((AppOrganizationTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.PriceGroup: objColl = new AppPriceGroupColl(RequestingUserID); ((AppPriceGroupColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.PriceGroupItem: objColl = new AppPriceGroupItemColl(RequestingUserID); ((AppPriceGroupItemColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.PurchaseGRN: objColl = new AppPurchaseGRNColl(RequestingUserID); ((AppPurchaseGRNColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.PurchaseGRNItem: objColl = new AppPurchaseGRNItemColl(RequestingUserID); ((AppPurchaseGRNItemColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.PurchaseOrder: objColl = new AppPurchaseOrderColl(RequestingUserID); ((AppPurchaseOrderColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.PurchaseOrderItem: objColl = new AppPurchaseOrderItemColl(RequestingUserID); ((AppPurchaseOrderItemColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.SaleOrder: objColl = new AppSaleOrderColl(RequestingUserID); ((AppSaleOrderColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.StatutoryType: objColl = new AppStatutoryTypeColl(RequestingUserID); ((AppStatutoryTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.StockIn: objColl = new AppStockInColl(RequestingUserID); ((AppStockInColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.TaxType: objColl = new AppTaxTypeColl(RequestingUserID); ((AppTaxTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.TaxTypeConfiguration: objColl = new AppTaxTypeConfigurationColl(RequestingUserID); ((AppTaxTypeConfigurationColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.TermsAndConditions: objColl = new AppTermsAndConditionsColl(RequestingUserID); ((AppTermsAndConditionsColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.UOMConversion: objColl = new AppUOMConversionColl(RequestingUserID); ((AppUOMConversionColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.UOMType: objColl = new AppUOMTypeColl(RequestingUserID); ((AppUOMTypeColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.VendorItem: objColl = new AppVendorItemColl(RequestingUserID); ((AppVendorItemColl)objColl).Search(RecordStatus.AllActive); break;
            case AppObjects.Components.ZipCodeType: objColl = new AppZipCodeTypeColl(RequestingUserID); ((AppZipCodeTypeColl)objColl).Search(RecordStatus.AllActive); break;


            case AppObjects.Components.PaymentType: objColl = new AppPaymentTypeColl(RequestingUserID); ((AppPaymentTypeColl)objColl).Search(Param); break;

        }
        if (objColl != null)
        {
            ddlControl.DataSource = objColl;
            ddlControl.DataTextField = DataTextField;
            ddlControl.DataValueField = DataValueField;
            ddlControl.DataBind();
            if (AddSelect)
            {
                ListItem defaultItem = new ListItem();
                defaultItem.Text = "Select Type";
                defaultItem.Value = "0";
                ddlControl.Items.Insert(0, defaultItem);
            }
            if (SelectedValue == null)
                ddlControl.Items[0].Selected = true;
            else
            {
                foreach (ListItem item in ddlControl.Items)
                    item.Selected = item.Value.ToUpper().Trim().Equals(SelectedValue.ToUpper().Trim());
            }
        }
    }
    #region GetStatus
    public static RecordStatus GetStatus(string Status)
    {
        RecordStatus ReturtnVal = RecordStatus.ALL;
        switch (Status)
        {
            case "-32768":
                ReturtnVal = RecordStatus.ALL;
                break;
            case "-10":
                ReturtnVal = RecordStatus.Deleted;
                break;
            case "-1":
                ReturtnVal = RecordStatus.InActive;
                break;
            case "2":
                ReturtnVal = RecordStatus.Approve;
                break;
            case "1":
                ReturtnVal = RecordStatus.Active;
                break;
            default:
                ReturtnVal = RecordStatus.ALL;
                break;
        }
        return ReturtnVal;
    }
    #endregion

    public static void BindCompanyBanks(int RequestingUserID, DropDownList ddlControl, string DataTextField, string DataValueField, string SelectedValue = null, bool AddSelect = true, int Param = 0)
    {

        AppBankTypeColl objBankTypeTypeColl = new AppBankTypeColl(RequestingUserID);
        objBankTypeTypeColl.SearchBankBranch(Param, RequestingUserID);
        ddlControl.DataSource = objBankTypeTypeColl;
        ddlControl.DataTextField = DataTextField;
        ddlControl.DataValueField = DataValueField;
        ddlControl.DataBind();
        if (AddSelect)
        {
            ListItem defaultItem = new ListItem();
            defaultItem.Text = "Select Type";
            defaultItem.Value = "0";
            ddlControl.Items.Insert(0, defaultItem);
        }
    }

    static int discountLedgerID = 0;
    public static int DiscountLedgerID
    {
        get
        {
            if (discountLedgerID == 0)
            {
                AppLedgerTypeColl objLedgerColl = new AppLedgerTypeColl(1);
                discountLedgerID = (int)objLedgerColl.Where(x => x.LedgerTypeName == "DiscountType").Select(x => x.ID).FirstOrDefault();
            }

            return discountLedgerID;
        }
        set
        {
            discountLedgerID = value;
        }
    }

    public decimal GetTDSRate(string TDSID, string VoucherDate)
    {
        if (!string.IsNullOrEmpty(TDSID) && !string.IsNullOrEmpty(VoucherDate))
        {
            AppTDSRateColl objTDSRateColl = new AppTDSRateColl(1);
            objTDSRateColl.AddCriteria(TDSRate.EffectiveFrom, Operators.LessOrEqualTo, VoucherDate);
            objTDSRateColl.AddCriteria(TDSRate.TDS_ID, Operators.Equals, TDSID);
            objTDSRateColl.Search(RecordStatus.Active);
            if (objTDSRateColl != null)
            {
                if (objTDSRateColl.Count > 0)
                {
                    return new AppConvert(objTDSRateColl.FirstOrDefault(x=> x.ID==(objTDSRateColl.Max(m=> m.ID))).Rate);
                }
                else
                {
                    return -99;
                }

            }
            else
            {
                return -99;
            }
        }

        return 0;
    }

    public string GetReportName(string ReportID, string CompanyID)
    {
        if (!string.IsNullOrEmpty(ReportID) && !string.IsNullOrEmpty(CompanyID))
        {
            AppReportsColl objReportColl = new AppReportsColl(1);
            objReportColl.AddCriteria(Reports.ReportID, Operators.Equals, ReportID);
            objReportColl.AddCriteria(Reports.Company_ID, Operators.Equals, CompanyID);
            objReportColl.Search(RecordStatus.Active);
            if (objReportColl != null)
            {
                if (objReportColl.Count > 0)
                {
                    return new AppConvert(objReportColl.FirstOrDefault().ReportsName);
                }
            }
        }
        return "";
    }

    public string GetReportOutput(string ReportID, string CompanyID)
    {
        if (!string.IsNullOrEmpty(ReportID) && !string.IsNullOrEmpty(CompanyID))
        {
            AppReportsColl objReportColl = new AppReportsColl(1);
            objReportColl.AddCriteria(Reports.ReportID, Operators.Equals, ReportID);
            objReportColl.AddCriteria(Reports.Company_ID, Operators.Equals, CompanyID);
            objReportColl.Search(RecordStatus.Active);
            if (objReportColl != null)
            {
                if (objReportColl.Count > 0)
                {
                    return new AppConvert(objReportColl.FirstOrDefault().ReportOutput);
                }
            }
        }
        return "";
    }
}