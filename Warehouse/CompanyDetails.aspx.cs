﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to add details of vendors
*/
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
#endregion
public partial class CompanyDetails :BigSunPage
{
    AppObjects.AppCompany obj = null;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
       obj = new AppObjects.AppCompany(this.intRequestingUserID);
        if (!IsPostBack)
        {  
            Session.Add(TableConstants.ObjCompanySession,obj);            
            CommonFunctions.BindDropDown(intRequestingUserID, Components.OrganizationType, ddlOrganizationType, "Name", "ID", "", false);
            CommonFunctions.BindDropDown(intRequestingUserID, Components.CurrencyType, ddlCurrency, "Name", "ID","",false);
            string ID = Convert.ToString(Request.QueryString["CompanyID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int CompanyID = new AppUtility.AppConvert(ID);
                AppObjects.AppCompany objCompany = new AppObjects.AppCompany(intRequestingUserID,CompanyID);
                txtCompanyCode.Text = objCompany.Code;
                txtCompanyName.Text = objCompany.Name;
                txtDescription.Text = objCompany.Description;
                hdfParentCompanyID.Value = new AppConvert(objCompany.Parent_ID);
                hdfParentCompanyName.Value = objCompany.Parent.Name;
                txtParentCompany.Text = objCompany.Parent.Name;
                ddlCurrency.SelectedValue = new AppConvert(objCompany.CurrencyType_ID);
                ddlOrganizationType.SelectedValue = new AppUtility.AppConvert(objCompany.OrganizationType_ID);
                hdfTermsAndConditionsID.Value = new AppConvert(objCompany.TermsAndConditions_ID);
                hdfTermsAndConditionsName.Value = new AppConvert(objCompany.TermsAndConditions.Statement);
                txtTermsAndConditions.Text = new AppConvert(objCompany.TermsAndConditions.Statement);
                Session.Add(TableConstants.ObjCompanySession, objCompany);
                btnApprove.Visible = true;
            }  
            else
            {
                btnApprove.Visible = false;
            }          
        }
    }
    #endregion    
    #region btnSave_Click
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string Message = "Company Updated Successfully";
        AppObjects.AppCompany objVendor = (AppObjects.AppCompany)Session[TableConstants.ObjCompanySession];
        if (objVendor == null)
        {
            objVendor = new AppObjects.AppCompany(this.intRequestingUserID);
            Message = "Company Added Successfully";
        }        
        objVendor.Code = txtCompanyCode.Text;
        objVendor.Name = txtCompanyName.Text;
        objVendor.Description = txtDescription.Text;
        objVendor.Parent_ID = new AppConvert(hdfParentCompanyID.Value);
        objVendor.OrganizationType_ID = new AppConvert(ddlOrganizationType.SelectedValue);
        objVendor.CurrencyType_ID = new AppConvert(ddlCurrency.SelectedValue);
        objVendor.Status = (chkActive.Checked==true?new AppConvert((int)RecordStatus.Active): new AppConvert((int)RecordStatus.Created));
        objVendor.Type = new AppConvert((int)CompanyRoleType.Self);
        objVendor.TermsAndConditions_ID = new AppConvert(hdfTermsAndConditionsID.Value);        
        objVendor.ModifiedBy = this.intRequestingUserID;
        objVendor.ModifiedOn = System.DateTime.Now;
        objVendor.Save();
        Session.Remove(TableConstants.ObjCompanySession);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + Message + "','1'" + ");", true);
        AppCompany objNewCompany = new AppCompany(intRequestingUserID);
        Session.Add(TableConstants.ObjCompanySession,objNewCompany);
    }
    #endregion
    #region btnCancel_Click
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.ObjCompanySession);
        Response.Redirect("CompanySummary.aspx");
    }
    #endregion
    #region btnApprove_Click
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        obj = (AppObjects.AppCompany)Session[TableConstants.ObjCompanySession];
        obj.Status = new AppConvert(RecordStatus.Approve);
        obj.Save();
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Company Approved','1'" + ");", true);
        //Response.Redirect("VendorSummary.Aspx");
    }
    #endregion
}