﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="RoleRights" Codebehind="RoleRights.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script runat="server">
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="PnlAddAddress" runat="server">
        <ContentTemplate>
            <div class="col-lg-12">
                <div class=" panel panel-body">
                    <div class="row">
                        <div class=" col-lg-3">
                            <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClas="bold"></asp:Label>
                            <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </div>
                        <div class=" col-lg-3">
                            <asp:Label ID="lblDesignation" runat="server" Text="Designation" CssClas="bold"></asp:Label>
                            <asp:DropDownList ID="ddlDesignation" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlDesignation_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="newline"></div>
                    <br />
                    <div class="row">
                        <div class=" col-lg-6">
                            <asp:GridView ID="grdRoles" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-hover text-nowrap" OnPreRender="grdRoles_PreRender">
                                <Columns>
                                    <asp:TemplateField HeaderText="Component">
                                        <ItemTemplate>
                                            <asp:Label ID="lblComponentgrd" runat="server" Text='<%# Eval("AppComponent.Name")%>' CssClas="bold"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rights Assigned">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlRightsgrd" runat="server" CssClass="form-control" >
                                                <asp:ListItem Text="No Right" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="View Access" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Add Access" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Edit Access" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="Delete Access" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Div for footer  -->
    <div class="navbar navbar-fixed-bottom navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#divFooter">
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="divFooter" class="navbar-collapse collapse">
            <div class="container">
                <ul class="nav navbar-nav navbar-right" style="padding-top: 0.7%;">
                    <li>                      
                        <div class="col-sm-1">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary btn-sm" OnClick="btnSave_Click" />
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end footer -->
</asp:Content>

