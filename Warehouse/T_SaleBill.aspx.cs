﻿#region Author
/*
Name    :   Pradip BObhate
Date    :   04/08/2016
Use     :   This form is used to add Sale Order details  
*/
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.Services;
using AppUtility;
using Objects;
#endregion
public partial class T_SaleBill : BigSunPage
{
    #region Page_Load
    AppObjects.AppSaleBill obj = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = new AppObjects.AppSaleBill(intRequestingUserID);
        if (!IsPostBack)
        {
            
            ACEtxtSaleOrder.ContextKey = new AppConvert(LoginCompanyID);
            obj = new AppSaleBill(intRequestingUserID);
            obj.BillType = new AppConvert(Request.QueryString["BillType"]);
            Session[TableConstants.SaleBillSession] = obj;
            string ID = Convert.ToString(Request.QueryString["SaleBillID"]);
            divSaleOrder.Visible = obj.BillType == 1 ? false : true;
            if (!string.IsNullOrEmpty(ID))
            {
                int SaleBillID = new AppUtility.AppConvert(ID);
                obj = new AppObjects.AppSaleBill(intRequestingUserID, SaleBillID);
                Session[TableConstants.SaleBillSession] = obj;
                PopulateData();
            }
            else
            {
                DefaultData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
            }
            BindData();
            SetValueToGridFooter();
           
        }
    }
    #endregion
    #region Button Click Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppSaleBill SaleBill = (AppSaleBill)Session[TableConstants.SaleBillSession];
        SaleBill.SaleBillItemColl.RemoveAll(x => x.Status == new AppConvert((int)RecordStatus.Deleted));
        if (SaleBill.SaleBillItemColl.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please add atleast one item','2'" + ");", true);
            return;
        }

        if (txtDiscount.Text!="0" && txtDiscount.Text != "" &&  DiscountLedgerID==0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please create discount ledger','2'" + ");", true);
            return;
        }
        if (SaleBill.RoundAmount!=0 && RoundingOFFLedgerID == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please create RoundOFF ledger','2'" + ");", true);
            return;
        }
        SaveSaleBillHeader();
        #region Add Ledger Entries
        LedgerEntry(LoginCompanyID, 101501, 1015, SaleBill.Customer_Company.Customer_Ledger_ID, SaleBill.ID, SaleBill.SaleBillDate,0, SaleBill.BaseAmount, SaleBill.SaleBillNumber, SaleBill.ModifiedBy, SaleBill.ModifiedOn,0, SaleBill.Status, SaleBill.Type,0,0, SaleBill.VoucherType_ID, SaleBill.Remark);
        if (SaleBill.TaxType.TaxTypeConfigurationColl.Count > 0 && SaleBill.TaxType_ID>0)
        {
            List<TaxDetails> objTaxDetails = SaleBill.TaxType.CalculateTax(SaleBill.BaseAmount-SaleBill.TaxAmount, 0, 0);
            foreach (TaxDetails item in objTaxDetails)
            {
                if (item.Ledger_ID != 0)
                {
                    UpdateLedgerEntryStatus(101502, 1015, SaleBill.ID, new AppUtility.AppConvert((int)RecordStatus.Deleted));
                    LedgerEntry(LoginCompanyID, 101502, 1015, item.Ledger_ID, SaleBill.ID, SaleBill.SaleBillDate, item.TaxValue, 0, SaleBill.SaleBillNumber, SaleBill.ModifiedBy, SaleBill.ModifiedOn,0,SaleBill.Status, SaleBill.Type, 0, 0, SaleBill.VoucherType_ID, SaleBill.Remark);
                }
            }
        }
         
        LedgerEntry(LoginCompanyID, 101503, 1015, DiscountLedgerID, SaleBill.ID, SaleBill.SaleBillDate,0, SaleBill.DiscountAmount, SaleBill.SaleBillNumber, SaleBill.ModifiedBy, SaleBill.ModifiedOn,0,SaleBill.Status, SaleBill.Type, 0, 0, SaleBill.VoucherType_ID, SaleBill.Remark);
         
        LedgerEntry(LoginCompanyID, 101504, 1015, RoundingOFFLedgerID, SaleBill.ID, SaleBill.SaleBillDate,0, SaleBill.RoundAmount, SaleBill.SaleBillNumber, SaleBill.ModifiedBy, SaleBill.ModifiedOn,0, SaleBill.Status, SaleBill.Type, 0, 0, SaleBill.VoucherType_ID, SaleBill.Remark);
        if (SaleBill.SaleBillItemColl.Count > 0)
        {
            foreach (var item in SaleBill.SaleBillItemColl)
            {                           // Item Ledger
                if (item.BaseAmount != 0) { LedgerEntry(LoginCompanyID, 101601, 1016, item.Item.Sales_Ledger_ID, item.ID, SaleBill.SaleBillDate, item.BaseAmount, 0, SaleBill.SaleBillNumber, item.ModifiedBy, item.ModifiedOn, item.Status, SaleBill.Status, item.Type, 0, 0, SaleBill.VoucherType_ID); }
                // tax Ledger
                if (item.TaxAmount != 0)
                {
                    if (item.TaxType.TaxTypeConfigurationColl.Count > 0 && item.TaxType_ID>0)
                    {
                        List<TaxDetails> objTaxDetails = item.TaxType.CalculateTax(item.BaseAmount, 0, 0);
                        foreach (TaxDetails td in objTaxDetails)
                        {
                            if (td.Ledger_ID != 0)
                            {
                                UpdateLedgerEntryStatus(101602, 1016, item.ID, new AppUtility.AppConvert((int)RecordStatus.Deleted));
                                LedgerEntry(LoginCompanyID, 101602, 1016, td.Ledger_ID, item.ID, SaleBill.SaleBillDate, td.TaxValue, 0, SaleBill.SaleBillNumber, SaleBill.ModifiedBy, SaleBill.ModifiedOn, item.Status, SaleBill.Status, SaleBill.Type, 0, 0, SaleBill.VoucherType_ID);
                            }
                        }
                    }
                }
            }
        }
        #endregion
        Session.Remove(TableConstants.SaleBillSession);
        Response.Redirect("T_SaleBill.aspx?Msg=1&BillType=" + new AppConvert(Request.QueryString["BillType"]));
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.SaleBillSession);
        Response.Redirect("T_SaleBillSummary.aspx");
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        AppSaleBill SaleBill = (AppSaleBill)Session[TableConstants.SaleBillSession];
        SaleBill.Status = new AppConvert((int)RecordStatus.Approve);
        SaleBill.Save();
        Session[TableConstants.SaleBillSession] = SaleBill;
    }
    protected void btnAddLineItem_Click(object sender, EventArgs e)
    {
        //BindGridData();
        AddEmptyRow(1);
        if (grdSaleBillDetails.Rows.Count >= 0)
        {
            TextBox txtItem = (TextBox)grdSaleBillDetails.Rows[0].FindControl("txtItem");
            txtItem.Focus();
        }
    }
    #endregion
    #region Functions
    public void DefaultData()
    {
        txtSaleDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtDeliveryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtCompanyAddress.Text = this.DefaultCompanyAddress;
        hdfCompanyAddressName.Value = txtCompanyAddress.Text;
        hdfCompanyAddressID.Value = new AppConvert(this.DefaultCompanyAddressID);
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyName.Value = txtHeaderBaseCurrency.Text;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);

        //hdfTermsAndConditionsID.Value =
        ACEtxtDeliveryAddress.ContextKey = new AppConvert(Session[TableConstants.LoginCompanyID]);
        ACEtxtCompanyAddress.ContextKey = new AppConvert(Session[TableConstants.LoginCompanyID]);
    }
    public void BindData()
    {
        AppSaleBill SaleBill = (AppSaleBill)Session[TableConstants.SaleBillSession];
       
        if (SaleBill == null)
        {
            SaleBill = new AppSaleBill(intRequestingUserID);
        }
        grdSaleBillDetails.DataSource = SaleBill.SaleBillItemColl.Where(x => x.Status >= 0);
        grdSaleBillDetails.DataBind();
        if (SaleBill.SaleBillItemColl.Count <= 0)
        {
            AddEmptyRow(1);
        }
    }
    public void SaveSaleBillHeader()
    {
        AppSaleBill SaleBill = (AppSaleBill)Session[TableConstants.SaleBillSession];
        SaleBill.Company_ID = new AppConvert(LoginCompanyID);
        SaleBill.CompanyAddress_ID = new AppConvert(hdfCompanyAddressID.Value);
        SaleBill.Customer_Company_ID = new AppConvert(hdfCustomerID.Value);
        SaleBill.Delivery_CompanyAddress_ID = new AppConvert(hdfDeliveryAddressID.Value);
        SaleBill.SaleBillDate = new AppConvert(txtSaleDate.Text);
        SaleBill.TermsAndConditions_ID = new AppConvert(hdfTermsAndConditionsID.Value);
        SaleBill.TaxType_ID = new AppConvert(hdfHeaderTaxID.Value);
        SaleBill.CurrencyType_ID = new AppConvert(hdfHeaderExchangeCurrencyID.Value);
        SaleBill.BaseAmount = new AppConvert(txtTotalAmount.Text);
        SaleBill.TaxAmount = new AppConvert(txtHeaderTaxAmount.Text);
        SaleBill.OtherCharges = 0;
        SaleBill.DiscountValue = new AppConvert(txtDiscount.Text);
        SaleBill.DiscountType = new AppConvert(hdfDiscountType.Value);
        SaleBill.DiscountAmount = SaleBill.CalDiscountAmount;
        SaleBill.FinalAmount = new AppConvert(txtTotalAmount.Text);
        SaleBill.RoundAmount = 0;
        SaleBill.PODate = new AppConvert(txtPODate.Text);
        SaleBill.PORefernce = new AppConvert(txtPOReference.Text);
        SaleBill.VoucherType_ID = Convert.ToString(Request.QueryString["SaleBillID"]) == "1" ? 16 : 14;
      
        SaleBill.DeliveryDate = new AppConvert(txtDeliveryDate.Text);
        SaleBill.Status = new AppConvert((int)RecordStatus.Created);
        SaleBill.Type = SaleBill.Type;
        SaleBill.ModifiedBy = intRequestingUserID;
        SaleBill.ModifiedOn = System.DateTime.Now;
        SaleBill.Remark = txtRemark.Text;
        if (SaleBill.ID == 0)
        {
            SaleBill.SaleBillNumber = new AppConvert(AppUtility.AutoID.GetNextNumber(LoginCompanyID, "SB", System.DateTime.Now,"SB-", 4, ResetCycle.Yearly));
        }
        SaleBill.Save();
    }
    public void LineItemCalculation(int RowIndex)
    {
        AppSaleBill SaleBill = (AppSaleBill)Session[TableConstants.SaleBillSession];
        TextBox txtQuantity = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtQuantity");
        TextBox txtRate = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtRate");
        TextBox txtBasePrice = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtBasePrice");
        TextBox txtNetAmount = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtNetAmount");
        TextBox txtDiscount = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtDiscount");
        HiddenField hdfDiscountType = (HiddenField)grdSaleBillDetails.Rows[RowIndex].FindControl("hdfDiscountType");
        HiddenField hdfLineItemTaxID = (HiddenField)grdSaleBillDetails.Rows[RowIndex].FindControl("hdfLineItemTaxID");
        HiddenField hdfUOMID = (HiddenField)grdSaleBillDetails.Rows[RowIndex].FindControl("hdfUOMID");
        SaleBill.SaleBillItemColl[RowIndex].Company_ID = new AppConvert(LoginCompanyID);
        SaleBill.SaleBillItemColl[RowIndex].CompanyAddress_ID = new AppConvert(hdfCompanyAddressID.Value);
        SaleBill.SaleBillItemColl[RowIndex].Delivery_Company_ID = new AppConvert(hdfCustomerID.Value);
        SaleBill.SaleBillItemColl[RowIndex].Delivery_CompanyAddress_ID = new AppConvert(hdfDeliveryAddressID.Value);
        decimal Quantity = new AppUtility.AppConvert(txtQuantity.Text);
        SaleBill.SaleBillItemColl[RowIndex].Quantity = Quantity;
        SaleBill.SaleBillItemColl[RowIndex].Rate = new AppUtility.AppConvert(txtRate.Text);
        SaleBill.SaleBillItemColl[RowIndex].DiscountValue = new AppUtility.AppConvert(txtDiscount.Text);
        SaleBill.SaleBillItemColl[RowIndex].DiscountAmount = SaleBill.SaleBillItemColl[RowIndex].CalDiscountAmount;
        SaleBill.SaleBillItemColl[RowIndex].DiscountType = new AppConvert(hdfDiscountType.Value == "0" ? 1 : new AppConvert(hdfDiscountType.Value));
        SaleBill.SaleBillItemColl[RowIndex].BaseAmount = SaleBill.SaleBillItemColl[RowIndex].PostDiscountPrice;
        SaleBill.SaleBillItemColl[RowIndex].UOMType_ID = new AppUtility.AppConvert(hdfUOMID.Value);
        SaleBill.SaleBillItemColl[RowIndex].TaxType_ID = new AppUtility.AppConvert(hdfLineItemTaxID.Value);
        if (string.IsNullOrEmpty(hdfLineItemTaxID.Value) == false && hdfLineItemTaxID.Value != "0")
        {
            SaleBill.SaleBillItemColl[RowIndex].TaxType.CalculateTax(new AppConvert(SaleBill.SaleBillItemColl[RowIndex].BaseAmount), 0, 0);
            SaleBill.SaleBillItemColl[RowIndex].TaxAmount = SaleBill.SaleBillItemColl[RowIndex].TaxType.TaxValue;
        }
        SaleBill.SaleBillItemColl[RowIndex].NetAmount = SaleBill.SaleBillItemColl[RowIndex].CalNetAmount;
        SaleBill.SaleBillItemColl[RowIndex].FinalAmount = SaleBill.SaleBillItemColl[RowIndex].CalFinalAmount;
        SaleBill.SaleBillItemColl[RowIndex].ModifiedBy = intRequestingUserID;
        SaleBill.SaleBillItemColl[RowIndex].ModifiedOn = System.DateTime.Now;
        Session.Add(TableConstants.SaleBillSession, SaleBill);
        BindData();
        SetValueToGridFooter();
        HeaderCalculation();
    }
    public void SetValueToGridFooter()
    {
        AppSaleBill SaleBill = (AppSaleBill)Session[TableConstants.SaleBillSession];
        Label lblFooterCurrencyAmount = (Label)grdSaleBillDetails.FooterRow.FindControl("lblFooterCurrencyAmount");
        lblFooterCurrencyAmount.Text = Convert.ToDecimal(SaleBill.SaleBillItemColl.Where(x => x.Status >= 0).Sum(x => x.ConvertedCurrencyAmount)).ToString("#,##0.00");
        Label lblFooterNetAmount = (Label)grdSaleBillDetails.FooterRow.FindControl("lblFooterNetAmount");
        lblFooterNetAmount.Text = Convert.ToDecimal(SaleBill.SaleBillItemColl.Where(x => x.Status >= 0).Sum(x => x.NetAmount)).ToString("#,##0.00");
        Label lblFooterTax = (Label)grdSaleBillDetails.FooterRow.FindControl("lblFooterTax");
        lblFooterTax.Text = Convert.ToDecimal(SaleBill.SaleBillItemColl.Where(x => x.Status >= 0).Sum(x => x.TaxAmount)).ToString("#,##0.00");
        Label lblFooterDiscount = (Label)grdSaleBillDetails.FooterRow.FindControl("lblFooterDiscount");
        lblFooterDiscount.Text = Convert.ToDecimal(SaleBill.SaleBillItemColl.Where(x => x.Status >= 0).Sum(x => x.DiscountValue)).ToString("#,##0.00");
        Label lblFooterBasePrice = (Label)grdSaleBillDetails.FooterRow.FindControl("lblFooterBasePrice");
        lblFooterBasePrice.Text = Convert.ToDecimal(SaleBill.SaleBillItemColl.Where(x => x.Status >= 0).Sum(x => x.BasePrice)).ToString("#,##0.00");
        Label lblFooterQuantity = (Label)grdSaleBillDetails.FooterRow.FindControl("lblFooterQuantity");
        lblFooterQuantity.Text = Convert.ToDecimal(SaleBill.SaleBillItemColl.Where(x => x.Status >= 0).Sum(x => x.Quantity)).ToString("#,##0.00");
       
    }
    public void AddEmptyRow(int NumberOfRows)
    {
        AppSaleBill SaleBill = (AppSaleBill)Session[TableConstants.SaleBillSession];
        if (SaleBill == null)
        {
            SaleBill = new AppSaleBill(intRequestingUserID);
        }
        AppSaleBillItem SaleBillItem = new AppSaleBillItem(intRequestingUserID);
        for (int i = 0; i < NumberOfRows; i++)
        {
            if (SaleBill.SaleBillItemColl.Count == 0)
            {
                SaleBill.SaleBillItemColl.Add(SaleBillItem);
            }
            else
            {
                SaleBill.SaleBillItemColl.Insert(0, SaleBillItem);
            }
            Session.Add(TableConstants.SaleBillSession, SaleBill);
        }
        BindData();
    }
    public void DefaultCustomerRecord(int NumberOfRows, int CustomerID)
    {
        AppSaleBill SaleBill = (AppSaleBill)Session[TableConstants.SaleBillSession];
        if (SaleBill.SaleBillItemColl == null)
        {
            SaleBill = new AppSaleBill(intRequestingUserID);
        }
        AppSaleBillItem SaleBillItem = new AppSaleBillItem(intRequestingUserID);
        for (int i = 0; i < NumberOfRows; i++)
        {
            SaleBill.SaleBillItemColl.Add(SaleBillItem);
            Session.Add(TableConstants.SaleBillSession, SaleBill.SaleBillItemColl);
        }
        BindData();
    }
    public void PopulateData()
    {
        AppSaleBill obj = (AppSaleBill)Session[TableConstants.SaleBillSession];
        txtSaleDate.Text = new AppConvert(this.obj.SaleBillDate);
        hdfSaleOrderID.Value = new AppConvert(obj.VoucherID);
        hdfSaleOrderName.Value = obj.GetSaleOrderDetails.SaleOrderNumber;
        txtSaleOrder.Text = obj.GetSaleOrderDetails.SaleOrderNumber;
        txtSaleOrder.Enabled = false;
        obj.BillType = obj.VoucherType_ID ==16 ? 1 : 2;
        txtCustomer.Text = new AppConvert(obj.Customer_Company.Name);
        hdfCustomerName.Value = txtCustomer.Text;
        hdfCustomerID.Value = new AppConvert(this.obj.Customer_Company.ID);
        txtTotalAmount.Text = new AppConvert(this.obj.BaseAmount);
        txtSaleBillNo.Text = new AppConvert(this.obj.SaleBillNumber);
        txtStatus.Text = this.obj.StatusName;
        txtTermsAndConditions.Text = new AppConvert(this.obj.TermsAndConditions.Statement);
        hdfTermsAndConditionsName.Value = txtTermsAndConditions.Text;
        hdfTermsAndConditionsID.Value = new AppConvert(this.obj.TermsAndConditions.ID);
        txtDiscount.Text = new AppConvert(this.obj.DiscountValue);
        hdfDiscountType.Value = new AppConvert(this.obj.DiscountType);
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyName.Value = txtHeaderBaseCurrency.Text;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);
        txtHeaderExchangeCurrency.Text = this.obj.CurrencyType.Name;
        hdfHeaderExchangeCurrencyName.Value = txtHeaderExchangeCurrency.Text;
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(this.obj.CurrencyType_ID);
        txtCompanyAddress.Text = this.obj.SetCompanyAddress;
        hdfCompanyAddressName.Value = txtCompanyAddress.Text;
        hdfCompanyAddressID.Value = new AppConvert(this.obj.CompanyAddress_ID);
        txtDeliveryAddress.Text = this.obj.SetDeliveryAddress;
        hdfDeliveryAddressName.Value = txtDeliveryAddress.Text;
        hdfDeliveryAddressID.Value = new AppConvert(this.obj.Delivery_CompanyAddress_ID);
        txtDeliveryAddress.Text = this.obj.CustomerAddress;
        hdfDeliveryAddressName.Value = txtDeliveryAddress.Text;
        hdfDeliveryAddressID.Value = new AppConvert(this.obj.CustomerAddressID);
        txtRemark.Text = this.obj.Remark;
        txtPODate.Text = new AppUtility.AppConvert(this.obj.PODate);
        txtPOReference.Text = this.obj.PORefernce;
        txtHeaderTax.Text = this.obj.TaxType.Name;
        hdfHeaderTaxName.Value = txtHeaderTax.Text;
        hdfHeaderTaxID.Value = new AppConvert(this.obj.TaxType_ID);
        txtHeaderTaxAmount.Text = new AppConvert(this.obj.TaxAmount);
        txtPODate.Text = new AppConvert(obj.PODate);
        txtPOReference.Text = obj.PORefernce;
    }
    public void HeaderCalculation()
    {
        AppSaleBill SaleBill = (AppSaleBill)Session[TableConstants.SaleBillSession];
        SaleBill.DiscountType = new AppConvert(hdfDiscountType.Value);
        SaleBill.DiscountValue = new AppConvert(txtDiscount.Text);
        SaleBill.FinalAmount = SaleBill.SaleBillItemColl.Sum(x => x.NetAmount);
        txtTotalAmount.Text = new AppConvert(SaleBill.PostDiscountPrice);

        if (string.IsNullOrEmpty(hdfHeaderTaxID.Value) == false && hdfHeaderTaxID.Value != "0")
        {
            SaleBill.TaxType_ID = new AppConvert(hdfHeaderTaxID.Value);
            SaleBill.TaxType.CalculateTax(new AppConvert(txtTotalAmount.Text), 0, 0);
            txtHeaderTaxAmount.Text = new AppConvert(SaleBill.TaxType.TaxValue);
            txtTotalAmount.Text = new AppConvert(SaleBill.PostDiscountPrice + SaleBill.TaxType.TaxValue);

        }
        else {
            txtHeaderTaxAmount.Text = "0";
        }
        SaleBill.AllocateAmount();
        Session[TableConstants.SaleBillSession] = SaleBill;
    }
    #endregion
    #region Text Box Event
    protected void txtSaleOrder_TextChanged(object sender, EventArgs e)
    {
        obj.VoucherID = new AppConvert(hdfSaleOrderID.Value);
        AppSaleOrder objSaleOrder = new AppSaleOrder(intRequestingUserID, obj.VoucherID);
        obj.Company_ID = LoginCompanyID;
        obj.Customer_Company_ID = objSaleOrder.Delivery_Company_ID;
        hdfDeliveryAddressID.Value = new AppConvert(objSaleOrder.Delivery_CompanyAddress_ID);
        hdfDeliveryAddressName.Value = new AppConvert(objSaleOrder.CustomerAddress);
        txtDeliveryAddress.Text = new AppConvert(objSaleOrder.CustomerAddress);
        hdfTermsAndConditionsID.Value = new AppConvert(objSaleOrder.TermsAndConditions_ID);
        hdfTermsAndConditionsName.Value = new AppConvert(objSaleOrder.TermsAndConditions.Statement);
        txtTermsAndConditions.Text = new AppConvert(objSaleOrder.TermsAndConditions.Statement);
        txtHeaderTax.Text = objSaleOrder.TaxType.Name;
        hdfHeaderTaxName.Value = objSaleOrder.TaxType.Name;
        hdfHeaderTaxID.Value = new AppConvert(objSaleOrder.TaxType_ID);
        hdfHeaderBaseCurrencyID.Value = new AppConvert(objSaleOrder.CurrencyType_ID);
        hdfHeaderBaseCurrencyName.Value = new AppConvert(objSaleOrder.CurrencyType.Name);
        txtHeaderBaseCurrency.Text = new AppConvert(objSaleOrder.CurrencyType.Name);
        txtDiscount.Text = new AppConvert(objSaleOrder.DiscountValue);
        hdfDiscountType.Value = new AppConvert(objSaleOrder.DiscountType);
        txtPODate.Text = new AppConvert(objSaleOrder.PODate);
        txtPOReference.Text = new AppConvert(objSaleOrder.PORefernce);
        txtRemark.Text = objSaleOrder.Remark;
        hdfCustomerID.Value = new AppConvert(objSaleOrder.Delivery_Company_ID);
        obj.GetAllSaleOrderDetails(LoginCompanyID);
        Session.Add(TableConstants.SaleBillSession, obj);
        BindData();
        SetValueToGridFooter();
        HeaderCalculation();
     
    }
    protected void txtDiscount_TextChanged1(object sender, EventArgs e)
    {
        HeaderCalculation();
    }
    protected void txtHeaderTax_TextChanged(object sender, EventArgs e)
    {
        HeaderCalculation();
    }
    protected void txtItem_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfItemID = (HiddenField)grdSaleBillDetails.Rows[RowIndex].FindControl("hdfItemID");
        TextBox txtQuantity = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtQuantity");
        AppSaleBill SaleBill = (AppSaleBill)Session[TableConstants.SaleBillSession];
        SaleBill.SaleBillItemColl[RowIndex].Item_ID = new AppUtility.AppConvert(hdfItemID.Value);
        SaleBill.SaleBillItemColl[RowIndex].Delivery_Company_ID = new AppConvert(hdfCustomerID.Value);
        Session.Remove(TableConstants.SaleBillSession);
        Session.Add(TableConstants.SaleBillSession, SaleBill);
        BindData();
        SetValueToGridFooter();
        txtQuantity.Focus();
    }
    protected void txtQuantity_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtRate = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtRate");
        LineItemCalculation(RowIndex);
        txtRate.Focus();
    }
    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtDiscount = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtDiscount");
        LineItemCalculation(RowIndex);
        txtDiscount.Focus();
    }
    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtLineItemTax = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtLineItemTax");
        LineItemCalculation(RowIndex);
        txtLineItemTax.Focus();
    }
    protected void txtLineItemTax_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        Button btnAddLineItem = (Button)grdSaleBillDetails.HeaderRow.FindControl("btnAddLineItem");
        LineItemCalculation(RowIndex);
        btnAddLineItem.Focus();
    }
    protected void txtCustomer_TextChanged(object sender, EventArgs e)
    {
        AppSaleBill SaleBill = (AppSaleBill)Session[TableConstants.SaleBillSession];

        ACEtxtSaleOrder.ContextKey = hdfCustomerID.Value;
        SaleBill.Customer_Company_ID = new AppConvert(hdfCustomerID.Value);
        txtTermsAndConditions.Text = SaleBill.DefaultTermsAndConditionsStatement;
        hdfTermsAndConditionsName.Value = SaleBill.DefaultTermsAndConditionsStatement;
        hdfTermsAndConditionsID.Value = new AppConvert(SaleBill.DefaultTermsAndConditionsID);
        hdfDeliveryAddressID.Value = new AppConvert(SaleBill.CustomerAddressID);
        hdfDeliveryAddressName.Value = new AppConvert(SaleBill.CustomerAddress);
        txtDeliveryAddress.Text = new AppConvert(SaleBill.CustomerAddress);
        Session.Add(TableConstants.SaleBillSession, SaleBill);
        BindData();
        txtCustomer.Focus();

    }
    #endregion
    #region Grid View Events
    protected void grdSaleBillDetails_PreRender(object sender, EventArgs e)
    {
        if (grdSaleBillDetails.Rows.Count > 0)
        {
            grdSaleBillDetails.UseAccessibleHeader = true;
            grdSaleBillDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdSaleBillDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
            AppSaleBill SaleBill = (AppSaleBill)Session[TableConstants.SaleBillSession];
            SaleBill.SaleBillItemColl.Where(x => x.Status == 0).ToList()[row.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
            Session.Add(TableConstants.SaleBillSession, SaleBill);
            BindData();
            SetValueToGridFooter();
        }
    }
    #endregion      
}