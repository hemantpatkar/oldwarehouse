﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to display all Customer lists.
*/
#endregion
#region Using
using System;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.UI;
using AppUtility;
#endregion
public partial class CustomerSummary : BigSunPage
{
    AppCompanyColl objCustomer;
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
         
        if (!IsPostBack)
        {
            BindData();
        }
    }
    #endregion
    #region BindData   
    public void BindData()
    {
        AppCompanyColl objCollection = new AppCompanyColl(intRequestingUserID);
        RecordStatus Status = CommonFunctions.GetStatus(ddlStatus.SelectedValue);
        objCollection.Clear();
        if (!string.IsNullOrEmpty(txtCustomerCode.Text))
        {
            objCollection.AddCriteria(AppObjects.Company.Code, AppUtility.Operators.Like, txtCustomerCode.Text);
        }

        if (!string.IsNullOrEmpty(txtCustomerName.Text))
        {
            objCollection.AddCriteria(AppObjects.Company.Name, AppUtility.Operators.Like, txtCustomerName.Text);
        }

        if (ddlStatus.SelectedValue != "-32768")
        {
            objCollection.AddCriteria(AppObjects.Company.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objCollection.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Company.Type, AppUtility.Operators.Equals, (int)CompanyRoleType.Customer, 1);
        objCollection.Search(RecordStatus.ALL);
        Session.Add(TableConstants.CustomerCollSession, objCollection);
        grdCustomerSummary.DataSource = objCollection;
        grdCustomerSummary.DataBind();
        
        if (objCollection.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Records Found','2'" + ");", true);
        }
    }
    #endregion
    #region GridView Events
    protected void grdCustomerSummary_PreRender(object sender, EventArgs e)
    {
        if (grdCustomerSummary.Rows.Count > 0)
        {
            grdCustomerSummary.UseAccessibleHeader = true;
            grdCustomerSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdCustomerSummary_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        objCustomer = (AppCompanyColl)Session[TableConstants.CustomerCollSession];
        objCustomer[e.RowIndex].Status = new AppUtility.AppConvert((int)RecordStatus.Deleted);
        objCustomer[e.RowIndex].Save();
        Session.Add(TableConstants.CustomerCollSession, objCustomer);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Customer Deleted Successfully','1'" + ");", true);
        BindData();
    }
    #endregion   
    #region Button Click Events
    protected void lnkNewCustomer_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.CustomerSessionObj);
        Response.Redirect("CustomerDetails.aspx");
    }
    protected void Search_Click(object sender, EventArgs e)
    {
        BindData();
    }
    #endregion
    protected void grdCustomerSummary_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string CommandName = e.CommandName;
        int commandArgument = new AppUtility.AppConvert(e.CommandArgument);
        if(CommandName=="APPROVE")
        {
            objCustomer = (AppCompanyColl)Session[TableConstants.CustomerCollSession];
            objCustomer[commandArgument].Status = new AppUtility.AppConvert((int)RecordStatus.Approve);
            objCustomer[commandArgument].Save();
            Session.Add(TableConstants.CustomerCollSession, objCustomer);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Customer Approved Successfully','1'" + ");", true);
        }
    }
    protected void grdCustomerSummary_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        objCustomer = (AppCompanyColl)Session[TableConstants.CustomerCollSession];
        if(objCustomer.Count>0 && objCustomer!=null)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (objCustomer[e.Row.RowIndex].Status == new AppConvert((int)RecordStatus.Deleted))
                {
                    LinkButton lbtnApprove = (LinkButton)e.Row.FindControl("lbtnApprove");
                    lbtnApprove.Enabled = false;
                    lbtnApprove.ToolTip = "Already Deleted";
                    LinkButton lbtnDelete = (LinkButton)e.Row.FindControl("lbtnDelete");
                    lbtnDelete.Enabled = false;
                    lbtnDelete.ToolTip = "Already Deleted";
                }
                else if (objCustomer[e.Row.RowIndex].Status == new AppUtility.AppConvert((int)RecordStatus.Approve))
                {
                    LinkButton lbtnApprove = (LinkButton)e.Row.FindControl("lbtnApprove");
                    lbtnApprove.Enabled = false;
                    lbtnApprove.ToolTip = "Already Approved";
                }
            }
        }
    }

    protected void grdCustomerSummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        
        grdCustomerSummary.PageIndex = e.NewPageIndex;
        BindData();
    }
}