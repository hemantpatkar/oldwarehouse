﻿<%@ Page Title="Pre Inward" Language="C#" MasterPageFile="~/Base.master" Culture="en-gb" AutoEventWireup="true" Inherits="StockPreInDetail" Codebehind="StockPreInDetail.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">

        function validateForm() {
            var errmsg = '';
            var ItemQty = document.getElementById('<%=txtItemQuantity.ClientID%>').value;
            var cvitm = document.getElementById('<%=cvitemqty.ClientID%>').valuetocompare;

            if (ItemQty == null || ItemQty == "") {
                errmsg += "<br />You must enter 'Quantity'";
            } else {
                if (isNaN(ItemQty)) {
                    errmsg += "<br />'Please enter proper quantity";
                }
            }

            var Item = document.getElementById('<%=UsrhdfItem.ClientID%>').value;
            if (Item == null || Item == "") {
                errmsg += "<br />You must Select an 'Item'";
            } else {
                if (isNaN(Item)) {
                    errmsg += "<br />'Please Select Proper Item";
                }
            }

            ShowAnimatedMessage(errmsg, '2');
            if (errmsg == '') {
                return true;
            } else {
                return false;
            }
        }


        function GateRegister(sender, eventArgs) {
            var hdSchID = $get('<%= hdfGateRegis.ClientID %>');
            var Schcode = $get('<%= txtGateRegis.ClientID %>');
            var txtcode = $get('<%= txtVehName.ClientID %>');
            var lblSchname = $get('<%= txtVehicalNo.ClientID %>');
            var lblChalan = $get('<%= txtChallanNo.ClientID %>');
            var detail = eventArgs.get_value().split('~');
            hdSchID.value = detail[0];
            Schcode.value = eventArgs.get_text();
            txtcode.value = eventArgs.get_text();
            txtcode.innerHTML = eventArgs.get_text();
            lblSchname.value = detail[1];
            lblSchname.innerHTML = detail[1];
            lblChalan.value = detail[2];
            lblChalan.innerHTML = detail[2];
        }


        function ClearGateReg() {
            var hdpartID = $get('<%= hdfGateRegis.ClientID %>');
            var PatyGroup = $get('<%=txtGateRegis.ClientID %>');
            var party = $get('<%=txtVehicalNo.ClientID %>');
            var txtcode = $get('<%= txtVehName.ClientID %>');
            if ((PatyGroup.value != txtcode.value) || PatyGroup.value == "") {
                $get('<%=txtGateRegis.ClientID %>').value = "";
                $get('<%=txtVehicalNo.ClientID %>').value = "";
                $get('<%=hdfGateRegis.ClientID %>').value = "";
                $get('<%=txtVehName.ClientID %>').value = "";
            }
        }


        function ItemSearch(sender, eventArgs) {

            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());

            var hdschID = $get('<%= hdfStorageSchemeRateID.ClientID %>');
            var lblschname = $get('<%= hdfStorageSchemeRateName.ClientID %>');
            var schcode = $get('<%= txtStorageSchemeRate.ClientID %>');
            hdschID.value = "";
            lblschname.innerHTML = "";
            schcode.value = "";
            var hdItemID = $get('<%= UsrhdfItem.ClientID %>');
            var lblItemname = $get('<%= UsrlblItem.ClientID %>');
            var Itemcode = $get('<%= UsrtxtItemN.ClientID %>');
            var today = new Date();
            var dd = ((today.getDate()) < 10 ? "0" : "") + (today.getDate());
            var mm = ((today.getMonth() + 1) < 10 ? "0" : "") + (today.getMonth() + 1);
            var yy = today.getFullYear();
            var yymmdd = yy + '' + mm + '' + dd;
            hdItemID.value = eventArgs.get_value();
            if (hdItemID.value != "") {
                Itemcode.value = "";
                Itemcode.value = eventArgs.get_text();
                lblItemname.innerHTML = eventArgs.get_text();
                lblItemname.value = eventArgs.get_text();
                lblItemname.textContent = eventArgs.get_text();
                $find('<%=ACEtxtStorageSchemeRate.ClientID %>').set_contextKey(eventArgs.get_value() + ',' + yymmdd);
                $find('<%=ACEtxtVariety.ClientID %>').set_contextKey(eventArgs.get_value());
                $find('<%=ACEtxtBrand.ClientID %>').set_contextKey(eventArgs.get_value());
                $find('<%=ACEtxtOrigin.ClientID %>').set_contextKey(eventArgs.get_value());
            }
        }

        function ClearItem() {
            var hdpartID = $get('<%= UsrhdfItem.ClientID %>');
            var PatyGroup = $get('<%=UsrtxtItemN.ClientID %>');
            var party = $get('<%=UsrlblItem.ClientID %>');
            if ((PatyGroup.value != party.value) || PatyGroup.value == "") {
                $get('<%=UsrtxtItemN.ClientID %>').value = "";
                $get('<%=UsrlblItem.ClientID %>').value = "";
                $get('<%=UsrhdfItem.ClientID %>').value = "";
            }
        }

        function PartnerSearch(sender, eventArgs) {

            var hdpartID = $get('<%= hdfCustomerID.ClientID %>');
            var hdfCustomerName = $get('<%= hdfCustomerName.ClientID %>');
            var txtCustomer = $get('<%= txtCustomer.ClientID %>');
            hdpartID.value = eventArgs.get_value();

            if (hdpartID.value != "") {
                hdfCustomerName.value = eventArgs.get_text();
                txtCustomer.value = eventArgs.get_text();
                $find('<%=UsrtxtPartnerGroupCode_AutoCompleteExtender.ClientID %>').set_contextKey("4" + eventArgs.get_value());

            }
        }

        function ClearPartner() {
            var hdpartID = $get('<%= hdfCustomerID.ClientID %>');
            var PatyGroup = $get('<%=hdfCustomerName.ClientID %>');
            var party = $get('<%=txtCustomer.ClientID %>');

            if ((PatyGroup.value != party.value) || PatyGroup.value == "") {
                $get('<%=hdfCustomerID.ClientID %>').value = "";
                $get('<%=hdfCustomerName.ClientID %>').value = "";
                $get('<%=txtCustomer.ClientID %>').value = "";
                $find('<%=UsrtxtPartnerGroupCode_AutoCompleteExtender.ClientID %>').set_contextKey("4");
            }
        }


        var i = 1;
        function clickButton(e, buttonid) {

            var evt = (e || window.event);
            var keyCode = (evt.keyCode || evt.which);
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (keyCode == 13) {
                    if (bt == "ctl00_ContentPlaceHolder1_btnDetailSave") {
                        validateForm();
                    }
                    bt.click();
                    return false;
                }
            }
        }

        function clickItem(e, buttonid) {
            var evt = (e || window.event);
            var keyCode = (evt.keyCode || evt.which);
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (keyCode == 40) {
                    bt.click();
                    return false;
                }
            }
        }

        function LockSave() {
            document.getElementById('<%=btnSave.ClientID%>').style.display = 'none';
        }


        function confirmalert(ask) {
            return confirm(ask);
        }

        function PartnerGroupSearch(sender, eventArgs) {

            var hdpartID = $get('<%= UsrhdfPartnerGroup.ClientID %>');
            var lblPartname = $get('<%= UsrlblPartnerGroup.ClientID %>');
            var partycode = $get('<%= UsrtxtPartnerGroupCode.ClientID %>');

            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=ACEtxtCustomer.ClientID %>').set_contextKey("3," + eventArgs.get_value());
            }
        }


        function ClearPartnerGroupSearch() {
            var OrignalProfitCenter = $get('<%=UsrtxtPartnerGroupCode.ClientID %>');
            var HidenProfitCenter = $get('<%= UsrlblPartnerGroup.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=UsrhdfPartnerGroup.ClientID %>').value = "";
                $get('<%=UsrlblPartnerGroup.ClientID %>').value = "";
                $get('<%=UsrtxtPartnerGroupCode.ClientID %>').value = "";
                $find('<%=ACEtxtCustomer.ClientID %>').set_contextKey("3");
            }
        }

        function grdLocationSearch(sender, eventArgs) {
            var GridRowID = (sender.valueOf('id')._id).replace("txtLocation_AutoCompleteExtender", "");
            var hdfID = GridRowID + 'hdfStorageLocationIDInitial';
            var Orignal = GridRowID + 'txtLocation';
            var Hiden = GridRowID + 'hideStorageLocationNameInitial';
            document.getElementById(hdfID).value = eventArgs.get_value();
            if (hdfID.value != "") {
                document.getElementById(Orignal).value = eventArgs.get_text();
                document.getElementById(Hiden).value = eventArgs.get_text();
            }
            else {

                document.getElementById(Orignal).value = "";
                document.getElementById(Hiden).value = "";
            }
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
        <ProgressTemplate>
            <div class="modal fade in" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-keyboard="false" style="display: block">
                <div class="modal-dialog  modal-sm">
                    <h1 style="text-align: center;">
                        <i class="fa fa-truck fa-magic fa-2x faa-passing animated"></i>
                    </h1>
                </div>
            </div>
            <div class="modal-backdrop fade in"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <i class="fa fa-truck"></i>
                        <asp:Label ID="lblInward" runat="server"></asp:Label>
                        &nbsp;/&nbsp;
                  <asp:Label ID="lblInwardNoValue" ForeColor="Maroon" runat="server"></asp:Label>
                        <div class="pull-right">
                            <asp:LinkButton ID="btnDeleteInwardDetail" runat="server" CausesValidation="False" Visible="false"
                                TabIndex="1" OnClick="btnDeleteInwardDetail_Click"><i class="fa fa-trash text-danger"></i>
                            </asp:LinkButton>
                            <a onclick="ShowHelp()" class="anchor"><i class="fa fa-question"></i></a>
                            <asp:ConfirmButtonExtender ID="btnDeleteInwardDetail_ConfirmButtonExtender" runat="server"
                                ConfirmText="Do you want to permanently delete?" Enabled="True" TargetControlID="btnDeleteInwardDetail">
                            </asp:ConfirmButtonExtender>
                            <asp:LinkButton ID="btnhistory" runat="server" CausesValidation="False" Visible="false" TabIndex="1" OnClick="btnhistory_Click"><i class="fa fa-history"></i> 
                            </asp:LinkButton>
                            <asp:HyperLink ID="btnPrint" runat="server" Target="_blank" Visible="false"
                                TabIndex="1" NavigateUrl=""><i class="fa fa-print"></i> </asp:HyperLink>
                            <asp:LinkButton ID="btnBack" runat="server" OnClick="btnBack_Click" CausesValidation="False"
                                TabIndex="1"><i class="fa fa-search"></i>
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row btnrow">
                <div class="col-lg-6">
                    <asp:HiddenField ID="hdfapproved" runat="server" />
                    <asp:HiddenField ID="hdfrevised" runat="server" />
                    <ul class="nav navbar-nav navbar-left">
                        <li></li>
                        <li>
                            <asp:LinkButton ValidationGroup="Inward" ID="btnSave" runat="server" AccessKey="s" OnClick="btnSave_Click"
                                TabIndex="1">
                            </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ValidationGroup="Inward" ID="btnApprove" Text="Approve" Visible="false" runat="server" AccessKey="a" OnClick="btnApprove_Click"
                                TabIndex="1">
                            </asp:LinkButton></li>
                        <li>
                            <asp:LinkButton ID="btnNew" runat="server" AccessKey="n" OnClick="btnNew_Click" CausesValidation="False"
                                TabIndex="1">
                            </asp:LinkButton>
                        </li>
                        <li>
                            <asp:LinkButton ID="btnAddItem" runat="server" Text="Add Item" AccessKey="i" OnClick="btnAddItem_Click" CausesValidation="False"
                                TabIndex="1">
                            </asp:LinkButton>
                        </li>

                    </ul>
                </div>
                <div class="col-lg-6">
                    <div class="pull-right">
                        <ul class="nav nav-pills nav-wizard">
                            <li class="active"><a href="#" data-toggle="tab">New</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Pending Approval</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Available</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Partially Available</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Done</a></li>

                        </ul>
                    </div>
                </div>

            </div>
            <hr />
            <div class="panel">
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-2">

                            <asp:Label ID="lblGateRegisterID" runat="server" CssClass="bold"></asp:Label>
                            <asp:Label ID="Label1" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtGateRegis"
                                Display="Dynamic" ErrorMessage="" CssClass="text-danger" SetFocusOnError="true"
                                ValidationGroup="Inward">
                            </asp:RequiredFieldValidator>
                            <div class="input-group">
                                <asp:TextBox ID="txtGateRegis" runat="server" CssClass="form-control input-sm" TabIndex="1" onblur="ClearGateReg()" ValidationGroup="InwardDetail"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                            </div>
                            <asp:AutoCompleteExtender ID="txtGateRegis_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="15" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="GateRegister" ServiceMethod="GateEntrySearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtGateRegis">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:TextBox ID="txtVehName" CssClass="form-control input-sm" runat="server"></asp:TextBox>
                                <asp:TextBox ID="hdfGateRegis" CssClass="form-control input-sm" runat="server"></asp:TextBox>
                            </div>

                        </div>
                        <div class="col-lg-2">

                            <asp:Label ID="lblVehical" runat="server" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtVehicalNo" TabIndex="1" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                            </div>
                        </div>

                        <div class="col-lg-2">
                            <div style="display: none;">
                            </div>
                            <asp:Label ID="lblInwardDate" runat="server" CssClass="bold"></asp:Label>
                            <asp:Label ID="Label6" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtInwardDate" runat="server" CssClass="form-control input-sm" TabIndex="1"
                                    ValidationGroup="Inward">
                                </asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="txtInwardDate_MaskedEditExtender" runat="server" Enabled="True"
                                Mask="99/99/9999" MaskType="Date" TargetControlID="txtInwardDate">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="caltxtInwardDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtInwardDate"
                                TargetControlID="txtInwardDate">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RFVFromdate0" runat="server" ControlToValidate="txtInwardDate"
                                Display="Dynamic" ErrorMessage="" CssClass="text-danger" SetFocusOnError="true"
                                ValidationGroup="Inward">
                            </asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToCompare="txtDateCompare"
                                ControlToValidate="txtInwardDate" Display="Dynamic" ErrorMessage="" SetFocusOnError="true"
                                Type="Date" ValidationGroup="Inward">
                            </asp:CompareValidator>
                            <asp:RangeValidator ID="Rangeval" runat="server" Type="Date" ErrorMessage=""
                                CssClass="text-danger" SetFocusOnError="true"
                                Display="Dynamic" ControlToValidate="txtInwardDate" ValidationGroup="Inward">
                            </asp:RangeValidator>
                            <asp:RangeValidator ID="Rangback" runat="server" Type="Date" ErrorMessage=""
                                CssClass="text-danger"
                                Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtInwardDate" ValidationGroup="Inward">
                            </asp:RangeValidator>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPartnerGroupCode" runat="server" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="UsrtxtPartnerGroupCode" onblur="ClearPartnerGroupSearch()" TabIndex="1" runat="server" CssClass="form-control input-sm"></asp:TextBox>

                                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                            </div>
                            <asp:AutoCompleteExtender ID="UsrtxtPartnerGroupCode_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerGroupSearch"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" ContextKey="4" UseContextKey="true"
                                TargetControlID="UsrtxtPartnerGroupCode">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:HiddenField ID="UsrhdfPartnerGroup" runat="server" />
                                <asp:Label ID="UsrlblPartnerGroup" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPartnerCode" CssClass="bold" runat="server"></asp:Label>
                            <asp:Label ID="Label4" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtCustomer" CssClass="form-control input-sm" TabIndex="1" runat="server" onblur="ClearPartner()"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="ACEtxtCustomer" runat="server"
                                    CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                    CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                    CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                    FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerSearch"
                                    ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                    UseContextKey="true" ContextKey="3" TargetControlID="txtCustomer">
                                </asp:AutoCompleteExtender>
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div>
                            <asp:HiddenField ID="hdfCustomerID" runat="server" />
                            <asp:HiddenField ID="hdfCustomerName" runat="server" />
                            <asp:RequiredFieldValidator ID="RFVFromdate1" runat="server" ControlToValidate="txtCustomer"
                                Display="Dynamic" ErrorMessage="" CssClass="text-danger" SetFocusOnError="true"
                                ValidationGroup="Inward">
                            </asp:RequiredFieldValidator>
                        </div>

                        <div class="col-lg-2">
                            <asp:Label ID="lblReceivedBy" runat="server" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtReceivedBy" CssClass="form-control input-sm" runat="server" TabIndex="1" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtReceivedBy" runat="server"
                                CompletionInterval="1" CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListItemCssClass="AutoExtenderList"
                                CompletionSetCount="15" ContextKey="0" DelimiterCharacters="" EnableCaching="false"
                                Enabled="True" FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch"
                                ServiceMethod="CompanyUserSearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtReceivedBy">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfReceivedByID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdfReceivedByName" runat="server"></asp:HiddenField>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblReceivedDate" runat="server" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtReceivedDate" runat="server" CssClass="form-control input-sm" TabIndex="1" ValidationGroup="Inward">
                                </asp:TextBox>
                                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" AutoComplete="False" ClearMaskOnLostFocus="true"
                                    ClearTextOnInvalid="True" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtReceivedDate"
                                    UserDateFormat="DayMonthYear">
                                </asp:MaskedEditExtender>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtReceivedDate"
                                    TargetControlID="txtReceivedDate">
                                </asp:CalendarExtender>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-2" id="divChallanNo" runat="server">
                            <asp:Label ID="lblChallanNo" runat="server" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtChallanNo" ValidationGroup="Inward" runat="server" CssClass="form-control input-sm" TabIndex="1"></asp:TextBox>
                        </div>
                        <div class="col-lg-8">
                            <asp:Label ID="lblRemarks" runat="server" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control input-sm" MaxLength="500" TabIndex="1"></asp:TextBox>
                        </div>


                        <div style="display: none">


                            <asp:HiddenField ID="hdfInward" runat="server" />

                            <asp:TextBox ID="txtDateCompare" CssClass="form-control input-sm" runat="server"></asp:TextBox>

                        </div>

                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-lg-12" style="overflow: auto">
                    <asp:GridView ID="grdInward" runat="server" AutoGenerateColumns="False" OnRowCommand="grdInward_RowCommand"
                        ShowFooter="True" CssClass="table table-hover table-striped table-Full text-nowrap">
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDeleteItem" CommandName="Del" Visible='<%# Convert.ToInt32(Eval("Status"))>1?false:true %>' ForeColor="Maroon" runat="server" CausesValidation="False" TabIndex="1">
                                        <i class="fa fa-close fa-2x"></i>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnSplitRow" runat="server" Visible='<%# Convert.ToInt32(Eval("ID"))>0?false:true %>' OnClientClick='<% #"return showmore(" + Container.DataItemIndex + ")" %>'>
                                        <i class="fa fa-tasks fa-2x"></i>
                                    </asp:LinkButton>
                                    <div class="panel panel-info" id='<%#Container.DataItemIndex %>' style="height: 100px; width: 200px; position: absolute; display: none">
                                        <div class="panel-heading element">
                                            <h4>Slipt Row </h4>
                                            <hr />
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <asp:TextBox ID="txtNo" runat="server" CssClass="form-control input-sm" MaxLength="2" TabIndex="1" ValidationGroup="Inward"></asp:TextBox>
                                                    <asp:FilteredTextBoxExtender ID="txtNo_FilteredTextBoxExtender" runat="server"
                                                        Enabled="True" FilterType="Numbers" TargetControlID="txtNo">
                                                    </asp:FilteredTextBoxExtender>
                                                    <asp:RequiredFieldValidator ID="rfvNo" Display="Dynamic" runat="server" ControlToValidate="txtNo"
                                                        CssClass="text-danger" ErrorMessage="" ValidationGroup='<%#"Nos"+Container.DataItemIndex %>'></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <br />
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <asp:LinkButton ID="btnSplit" CommandName="Split" runat="server" ValidationGroup='<%#"Nos"+Container.DataItemIndex %>' CssClass="btn btn-primary btn-sm btn-block"
                                                        TabIndex="1" Text="Split"></asp:LinkButton>
                                                </div>
                                                <div class="col-lg-6">
                                                    <a type="button" onclick='<%#"return showmore(" + Container.DataItemIndex + ")" %>' class="btn btn-default btn-sm btn-block">Cancle</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblSrNo" Text="No." runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" Text='<%#Container.DataItemIndex + 1 %>' runat="server" />
                                    <asp:Label ID="lblRowIndex" Text='<%#Container.DataItemIndex %>' Visible="false" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblLotNo" Text="Lot No" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblLotNoInternal" Text='<%# Eval("LotInternal") %>' ToolTip='<%# Eval("LotInternal") %>' runat="server" />
                                    <asp:Label ID="lblLotVersionWise" Text='<%# Eval("LotVersion") %>' ToolTip='<%# Eval("LotVersion") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblItem" Text="Item" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblInwardDetID" Visible="false" Text='<%#Eval("ID") %>' runat="server" />
                                    <asp:LinkButton ID="lnkSelect" Text='<%#Eval("Item.Name") %>' Font-Bold="true" ToolTip='<%#Eval("Item.Name") %>'
                                        runat="server" CommandName="Sel" CausesValidation="false" ForeColor="Maroon" CommandArgument='<%#Eval("Item_ID") %>' TabIndex="18" />
                                    <br />
                                    <asp:Label ID="Label3" Text='<%#Eval("Item.Sales_TaxType.Name") %>' ForeColor="#3399ff" Font-Bold="true" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPackRef" Text="Packing Ref" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPackingReference" Text='<%# Eval("PackingReference") %>' ToolTip='<%# Eval("PackingReference") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblItemMark" Text="Item Mark" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblItemMark" Text='<%# Eval("ItemMark") %>' ToolTip='<%# Eval("ItemMark") %>'
                                        runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblVakkal" Text="Vakkal" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblLotNoCustomer" Text='<%# Eval("LotCustomer") %>' ToolTip='<%# Eval("LotCustomer") %>'
                                        runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdVariety" Text="Variety/Count" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblVariety" Text='<%# Eval("Variety.Name") %>' ToolTip='<%# Eval("Variety.Name") %>' runat="server" />
                                    <br />
                                    <asp:Label ID="lblCount" Text='<%# Eval("Count") %>' ToolTip='<%# Eval("Count") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblApprovedQty" Text="Quantity" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtApprovedQuantity" Width="70px" runat="server" CssClass="form-control input-sm" Style="text-align: right"
                                        Text='<%# Eval("Quantity") %>' ToolTip='<%# Eval("Quantity") %>'
                                        ValidationGroup="Inward" CausesValidation="true" TabIndex="18" MaxLength="10" />
                                    <asp:RequiredFieldValidator ID="rfvApprovedQuantity" SetFocusOnError="true" Display="Dynamic"
                                        ValidationGroup="Inward" runat="server" ErrorMessage="" ControlToValidate="txtApprovedQuantity"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CPVWeighQty" SetFocusOnError="true" Display="Dynamic" runat="server"
                                        ErrorMessage="Incorect Qty" ValidationGroup="Inward" ControlToValidate="txtApprovedQuantity"
                                        Type="Double" ValueToCompare='<%# Eval("Quantity") %>' Operator="LessThanEqual"></asp:CompareValidator>
                                    <asp:FilteredTextBoxExtender ID="FTApprovedQuantity" TargetControlID="txtApprovedQuantity"
                                        FilterType="Numbers,Custom" ValidChars="." runat="server">
                                    </asp:FilteredTextBoxExtender>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lbltotal" runat="server"></asp:Label>
                                </FooterTemplate>
                                <ItemStyle HorizontalAlign="Right" />
                                <FooterStyle HorizontalAlign="Right" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblLocation" Text="Location" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtLocation" runat="server" Width="100px" CssClass="form-control input-sm" Text='<%# Eval("StorageLocation.Name") %>' TabIndex="0"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="txtLocation_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                        CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                        DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                        MinimumPrefixLength="1" OnClientItemSelected="grdLocationSearch" ServiceMethod="StorageLocationSearch"
                                        ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                        TargetControlID="txtLocation">
                                    </asp:AutoCompleteExtender>
                                    <div style="display: none">
                                        <asp:TextBox ID="hideStorageLocationNameInitial" Text='<%# Eval("StorageLocation.Name") %>' runat="server" />
                                        <asp:HiddenField ID="hdfStorageLocationIDInitial" Value='<%# Eval("StorageLocation_ID") %>' runat="server" />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblScheme" Text="Scheme" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblSchemeName" Text='<%# Eval("StorageSchemeRate.StorageScheme.Code") %>' ToolTip='<%# Eval("StorageSchemeRate.StorageScheme.Code") %>' runat="server" />
                                    <asp:Label ID="lblStorageItemSchemeDetailID" Visible="false" Text='<%# Eval("StorageSchemeRate_ID") %>' runat="server" /><br />
                                    <asp:Label ID="lblSchemeRate" Text='<%# Eval("StorageSchemeRate.Rate") %>' ForeColor="Maroon" Font-Bold="true" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdBrand" Text="Brand/Origin" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblBrand" Text='<%# Eval("Brand.Name") %>' ToolTip='<%# Eval("Brand.Name") %>' runat="server" />

                                    <br />
                                    <asp:Label ID="lblorigin" Text='<%# Eval("Origin.Name") %>' ToolTip='<%# Eval("Origin.Name") %>' runat="server" />

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPalletNo" Text="Pallet/Container No" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPalletNo" Text='<%# Eval("PalletNo") %>' ToolTip='<%# Eval("PalletNo") %>'
                                        runat="server" /><br />
                                    <asp:Label ID="lblContainerNo" Text='<%# Eval("ContainerNo") %>' ToolTip='<%# Eval("ContainerNo") %>' runat="server" />
                                    <asp:Label ID="lblExpiryDate" Visible="false" Text='<%# Eval("ExpiryDate") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <br />
            <div class="panel panel-default" id="pnladditem" style="display: none" runat="server">
                <div class="panel-body">
                    <div style="display: none;">
                        <asp:TextBox ID="UsrlblItem" runat="server" CssClass="bold"></asp:TextBox>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="heading">
                                Item Details
                            </div>
                        </div>
                        <div class="col-lg-9">
                        </div>
                        <div class="col-lg-1">
                            <asp:LinkButton ID="btnDetailSave" CssClass="btn btn-success btn-sm" Text="Add" runat="server" CausesValidation="true" OnClick="btnDetailSave_Click" TabIndex="1" ValidationGroup="InwardDetail"></asp:LinkButton>
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label ID="lblLotNoInternal" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtLotNoInternal" runat="server" CssClass="form-control input-sm" MaxLength="20" TabIndex="1" />

                            <asp:FilteredTextBoxExtender ID="txtLotNoInternal_FilteredTextBoxExtender" FilterType="Numbers"
                                runat="server" Enabled="True" TargetControlID="txtLotNoInternal">
                            </asp:FilteredTextBoxExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblItem" runat="server" CssClass="bold" />
                            <asp:Label ID="Label7" runat="server" Text="*" CssClass="text-danger" />
                            <asp:TextBox ID="UsrtxtItemN" runat="server" CssClass="form-control input-sm" TabIndex="1" onblur="ClearItem()"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="UsrtxtItemN_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="15" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="ItemSearch" ServiceMethod="ItemSearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="UsrtxtItemN">
                            </asp:AutoCompleteExtender>
                            <asp:RequiredFieldValidator ID="reqitem" Display="Dynamic" runat="server"
                                ControlToValidate="UsrtxtItemN" CssClass="text-danger" ErrorMessage="" SetFocusOnError="true"
                                ValidationGroup="InwardDetail"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblItemMark" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtItemMark" runat="server" CssClass="form-control input-sm" MaxLength="15"
                                TabIndex="1" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPackingRefer" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtPackingReference" runat="server" CssClass="form-control input-sm" MaxLength="20"
                                TabIndex="1" />
                        </div>
                        <div class="col-lg-2" id="dtscheme" runat="server">
                            <asp:Label ID="lblSchemeID" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtStorageSchemeRate" CssClass="form-control input-sm" runat="server" TabIndex="1" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtStorageSchemeRate" runat="server" CompletionInterval="1"
                                CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionSetCount="10"
                                ContextKey="0" DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch" ServiceMethod="SchemeSearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtStorageSchemeRate" UseContextKey="true">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfStorageSchemeRateID" runat="server" />
                            <asp:HiddenField ID="hdfStorageSchemeRateName" runat="server"></asp:HiddenField>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblItemQuantity" runat="server" CssClass="bold" />
                            <asp:Label ID="Label10" runat="server" Text="*" CssClass="text-danger" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server"
                                ControlToValidate="txtItemQuantity" CssClass="text-danger" ErrorMessage="" SetFocusOnError="true"
                                ValidationGroup="InwardDetail"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtItemQuantity" runat="server" CssClass="form-control input-sm" MaxLength="10"
                                TabIndex="1" />
                            <asp:CompareValidator ID="cvitemqty" ValidationGroup="InwardDetail" runat="server"
                                ControlToValidate="txtItemQuantity" Display="Dynamic" ErrorMessage="" Operator="GreaterThanEqual"
                                Type="Double" ValueToCompare="0"></asp:CompareValidator>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" TargetControlID="txtItemQuantity"
                                FilterType="Numbers,Custom" ValidChars="." runat="server">
                            </asp:FilteredTextBoxExtender>

                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblStorageLocationIDInitial" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtStorageLocation" CssClass="form-control input-sm" runat="server" TabIndex="1" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtStorageLocation" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="10" ContextKey="0" DelimiterCharacters="" EnableCaching="false"
                                Enabled="True" FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch"
                                ServiceMethod="StorageLocationSearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtStorageLocation"
                                UseContextKey="true">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfStorageLocationName" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdfStorageLocationID" runat="server"></asp:HiddenField>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblorigin" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtOrigin" runat="server" CssClass="form-control input-sm" TabIndex="1" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtOrigin" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch" ServiceMethod="OriginSearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtOrigin">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfOriginID" runat="server" />
                            <asp:HiddenField ID="hdfOriginName" runat="server"></asp:HiddenField>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblBrand" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control input-sm" TabIndex="1" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtBrand" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch" ServiceMethod="BrandSearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtBrand">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfBrandID" runat="server" />
                            <asp:HiddenField ID="hdfBrandName" runat="server"></asp:HiddenField>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblVariety" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtVariety" runat="server" CssClass="form-control input-sm" TabIndex="1" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtVariety" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch" ServiceMethod="VarietySearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtVariety">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfVarietyID" runat="server" />
                            <asp:HiddenField ID="hdfVarietyName" runat="server"></asp:HiddenField>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblcount" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtcount" runat="server" CssClass="form-control input-sm" MaxLength="50" TabIndex="1" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblLotNoCustomer" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtLotNoCustomer" runat="server" CssClass="form-control input-sm" MaxLength="20"
                                TabIndex="1" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblContainerNo" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtContainerNo" runat="server" CssClass="form-control input-sm" MaxLength="50"
                                TabIndex="1" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPalletNo" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtPalletNo" runat="server" CssClass="form-control input-sm" MaxLength="50"
                                TabIndex="1" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblExpiryYearMonth" runat="server" CssClass="bold" />
                            <asp:TextBox ID="UsrtxtYearMonth" CssClass="form-control input-sm" runat="server" TabIndex="1"></asp:TextBox>
                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="True"
                                Mask="99/99/9999" MaskType="Date" TargetControlID="UsrtxtYearMonth">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="UsrtxtYearMonth"
                                TargetControlID="UsrtxtYearMonth">
                            </asp:CalendarExtender>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:HiddenField ID="UsrhdfItem" runat="server" />
                            <asp:HiddenField ID="hdfInwdDetailRowIndex" runat="server" />
                            <asp:HiddenField ID="hdfInwardDetail" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="Bottom" runat="server" style="display: none">
        <div class="row">
            <div class="col-lg-3">
            </div>
            <div class="col-lg-3">

                <asp:Label ID="lbleduser" runat="server" />
                <asp:Label ID="lbledbar" runat="server" Text=" |"></asp:Label>
                <asp:Label ID="lbleddate" runat="server" />

            </div>
            <div class="col-lg-3">
            </div>
            <div class="col-lg-3">
                <asp:Label ID="lblshowshotcut" runat="server" Text="Shortcuts" class="text-danger" />
            </div>
        </div>
    </div>

    <%--ALL Modal--%>
    <div class="modal fade" id="modalhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                <div class="modal-header">
                    <h1 class="modal-title">
                        <asp:Label ID="lbledit" runat="server"></asp:Label>
                    </h1>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>

                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:Label ID="lblcron" runat="server" CssClass="bold"></asp:Label>
                                    <asp:Label ID="lblcruser" runat="server" />
                                    <asp:Label ID="lblcrbar" runat="server" Text=" | "></asp:Label>
                                    <asp:Label ID="lblcrdate" runat="server" />
                                </div>
                            </div>
                            <div class="newline"></div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:Label ID="lbldelon" runat="server" CssClass="bold"></asp:Label>
                                    <asp:Label ID="lbldluser" runat="server" />
                                    <asp:Label ID="lbldelbar" runat="server" Text=" | " CssClass="bold"></asp:Label>
                                    <asp:Label ID="lbldldate" runat="server" />
                                </div>
                            </div>
                            <div class="newline"></div>
                            <div class="row">
                                <div class="col-lg-12" style="text-align: center">
                                    <asp:Label ID="lbledon" runat="server" CssClass="bold"></asp:Label>
                                </div>
                            </div>
                            <div class="newline"></div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:HiddenField ID="hdfhistory" runat="server" />
                                    <asp:GridView ID="grdedit" runat="server" AutoGenerateColumns="false" OnPreRender="grdedit_OnPreRender"
                                        CssClass="table table-hover table-Full text-nowrap">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblUser" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbleduser" Text='<%# Eval("UserInfo.UserCode") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblUserFullName" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUserFullName" Text='<%# Eval("UserInfo.UserFullName") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblDate" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbleddate" Text='<%# Eval("CreateDate") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnhistory" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.div modalhistory Modal -->

    <sidebar id="Favourite" style="display: none;">

        <div class="heading">
                Help
             <div class="pull-right">
            
        <a onclick="ShowHelp()" class="anchor"><i class="fa fa-close text-danger"></i></a>


            </div>
            </div>

        <hr />
                <asp:Label ID="Lblsh" runat="server" Text="Alt+S : Save"></asp:Label>                 
               <br />                   
                <asp:Label ID="lblal" runat="server"  Text="Alt+N : cursor goes directly to lot no textbox"></asp:Label>
    </sidebar>

    <script type="text/javascript">

        function showmore(x) {
            var sec = x;
            if (document.getElementById(sec).style.display == '')
                document.getElementById(sec).style.display = 'none';
            else
                document.getElementById(sec).style.display = ''
            return false
        }
        function ShowHelp() {
            $("sidebar").toggle("slide");
        }
    </script>

</asp:Content>

