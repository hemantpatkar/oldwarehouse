﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;

public partial class TermsAndConditions : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Terms & Conditions";
        if (!IsPostBack)
        {
            //BindTnCGroup();
            divAddTnCgroup.Visible = false;
        }
    }

    protected void btnAddTnC_Click(object sender, EventArgs e)
    {
        AppTermsAndConditions objTermsAndConditions = new AppTermsAndConditions(1);
        if (txtTnC.Text.Trim() != "" && txtTnCGroup.Text.Trim() != "")
        {
            AppTermsAndConditionsColl objTermsAndConditionsColl = new AppTermsAndConditionsColl(1);
            objTermsAndConditionsColl.Search();
            objTermsAndConditions.Statement = txtTnC.Text;
            objTermsAndConditions.Parent_ID = new AppConvert(hdfTnCGroupID.Value);
            objTermsAndConditions.ModifiedOn = DateTime.Now;
            objTermsAndConditions.Status = new AppConvert((int)RecordStatus.Active);
            objTermsAndConditions.Save();
        }
        else
        {
            return;
        }

        txtTnC.Text = "";
        txtTnCGroup.Text = "";
    }

    protected void btnAddTnCGroup_Click(object sender, EventArgs e)
    {
        divAddTnCgroup.Visible = true;
        btnClose.Visible = true;
        btnAddTnCGroup.Visible = false;
        grdTnc.DataSource = null;
        grdTnc.DataBind();
    }



    public void BindgrdTnC(int Parent_ID)
    {
        AppTermsAndConditionsColl objTermsAndConditionsColl = new AppTermsAndConditionsColl(1);
        objTermsAndConditionsColl.Search();
        grdTnc.DataSource = objTermsAndConditionsColl;
        grdTnc.DataBind();

    }

    //protected void ddlTnCGroup_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    int Parent_ID = new AppConvert(ddlTnCGroup.SelectedValue);
    //    BindgrdTnC(Parent_ID);
    //    divAddTnCgroup.Visible = false;
    //}

    protected void grdTnc_PreRender(object sender, EventArgs e)
    {
        if (grdTnc.Rows.Count > 0)
        {
            grdTnc.UseAccessibleHeader = true;
            grdTnc.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

    protected void txtTnCGroup_TextChanged(object sender, EventArgs e)
    {
        int Parent_ID = new AppConvert(hdfTnCGroupID.Value);
        BindgrdTnC(Parent_ID);
        divAddTnCgroup.Visible = false;
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        int Parent_ID = new AppConvert(hdfTnCGroupID.Value);
        divAddTnCgroup.Visible = false;
        btnAddTnCGroup.Visible = true;
        btnClose.Visible = false;
        if (txtTnCGroup.Text.Trim() != "")
        {
            BindgrdTnC(Parent_ID);
            divAddTnCgroup.Visible = false;
        }
    }
}