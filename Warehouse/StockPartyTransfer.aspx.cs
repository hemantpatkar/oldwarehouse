﻿using AppObjects;
using AppUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class StockPartyTransfer : BigSunPage
{
    #region Variables
    public static int lotid = 0;
    public int f = 0;
    int total = 0;
    public int temp;
    public int flag;
    public int old;
    public int alertflag = 0;
    public string UnitCode = "";
    private int oldPTID = 0;
    private DateTime oldINDate;
    List<int> InwardDetail_IDs;
    AppTransferStockCustomer objAppTransferStockCustomer;
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        DecideLanguage(Convert.ToString(Session["LanguageCode"]));
        Page.Title = "Party Transfer";
        objAppTransferStockCustomer = new AppTransferStockCustomer(intRequestingUserID);
        if (!IsPostBack)
        {
            Session.Add(TableConstants.TransferStockCustomerSession, objAppTransferStockCustomer);
            if (new AppConvert(Request.QueryString["ID"]) != "" && Request.QueryString["ID"] != null)
            {
                hdfPartyDetailID.Value = Request.QueryString["ID"];
                int id = Convert.ToInt32(Request.QueryString["ID"]);
                objAppTransferStockCustomer.ID = id;
                PopulateData(id);
            }
            else
            {
                hdfrevised.Value = "";
                txtLotSearch.Focus();
                searchlot.Attributes.Add("style", "display:");
            }
        }
    }
    #endregion

    #region Button Event
    protected void btnDeleteInwardDetail_Click(object sender, EventArgs e)
    {

    }
    protected void btnhistory_Click(object sender, EventArgs e)
    {

    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockPartyTransferSearch.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        #region Validation  Started Date and Ended Date
        if (txtTansferPartyStartedOn.Text.Trim() != "" && txtTansferPartyEndedOn.Text.Trim() != "" && txtTansferPartyStartedOn.Text.Trim() != "__/__/____" && txtTansferPartyEndedOn.Text.Trim() != "__/__/____")
        {
            DateTime Startedon, Endedon;
            Startedon = Convert.ToDateTime(txtTansferPartyStartedOn.Text);
            Endedon = Convert.ToDateTime(txtTansferPartyEndedOn.Text);
            if (Startedon > Endedon)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid Inward Date" + "','2'" + ");", true);
                return;
            }
            #region Code for Validating  Started On and Ended On Hour and Minute
            if (Startedon == Endedon)
            {
                int Shh = 0, Smm = 0, Ehh = 0, Emm = 0;
                Shh = Convert.ToInt32(txtTansferStartedOnHH.Text.Trim());
                Smm = Convert.ToInt32(txtTansferStartedOnMM.Text.Trim());
                Ehh = Convert.ToInt32(txtEndedOnHR.Text.Trim());
                Emm = Convert.ToInt32(txtEndedOnMM.Text.Trim());
                if (Shh > Ehh)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid start time and end time" + "','2'" + ");", true);
                    return;
                }

                if (Shh == Ehh)
                {
                    if (Smm > Emm)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid start time and end time" + "','2'" + ");", true);
                        return;
                    }
                }
            }
            #endregion
        }
        #endregion

        #region Save
        objAppTransferStockCustomer = (AppTransferStockCustomer)Session[TableConstants.TransferStockCustomerSession];

        if (objAppTransferStockCustomer.ID == 0)
        {
            objAppTransferStockCustomer.TransferNo = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "Invoice", System.DateTime.Now, 4, ResetCycle.Yearly));
        }
        objAppTransferStockCustomer.FromCustomer_Company_ID = new AppConvert(hdfPartnerID.Value);
        objAppTransferStockCustomer.ToCustomer_Company_ID = new AppConvert(hdfToPartnerID.Value);
        objAppTransferStockCustomer.TransferDate = new AppConvert(txtTransferDate.Text);
        objAppTransferStockCustomer.ReceivedBy_CompanyContact_ID = new AppConvert(hdfReceivedByID.Value);
        objAppTransferStockCustomer.ReceivedDate = new AppConvert(txtRecvdDate.Text);
        objAppTransferStockCustomer.Remarks = txtRemarks.Text;
        objAppTransferStockCustomer.OrderBy = txtOrderBy.Text;
        objAppTransferStockCustomer.Status = new AppConvert((int)RecordStatus.Active);
        objAppTransferStockCustomer.Type = objAppTransferStockCustomer.Type;
        objAppTransferStockCustomer.ModifiedBy = intRequestingUserID;
        objAppTransferStockCustomer.ModifiedOn = System.DateTime.Now;
        if (txtTansferPartyStartedOn.Text.Trim() != "" && txtTansferPartyStartedOn.Text.Trim() != "__/__/____")
        {
            DateTime startedon = Convert.ToDateTime(txtTansferPartyStartedOn.Text + " " + txtTansferStartedOnHH.Text + ":" + txtTansferStartedOnMM.Text);
            objAppTransferStockCustomer.TransferStartedOn = startedon;
        }
        if (txtTansferPartyEndedOn.Text.Trim() != "" && txtTansferPartyEndedOn.Text.Trim() != "__/__/____")
        {
            DateTime endedon = Convert.ToDateTime(txtTansferPartyEndedOn.Text + " " + txtEndedOnHR.Text + ":" + txtEndedOnMM.Text);
            objAppTransferStockCustomer.TransferEndedOn = endedon;
        }
        objAppTransferStockCustomer.Save();
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Saved Successfully','1'" + ");", true);
        #endregion
    }
    protected void btnNewDetail_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockPartyTransfer.aspx");
    }
    #endregion

    #region Function
    private void DecideLanguage(string languageCode)  // Method used to fill Screen Language on lables & Buttons text property
    {
        try
        {
            lblPartyTransferTitle.Text = "Party Transfer";
            btnSave.Text = "Save";
            btnDeleteItem.Text = "Delete" + " " + "Item";
            btnNewDetail.Text = "New";
            btnLotSearch.Text = "Search";
            btnAddItems.Text = "Add";
            lblchkZero.Text = "Zero" + " " + "";
            lblchkFyear.Text = "Previous" + " " + "" + " " + "Year";
            btnDelete.Text = "Delete";
        }
        catch (Exception)
        {

        }
    }
    public void SearchOptimised()
    {
        int pID = 0;

        #region Get Financial Year
        //int YID = Convert.ToInt32(Session["Finyear"]);
        //DateTime startdate = Convert.ToDateTime(GenericUtills.convertIntToDate(db.mFinancialYears.FirstOrDefault(x => x.FYearID == YID).StartDate));
        //DateTime enddate = Convert.ToDateTime(GenericUtills.convertIntToDate(db.mFinancialYears.FirstOrDefault(x => x.FYearID == YID).EndDate));
        #endregion

        #region declare variables
        int PartnerGroup = Convert.ToInt32("0" + UsrhdfPartnerGroup.Value);
        int Customer = Convert.ToInt32("0" + UsrhdfPartner.Value);
        int LotNoInt = new AppConvert(txtLotSearch.Text.Trim());
        string Vakkal = new AppConvert(txtVakkalSearch.Text.Trim());
        string packingref = new AppConvert(txtpackingrefSearch.Text);
        string Mark = new AppConvert(txtMarkSearch.Text);
        string ItemName = new AppConvert(txtStorageItemName.Text);
        string SchemeName = new AppConvert(txtSchemeSearch.Text);
        string itemgroup = new AppConvert(txtItemGroup.Text);
        decimal InwardQty = new AppConvert(txtReceivedQuantitySearch.Text);
        int UTID = Convert.ToInt32(Session["UnitID"]);
        #endregion

        #region ZeroStock and Fyear Check
        int Mono = Convert.ToInt32("0" + hdfmono.Value);
        if (chkZero.Checked == true && chkFyear.Checked == true)
        {
            //if (txtScheme.Text == "" && txtLotNo.Text == "" && txtItemGroup.Text == "" && txtStorageItemName.Text == "" && txtMarkSearch.Text == "" && txtpackingrefSearch.Text == "" && txtVakkalSearch.Text == "" && txtReceivedQuantitySearch.Text == "" && PartnerGroup == 0 && Customer == 0 && Mono == 0)
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Please select atlease one parameter for searching" + "','2'" + ");", true);
            //    return;
            //}
        }
        #endregion

        AppStockSearchVWColl objAppStockSearchVWColl = new AppStockSearchVWColl(intRequestingUserID);


        //if (db.AppSettings.Any(a => a.AppSettingID == 54001 && a.AppSettingValue == "Enable"))
        //{
        //    if (hdfMOID.Value != "" && hdfMOID.Value != null && hdfMOID.Value != "0")
        //    {
        //        if (gvDescription.Rows.Count > 1)
        //        {
        //            HiddenField hdfSchemeID = (HiddenField)gvDescription.Rows[0].FindControl("hdfSchemeID");
        //            int sche = Convert.ToInt32(hdfSchemeID.Value);
        //            int schemecat = db.Schemes.FirstOrDefault(x => x.SchemeID == sche).SchemeCategoryID;
        //            List<int> currentsches = getschemes(schemecat);

        //            searcheddata = from sd in searcheddata where currentsches.Contains(sd.SchemeID) select sd;
        //        }
        //    }
        //}
        //if (gvSearch.Rows.Count > 0)
        //{
        //    //if (partnergr == 1)
        //    if (UsrhdfPartnerGroup.Value != "0" && UsrhdfPartnerGroup.Value != "" && UsrhdfPartnerGroup.Value != null)
        //    {
        //        int partnergrp = Convert.ToInt32(UsrhdfPartnerGroup.Value);
        //        searcheddata = from sd in searcheddata where sd.PartnerGroupID == partnergrp orderby sd.LotNoInternal ascending, sd.NetQuantity descending select sd;
        //    }
        //    HiddenField hdfPartnerID = (HiddenField)gvSearch.Rows[gvSearch.Rows.Count - 1].FindControl("hdfPartnerID");
        //    pID = Convert.ToInt32(hdfPartnerID.Value);
        //}
        //else
        //{
        //    pID = 0;
        //}

        //if (UsrhdfPartnerGroup.Value != "0" && UsrhdfPartnerGroup.Value != "" && UsrhdfPartnerGroup.Value != null)
        //{
        //    int partnergrp = Convert.ToInt32(UsrhdfPartnerGroup.Value);
        //    searcheddata = from sd in searcheddata where sd.PartnerGroupID == partnergrp orderby sd.LotNoInternal ascending, sd.NetQuantity descending select sd;
        //    //partnergr = 1;
        //    flag = 1;
        //    UsrlblPartnerGroup.Text = db.PartnerGroups.FirstOrDefault(a => a.PartnerGroupID == partnergrp).PartnerGroupName;
        //    UsrtxtPartnerGroupCode.Text = db.PartnerGroups.FirstOrDefault(a => a.PartnerGroupID == partnergrp).PartnerGroupName;
        //}
        //else
        //{
        //    // partnergr = 0;
        //}
        //if (UsrhdfPartner.Value != "0" && UsrhdfPartner.Value != "" && UsrhdfPartner.Value != null)
        //{
        //    int partner = Convert.ToInt32(UsrhdfPartner.Value);
        //    searcheddata = from sd in searcheddata where sd.PartnerID == partner orderby sd.LotNoInternal ascending, sd.NetQuantity descending select sd;
        //    //partnerid = 1;
        //    flag = 1;
        //    UsrlblPartner.Text = db.Partners.FirstOrDefault(x => x.PartnerID == partner).PartnerName;
        //    UsrtxtPartnerCode.Text = db.Partners.FirstOrDefault(x => x.PartnerID == partner).PartnerName;
        //}

        //if (txtItemGroup.Text.Trim() != "")
        //{
        //    string[] itemgroup = txtItemGroup.Text.ToUpper().Split('-');
        //    string cat = itemgroup[0];
        //    searcheddata = from sd in searcheddata where sd.StorageItemCategoryCode.ToUpper().StartsWith(cat.ToUpper()) orderby sd.LotNoInternal ascending, sd.NetQuantity descending select sd;
        //    flag = 1;
        //}

        //if (chkZero.Checked == false)
        //{
        //    searcheddata = from sd in searcheddata where sd.BillingQuantity > 0 orderby sd.LotNoInternal ascending, sd.NetQuantity descending select sd;
        //}
        //if (chkFyear.Checked == false)
        //{
        //    searcheddata = from sd in searcheddata where (sd.BillingQuantity == 0 && sd.CreatedOn >= startdate && sd.CreatedOn <= enddate) || sd.BillingQuantity > 0 orderby sd.LotNoInternal ascending, sd.NetQuantity descending select sd;
        //}

        InwardDetail_IDs = (List<int>)Session[TableConstants.InwardDetailIDSession];
        objAppStockSearchVWColl.Search(0, Customer, 0, 0, InwardQty, LotNoInt, Vakkal, Mark, packingref, ItemName, InwardDetail_IDs, new AppConvert(chkZero.Checked));

        if (txtLotSearch.Text.ToString() != "")
        {
            if (objAppStockSearchVWColl.Count() <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Lot No Not Present" + "','2'" + ");", true);
                txtLotSearch.Focus();
                return;
            }

        }
        gvSearch.DataSource = objAppStockSearchVWColl;
        gvSearch.DataBind();

    }
    public List<Int32> GetInwardDetailIds()
    {

        List<Int32> InwardIDs = new List<Int32>();
        if (gvDescription.Rows.Count > 0)
        {
            for (int rowindex = 0; rowindex < gvDescription.Rows.Count; rowindex++)
            {
                HiddenField hdfInwardDetailID = (HiddenField)gvDescription.Rows[rowindex].FindControl("hdfInwardDetailID");
                if (new AppConvert(hdfInwardDetailID.Value) != "")
                {
                    InwardIDs.Add(Convert.ToInt32(hdfInwardDetailID.Value));
                }
            }
        }
        return InwardIDs;
    }
    public void TotalQuantityCount()
    {

        decimal count = 0;
        for (int rowindex = 0; rowindex < gvDescription.Rows.Count; rowindex++)
        {
            TextBox txtOrderQuantity = (TextBox)gvDescription.Rows[rowindex].FindControl("txtOrderQuantity");
            if (txtOrderQuantity.Text.Trim() != "" && txtOrderQuantity.Text != null)
            {
                count = count + Convert.ToDecimal(txtOrderQuantity.Text);
            }
            txtOrderQuantity.Focus();
        }
        //lblTotalQuantityToShow.Text = new AppConvert(count);
    }
    public void PopulateData(int ID)
    {
        objAppTransferStockCustomer = new AppObjects.AppTransferStockCustomer(intRequestingUserID, ID);

        txtTSPNo.Text = new AppConvert(objAppTransferStockCustomer.TransferNo);

        ACEtxtPartner.ContextKey = new AppConvert("3," + objAppTransferStockCustomer.FromCustomer_Company.Parent_ID);
        hdfPartnerGroupCodeID.Value = new AppConvert(objAppTransferStockCustomer.FromCustomer_Company.Parent_ID);
        txtPartnerGroupCode.Text = objAppTransferStockCustomer.FromCustomer_Company.Parent.Code + "--" + objAppTransferStockCustomer.FromCustomer_Company.Parent.Name;
        hdfPartnerGroupCodeName.Value = txtPartnerGroupCode.Text;

        txtPartner.Text = new AppConvert(objAppTransferStockCustomer.FromCustomer_Company.Code + "--" + objAppTransferStockCustomer.FromCustomer_Company.Name);
        hdfPartnerName.Value = txtPartner.Text;
        hdfPartnerID.Value = new AppConvert(objAppTransferStockCustomer.FromCustomer_Company_ID);

        ACEtxtToPartner.ContextKey = new AppConvert("3," + objAppTransferStockCustomer.ToCustomer_Company.Parent_ID);
        hdfPartnerGroupCodeToID.Value = new AppConvert(objAppTransferStockCustomer.ToCustomer_Company.Parent_ID);
        txtPartnerGroupCodeTo.Text = objAppTransferStockCustomer.ToCustomer_Company.Parent.Code + "--" + objAppTransferStockCustomer.ToCustomer_Company.Parent.Name;
        hdfPartnerGroupCodeToName.Value = txtPartnerGroupCodeTo.Text;

        txtToPartner.Text = new AppConvert(objAppTransferStockCustomer.ToCustomer_Company.Name);
        hdfToPartnerName.Value = txtToPartner.Text;
        hdfToPartnerID.Value = new AppConvert(objAppTransferStockCustomer.ToCustomer_Company_ID);

        txtOrderBy.Text = new AppConvert(objAppTransferStockCustomer.OrderBy);
        txtReceivedBy.Text = new AppConvert(objAppTransferStockCustomer.ReceivedBy_CompanyContact.FirstName);
        hdfReceivedByName.Value = txtReceivedBy.Text;
        hdfReceivedByID.Value = new AppConvert(objAppTransferStockCustomer.ReceivedBy_CompanyContact_ID);
        txtTransferDate.Text = new AppConvert(objAppTransferStockCustomer.TransferDate);
        txtRecvdDate.Text = new AppConvert(objAppTransferStockCustomer.ReceivedDate);
        txtTansferPartyStartedOn.Text = new AppConvert(objAppTransferStockCustomer.TransferStartedOn);
        txtTansferStartedOnHH.Text = new AppConvert(objAppTransferStockCustomer.TransferStartedOn.Hour);
        txtTansferStartedOnMM.Text = new AppConvert(objAppTransferStockCustomer.TransferStartedOn.Minute);
        txtTansferPartyEndedOn.Text = new AppConvert(objAppTransferStockCustomer.TransferEndedOn);
        txtEndedOnHR.Text = new AppConvert(objAppTransferStockCustomer.TransferEndedOn.Hour);
        txtEndedOnMM.Text = new AppConvert(objAppTransferStockCustomer.TransferEndedOn.Minute);
        txtRemarks.Text = objAppTransferStockCustomer.Remarks;

        gvDescription.DataSource = objAppTransferStockCustomer.TransferStockCustomerDetailColl.Where(x => x.Status == 1);
        gvDescription.DataBind();

        btnDeletePartyTransfer.Visible = true;
    }
    public void DeletePTDetail(int PartyTrasferID)
    {
        AppTransferStockCustomerDetail objAppTransferStockCustomerDetail = new AppTransferStockCustomerDetail(intRequestingUserID, PartyTrasferID);
        objAppTransferStockCustomerDetail.Delete();

    }
    #endregion

    #region GridView
    protected void gvDescription_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Del")
        {
            try
            {
                int selectedindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
                HiddenField hdfPartyTrasferID = (HiddenField)gvDescription.Rows[selectedindex].FindControl("hdfPartyTrasferID");
                if (hdfPartyTrasferID.Value != "0" && hdfPartyTrasferID.Value != "")
                {
                    int PartyTrasfer_ID = Convert.ToInt32(hdfPartyTrasferID.Value);

                    #region Outward Validation
                    int MaterialOrder_ID = Convert.ToInt32("0" + hdfPartyID.Value);
                    AppOutwardDetailColl objAppOutwardDetailColl = new AppOutwardDetailColl(intRequestingUserID);
                    objAppOutwardDetailColl.AddCriteria(OutwardDetail.MaterialOrderDetail_ID, Operators.Equals, PartyTrasfer_ID);
                    objAppOutwardDetailColl.AddCriteria(OutwardDetail.Status, Operators.Equals, (int)RecordStatus.Active);
                    objAppOutwardDetailColl.Search();

                    if (objAppOutwardDetailColl.Count() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Outward has been made. Please delete the outward first" + "','2'" + ");", true);
                        return;
                    }
                    #endregion

                    DeletePTDetail(PartyTrasfer_ID);
                    //InsertTransactionLog(21, MODID, 4, intRequestingUserID);
                }

                #region SMS Log 
                if (!string.IsNullOrEmpty(hdfPartyID.Value))
                {
                    //int moid = Convert.ToInt32(hdfPartyID.Value);
                    //var MoSMS = db.MaterialOrders.FirstOrDefault(a => a.MaterialOrderID == moid);
                    ////if (!db.SMSTransLogs.Any(a => a.UnitID == MoSMS.UnitID && a.TransRefID == MoSMS.MaterialOrderID && a.SMSTemplateID == 5))
                    ////{
                    //string strOB = "";
                    //if (MoSMS.OrderedBy != null)
                    //{
                    //    strOB = db.ParterContacts.FirstOrDefault(a => a.PartnerContID == MoSMS.OrderedBy).FirstName + " " + db.ParterContacts.FirstOrDefault(a => a.PartnerContID == MoSMS.OrderedBy).LastName;
                    //}
                    //else
                    //{
                    //    strOB = MoSMS.OrderName;
                    //}

                    //InsertSMSLog(6, MoSMS.MaterialOrderID, MoSMS.UnitID, MoSMS.PartnerID, TMDqty, "", MoSMS.MaterialOrderNo, MoSMS.DeliverTo, strOB); //Convert.ToInt32("0" + iwd.Totalqty)
                    // }
                }
                #endregion

                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Deleted Sucessfully" + "','2'" + ");", true);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Foreign"))
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "This Detail Is Used For Outward" + "','2'" + ");", true);
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + ex.Message + "','2'" + ");", true);
            }

            //gvDescription.DataSource = bindsearch(dt);
            //gvDescription.DataBind();

        }
    }

    protected void gvSearch_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //CheckBox CheckBoxSelect = (CheckBox)e.Row.FindControl("CheckBoxSelect");

            //HtmlGenericControl CheckBox_Select = (HtmlGenericControl)e.Row.FindControl("CheckBox_Select");
            //LinkButton lnkAdditem = (LinkButton)e.Row.FindControl("lnkAdditem");
            //CheckBoxSelect.Attributes.Add("onkeypress", "return clickButton(event,'" + lnkAdditem.ClientID + "')");

        }
    }


    protected void gvSearch_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName == "Sel")
        //{
        //    //new changes for quantity date 04/07/2014

        //    int rowindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
        //    List<string> InwardIDs = new List<string>();
        //    HiddenField hdfInwardDetailID = (HiddenField)gvSearch.Rows[rowindex].FindControl("hdfInwardDetailID");
        //    HiddenField hdfInwardDetailQtyID = (HiddenField)gvSearch.Rows[rowindex].FindControl("hdfInwardDetailQtyID");
        //    //InwardIDs.Add(hdfInwardDetailID.Value + "," + rowindex);
        //    InwardIDs.Add(hdfInwardDetailQtyID.Value + "," + rowindex);

        //    #region datevalidation

        //    //int id = Convert.ToInt32(hdfInwardDetailID.Value);
        //    Int64 id = Convert.ToInt64("0" + hdfInwardDetailQtyID.Value);
        //    int date = compare(id);
        //    old = Convert.ToInt32("0" + ViewState["oldDate"]);
        //    if (old < date)
        //    {
        //        //txtDateCompare.Text = GenericUtills.ConvertIntToDate(date);
        //        ViewState["oldDate"] = date;
        //    }

        //    #endregion

        //    ViewState["InwardIds"] = InwardIDs;
        //    ShowPartyDescription(InwardIDs);
        //    TotalQuantityCount();
        //    btnovrOk.Visible = true;
        //    //btnovrOk.Focus();
        //}

        //if (e.CommandName == "Detail")
        //{
        //    int selectedindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
        //    int InwardNo = Convert.ToInt32(e.CommandArgument);
        //    BindInwardItemDetail(InwardNo);
        //    BindOutwards(InwardNo);
        //    BindMO(InwardNo);
        //    BindLT(InwardNo);
        //    //string sb = GenericUtills.setfocus(mpInwardDetail.BehaviorID, btnCancelDetailPopup.ClientID);
        //    //Page.ClientScript.RegisterStartupScript(Page.GetType(), "Startup", sb.ToString());
        //    //btnCancelDetailPopup.Focus();
        //    //mpInwardDetail.Show();
        //    //GenericUtills.popup_Show("modalInwardDetail", "", this);
        //    focus = 4;
        //    rowsid.Value = "" + selectedindex;
        //}

        //#region outstanding popup

        //if (e.CommandName == "Customer")
        //{
        //    int selindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
        //    Int32 partnerid = Convert.ToInt32(e.CommandArgument);
        //    //mploverride.Show();
        //    //GenericUtills.popup_Show("modalOverride", "", this);
        //    //Outstanding.Filloutstanding(partnerid);
        //    //LabourOutstanding.Filloutstanding(partnerid);
        //    btnovrOk.Visible = false;
        //    //btnovrCancel.Focus();
        //}

        //#endregion
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSearch.PageIndex = e.NewPageIndex;
        btnLotSearch_Click(sender, e);
    }
    #endregion

    protected void btnLotSearch_Click(object sender, EventArgs e)
    {
        SearchOptimised();
        if (gvDescription.Rows.Count > 0)
        {
            TotalQuantityCount();
        }
        if (gvSearch.Rows.Count > 0)
        {
            TextBox txtOrderQtySearch = (TextBox)gvSearch.Rows[0].FindControl("txtOrderQtySearch");
            txtOrderQtySearch.Focus();
        }
        else
        {
            UsrtxtPartnerGroupCode.Focus();
        }
        if (UsrhdfPartnerGroup.Value != "0" && UsrhdfPartnerGroup.Value != "")
        {
            //int partnergrp = Convert.ToInt32(UsrhdfPartnerGroup.Value);
            //UsrlblPartnerGroup.Text = db.PartnerGroups.FirstOrDefault(a => a.PartnerGroupID == partnergrp).PartnerGroupName;
            //UsrtxtPartnerGroupCode.Text = db.PartnerGroups.FirstOrDefault(a => a.PartnerGroupID == partnergrp).PartnerGroupName;
        }
        if (UsrhdfPartner.Value != "0" && UsrhdfPartner.Value != "")
        {
            //int partner = Convert.ToInt32(UsrhdfPartner.Value);
            //UsrlblPartner.Text = db.Partners.FirstOrDefault(x => x.PartnerID == partner).PartnerName;
            //UsrtxtPartnerCode.Text = db.Partners.FirstOrDefault(x => x.PartnerID == partner).PartnerName;
        }
    }

    protected void btnAddItems_Click(object sender, EventArgs e)
    {
        objAppTransferStockCustomer = (AppTransferStockCustomer)Session[TableConstants.TransferStockCustomerSession];

        #region To Get IDs of Inward ID from Search Grid
        int ChkFlg = 0;
        //List<string> InwardDetail_IDs = new List<string>();
        //List<Int64> InwardDetailIDs = new List<Int64>();
        //List<Int64> IDs = new List<Int64>();

        string[] check = Convert.ToString(Request.Form["CheckBoxSelect"]).Split(',');

        for (int i = 0; i < check.Length; i++)
        {
            int InwardDetailID = Convert.ToInt32(check[i]);
            AppInwardDetail objAppInwardDetail = new AppInwardDetail(intRequestingUserID, InwardDetailID);

            #region TransferStockCustomerFrom
            TextBox txtOrderQtySearch = (TextBox)gvSearch.Rows[i].FindControl("txtOrderQtySearch");

            AppTransferStockCustomerDetail objAppTransferStockCustomerDetail = new AppTransferStockCustomerDetail(intRequestingUserID);

            if (txtOrderQtySearch.Text != "" && txtOrderQtySearch.Text != null)
                objAppTransferStockCustomerDetail.Quantity = Convert.ToInt32(txtOrderQtySearch.Text);

            objAppTransferStockCustomerDetail.Item_ID = objAppInwardDetail.Item_ID;
            objAppTransferStockCustomerDetail.InwardDetail_ID = objAppInwardDetail.ID;
            objAppTransferStockCustomerDetail.VoucherDetailID = 0;
            objAppTransferStockCustomerDetail.StorageLocation_ID = objAppInwardDetail.StorageLocation_ID;
            objAppTransferStockCustomerDetail.Status = new AppConvert((int)RecordStatus.Active);
            objAppTransferStockCustomerDetail.Type = 1;
            objAppTransferStockCustomerDetail.ModifiedBy = intRequestingUserID;
            objAppTransferStockCustomerDetail.ModifiedOn = System.DateTime.Now;
            objAppTransferStockCustomerDetail.StorageSchemeRate_ID = objAppInwardDetail.StorageSchemeRate_ID;
            objAppTransferStockCustomerDetail.LotInternal = objAppInwardDetail.LotInternal;
            objAppTransferStockCustomerDetail.LotVersion = objAppInwardDetail.LotVersion;
            objAppTransferStockCustomerDetail.LotCustomer = objAppInwardDetail.LotCustomer;
            objAppTransferStockCustomerDetail.Billable = objAppInwardDetail.Billable;
            objAppTransferStockCustomerDetail.TaxType_ID = objAppInwardDetail.TaxType_ID;

            objAppTransferStockCustomer.TransferStockCustomerDetailColl.Add(objAppTransferStockCustomerDetail);//Add

            #endregion
                      
            #region datevalidation
            //Int64 id = Convert.ToInt64("0" + hdfInwardDetailID.Value);
            //int date = compare(id);
            //old = Convert.ToInt32("0" + ViewState["oldDate"]);
            //if (old < date)
            //{
            //    txtDateCompare.Text = GenericUtills.ConvertIntToDate(date);
            //    ViewState["oldDate"] = date;
            //}

            #endregion

        }


        #endregion

        Session.Add(TableConstants.TransferStockCustomerSession, objAppTransferStockCustomer);

        gvDescription.DataSource = objAppTransferStockCustomer.TransferStockCustomerDetailColl;
        gvDescription.DataBind();

        gvSearch.DataSource = null;
        gvSearch.DataBind();

        TotalQuantityCount();
         

    }

    protected void btnDeleteItem_Click(object sender, EventArgs e)
    {

    }

    protected void btnDeletePartyTransfer_Click(object sender, EventArgs e)
    {
        if (hdfPartyDetailID.Value != "" && hdfPartyDetailID.Value != null)
        {
            int PartyID = Convert.ToInt32(hdfPartyDetailID.Value);
            AppTransferStockCustomer objAppTransferStockCustomer = new AppTransferStockCustomer(intRequestingUserID, PartyID);
            objAppTransferStockCustomer.Delete();
        }
    }
}