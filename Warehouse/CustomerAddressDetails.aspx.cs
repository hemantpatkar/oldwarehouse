﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class CustomerAddressDetails : BigSunPage
{
    AppCustomer obj;
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = (AppCustomer)Session[TableConstants.CustomerSessionObj];
        if (!IsPostBack)
        {
           BindData();
        }
    }  
    protected void grdCustomerAddress_PreRender(object sender, EventArgs e)
    {
        if (grdCustomerAddress.Rows.Count > 0)
        {
            grdCustomerAddress.UseAccessibleHeader = true;
            grdCustomerAddress.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdCustomerAddress_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = "";
        if (strCommandName == "INSERT")
        {
            TextBox Address1 = grdCustomerAddress.FooterRow.FindControl("txtNewAddress") as TextBox;
            TextBox Area = grdCustomerAddress.FooterRow.FindControl("txtNewVendorArea") as TextBox;
            HiddenField hdfCountryID = grdCustomerAddress.FooterRow.FindControl("hdfNewCountryId") as HiddenField;
            HiddenField hdfZipCodeID = grdCustomerAddress.FooterRow.FindControl("hdfNewZipCodeID") as HiddenField;
            CheckBox IsActiveAddress = grdCustomerAddress.FooterRow.FindControl("chkIsVendorActive") as CheckBox;
            CheckBox IsDefaultAddress = grdCustomerAddress.FooterRow.FindControl("chkIsVendorDefault") as CheckBox;
            SaveAddress(strCommandID, Address1.Text, Area.Text, hdfCountryID.Value, hdfZipCodeID.Value, IsActiveAddress.Checked, IsDefaultAddress.Checked);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Address Added Successfully','1'" + ");", true);
        }
        else if (strCommandName == "CANCELADD")
        {
            grdCustomerAddress.ShowFooter = false;
            BindData();
        }
    }
    protected void grdCustomerAddress_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        obj = (AppCustomer)Session[TableConstants.CustomerSessionObj];
        obj.CompanyAddressColl[e.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
        Session[TableConstants.CustomerSessionObj] = obj;
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Address Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void lbtnAddNew_Click(object sender, EventArgs e)
    {
        BindData(true);
        grdCustomerAddress.ShowFooter = true;
    }
    protected void grdCustomerAddress_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grdCustomerAddress.EditIndex = -1;
        BindData();
    }
    protected void grdCustomerAddress_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdCustomerAddress.ShowFooter = false;
        grdCustomerAddress.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdCustomerAddress_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string Index = new AppConvert(e.RowIndex);
        TextBox Address1 = grdCustomerAddress.Rows[e.RowIndex].FindControl("txtAddress") as TextBox;
        TextBox Area = grdCustomerAddress.Rows[e.RowIndex].FindControl("txtVendorArea") as TextBox;
        HiddenField hdfCountryID = grdCustomerAddress.Rows[e.RowIndex].FindControl("hdCountryID") as HiddenField;
        HiddenField hdfZipCodeID = grdCustomerAddress.Rows[e.RowIndex].FindControl("hdfVendorPincodeID") as HiddenField;
        CheckBox IsActiveAddress = grdCustomerAddress.Rows[e.RowIndex].FindControl("chkActive") as CheckBox;
        CheckBox IsDefaultAddress = grdCustomerAddress.Rows[e.RowIndex].FindControl("chkDefault") as CheckBox;
        SaveAddress(Index, Address1.Text, Area.Text, hdfCountryID.Value, hdfZipCodeID.Value, IsActiveAddress.Checked, IsDefaultAddress.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Address Updated Successfully','1'" + ");", true);        
        grdCustomerAddress.EditIndex = -1;
        BindData();
    }
    protected void txtNewVendorPinCode_TextChanged(object sender, EventArgs e)
    {
        GridViewRow gvr = (GridViewRow)((TextBox)sender).Parent.Parent;
        TextBox City = ((TextBox)(grdCustomerAddress.FooterRow.FindControl("txtNewCity")));
        TextBox State = ((TextBox)(grdCustomerAddress.FooterRow.FindControl("txtNewState")));
        TextBox Country = ((TextBox)(grdCustomerAddress.FooterRow.FindControl("txtNewCountry")));
        HiddenField CountryID = ((HiddenField)(grdCustomerAddress.FooterRow.FindControl("hdfNewCountryId")));
        HiddenField ZipCodeID = ((HiddenField)(grdCustomerAddress.FooterRow.FindControl("hdfNewZipCodeID")));
        TextBox txtPinCode = ((TextBox)(grdCustomerAddress.FooterRow.FindControl("txtNewVendorPinCode")));
        string PinCode = txtPinCode.Text;
        AppZipCodeTypeColl appZCTColl = new AppZipCodeTypeColl(intRequestingUserID);
        appZCTColl.Search(PinCode);
        if (appZCTColl.Count > 0)
        {
            AppZipCodeType objZip = appZCTColl[0];
            City.Text = objZip.CountryType.City;
            State.Text = objZip.CountryType.State;
            Country.Text = objZip.CountryType.Country;
            CountryID.Value = new AppUtility.AppConvert(objZip.CountryType.ID);
            ZipCodeID.Value = new AppUtility.AppConvert(objZip.CountryType.ID);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Pincode Found','2'" + ");", true);
            City.Text = "";
            State.Text = "";
            Country.Text = "";
            CountryID.Value = "";
            ZipCodeID.Value = "";
            txtPinCode.Focus();
        }
    }
    protected void txtVendorPinCode_TextChanged(object sender, EventArgs e)
    {
        GridViewRow gvr = (GridViewRow)((TextBox)sender).Parent.Parent;
        int RowIndex = new AppConvert(gvr.RowIndex);
        TextBox City = (TextBox)(grdCustomerAddress.Rows[RowIndex].FindControl("txtCity"));
        TextBox State = (TextBox)(grdCustomerAddress.Rows[RowIndex].FindControl("txtState"));
        TextBox Country = (TextBox)(grdCustomerAddress.Rows[RowIndex].FindControl("txtCountry"));
        HiddenField CountryID = (HiddenField)(grdCustomerAddress.Rows[RowIndex].FindControl("hdCountryID"));
        HiddenField PincodeId = (HiddenField)(grdCustomerAddress.Rows[RowIndex].FindControl("hdfVendorPincodeID"));
        TextBox Pincode = (TextBox)(grdCustomerAddress.Rows[RowIndex].FindControl("txtVendorPinCode"));
        string PinCode = Pincode.Text;
        AppZipCodeTypeColl appZCTColl = new AppZipCodeTypeColl(intRequestingUserID);
        appZCTColl.Search(PinCode);
        if (appZCTColl.Count > 0)
        {
            AppZipCodeType objZip = appZCTColl[0];
            City.Text = objZip.CountryType.City;
            State.Text = objZip.CountryType.State;
            Country.Text = objZip.CountryType.Country;
            CountryID.Value = new AppUtility.AppConvert(objZip.CountryType.ID);
            PincodeId.Value = new AppUtility.AppConvert(objZip.CountryType.ID);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Pincode Found','2'" + ");", true);
            City.Text = "";
            State.Text = "";
            Country.Text = "";
            CountryID.Value = "";
            PincodeId.Value = "";
            Pincode.Focus();           
        }
    }
    #region BindData    
    private void BindData(bool AddNew = false)
    {
        AppCompanyAddressColl appCustomerAddressColl = obj.CompanyAddressColl;
        grdCustomerAddress.DataSource = appCustomerAddressColl;
        grdCustomerAddress.DataBind();
        if (appCustomerAddressColl.Count <= 0)
        {
            AddEmptyRow();
        }
    }
    #endregion
    #region AddEmptyRow
    public void AddEmptyRow()
    {
        AppCompanyAddressColl CollOfAddress = new AppCompanyAddressColl(intRequestingUserID);
        AppCompanyAddress newCustomer = new AppCompanyAddress(intRequestingUserID);
        newCustomer.Address1 = "Add atleast one Address";
        CollOfAddress.Add(newCustomer);
        grdCustomerAddress.DataSource = CollOfAddress;
        grdCustomerAddress.DataBind();
        grdCustomerAddress.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdCustomerAddress.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdCustomerAddress.ShowFooter = true;
    }
    #endregion
    #region Save Address
    public void SaveAddress(string Id, string Address1, string Area, string CountryID,
        string ZipCodeID, bool IsActive, bool IsDefault)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppCompanyAddress SingleObj = new AppCompanyAddress(intRequestingUserID);
        if (String.IsNullOrEmpty(Id) == false)
        {
            SingleObj = obj.CompanyAddressColl[intCommandID];
        }
        else
        {
            obj.AddNewCompanyAddress(SingleObj);
        }
        SingleObj.Company_ID = obj.ID;
        SingleObj.Address1 = Address1;
        SingleObj.Address2 = Area;
        SingleObj.ZipCodeType_ID = new AppUtility.AppConvert(ZipCodeID);
        SingleObj.CountryType_ID = new AppUtility.AppConvert(CountryID);
        SingleObj.Status = new AppUtility.AppConvert(IsActive);
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;
        if (IsDefault == true)
        {
            obj.SetDefaultAddress();
        }
        Session.Add(TableConstants.CustomerSessionObj, obj);
        BindData();
        grdCustomerAddress.EditIndex = -1;
    }
    #endregion
}