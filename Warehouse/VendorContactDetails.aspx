﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="VendorContactDetails" Codebehind="VendorContactDetails.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class=" col-lg-12 ">
             <h3>Contact Details</h3>
            <hr />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdContactDetails" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Horizontal"
                        CssClass="table table-hover table-striped text-nowrap nowrap" OnRowDataBound="grdContactDetails_RowDataBound"
                        OnPreRender="grdContactDetails_PreRender" 
                        OnRowCommand="grdContactDetails_RowCommand" OnRowEditing="grdContactDetails_RowEditing" OnRowDeleting="grdContactDetails_RowDeleting"
                        OnRowUpdating="grdContactDetails_RowUpdating">
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <HeaderTemplate>
                                     <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="Insert" OnClick="lbtnAddNew_Click"
                                        Text="Add New" ToolTip="Add New" />
                                </HeaderTemplate>
                                <ItemTemplate>                                    
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CssClass=" fa fa-edit fa-2x" CommandName="Edit" 
                                        Text="" />
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x" CommandName="Delete" 
                                        Text="" ToolTip="Delete" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="Editcontact"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="CANCELEDIT" CssClass="fa fa-ban fa-2x" toot="Cancel"
                                        Text="" />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x" ToolTip="Save" ValidationGroup="Addcontact"
                                        Text="" UseSubmitBehavior="False" />
                                    <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban  fa-2x" Text="" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblTitle" Text="Title" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblTitle" runat="server" CssClass="bold" MaxLength="50" Text='<%#Eval("Soluation") %>'></asp:Label>                                       
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlTitleType" runat="server" CssClass="form-control" TabIndex="1">
                                        <asp:ListItem Value="Mr" Selected="True">Mr</asp:ListItem>
                                        <asp:ListItem Value="Mrs">Mrs</asp:ListItem>
                                    </asp:DropDownList>                                                                        
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlNewTitleType" runat="server" CssClass="form-control" ValidationGroup="Addcontact" TabIndex="1">
                                        <asp:ListItem Value="Mr" Selected="True">Mr</asp:ListItem>
                                        <asp:ListItem Value="Mrs">Mrs</asp:ListItem>
                                    </asp:DropDownList>                                                                        
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblFName" Text="First Name" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblFname" runat="server" TabIndex="2" Text='<%#Eval("FirstName") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtFirstName" runat="server" Text='<%# Eval("FirstName") %>' MaxLength="50" TabIndex="2" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:Label ID="lblFnameMsg" runat="server" CssClass="text-danger">
                                        <asp:RequiredFieldValidator ID="RFV_FirstName" runat="server" ControlToValidate="txtFirstName"
                                            Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*" ValidationGroup="Editcontact"></asp:RequiredFieldValidator>
                                    </asp:Label>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtFName" runat="server" Text='' MaxLength="50" TabIndex="2" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_3" runat="server" ControlToValidate="txtFName"
                                            Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*" ValidationGroup="Addcontact"></asp:RequiredFieldValidator>                                   
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblLastname" Text="Last Name" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblLName" runat="server" TabIndex="2" Text='<%#Eval("LastName") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtLastName" runat="server" TabIndex="3" MaxLength="50" Text='<%#Eval("LastName") %>' AutoComplete="Off" CssClass="bold input-sm form-control"  ValidationGroup="Editcontact"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtLastName" runat="server" ControlToValidate="txtLastName"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*" ValidationGroup="Editcontact"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewLastNName" runat="server" TabIndex="3" MaxLength="50" Text="" AutoComplete="Off" CssClass="bold input-sm form-control" ValidationGroup="Addcontact"></asp:TextBox>                                    
                                        <asp:RequiredFieldValidator ID="RFV_5" runat="server" ControlToValidate="txtNewLastNName"
                                            Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*" ValidationGroup="Addcontact"></asp:RequiredFieldValidator>                                    
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblEmailID" Text="Email ID" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblEmailId" runat="server" Text='<%#Eval("Email") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEmailID" runat="server" TabIndex="4" Text='<%#Eval("Email") %>' MaxLength="100" CssClass="input-sm form-control" ValidationGroup="Editcontact"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtEmailID" runat="server" ControlToValidate="txtEmailID"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*" ValidationGroup="Editcontact"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmailID"
                                        ErrorMessage="*" Display="Dynamic" SetFocusOnError="true" ValidationGroup="contact" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewEmailID" runat="server" Text='' MaxLength="100" TabIndex="4" CssClass="input-sm form-control" ValidationGroup="Addcontact"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_7" runat="server" ControlToValidate="txtNewEmailID"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*" ValidationGroup="Addcontact"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RFV_8" runat="server" ControlToValidate="txtNewEmailID"
                                        ErrorMessage="*" Display="Dynamic" SetFocusOnError="true" ValidationGroup="contact" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPhoneNo" Text="Phone No" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPhoneNo" runat="server" Text='<%#Eval("Phone1") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtPhoneNo" runat="server" Text='<%#Eval("Phone1") %>' TabIndex="5" CssClass="bold input-sm form-control" ValidationGroup="Editcontact"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtPhoneNo" runat="server" ControlToValidate="txtPhoneNo"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*" ValidationGroup="Editcontact"></asp:RequiredFieldValidator>
                                    <asp:FilteredTextBoxExtender ID="FTB_txtPhoneNo" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtPhoneNo">
                                    </asp:FilteredTextBoxExtender>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewPhoneNo" runat="server" Text='' TabIndex="5" CssClass="input-sm form-control" ValidationGroup="Addcontact"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FTB_txtNewPhoneNo" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtNewPhoneNo">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="RFV_10" runat="server" ControlToValidate="txtNewPhoneNo"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*" ValidationGroup="Addcontact"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblMobileNo" Text="Mobile No" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblMobileNo" runat="server" Text='<%#Eval("Phone2") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtMobileNo" runat="server" Text='<%#Eval("Phone2") %>' TabIndex="6" MaxLength="10" CssClass="input-sm form-control" ValidationGroup="Editcontact"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FTB_txtMobileNo" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtMobileNo">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="RFV_txtMobileNo" runat="server" ControlToValidate="txtMobileNo"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*" ValidationGroup="Editcontact"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewMobileNo" runat="server" Text='' MaxLength="10" TabIndex="6" CssClass="input-sm form-control" ValidationGroup="Addcontact"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FTB_txtNewMobileNo" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtNewMobileNo">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="RFV_11" runat="server" ControlToValidate="txtNewMobileNo"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*" ValidationGroup="Addcontact"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblLocation" Text="Location" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblContactLocation" runat="server" Text='<%# string.Concat(Eval("CompanyAddress.Address1")," ",Eval("CompanyAddress.Address2")) %>' CssClass="bold"></asp:Label>                                    
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlCompanyAddress" runat="server" TabIndex="7" CssClass="form-control input-sm" ValidationGroup="Editcontact">                                    
                                    </asp:DropDownList>   
                                </EditItemTemplate>
                                <FooterTemplate>
                                     <asp:DropDownList ID="ddlNewCompanyAddress" runat="server" TabIndex="7" CssClass="form-control input-sm" ValidationGroup="Addcontact">                                    
                                    </asp:DropDownList>                      
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" runat="server" Text='<%#(Eval("Status").ToString()=="1"?"Y":"N") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsActiveContact" CssClass="form-control" TabIndex="8"  Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkActiveContact" CssClass="form-control" TabIndex="8" Checked="true" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Default" Text="Default" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDefault" runat="server" Text='<%#(Eval("Type").ToString()=="1"?"Y":"N") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsDefaultContact" TabIndex="9" CssClass="form-control" Checked='<%#Eval("Type").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkDefaultContact" TabIndex="9" CssClass="form-control"  />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdContactDetails" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>

    <script type="text/javascript">
        var GridID = $get('<%=grdContactDetails.ClientID%>');
        $(document).ready(function () {
            //GridUI(GridID, 50);
        })
        //var prm = Sys.WebForms.PageRequestManager.getInstance();
        //prm.add_endRequest(function (s, e) {
        //    GridUI(GridID, 50);
        //});
    </script>
</asp:Content>

