﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;



public partial class VendorAdditionalInfo : System.Web.UI.Page
{
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
         
            BindGrid();
        }
    }
    #endregion

    #region BindMaster
    protected override void OnPreInit(EventArgs e)
    {
        base.OnPreInit(e);
        if (Convert.ToString(Request.QueryString["Master"]) == "Simple")
            MasterPageFile = "~/Simple.Master";
        else
            MasterPageFile = "~/Base.Master";

    }
    #endregion

    protected void grdAdditionalInfo_PreRender(object sender, EventArgs e)
    {
        try
        {
            if (grdAdditionalInfo.Rows.Count > 0)
            {
                grdAdditionalInfo.UseAccessibleHeader = true;
                grdAdditionalInfo.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        catch (Exception)
        {

        }
    }

    private DataTable GetSchema()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("Type");
        dt.Columns.Add("TypeNo");
        dt.Columns.Add("Date");    
        return dt;
    }

    private void BindGrid()
    {
        DataTable dt = GetSchema(); // get select column header only records not required
        if (ViewState["GrdAdditioanlInfo"] == null)
        {
            for (int i = 0; i < 15; i++)
            {
                DataRow dr = dt.NewRow(); // add last empty row
                
                dr["Type"] = "CST" + i;
                dr["TypeNo"] = "1234" + i;
                dr["Date"] = i;             
                dt.Rows.Add(dr);
            }
            grdAdditionalInfo.DataSource = dt; // bind new datatable to grid
            grdAdditionalInfo.DataBind();
            ViewState["GrdAdditioanlInfo"] = dt;
        }
        else
        {
            grdAdditionalInfo.DataSource = (DataTable)ViewState["GrdAdditioanlInfo"];
            grdAdditionalInfo.DataBind();
        }
    }


    protected void lbtnAddNew_Click(object sender, EventArgs e)
    {
        grdAdditionalInfo.ShowFooter = true;
        BindGrid();       
    }

    protected void lbtnCancel_Click(object sender, EventArgs e)
    {
        grdAdditionalInfo.ShowFooter = false;
    }
}