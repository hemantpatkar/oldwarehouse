﻿#region Using
using System;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.UI;
using AppUtility;
using System.IO;
using System.Drawing;
using System.Collections;
using System.Linq;
#endregion
public partial class GateEntrySearch : BigSunPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void grdGateRegister_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Print")
        {
            //Int64 GateRegisterID = Convert.ToInt64(e.CommandArgument);
            //ReportManager RP = new ReportManager();
            //RP = db.ReportManagers.FirstOrDefault(p => p.ReportID == 1101);
            //if (RP == null)
            //{
            //    lblMsg.Text = LanguageUtills.GetMessageLanguage("ValidateReport", Convert.ToString(Session["LanguageCode"]));
            //    return;
            //}
            //string url = RP.ReportURL; // Fetch Path from Database
            //string url_updated = url.Replace("@GateRegID", Convert.ToString(GateRegisterID));
            //ViewReport(url_updated, e);
        }
    }

    #region Button Events

    protected void Search()
    {
        AppGateEntryColl objAppGateEntryColl = new AppGateEntryColl(intRequestingUserID);

        #region DateValidation
        DateTime fromdate, to;
        if (!string.IsNullOrEmpty(txtfromdate.Text.Trim()) && !string.IsNullOrEmpty(txttodate.Text.Trim()))
        {
            fromdate = new AppConvert(txtfromdate.Text);
            to = new AppConvert(txttodate.Text);
            if (fromdate > to)
            {
                grdGateRegister.DataSource = null;
                grdGateRegister.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please Select Validate Date range..!" + "','2'" + ");", true);
                return;
            }
        }

        #endregion

        #region Search

        if (ddlStatus.SelectedValue == "0")
        {
            if (txtfromdate.Text.Trim() != "" && txttodate.Text.Trim() != "" && txtfromdate.Text.Trim() != "__/__/____" && txttodate.Text.Trim() != "__/__/____")
            {
                fromdate = Convert.ToDateTime(txtfromdate.Text);
                to = Convert.ToDateTime(txttodate.Text + " 23:59:59");
                objAppGateEntryColl.AddCriteria(GateEntry.ExitOn, Operators.GreaterOrEqualTo, fromdate);
                objAppGateEntryColl.AddCriteria(GateEntry.ExitOn, Operators.LessOrEqualTo, to);
            }
            else if (txtfromdate.Text.Trim() != "" && txtfromdate.Text.Trim() != "__/__/____" && (txttodate.Text.Trim() == "" || txttodate.Text.Trim() == "__/__/____"))
            {
                fromdate = Convert.ToDateTime(txtfromdate.Text);

                objAppGateEntryColl.AddCriteria(GateEntry.ExitOn, Operators.GreaterOrEqualTo, fromdate);
            }
            else if (txttodate.Text.Trim() != "" && txttodate.Text.Trim() != "__/__/____" && (txtfromdate.Text.Trim() == "" || txtfromdate.Text.Trim() == "__/__/____"))
            {
                to = Convert.ToDateTime(txttodate.Text + " 23:59:59");

                objAppGateEntryColl.AddCriteria(GateEntry.ExitOn, Operators.LessOrEqualTo, to);
            }
        }
        else if (ddlStatus.SelectedValue == "1")
        {
            if (txtfromdate.Text.Trim() != "" && txttodate.Text.Trim() != "" && txtfromdate.Text.Trim() != "__/__/____" && txttodate.Text.Trim() != "__/__/____")
            {
                fromdate = Convert.ToDateTime(txtfromdate.Text);
                to = Convert.ToDateTime(txttodate.Text + " 23:59:59");

                objAppGateEntryColl.AddCriteria(GateEntry.EntryOn, Operators.GreaterOrEqualTo, fromdate);
                objAppGateEntryColl.AddCriteria(GateEntry.EntryOn, Operators.LessOrEqualTo, to);
            }
            else if (txtfromdate.Text.Trim() != "" && txtfromdate.Text.Trim() != "__/__/____" && (txttodate.Text.Trim() == "" || txttodate.Text.Trim() == "__/__/____"))
            {
                fromdate = Convert.ToDateTime(txtfromdate.Text);
                objAppGateEntryColl.AddCriteria(GateEntry.EntryOn, Operators.GreaterOrEqualTo, fromdate);
            }
            else if (txttodate.Text.Trim() != "" && txttodate.Text.Trim() != "__/__/____" && (txtfromdate.Text.Trim() == "" || txtfromdate.Text.Trim() == "__/__/____"))
            {
                to = Convert.ToDateTime(txttodate.Text + " 23:59:59");
                objAppGateEntryColl.AddCriteria(GateEntry.EntryOn, Operators.LessOrEqualTo, to);
            }

        }
        else
        {
            if (txtfromdate.Text.Trim() != "" && txttodate.Text.Trim() != "" && txtfromdate.Text.Trim() != "__/__/____" && txttodate.Text.Trim() != "__/__/____")
            {
                fromdate = Convert.ToDateTime(txtfromdate.Text);
                to = Convert.ToDateTime(txttodate.Text + " 23:59:59");
                objAppGateEntryColl.AddCriteria(GateEntry.EntryOn, Operators.GreaterOrEqualTo, fromdate);
                objAppGateEntryColl.AddCriteria(GateEntry.EntryOn, Operators.LessOrEqualTo, to);
                objAppGateEntryColl.AddCriteria(GateEntry.ExitOn, Operators.GreaterOrEqualTo, fromdate);
                objAppGateEntryColl.AddCriteria(GateEntry.ExitOn, Operators.LessOrEqualTo, to);
            }
            else if (txtfromdate.Text.Trim() != "" && txtfromdate.Text.Trim() != "__/__/____" && (txttodate.Text.Trim() == "" || txttodate.Text.Trim() == "__/__/____"))
            {
                fromdate = Convert.ToDateTime(txtfromdate.Text);

                objAppGateEntryColl.AddCriteria(GateEntry.EntryOn, Operators.GreaterOrEqualTo, fromdate);
                objAppGateEntryColl.AddCriteria(GateEntry.ExitOn, Operators.LessOrEqualTo, fromdate);
            }
            else if (txttodate.Text.Trim() != "" && txttodate.Text.Trim() != "__/__/____" && (txtfromdate.Text.Trim() == "" || txtfromdate.Text.Trim() == "__/__/____"))
            {
                to = Convert.ToDateTime(txttodate.Text + " 23:59:59");
                objAppGateEntryColl.AddCriteria(GateEntry.EntryOn, Operators.GreaterOrEqualTo, to);
                objAppGateEntryColl.AddCriteria(GateEntry.ExitOn, Operators.LessOrEqualTo, to);
            }
        }

        if (txtsearch.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(GateEntry.GateNo, Operators.Like, txtsearch.Text);
        }

        if (txtMobile.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(GateEntry.MobileNo, Operators.Like, txtMobile.Text);
        }

        if (txtVehical.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(GateEntry.VehicalNo, Operators.Like, txtVehical.Text);
        }
        if (txtDriver.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(GateEntry.DriverName, Operators.Like, txtDriver.Text);
        }
        if (txtChallanNo.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(GateEntry.DriverName, Operators.Like, txtChallanNo.Text);
        }
        if (txtTransporterName.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(GateEntry.TransporterName, Operators.Like, txtTransporterName.Text);
        }
        if (ddlStatus.SelectedValue != "-1")
        {
            int status = Convert.ToInt32(ddlStatus.SelectedValue);
            objAppGateEntryColl.AddCriteria(GateEntry.Status, Operators.Equals, status); //
        }
        objAppGateEntryColl.Search();        
        objAppGateEntryColl.Sort(AppGateEntry.ComparisionDesc);
        grdGateRegister.VirtualItemCount = objAppGateEntryColl.Count();
        grdGateRegister.DataSource = objAppGateEntryColl;
        grdGateRegister.DataBind();

        #endregion
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Search();
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (grdGateRegister.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Doesnot contain data" + "','2'" + ");", true);
            return;
        }

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grdGateRegister.AllowPaging = false;
            grdGateRegister.HeaderRow.BackColor = Color.White;
            foreach (TableCell cell in grdGateRegister.HeaderRow.Cells)
            {
                cell.BackColor = grdGateRegister.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in grdGateRegister.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = grdGateRegister.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = grdGateRegister.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            grdGateRegister.RenderControl(hw);
            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }


    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("GateEntryDetail.aspx");
    }
    #endregion



    protected void grdGateRegister_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdGateRegister.PageIndex = e.NewPageIndex;
        Search();
    }
}