﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;

public partial class PriceGroupSummary : BigSunPage
{
    AppPriceGroupColl objPriceGroupColl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            BindData();
        }
    }

    protected void Search_Click(object sender, EventArgs e)
    {
        string Code = txtPriceGrpCode.Text;
        string Name = txtPriceGrpName.Text;

        //objPriceGroupColl.Search(Code, Name, CompanyStatus.ALL);
        //grdVendorSummary.DataSource = objCompany;
        //grdVendorSummary.DataBind();
    }   

    protected void lnkNewPriceGrp_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.PriceGroupConst);
        Response.Redirect("PriceGroup.aspx");
    }

    protected void grdPriceGrpSummary_PreRender(object sender, EventArgs e)
    {
        if (grdPriceGrpSummary.Rows.Count > 0)
        {
            grdPriceGrpSummary.UseAccessibleHeader = true;
            grdPriceGrpSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

    #region BindData   
    protected void BindData()
    {
        AppPriceGroupColl objPriceGroupColl = new AppPriceGroupColl(this.intRequestingUserID);
        objPriceGroupColl.Search();
        grdPriceGrpSummary.DataSource = objPriceGroupColl;
        grdPriceGrpSummary.DataBind();
        Session.Add(TableConstants.PriceGroupCollection, objPriceGroupColl);
    }
    #endregion

    protected void grdPriceGrpSummary_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        objPriceGroupColl = (AppPriceGroupColl)Session[TableConstants.PriceGroupCollection];
        objPriceGroupColl[e.RowIndex].Status = new AppConvert(RecordStatus.Deleted);
        objPriceGroupColl[e.RowIndex].Save();
        Session.Add(TableConstants.PriceGroupCollection, objPriceGroupColl);
    }
}