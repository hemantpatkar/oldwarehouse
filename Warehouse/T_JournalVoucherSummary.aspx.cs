﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to display all Company lists.
*/
#endregion
#region Using
using System;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.UI;
using AppUtility;
#endregion
public partial class T_JournalVoucherSummary : BigSunPage
{
    #region PageLoad
    AppJournalVoucherColl objJV;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtVoucherDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            BindData();
        }
    }
    #endregion
    #region Function   
    protected void BindData()
    {
        AppObjects.AppJournalVoucherColl objColl = new AppJournalVoucherColl(intRequestingUserID);
        if (txtJournalVoucherNo.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.JournalVoucher.JournalVoucherNo, AppUtility.Operators.Equals, txtJournalVoucherNo.Text, 0);
        }
        if (txtJournalVoucherAmount.Text != "")
        {
            switch (ddlOperators.SelectedValue)
            {
                case "1":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.JournalVoucher.ExchangeRate, AppUtility.Operators.Equals, txtJournalVoucherAmount.Text, 0);
                    break;
                case "2":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.JournalVoucher.ExchangeRate, AppUtility.Operators.GreaterOrEqualTo, txtJournalVoucherAmount.Text, 0);
                    break;
                case "3":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.JournalVoucher.ExchangeRate, AppUtility.Operators.LessOrEqualTo, txtJournalVoucherAmount.Text, 0);
                    break;
                case "4":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.JournalVoucher.ExchangeRate, AppUtility.Operators.GreaterThan, txtJournalVoucherAmount.Text, 0);
                    break;
                case "5":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.JournalVoucher.ExchangeRate, AppUtility.Operators.LessThan, txtJournalVoucherAmount.Text, 0);
                    break;
            }
        }
        if (!string.IsNullOrEmpty(txtVoucherFromDate.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.JournalVoucher.JournalVoucherDate, AppUtility.Operators.GreaterOrEqualTo, txtVoucherFromDate.Text, 0);
        }
        if (!string.IsNullOrEmpty(txtVoucherToDate.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.JournalVoucher.JournalVoucherDate, AppUtility.Operators.LessOrEqualTo, txtVoucherToDate.Text, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
            objColl.AddCriteria(AppObjects.JournalVoucher.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.JournalVoucherSummary] = objColl;
        grdJournalVoucherSummary.DataSource = objColl;
        grdJournalVoucherSummary.DataBind();
    }
    public void CancelJournalVoucher(AppJournalVoucher ObjJournalVoucher)
    {
        AppJournalVoucher ObjDuplicateJournalVoucher = ObjJournalVoucher;
        ObjDuplicateJournalVoucher.ID = 0;
        ObjDuplicateJournalVoucher.VoucherType_ID = 32; //Journal Voucher Cancel
        ObjDuplicateJournalVoucher.ModifiedBy = intRequestingUserID;
        ObjDuplicateJournalVoucher.ModifiedOn = System.DateTime.Now;
        ObjDuplicateJournalVoucher.Status = new AppConvert((int)RecordStatus.Cancelled);

        if (ObjDuplicateJournalVoucher.JournalVoucherDetailColl.Count > 0)
        {
            int TotalCount = ObjDuplicateJournalVoucher.JournalVoucherDetailColl.Count;
            for (int i = 0; i < TotalCount; i++)
            {
                ObjDuplicateJournalVoucher.JournalVoucherDetailColl[i].ID = 0;
                if (ObjDuplicateJournalVoucher.JournalVoucherDetailColl[i].DebitAmount > 0)
                {
                    ObjDuplicateJournalVoucher.JournalVoucherDetailColl[i].CreditAmount = ObjDuplicateJournalVoucher.JournalVoucherDetailColl[i].DebitAmount;
                    ObjDuplicateJournalVoucher.JournalVoucherDetailColl[i].DebitAmount = 0;
                }
                else
                {
                    ObjDuplicateJournalVoucher.JournalVoucherDetailColl[i].DebitAmount = ObjDuplicateJournalVoucher.JournalVoucherDetailColl[i].CreditAmount;
                    ObjDuplicateJournalVoucher.JournalVoucherDetailColl[i].CreditAmount = 0;
                }
                ObjDuplicateJournalVoucher.JournalVoucherDetailColl[i].ModifiedBy = intRequestingUserID;
                ObjDuplicateJournalVoucher.JournalVoucherDetailColl[i].ModifiedOn = System.DateTime.Now;
                ObjDuplicateJournalVoucher.JournalVoucherDetailColl[i].Status = new AppConvert((int)RecordStatus.Cancelled);
            }
        }
        ObjDuplicateJournalVoucher.Save();

        #region Add Ledger Entry


        if (ObjDuplicateJournalVoucher.JournalVoucherDetailColl.Count > 0)
        {
            foreach (var item in ObjDuplicateJournalVoucher.JournalVoucherDetailColl)
            {
                if (item.CreditAmount != 0)
                {
                    LedgerEntry(LoginCompanyID, 100601, 1006, item.Ledger_ID, item.ID, ObjDuplicateJournalVoucher.JournalVoucherDate, item.CreditAmount, 0, ObjDuplicateJournalVoucher.JournalVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicateJournalVoucher.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, ObjDuplicateJournalVoucher.VoucherType_ID, item.JournalVoucherDesc);
                }
                else
                {
                    LedgerEntry(LoginCompanyID, 100602, 1006, item.Ledger_ID, item.ID, ObjDuplicateJournalVoucher.JournalVoucherDate, 0, item.DebitAmount, ObjDuplicateJournalVoucher.JournalVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicateJournalVoucher.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, ObjDuplicateJournalVoucher.VoucherType_ID, item.JournalVoucherDesc);
                }


            }

        }


        #endregion
    }
    #endregion
    #region GridView Events
    protected void grdJournalVoucherSummary_PreRender(object sender, EventArgs e)
    {
        if (grdJournalVoucherSummary.Rows.Count > 0)
        {
            grdJournalVoucherSummary.UseAccessibleHeader = true;
            grdJournalVoucherSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region Button Click Events
    protected void btnNewJournalVoucher_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.ObjCompanySession);
        Response.Redirect("T_JournalVoucher.aspx");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (Update(grdJournalVoucherSummary, new AppConvert((int)RecordStatus.Deleted), 1006, 1006) == 1)
        {
            BindData();
        }
    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        if (Update(grdJournalVoucherSummary, new AppConvert((int)RecordStatus.Approve), 1006, 1006) == 1)
        {
            BindData();
        }
    }
    protected void btnCanceled_Click(object sender, EventArgs e)
    {
        AppObjects.AppJournalVoucherColl objColl = new AppJournalVoucherColl(intRequestingUserID);
        if (txtCancelJVNo.Text != "" && txtVoucherDate.Text != "")
        {
            objColl.AddCriteria(AppObjects.JournalVoucher.JournalVoucherNo, AppUtility.Operators.Equals, txtCancelJVNo.Text);
            objColl.Search(RecordStatus.ALL);

            if (objColl.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Record not found','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                return;
            }
            else
            {
                AppJournalVoucher objSingleJV = objColl[0];
                if (objColl[0].Status == new AppUtility.AppConvert((int)RecordStatus.Cancelled))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelJVNo.Text + " Transaction alerady cancelled','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelJVNo.Focus();
                    return;
                }
                else if (objColl[0].Status == new AppUtility.AppConvert((int)RecordStatus.Deleted))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelJVNo.Text + " Transaction is deleted','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelJVNo.Focus();
                    return;
                }
                else if (objColl[0].Status == new AppUtility.AppConvert((int)RecordStatus.Created))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelJVNo.Text + " Transaction is not approved','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelJVNo.Focus();
                    return;
                }
                else if (objColl[0].JournalVoucherDate > new AppUtility.AppConvert(Convert.ToDateTime(txtVoucherDate.Text)))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelJVNo.Text + " Cancel voucher date should be greater than or equal to voucher date.','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelJVNo.Focus();
                    return;
                }
                objSingleJV.Status = new AppUtility.AppConvert((int)RecordStatus.Cancelled);
                // objSingleJV.JournalVoucherDetailColl.ForEach(x => x.Status = new AppConvert((int)RecordStatus.Cancelled));
                objSingleJV.Save();

                if (objSingleJV.JournalVoucherDetailColl.Count > 0)
                {
                    foreach (var item in objSingleJV.JournalVoucherDetailColl)
                    {
                        if (item.CreditAmount != 0)
                        {
                            UpdateLedgerEntryStatus(100601, 1006, item.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                        }
                        else
                        {
                            UpdateLedgerEntryStatus(100602, 1006, item.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                        }
                    }
                }
                CancelJournalVoucher(objSingleJV);
            }
            Session[TableConstants.JournalVoucherSummary] = objColl;
            grdJournalVoucherSummary.DataSource = objColl;
            grdJournalVoucherSummary.DataBind();
            txtCancelJVNo.Text = "";
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Journal voucher cancelled successfully','1'" + ");", true);


        }
        else
        {
            if (txtCancelJVNo.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter journal voucher no.','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                txtCancelJVNo.Focus();
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter voucher date.','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                txtVoucherDate.Focus();
                return;
            }
        }
    }
    #endregion 
}