﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="T_PurchaseGRN" Codebehind="T_PurchaseGRN.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function toggleColor(sender) {
            debugger
            var id = sender.id;
            if (id == "HeaderDiscount") {
                var DiscountType = $get('<%=hdfDiscountType.ClientID %>');
                $("#iDiscount").toggleClass("fa fa-percent");
                $("#iDiscount").toggleClass("fa fa-inr");
                var li = document.getElementById('iDiscount');
                if (li.className == "fa fa-inr") {
                    DiscountType.value = 2
                }
                else {
                    DiscountType.value = 1
                }
            }
            else {
                var SplitIdForIndex = sender.id.split('_');
                var DiscountType = $get(SplitIdForIndex[0] + '_' + SplitIdForIndex[1] + "_hdfDiscountType_" + SplitIdForIndex[3]);
                var iLinItemDiscount = $get(SplitIdForIndex[0] + '_' + SplitIdForIndex[1] + "_iLinItemDiscount_" + SplitIdForIndex[3]);
                $("#" + iLinItemDiscount.id + "").toggleClass("fa fa-percent");
                $("#" + iLinItemDiscount.id + "").toggleClass("fa fa-inr");
                var li = document.getElementById(iLinItemDiscount.id);
                if (li.className == "fa fa-inr") {
                    DiscountType.value = 2
                }
                else {
                    DiscountType.value = 1
                }
            }
        }
        function validateFloatKeyPress(el, evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            if (charCode == 46 && el.value.indexOf(".") !== -1) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div id="divButtons" class="pull-right">
                        <div class="btn-group">
                            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="btn btn-sm  btn-primary" OnClick="btnSave_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnCancel" Text="Back" CssClass="btn  btn-sm  btn-primary" OnClick="btnCancel_Click"></asp:Button>
                            <asp:Button runat="server" ID="btnApprove" Text="Approve" CssClass="btn  btn-sm  btn-primary" OnClick="btnApprove_Click"></asp:Button>
                        </div>
                    </div>
                    <h4>Purchase GRN </h4>
                    <hr />
                    <div class="row">
                        <div class=" col-lg-12">
                            <asp:UpdatePanel ID="up_PurchaseGRN" runat="server">
                                <ContentTemplate>
                                    <asp:TabContainer runat="server" CssClass="Bigtab" ActiveTabIndex="0" ID="tabMain">
                                        <asp:TabPanel runat="server" HeaderText="Basic Info" TabIndex="11" ID="tblLineItemDetail">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div id="divPurchaseGRNDate" class="col-lg-3">
                                                        <asp:Label ID="lblPurchaseGRNDate" runat="server" Text="GRN Date" CssClass="bold"></asp:Label>
                                                        <asp:Label ID="lblGRNDATEMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                        <div class="input-group input-group-sm">
                                                            <asp:TextBox ID="txtPurchaseGRNDate" TabIndex="1" runat="server" placeholder="GRN Date" CssClass="form-control input-sm" ValidationGroup="MainSave"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <asp:MaskedEditExtender ID="Mee_txtPurchaseGRNDate" runat="server"
                                                            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtPurchaseGRNDate">
                                                        </asp:MaskedEditExtender>
                                                        <asp:CalendarExtender ID="Ce_PurchaseGRNDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtPurchaseGRNDate"
                                                            TargetControlID="txtPurchaseGRNDate">
                                                        </asp:CalendarExtender>
                                                    </div>
                                                    <div id="divPurchaseOrder" class="col-lg-3">
                                                        <asp:Label ID="lblPO" CssClass="bold " runat="server" Text="Purchase Order">             
                                                        </asp:Label>
                                                        <asp:Label ID="lblPOMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfvtxtPurchaseOrder" runat="server" ControlToValidate="txtPurchaseOrder" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage="" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <div class="input-group input-group-sm">
                                                            <asp:TextBox ID="txtPurchaseOrder" TabIndex="1" placeholder="Select Purchase Order" AutoPostBack="true" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" OnTextChanged="txtPurchaseOrder_TextChanged"
                                                                CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfPurchaseOrderName" Value="" />
                                                        <asp:HiddenField runat="server" ID="hdfPurchaseOrderID" Value="" />
                                                        <asp:AutoCompleteExtender ID="ACEtxtPurchaseOrder" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="PurchaseOrderSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtPurchaseOrder">
                                                        </asp:AutoCompleteExtender>
                                                    </div>
                                                    <div id="divPurchaseOrderDate" class="col-lg-3">
                                                        <asp:Label ID="lblPurchaseOrderDate" runat="server" Text="Purchase Order Date" CssClass="bold"></asp:Label>
                                                        <div class="input-group input-group-sm">
                                                            <asp:TextBox ID="txtPurchaseOrderDate" TabIndex="1" runat="server" placeholder="Purchase Order Date" Enabled="false" CssClass="form-control input-sm" ValidationGroup="MainSave"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <asp:MaskedEditExtender ID="Mee_txtPurchaseOrderDate" runat="server" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtPurchaseOrderDate">
                                                        </asp:MaskedEditExtender>
                                                        <asp:CalendarExtender ID="Ce_txtPurchaseOrderDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtPurchaseOrderDate"
                                                            TargetControlID="txtPurchaseOrderDate">
                                                        </asp:CalendarExtender>
                                                    </div>
                                                    <div id="divTotalPurchaseGRNAmount" class="col-lg-3">
                                                        <asp:Label ID="lblTotalPurchaseGRNAmount" runat="server" CssClass="bold" Text="Total GRN Amount"></asp:Label>
                                                        <div class="input-group input-group-sm">
                                                            <asp:TextBox ID="txtTotalAmount" runat="server" Enabled="false" TabIndex="1" placeholder="0.00" CssClass="form-control amt input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-inr"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:TabPanel>
                                        <asp:TabPanel runat="server" HeaderText="Other Info" TabIndex="15" ID="tblAttachment">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div id="divPurchaseGRNNo" class="col-lg-3">
                                                        <asp:Label ID="lblPurchaseGRNNo" runat="server" CssClass="bold" Text="PurchaseGRN  No"></asp:Label>
                                                        <asp:TextBox ID="txtPurchaseGRNNo" TabIndex="1" placeholder="GRN  No" Enabled="false" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                    <div id="divStatus" class="col-lg-3">
                                                        <asp:Label ID="lblStatus" runat="server" CssClass="bold" Text="Status"></asp:Label>
                                                        <asp:TextBox ID="txtStatus" placeholder="Transaction Status" TabIndex="1" Enabled="false" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                    <div id="divTermsAndConditions" class="col-lg-3">
                                                        <asp:Label ID="lblTermsAndConditions" CssClass="bold " runat="server" Text="Terms And Conditions">             
                                                        </asp:Label>
                                                        <asp:Label ID="lblTermsAndConditionsMsg" runat="server" CssClass="bold text-danger" Text=" * "></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfvTermsAndConditions" runat="server" ControlToValidate="txtTermsAndConditions" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage=" (Select Terms And Conditions)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <div class="input-group input-group-sm">
                                                            <asp:TextBox ID="txtTermsAndConditions" TabIndex="1" placeholder="Select Terms And Conditions" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)"
                                                                CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfTermsAndConditionsName" Value="" />
                                                        <asp:HiddenField runat="server" ID="hdfTermsAndConditionsID" Value="" />
                                                        <asp:AutoCompleteExtender ID="ACEtxtTermsAndConditions" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TermsAndConditionsSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtTermsAndConditions">
                                                        </asp:AutoCompleteExtender>
                                                    </div>
                                                    <div id="divDiscount" class="col-lg-3">
                                                        <asp:Label ID="lblDiscount" runat="server" CssClass="bold" Text="Discount">  </asp:Label>
                                                        <div class="input-group input-group-sm">
                                                            <asp:TextBox ID="txtDiscount" TabIndex="1" placeholder="0.00" runat="server" AutoCompleteType="None" AutoPostBack="true" AutoComplete="Off"
                                                                CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon" id="HeaderDiscount" onclick="return toggleColor(this)"><i id="iDiscount" class="fa fa-percent"></i></span>
                                                            <asp:HiddenField runat="server" ID="hdfDiscountType" Value="1" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="rowMulticurrency" runat="server">
                                                    <div class="newline"></div>
                                                    <div class="row">
                                                        <div id="divBaseCurrency" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderBaseCurrency" Text="Base Currency" runat="server" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblHeaderBaseCurrencyMsg" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                            <asp:TextBox ID="txtHeaderBaseCurrency" placeholder="Select Base Currency" runat="server" AutoPostBack="True" CssClass="form-control amt input-sm" TabIndex="1" Enabled="false"></asp:TextBox>
                                                            <asp:HiddenField runat="server" ID="hdfHeaderBaseCurrencyName" Value="" />
                                                            <asp:HiddenField runat="server" ID="hdfHeaderBaseCurrencyID" Value="" />
                                                        </div>
                                                        <div id="divExchangeCurrency" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderExchangeCurrency" CssClass="bold " runat="server" Text="Transaction Currency"> </asp:Label>
                                                            <asp:Label ID="lblHeaderExchangeCurrencyMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                            <asp:RequiredFieldValidator ID="Rfv_txtHeaderExchangeCurrency" runat="server" ControlToValidate="txtHeaderExchangeCurrency" CssClass="text-danger"
                                                                Display="Dynamic" ErrorMessage=" (Select Transaction Currency)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                            <div class="input-group input-group-sm">
                                                                <asp:TextBox ID="txtHeaderExchangeCurrency" TabIndex="1" placeholder="Select Transaction Currency" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfHeaderExchangeCurrencyName" Value="" />
                                                            <asp:HiddenField runat="server" ID="hdfHeaderExchangeCurrencyID" Value="" />
                                                            <asp:AutoCompleteExtender ID="ACEtxtHeaderExchangeCurrency" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CurrencyTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtHeaderExchangeCurrency">
                                                            </asp:AutoCompleteExtender>
                                                        </div>
                                                        <div id="divExchangeCurrencyRate" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderExchangeCurrencyRate" Text="Exchange Currency Rate" runat="server" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblHeaderExchangeCurrencyRateMsg" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                            <div class="input-group input-group-sm">
                                                                <asp:TextBox ID="txtHeaderExchangeCurrencyRate" placeholder="0.00" runat="server" AutoPostBack="True" CssClass="form-control amt input-sm"
                                                                    onkeypress="return validateFloatKeyPress(this, event)" TabIndex="1" ReadOnly="True"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                            </div>
                                                        </div>
                                                        <div id="divCurrencyConvertedAmount" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderCurrencyConvertedAmount" Text="Converted Currency Amount" runat="server" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblHeaderCurrencyConvertedAmountMsg" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                            <asp:TextBox ID="txtHeaderCurrencyConvertedAmount" placeholder="0.00" runat="server" CssClass="form-control amt input-sm" Enabled="False" TabIndex="1"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="newline"></div>
                                                <div class="row">
                                                    <div id="divCompanyAddress" class="col-lg-3">
                                                        <asp:Label ID="lblCompanyAddress" CssClass="bold " runat="server" Text="Company Address"> </asp:Label>
                                                        <asp:Label ID="lblCompanyAddressMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                        <asp:RequiredFieldValidator ID="Rfv_txtCompanyAddress" runat="server" ControlToValidate="txtCompanyAddress" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage="" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtCompanyAddress" TextMode="MultiLine" TabIndex="1" placeholder="Select Company Address" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfCompanyAddressName" Value="" />
                                                        <asp:HiddenField runat="server" ID="hdfCompanyAddressID" Value="" />
                                                        <asp:AutoCompleteExtender ID="ACEtxtCompanyAddress" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanyAddressSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtCompanyAddress">
                                                        </asp:AutoCompleteExtender>
                                                    </div>
                                                    <div id="divDeliveryAddress" class="col-lg-3">
                                                        <asp:Label ID="lblDeliveryAddress" CssClass="bold " runat="server" Text="Delivery Address"> </asp:Label>
                                                        <asp:Label ID="lblDeliveryAddressMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RFV_txtDeliveryAddress" runat="server" ControlToValidate="txtDeliveryAddress" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage=" (Select Delivery Address)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtDeliveryAddress" TextMode="MultiLine" TabIndex="1" placeholder="Select Delivery Address" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfDeliveryAddressName" Value="" />
                                                        <asp:HiddenField runat="server" ID="hdfDeliveryAddressID" Value="" />
                                                        <asp:AutoCompleteExtender ID="ACEtxtDeliveryAddress" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanyAddressSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtDeliveryAddress">
                                                        </asp:AutoCompleteExtender>
                                                    </div>
                                                    <div id="divVendorAddress" class="col-lg-3">
                                                        <asp:Label ID="lblVendorAddress" CssClass="bold " runat="server" Text="Vendor Address"> </asp:Label>
                                                        <asp:Label ID="lblVendorAddressMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RFV_txtVendorAddress" runat="server" ControlToValidate="txtVendorAddress" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage=" (Select Transaction Currency)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtVendorAddress" TextMode="MultiLine" TabIndex="1" placeholder="Select Vendor Address" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfVendorAddressName" Value="" />
                                                        <asp:HiddenField runat="server" ID="hdfVendorAddressID" Value="" />
                                                        <asp:HiddenField runat="server" ID="hdfVendorID" Value="" />
                                                        <asp:AutoCompleteExtender ID="ACEtxtVendorAddress" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanyAddressSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtVendorAddress">
                                                        </asp:AutoCompleteExtender>
                                                    </div>
                                                    <div id="divRemark" class="col-lg-3">
                                                        <asp:Label ID="lblRemark" runat="server" CssClass="bold" Text="Remark"></asp:Label>
                                                        <asp:TextBox ID="txtRemark" placeholder="Remark" TextMode="MultiLine" runat="server" TabIndex="1" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div id="divTax" class="col-lg-3">
                                                        <asp:Label ID="lblHeaderTax" CssClass="bold " runat="server" Text="Tax"> </asp:Label>
                                                        <asp:Label ID="lblHeaderTaxMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RFV_txtHeaderTax" runat="server" ControlToValidate="txtHeaderTax" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage=" (Select Transaction Currency)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <div class="input-group input-group-sm">
                                                            <asp:TextBox ID="txtHeaderTax" AutoPostBack="true" TabIndex="1" placeholder="Select Tax" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfHeaderTaxName" Value="" />
                                                        <asp:HiddenField runat="server" ID="hdfHeaderTaxID" Value="" />
                                                        <asp:AutoCompleteExtender ID="ACEtxtHeaderTax" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TaxTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtHeaderTax">
                                                        </asp:AutoCompleteExtender>
                                                    </div>
                                                    <div id="divHeaderTaxAmount" class="col-lg-3">
                                                        <asp:Label ID="lblHeadertaxAmount" runat="server" CssClass="bold" Text="Tax Amount"></asp:Label>
                                                        <asp:TextBox ID="txtHeaderTaxAmount" placeholder="0.00" Enabled="false" runat="server" TabIndex="1" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <asp:Label ID="lblDCReference" runat="server" CssClass="bold" Text="Delivery Challan"></asp:Label>
                                                        <asp:TextBox ID="txtDeliveryChallanNo" placeholder="Delivery Challan No" runat="server" TabIndex="1" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                    <div id="divDCDate" class="col-lg-3">
                                                        <asp:Label ID="lblDCDate" runat="server" Text="Delivery Challan Date" CssClass="bold"></asp:Label>
                                                        <div class="input-group input-group-sm">
                                                            <asp:TextBox ID="txtDCDate" TabIndex="1" runat="server" placeholder="Delivery Challan Date" CssClass="form-control input-sm" ValidationGroup="MainSave"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <asp:MaskedEditExtender ID="MEE_DCDate" runat="server" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtDCDate">
                                                        </asp:MaskedEditExtender>
                                                        <asp:CalendarExtender ID="CE_DCDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtDCDate"
                                                            TargetControlID="txtDCDate">
                                                        </asp:CalendarExtender>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:TabPanel>
                                    </asp:TabContainer>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <asp:UpdatePanel runat="server" ID="PurchaseGRNItems">
                        <ContentTemplate>
                            <asp:GridView ID="grdPurchaseGRNItemDetails" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Both" ShowFooter="true" OnRowCommand="grdPurchaseGRNItemDetails_RowCommand"
                                CssClass="table table-hover table-striped text-nowrap nowrap" OnPreRender="grdPurchaseGRNItemDetails_PreRender">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="200">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderItem" Text="Item" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtItem" runat="server" ControlToValidate="txtItem" CssClass="text-danger" Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtItem" TabIndex="1" Text='<%# Eval("Item.Name") %>' AutoPostBack="true" OnTextChanged="txtItem_TextChanged" placeholder="Select Item" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfItemName" Value='<%# Eval("Item.Name") %>' />
                                            <asp:HiddenField runat="server" ID="hdfItemID" Value='<%# Eval("Item_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtItem" runat="server" CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="ItemSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtItem">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderUOM" Text="UOM" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hdfUOMID" Value='<%# Eval("UOMType_ID") %>' />
                                            <asp:RequiredFieldValidator ID="Rfv_txtUOM" runat="server" ControlToValidate="txtUOM" CssClass="text-danger" Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtUOM" TabIndex="1" Text='<%# Eval("UOMType.Name") %>' AutoPostBack="false" placeholder="Select UOM" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfUOMName" Value='<%# Eval("UOMType.Name") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtUOM" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="UOMTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtUOM">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterTotal" Text="Total" runat="server" CssClass="bold"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderQuantity" Text="Quantity" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtQuantity" Enabled="false" TabIndex="1" AutoPostBack="true" Text='<%# Convert.ToDecimal(Eval("Quantity")).ToString("#,##0.00") %>' runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterQuantity" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderReceivedQuantity" Text="Received Qty" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtReceivedQuantity" TabIndex="1" AutoPostBack="true" Text='<%# Convert.ToDecimal(Eval("ReceivedQuantity")).ToString("#,##0.00") %>' OnTextChanged="txtReceivedQuantity_TextChanged" placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterReceivedQuantity" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderAcceptedQuantity" Text="Accepted Qty" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAcceptedQuantity" TabIndex="1" AutoPostBack="true" Text='<%# Convert.ToDecimal(Eval("AcceptedQuantity")).ToString("#,##0.00") %>' OnTextChanged="txtAcceptedQuantity_TextChanged" placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterAcceptedQuantity" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderRejectedQuantity" Text="Rejected Qty" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtRejectedQuantity" TabIndex="1" AutoPostBack="true" Text='<%# Convert.ToDecimal(Eval("RejectQuantity")).ToString("#,##0.00") %>' placeholder="0.00" runat="server" AutoCompleteType="None" OnTextChanged="txtRejectedQuantity_TextChanged"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterRejectedQuantity" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderRate" Text="Rate" runat="server" CssClass="bold"></asp:Label>
                                                (  <span><i class="fa fa-inr"></i></span>)
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtRate" TabIndex="1" OnTextChanged="txtRate_TextChanged" AutoPostBack="true" Text='<%# Convert.ToDecimal(Eval("ItemRate")).ToString("#,##0.00") %>' placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderBasePrice" Text="Base Price" runat="server" CssClass="bold"></asp:Label>
                                                (  <span><i class="fa fa-inr"></i></span>)
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtBasePrice" TabIndex="1" Text='<%# Convert.ToDecimal(Eval("BasePrice")).ToString("#,##0.00") %>' placeholder="0.00" runat="server" AutoCompleteType="None" Enabled="false"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterBasePrice" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderDiscount" Text="Discount" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="input-group input-group-sm">
                                                <asp:TextBox ID="txtDiscount" TabIndex="1" OnTextChanged="txtDiscount_TextChanged" AutoPostBack="true" Text='<%# Convert.ToDecimal(Eval("DiscountValue")).ToString("#,##0.00") %>' placeholder="0.00" runat="server" AutoCompleteType="None" AutoComplete="Off"
                                                    CssClass="form-control input-sm"></asp:TextBox>
                                                <span class="input-group-addon input-group-sm" id="LinItemDiscount"><i id="iLinItemDiscount" onclick="return toggleColor(this)" runat="server" class="fa fa-percent"></i></span>
                                                <asp:HiddenField runat="server" ID="hdfDiscountType" Value='<%# Eval("DiscountType") %>' />
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterDiscount" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderLineItemTax" Text="Tax" runat="server" CssClass="bold"></asp:Label>
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="input-group">
                                                <asp:TextBox ID="txtLineItemTax" TabIndex="1" AutoPostBack="true" Text='<%# Eval("TaxType.Name") %>' OnTextChanged="txtLineItemTax_TextChanged" placeholder="Select Tax" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="hdfLineItemTaxName" Value='<%# Eval("TaxType.Name") %>' />
                                                <asp:HiddenField runat="server" ID="hdfLineItemTaxID" Value='<%# Eval("TaxType_ID") %>' />
                                            </div>
                                            <asp:AutoCompleteExtender ID="ACEtxtLineItemTax" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TaxTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLineItemTax">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderLineItemTaxAmount" Text="Tax" runat="server" CssClass="bold"></asp:Label>
                                                (  <span><i class="fa fa-inr"></i></span>)
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="form-control input-sm" Enabled="false" ID="txtTaxAmount" TabIndex="1" Text='<%# Convert.ToDecimal(Eval("TaxAmount")).ToString("#,##0.00") %>' runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterTax" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderNetAmount" Text="Net Amount" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="input-group">
                                                <asp:TextBox ID="txtNetAmount" Text='<%# Convert.ToDecimal(Eval("NetAmount")).ToString("#,##0.00") %>' TabIndex="1" placeholder="0.00" Enabled="false" runat="server" AutoCompleteType="None" AutoComplete="Off"
                                                    CssClass="form-control input-sm"></asp:TextBox>
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterNetAmount" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderLineItemExchangeCurrency" CssClass="bold " runat="server" Text="Trans Currency"> </asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtLineItemExchangeCurrency" runat="server" ControlToValidate="txtLineItemExchangeCurrency" CssClass="text-danger"
                                                Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtLineItemExchangeCurrency" Text='<%# Eval("CurrencyType.Name") %>' TabIndex="1" placeholder="Select Transaction Currency" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfLineItemExchangeCurrencyName" Value='<%# Eval("CurrencyType.Name") %>' />
                                            <asp:HiddenField runat="server" ID="hdfLineItemExchangeCurrencyID" Value='<%# Eval("CurrencyType_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtLineItemExchangeCurrency" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CurrencyTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLineItemExchangeCurrency">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="5">
                                        <HeaderTemplate>
                                            <asp:Button ID="btnAddLineItem" TabIndex="1" Text="Add Item" runat="server" OnClick="btnAddLineItem_Click" CssClass=" btn btn-xs btn-primary"></asp:Button>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnRemoveLineItem" CommandName="Remove" TabIndex="1" runat="server" Text="Remove"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
