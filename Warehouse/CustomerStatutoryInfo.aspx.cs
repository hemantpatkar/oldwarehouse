﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to add,edit and delete statutory info of Company
*/
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
#endregion
public partial class CustomerStatutoryInfo : BigSunPage
{
    AppCustomer objCompany = null;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        objCompany = (AppCustomer)Session[TableConstants.CustomerSessionObj];     
        if (!IsPostBack)
        {
            BindData();
        }
    }
    #endregion
    #region grdCustomerStatuInfo_PreRender
    protected void grdCustomerStatuInfo_PreRender(object sender, EventArgs e)
    {
        if (grdCustomerStatuInfo.Rows.Count > 0)
        {
            grdCustomerStatuInfo.UseAccessibleHeader = true;
            grdCustomerStatuInfo.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region BindData
    protected void BindData()
    {       
        grdCustomerStatuInfo.DataSource = objCompany.CompanyStatutoryCollAllList;
        grdCustomerStatuInfo.DataBind();
    }
    #endregion
    #region txtValue_TextChanged
    protected void txtValue_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        string statutoryId = ((HiddenField)currentRow.FindControl("hdfStatutoryId")).Value;
        string statutoryval = ((TextBox)currentRow.FindControl("txtValue")).Text;             
        AppObjects.AppCompanyStatutory SingleObj = new AppObjects.AppCompanyStatutory(intRequestingUserID);
        SingleObj.Company_ID = objCompany.ID;
        SingleObj.StatutoryType_ID = new AppConvert(statutoryId);
        SingleObj.Value = statutoryval;
        SingleObj.Status = new AppConvert((int)RecordStatus.Active);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;
        objCompany.AddNewCompanyStatutory(SingleObj);
        int index = objCompany.CompanyStatutoryCollAllList.FindIndex(o => o.StatutoryType_ID == SingleObj.StatutoryType_ID);
        if (index >= -1)
            objCompany.CompanyStatutoryCollAllList[index] = SingleObj;
        Session.Add(TableConstants.CustomerSessionObj,objCompany);
        BindData();
    }
    #endregion
}