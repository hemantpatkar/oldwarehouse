﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="DepartmentType" Codebehind="DepartmentType.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row" runat="server">
        <div class="col-lg-12">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="panel">
                        <div class="panel-body">
                            <h3>User Department Type </h3>
                            <hr />
                            <div class="row" id="pnlDeptEntry" runat="server">
                                <div class="col-lg-12">
                                    <div class="col-lg-2">
                                        <asp:Label ID="lblDeptCode" runat="server" CssClass="bold" Text="Department Code"></asp:Label>
                                        <asp:Label ID="Label8" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ControlToValidate="txtDeptCode" CssClass="text-danger"
                                            Display="Dynamic" ErrorMessage="*" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtDeptCode" runat="server" TabIndex="1" MaxLength="50" ValidationGroup="FinalSave" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-2">
                                        <asp:Label ID="lblDeptName" runat="server" CssClass="bold" Text="Department Name"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDeptName" CssClass="text-danger"
                                            Display="Dynamic" ErrorMessage="*" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtDeptName" runat="server" TabIndex="1" MaxLength="50" ValidationGroup="FinalSave" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-1">
                                        <asp:Label ID="lblActive" runat="server" Text="Active/InActive" CssClass="bold"></asp:Label>
                                        <div class="material-switch pull-right">
                                            <asp:CheckBox runat="server" ID="chkActive" TabIndex="1" Checked="true" />
                                            <label for='<%=chkActive.ClientID %>' class="label-primary"></label>
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="row" runat="server" id="pnlSearch">
                                <div class="col-lg-12">
                                   <%-- <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>--%>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-2">
                                                        <asp:Label ID="Label1" runat="server" CssClass="bold" Text="Department Code"></asp:Label>
                                                        <asp:TextBox ID="txtCode" runat="server" TabIndex="1" MaxLength="50" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <asp:Label ID="Label4" runat="server" CssClass="bold" Text="Department Name"></asp:Label>
                                                        <asp:TextBox ID="txtName" runat="server" TabIndex="1" MaxLength="50" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                                                    </div>
                                                    <div class="col-lg-1">
                                                    </div>
                                                </div>
                                            </div>
                                       <%-- </ContentTemplate>
                                    </asp:UpdatePanel>--%>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                             <div class="row">
                                <div class="col-lg-12">
                                    <div class="btn-group pull-right">
                                        <asp:Button runat="server" ID="btnSearch" Text="Search" CssClass="btn btn-primary" OnClick="btnSearch_Click" TabIndex="1" />
                                         <asp:Button runat="server" ID="btnSaveDept" Text="Save" CssClass="btn btn-primary" TabIndex="1" ValidationGroup="FinalSave" OnClick="btnSaveDept_Click" />
                                        <asp:HiddenField ID="hdfID" runat="server" Value="" />
                                         <asp:Button runat="server" TabIndex="1" ID="btnCancel" Text="Reset" CssClass="btn btn-default" OnClick="btnCancel_Click" />
                                        <asp:Button runat="server" TabIndex="1" ID="btnResetCancel" Text="Cancel" CssClass="btn btn-default" OnClick="btnResetCancel_Click" />
                                        </div>
                                    </div>
                                 </div>                            
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <div class="row" runat="server" id="pnlGridViewDisplay">
        <div class="col-lg-12">
            <asp:UpdatePanel ID="pnlGridView" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdDepartments" runat="server" AutoGenerateColumns="False" OnPreRender="grdDepartments_PreRender" CssClass="table table-striped dt-responsive table-hover" OnRowDeleting="grdDepartments_RowDeleting"
                        OnRowEditing="grdDepartments_RowEditing">
                        <Columns>
                            <asp:TemplateField HeaderText="Name">
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="AddNew" OnClick="lbtnAddNew_Click"
                                        Text="Add New" ToolTip="Add New" />                                    
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CssClass="fa fa-edit fa-2x fa-color-Edit space" CommandName="EDIT" CommandArgument='<%#Eval("ID") %>'
                                        Text="" ToolTip="Edit" />
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete space" CommandName="DELETE" CommandArgument='<%#Eval("ID") %>' Text="" ToolTip="Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label10" runat="server" Text="Code"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Code") %>' TabIndex="2" AutoComplete="Off"
                                        CssClass="bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="Name"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblAreaText" runat="server" Text='<%#Eval("Name") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Label8" runat="server" Text="Active"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" runat="server" Text='<%#Eval("Status").ToString()=="1"?"Y":"N" %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdDepartments" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            if (args.get_error() != undefined) {
                args.set_errorHandled(true);
            }
        }
    </script>
</asp:Content>
