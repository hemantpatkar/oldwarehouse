﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="CurrencyExchangeRate" Codebehind="CurrencyExchangeRate.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .ajax__calendar_container { z-index : 5000 ; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <h3>Currency Exchange Rate
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <asp:TabContainer ID="tbcExchangeRate" runat="server" CssClass="Bigtab" ActiveTabIndex="0">
                <asp:TabPanel runat="server" HeaderText="Daily Rate" TabIndex="1" ID="tpDailyRate">
                    <ContentTemplate>
                        <asp:UpdatePanel ID="upExchangeRate" runat="server">
                            <ContentTemplate>
                                <div class="col-lg-12">
                                    <h3>Daily Exchange Rate </h3>
                                    <hr />
                                </div>
                                <div class="row" runat="server" id="divDailyEx">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-2">
                                                        <asp:Label ID="lblFromCurrency" CssClass="bold " runat="server" Text="From Currency"></asp:Label>
                                                        <asp:Label ID="Label2" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RFV_FrmCurrency" runat="server" ControlToValidate="ddlFromCurrency" InitialValue="0" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage="Select" ValidationGroup="SaveRate" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        <asp:DropDownList ID="ddlFromCurrency" runat="server" CssClass="form-control input-sm" TabIndex="1">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <asp:Label ID="lblToCurrency" CssClass="bold " runat="server" Text="To Currency"></asp:Label>
                                                        <asp:Label ID="Label1" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RFV_ToCurrency" runat="server" ControlToValidate="ddlToCurrency" InitialValue="0" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage="Select" ValidationGroup="SaveRate" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        <asp:DropDownList ID="ddlToCurrency" runat="server" CssClass="form-control input-sm" TabIndex="1">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <asp:Label ID="lblExchangeRate" runat="server" CssClass="bold" Text="Exchange Rate"></asp:Label>
                                                        <asp:Label ID="Label8" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RFV_Rate" runat="server" ControlToValidate="txtExchangeRate" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage="" ValidationGroup="SaveRate" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtExchangeRate" runat="server" TabIndex="1" MaxLength="13" ValidationGroup="SaveRate" CssClass="form-control input-sm" onkeypress="return validateFloatKeyPress(this,event)"></asp:TextBox>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <asp:Label ID="Label12" runat="server" Text="From Date" CssClass="bold"></asp:Label>
                                                        <asp:Label ID="Label13" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtFromDate" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage="Select Date" ValidationGroup="SaveRate" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtFromDate" TabIndex="1" runat="server" CssClass="form-control input-sm" ValidationGroup="SaveRate"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server"
                                                            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFromDate" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="">
                                                        </asp:MaskedEditExtender>
                                                        <asp:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtFromDt"
                                                            TargetControlID="txtFromDate" Enabled="True">
                                                        </asp:CalendarExtender>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <asp:Label ID="lblEffToDate" runat="server" Text="To Date" CssClass="bold"></asp:Label>
                                                        <asp:Label ID="Label14" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEffToDate" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage="Select Date" ValidationGroup="SaveRate" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtEffToDate" TabIndex="1" runat="server" CssClass="form-control input-sm" ValidationGroup="SaveRate"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <asp:MaskedEditExtender ID="txtEffToDate_MaskedEditExtender" runat="server"
                                                            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtEffToDate"
                                                            CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder=""
                                                            CultureThousandsPlaceholder="" CultureTimePlaceholder="">
                                                        </asp:MaskedEditExtender>
                                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd-MM-yyyy" PopupButtonID="txtEffToDt"
                                                            TargetControlID="txtEffToDate" Enabled="True">
                                                        </asp:CalendarExtender>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="btn-group pull-right">
                                                        <asp:Button runat="server" ID="btnSaveRate" Text="Save" TabIndex="1" CssClass="btn btn-sm btn-default" ValidationGroup="SaveRate" OnClick="btnSaveRate_Click" />
                                                        <asp:HiddenField ID="hdfDailyExId" runat="server" Value="" />
                                                        <asp:Button runat="server" ID="btnCancel" Text="Reset" TabIndex="1" CssClass="btn btn-default btn-sm" OnClick="btnCancel_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpDaily" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <asp:GridView ID="grdDailyExchangeRates" runat="server"
                                            AutoGenerateColumns="False" GridLines="Horizontal" OnPreRender="grdDailyExchangeRates_PreRender" OnRowEditing="grdDailyExchangeRates_RowEditing" OnRowCommand="grdDailyExchangeRates_RowCommand"
                                            CssClass="table table-striped table-hover table-condensed  text-nowrap">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Name">
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="Insert" OnClick="lbtnAddNew_Click"
                                                            Text="Add New" ToolTip="Add New" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnEdit" runat="server" CssClass="fa fa-edit fa-2x fa-color-Edit space" CommandName="EDIT" CommandArgument='<%#Eval("ID") %>'
                                                            Text="" ToolTip="Edit" />
                                                        <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete space" CommandName="DELETE" CommandArgument='<%#Eval("ID") %>' Text="" ToolTip="Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblHeaderItem" Text="From Currency" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFromCurrency" runat="server" Text='<%# Eval("From_CurrencyType.Name") %>' TabIndex="2" AutoComplete="Off"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblHeaderItemQty" Text="To Currency" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblToCurrency" runat="server" TabIndex="2" Text='<%#Eval("To_CurrencyType.Name") %>' AutoComplete="Off"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblRate" Text="Exchange Rate" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExchangeRate" runat="server" TabIndex="2" Text='<%#Eval("Rate") %>' AutoComplete="Off"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="grdDailyExchangeRates" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" HeaderText="Monthly Rate" TabIndex="1" ID="tpMonthlyRate">
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="col-lg-12">
                                    <h3>Monthly Exchange Rate </h3>
                                    <hr />
                                </div>
                                <div class="row" runat="server" id="divMonthlyEx">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <div class="col-lg-2">
                                                <asp:Label ID="Label3" CssClass="bold " runat="server" Text="From Currency"></asp:Label>
                                                <asp:Label ID="Label4" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlMonthlyFrmCurrency" InitialValue="0" CssClass="text-danger"
                                                    Display="Dynamic" ErrorMessage="Select" ValidationGroup="SaveMonthlyRate" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlMonthlyFrmCurrency" TabIndex="1" runat="server" CssClass="form-control input-sm">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-lg-2">
                                                <asp:Label ID="Label5" CssClass="bold " runat="server" Text="To Currency"></asp:Label>
                                                <asp:Label ID="Label6" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlMonthlyToCurrency" InitialValue="0" CssClass="text-danger"
                                                    Display="Dynamic" ErrorMessage="Select" ValidationGroup="SaveMonthlyRate" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <asp:DropDownList ID="ddlMonthlyToCurrency" TabIndex="1" runat="server" CssClass="form-control input-sm">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-lg-2">
                                                <asp:Label ID="Label7" runat="server" CssClass="bold" Text="Exchange Rate"></asp:Label>
                                                <asp:Label ID="Label9" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtRate" CssClass="text-danger"
                                                    Display="Dynamic" ErrorMessage="Enter Value" ValidationGroup="SaveMonthlyRate" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtRate" Display="Dynamic" ErrorMessage="Enter valid rate" SetFocusOnError="true" ValidationExpression="" ValidationGroup="SaveMonthlyRate"></asp:RegularExpressionValidator>--%>
                                                <asp:TextBox ID="txtRate" runat="server" TabIndex="1" MaxLength="13" ValidationGroup="SaveMonthlyRate" CssClass="form-control input-sm" onkeypress="return validateFloatKeyPress(this, event)"></asp:TextBox>
                                            </div>
                                            <div class="col-lg-2">
                                                <asp:Label ID="Label10" runat="server" Text="To Date" CssClass="bold"></asp:Label>
                                                <asp:Label ID="Label11" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtEffectiveToDate" CssClass="text-danger"
                                                    Display="Dynamic" ErrorMessage="Select Date" ValidationGroup="SaveMonthlyRate" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtEffectiveToDate" TabIndex="1" runat="server" CssClass="form-control input-sm" ValidationGroup="SaveMonthlyRate"></asp:TextBox>
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                </div>
                                                <%--   <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" 
                                                Enabled="True" Mask="999/99" MaskType="None" TargetControlID="txtEffectiveToDate" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="">
                                            </asp:MaskedEditExtender>--%>
                                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="MMM-yy" PopupButtonID="txtEffectiveToDt" DefaultView="Months"
                                                    TargetControlID="txtEffectiveToDate" Enabled="True">
                                                </asp:CalendarExtender>
                                            </div>
                                            <div class="col-lg-1">
                                            </div>
                                            <div class="col-lg-1">
                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="btn-group pull-right">
                                                        <asp:Button runat="server" ID="btnSaveMonthlyRate" TabIndex="1" Text="Save" CssClass="btn btn-default btn-sm" ValidationGroup="SaveMonthlyRate" OnClick="btnSaveMonthlyRate_Click" />
                                                        <asp:HiddenField ID="hdfMonthlyExId" runat="server" Value="" />
                                                        <asp:Button runat="server" ID="btnResetMonthly" TabIndex="1" Text="Reset" CssClass="btn btn-default btn-sm" OnClick="btnResetMonthly_Click" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <asp:GridView ID="grdMonthlyRates" runat="server"
                                            AutoGenerateColumns="False" GridLines="Horizontal" OnPreRender="grdMonthlyRates_PreRender" OnRowEditing="grdMonthlyRates_RowEditing" OnRowCommand="grdMonthlyRates_RowCommand"
                                            CssClass="table table-striped table-hover table-condensed  text-nowrap">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Name">
                                                    <HeaderTemplate>
                                                        <asp:LinkButton ID="lbtnAddMonthlyRate" runat="server" CssClass="btn btn-primary btn-xs" CommandName="Insert" OnClick="lbtnAddMonthlyRate_Click"
                                                            Text="Add New" ToolTip="Add New" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnEditMonthlyRate" runat="server" CssClass="fa fa-edit fa-2x fa-color-Edit space" CommandName="EDIT" CommandArgument='<%#Eval("ID") %>'
                                                            Text="" ToolTip="Edit" />
                                                        <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete space" CommandName="DELETE" CommandArgument='<%#Eval("ID") %>' Text="" ToolTip="Delete" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblHeaderItem" Text="From Currency" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblfrmcurrency" runat="server" Text='<%# Eval("From_CurrencyType.Name") %>' TabIndex="2" AutoComplete="Off"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblHeaderItemQty" Text="To Currency" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtgrdItemQty" runat="server" TabIndex="2" Text='<%#Eval("To_CurrencyType.Name") %>' AutoComplete="Off"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblHeaderDiscount" Text="Rate" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtDiscountPer" runat="server" TabIndex="2" Text='<%#Eval("Rate") %>' AutoComplete="Off"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="grdMonthlyRates" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </asp:TabPanel>
            </asp:TabContainer>
        </div>
    </div>
    <script type="text/javascript">
        var DailyGridID = $get('<%=grdDailyExchangeRates.ClientID%>');
        var MonthlyGridID = $get('<%=grdMonthlyRates.ClientID%>');
        $(document).ready(function () {
            //GridUI(DailyGridID, 50);
            //GridUI(MonthlyGridID, 50);
        })
        function validateFloatKeyPress(el, evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57)) {
                return false;
            }
            if (charCode == 46 && el.value.indexOf(".") !== -1) {
                return false;
            }
            return true;
        }
        function checkIfNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if (key < 48 || key > 57) {
                return false;
            }
            else return true;
        };
    </script>
</asp:Content>
