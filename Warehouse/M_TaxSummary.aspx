﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="M_TaxSummary" Codebehind="M_TaxSummary.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <h3>Tax Summary </h3>
                    <hr />
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:Label ID="lblTaxCode" runat="server" CssClass="bold " Text="Tax Code"></asp:Label>
                            <asp:TextBox ID="txtTaxCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblTaxName" runat="server" CssClass="bold " Text="Tax Name"></asp:Label>
                            <asp:TextBox ID="txtTaxName" TabIndex="1" runat="server" AutoCompleteType="None" AutoComplete="Off"
                                CssClass=" form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">                        
                            <div class="col-lg-2">
                                <asp:Button runat="server" Text="Search" CssClass="btn btn-sm btn-block btn-primary" ID="Search" OnClick="Search_Click">                                    
                                </asp:Button>
                            </div>
                            <div class="col-lg-2">
                                <asp:Button runat="server" ID="lnkNewTax" Text="New Tax" CssClass="btn  btn-sm btn-block btn-primary" OnClick="lnkNewTax_Click"> 
                                </asp:Button>
                            </div>                            
                        </div>                     
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-body">
                <asp:GridView ID="grdTaxSummary" runat="server"  AutoGenerateColumns="False" Width="100%" 
                    OnPreRender="grdTaxSummary_PreRender" GridLines="Horizontal" OnRowDeleting="grdTaxSummary_RowDeleting"
                    CssClass="table table-striped table-hover  text-nowrap  dt-responsive nowrap">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblSelect" Text="Delete" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                 <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete" CommandName="DELETE" CommandArgument='<%#Eval("ID") %>' Text="" ToolTip="Delete" />
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblTaxCode" Text="Tax Code" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink TabIndex="1" CausesValidation="false" ID="lbtnTaxCode" runat="server"
                                    Text='<%# Eval("Code")%>' ToolTip="History" NavigateUrl='<% #"TaxConfigurationDetails.aspx?TaxID=" + Eval("ID")%>' Target="_blank"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrTaxName" Text="Name" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("Name") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrType" Text="Type" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>                                
                                <asp:Label ID="lblType" runat="server" Text='<%#Eval("Type").ToString()=="1"?"Output":"Input" %>' CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrFromDt" Text="From Date" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate> 
                                <asp:Label ID="lblFromDt" runat="server" TabIndex="2" Text='<%#DateTime.Parse(Eval("FromDate").ToString()).ToString("d") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrToDt" Text="To Date" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblToDt" runat="server" TabIndex="2" Text='<%#DateTime.Parse(Eval("ToDate").ToString()).ToString("d") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdTaxSummary.ClientID%>');
        $(document).ready(function () {
            GridUI(GridID, 50);
        })    
    </script>
</asp:Content>
