﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Home : BigSunPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //int  intRequestingUserID = Convert.ToInt32(System.Web.SessionState.HttpSessionState.["UserIdConst"]);
    }


    [WebMethod]
    public static List<object> GetBarChart()
    {
        List<object> iData = new List<object>();
        List<string> labels = new List<string>();
        List<decimal> lst_Inward = new List<decimal>();
        List<decimal> lst_Outward = new List<decimal>();
        List<string> Color = new List<string>();

        //decimal TotalYearlyRevenue = 0;

        for (var month = 1; month <= 12; month++)
        {
            decimal TotalInward = month + 10;// AppItemAllocationColl.GetBookingCount(0, 0, DateTime.Now.Year, month, 0, true);         
            decimal TotalOutward = month + 20;// AppItemAllocationColl.GetTotalMonthlyRevenue(DateTime.Now.Year, month, 0, true);

            string color = String.Format("#{0:X6}", new Random().Next(0x1000000));
            labels.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(month));
            lst_Inward.Add(TotalInward);
            lst_Outward.Add(TotalOutward);
            Color.Add(color);
        }

        iData.Add(labels);
        iData.Add(lst_Inward);
        iData.Add(lst_Outward);
        iData.Add(Color);

        return iData;
    }


}