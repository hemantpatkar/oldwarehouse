﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using System.IO;
using System.Drawing;

public partial class TranshipmentSearch : BigSunPage
{
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Transhipment Search";
        if (!IsPostBack)
        {
        }
    }
    #endregion

    #region Button Event
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("TranshipmentDetail.aspx");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        AppTranshipmentColl objTranshipmentColl = new AppTranshipmentColl(intRequestingUserID);

        DateTime fromdate;
        if (txtTransferDate.Text.Trim() != "" && txtTransferDate.Text.Trim() != "__/__/____")
        {
            fromdate = Convert.ToDateTime(txtTransferDate.Text);
            objTranshipmentColl.AddCriteria(Transhipment.TranshipmentDate, Operators.GreaterOrEqualTo, fromdate);
        }
        if (txtCustomer.Text.Trim() != "")
        {
            objTranshipmentColl.AddCriteria(Transhipment.Customer_Company_ID, Operators.StartWith, txtCustomer.Text);
        }
        if (txtTranshipmentNo.Text.Trim() != "")
        {
            objTranshipmentColl.AddCriteria(Transhipment.TranshipmentNo, Operators.Equals, txtTranshipmentNo.Text);
        }
        if (txtReceivedBy.Text.Trim() != "")
        {
            objTranshipmentColl.AddCriteria(Transhipment.ReceivedBy_CompanyContact_ID, Operators.Equals, txtReceivedBy.Text);
        }
        if (ddlStatus.SelectedValue != "-1")
        {
            int status = Convert.ToInt32(ddlStatus.SelectedValue);
            objTranshipmentColl.AddCriteria(Transhipment.Status, Operators.Equals, status); //
        }

        objTranshipmentColl.Search();
        //objAppGateEntryColl.Sort(AppPreInward.ComparisionDesc);
        grdTranshipment.DataSource = objTranshipmentColl;
        grdTranshipment.DataBind();
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (grdTranshipment.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Doesnot contain data" + "','2'" + ");", true);
            return;
        }

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grdTranshipment.AllowPaging = false;
            grdTranshipment.HeaderRow.BackColor = Color.White;
            foreach (TableCell cell in grdTranshipment.HeaderRow.Cells)
            {
                cell.BackColor = grdTranshipment.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in grdTranshipment.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = grdTranshipment.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = grdTranshipment.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            grdTranshipment.RenderControl(hw);
            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    #endregion

    #region Grid
    protected void grdTranshipment_PreRender(object sender, EventArgs e)
    {

    }
    #endregion
}