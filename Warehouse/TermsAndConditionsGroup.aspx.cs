﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;

public partial class TermsAndConditionsGroup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnAddGroup_Click(object sender, EventArgs e)
    {
        AppTermsAndConditions objTermsAndConditions = new AppTermsAndConditions(1);
        if (txtTnCGroupName.Text.Trim() != "")
        {
            AppTermsAndConditionsColl objTermsAndConditionsColl = new AppTermsAndConditionsColl(1);
            objTermsAndConditionsColl.Search();
            if (!objTermsAndConditionsColl.Any(x => x.Statement.ToUpper() == txtTnCGroupName.Text.ToUpper()))
            {
                objTermsAndConditions.Statement = txtTnCGroupName.Text;
                objTermsAndConditions.Parent_ID = 0;
                objTermsAndConditions.ModifiedOn = DateTime.Now;
                objTermsAndConditions.Status = new AppConvert((int)RecordStatus.Active);
                objTermsAndConditions.Save();
            }
        }
        else
        {
            return;
        }

        txtTnCGroupName.Text = "";
    }
}