﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" Culture="en-gb" AutoEventWireup="true" Inherits="StockOrder" Codebehind="StockOrder.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        function checkedAllSearch() {
            var but = document.getElementById("SelectAll");
            var TargetBaseControl = document.getElementById('<%= this.gvSearch.ClientID %>');
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var icount = 0; icount < Inputs.length; ++icount) {
                if (Inputs[icount].type == 'checkbox') {
                    if (Inputs[icount].disabled == false) {
                        if (but.checked == true) {
                            Inputs[icount].checked = true;
                        }
                        else {

                            Inputs[icount].checked = false;
                        }
                    }
                    else {
                        Inputs[icount].checked = false;
                    }
                }
            }
        }

        function PartnerGroupSearch(sender, eventArgs) {

            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());
            var hdpartID = $get('<%= UsrhdfPartnerGroup.ClientID %>');
            var lblPartname = $get('<%= UsrlblPartnerGroup.ClientID %>');
            var partycode = $get('<%= UsrtxtPartnerGroupCode.ClientID %>');


            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=UsrtxtPartnerCode_AutoCompleteExtender.ClientID %>').set_contextKey("3," + eventArgs.get_value());
            }
        }


        function ClearPartnerGroupSearch() {
            var OrignalProfitCenter = $get('<%=UsrtxtPartnerGroupCode.ClientID %>');
            var HidenProfitCenter = $get('<%= UsrlblPartnerGroup.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=UsrhdfPartnerGroup.ClientID %>').value = "";
                $get('<%=UsrlblPartnerGroup.ClientID %>').value = "";
                $get('<%=UsrtxtPartnerGroupCode.ClientID %>').value = "";
            }
        }


        function PartnerSearch(sender, eventArgs) {
            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());
            var hdpartID = $get('<%= UsrhdfPartner.ClientID %>');
            var lblPartname = $get('<%= UsrlblPartner.ClientID %>');
            var partycode = $get('<%= UsrtxtPartnerCode.ClientID %>');
            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            lblPartname.defaultvalue = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=UsrtxtPartnerGroupCode_AutoCompleteExtender.ClientID %>').set_contextKey(eventArgs.get_value());
            }

        }

        function ClearPartnerSearch() {
            var OrignalProfitCenter = $get('<%=UsrtxtPartnerCode.ClientID %>');
            var HidenProfitCenter = $get('<%= UsrlblPartner.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=UsrlblPartner.ClientID %>').value = "";
                $get('<%=UsrtxtPartnerCode.ClientID %>').value = "";
                $get('<%=UsrhdfPartner.ClientID %>').value = "";
            }
        }


        function GridclickButton(e, buttonid, ControlID) {

            var ArrayIDs = ControlID.id.split('_');
            var ContentPlaceHolderID = ArrayIDs[0]
            var gridVIew = ArrayIDs[1]
            var RowIndex = ArrayIDs[3]
            var evt = (e || window.event);
            var keyCode = (evt.keyCode || evt.which);
            var bt = document.getElementById(ContentPlaceHolderID + "_" + gridVIew + "_" + buttonid + "_" + RowIndex);
            if (bt) {
                if (keyCode == 13) {
                    bt.click();
                    return false;
                }
            }

        }

        function clickButton(e, buttonid) {

            var evt = (e || window.event);
            var keyCode = (evt.keyCode || evt.which);
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }

        function Count() {

            var cou = 0;
            var grid = document.getElementById("<%= gvDescription.ClientID %>");
            var total = document.getElementById("<%= lblTotalQuantityToShow.ClientID %>");
            if (grid != null) {
                for (var i = 1; i < grid.rows.length - 1; i++) {
                    var inputs = grid.rows[i].getElementsByTagName('input');
                    if (inputs != null) {
                        if (inputs[4].type == "text" && inputs[4].value != "") {
                            cou = parseInt(cou) + parseInt(inputs[4].value);

                        }
                    }
                }
            }
            total.innerHTML = cou;
        }

        function validateCheckBoxes() {
            var isValid = false;
            var gridView = document.getElementById('<%=gvSearch.ClientID %>');
            if (gridView != null) {
                for (var i = 1; i < gridView.rows.length; i++) {
                    var inputs = gridView.rows[i].getElementsByTagName('input');
                    if (inputs != null) {
                        if (inputs[0].type == "checkbox") {
                            if (inputs[0].checked) {
                                isValid = true;
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            return false;
        }


        function GridShortcutFunctionsClick(e, buttonDetail, buttonLocation, ControlID) {

            var ArrayIDs = ControlID.id.split('_');
            var ContentPlaceHolderID = ArrayIDs[0]
            var gridVIew = ArrayIDs[1]
            var RowIndex = ArrayIDs[3]

            var evt = (e || window.event);
            var keyCode = (evt.keyCode || evt.which);
            var shift = 0;
            var alt = 0;
            var ctrl = 0;
            shift = evt.shiftKey;
            alt = evt.altKey;
            ctrl = evt.ctrlKey;

            var BtnDelivery = document.getElementById('<%=txtDeliverDate.ClientID %>');
            if (BtnDelivery) {
                if (keyCode == 13) {
                    BtnDelivery.focus();
                    Count();
                    return false;
                }
                if (keyCode == 9) {
                    Count();
                    return true;
                }
            }
            var btDetail = document.getElementById(ContentPlaceHolderID + "_" + gridVIew + "_" + buttonDetail + "_" + RowIndex);
            if (btDetail) {
                if (keyCode == 113) {
                    btDetail.click();
                    btDetail.disabled = true;
                    Count();
                    return false;
                }
            }
            var btnLoc = document.getElementById(ContentPlaceHolderID + "_" + gridVIew + "_" + buttonLocation + "_" + RowIndex);
            if (btnLoc) {
                if (keyCode == 114) {
                    btnLoc.click();
                    btnLoc.disabled = true;
                    Count();
                    return false;
                }
            }


        }
        function labour(e, buttonLabour, buttonid) {
            var evt = (e || window.event);
            var keyCode = (evt.keyCode || evt.which);
            var shift = 0;
            var alt = 0;
            var ctrl = 0;
            shift = evt.shiftKey;
            alt = evt.altKey;
            ctrl = evt.ctrlKey;
            var btLabour = document.getElementById(buttonLabour);
            if (btLabour) {
                if (keyCode == 76 && alt == 1) {
                    btLabour.click();
                    btLabour.disabled = true;
                    Count();
                    return false;
                }
            }
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (keyCode == 13) {
                    bt.click();
                    bt.disabled = true;
                    Count();
                    return false;
                }
            }
        }


        function confirmalert(ask) {
            return confirm(ask);
        }



    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
        <ProgressTemplate>
            <div class="modal fade in" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-keyboard="false" style="display: block">
                <div class="modal-dialog  modal-sm">
                    <h1 style="text-align: center;">
                        <i class="fa fa-truck fa-magic fa-2x faa-passing animated"></i>
                    </h1>
                </div>
            </div>
            <div class="modal-backdrop fade in"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <i class="fa fa-shopping-cart"></i>
                        <asp:Label ID="lblMaterialOrder" runat="server"></asp:Label>
                        &nbsp;/&nbsp;
                        <asp:Label ID="txtMONo" ForeColor="Maroon" runat="server"></asp:Label>
                        <div class="pull-right">
                            <asp:LinkButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" TabIndex="19"
                                Visible="False" OnClientClick="return confirmalert('Do you want to Delete ?' )"> <i class="fa fa-trash text-danger"></i>
                            </asp:LinkButton>

                            <asp:LinkButton ID="btnBack" runat="server" CausesValidation="False" OnClick="btnBack_Click"
                                TabIndex="12" Text="">
                                  <i class="fa fa-search"></i>
                            </asp:LinkButton>
                            <asp:HyperLink ID="btnPrint" runat="server" Target="_blank"
                                Visible="False" TabIndex="12"> <i class="fa fa-print"></i> </asp:HyperLink>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row btnrow">
                <div class="col-lg-6">
                    <ul class="nav navbar-nav navbar-left">
                        <li>

                            <asp:HiddenField ID="hdfrevised" runat="server" />
                            <asp:ConfirmButtonExtender ID="cbeDelet" runat="server" ConfirmText="Are you sure?" TargetControlID="btnDelete">
                            </asp:ConfirmButtonExtender>
                        </li>

                        <li>
                            <asp:LinkButton ID="btnSave" runat="server" OnClick="btnSave_Click" TabIndex="11" Text=""
                                ValidationGroup="MaterialOrder" AccessKey="w"></asp:LinkButton><span></span>

                        </li>

                        <li>
                            <asp:LinkButton ID="btnNewDetail" runat="server" Text="" CausesValidation="false" TabIndex="13"
                                OnClick="btnNewDetail_Click" AccessKey="n" />
                        </li>
                        <li></li>
                        <li>
                            <asp:LinkButton ID="btnhistory" runat="server" CausesValidation="False" CssClass="btn btn-primary btn-sm"
                                TabIndex="160" OnClick="btnhistory_Click" Visible="false" />
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <div class="pull-right">
                        <ul class="nav nav-pills nav-wizard">
                            <li class="active"><a href="#" data-toggle="tab">New</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Pending Approval</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Available</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Partially Available</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Done</a></li>

                        </ul>
                    </div>
                </div>
            </div>

            <hr />

            <div class="row">
                <div class="col-lg-12" style="overflow: auto; max-height: 300px;">
                    <asp:GridView ID="gvDescription" ShowHeaderWhenEmpty="true" GridLines="None" ShowHeader="true" runat="server" AutoGenerateColumns="False"
                        OnRowCommand="gvDescription_RowCommand" CssClass="table table-striped table-hover text-nowrap">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDeleteItem" ForeColor="Maroon" OnClientClick="return confirmalert('Do you want to Delete ?' )" runat="server" CommandName="Del" CommandArgument='<%# Eval("ID") %>' CausesValidation="False" TabIndex="1">
                                                <i class="fa fa-close fa-2x"></i>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblParty" Text="Customer Info" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblparty" ForeColor="#0099ff" runat="server" Text='<%#Eval("InwardDetail.Inward.Customer_Company.Name")  %>' ToolTip='<%#Eval("InwardDetail.Inward.Customer_Company.Name") %>'></asp:Label>
                                    <br />
                                    <asp:Label ID="lblPartyGroup" runat="server" Text='<%#Eval("InwardDetail.Inward.Customer_Company.Parent.Name")  %>'
                                        ForeColor="Maroon" ToolTip='<%#Eval("InwardDetail.Inward.Customer_Company.Parent.Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblLotNo" Text="LotNo" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="btnDetail" CommandArgument='<%# Eval("InwardDetail_ID")%>' Text='<%#Eval("LotInternal") %>' ToolTip='<%#Eval("LotInternal")%>' CommandName="Detail" />
                                    <asp:Label ID="lblLotVersion" runat="server" Text='<%#Eval("LotVersion") %>' ToolTip='<%#Eval("LotVersion")%>'></asp:Label>
                                    <asp:HiddenField ID="hdfInwardDetailID" runat="server" Value='<%#Eval("InwardDetail_ID") %>' />
                                    <asp:HiddenField ID="hdfMaterialOrderID" runat="server" Value='<%#Eval("MaterialOrder_ID") %>' />
                                    <asp:HiddenField ID="hdfMaterialOrderDetailID" runat="server" Value='<%#Eval("ID") %>' />
                                    <asp:LinkButton ID="lnkDelLoc" CommandArgument='<%# Eval("InwardDetail_ID")%>' runat="server" CommandName="Location" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblOrderQty" Text="Order Qty" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:RequiredFieldValidator ID="rfvOrderQuant" runat="server" Display="Dynamic" ControlToValidate="txtOrderQuantity"
                                        ValidationGroup="MaterialOrder" SetFocusOnError="true" ErrorMessage="asd"></asp:RequiredFieldValidator>
                                    <asp:TextBox runat="server" ID="txtOrderQuantity" CssClass="form-control input-sm" OnTextChanged="txtOrderQuantity_TextChanged" AutoPostBack="true" onkeydown="GridShortcutFunctionsClick(event,'btnDetail','lnkDelLoc',this)"
                                        Text='<%#Eval("OrderQuantity") %>' ValidationGroup="MaterialOrder" MaxLength="7" TabIndex="3" />
                                    <asp:CompareValidator ID="cmpOrderQuantity" runat="server" ControlToValidate="txtOrderQuantity"
                                        ErrorMessage="net" ControlToCompare="txtNetQuan" ValidationGroup="MaterialOrder" CssClass="text-danger"
                                        Type="Double" Operator="LessThanEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                    <asp:CompareValidator ID="cmpLessThan1" runat="server" ValidationGroup="MaterialOrder"
                                        ErrorMessage="Zero not allowed" ControlToValidate="txtOrderQuantity" ValueToCompare="0" Type="Double" CssClass="text-danger"
                                        Operator="NotEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                    <div style="height: 0px; width: 0px;">
                                        <asp:TextBox ID="txtNetQuan" runat="server" Text='<%#Eval("StockQuantity") %>' Height="0px"
                                            Width="0px" BorderColor="White" BackColor="White" BorderStyle="None" CausesValidation="True"
                                            BorderWidth="0px" ReadOnly="True"></asp:TextBox>
                                    </div>
                                    <asp:FilteredTextBoxExtender ID="ftbOrderQuantity" runat="server" FilterType="Numbers,Custom" ValidChars="."
                                        TargetControlID="txtOrderQuantity">
                                    </asp:FilteredTextBoxExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblNetQty" Text="Net Qty" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblNetQuantity" runat="server" Text='<%#Eval("StockQuantity") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblReceivedQty" Text="Inward Qty" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblReceivedQuantity" runat="server" Text='<%#Eval("InwardQuantity") %>' ToolTip='<%#Eval("InwardQuantity")  %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblItemCategory" Text="Item Info" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblStorageItemName" runat="server" Text='<%#Eval("Item.Name") %>' ForeColor="#3399ff"
                                        ToolTip='<%#Eval("Item.Name")%>'></asp:Label>
                                    <br />
                                    <asp:Label ID="lblItemGroup" runat="server" Text='<%#Eval("Item.CategoryType.Code") %>' ForeColor="Maroon"
                                        ToolTip='<%#Eval("Item.CategoryType.Code")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPackRef" Text="Packing Ref" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPackingReference" Text='<%# Eval("InwardDetail.PackingReference") %>' ToolTip='<%# Eval("InwardDetail.PackingReference") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblMark" Text="Item Mark" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblMark" runat="server" Text='<%# Eval("InwardDetail.ItemMark") %>' ToolTip='<%# Eval("InwardDetail.ItemMark") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblVakkal" Text="Vakkal" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblVakkal" runat="server" Text='<%#Eval("InwardDetail.LotCustomer")  %>' ToolTip='<%#Eval("InwardDetail.LotCustomer") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPalletNo" Text="Pallete No" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPallete" runat="server" Text='<%# Eval("InwardDetail.PalletNo") %>' ToolTip='<%#Eval("InwardDetail.PalletNo")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblScheme" Text="Scheme/Location" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblScheme" runat="server" Text='<%#Eval("InwardDetail.StorageSchemeRate.StorageScheme.Code")  %>' ForeColor="#3399ff"
                                        ToolTip='<%#Eval("InwardDetail.StorageSchemeRate.StorageScheme.Code") %>'></asp:Label>
                                    <br />
                                    <asp:Label ID="lblLocationNo" runat="server" Text='<%#Eval("InwardDetail.StorageLocation.Name") %>' ForeColor="Maroon"
                                        ToolTip='<%#Eval("InwardDetail.StorageLocation.Name")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdCount" Text="Variety/Count" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblVariety" Text='<%# Eval("InwardDetail.Variety.Name") %>' ToolTip='<%# Eval("InwardDetail.Variety.Name") %>' runat="server" />
                                    <br />
                                    <asp:Label ID="lblCount" Text='<%# Eval("InwardDetail.Count") %>' ToolTip='<%# Eval("InwardDetail.Count") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdBrand" Text="Brand/Origin" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblBrand" Text='<%# Eval("InwardDetail.Brand.Name") %>' ToolTip='<%# Eval("InwardDetail.Brand.Name") %>' runat="server" />
                                    <br />
                                    <asp:Label ID="lblorigin" Text='<%# Eval("InwardDetail.Origin.Name") %>' ToolTip='<%# Eval("InwardDetail.Origin.Name") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>



                        </Columns>
                    </asp:GridView>
                </div>
            </div>

            <div class="panel" runat="server" id="searchlot" style="display: none">
                <div class="panel-heading btn-primary">
                    <div class="row">
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtLotSearch" placeholder="Lot No" CssClass="form-control input-sm" MaxLength="10" runat="server"
                                TabIndex="4"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftblot" runat="server" FilterType="Numbers" TargetControlID="txtLotSearch">
                            </asp:FilteredTextBoxExtender>
                        </div>
                        <div class="col-lg-2">

                            <asp:TextBox ID="UsrtxtPartnerGroupCode" onblur="ClearPartnerGroupSearch()" TabIndex="1" placeholder="Customer Group"
                                runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="UsrtxtPartnerGroupCode_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerGroupSearch"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" ContextKey="4" UseContextKey="true"
                                TargetControlID="UsrtxtPartnerGroupCode">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:HiddenField ID="UsrhdfPartnerGroup" runat="server" />
                                <asp:Label ID="UsrlblPartnerGroup" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-lg-2">

                            <asp:TextBox ID="UsrtxtPartnerCode" onblur="ClearPartnerSearch()" TabIndex="2" runat="server" placeholder="Customer"
                                AccessKey="p" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="UsrtxtPartnerCode_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerSearch"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                UseContextKey="true" ContextKey="3" TargetControlID="UsrtxtPartnerCode">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:HiddenField ID="UsrhdfPartner" runat="server" />
                                <asp:HiddenField ID="UserhdfPartner1" runat="server" />
                                <asp:Label ID="UsrlblPartner" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtReceivedQuantitySearch" TabIndex="4" MaxLength="20" placeholder="Received Quantity" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtItemGroup" TabIndex="4" MaxLength="20" placeholder="Item Group"
                                runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtItemGroup_AutoCompleteExtender" runat="server" EnableCaching="false"
                                TargetControlID="txtItemGroup" CompletionInterval="1" CompletionSetCount="10"
                                UseContextKey="true" ContextKey="0" DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1"
                                ServiceMethod="ItemCategory" FirstRowSelected="false" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" CompletionListElementID="divwidth"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListCssClass="AutoExtender">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtStorageItemName" TabIndex="4" MaxLength="20" placeholder="Item Name"
                                runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtLocSearch_AutoCompleteExtender" runat="server" EnableCaching="false"
                                TargetControlID="txtStorageItemName" CompletionInterval="1" CompletionSetCount="10" ContextKey="0" UseContextKey="true"
                                DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="ItemSearch"
                                FirstRowSelected="false" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListCssClass="AutoExtender">
                            </asp:AutoCompleteExtender>
                        </div>
                    </div>
                    <div class="newline">
                    </div>
                    <div class="row">

                        <div class="col-lg-2">
                            <asp:TextBox ID="txtpackingrefSearch" placeholder="Packing Reference" TabIndex="4" MaxLength="20" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtMarkSearch" placeholder="Item Mark" TabIndex="4" MaxLength="20" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtVakkalSearch" placeholder="Vakkal" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtSchemeSearch" placeholder="Scheme" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtBrand" placeholder="Brand" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtOrigin" placeholder="Origin" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>

                    </div>
                    <div class="newline">
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <asp:Button ID="btnLotSearch" runat="server" OnClick="btnLotSearch_Click" TabIndex="19" CssClass="btn btn-primary btn-sm btn-block" Text="Search" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Button ID="btnAddItems" runat="server" CausesValidation="true" ValidationGroup="OrderQty" CssClass="btn btn-primary btn-sm btn-block"
                                OnClick="btnAddItems_Click" TabIndex="19" Text="" />
                        </div>
                        <div class="col-lg-2">
                            <label class="btn btn-primary btn-sm  btn-block" id="divIssue" runat="server">
                                <asp:CheckBox ID="chkZero" runat="server" TabIndex="2" />
                                <asp:Label ID="lblchkZero" runat="server" CssClass="bold" />
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <label class="btn btn-primary btn-sm  btn-block" id="divNewRequirement" runat="server">
                                <asp:CheckBox ID="chkFyear" runat="server" TabIndex="2" />
                                <asp:Label ID="lblchkFyear" runat="server" CssClass="bold" />
                            </label>
                        </div>

                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-lg-12" style="overflow: auto">
                            <asp:GridView ID="gvSearch" runat="server" AutoGenerateColumns="False" PagerStyle-CssClass="pagination-ys"
                                OnRowCommand="gvSearch_RowCommand" AllowPaging="True" OnPageIndexChanging="gvSearch_PageIndexChanging"
                                CssClass="table table-hover  table-striped text-nowrap" PageSize="30">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <input id="SelectAll" onclick="checkedAllSearch();" type="checkbox" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="CheckBoxSelect" name="CheckBoxSelect" onkeypress="clickButton(event,'<%# ((GridViewRow)Container).FindControl("lnkAdditem").ClientID %>')" type="checkbox" tabindex="4" value='<%#Eval("InwardDetail_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblorderQty" Text="Order Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CompareValidator ID="cmpOrderQuantity" runat="server" ControlToValidate="txtOrderQtySearch"
                                                ErrorMessage="" ControlToCompare="txtNetQuantity" ValidationGroup="OrderQty" CssClass="text-danger"
                                                Type="Double" Operator="LessThanEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                            <asp:CompareValidator ID="cmpLessThan1" runat="server" ValidationGroup="OrderQty"
                                                ErrorMessage="" ControlToValidate="txtOrderQtySearch" ValueToCompare="0" Type="Double" CssClass="text-danger"
                                                Operator="NotEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                            <asp:CompareValidator ID="cmpgrthan" runat="server" ValidationGroup="OrderQty"
                                                ErrorMessage="" ControlToValidate="txtOrderQtySearch" ValueToCompare="0" Type="Double" CssClass="text-danger"
                                                Operator="GreaterThanEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                            <asp:TextBox ID="txtOrderQtySearch" ValidationGroup="OrderQty" TabIndex="5" CausesValidation="true" CssClass="form-control input-sm" Text="" onkeypress="return GridclickButton(event,'lnkAdditem',this)" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblLotNo" Text="LotNo" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div style="display: none;">
                                                <asp:LinkButton ID="lnkAdditem" ValidationGroup="OrderQty" runat="server" CommandName="Sel" CommandArgument='<%#Eval("InwardDetail_ID") %>' />
                                                <asp:HiddenField ID="hdfInwardDetailID" runat="server" Value='<%#Eval("InwardDetail_ID") %>' />
                                                <asp:HiddenField ID="hdfMaterialOrderID" runat="server" Value="" />
                                                <asp:TextBox ID="txtNetQuantity" runat="server" Text='<% #Eval("NetQuantity")%>'></asp:TextBox>
                                            </div>
                                            <asp:LinkButton ID="lblLotNo" Text='<%#Eval("LotInternal") %>' CommandArgument='<%# Eval("InwardDetail_ID")%>'
                                                CommandName="Detail" ToolTip='<%#Eval("LotInternal")%>' runat="server"></asp:LinkButton>
                                            <asp:LinkButton ID="lblLotVersion" Text='<%#Eval("LotVersion") %>' CommandArgument='<%# Eval("InwardDetail_ID")%>'
                                                CommandName="Detail" ToolTip='<%#Eval("LotVersion")%>' runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblParty" Text="Customer Info" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkpartner" Text='<%#Eval("CustomerName") %>' CommandArgument='<%# Eval("Customer_Company_ID")%>'
                                                CommandName="Customer" ToolTip='<%#Eval("CustomerName")%>' runat="server"></asp:LinkButton>
                                            <br />
                                            <asp:Label ID="lblPartyGroup" runat="server" Text='<%#Eval("CustomerGroupName")  %>'
                                                ForeColor="Maroon" ToolTip='<%#Eval("CustomerGroupName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblItem" Text="Item Info" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItem" runat="server" Text='<%#Eval("ItemName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblMark" Text="Item Mark" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMark" runat="server" Text='<%# Eval("ItemMark") %>' ToolTip='<%# Eval("ItemMark") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblPackingRef" Text="Packing Ref" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPackingReference" Text='<%# Eval("PackingReference") %>' ToolTip='<%# Eval("PackingReference") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblVakkal" Text="Vakkal" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVakkal" runat="server" Text='<%#Eval("LotCustomer")  %>' ToolTip='<%#Eval("LotCustomer") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblPalleteNo" Text="Pallete No" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPallete" runat="server" Text='<%# Eval("PalletNo") %>' ToolTip='<%#Eval("PalletNo")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblStockQty" Text="Stock Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStockQuantity" runat="server" Text='<%#Eval("StockQuantity") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblOrderQty" Text="Order Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrderQuantity" runat="server" Text='<%#Eval("OrderQuantity") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblNetQty" Text="Net Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetQuantity" runat="server" Text='<% #Eval("NetQuantity")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblScheme" Text="Scheme/Location" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblScheme" runat="server" Text='<%#Eval("SchemeCode")  %>' ForeColor="#3399ff"></asp:Label>
                                            <br />
                                            <asp:Label ID="lblLocationNo" runat="server" Text='<%#Eval("LocationName") %>' ForeColor="Maroon"
                                                ToolTip='<%#Eval("LocationName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblhdCount" Text="Variety/Count" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVariety" Text='<%# Eval("VarietyName") %>' ToolTip='<%# Eval("VarietyName") %>' runat="server" />
                                            <br />
                                            <asp:Label ID="lblCount" Text='<%# Eval("Count") %>' ToolTip='<%# Eval("Count") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblhdBrand" Text="Brand/Origin" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBrand" Text='<%# Eval("BrandName") %>' ToolTip='<%# Eval("BrandName") %>' runat="server" />
                                            <br />
                                            <asp:Label ID="lblorigin" Text='<%# Eval("OriginName") %>' ToolTip='<%# Eval("OriginName") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label ID="lblOrderDate" runat="server" CssClass="bold" />
                            <asp:Label ID="Label4" runat="server" CssClass="text-danger" Text="*" />
                            <div style="display: none">
                                <asp:TextBox ID="txtDateCompare" runat="server"></asp:TextBox>
                            </div>
                            <asp:CompareValidator ID="Cmpdate" runat="server" ErrorMessage=""
                                CssClass="text-danger" ControlToValidate="txtOrderDate" Type="Date" Display="Dynamic"
                                Operator="GreaterThanEqual" ControlToCompare="txtDateCompare" ValidationGroup="MaterialOrder"></asp:CompareValidator>
                            <asp:TextBox ID="txtOrderDate" runat="server" CssClass="form-control input-sm" TabIndex="4" ValidationGroup="MaterialOrder"></asp:TextBox>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder=""
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtOrderDate">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtOrderDate">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="rfvOrderDate" runat="server" ControlToValidate="txtOrderDate"
                                CssClass="text-danger" Display="Dynamic" ErrorMessage="" SetFocusOnError="true"
                                ValidationGroup="MaterialOrder"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="rgvOrderDate" runat="server" ControlToValidate="txtOrderDate"
                                CssClass="text-danger" Display="Dynamic" ErrorMessage="Invalid Date" MaximumValue="01-01-2099"
                                MinimumValue="01-01-1900" Type="Date" ValidationGroup="MaterialOrder"></asp:RangeValidator>
                            <asp:RangeValidator ID="Rangeval" runat="server" Display="Dynamic" Type="Date" ErrorMessage="Invalid Date"
                                ControlToValidate="txtOrderDate" ValidationGroup="MaterialOrder"></asp:RangeValidator>
                            <asp:RangeValidator ID="Rangback" runat="server" Type="Date" ErrorMessage="Invalid Date" Display="Dynamic"
                                SetFocusOnError="true" ControlToValidate="txtOrderDate" ValidationGroup="MaterialOrder"></asp:RangeValidator>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblDeliverDate" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtDeliverDate" runat="server" CssClass="form-control input-sm" TabIndex="4"
                                ValidationGroup="MaterialOrder"></asp:TextBox>
                            <asp:RangeValidator ID="rgvDeliverDate0" runat="server" ControlToValidate="txtDeliverDate"
                                CssClass="text-danger" Display="Dynamic" ErrorMessage="Invalid Date" MaximumValue="01-01-2099"
                                MinimumValue="01-01-1900" Type="Date" ValidationGroup="MaterialOrder"></asp:RangeValidator>
                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" CultureAMPMPlaceholder=""
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtDeliverDate">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" TargetControlID="txtDeliverDate">
                            </asp:CalendarExtender>
                            <asp:CompareValidator ID="cmpdeliverdate" runat="server" ErrorMessage="" CssClass="text-danger"
                                ControlToValidate="txtDeliverDate" Type="Date" Display="Dynamic" Operator="GreaterThanEqual"
                                ControlToCompare="txtOrderDate" ValidationGroup="MaterialOrder"></asp:CompareValidator>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblTotalQuantity" runat="server" CssClass="bold" />
                            <asp:Label ID="lblTotalQuantityToShow" runat="server" TabIndex="66" CssClass="form-control input-sm"></asp:Label>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblTotalPallete" runat="server" CssClass="bold" />
                            <asp:Label ID="txtTotalPallete" runat="server" MaxLength="9" ValidationGroup="MaterialOrder" CssClass="form-control input-sm"></asp:Label>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblDeliverTo" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtDeliverTo" runat="server" TabIndex="4" ValidationGroup="MaterialOrder" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RFVtxtDeliverTo" runat="server" ControlToValidate="txtDeliverTo" Display="Dynamic" SetFocusOnError="true" ErrorMessage="" CssClass="text-danger bold" ValidationGroup="MaterialOrder"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblTransporter" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtTransporter" runat="server" MaxLength="100" TabIndex="4" ValidationGroup="MaterialOrder" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblOrderBy" runat="server" CssClass="bold" />
                            <asp:Label ID="Label5" runat="server" CssClass="text-danger" Text="*" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtOrderBy"
                                Display="Dynamic" SetFocusOnError="true" CssClass="text-danger bold"
                                ErrorMessage="" ValidationGroup="MaterialOrder"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtOrderBy" runat="server" TabIndex="4" ValidationGroup="MaterialOrder" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtOrderBy_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" ServiceMethod="MOrderBy"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtOrderBy">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class="col-lg-10">
                            <asp:Label ID="lblRemarks" runat="server" CssClass="bold" />
                            <asp:TextBox ID="txtRemarks" runat="server" MaxLength="500" TabIndex="9" ValidationGroup="MaterialOrder" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-1">
                    <asp:HiddenField ID="hdfMOID" runat="server" />
                    <asp:HiddenField ID="tempPtID" runat="server" />
                </div>

            </div>

            <div runat="server" id="bottem">
                <div class="row">
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-3">

                        <asp:Label ID="lbleduser" Text="" runat="server" />
                        <asp:Label ID="lbledbar" runat="server" Text=" |"></asp:Label>
                        <asp:Label ID="lbleddate" Text="" runat="server" />

                    </div>

                    <div class="col-lg-3">
                        <asp:Label ID="lblshowshotcut" runat="server" CssClass="text-danger" />
                        <asp:CollapsiblePanelExtender ID="colshotcut" runat="server"
                            CollapseControlID="lblshowshotcut" ExpandControlID="lblshowshotcut"
                            ExpandDirection="Vertical" AutoCollapse="false" AutoExpand="false" Collapsed="true"
                            TargetControlID="Pnlcol">
                        </asp:CollapsiblePanelExtender>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <asp:Label ID="Lblsh" runat="server" Text="Alt+R : Search In MO Detail"></asp:Label>
                    <asp:Label ID="lblb" runat="server" Text=" |"></asp:Label>
                    <asp:Label ID="lblal" runat="server" Text="F2 (In order qty) : Inward,Outward and material order detail of particular LineItem "></asp:Label>
                    <asp:Label ID="Lblbb" runat="server" Text=" |"></asp:Label>
                    <asp:Label ID="lbll" runat="server" Text="Enter (On Detail grid control) : Search In MO Detail"></asp:Label>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>

    <%--ALL Modals--%>

    <div class="modal fade" id="modalOverride" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="width: 90%">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <div class="modal-header">
                    <h1 class="modal-title">
                        <asp:Label ID="lblOutstanding" runat="server"></asp:Label>
                    </h1>
                </div>
                <div class="modal-body" style="overflow-y: auto; height: 500px">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <div class="row" style="display: none;">
                                <div class="col-lg-12">
                                    <asp:HiddenField ID="hdfoverride" runat="server" />
                                    <asp:Button ID="btnovrCancel" runat="server" CausesValidation="false" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12" style="text-align: right">
                                    <asp:Button ID="btnovrOk" runat="server" CausesValidation="false" CssClass="btn btn-primary" OnClick="btnovrOk_Click" />
                                </div>
                            </div>
                            <div class="row">
                                <%--  <div class="col-lg-12">
                                    <uc1:outstandingamt id="Outstanding" runat="server" />
                                </div>--%>
                            </div>
                            <div class="row">
                                <%--<div class="col-lg-12">
                                    <uc2:labouroutstanding id="LabourOutstanding" runat="server" />
                                </div>--%>
                            </div>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalInstruction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <div class="modal-header">
                    <h1 class="modal-title">
                        <asp:Label ID="lblinsthead" runat="server"></asp:Label>
                    </h1>
                </div>
                <div class="modal-body" style="overflow-y: auto; height: 500px">
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlInstruction" runat="server">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <asp:Button ID="btnCancel" runat="server" CausesValidation="false" Visible="false" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <asp:Label ID="lblGroupInst" runat="server"></asp:Label>
                                        <div class="newline"></div>
                                        <asp:Label ID="GroupInst" runat="server" CssClass="form-control bold"></asp:Label>
                                    </div>
                                    <div class="col-lg-6">
                                        <asp:Label ID="lblPartyInst" runat="server"></asp:Label>
                                        <div class="newline"></div>
                                        <asp:Label ID="PartyInst" runat="server" CssClass="form-control bold"></asp:Label>
                                        <asp:HiddenField ID="HDfInwardIDs" runat="server" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <asp:Label ID="lblOverRiddenRemark" runat="server"></asp:Label>
                                        <asp:TextBox ID="txtOverRiddenRemark" runat="server" ValidationGroup="OverRide" CssClass="form-control input-sm"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" Display="Dynamic" SetFocusOnError="true"
                                            ValidationGroup="OverRide" ControlToValidate="txtOverRiddenRemark"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-lg-6">
                                        <asp:Label ID="lblOverRiddenBy" runat="server"></asp:Label>
                                        <asp:DropDownList ID="ddlOverRiddenBy" runat="server" ValidationGroup="OverRide" CssClass="form-control dropdown">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage=""
                                            SetFocusOnError="true" CssClass="text-danger"
                                            ValidationGroup="OverRide" InitialValue="0" Display="Dynamic" ControlToValidate="ddlOverRiddenBy"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="col-lg-1" style="display: none;">
                                        <asp:Label ID="lblOverRiddenMsg" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="newline"></div>
                                <div class="row">
                                    <div class="col-lg-12" style="overflow: auto;">
                                        <asp:GridView ID="GrdOverRidden" runat="server" AutoGenerateColumns="False" EmptyDataText=""
                                            CssClass="table table-hover table-striped text-nowrap" OnPreRender="GrdOverRidden_PreRender">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblMaterialOrderNo" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMONo" Text='<%#Eval("MaterialOrderNo") %>' ToolTip='<%#Eval("MaterialOrderNo") %>'
                                                            runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOverRiddenDate" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDeliveryDate" Text='<%#Convert.ToString(Eval("OverRiddenDate"))=="19000101"?"": Eval("OverRiddenDate") %>'
                                                            ToolTip='<%#Eval("OverRiddenDate") %>'
                                                            runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOverRiddenBy" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDeliverTo" Text='<%#Eval("UserInfo3.UserNamewithCode") %>' ToolTip='<%#Eval("UserInfo3.UserNamewithCode") %>'
                                                            runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblOverRiddenRemarks" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTransporter" Text='<%#Eval("OverRiddenRemark") %>' ToolTip='<%#Eval("OverRiddenRemark") %>'
                                                            runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="newline"></div>
                                <div class="row">
                                    <%-- <div class="col-lg-12" style="overflow: auto">
                                        <uc1:outstandingamt id="OutstandingAmt1" runat="server" />
                                    </div>--%>
                                </div>
                                <div class="row">
                                    <%-- <div class="col-lg-12" style="overflow: auto">
                                        <uc2:labouroutstanding id="LabourOutstanding1" runat="server" />
                                    </div>--%>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <%--  <asp:AsyncPostBackTrigger ControlID="btnAddItems" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnOk" EventName="Click" />--%>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnOk" runat="server" ValidationGroup="OverRide" CausesValidation="true"
                        CssClass="btn btn-primary"
                        OnClick="btnOk_Click" />
                </div>
            </div>

        </div>

    </div>

    <div class="modal fade" id="modalInwardDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header btn-primary">
                    <div class="heading">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <asp:Label ID="lblInwardDetail" runat="server" />
                    </div>
                </div>
                <div class="modal-body" style="overflow-y: auto; min-height: 400px;">
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TabContainer runat="server" CssClass="Bigtab" ActiveTabIndex="0" ID="tabMain">
                                <asp:TabPanel runat="server" HeaderText="Inward Details" TabIndex="11" ID="tblInwardDetail">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblLotNo" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtLotNo" ReadOnly="true" CssClass="form-control input-sm" runat="server" />
                                            </div>
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblItemName" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtItemName" ReadOnly="true" CssClass="form-control input-sm" runat="server" />
                                            </div>
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblParty" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtParty" ReadOnly="true" CssClass="form-control input-sm" runat="server" />
                                            </div>
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblInwardno" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtInwardno" runat="server" CssClass="form-control input-sm" ReadOnly="true" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblReceivedQuanity" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtReceivedQuanity" ReadOnly="true" CssClass="form-control input-sm" runat="server" />
                                            </div>
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblBalanceQuantity" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtBalanceQuantity" ReadOnly="true" CssClass="form-control input-sm" runat="server" />
                                            </div>
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblScheme" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtScheme" runat="server" CssClass="form-control input-sm" ReadOnly="true" />
                                            </div>
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblOrderedQtyInDetail" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtOrderQuanityInDetail" runat="server" CssClass="form-control input-sm" ReadOnly="true" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblPalletNo" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtPalletNo" runat="server" CssClass="form-control input-sm" ReadOnly="true" />
                                            </div>
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblDate" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control input-sm" ReadOnly="true" />
                                            </div>
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblPacking" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtPacking" runat="server" CssClass="form-control input-sm" ReadOnly="true" />
                                            </div>
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblVehicleNo" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtVehicleNo" runat="server" CssClass="form-control input-sm" ReadOnly="true" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblContainerNo" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtContainerNo" runat="server" CssClass="form-control input-sm" ReadOnly="true" />
                                            </div>
                                            <div class="col-lg-9">
                                                <asp:Label ID="lblRemark" runat="server" CssClass="bold" />
                                                <asp:TextBox ID="txtRemarkInPopup" ReadOnly="true" CssClass="form-control input-sm" runat="server" />
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" HeaderText="Outwards" TabIndex="11" ID="tblOrder">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <asp:Label ID="lblOutwardDetails" runat="server" CssClass="bold" />
                                            </div>
                                            <div class="col-lg-10 text-align-right">
                                                <asp:Label ID="lblTotalOutwardQty" runat="server" CssClass="bold" />
                                                <asp:Label ID="lblTotalOutwardQtyToShow" runat="server" CssClass="bold" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12" style="overflow: auto;">
                                                <asp:GridView ID="grdOutwardDetails" runat="server" AutoGenerateColumns="False"
                                                    OnPreRender="grdOutwardDetails_OnPreRender"
                                                    CssClass="table table-hover text-nowrap">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblOutwardNo" Text="OutwardNo" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOutwardNo" Text='<%#Eval("Outward.OutwardNo") %>' ToolTip='<%#Eval("Outward.OutwardNo") %>'
                                                                    runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblOutwardDate" Text="OutwardDate" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOutwardDate" Text='<%#Eval("Outward.OutwardDate") %>'
                                                                    ToolTip='<%#Eval("Outward.OutwardDate") %>'
                                                                    runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblOutwardQuantity" Text="Quantity" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOutwardQty" Text='<%#Eval("Quantity") %>' ToolTip='<%#Eval("Quantity") %>'
                                                                    runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblVehicleNo" Text="Vehicle No" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVehicleNo" Text='<%#Eval("Outward.GateEntry.VehicalNo") %>'
                                                                    ToolTip='<%#Eval("Outward.GateEntry.VehicalNo") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblMaterialOrderNo" Text="MaterialOrderNo" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMONo" Text='<%#Eval("MaterialOrderDetail.MaterialOrder.MaterialOrderNo") %>'
                                                                    ToolTip='<%#Eval("MaterialOrderDetail.MaterialOrder.MaterialOrderNo") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblMaterialOrderDate" Text="OrderDate" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMODate" Text='<%#Eval("MaterialOrderDetail.MaterialOrder.MaterialOrderDate") %>'
                                                                    ToolTip='<%#Eval("MaterialOrderDetail.MaterialOrder.MaterialOrderDate") %>'
                                                                    runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" HeaderText="Pending Orders" TabIndex="11" ID="tblOutward">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <asp:Label ID="lblMaterialOrderPending" runat="server" CssClass="bold" />
                                            </div>
                                            <div class="col-lg-9 text-align-right">
                                                <asp:Label ID="lblTotalPendingQty" runat="server" CssClass="bold" />
                                                &nbsp;
                                    <asp:Label ID="lblTotalPendingQtyToShow" runat="server" CssClass="bold" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12" style="overflow: auto;">
                                                <asp:GridView ID="grdMaterialOrderDetails" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-hover text-nowrap" OnPreRender="grdMaterialOrderDetails_OnPreRender">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblMaterialOrderNo" Text="MaterialOrderNo" runat="server"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMONo" Text='<%#Eval("MaterialOrder.MaterialOrderNo") %>' ToolTip='<%#Eval("MaterialOrder.MaterialOrderNo") %>'
                                                                    runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblMaterialOrderDate" Text="Order Date" runat="server"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblMODate" Text='<%#Eval("MaterialOrder.MaterialOrderDate") %>'
                                                                    ToolTip='<%#Eval("MaterialOrder.MaterialOrderDate") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblDeliveryDate" Text="Delivery Date" runat="server"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeliveryDate" Text='<%#Eval("MaterialOrder.DeliveryDate") %>'
                                                                    ToolTip='<%#Eval("MaterialOrder.DeliveryDate") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblDeliverTo" Text="Deliver Location" runat="server"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeliverTo" Text='<%#Eval("MaterialOrder.DeliverLocation") %>' ToolTip='<%#Eval("MaterialOrder.DeliverLocation") %>'
                                                                    runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblTransporter" Text="Transporter" runat="server"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTransporter" Text='<%#Eval("MaterialOrder.DeliverLocation") %>' ToolTip='<%#Eval("MaterialOrder.DeliverLocation") %>'
                                                                    runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblOrderQuantity" Text="Balance Qrder Qty" runat="server"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblOrderQuantity" Text='<%#Eval("BalOrderQuantity") %>' ToolTip='<%#Eval("BalOrderQuantity") %>'
                                                                    runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" HeaderText="Location Transfer" TabIndex="11" ID="tblLoc">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <asp:Label ID="lblLocationTransfer" runat="server" CssClass="bold" />
                                            </div>
                                            <div class="col-lg-10 text-align-right">
                                                <asp:Label ID="lblTotalTransferQty" runat="server" CssClass="bold" />
                                                &nbsp;
                                    <asp:Label ID="lblTotalTransferQtyShow" runat="server" CssClass="bold" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12" style="overflow: auto;">
                                                <asp:GridView ID="grdlocationtransfer" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-hover text-nowrap" OnPreRender="grdlocationtransfer_OnPreRender">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblLocationTransferNo" runat="server"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblltno" Text='<%#Eval("TransferNo") %>' ToolTip='<%#Eval("TransferNo") %>'
                                                                    runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblTransferDate" runat="server"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTransferDate" Text='<%#Eval("TransferDate") %>'
                                                                    ToolTip='<%#Eval("TransferDate") %>' runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblFromLocation" runat="server"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblfrmlocation" Text='<%#Eval("FromLocation") %>' ToolTip='<%#Eval("FromLocation") %>'
                                                                    runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblToLocation" runat="server"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltolocation" Text='<%#Eval("ToLocation") %>' ToolTip='<%#Eval("ToLocation") %>'
                                                                    runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblTransferQuantity" runat="server"></asp:Label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbltquanty" Text='<%#Eval("LTQuantity") %>' ToolTip='<%#Eval("LTQuantity") %>'
                                                                    runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:TabPanel>
                            </asp:TabContainer>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="gvDescription" EventName="RowCommand" />
                            <asp:AsyncPostBackTrigger ControlID="btnCancelDetailPopup" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="gvSearch" EventName="RowCommand" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer" style="display: none;">
                    <asp:HiddenField ID="rowsid" runat="server" />
                    <asp:Button ID="btnCancelDetailPopup" runat="server" OnClick="btnCancelDetailPopup_Click"
                        TabIndex="35" CssClass="btn btn-default" Visible="false" />
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDeliveryLoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <div class="modal-header btn-primary">
                    <div class="heading">
                        <asp:Label ID="Label1" Text="Delivery Location" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="pnlDeliveryLoc" runat="server">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <asp:Label ID="lblLocmsg" Text="" runat="server" CssClass="text-danger" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <asp:Label ID="lblNetQty" runat="server" />
                                        <asp:TextBox ID="txtNetQty" ReadOnly="true" Text="" runat="server" CssClass="form-control input-sm" />
                                    </div>
                                    <div class="col-lg-6">
                                        <asp:Label ID="lblOrderQty" runat="server" />
                                        <asp:TextBox ID="txtOrderQty" ReadOnly="true" Text="" runat="server" CssClass="form-control input-sm" />
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-lg-12" style="overflow: auto;">
                                        <asp:GridView ID="grdLoc" runat="server" AutoGenerateColumns="false"
                                            CssClass="table table-striped table-hover text-nowrap">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblDeliveryLocation" Text="Delivery Location" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLOc" TabIndex="91" runat="server"
                                                            Text='<%# Eval("DeliveryLocation") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblQuantity" Text="OrderQuantity" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lbqty" Text='<%#Eval("OrderQuantity") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-1">
                                        <asp:HiddenField ID="hdfRowId" runat="server" />
                                        <asp:HiddenField ID="hdfdelinwddetid" runat="server" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="gvDescription" EventName="RowCommand" />
                            <asp:AsyncPostBackTrigger ControlID="btnAddDelLoc" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnCancelLoction" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-lg-4">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDeliveryLoc"
                                Display="Dynamic" SetFocusOnError="true" CssClass="text-danger"
                                ErrorMessage="" ValidationGroup="DelLocation"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtDeliveryLoc" TabIndex="91" runat="server" ValidationGroup="DelLocation"
                                placeholder="Delivery Location" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-4">
                            <asp:RequiredFieldValidator ID="rfvOrderQuant" runat="server" Display="Dynamic" ControlToValidate="txtlbqty" CssClass="text-danger"
                                ValidationGroup="DelLocation" SetFocusOnError="true" ErrorMessage="*"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cmpOrderQuantity" runat="server" ControlToValidate="txtlbqty" CssClass="text-danger"
                                ErrorMessage="" ControlToCompare="txtNetQuan" ValidationGroup="DelLocation"
                                Type="Integer" Operator="LessThanEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ValidationGroup="DelLocation" CssClass="text-danger"
                                ErrorMessage="" ControlToValidate="txtlbqty" ValueToCompare="0" Type="Integer"
                                Operator="NotEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                            <asp:CompareValidator ID="CompareValidator2" runat="server" ValidationGroup="DelLocation" CssClass="text-danger"
                                ErrorMessage="" ControlToValidate="txtlbqty" ValueToCompare="0" Type="Integer"
                                Operator="GreaterThanEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                            <asp:TextBox runat="server" ID="txtlbqty" placeholder="Quantity"
                                ValidationGroup="DelLocation" CssClass="form-control input-sm" CausesValidation="True" MaxLength="7" TabIndex="91" />
                            <div style="height: 0px; width: 0px;">
                                <asp:TextBox ID="txtNetQuan" runat="server" Text='<%#Eval("StockQuantity") %>' Height="0px"
                                    Width="0px" BorderColor="White" BackColor="White" BorderStyle="None" CausesValidation="True"
                                    BorderWidth="0px" ReadOnly="True"></asp:TextBox>
                            </div>
                            <asp:FilteredTextBoxExtender ID="ftbOrderQuantity" runat="server" FilterType="Numbers"
                                TargetControlID="txtlbqty">
                            </asp:FilteredTextBoxExtender>
                        </div>
                        <div class="col-lg-4">
                            <div class="btn-group">
                                <asp:Button ID="btnAddDelLoc" runat="server" CssClass="btn btn-primary" OnClick="btnAddDelLoc_Click" ValidationGroup="DelLocation" CausesValidation="true"
                                    TabIndex="92" Text="" />
                                <asp:Button ID="btnCancelLoction" runat="server" CssClass="btn btn-primary" Text=""
                                    TabIndex="92" OnClick="btnCancelLoction_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modalDeliveryLoc-dialog -->
    </div>

    <div class="modal fade" id="MatrialOrderhistory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h1 class="modal-title">
                        <asp:Label ID="lblHistory" runat="server"></asp:Label>
                    </h1>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="Updatehistory" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-lg-8">
                                    <asp:Label ID="lblhistoryremarks" runat="server" CssClass="bold" />
                                    <asp:TextBox ValidationGroup="history" ID="txthistoryremarks" TabIndex="99" CssClass="form-control input-sm" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqhis" runat="server" ControlToValidate="txthistoryremarks" Display="Dynamic" ErrorMessage="Please Enter Valid Comments" ValidationGroup="history"></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-lg-1" style="text-align: left">
                                    <br />
                                    <asp:Button ID="btnHisSave" runat="server" TabIndex="99" OnClick="btnHisSave_Click" ValidationGroup="history" CssClass="btn btn-primary" />
                                </div>
                                <div class="col-lg-1" style="text-align: left">
                                    <br />
                                    <asp:Button ID="btnhisCancel" runat="server" TabIndex="99" OnClick="btnhisCancel_Click" CausesValidation="false" CssClass="btn btn-primary" />
                                </div>
                            </div>
                            <div class="newline"></div>

                            <div class="row">
                                <div class="col-lg-12" style="overflow: auto;">
                                    <asp:GridView ID="grdhistory" runat="server" AutoGenerateColumns="false" OnPreRender="grdhistory_OnPreRender" CssClass="table table-hover table-Full text-nowrap">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label runat="server" ID="lblgrdSrno" Text=""></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSrno" runat="server" Text="<%# Convert.ToInt32(Container.DataItemIndex)+1%>" Visible="true"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblUser" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbleduser" Text='<%# Eval("UserCode") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblUserFullName" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUserFullName" Text='<%# Eval("UserFullName") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblDate" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbleddate" Text='<%# Eval("CreatedDate") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblremarks" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblremark" Text='<%# Eval("Remark") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnhisCancel" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnHisSave" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="StockAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-header">
                    <h1 class="modal-title">
                        <asp:Label ID="lblheaderstock" runat="server"></asp:Label>
                    </h1>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-lg-12" style="text-align: center">
                                    <asp:Label ID="lblStockAlertMsg" runat="server" Text="" CssClass="text-danger"></asp:Label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12" style="text-align: center">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12" style="text-align: right">
                                    <asp:Button ID="btnYes" runat="server" TabIndex="1" OnClick="btnYes_Click" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnNo" runat="server" TabIndex="1" OnClick="btnNo_Click" CssClass="btn btn-primary" />
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnYes" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnNo" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnAddItems" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

