﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class CompanyUser : BigSunPage
{
    AppCompany obj;
    protected void Page_Load(object sender, EventArgs e)
    {        
        obj = (AppCompany)Session[TableConstants.ObjCompanySession];
        if (!IsPostBack)
        {
            BindData();
        }
    }
    protected void grdUsers_PreRender(object sender, EventArgs e)
    {
        if (grdUsers.Rows.Count > 0)
        {
            grdUsers.UseAccessibleHeader = true;
            grdUsers.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdUsers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = new AppConvert(e.CommandName);
        string strCommandID = "";
        if (strCommandName == "INSERT")
        {
            TextBox txtNewLoginName = grdUsers.FooterRow.FindControl("txtNewLoginName") as TextBox;
            TextBox txtNewPassword = grdUsers.FooterRow.FindControl("txtNewPassword") as TextBox;
            HiddenField hdfNewReportingToID = grdUsers.FooterRow.FindControl("hdfNewReportingToID") as HiddenField;
            DropDownList ddlNewCompanyAddress = grdUsers.FooterRow.FindControl("ddlNewCompanyAddress") as DropDownList;
            DropDownList ddlNewUserContact = grdUsers.FooterRow.FindControl("ddlNewUserContact") as DropDownList;
            HiddenField hdfNewDesignationID = grdUsers.FooterRow.FindControl("hdfNewDesignationID") as HiddenField;
            HiddenField hdfNewDepartmentID = grdUsers.FooterRow.FindControl("hdfNewDepartmentID") as HiddenField;
            CheckBox IsActive = grdUsers.FooterRow.FindControl("chkIsActive") as CheckBox;
            SaveCompanyUser(strCommandID, txtNewLoginName.Text, txtNewPassword.Text, hdfNewReportingToID.Value, ddlNewCompanyAddress, ddlNewUserContact, 
                hdfNewDesignationID.Value, hdfNewDepartmentID.Value, IsActive.Checked);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('User Successfully Added','1'" + ");", true);
            grdUsers.EditIndex = -1;
            BindData();
        }
        else if (strCommandName == "CANCELADD")
        {
            grdUsers.ShowFooter = false;
            BindData();
        }
        else if (strCommandName == "CANCELEDIT")
        {
            grdUsers.EditIndex = -1;
            BindData();
        }
        else if(strCommandName== "ADDNEW")
        {
            grdUsers.EditIndex = -1;
            grdUsers.ShowFooter = true;
            ((TextBox)grdUsers.FooterRow.FindControl("txtNewLoginName")).Focus();
            BindData();
        }
    }
    protected void grdUsers_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdUsers.ShowFooter = false;
        grdUsers.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdUsers_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        obj = (AppCompany)Session[TableConstants.ObjCompanySession];
        obj.CompanyUserColl[e.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
        Session[TableConstants.ObjCompanySession] = obj;
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('User Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void grdUsers_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strCommandID = new AppConvert(e.RowIndex);
        TextBox txtLoginName = grdUsers.Rows[e.RowIndex].FindControl("txtLoginName") as TextBox;
        TextBox txtPassword = grdUsers.Rows[e.RowIndex].FindControl("txtPassword") as TextBox;
        HiddenField hdfReportingToID = grdUsers.Rows[e.RowIndex].FindControl("hdfReportingToID") as HiddenField;
        DropDownList ddlCompanyAddress = grdUsers.Rows[e.RowIndex].FindControl("ddlCompanyAddress") as DropDownList;
        DropDownList ddlUserContact = grdUsers.Rows[e.RowIndex].FindControl("ddlUserContact") as DropDownList;
        HiddenField hdfDesignationID = grdUsers.Rows[e.RowIndex].FindControl("hdfDesignationID") as HiddenField;
        CheckBox IsActiveContact = grdUsers.Rows[e.RowIndex].FindControl("chkActive") as CheckBox;        
        HiddenField hdfDepartmentID = grdUsers.Rows[e.RowIndex].FindControl("hdfDepartmentID") as HiddenField;
        SaveCompanyUser(strCommandID, txtLoginName.Text, txtPassword.Text, hdfReportingToID.Value, ddlCompanyAddress, ddlUserContact,
                hdfDesignationID.Value, hdfDepartmentID.Value, IsActiveContact.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('User Successfully Updated','1'" + ");", true);
        grdUsers.EditIndex = -1;
    }
    protected void lbtnAddNew_Click(object sender, EventArgs e)
    {
    }
    protected void grdUsers_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Footer:
                DropDownList ddlAddresss = (DropDownList)e.Row.FindControl("ddlNewCompanyAddress");
                BindDropDown("Address1", "ID", ddlAddresss, 1);
                DropDownList ddlContact = (DropDownList)e.Row.FindControl("ddlNewUserContact");                
                BindDropDown("FirstName", "ID", ddlContact, 2);
                break;
            case DataControlRowType.DataRow:               
                DropDownList ddlCompanyAddresss = (DropDownList)e.Row.FindControl("ddlCompanyAddress");
                if (ddlCompanyAddresss != null)
                {
                    BindDropDown("Address1", "IndexOfAddress", ddlCompanyAddresss,1);
                    ddlCompanyAddresss.SelectedValue = new AppConvert(obj.CompanyUserColl[e.Row.RowIndex].CompanyAddress_ID);
                }
                DropDownList ddlCompanyContact = (DropDownList)e.Row.FindControl("ddlUserContact");
                if (ddlCompanyContact != null)
                {
                    BindDropDown("FirstName", "IndexOfContact", ddlCompanyContact, 2);
                    ddlCompanyContact.SelectedValue = new AppConvert(obj.CompanyUserColl[e.Row.RowIndex].CompanyContact_ID);
                }
                break;
            default:
                break;
        }
    }
    public void AddEmptyRow()
    {
        AppCompanyUserColl CollOfUsers = new AppCompanyUserColl(intRequestingUserID);
        AppCompanyUser newUser = new AppCompanyUser(intRequestingUserID);
        newUser.LoginName = "Add user";      
        CollOfUsers.Add(newUser);
        grdUsers.DataSource = CollOfUsers;
        grdUsers.DataBind();
        grdUsers.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdUsers.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdUsers.ShowFooter = true;
    }
    #region BindData    
    private void BindData()
    {        
        if(obj!=null)
        {
            grdUsers.DataSource = obj.CompanyUserColl;            
            grdUsers.DataBind();
            if (obj.CompanyUserColl.Count <= 0)
            {
                AddEmptyRow();
            }
        }
    }
    #endregion
    #region BindDropDown
    public void BindDropDown(string DataTextField, string DataValueField, DropDownList ddlID, int Type)
    {
        if (Type == 1)
        {            
            ddlID.DataSource = obj.CompanyAddressColl;
            ddlID.DataValueField = DataValueField;
        }
        else
        {            
            ddlID.DataSource = obj.CompanyContactColl;
            ddlID.DataValueField = DataValueField;
        }
        ddlID.DataTextField = DataTextField;        
        ddlID.DataBind();
    }
    #endregion
    #region Save Company User
    public void SaveCompanyUser(string Id, string LoginName, string Password, string ReportingToID,
        DropDownList CompanyAddress, DropDownList UserContact, string DesignationID, string DepartmentID, bool IsActive)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppCompanyUser SingleObj = new AppCompanyUser(intRequestingUserID);
        if (String.IsNullOrEmpty(Id) == false)
        {
            SingleObj = obj.CompanyUserColl[intCommandID];
        }
        else
        {
            obj.AddNewCompanyUser(SingleObj);
        }       
        SingleObj.Company_ID = obj.ID;
        SingleObj.CompanyContact = obj.CompanyContactColl[UserContact.SelectedIndex];
        SingleObj.LoginName = LoginName;
        SingleObj.Password = Password;
        SingleObj.Parent_ID = new AppConvert(ReportingToID);
        SingleObj.DesignationType_ID = new AppConvert(DesignationID);
        SingleObj.DepartmentType_ID = new AppConvert(DepartmentID);
        SingleObj.CompanyAddress = obj.CompanyAddressColl[CompanyAddress.SelectedIndex];
        SingleObj.Status = (IsActive==true?new AppUtility.AppConvert((int)RecordStatus.Active):new AppConvert((int)RecordStatus.Created));
        //SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;        
        Session.Add(TableConstants.ObjCompanySession, obj);        
        grdUsers.EditIndex = -1;
        BindData();        
    }
    #endregion
}