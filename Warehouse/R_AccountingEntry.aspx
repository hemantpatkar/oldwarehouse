﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="R_AccountingEntry" Codebehind="R_AccountingEntry.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="rsweb" Namespace="Microsoft.Reporting.WebForms" Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <div id="divButtons" class="pull-right">
                        <br />
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm  btn-primary" data-toggle="collapse" data-target="#demo">
                                <span class="glyphicon glyphicon-search"></span>Search Tools
                            </button>
                            <asp:Button runat="server" Text="Search" CssClass="btn btn-sm  btn-primary" ID="btnSearch" ValidationGroup="MainSearch" OnClick="btnSearch_Click"></asp:Button>

                        </div>
                    </div>
                    <h3>Accounting Entry </h3>
                    <hr />
                    <div id="demo" class="collapse">
                        <div class="row" runat="server">
                            <div class="col-lg-3">
                                <asp:Label ID="lblVoucherNo" runat="server" Text="Voucher No" CssClass="bold"></asp:Label>
                                <asp:TextBox ID="txtVoucherNo" runat="server" placeholder="Enter Voucher No" CssClass=" form-control input-sm"></asp:TextBox>
                            </div>
                            <div id="divFromDate" class="col-lg-2">
                                <asp:Label ID="lblFromDate" runat="server" Text="From Date" CssClass="bold"></asp:Label>
                                <div class="input-group input-group-sm">
                                    <asp:TextBox ID="txtFromDate" TabIndex="1" runat="server" placeholder="From Date" CssClass="form-control input-sm"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <asp:RequiredFieldValidator ControlToValidate="txtFromDate" runat="server" ID="RFV_txtFromDate" ValidationGroup="MainSearch" ></asp:RequiredFieldValidator>
                                <asp:MaskedEditExtender ID="Mee_txtFromDate" runat="server"
                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFromDate">
                                </asp:MaskedEditExtender>
                                <asp:CalendarExtender ID="Ce_txtFromDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtFromDate"
                                    TargetControlID="txtFromDate">
                                </asp:CalendarExtender>
                            </div>
                            <div id="divToDate" class="col-lg-2">
                                <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="bold"></asp:Label>
                                <div class="input-group input-group-sm">
                                    <asp:TextBox ID="txtToDate" TabIndex="1" runat="server" placeholder="To Date" CssClass="form-control input-sm"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <asp:RequiredFieldValidator ControlToValidate="txtToDate" runat="server" ID="RFV_txtToDate" ValidationGroup="MainSearch"></asp:RequiredFieldValidator>
                                <asp:MaskedEditExtender ID="Mee_txtToDate" runat="server"
                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtToDate">
                                </asp:MaskedEditExtender>
                                <asp:CalendarExtender ID="Ce_txtToDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtToDate"
                                    TargetControlID="txtToDate">
                                </asp:CalendarExtender>
                            </div>
                            <div class="col-lg-2">
                                <asp:Label ID="lblVocuerType" runat="server" Text="Voucher Type" CssClass="bold"></asp:Label>
                                <asp:DropDownList ID="ddlVocuerType" runat="server" CssClass="form-control  input-sm">
                                    <asp:ListItem Value="-1" Selected="True">ALL</asp:ListItem>
                                    <asp:ListItem Value="1001">Purchase Bill</asp:ListItem>
                                    <asp:ListItem Value="1003">Payment Voucher</asp:ListItem>
                                    <asp:ListItem Value="1005">Journal Voucher</asp:ListItem>
                                    <asp:ListItem Value="1007">Credit Note</asp:ListItem>
                                    <asp:ListItem Value="1009">Debit Note</asp:ListItem>
                                    <asp:ListItem Value="1011">Contra</asp:ListItem>
                                    <asp:ListItem Value="1015">Sales Bill</asp:ListItem>
                                    <asp:ListItem Value="1013">Receipt Voucher</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label ID="lblStatus" runat="server" Text="Transaction Status" CssClass="bold"></asp:Label>
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control  input-sm">
                                    <asp:ListItem Text="ALL" Value="-32768"></asp:ListItem>
                                    <asp:ListItem Text="Approved" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Created" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="Cancelled" Value="-9"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="panel panel-body">
            <rsweb:ReportViewer ID="ReportViewer1" AsyncRendering="false" runat="server" Width="100%">
            </rsweb:ReportViewer>
        </div>
    </div>
</asp:Content>
