﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" EnableSessionState="true"  Culture="en-GB" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" Inherits="T_SaleOrder" Codebehind="T_SaleOrder.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function AutoCompleteSearch(sender, eventArgs) {
            debugger
            var id = sender._id;
            var ArrayIDs = id.split('_');
            if (isNaN(ArrayIDs[ArrayIDs.length - 1]) == true) {// out side gridview textbox
                var ContentPlaceHolderID = id.replace(ArrayIDs[ArrayIDs.length - 1], '')
                var NameOfTextBox = ArrayIDs[ArrayIDs.length - 1].replace('ACEtxt', '');
                var txt = $get(ContentPlaceHolderID + 'txt' + NameOfTextBox);
                var txtName = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'Name');
                var txtID = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'ID');
                txt.value = eventArgs.get_text();
                txtName.value = eventArgs.get_text();
                txtID.value = eventArgs.get_value();
            }
            else { // in side gridview textbox
                debugger
                var ContentPlaceHolderID = id.replace("_" + ArrayIDs[ArrayIDs.length - 1], '').replace(ArrayIDs[ArrayIDs.length - 2], '')
                var NameOfTextBox = ArrayIDs[ArrayIDs.length - 2].replace('ACEtxt', '');
                var RowIndex = ArrayIDs[ArrayIDs.length - 1];
                var txt = $get(ContentPlaceHolderID + 'txt' + NameOfTextBox + '_' + RowIndex);
                var txtName = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'Name_' + RowIndex);
                var txtID = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'ID_' + RowIndex);
                txt.value = eventArgs.get_text();
                txtName.value = eventArgs.get_text();
                txtID.value = eventArgs.get_value();
            }
        }
        function ClearAutocompleteTextBox(sender) {
            debugger
            var id = sender.id;
            var ArrayIDs = id.split('_');
            if (isNaN(ArrayIDs[ArrayIDs.length - 1]) == true) { // out side gridview textbox
                var ContentPlaceHolderID = id.replace(ArrayIDs[ArrayIDs.length - 1], '')
                var NameOfTextBox = ArrayIDs[ArrayIDs.length - 1].replace('txt', '');
                var txt = $get(ContentPlaceHolderID + 'txt' + NameOfTextBox);
                var txtName = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'Name');
                var txtID = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'ID');
                if (txt.value == "No Records Found" || txt.value != txtName.value) {
                    txt.value = "";
                    txtName.value = "";
                    txtID.value = "";
                }
            }
            else {// In side gridview textbox
                var ContentPlaceHolderID = id.replace("_" + ArrayIDs[ArrayIDs.length - 1], '').replace(ArrayIDs[ArrayIDs.length - 2], '')
                var NameOfTextBox = ArrayIDs[ArrayIDs.length - 2].replace('txt', '');
                var RowIndex = ArrayIDs[ArrayIDs.length - 1];
                var txt = $get(ContentPlaceHolderID + 'txt' + NameOfTextBox + '_' + RowIndex);
                var txtName = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'Name_' + RowIndex);
                var txtID = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'ID_' + RowIndex);
                if (txt.value == "No Records Found" || txt.value != txtName.value) {
                    txt.value = "";
                    txtName.value = "";
                    txtID.value = "";
                }
            }
        }
        function toggleColor(sender) {
            debugger
            var id = sender.id;
            if (id == "HeaderDiscount") {
                var DiscountType = $get('<%=hdfDiscountType.ClientID %>');
                $("#iDiscount").toggleClass("fa fa-percent");
                $("#iDiscount").toggleClass("fa fa-inr");
                var li = sender.firstChild.className; //document.getElementById('iDiscount');
                if (li.className == "fa fa-inr") {
                    DiscountType.value = 2
                }
                else {
                    DiscountType.value = 1
                }
            }
            else {
                var SplitIdForIndex = sender.id.split('_');
                var DiscountType = $get(SplitIdForIndex[0] + '_' + SplitIdForIndex[1] + "_hdfDiscountType_" + SplitIdForIndex[3]);
                var iLinItemDiscount = $get(SplitIdForIndex[0] + '_' + SplitIdForIndex[1] + "_iLinItemDiscount_" + SplitIdForIndex[3]);
                $("#" + iLinItemDiscount.id + "").toggleClass("fa fa-percent");
                $("#" + iLinItemDiscount.id + "").toggleClass("fa fa-inr");
                var li = document.getElementById(iLinItemDiscount.id);
                if (li.className == "fa fa-inr") {
                    DiscountType.value = 2
                }
                else {
                    DiscountType.value = 1
                }
            }
        }
        function validateFloatKeyPress(el, evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            if (charCode == 46 && el.value.indexOf(".") !== -1) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div id="divButtons" class="pull-right">
                        <asp:LinkButton ID="btnSave" runat="server" ValidationGroup="FinalSave" OnClick="btnSave_Click"><i class="fa fa-check  fa-2x text-success"></i></asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" OnClick="btnCancel_Click"><i class="fa fa-close fa-2x"></i></asp:LinkButton>
                        <asp:LinkButton ID="btnApprove" runat="server" OnClick="btnApprove_Click"><i class="fa fa-thumb fa-2x"></i></asp:LinkButton>
                    </div>
                    <h4>Sale Order</h4>
                    <hr />
                    <div class="row">
                        <div class=" col-lg-12">
                            <asp:UpdatePanel ID="up_SaleOrder" runat="server">
                                <ContentTemplate>
                                    <asp:TabContainer runat="server" CssClass="Bigtab" ActiveTabIndex="0" ID="tabMain">
                                        <asp:TabPanel runat="server" HeaderText="Basic Info" TabIndex="11" ID="tblLineItemDetail">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div id="divSaleDate" class="col-lg-3">
                                                        <asp:Label ID="lblSaleDate" runat="server" Text="Sale Order Date" CssClass="bold"></asp:Label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtSaleDate" TabIndex="1" runat="server" placeholder="Sale Order Date" CssClass="form-control input-sm" ValidationGroup="MainSave"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <asp:MaskedEditExtender ID="Mee_txtSaleDate" runat="server"
                                                            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtSaleDate">
                                                        </asp:MaskedEditExtender>
                                                        <asp:CalendarExtender ID="Ce_SaleDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtSaleDate"
                                                            TargetControlID="txtSaleDate">
                                                        </asp:CalendarExtender>
                                                    </div>
                                                    <div id="divCustomer" class="col-lg-3">
                                                        <asp:Label ID="lblCustomer" CssClass="bold " runat="server" Text="Customer">             
                                                        </asp:Label>
                                                        <asp:Label ID="lblCustomerMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfvtxtCustomer" runat="server" ControlToValidate="txtCustomer" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage=" (Select Customer)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtCustomer" TabIndex="1" OnTextChanged="txtCustomer_TextChanged" AutoPostBack="true" placeholder="Select Customer" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)"
                                                                CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfCustomerName" Value="" />
                                                        <asp:HiddenField runat="server" ID="hdfCustomerID" Value="" />
                                                        <asp:AutoCompleteExtender ID="ACEtxtCustomer" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtCustomer">
                                                        </asp:AutoCompleteExtender>
                                                    </div>
                                                    <div id="divDeliveryDate" class="col-lg-3">
                                                        <asp:Label ID="lblDeliveryDate" runat="server" Text="Delivery Date" CssClass="bold"></asp:Label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtDeliveryDate" TabIndex="1" runat="server" placeholder="Delivery Date" CssClass="form-control input-sm" ValidationGroup="MainSave"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <asp:MaskedEditExtender ID="Mee_txtDeliveryDate" runat="server" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtDeliveryDate">
                                                        </asp:MaskedEditExtender>
                                                        <asp:CalendarExtender ID="Ce_txtDeliveryDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtDeliveryDate"
                                                            TargetControlID="txtDeliveryDate">
                                                        </asp:CalendarExtender>
                                                    </div>
                                                    <div id="divTotalSaleAmount" class="col-lg-3">
                                                        <asp:Label ID="lblTotalSaleAmount" runat="server" CssClass="bold" Text="Total Sale Amount"></asp:Label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtTotalAmount" runat="server" Enabled="false" TabIndex="1" placeholder="0.00" CssClass="form-control amt input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-inr"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:TabPanel>
                                        <asp:TabPanel runat="server" HeaderText="Other Info" TabIndex="15" ID="tblAttachment">
                                            <ContentTemplate>
                                                <div class="row">
                                                    <div id="divSaleOrderNo" class="col-lg-3">
                                                        <asp:Label ID="lblSaleOrderNo" runat="server" CssClass="bold" Text="Sale Order No"></asp:Label>
                                                        <asp:TextBox ID="txtSaleOrderNo" TabIndex="1" placeholder="Sale Order No" Enabled="false" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                    <div id="divStatus" class="col-lg-3">
                                                        <asp:Label ID="lblStatus" runat="server" CssClass="bold" Text="Status"></asp:Label>
                                                        <asp:TextBox ID="txtStatus" placeholder="Transaction Status" TabIndex="1" Enabled="false" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                    <div id="divTermsAndConditions" class="col-lg-3">
                                                        <asp:Label ID="lblTermsAndConditions" CssClass="bold " runat="server" Text="Terms And Conditions">             
                                                        </asp:Label>
                                                        <asp:Label ID="lblTermsAndConditionsMsg" runat="server" CssClass="bold text-danger" Text=" * "></asp:Label>
                                                        <asp:RequiredFieldValidator ID="rfvTermsAndConditions" runat="server" ControlToValidate="txtTermsAndConditions" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage=" (Select Terms And Conditions)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtTermsAndConditions" TabIndex="1" placeholder="Select Terms And Conditions" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)"
                                                                CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfTermsAndConditionsName" Value="" />
                                                        <asp:HiddenField runat="server" ID="hdfTermsAndConditionsID" Value="" />
                                                        <asp:AutoCompleteExtender ID="ACEtxtTermsAndConditions" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TermsAndConditionsSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtTermsAndConditions">
                                                        </asp:AutoCompleteExtender>
                                                    </div>
                                                    <div id="divDiscount" class="col-lg-3">
                                                        <asp:Label ID="lblDiscount" runat="server" CssClass="bold" Text="Discount">  </asp:Label>
                                                         <div class="input-group">
                                                            <asp:TextBox ID="txtDiscount" TabIndex="1" placeholder="0.00" AutoPostBack="true" runat="server" AutoCompleteType="None" AutoComplete="Off" OnTextChanged="txtDiscount_TextChanged1"
                                                                CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon" id="HeaderDiscount" onclick="return toggleColor(this)"><i id="iDiscount" runat="server" class="fa fa-percent"></i></span>
                                                            <asp:HiddenField runat="server" ID="hdfDiscountType" Value="1" />
                                                        </div>                                                  
                                                    </div>
                                                </div>
                                                <div id="rowMulticurrency" runat="server">
                                                    <div class="newline"></div>
                                                    <div class="row">
                                                        <div id="divBaseCurrency" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderBaseCurrency" Text="Base Currency" runat="server" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblHeaderBaseCurrencyMsg" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                            <asp:TextBox ID="txtHeaderBaseCurrency" placeholder="Select Base Currency" runat="server" AutoPostBack="True" CssClass="form-control amt input-sm" TabIndex="1" Enabled="false"></asp:TextBox>
                                                            <asp:HiddenField runat="server" ID="hdfHeaderBaseCurrencyID" Value="" />
                                                            <asp:HiddenField runat="server" ID="hdfHeaderBaseCurrencyName" Value="" />
                                                        </div>
                                                        <div id="divExchangeCurrency" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderExchangeCurrency" CssClass="bold " runat="server" Text="Transaction Currency"> </asp:Label>
                                                            <asp:Label ID="lblHeaderExchangeCurrencyMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                            <asp:RequiredFieldValidator ID="Rfv_txtHeaderExchangeCurrency" runat="server" ControlToValidate="txtHeaderExchangeCurrency" CssClass="text-danger"
                                                                Display="Dynamic" ErrorMessage=" (Select Transaction Currency)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtHeaderExchangeCurrency" TabIndex="1" placeholder="Select Transaction Currency" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfHeaderExchangeCurrencyName" Value="" />
                                                            <asp:HiddenField runat="server" ID="hdfHeaderExchangeCurrencyID" Value="" />
                                                            <asp:AutoCompleteExtender ID="ACEtxtHeaderExchangeCurrency" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CurrencyTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtHeaderExchangeCurrency">
                                                            </asp:AutoCompleteExtender>
                                                        </div>
                                                        <div id="divExchangeCurrencyRate" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderExchangeCurrencyRate" Text="Exchange Currency Rate" runat="server" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblHeaderExchangeCurrencyRateMsg" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtHeaderExchangeCurrencyRate" placeholder="0.00" runat="server" AutoPostBack="True" CssClass="form-control amt input-sm"
                                                                    onkeypress="return validateFloatKeyPress(this, event)" TabIndex="1" ReadOnly="True"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                            </div>
                                                        </div>
                                                        <div id="divCurrencyConvertedAmount" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderCurrencyConvertedAmount" Text="Converted Currency Amount" runat="server" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblHeaderCurrencyConvertedAmountMsg" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                                                            <asp:TextBox ID="txtHeaderCurrencyConvertedAmount" placeholder="0.00" runat="server" CssClass="form-control amt input-sm" Enabled="False" TabIndex="1"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="newline"></div>
                                                <div class="row">
                                                    <div id="divCompanyAddress" class="col-lg-6">
                                                        <asp:Label ID="lblCompanyAddress" CssClass="bold " runat="server" Text="Billing Address"> </asp:Label>
                                                        <asp:Label ID="lblCompanyAddressMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                        <asp:RequiredFieldValidator ID="Rfv_txtCompanyAddress" runat="server" ControlToValidate="txtCompanyAddress" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage=" (Select Transaction Currency)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtCompanyAddress" TextMode="MultiLine" TabIndex="1" placeholder="Select Company Address" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfCompanyAddressName" Value="" />
                                                        <asp:HiddenField runat="server" ID="hdfCompanyAddressID" Value="" />
                                                        <asp:AutoCompleteExtender ID="ACEtxtCompanyAddress" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanyAddressSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtCompanyAddress">
                                                        </asp:AutoCompleteExtender>
                                                    </div>
                                                    <div id="divDeliveryAddress" class="col-lg-6">
                                                        <asp:Label ID="lblDeliveryAddress" CssClass="bold " runat="server" Text="Delivery Address"> </asp:Label>
                                                        <asp:Label ID="lblDeliveryAddressMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RFV_txtDeliveryAddress" runat="server" ControlToValidate="txtDeliveryAddress" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage=" (Select Transaction Currency)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtDeliveryAddress" TextMode="MultiLine" TabIndex="1" placeholder="Select Delivery Address" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfDeliveryAddressName" Value="" />
                                                        <asp:HiddenField runat="server" ID="hdfDeliveryAddressID" Value="" />
                                                        <asp:AutoCompleteExtender ID="ACEtxtDeliveryAddress" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanyAddressSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtDeliveryAddress">
                                                        </asp:AutoCompleteExtender>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div id="divTax" class="col-lg-3">
                                                        <asp:Label ID="lblHeaderTax" CssClass="bold " runat="server" Text="Tax"> </asp:Label>
                                                        <asp:Label ID="lblHeaderTaxMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RFV_txtHeaderTax" runat="server" ControlToValidate="txtHeaderTax" CssClass="text-danger"
                                                            Display="Dynamic" ErrorMessage=" (Select Transaction Currency)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtHeaderTax" TabIndex="1"  AutoPostBack="true" placeholder="Select Tax" OnTextChanged="txtHeaderTax_TextChanged" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfHeaderTaxName" Value="" />
                                                        <asp:HiddenField runat="server" ID="hdfHeaderTaxID" Value="" />
                                                        <asp:AutoCompleteExtender ID="ACEtxtHeaderTax" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TaxTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtHeaderTax">
                                                        </asp:AutoCompleteExtender>
                                                    </div>
                                                     <div id="divHeaderTaxAmount" class="col-lg-3">
                                                        <asp:Label ID="lblHeadertaxAmount" runat="server" CssClass="bold" Text="Tax Amount"></asp:Label>
                                                        <asp:TextBox ID="txtHeaderTaxAmount" placeholder="0.00" Enabled="false" runat="server" TabIndex="1" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                    <div id="divRemark" class="col-lg-6">
                                                        <asp:Label ID="lblRemark" runat="server" CssClass="bold" Text="Remark"></asp:Label>
                                                        <asp:TextBox ID="txtRemark" placeholder="Remark" runat="server" TabIndex="1" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-3" id="divPOReference">
                                                         <asp:Label ID="lblPoReference" runat="server" CssClass="bold" Text="PO Reference"></asp:Label>
                                                        <asp:TextBox ID="txtPOReference" placeholder="PO Reference" runat="server" TabIndex="1" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                    <div class="col-lg-3" id="divPODate">
                                                         <asp:Label ID="lblPoDate" runat="server" CssClass="bold" Text="PO Date"></asp:Label>
                                                     <div class="input-group">
                                                        <asp:TextBox ID="txtPODate" placeholder="PO Date" runat="server" TabIndex="1" CssClass="form-control input-sm"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <asp:MaskedEditExtender ID="MEE_PoDAte" runat="server"
                                                            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtPODate">
                                                        </asp:MaskedEditExtender>
                                                        <asp:CalendarExtender ID="CE_PoDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtPODate"
                                                            TargetControlID="txtPODate">
                                                        </asp:CalendarExtender>                                                    
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:TabPanel>
                                    </asp:TabContainer>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <asp:UpdatePanel runat="server" ID="SaleOrder">
                        <ContentTemplate>
                            <asp:GridView ID="grdSaleOrderDetails" runat="server" AutoGenerateColumns="False"   ShowHeaderWhenEmpty="true" GridLines="Both" ShowFooter="true"
                                CssClass="table table-hover table-striped text-nowrap nowrap" OnRowCommand="grdSaleOrderDetails_RowCommand" OnPreRender="grdSaleOrderDetails_PreRender">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="200">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderItem" Text="Item" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtItem" runat="server" ControlToValidate="txtItem" CssClass="text-danger" Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtItem" TabIndex="1" Text='<%# Eval("Item.Name") %>' AutoPostBack="true" OnTextChanged="txtItem_TextChanged" placeholder="Select Item" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfItemName" Value='<%# Eval("Item.Name") %>' />
                                            <asp:HiddenField runat="server" ID="hdfItemID" Value='<%# Eval("Item_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtItem" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"  ContextKey='<%# hdfCustomerID.Value %>' UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CustomerItemSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtItem">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderUOM" Text="UOM" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>                                            
                                            <asp:RequiredFieldValidator ID="Rfv_txtUOM" runat="server" ControlToValidate="txtUOM" CssClass="text-danger" Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtUOM" TabIndex="1" Text='<%# Eval("Item.UOMType.Name") %>' AutoPostBack="false" placeholder="Select UOM" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfUOMName" Value='<%# Eval("Item.UOMType.Name") %>' />
                                            <asp:HiddenField runat="server" ID="hdfUOMID" Value='<%# Eval("ItemUOMID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtUOM" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="UOMTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtUOM">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                           <asp:Label ID="lblFooterTotal" Text="Total" runat="server" CssClass="bold"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderQuantity" Text="Quantity" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtQuantity" TabIndex="1" AutoPostBack="true" Text='<%# Eval("Quantity") %>' OnTextChanged="txtQuantity_TextChanged" placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                           <asp:Label ID="lblFooterQuantity" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>                               
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderRate" Text="Rate" runat="server" CssClass="bold"></asp:Label>
                                                (  <span><i class="fa fa-inr"></i></span>)
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtRate" TabIndex="1" OnTextChanged="txtRate_TextChanged" AutoPostBack="true" Text='<%# Convert.ToDecimal(Eval("ItemRate")).ToString("#,##0.00") %>' placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>                                          
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderBasePrice" Text="Base Price" runat="server" CssClass="bold"></asp:Label>
                                                (  <span><i class="fa fa-inr"></i></span>)
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtBasePrice" TabIndex="1" Text='<%# Convert.ToDecimal(Eval("ItemBaseAmount")).ToString("#,##0.00") %>' placeholder="0.00" runat="server" AutoCompleteType="None" Enabled="false"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                         <FooterTemplate>
                                           <asp:Label ID="lblFooterBasePrice" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderDiscount" Text="Discount" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="input-group">
                                                <asp:TextBox ID="txtDiscount" TabIndex="1" OnTextChanged="txtDiscount_TextChanged" AutoPostBack="true" Text='<%# Convert.ToDecimal(Eval("DiscountValue")).ToString("#,##0.00") %>' placeholder="0.00" runat="server" AutoCompleteType="None" AutoComplete="Off"
                                                    CssClass="form-control input-sm"></asp:TextBox>
                                                <span class="input-group-addon" id="LinItemDiscount"><i id="iLinItemDiscount" onclick="return toggleColor(this)" runat="server" class="fa fa-percent"></i></span>
                                                <asp:HiddenField runat="server" ID="hdfDiscountType" Value='<%# Convert.ToDecimal(Eval("DiscountType")) %>' />
                                            </div>
                                        </ItemTemplate>
                                          <FooterTemplate>
                                           <asp:Label ID="lblFooterDiscount" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderLineItemTax" Text="Tax" runat="server" CssClass="bold"></asp:Label>
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="input-group">
                                                <asp:TextBox ID="txtLineItemTax" TabIndex="1" AutoPostBack="true" Text='<%# Eval("TaxType.Name") %>' OnTextChanged="txtLineItemTax_TextChanged" placeholder="Select Tax" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="hdfLineItemTaxName" Value='<%# Eval("TaxType.Name") %>' />
                                                <asp:HiddenField runat="server" ID="hdfLineItemTaxID" Value='<%# Eval("TaxType_ID") %>' />
                                            </div>
                                            <asp:AutoCompleteExtender ID="ACEtxtLineItemTax" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TaxTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLineItemTax">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderLineItemTaxAmount" Text="Tax" runat="server" CssClass="bold"></asp:Label>
                                                (  <span><i class="fa fa-inr"></i></span>)
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="form-control input-sm" Enabled="false" ID="txtTaxAmount" TabIndex="1" Text='<%# Convert.ToDecimal(Eval("TaxAmount")).ToString("#,##0.00") %>' runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                         <FooterTemplate>
                                           <asp:Label ID="lblFooterTax" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderNetAmount" Text="Net Amount" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="input-group">
                                                <asp:TextBox ID="txtNetAmount" Text='<%# Convert.ToDecimal(Eval("NetAmount")).ToString("#,##0.00") %>' TabIndex="1" placeholder="0.00" Enabled="false" runat="server" AutoCompleteType="None" AutoComplete="Off"
                                                    CssClass="form-control input-sm"></asp:TextBox>                                           
                                            </div>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                           <asp:Label ID="lblFooterNetAmount" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderLineItemExchangeCurrency" CssClass="bold " runat="server" Text="Trans Currency"> </asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtLineItemExchangeCurrency" runat="server" ControlToValidate="txtLineItemExchangeCurrency" CssClass="text-danger"
                                                Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtLineItemExchangeCurrency" Text='<%# Eval("CurrencyType.Name") %>' TabIndex="1" placeholder="Select Transaction Currency" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfLineItemExchangeCurrencyName" Value='<%# Eval("CurrencyType.Name") %>' />
                                            <asp:HiddenField runat="server" ID="hdfLineItemExchangeCurrencyID" Value='<%# Eval("CurrencyType_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtLineItemExchangeCurrency" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CurrencyTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLineItemExchangeCurrency">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderConvertedCurrencyAmount" Text="Currency Amount" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="input-group">
                                                <asp:TextBox ID="txtConvertedCurrencyAmount" Text='<%# Convert.ToDecimal(Eval("ConvertedCurrencyAmount")).ToString("#,##0.00") %>' TabIndex="1" placeholder="0.00" Enabled="false" runat="server" AutoCompleteType="None" AutoComplete="Off"
                                                    CssClass="form-control input-sm"></asp:TextBox>
                                            </div>
                                        </ItemTemplate>
                                         <FooterTemplate>
                                           <asp:Label ID="lblFooterCurrencyAmount" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderLineItemCustomer" Text="Customer" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtLineItemCustomer" TabIndex="1" Text='<%# Eval("Company.Name") %>' placeholder="Select Customer" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfLineItemCustomerName" Value='<%# Eval("Company.Name") %>' />
                                            <asp:HiddenField runat="server" ID="hdfLineItemCustomerID" Value='<%# Eval("Company_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACELineItemtxtCustomer" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLineItemCustomer">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderLineItemCompanyAddress" Text="Company Address" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtLineItemCompanyAddress" Text='<%# Eval("CompanyAddress.Address1") %>' TabIndex="1" placeholder="Select Company Address" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfLineItemCompanyAddressName" Value='<%# Eval("CompanyAddress.Address1") %>' />
                                            <asp:HiddenField runat="server" ID="hdfLineItemCompanyAddressID" Value='<%# Eval("CompanyAddress_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtLineItemCompanyAddress" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanyAddressSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLineItemCompanyAddress">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="5">
                                        <HeaderTemplate>
                                            <asp:Button ID="btnAddLineItem" TabIndex="1" Text="Add Item" runat="server" OnClick="btnAddLineItem_Click" CssClass="fa fa -button"></asp:Button>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnRemoveLineItem" CommandName="Remove" TabIndex="1" runat="server" Text="Remove"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
