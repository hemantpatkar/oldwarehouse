﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class T_Contra : BigSunPage
{
    #region PageLoad
    AppObjects.AppContra objContra = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        objContra = new AppContra(intRequestingUserID);
        if (!IsPostBack)
        {
            Session[TableConstants.Contra] = objContra;
            Ce_PaymentDate.SelectedDate = DateTime.Today;
            BindToDropdown();
            string ID = Convert.ToString(Request.QueryString["ContraID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int ContraID = new AppUtility.AppConvert(ID);
                objContra = new AppObjects.AppContra(intRequestingUserID, ContraID);
                Session[TableConstants.Contra] = objContra;
                PopulateData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "2")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Contra Approved Sucessfully','1'" + ");", true);
                }
            }
            else
            {
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
                Session[TableConstants.Contra] = objContra;
            }

        }
    }
    #endregion
    #region Functions
    public void BindToDropdown()
    {
        CommonFunctions.BindDropDown(intRequestingUserID, Components.PaymentType, ddlPaymentType, "PaymentTypeName", "ID", "", false, new AppConvert(ddlTransactionType.SelectedValue));

        CommonFunctions.BindDropDown(intRequestingUserID, Components.CompanyBank, ddlDebitAccountNo, "AccountNo", "ID", "", true, LoginCompanyID);

        ddlDebitAccountNo.SelectedValue = new AppConvert(DefaultCompanyBank.ID);
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlDebitBankBranch, "BankBranch", "ID", "", true, LoginCompanyID);
        ddlDebitBankBranch.SelectedValue = new AppConvert(DefaultCompanyBank.BankType_ID);
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlDebitBank, "Name", "ID", "", true, LoginCompanyID);
        ddlDebitBank.SelectedValue = new AppConvert(DefaultCompanyBank.BankType_ID);
        txtDebitLedger.Text = new AppConvert(DefaultCompanyBank.Ledger.LedgerName);
        hdfDebitLedgerName.Value = new AppConvert(DefaultCompanyBank.Ledger.LedgerName);
        hdfDebitLedgerID.Value = new AppConvert(DefaultCompanyBank.Ledger_ID);


        CommonFunctions.BindDropDown(intRequestingUserID, Components.CompanyBank, ddlCreditAccountNo, "AccountNo", "ID", "", true, LoginCompanyID);

        ddlCreditAccountNo.SelectedValue = new AppConvert(DefaultCompanyBank.ID);
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCreditBankBranch, "BankBranch", "ID", "", true, LoginCompanyID);
        ddlCreditBankBranch.SelectedValue = new AppConvert(DefaultCompanyBank.BankType_ID);
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCreditBank, "Name", "ID", "", true, LoginCompanyID);
        ddlCreditBank.SelectedValue = new AppConvert(DefaultCompanyBank.BankType_ID);
        txtCreditLedger.Text = new AppConvert(DefaultCompanyBank.Ledger.LedgerName);
        hdfCreditLedgerName.Value = new AppConvert(DefaultCompanyBank.Ledger.LedgerName);
        hdfCreditLedgerID.Value = new AppConvert(DefaultCompanyBank.Ledger_ID);
        SetBankDetails();
    }
    public void SetLedgerContextKey()
    {
        int TransactionType = new AppConvert(ddlTransactionType.SelectedValue);

        txtDebitLedger.Text = "";
        hdfDebitLedgerName.Value = "";
        hdfDebitLedgerID.Value = "";

        txtCreditLedger.Text = "";
        hdfCreditLedgerName.Value = "";
        hdfCreditLedgerID.Value = "";

        switch (TransactionType)
        {

            case 1:
                ACEtxtDebitLedger.ContextKey = "4";
                ACEtxtCreditLedger.ContextKey = "4";
                txtDebitLedger.Enabled = true;
                txtCreditLedger.Enabled = true;
                break;
            case 2:
                ACEtxtDebitLedger.ContextKey = "4";
                ACEtxtCreditLedger.ContextKey = "3";
                txtDebitLedger.Enabled = true;
                txtCreditLedger.Enabled = false;
                divDebitBankDetail.Visible = false;
                divCreditBankDetail.Visible = true;
                break;
            case 3:
                ACEtxtDebitLedger.ContextKey = "3";
                ACEtxtCreditLedger.ContextKey = "4";
                txtDebitLedger.Enabled = false;
                txtCreditLedger.Enabled = true;
                divDebitBankDetail.Visible = true;
                divCreditBankDetail.Visible = false;
                divBankDetails.Visible = true;
                break;
            case 4:
                ACEtxtDebitLedger.ContextKey = "3";
                ACEtxtCreditLedger.ContextKey = "3";
                txtDebitLedger.Enabled = false;
                txtCreditLedger.Enabled = false;
                divDebitBankDetail.Visible = true;
                divCreditBankDetail.Visible = true;
                break;
        }
    }
    public void SetBankDetails()
    {
        int PaymentType = new AppConvert(ddlPaymentType.SelectedValue);
        CommonFunctions.BindDropDown(intRequestingUserID, Components.PaymentType, ddlPaymentType, "PaymentTypeName", "ID", "", false, new AppConvert(ddlTransactionType.SelectedValue));
        ListItem cost = ddlPaymentType.Items.FindByValue(Convert.ToString(PaymentType));
        if (cost != null)
        {
            ddlPaymentType.SelectedValue = new AppConvert(PaymentType);
        }
        else
        {
            PaymentType = new AppConvert(ddlPaymentType.SelectedValue);
        }
        switch (PaymentType)
        {
            case 1:
                divBankDetails.Visible = false;
                divChequeNo.Visible = false;
                divPayeeName.Visible = false;
                RFV_txtChequeNo.Enabled = false;
                RFV_txtChequeDate.Enabled = false;
                divChequeDate.Visible = false;
                divBankReferenceNo.Visible = false;
                divPayeeName.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            case 2:
                divBankDetails.Visible = true;
                divChequeNo.Visible = true;
                divPayeeName.Visible = true;
                RFV_txtChequeNo.Enabled = true;
                RFV_txtChequeDate.Enabled = true;
                divBankReferenceNo.Visible = true;
                divChequeDate.Visible = true;
                break;
            case 3:
                divBankDetails.Visible = true;
                divChequeNo.Visible = true;
                divPayeeName.Visible = true;
                RFV_txtChequeNo.Enabled = false;
                RFV_txtChequeDate.Enabled = false;
                divBankReferenceNo.Visible = true;
                divChequeDate.Visible = true;
                break;
            case 4:
                divBankDetails.Visible = true;
                divChequeNo.Visible = false;
                divPayeeName.Visible = false;
                RFV_txtChequeNo.Enabled = false;
                RFV_txtChequeDate.Enabled = false;
                divBankReferenceNo.Visible = true;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;

            case 5:
                divBankDetails.Visible = true;
                divChequeNo.Visible = false;
                divPayeeName.Visible = false;
                RFV_txtChequeNo.Enabled = false;
                RFV_txtChequeDate.Enabled = false;
                divBankReferenceNo.Visible = true;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            default:
                divBankDetails.Visible = false;
                divChequeNo.Visible = false;
                divPayeeName.Visible = false;
                RFV_txtChequeNo.Enabled = false;
                RFV_txtChequeDate.Enabled = false;
                divBankReferenceNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
        }
        SetLedgerContextKey();
    }
    public void PopulateData()
    {
        AppContra objContra = (AppContra)Session[TableConstants.Contra];
        txtPaymentDate.Text = new AppConvert(objContra.ContraDate);
        ddlTransactionType.SelectedValue = new AppConvert(objContra.ContraTypeID);
        ddlPaymentType.SelectedValue = new AppConvert(objContra.PaymentType_ID);
        ddlDebitAccountNo.SelectedValue = new AppConvert(objContra.From_CompanyBank_ID);
        ddlDebitBankBranch.SelectedValue = new AppConvert(objContra.From_CompanyBank.BankType.Name);
        ddlDebitBankBranch.SelectedValue = new AppConvert(objContra.From_CompanyBank.BankType.BankBranch);
        if (objContra.PaymentType_ID > 1)
        {

            txtChequeNo.Text = new AppConvert(objContra.ChequeNo);
            txtChequeDate.Text = new AppConvert(objContra.ChequeDate);
            ddlCreditAccountNo.SelectedValue = new AppConvert(objContra.To_CompanyBank_ID);
            ddlCreditBankBranch.SelectedValue = new AppConvert(objContra.To_CompanyBank.BankType.BankBranch);
            ddlCreditBank.SelectedValue = new AppConvert(objContra.To_CompanyBank.BankType.Name);
            divBankDetails.Visible = true;
        }
        else
        {
            divBankDetails.Visible = false;
        }
        txtBankReferenceNo.Text = objContra.RefrenceNo;
        txtPayee.Text = objContra.PayeeName;
        txtAmount.Text = new AppConvert(objContra.Amount);
        txtDescription.Text = objContra.Description;
        txtVoucherNo.Text = new AppConvert(objContra.ContraNo);

        txtDebitLedger.Text = new AppConvert(objContra.From_Ledger.LedgerName);
        hdfDebitLedgerName.Value = new AppConvert(objContra.From_Ledger.LedgerName);
        hdfDebitLedgerID.Value = new AppConvert(objContra.From_Ledger_ID);

        txtCreditLedger.Text = new AppConvert(objContra.To_Ledger.LedgerName);
        hdfCreditLedgerName.Value = new AppConvert(objContra.To_Ledger.LedgerName);
        hdfCreditLedgerID.Value = new AppConvert(objContra.To_Ledger_ID);
        btnSave.Visible = objContra.Status != new AppConvert((int)RecordStatus.Created) ? false : true;
        btnApprove.Visible = objContra.Status == new AppConvert((int)RecordStatus.Created) ? true : false;
        #region  Show/Hide Bank Detal
        switch (objContra.ContraTypeID)
        {

            case 1:
                ACEtxtDebitLedger.ContextKey = "4";
                ACEtxtCreditLedger.ContextKey = "4";
                txtDebitLedger.Enabled = true;
                txtCreditLedger.Enabled = true;
                break;
            case 2:
                ACEtxtDebitLedger.ContextKey = "4";
                ACEtxtCreditLedger.ContextKey = "3";
                txtDebitLedger.Enabled = true;
                txtCreditLedger.Enabled = false;
                divDebitBankDetail.Visible = false;
                divCreditBankDetail.Visible = true;
                break;
            case 3:
                ACEtxtDebitLedger.ContextKey = "3";
                ACEtxtCreditLedger.ContextKey = "4";
                txtDebitLedger.Enabled = false;
                txtCreditLedger.Enabled = true;
                divDebitBankDetail.Visible = true;
                divCreditBankDetail.Visible = false;
                divBankDetails.Visible = true;
                break;
            case 4:
                ACEtxtDebitLedger.ContextKey = "3";
                ACEtxtCreditLedger.ContextKey = "3";
                txtDebitLedger.Enabled = false;
                txtCreditLedger.Enabled = false;
                divDebitBankDetail.Visible = true;
                divCreditBankDetail.Visible = true;
                break;
        }

        switch (objContra.PaymentType_ID)
        {
            case 1:
                divBankDetails.Visible = false;
                divChequeNo.Visible = false;
                divPayeeName.Visible = false;
                divChequeDate.Visible = false;
                RFV_txtChequeNo.Enabled = false;
                RFV_txtChequeDate.Enabled = false;
                divBankReferenceNo.Visible = false;
                divPayeeName.Visible = false;
                break;
            case 2:
                divBankDetails.Visible = true;
                divChequeNo.Visible = true;
                divPayeeName.Visible = true;
                RFV_txtChequeNo.Enabled = true;
                RFV_txtChequeDate.Enabled = true;
                divBankReferenceNo.Visible = true;
                divChequeDate.Visible = true;
                break;
            case 3:
                divBankDetails.Visible = true;
                divChequeNo.Visible = true;
                divPayeeName.Visible = true;
                RFV_txtChequeNo.Enabled = false;
                RFV_txtChequeDate.Enabled = false;
                divBankReferenceNo.Visible = true;
                divChequeDate.Visible = true;
                break;
            case 4:
                divBankDetails.Visible = true;
                divChequeNo.Visible = false;
                divPayeeName.Visible = false;
                RFV_txtChequeNo.Enabled = false;
                RFV_txtChequeDate.Enabled = false;
                divBankReferenceNo.Visible = true;
                divChequeDate.Visible = false;

                break;

            case 5:
                divBankDetails.Visible = true;
                divChequeNo.Visible = false;
                divPayeeName.Visible = false;
                divBankReferenceNo.Visible = true;
                RFV_txtChequeNo.Enabled = false;
                RFV_txtChequeDate.Enabled = false;
                divChequeDate.Visible = false;

                break;
            default:
                divBankDetails.Visible = false;
                divChequeNo.Visible = false;
                divPayeeName.Visible = false;
                RFV_txtChequeNo.Enabled = false;
                RFV_txtChequeDate.Enabled = false;
                divBankReferenceNo.Visible = false;
                divChequeDate.Visible = false;

                break;
        }
        #endregion

        Session[TableConstants.Contra] = objContra;
    }
    #endregion
    #region Dropdown Events
    protected void ddlPaymentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetBankDetails();
    }
    protected void ddlTransactionType_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetBankDetails();
        ddlDebitAccountNo_TextChanged(sender, e);
        ddlCreditAccountNo_TextChanged(sender, e);
    }
    protected void ddlDebitAccountNo_TextChanged(object sender, EventArgs e)
    {
        int Company_Bank_Id = new AppConvert(ddlDebitAccountNo.SelectedValue);
        if (Company_Bank_Id > 0)
        {
            if (ddlTransactionType.SelectedValue == "3" || ddlTransactionType.SelectedValue == "4") { } else { ddlDebitAccountNo.SelectedValue = "0"; Company_Bank_Id = 10000; }
            AppCompanyBank objCOmpanyBank = new AppCompanyBank(intRequestingUserID, Company_Bank_Id);
            ddlDebitBank.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
            ddlDebitBankBranch.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
            txtDebitLedger.Text = new AppConvert(objCOmpanyBank.Ledger.LedgerName);
            hdfDebitLedgerName.Value = new AppConvert(objCOmpanyBank.Ledger.LedgerName);
            hdfDebitLedgerID.Value = new AppConvert(objCOmpanyBank.Ledger_ID);

        }


    }
    protected void ddlCreditAccountNo_TextChanged(object sender, EventArgs e)
    {
        int Company_Bank_Id = new AppConvert(ddlCreditAccountNo.SelectedValue);
        if (Company_Bank_Id > 0)
        {
            if (ddlTransactionType.SelectedValue == "2" || ddlTransactionType.SelectedValue == "4") { } else { ddlCreditAccountNo.SelectedValue = "0"; Company_Bank_Id = 10000; }
            AppCompanyBank objCOmpanyBank = new AppCompanyBank(intRequestingUserID, Company_Bank_Id);
            ddlCreditBank.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
            ddlCreditBankBranch.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
            txtCreditLedger.Text = new AppConvert(objCOmpanyBank.Ledger.LedgerName);
            hdfCreditLedgerName.Value = new AppConvert(objCOmpanyBank.Ledger.LedgerName);
            hdfCreditLedgerID.Value = new AppConvert(objCOmpanyBank.Ledger_ID);
        }
    }
    #endregion
    #region Button Events
    protected void btnSave_Click(object sender, EventArgs e)
    {

        if(ddlTransactionType.SelectedValue=="4" && ddlDebitAccountNo.SelectedValue == ddlCreditAccountNo.SelectedValue)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Debit and credit account number sholud not be same.','2'" + ");", true);
            return;
        }
        AppContra ct = (AppContra)Session[TableConstants.Contra];
        if (ct.ID == 0)
        {
            ct.ContraNo = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "Contra", System.DateTime.Now,"Contra-",4, ResetCycle.Yearly));
        }
        ct.Company_ID = new AppConvert(LoginCompanyID);
        ct.VoucherType_ID = 22;
        ct.ContraDate = new AppConvert(txtPaymentDate.Text);
        ct.PaymentType_ID = new AppConvert(ddlPaymentType.SelectedValue);
        ct.ContraTypeID = new AppConvert(ddlTransactionType.SelectedValue);
        ct.Description = txtDescription.Text;
        ct.ChequeNo = new AppConvert(txtChequeNo.Text);
        ct.ChequeDate = new AppConvert(txtChequeDate.Text);
        ct.PayeeName = txtPayee.Text;
        ct.RefrenceNo = txtBankReferenceNo.Text;
        ct.Amount = new AppConvert(txtAmount.Text);
        ct.From_Ledger_ID = new AppConvert(hdfDebitLedgerID.Value);
        ct.To_Ledger_ID = new AppConvert(hdfCreditLedgerID.Value);
        ct.From_CompanyBank_ID = new AppConvert(ddlDebitAccountNo.SelectedValue);
        ct.To_CompanyBank_ID = new AppConvert(ddlCreditAccountNo.SelectedValue);
        ct.ModifiedBy = intRequestingUserID;
        ct.ModifiedOn = System.DateTime.Now;
        ct.Status = new AppConvert((int)RecordStatus.Created);
        ct.Type = 0;
        ct.Save();

        #region Add Ledger Entry
        LedgerEntry(LoginCompanyID, 101201, 1012, ct.To_Ledger_ID, ct.ID, ct.ContraDate, ct.Amount, 0, ct.ContraNo, ct.ModifiedBy, ct.ModifiedOn,0, ct.Status, ct.Type,
                        ct.ProfitCenter_ID, ct.CostCenter_ID, ct.VoucherType_ID, ct.Description);

        LedgerEntry(LoginCompanyID, 101202, 1012, ct.From_Ledger_ID, ct.ID, ct.ContraDate, 0, ct.Amount, ct.ContraNo, ct.ModifiedBy, ct.ModifiedOn, 0, ct.Status, ct.Type,
                                 ct.ProfitCenter_ID, ct.CostCenter_ID, ct.VoucherType_ID, ct.Description);
        #endregion
        Response.Redirect("T_Contra.aspx?Msg=1");




    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.Contra);
        Response.Redirect("T_ContraSummary.aspx");
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        AppContra ct = (AppContra)Session[TableConstants.Contra];
        ct.Status = new AppUtility.AppConvert((int)RecordStatus.Approve);
        ct.Save();
        Response.Redirect("T_Contra.aspx?Msg=2");
    }
    #endregion
}