﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppUtility;
using AppObjects;
public partial class M_Bank : BigSunPage
{
    AppBankTypeColl obj;
    protected void Page_Load(object sender, EventArgs e)
    {
  
        obj = new AppObjects.AppBankTypeColl(intRequestingUserID);
        Session[TableConstants.SessionBankType] = obj;
        if (!IsPostBack)
        {
           BindData();
        }
    }
    #region GridViewEvents
   
    protected void grdBank_PreRender(object sender, EventArgs e)
    {
        if (grdBank.Rows.Count > 0)
        {
            grdBank.UseAccessibleHeader = true;
            grdBank.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdBank_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if (strCommandName == "INSERT")
        {       

            TextBox txtBankName = grdBank.FooterRow.FindControl("txtNewBankName") as TextBox;
            TextBox txtAddress1 = grdBank.FooterRow.FindControl("txtNewAddress1") as TextBox;
            TextBox txtAddress2 = grdBank.FooterRow.FindControl("txtNewAddress2") as TextBox;
            TextBox txtIFSCCode = grdBank.FooterRow.FindControl("txtNewIFSCCode") as TextBox;
            TextBox txtMICRCode = grdBank.FooterRow.FindControl("txtNewMICRCode") as TextBox;
            HiddenField hdfZipCodeTypeID = grdBank.FooterRow.FindControl("hdfNewZipCodeTypeID") as HiddenField;
            CheckBox chkIsActiveBank = grdBank.FooterRow.FindControl("chkActiveBank") as CheckBox;
            CheckBox chkIsDefaultBank = grdBank.FooterRow.FindControl("chkDefaultBank") as CheckBox;

            SaveBankDetails("0", txtBankName.Text, txtAddress1.Text, txtAddress2.Text, "1", hdfZipCodeTypeID.Value, txtIFSCCode.Text, txtMICRCode.Text, chkIsActiveBank.Checked, chkIsDefaultBank.Checked);

            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Bank Successfully Added','1'" + ");", true);
            grdBank.EditIndex = -1;
            BindData();
        }
        else if (strCommandName == "CANCELADD")
        {
            grdBank.ShowFooter = false;
            BindData();
        }
        else if (strCommandName == "CANCELEDIT")
        {
            grdBank.EditIndex = -1;
            BindData();
        }
        else if(strCommandName=="ADDNEW")
        {
            grdBank.EditIndex = -1;
            grdBank.ShowFooter = true;
            BindData();
        }
    }
    protected void grdBank_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdBank.ShowFooter = false;
        grdBank.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdBank_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HiddenField hdfID = grdBank.Rows[e.RowIndex].FindControl("hdfID") as HiddenField;
        AppBankType SingleObj = new AppBankType(intRequestingUserID);
        SingleObj.ID = new AppConvert(hdfID.Value);
        SingleObj.Status = new AppConvert((int)RecordStatus.Deleted);
        SingleObj.Save();
     
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Bank Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void grdBank_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hdfBankID = grdBank.Rows[e.RowIndex].FindControl("hdfBankID") as HiddenField;
        TextBox txtBankName = grdBank.Rows[e.RowIndex].FindControl("txtBankName") as TextBox;
        TextBox txtAddress1 = grdBank.Rows[e.RowIndex].FindControl("txtAddress1") as TextBox;
        TextBox txtAddress2 = grdBank.Rows[e.RowIndex].FindControl("txtAddress2") as TextBox;
        TextBox txtIFSCCode = grdBank.Rows[e.RowIndex].FindControl("txtIFSCCode") as TextBox;
        TextBox txtMICRCode = grdBank.Rows[e.RowIndex].FindControl("txtMICRCode") as TextBox;
        HiddenField hdfZipCodeTypeID = grdBank.Rows[e.RowIndex].FindControl("hdfZipCodeTypeID") as HiddenField;
        CheckBox chkIsActiveBank = grdBank.Rows[e.RowIndex].FindControl("chkIsActiveBank") as CheckBox;
        CheckBox chkIsDefaultBank = grdBank.Rows[e.RowIndex].FindControl("chkIsDefaultBank") as CheckBox;

        SaveBankDetails(hdfBankID.Value,txtBankName.Text,txtAddress1.Text,txtAddress2.Text,"1",hdfZipCodeTypeID.Value,txtIFSCCode.Text,txtMICRCode.Text, chkIsActiveBank.Checked, chkIsDefaultBank.Checked);

        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Bank Successfully Updated','1'" + ");", true);
        grdBank.EditIndex = -1;
        BindData();
    }
    #endregion
    #region MiscFunctions
    public void SaveBankDetails(string Id,  string BankName, string Address1,string Address2,string CountryType_ID,string ZipCodeType_ID,string IFSCCode,string MICRCode, bool IsActive, bool IsDefault)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppBankType SingleObj = new AppBankType(intRequestingUserID);      
        SingleObj.ID = new AppConvert(Id);
        SingleObj.Name = new AppConvert(BankName);
        SingleObj.Address1 = new AppConvert(Address1);
        SingleObj.Address2 = new AppConvert(Address2);
        SingleObj.CountryType_ID = new AppConvert(CountryType_ID);
        SingleObj.ZipCodeType_ID = new AppConvert(ZipCodeType_ID);
        SingleObj.IFSCCode = new AppConvert(IFSCCode);
        SingleObj.MICRCode = new AppConvert(MICRCode);
        SingleObj.Status = (IsActive == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        if (IsDefault == true)
        {
           
            obj.SetDefaultBank();
        }
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;

        Session.Add(TableConstants.SessionBankType, obj);
        SingleObj.Save();
        BindData();
    }
    

    protected void BindData()
    {
        AppObjects.AppBankTypeColl objColl = new AppBankTypeColl(intRequestingUserID);
        if (!string.IsNullOrEmpty(txtBankName.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.BankType.Name, AppUtility.Operators.Equals, txtBankName.Text, 0);
        }
        if (!string.IsNullOrEmpty(txtAddress.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.BankType.Address1, AppUtility.Operators.Equals, txtAddress.Text, 0);
            objColl.AddCriteria(AppUtility.CollOperator.OR, AppObjects.BankType.Address2, AppUtility.Operators.Equals, txtAddress.Text, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
             
            objColl.AddCriteria(AppObjects.BankType.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.SessionBankType] = objColl;
        if (objColl != null)
        {
            grdBank.DataSource = objColl;
            grdBank.DataBind();
            if (objColl.Count <= 0)
            {
                AddEmptyRow();
            }
        }
       
       
    }
    public void AddEmptyRow()
    {
        AppBankTypeColl collOfContacts = new AppBankTypeColl(intRequestingUserID);
        AppBankType newContact = new AppBankType(intRequestingUserID);
        newContact.Name = "Add atleast one Bank";
        collOfContacts.Add(newContact);
        grdBank.DataSource = collOfContacts;
        grdBank.DataBind();
        grdBank.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdBank.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdBank.ShowFooter = true;
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}