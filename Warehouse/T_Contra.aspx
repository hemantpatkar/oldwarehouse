﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="T_Contra" Codebehind="T_Contra.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainDiv">
        <asp:UpdatePanel ID="up_PaymentVoucher" runat="server">
            <ContentTemplate>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <div id="divButtons" class="pull-right">
                                    <div class="btn-group">
                                        <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="btn btn-sm  btn-primary" OnClick="btnSave_Click"></asp:Button>
                                        <asp:Button runat="server" ID="btnApprove" Visible="false" Text="Approve" CssClass="btn  btn-sm  btn-primary" OnClick="btnApprove_Click"></asp:Button>
                                        <asp:Button runat="server" ID="btnCancel" CausesValidation="false" Text="Back" CssClass="btn  btn-sm  btn-primary" OnClick="btnCancel_Click"></asp:Button>
                                    </div>
                                </div>
                                <h4>Contra Details </h4>
                                <hr />
                                <div class="row">
                                    <div class=" col-lg-12">
                                        <asp:TabContainer runat="server" CssClass="Bigtab" ActiveTabIndex="0" ID="tabMain">
                                            <asp:TabPanel runat="server" HeaderText="Basic Info" TabIndex="0" ID="tblBasicInfo">
                                                <ContentTemplate>

                                                    <div id="divPaymentDate" class="col-lg-3">
                                                        <asp:Label ID="lblPaymentDate" runat="server" Text="Voucher Date" CssClass="bold"></asp:Label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtPaymentDate" TabIndex="0" runat="server" placeholder="Payment Date" CssClass="form-control input-sm" ValidationGroup="MainSave"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RFV_txtPaymentDate" runat="server" ControlToValidate="txtPaymentDate"></asp:RequiredFieldValidator>
                                                        <asp:MaskedEditExtender ID="Mee_txtPaymentDate" runat="server"
                                                            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtPaymentDate">
                                                        </asp:MaskedEditExtender>
                                                        <asp:CalendarExtender ID="Ce_PaymentDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtPaymentDate"
                                                            TargetControlID="txtPaymentDate">
                                                        </asp:CalendarExtender>
                                                    </div>
                                                    <div id="divVoucherNo" class="col-lg-3">
                                                        <asp:Label ID="lblVoucherNo" runat="server" Text="Voucher No" CssClass="bold"></asp:Label>
                                                        <asp:TextBox ID="txtVoucherNo" TabIndex="0" Enabled="false" runat="server" AutoCompleteType="None" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                    <div id="divTransactionType" class="col-lg-3">
                                                        <asp:Label ID="lblTransactionType" CssClass="bold " runat="server" Text="Transaction Type"></asp:Label>
                                                        <asp:DropDownList ID="ddlTransactionType" AutoPostBack="true" runat="server" CssClass="form-control input-sm" TabIndex="0" OnSelectedIndexChanged="ddlTransactionType_SelectedIndexChanged">
                                                            <asp:ListItem Text="Cash-Cash" Value="1" Selected="True"></asp:ListItem>
                                                            <asp:ListItem Text="Cash-Bank" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="Bank-Cash" Value="3"></asp:ListItem>
                                                            <asp:ListItem Text="Bank-Bank" Value="4"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div id="divPaymentType" class="col-lg-3">
                                                        <asp:Label ID="lblPaymentType" CssClass="bold " runat="server" Text="Payment Type"></asp:Label>
                                                        <asp:DropDownList ID="ddlPaymentType" AutoPostBack="true" runat="server" CssClass="form-control input-sm" TabIndex="0" OnSelectedIndexChanged="ddlPaymentType_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div id="divAmount" class="col-lg-4">
                                                        <asp:Label ID="lblAmount" runat="server" Text="Amount" CssClass="bold"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RFV_txtAmount" runat="server" ControlToValidate="txtAmount"></asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtAmount" runat="server" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>


                                                    <div class="col-lg-4">
                                                        <asp:Label ID="lblDebitLedger" runat="server" Text="Debit Ledger" CssClass="bold"></asp:Label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtDebitLedger" placeholder="Select Debit Ledger" runat="server" MaxLength="500" AutoPostBack="false" TabIndex="0" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfDebitLedgerName" />
                                                        <asp:HiddenField runat="server" ID="hdfDebitLedgerID" />
                                                        <asp:RequiredFieldValidator ID="RFV_txtDebitLedger" runat="server" ControlToValidate="txtDebitLedger"></asp:RequiredFieldValidator>
                                                        <asp:AutoCompleteExtender ID="ACEtxtDebitLedger" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtDebitLedger">
                                                        </asp:AutoCompleteExtender>
                                                    </div>

                                                    <div class="col-lg-4">
                                                        <asp:Label ID="lblCreditLedger" runat="server" Text="Credit Ledger" CssClass="bold"></asp:Label>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtCreditLedger" placeholder="Select Credit Ledger" runat="server" MaxLength="500" AutoPostBack="false" TabIndex="0" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                        </div>
                                                        <asp:HiddenField runat="server" ID="hdfCreditLedgerName" />
                                                        <asp:HiddenField runat="server" ID="hdfCreditLedgerID" />
                                                        <asp:RequiredFieldValidator ID="RFV_txtCreditLedger" runat="server" ControlToValidate="txtCreditLedger"></asp:RequiredFieldValidator>
                                                        <asp:AutoCompleteExtender ID="ACEtxtCreditLedger" runat="server"
                                                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                                            ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtCreditLedger">
                                                        </asp:AutoCompleteExtender>
                                                    </div>




                                                    <div id="divDescription" class="col-lg-12">
                                                        <asp:Label ID="lblDescription" runat="server" Text="Description" CssClass="bold"></asp:Label>
                                                        <asp:TextBox ID="txtDescription" TextMode="MultiLine" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:TabPanel>

                                        </asp:TabContainer>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="divBankDetails" runat="server" visible="false">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body" style="padding-top: 10px;">

                                <div id="divDebitBankDetail" visible="true" runat="server">
                                    <div id="divDebitAccountNo" class="col-lg-3">
                                        <asp:Label ID="lblDebitAccountNo" CssClass="bold " runat="server" Text="Debit Account Number"></asp:Label>
                                        <asp:DropDownList ID="ddlDebitAccountNo" OnTextChanged="ddlDebitAccountNo_TextChanged" AutoPostBack="true" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divDebitBankBranch" class="col-lg-3">
                                        <asp:Label ID="lblDebitBankBranch" CssClass="bold " runat="server" Text="Debit Bank Branch"></asp:Label>
                                        <asp:DropDownList ID="ddlDebitBankBranch" Enabled="false" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divDebitBank" class="col-lg-2">
                                        <asp:Label ID="lblDebitBank" CssClass="bold " runat="server" Text="Debit Bank"></asp:Label>
                                        <asp:DropDownList ID="ddlDebitBank" Enabled="false" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="divChequeNo" class="col-lg-2" runat="server">
                                    <asp:Label ID="lblChequeNo" runat="server" Text="Cheque No" CssClass="bold"></asp:Label>
                                     <asp:RequiredFieldValidator ID="RFV_txtChequeNo" runat="server" ControlToValidate="txtChequeNo"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtChequeNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div id="divChequeDate" class="col-lg-2" runat="server">
                                    <asp:Label ID="lblChequeDate" runat="server" Text="Cheque Date" CssClass="bold"></asp:Label>
                                    <div class="input-group">
                                        <asp:RequiredFieldValidator ID="RFV_txtChequeDate" runat="server" ControlToValidate="txtChequeDate"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtChequeDate" TabIndex="0" runat="server" placeholder="Cheque Date" CssClass="form-control input-sm"></asp:TextBox>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                    <asp:MaskedEditExtender ID="MEE_ChequeDate" runat="server"
                                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtChequeDate">
                                    </asp:MaskedEditExtender>
                                    <asp:CalendarExtender ID="CE_ChequeDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtChequeDate"
                                        TargetControlID="txtChequeDate">
                                    </asp:CalendarExtender>
                                </div>
                                <div id="divCreditBankDetail" visible="true" runat="server">
                                    <div id="divCreditAccountNo" class="col-lg-3">
                                        <asp:Label ID="lblCreditAccountNo" CssClass="bold " runat="server" Text="Credit Account Number"></asp:Label>
                                        <asp:DropDownList ID="ddlCreditAccountNo" AutoPostBack="true" OnTextChanged="ddlCreditAccountNo_TextChanged" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divCreditBankBranch" class="col-lg-3">
                                        <asp:Label ID="lblCreditBankBranch" CssClass="bold " runat="server" Text="Credit Bank Branch"></asp:Label>
                                        <asp:DropDownList ID="ddlCreditBankBranch" Enabled="false" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divCreditBank" class="col-lg-2">
                                        <asp:Label ID="lblCreditBank" CssClass="bold " runat="server" Text="Credit Bank"></asp:Label>
                                        <asp:DropDownList ID="ddlCreditBank" Enabled="false" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="divBankReferenceNo" runat="server" class="col-lg-2">
                                    <asp:Label ID="lblBankReferenceNo" runat="server" Text="Bank Reference Numbre" CssClass="bold"></asp:Label>
                                    <asp:TextBox ID="txtBankReferenceNo" TabIndex="0" placeholder="Enter Bank Reference No" runat="server" AutoCompleteType="None" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div id="divPayeeName" runat="server" class="col-lg-2">
                                    <asp:Label ID="lblPayeeName" runat="server" Text="Payee Name" CssClass="bold"></asp:Label>
                                    <asp:TextBox ID="txtPayee" TabIndex="0" placeholder="Enter Payee Name" runat="server" AutoCompleteType="None" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
