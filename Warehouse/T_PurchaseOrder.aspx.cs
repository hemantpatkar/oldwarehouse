﻿#region Author
/*
Name    :   Navnath Sonavane
Date    :   04/08/2016
Use     :   This form is used to add Purchase Order details  
*/
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.Services;
using AppUtility;
using Objects;
using AjaxControlToolkit;
#endregion
public partial class T_PurchaseOrder : BigSunPage
{
    #region Page_Load
    AppObjects.AppPurchaseOrder obj = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = new AppObjects.AppPurchaseOrder(intRequestingUserID);
        if (!IsPostBack)
        {
            Session[TableConstants.PurchaseOrder] = obj;
            ACEtxtVendor.ContextKey = "2" + ",0";
            string ID = Convert.ToString(Request.QueryString["PurchaseOrderID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int PurchaseOrderID = new AppUtility.AppConvert(ID);
                obj = new AppObjects.AppPurchaseOrder(intRequestingUserID, PurchaseOrderID);
                PopulateData();
            }
            else
            {
                DefaultData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
            }
            Session[TableConstants.PurchaseOrder] = obj;
            BindData();
            SetValueToGridFooter();
        }
    }
    #endregion
    #region Button Click Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppPurchaseOrder PurchaseOrder = (AppPurchaseOrder)Session[TableConstants.PurchaseOrder];
        PurchaseOrder.PurchaseOrderItemColl.RemoveAll(x=> x.Item_ID==0);
        if (PurchaseOrder.PurchaseOrderItemColl.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please add atleast one item','2'" + ");", true);
            return;
        }
        Session.Add(TableConstants.PurchaseOrder, PurchaseOrder);
        SavePurchaseOrderHeader();
        Session.Remove(TableConstants.PurchaseOrder);
        Response.Redirect("T_PurchaseOrder.aspx?Msg=1");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("T_PurchaseOrderSummary.aspx");
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
    }
    protected void btnAddLineItem_Click(object sender, EventArgs e)
    {
        AddEmptyRow(1);
        if (grdPurchaseOrderDetails.Rows.Count >= 0)
        {
            TextBox txtItem = (TextBox)grdPurchaseOrderDetails.Rows[0].FindControl("txtItem");
            AutoCompleteExtender ACEtxtItem = (AutoCompleteExtender)grdPurchaseOrderDetails.Rows[0].FindControl("ACEtxtItem");
            ACEtxtItem.ContextKey = hdfVendorID.Value;
            txtItem.Focus();
        }
    }
    #endregion
    #region Functions
    public void DefaultData()
    {
        txtPurchaseDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtDeliveryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtCompanyAddress.Text = base.DefaultCompanyAddress;
        hdfCompanyAddressName.Value = txtCompanyAddress.Text;
        hdfCompanyAddressID.Value = new AppConvert(this.DefaultCompanyAddressID);
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);

        
        txtDeliveryAddress.Text = this.DefaultDeliveryAddress;
        hdfDeliveryAddressName.Value = txtDeliveryAddress.Text;
        hdfDeliveryAddressID.Value = new AppConvert(this.DefaultDeliveryAddressID);
        ACEtxtDeliveryAddress.ContextKey = new AppConvert(Session[TableConstants.LoginCompanyID]);
        ACEtxtCompanyAddress.ContextKey = new AppConvert(Session[TableConstants.LoginCompanyID]);
    }
    public void PopulateData()
    {
        txtVendor.Enabled = false;
        txtPurchaseDate.Text = new AppConvert(obj.PurchaseOrderDate);
        txtDeliveryDate.Text = new AppConvert(obj.ExpectedDelivery);
        txtVendor.Text = new AppConvert(obj.Vendor_Company.Name);
        hdfVendorName.Value = txtVendor.Text;
        hdfVendorID.Value = new AppConvert(obj.Vendor_Company.ID);
        txtPurchaseOrderNo.Text = new AppConvert(obj.PurchaseOrderNumber);
        txtStatus.Text = obj.StatusName;
        txtTermsAndConditions.Text = new AppConvert(obj.TermsAndConditions.Statement);
        hdfTermsAndConditionsName.Value = txtTermsAndConditions.Text;
        hdfTermsAndConditionsID.Value = new AppConvert(obj.TermsAndConditions.ID);
        txtDiscount.Text = new AppConvert(obj.DiscountValue);
        hdfDiscountType.Value = new AppConvert(obj.DiscountType);
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);

       

        txtHeaderExchangeCurrency.Text = obj.CurrencyType.Name;
        hdfHeaderExchangeCurrencyName.Value = txtHeaderExchangeCurrency.Text;
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(obj.CurrencyType_ID);
        txtCompanyAddress.Text = obj.GetCompanyAddress;
        hdfCompanyAddressName.Value = txtCompanyAddress.Text;
        hdfCompanyAddressID.Value = new AppConvert(obj.CompanyAddress_ID);
        txtDeliveryAddress.Text = obj.GetDeliveryAddress;
        hdfDeliveryAddressName.Value = txtDeliveryAddress.Text;
        hdfDeliveryAddressID.Value = new AppConvert(obj.Delivery_CompanyAddress_ID);
        txtVendorAddress.Text = obj.VendorAddress;
        hdfVendorAddressName.Value = txtVendorAddress.Text;
        hdfVendorAddressID.Value = new AppConvert(obj.VendorAddressID);
        txtRemark.Text = obj.Remark;
        txtHeaderTax.Text = obj.TaxType.Name;
        hdfHeaderTaxName.Value = txtHeaderTax.Text;
        hdfHeaderTaxID.Value = new AppConvert(obj.TaxType_ID);
        txtHeaderTaxAmount.Text = new AppConvert(obj.TaxAmount);
        txtTotalAmount.Text = new AppConvert(obj.FinalAmount);
    }
    public void BindData()
    { 
        AppPurchaseOrder PurchaseOrder = (AppPurchaseOrder)Session[TableConstants.PurchaseOrder];
        if (PurchaseOrder == null)
        {
            PurchaseOrder = new AppPurchaseOrder(intRequestingUserID);
        }
        grdPurchaseOrderDetails.DataSource = PurchaseOrder.PurchaseOrderItemColl.Where(x=> x.Status==0);
        grdPurchaseOrderDetails.DataBind();
        if (PurchaseOrder.PurchaseOrderItemColl.Count <= 0)
        {
            AddEmptyRow(1);
            AutoCompleteExtender ACEtxtItem = (AutoCompleteExtender)grdPurchaseOrderDetails.Rows[0].FindControl("ACEtxtItem");
            ACEtxtItem.ContextKey = hdfVendorID.Value;
        }
    }
    public void SavePurchaseOrderHeader()
    {
        AppPurchaseOrder PO = (AppPurchaseOrder)Session[TableConstants.PurchaseOrder];
        if (PO.ID == 0)
        {
            PO.PurchaseOrderNumber = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "PO", System.DateTime.Now,"PO-", 4, ResetCycle.Yearly));
        }
        PO.Company_ID = new AppConvert(LoginCompanyID);
        PO.CompanyAddress_ID = new AppConvert(hdfCompanyAddressID.Value);
        PO.Vendor_Company_ID = new AppConvert(hdfVendorID.Value);
        PO.Vendor_CompanyAddress_ID = new AppConvert(hdfVendorAddressID.Value);
        PO.Delivery_CompanyAddress_ID = new AppConvert(hdfDeliveryAddressID.Value);
        PO.PurchaseOrderDate = new AppConvert(txtPurchaseDate.Text);
        PO.ExpectedDelivery = new AppConvert(txtDeliveryDate.Text);
        PO.TermsAndConditions_ID = new AppConvert(hdfTermsAndConditionsID.Value);
        PO.TaxType_ID = new AppConvert(hdfHeaderTaxID.Value);
        PO.CurrencyType_ID = new AppConvert(hdfHeaderExchangeCurrencyID.Value);
        PO.PurchaseOrderBaseAmount = new AppConvert(txtTotalAmount.Text);
        PO.TaxAmount = new AppConvert(txtHeaderTaxAmount.Text);
        PO.OtherCharges = 0;
        PO.FinalAmount = PO.HeaderFinalAmount;
        PO.DiscountValue = new AppConvert(txtDiscount.Text);
        PO.DiscountType = new AppConvert(hdfDiscountType.Value);
        PO.DiscountAmount = PO.CalDiscountAmount;
        PO.RoundAmount = 0;
        PO.Status = new AppConvert((int)RecordStatus.Created);
        PO.Type = obj.Type;
        PO.ModifiedBy = intRequestingUserID;
        PO.ModifiedOn = System.DateTime.Now;
        PO.Remark = txtRemark.Text;
        PO.Save();
    } 
    public void LineItemCalculation(int RowIndex)
    {
        AppPurchaseOrder PurchaseOrder = (AppPurchaseOrder)Session[TableConstants.PurchaseOrder];
        TextBox txtQuantity = (TextBox)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("txtQuantity");
        TextBox txtRate = (TextBox)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("txtRate");
        TextBox txtBasePrice = (TextBox)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("txtBasePrice");
        TextBox txtNetAmount = (TextBox)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("txtNetAmount");
        TextBox txtDiscount = (TextBox)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("txtDiscount");
        HiddenField hdfDiscountType = (HiddenField)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("hdfDiscountType");
        HiddenField hdfLineItemTaxID = (HiddenField)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("hdfLineItemTaxID");
        UserControl MoreDetail1 = (UserControl)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("MoreDetail1");
        HiddenField hdfProfitCenterID = (HiddenField)MoreDetail1.FindControl("hdfProfitCenterID");
       // string ProfitCentriID = ((UserControl_MoreDetail)MoreDetail1).hdfProfitCenterID.Value;
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].Company_ID = new AppConvert(LoginCompanyID);
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].CompanyAddress_ID = new AppConvert(hdfCompanyAddressID.Value);
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].Vendor_CompanyAddress_ID = new AppConvert(hdfVendorAddressID.Value);
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].Vendor_Company_ID = new AppConvert(hdfVendorID.Value);
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].Delivery_CompanyAddress_ID = new AppConvert(hdfDeliveryAddressID.Value);
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].Quantity = new AppUtility.AppConvert(txtQuantity.Text);
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].Rate = new AppUtility.AppConvert(txtRate.Text);
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].DiscountType = new AppUtility.AppConvert(new AppUtility.AppConvert(hdfDiscountType.Value) == 0 ? 1 : new AppUtility.AppConvert(hdfDiscountType.Value));
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].DiscountValue = new AppUtility.AppConvert(txtDiscount.Text);
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].DiscountAmount = PurchaseOrder.PurchaseOrderItemColl[RowIndex].CalDiscountAmount;
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].ItemBaseAmount = PurchaseOrder.PurchaseOrderItemColl[RowIndex].PostDiscountPrice;
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].TaxType_ID = new AppUtility.AppConvert(hdfLineItemTaxID.Value);
        if (string.IsNullOrEmpty(hdfLineItemTaxID.Value) == false && hdfLineItemTaxID.Value != "0")
        {
            PurchaseOrder.PurchaseOrderItemColl[RowIndex].TaxType.CalculateTax(new AppConvert(PurchaseOrder.PurchaseOrderItemColl[RowIndex].ItemBaseAmount), 0, 0);
            PurchaseOrder.PurchaseOrderItemColl[RowIndex].TaxAmount = PurchaseOrder.PurchaseOrderItemColl[RowIndex].TaxType.TaxValue;
        }
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].NetAmount = PurchaseOrder.PurchaseOrderItemColl[RowIndex].CalNetAmount;
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].FinalAmount = PurchaseOrder.PurchaseOrderItemColl[RowIndex].CalFinalAmount;
         
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].ModifiedBy = intRequestingUserID;
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].ModifiedOn = System.DateTime.Now;
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].Status = new AppConvert((int)RecordStatus.Created);
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].Type = new AppConvert((int)DefaultNormalType.Normal);
        Session.Remove(TableConstants.PurchaseOrder);
        Session.Add(TableConstants.PurchaseOrder, PurchaseOrder);
        BindData();
        SetValueToGridFooter();
        HeaderCalculation();
    }
    public void SetValueToGridFooter()
    {
        AppPurchaseOrder PurchaseOrder = (AppPurchaseOrder)Session[TableConstants.PurchaseOrder];
        Label lblFooterCurrencyAmount = (Label)grdPurchaseOrderDetails.FooterRow.FindControl("lblFooterCurrencyAmount");
        lblFooterCurrencyAmount.Text = Convert.ToDecimal(PurchaseOrder.PurchaseOrderItemColl.Where(x => x.Status == 0).Sum(x => x.ConvertedCurrencyAmount)).ToString("#,##0.00");
        Label lblFooterNetAmount = (Label)grdPurchaseOrderDetails.FooterRow.FindControl("lblFooterNetAmount");
        lblFooterNetAmount.Text = Convert.ToDecimal(PurchaseOrder.PurchaseOrderItemColl.Where(x => x.Status == 0).Sum(x => x.FinalAmount)).ToString("#,##0.00");
        Label lblFooterTax = (Label)grdPurchaseOrderDetails.FooterRow.FindControl("lblFooterTax");
        lblFooterTax.Text = Convert.ToDecimal(PurchaseOrder.PurchaseOrderItemColl.Where(x => x.Status == 0).Sum(x => x.TaxAmount)).ToString("#,##0.00");
        Label lblFooterDiscount = (Label)grdPurchaseOrderDetails.FooterRow.FindControl("lblFooterDiscount");
        lblFooterDiscount.Text = Convert.ToDecimal(PurchaseOrder.PurchaseOrderItemColl.Where(x => x.Status == 0).Sum(x => x.DiscountValue)).ToString("#,##0.00");
        Label lblFooterBasePrice = (Label)grdPurchaseOrderDetails.FooterRow.FindControl("lblFooterBasePrice");
        lblFooterBasePrice.Text = Convert.ToDecimal(PurchaseOrder.PurchaseOrderItemColl.Where(x => x.Status == 0).Sum(x => x.BasePrice)).ToString("#,##0.00");
        Label lblFooterQuantity = (Label)grdPurchaseOrderDetails.FooterRow.FindControl("lblFooterQuantity");
        lblFooterQuantity.Text = Convert.ToDecimal(PurchaseOrder.PurchaseOrderItemColl.Where(x => x.Status == 0).Sum(x => x.Quantity)).ToString("#,##0.00");
       
    }
   

    public void HeaderCalculation()
    {
        AppPurchaseOrder PurchaseOrder = (AppPurchaseOrder)Session[TableConstants.PurchaseOrder];
        PurchaseOrder.DiscountType = new AppConvert(hdfDiscountType.Value);
        PurchaseOrder.DiscountValue = new AppConvert(txtDiscount.Text);
        PurchaseOrder.FinalAmount = PurchaseOrder.PurchaseOrderItemColl.Sum(x => x.NetAmount);
        txtTotalAmount.Text = new AppConvert(PurchaseOrder.PostDiscountPrice);

        if (string.IsNullOrEmpty(hdfHeaderTaxID.Value) == false && hdfHeaderTaxID.Value != "0")
        {
            PurchaseOrder.TaxType_ID = new AppConvert(hdfHeaderTaxID.Value);
            PurchaseOrder.TaxType.CalculateTax(new AppConvert(txtTotalAmount.Text), 0, 0);
            txtHeaderTaxAmount.Text = new AppConvert(PurchaseOrder.TaxType.TaxValue);
            txtTotalAmount.Text = new AppConvert(PurchaseOrder.PostDiscountPrice + PurchaseOrder.TaxType.TaxValue);
        }
        else {
            txtHeaderTaxAmount.Text = "0";
        }
        PurchaseOrder.AllocateAmount();

        Session[TableConstants.SaleOrderSession] = PurchaseOrder;

    }
    public void AddEmptyRow(int NumberOfRows)
    {
        AppPurchaseOrder PurchaseOrder = (AppPurchaseOrder)Session[TableConstants.PurchaseOrder];
        if (PurchaseOrder == null)
        {
            PurchaseOrder = new AppPurchaseOrder(intRequestingUserID);
        }
        AppPurchaseOrderItem PurchaseOrderItem = new AppPurchaseOrderItem(intRequestingUserID);
        for (int i = 0; i < NumberOfRows; i++)
        {
            if (PurchaseOrder.PurchaseOrderItemColl.Count == 0)
            {
                PurchaseOrder.PurchaseOrderItemColl.Add(PurchaseOrderItem);
            }
            else
            {
                PurchaseOrder.PurchaseOrderItemColl.Insert(0, PurchaseOrderItem);
            }
            Session.Add(TableConstants.PurchaseOrder, PurchaseOrder);
        }
        BindData();
    }
    #endregion
    #region Text Box Event
    protected void txtVendor_TextChanged(object sender, EventArgs e)
    {
        ACEtxtVendorAddress.ContextKey = hdfVendorID.Value;
        obj.Vendor_Company_ID = new AppConvert(hdfVendorID.Value);
        txtTermsAndConditions.Text = obj.DefaultTermsAndConditionsStatement;
        hdfTermsAndConditionsName.Value = obj.DefaultTermsAndConditionsStatement;
        hdfTermsAndConditionsID.Value = new AppConvert(obj.DefaultTermsAndConditionsID);       
        txtVendorAddress.Text = obj.VendorAddress;
        hdfVendorAddressName.Value = txtVendorAddress.Text;
        txtHeaderExchangeCurrency.Text = new AppConvert(obj.VendorCurrencyName);
        hdfHeaderExchangeCurrencyName.Value = txtHeaderExchangeCurrency.Text;
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(obj.VendorCurrencyID);
        hdfVendorAddressID.Value = new AppConvert(obj.VendorAddressID);
        obj.GetTopVendorItem(5);
        Session.Add(TableConstants.PurchaseOrder, obj);
        BindData();
    }
    protected void txtItem_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfItemID = (HiddenField)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("hdfItemID");
        TextBox txtQuantity = (TextBox)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("txtQuantity");
        AppPurchaseOrder PurchaseOrder = (AppPurchaseOrder)Session[TableConstants.PurchaseOrder];
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].Item_ID = new AppUtility.AppConvert(hdfItemID.Value);
        PurchaseOrder.PurchaseOrderItemColl[RowIndex].Company_ID = new AppConvert(hdfVendorID.Value);
        Session.Remove(TableConstants.PurchaseOrder);
        Session.Add(TableConstants.PurchaseOrder, PurchaseOrder);
        BindData();
        txtQuantity.Focus();
    }
    protected void txtQuantity_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtRate = (TextBox)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("txtRate");
        LineItemCalculation(RowIndex);
        txtRate.Focus();
    }
    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtDiscount = (TextBox)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("txtDiscount");
        LineItemCalculation(RowIndex);
        txtDiscount.Focus();
    }
    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtLineItemTax = (TextBox)grdPurchaseOrderDetails.Rows[RowIndex].FindControl("txtLineItemTax");
        LineItemCalculation(RowIndex);
        txtLineItemTax.Focus();
    }
    protected void txtLineItemTax_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
        Button btnAddLineItem = (Button)grdPurchaseOrderDetails.HeaderRow.FindControl("btnAddLineItem");
        LineItemCalculation(RowIndex);
        btnAddLineItem.Focus();
    }
    protected void txtHeaderTax_TextChanged(object sender, EventArgs e)
    {
        HeaderCalculation();
    }
    protected void txtDiscount_TextChanged1(object sender, EventArgs e)
    {
        HeaderCalculation();
    }
    #endregion
    #region Grid View Events
    protected void grdPurchaseOrderDetails_PreRender(object sender, EventArgs e)
    {
        if (grdPurchaseOrderDetails.Rows.Count > 0)
        {
            grdPurchaseOrderDetails.UseAccessibleHeader = true;
            grdPurchaseOrderDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdPurchaseOrderDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
            AppPurchaseOrder PurchaseOrder = (AppPurchaseOrder)Session[TableConstants.PurchaseOrder];                        
            PurchaseOrder.PurchaseOrderItemColl.Where(x => x.Status == 0).ToList()[row.RowIndex].Status= new AppConvert((int)RecordStatus.Deleted);         
            Session.Add(TableConstants.PurchaseOrder, PurchaseOrder);
            BindData();
            SetValueToGridFooter();
        }
        //if (e.CommandName == "Remove")
        //{
        //    GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
        //    div PurchaseOrder = (AppPurchaseOrder)Session[TableConstants.PurchaseOrder];
        //    popup_Hide("ModalPopupExtender1", txtVendor.ClientID, this);
        //}
    }
    #endregion
}