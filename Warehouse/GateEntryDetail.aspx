﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Culture="en-gb" Inherits="GateEntryDetail" Codebehind="GateEntryDetail.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function clickButton(e, buttonid) {
            var evt = (e || window.event);
            var keyCode = (evt.keyCode || evt.which);
            var bt = document.getElementById(buttonid);
            if (bt) {
                if (keyCode == 13) {
                    bt.click();
                    return false;
                }
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel runat="server" ID="UpdateVechicalType">
        <ContentTemplate>
            <div class="panel" id="div_Vehicletype" runat="server" style="display: none">
                <div class="panel-content">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-10">
                                <div class="heading">
                                    <asp:Label runat="server" ID="lblVehicleTypeName" Text="Vehical Type" />
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="btn-group">
                                    <asp:Button ID="btnSaveVehicleType" runat="server" TabIndex="2" Text="Save" ValidationGroup="VehicleType" CssClass="btn btn-success btn-sm" OnClick="btnSaveVehicleType_Click" />

                                    <asp:Button ID="btnNewVehicleType" runat="server" Text="New" CausesValidation="false" TabIndex="2" CssClass="btn btn-primary btn-sm" OnClick="btnNewVehicleType_Click" />

                                    <asp:Button ID="btnBackVehicleType" runat="server" Text="Close" CausesValidation="False" TabIndex="2" CssClass="btn btn-primary btn-sm" OnClick="btnBackVehicleType_Click" />
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-lg-3">
                                <asp:Label runat="server" Text="Code" ID="lblVehicleTypeCode" CssClass="bold" />
                                <asp:Label ID="Label1" runat="server" CssClass="text-danger" Text="*" />
                                <asp:TextBox runat="server" ID="txtVehicleTypeCode" MaxLength="20" TabIndex="1" CssClass="form-control input-sm" ValidationGroup="VehicleType" />
                                <asp:RequiredFieldValidator ID="rfvtxtVehicleTypeCode" runat="server" ControlToValidate="txtVehicleTypeCode" Display="Dynamic" SetFocusOnError="true"
                                    ErrorMessage="Please Enter Vehicle Type Code" CssClass="text-danger" ValidationGroup="VehicleType"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label runat="server" Text="Name" ID="lblTypeName" CssClass="bold" />
                                <asp:Label ID="Label4" runat="server" CssClass="text-danger" Text="*" />
                                <asp:TextBox runat="server" ID="txtVehicleTypeName" MaxLength="50" TabIndex="1" CssClass="form-control input-sm" ValidationGroup="VehicleType" />
                                <asp:RequiredFieldValidator ID="rfvtxtVehicletypeName" runat="server" ControlToValidate="txtVehicleTypeName" Display="Dynamic" SetFocusOnError="true" CssClass="text-danger"
                                    ErrorMessage="Please Enter Vehicle Type Name" ValidationGroup="VehicleType"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label runat="server" Text="Capacity" ID="Label2" CssClass="bold" />
                                <asp:TextBox runat="server" ID="txtCapacity" MaxLength="50" TabIndex="1" CssClass="form-control input-sm" ValidationGroup="VehicleType" />

                            </div>
                            <div class="col-lg-3">
                                <asp:Label runat="server" Text="Standard TAT" ID="Label3" CssClass="bold" />
                                <asp:TextBox runat="server" ID="txtStandardTAT" MaxLength="50" TabIndex="1" CssClass="form-control input-sm" ValidationGroup="VehicleType" />
                                <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtStandardTAT" FilterType="Numbers">
                                </asp:FilteredTextBoxExtender>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <i class="fa fa-truck"></i>
                        <asp:Label runat="server" ID="lblGateRegisterDetail" Text="Gate Entry" />
                        &nbsp;/&nbsp;
                           <asp:Label ID="txtGateRegNo" ForeColor="Maroon" runat="server" />
                        <asp:HiddenField ID="hdfGRID" runat="server" />
                        <asp:HiddenField ID="hdfStatus" Value="0" runat="server" />
                        <div class="pull-right">
                            <asp:LinkButton ID="lnkSearch" runat="server" OnClick="lnkSearch_Click" CausesValidation="false" TabIndex="4">
                                <i class="fa fa-search"></i>
                            </asp:LinkButton>
                            <asp:HyperLink ID="btnPrint" runat="server" ToolTip="View Report" Target="_blank"
                                TabIndex="4" NavigateUrl="">
                              <i class="fa fa-print"></i>
                            </asp:HyperLink>
                        </div>
                    </div>
                    <asp:CompareValidator ID="cmpDate" runat="server" ControlToCompare="txtEntryDate" CssClass="text-danger"
                        ControlToValidate="txtExitDate" Display="Dynamic" ErrorMessage="Exit Date Should Not Be Less Than Entry Date"
                        Operator="GreaterThanEqual" SetFocusOnError="True" Type="Date" ValidationGroup="GRDet"></asp:CompareValidator>
                    <asp:RangeValidator ID="Rangeval" runat="server" Type="Date" ErrorMessage="" CssClass="text-danger"
                        Display="Dynamic" ControlToValidate="txtEntryDate" SetFocusOnError="True" ValidationGroup="GRDet"></asp:RangeValidator>
                    <asp:RangeValidator ID="Rangback" runat="server" Type="Date" ErrorMessage="" CssClass="text-danger"
                        Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtEntryDate" ValidationGroup="GRDet"></asp:RangeValidator>
                    <asp:CompareValidator ID="cmptodate" ValidationGroup="GRDet" runat="server" ControlToValidate="txtExitDate" Display="Dynamic" ErrorMessage="Invalid Out Date" SetFocusOnError="true" Operator="lessThanEqual" Type="Date" CssClass="text-danger"></asp:CompareValidator>

                </div>
            </div>
            <div class="row btnrow">
                <div class="col-lg-6">
                    <ul class="nav navbar-nav navbar-left">
                        <li></li>
                        <li>
                            <asp:LinkButton ID="btnSave" runat="server" TabIndex="3" Text="" ValidationGroup="GRDet" OnClick="btnSave_Click" />
                        </li>
                        <li>
                            <asp:LinkButton ID="btnNewDet" runat="server" Text="" CausesValidation="false" TabIndex="3" OnClick="btnNewDet_Click" />
                        </li>
                        <li></li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <div class="pull-right">
                        <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                        <ul class="nav nav-pills nav-wizard" id="CustomerType" >
                            <li id="C0" class="active">
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">New</a><div class="nav-arrow"></div>
                            </li>
                            <li id="C1">
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Pending</a><div class="nav-arrow"></div>
                            </li>
                            <li id="C3">
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Used</a><div class="nav-arrow"></div>
                            </li>
                            <li id="C4">
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Out</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr />
            <div class="panel">
                <div class="panel-content">
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-lg-3">
                                <asp:Label ID="lblPartyName" runat="server" CssClass="bold" />
                                <asp:RequiredFieldValidator ID="reqpartern" runat="server" ErrorMessage="( Please Enter )" SetFocusOnError="true" CssClass="text-danger" Display="Dynamic"
                                    ValidationGroup="GRDet" ControlToValidate="txtCustomer"></asp:RequiredFieldValidator>
                                <div class="input-group">
                                    <asp:TextBox ID="txtCustomer" TabIndex="2" CssClass="form-control input-sm" runat="server"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label ID="lblVehicleID" runat="server" />
                                <asp:Label ID="Label19" runat="server" CssClass="text-danger" Text="*" />
                                <asp:RequiredFieldValidator ID="rfvVehicle" runat="server" ControlToValidate="txtVehical"
                                    CssClass="text-danger" ErrorMessage="( Please Select )" SetFocusOnError="true" Display="Dynamic"
                                    ValidationGroup="GRDet"></asp:RequiredFieldValidator>
                                <asp:LinkButton runat="server" OnClick="lnkAddVehicalType_Click" ID="lnkAddVehicalType">( Add )</asp:LinkButton>
                                <div class="input-group">
                                    <asp:TextBox ID="txtVehical" TabIndex="2" runat="server" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-truck"></i></span>
                                </div>
                                <asp:AutoCompleteExtender ID="ACEtxtVehical" runat="server"
                                    CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                    CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                    CompletionSetCount="10" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                    FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch"
                                    ServiceMethod="VehicleTypeSearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                    UseContextKey="true" ContextKey="0" TargetControlID="txtVehical">
                                </asp:AutoCompleteExtender>
                                <div style="display: none">
                                    <asp:HiddenField ID="hdfVehicalID" runat="server" />
                                    <asp:HiddenField ID="hdfVehicalName" runat="server"></asp:HiddenField>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <asp:Label ID="lblVehicleNo" runat="server" CssClass="bold"></asp:Label>
                                <asp:Label ID="Label5" runat="server" CssClass="text-danger" Text="*" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtVehicleNo"
                                    CssClass="text-danger" ErrorMessage="( Please Enter )" SetFocusOnError="true" Display="Dynamic"
                                    ValidationGroup="GRDet"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtVehicleNo" runat="server" TabIndex="2" CssClass="form-control input-sm"></asp:TextBox>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label ID="lblDriverName" runat="server" CssClass="bold" />
                                <asp:Label ID="Label13" runat="server" CssClass="text-danger" Text="*" />
                                <asp:RequiredFieldValidator ID="rfvDriverName" runat="server" ControlToValidate="txtDriverName" CssClass="text-danger"
                                    Display="Dynamic" ErrorMessage="( Please Enter )" SetFocusOnError="true" ValidationGroup="GRDet"></asp:RequiredFieldValidator>
                                <div class="input-group">
                                    <asp:TextBox ID="txtDriverName" runat="server" CssClass="form-control input-sm" MaxLength="100"
                                        TabIndex="2" ValidationGroup="GRDet" />
                                    <span class="input-group-addon"><i class="fa fa-car"></i></span>
                                </div>

                            </div>
                            <div class="col-lg-3">
                                <asp:Label ID="lblMobileNo" runat="server" CssClass="bold" />
                                <asp:Label ID="Label18" runat="server" CssClass="text-danger" Text="*" />
                                <asp:RequiredFieldValidator ID="rfvMobNo" runat="server" ControlToValidate="txtMobileNo"
                                    Display="Dynamic" ErrorMessage="( Please Enter )" SetFocusOnError="true" ValidationGroup="GRDet" CssClass="text-danger"></asp:RequiredFieldValidator>
                                <div class="input-group">
                                    <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control input-sm" MaxLength="11"
                                        TabIndex="2" ValidationGroup="GRDet" />
                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                </div>
                                <asp:FilteredTextBoxExtender ID="fteMobileNo" runat="server" TargetControlID="txtMobileNo"
                                    FilterType="Numbers,Custom" ValidChars=",,+">
                                </asp:FilteredTextBoxExtender>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label ID="lblLicenseNo" runat="server" CssClass="bold" />
                                <asp:TextBox ID="txtLicenseNo" runat="server" CssClass="form-control input-sm" MaxLength="100" TabIndex="2" ValidationGroup="GRDet" />
                            </div>

                            <div class="col-lg-3">
                                <asp:Label ID="lblTransporterName" runat="server" CssClass="bold"></asp:Label>
                                <asp:TextBox ID="txtTransporterName" runat="server" TabIndex="2" CssClass="form-control input-sm"></asp:TextBox>
                            </div>


                            <div class="col-lg-3" id="divChallanNopop" runat="server">
                                <asp:Label ID="lblChallanNopop" runat="server" CssClass="bold"></asp:Label>
                                <asp:TextBox ID="txtChallanNopop" ValidationGroup="Inward" runat="server" CssClass="form-control input-sm" TabIndex="2"></asp:TextBox>
                            </div>

                            <div class="col-lg-3">
                                <asp:Label ID="lblEntryOn" runat="server" CssClass="bold"></asp:Label>
                                <div class="input-group">
                                    <asp:TextBox ID="txtEntryDate" runat="server" CssClass="form-control input-sm" TabIndex="2"
                                        ValidationGroup="GRDet" OnTextChanged="txtEntryDate_TextChanged" AutoPostBack="True"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <asp:MaskedEditExtender ID="txtEffectiveFrom_MaskedEditExtender" runat="server" CultureAMPMPlaceholder=""
                                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                    CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtEntryDate">
                                </asp:MaskedEditExtender>
                                <asp:CalendarExtender Format="dd/MM/yyyy" ID="CalendarExtender1" TargetControlID="txtEntryDate"
                                    runat="server">
                                </asp:CalendarExtender>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label ID="lblEntryHH" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="rfvEntryHour" runat="server" ControlToValidate="txtEntryHour" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="( Please Enter )" Enabled="true" ValidationGroup="GRDet"></asp:RequiredFieldValidator>
                                <div class="input-group">
                                    <asp:TextBox ID="txtEntryHour" MaxLength="2" runat="server" CssClass="form-control input-sm"
                                        TabIndex="2" ValidationGroup="GRDet"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                </div>
                                <asp:MaskedEditExtender ID="meEntryHour" Mask="99" MaskType="Number" ClearMaskOnLostFocus="true"
                                    TargetControlID="txtEntryHour" runat="server" Enabled="true">
                                </asp:MaskedEditExtender>
                                <asp:MaskedEditValidator ID="mevEntryHour" ControlToValidate="txtEntryHour" ValidationGroup="GRDet"
                                    ControlExtender="meEntryHour" MaximumValue="23" MinimumValue="0" InvalidValueMessage="Enter Valid Hour"
                                    MaximumValueMessage="Please Enter Valid Hour" InvalidValueBlurredMessage="Enter Valid Hour"
                                    runat="server" CssClass="text-danger" Enabled="true"></asp:MaskedEditValidator>

                            </div>
                            <div class="col-lg-3">
                                <asp:Label ID="lblEntryMM" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="rfvEntryMinute" runat="server" ControlToValidate="txtEntryMinute" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="( Please Enter )" Enabled="true" ValidationGroup="GRDet"></asp:RequiredFieldValidator>
                                <div class="input-group">
                                    <asp:TextBox ID="txtEntryMinute" runat="server" CssClass="form-control input-sm"
                                        TabIndex="2" ValidationGroup="GRDet" MaxLength="2"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                </div>
                                <asp:MaskedEditExtender ID="meEntryMinute" Mask="99" MaskType="Number" ClearMaskOnLostFocus="true"
                                    TargetControlID="txtEntryMinute" runat="server" Enabled="true">
                                </asp:MaskedEditExtender>
                                <asp:MaskedEditValidator ID="mevEntryMinute" ControlToValidate="txtEntryMinute" ValidationGroup="GRDet"
                                    ControlExtender="meEntryMinute" MaximumValue="59" MinimumValue="0" InvalidValueMessage="Enter Valid Minutes"
                                    MaximumValueMessage="Please Enter Valid Minutes" InvalidValueBlurredMessage="Enter Valid Minutes"
                                    runat="server" CssClass="text-danger" Enabled="true"></asp:MaskedEditValidator>

                            </div>
                            <div class="col-lg-3">
                                <asp:Label ID="lblnounit" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="requnit" runat="server" ControlToValidate="txtunit" CssClass="text-danger"
                                    Display="Dynamic" ErrorMessage="( Please Enter )" SetFocusOnError="true" ValidationGroup="GRDet"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtunit" runat="server" CssClass="form-control input-sm" MaxLength="50"
                                    TabIndex="2" ValidationGroup="GRDet" />

                            </div>

                            <div class="col-lg-3">
                                <asp:Label ID="lblExitOn" runat="server" CssClass="bold"></asp:Label>
                                <div class="input-group">
                                    <asp:TextBox ID="txtExitDate" runat="server" CssClass="form-control input-sm" TabIndex="2" ValidationGroup="GRDet"
                                        OnTextChanged="txtExitDate_TextChanged" AutoPostBack="True" CausesValidation="True"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder=""
                                    CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                    CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtExitDate">
                                </asp:MaskedEditExtender>
                                <asp:CalendarExtender Format="dd/MM/yyyy" ID="CalendarExtender2" TargetControlID="txtExitDate"
                                    runat="server">
                                </asp:CalendarExtender>
                            </div>
                            <div class="col-lg-3">
                                <asp:Label ID="lblExitHH" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="rfvExitHour" runat="server" ControlToValidate="txtExitHour" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="( Please Enter )" Enabled="False" ValidationGroup="GRDet"></asp:RequiredFieldValidator>
                                <div class="input-group">
                                    <asp:TextBox ID="txtExitHour" MaxLength="2" runat="server" CssClass="form-control input-sm"
                                        TabIndex="2" ValidationGroup="GRDet"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                </div>
                                <asp:MaskedEditExtender ID="meExitHour" Mask="99" MaskType="Number" ClearMaskOnLostFocus="true"
                                    TargetControlID="txtExitHour" runat="server" Enabled="false">
                                </asp:MaskedEditExtender>
                                <asp:MaskedEditValidator ID="MEVExitHour" ControlToValidate="txtExitHour" ValidationGroup="GRDet"
                                    ControlExtender="meExitHour" MaximumValue="23" MinimumValue="0" InvalidValueMessage="Enter Valid Hour"
                                    MaximumValueMessage="Please Enter Valid Hour" InvalidValueBlurredMessage="Enter Valid Hour"
                                    runat="server" CssClass="text-danger" Enabled="false"></asp:MaskedEditValidator>

                            </div>
                            <div class="col-lg-3">
                                <asp:Label ID="lblExitMM" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="rfvExitMinutes" runat="server" ControlToValidate="txtExitMinutes" Display="Dynamic"
                                    CssClass="text-danger" ErrorMessage="( Please Enter )" Enabled="False" ValidationGroup="GRDet"></asp:RequiredFieldValidator>
                                <div class="input-group">
                                    <asp:TextBox ID="txtExitMinutes" runat="server" CssClass="form-control input-sm"
                                        TabIndex="2" ValidationGroup="GRDet" MaxLength="2"></asp:TextBox>
                                    <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                </div>
                                <asp:MaskedEditExtender ID="MEExitMinues" Mask="99" MaskType="Number" ClearMaskOnLostFocus="true"
                                    TargetControlID="txtExitMinutes" runat="server" Enabled="false">
                                </asp:MaskedEditExtender>
                                <asp:MaskedEditValidator ID="MEVExitMinutes" ControlToValidate="txtExitMinutes" ValidationGroup="GRDet"
                                    ControlExtender="MEExitMinues" MaximumValue="59" MinimumValue="0" InvalidValueMessage="Enter Valid Minutes"
                                    MaximumValueMessage="Please Enter Valid Minutes" InvalidValueBlurredMessage="Enter Valid Minutes"
                                    runat="server" CssClass="text-danger" Enabled="false"></asp:MaskedEditValidator>

                            </div>
                            <div class="col-lg-3">
                                <asp:Label ID="lbloutunit" runat="server"></asp:Label>
                                <asp:RequiredFieldValidator ID="reqoutunit" runat="server" ControlToValidate="txtoutunit" CssClass="text-danger"
                                    Display="Dynamic" ErrorMessage="( Please Enter )" SetFocusOnError="true" ValidationGroup="GRDet"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtoutunit" runat="server" CssClass="form-control input-sm" MaxLength="50"
                                    TabIndex="2" ValidationGroup="GRDet" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnNewDet" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnSaveVehicleType" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnBackVehicleType" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnNewVehicleType" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>

    <script>

        function pageLoad() {
            DiplayCustomerType();
        }

        function DiplayCustomerType() {
            debugger
            var val = $get('<%=hdfStatus.ClientID %>');
            var id = 'C' + val.value;

            var CustomerType = document.getElementById('CustomerType');
            for (var i = 0; i < CustomerType.childElementCount; i++) {
                if (CustomerType.children[i].id == id) {
                    var as = CustomerType.children[i].id;
                    document.getElementById(CustomerType.children[i].id).className = "active";
                }
                else {
                    var sta = CustomerType.children[i].id;
                    document.getElementById(CustomerType.children[i].id).className = "";
                }
            }
        }

    </script>

</asp:Content>

