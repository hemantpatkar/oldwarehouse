﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;

public partial class M_TaxSummary : BigSunPage
{
    AppTaxTypeColl objTaxTypeColl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }

    protected void Search_Click(object sender, EventArgs e)
    {

    }

    protected void lnkNewTax_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.TaxSessionCollObj);
        Response.Redirect("TaxConfigurationDetails.aspx");
    }

    protected void grdTaxSummary_PreRender(object sender, EventArgs e)
    {
        if (grdTaxSummary.Rows.Count > 0)
        {
            grdTaxSummary.UseAccessibleHeader = true;
            grdTaxSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

    protected void grdTaxSummary_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        objTaxTypeColl = (AppTaxTypeColl)Session[TableConstants.TaxSessionCollObj];
        objTaxTypeColl[e.RowIndex].Status = -10;
        objTaxTypeColl[e.RowIndex].Save();
        Session.Add(TableConstants.TaxSessionCollObj, objTaxTypeColl);
    }

    #region BindData   
    protected void BindData()
    {
        AppTaxTypeColl objTaxCollType = new AppTaxTypeColl(intRequestingUserID);
        objTaxCollType.Search();

        grdTaxSummary.DataSource = objTaxCollType;
        grdTaxSummary.DataBind();
        Session.Add(TableConstants.TaxSessionCollObj, objTaxCollType);
    }
    #endregion
}