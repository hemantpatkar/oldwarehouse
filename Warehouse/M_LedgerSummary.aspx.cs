﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class M_LedgerSummary : BigSunPage
{
    AppLedgerColl objLedgerColl = null;
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        objLedgerColl = (AppLedgerColl)Session[TableConstants.LedgerCollObj];
        if (!IsPostBack)
        {
            CommonFunctions.BindDropDown(intRequestingUserID, Components.AccountType, ddlAccountType, "AccountTypeName", "ID");
            CommonFunctions.BindDropDown(intRequestingUserID, Components.LedgerType, ddlLedgerType, "LedgerTypeName", "ID");
            BindData();
        }
    }
    #endregion
    protected void lbtnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }   
    protected void grdLedgerSummary_PreRender(object sender, EventArgs e)
    {
        if (grdLedgerSummary.Rows.Count > 0)
        {
            grdLedgerSummary.UseAccessibleHeader = true;
            grdLedgerSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdLedgerSummary_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        objLedgerColl = (AppLedgerColl)Session[TableConstants.LedgerTypeCollObj];        
        objLedgerColl[e.RowIndex].Status = -10;
        objLedgerColl[e.RowIndex].Save();
    }
    protected void lbtnNewLedger_Click(object sender, EventArgs e)
    {
        Response.Redirect("M_Ledger.aspx"); 
    }
    public void BindData()
    {

        AppObjects.AppLedgerColl objLedgerColl = new AppLedgerColl(intRequestingUserID);
        if (string.IsNullOrEmpty(txtLedgerName.Text) == false)
        {
            objLedgerColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Ledger.LedgerName, AppUtility.Operators.Like, txtLedgerName.Text, 0);
        }
        if (string.IsNullOrEmpty(txtLedgerCode.Text) == false)
        {
            objLedgerColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Ledger.LedgerCode, AppUtility.Operators.Like, txtLedgerCode.Text, 0);
        }
        if (string.IsNullOrEmpty(txtAccountNo.Text) == false)
        {
            objLedgerColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Ledger.AccountNo, AppUtility.Operators.Like, txtAccountNo.Text, 0);
        }
        if (ddlAccountType.SelectedValue != "0")
        {
            objLedgerColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Ledger.AccountType_ID, AppUtility.Operators.Equals, ddlAccountType.SelectedValue, 0);
        }
        if (ddlLedgerType.SelectedValue != "0")
        {
            objLedgerColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Ledger.LedgerType_ID, AppUtility.Operators.Equals, ddlLedgerType.SelectedValue, 0);
        }
        if (ddlTypeofLedger.SelectedValue != "0")
        {
            objLedgerColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Ledger.LedgerFlag, AppUtility.Operators.Equals, ddlTypeofLedger.SelectedValue, 0);
        }
        if (string.IsNullOrEmpty(txtParentLedgerGrp.Text) == false)
        {
            objLedgerColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Ledger.Parent_ID, AppUtility.Operators.Equals, hdfParentLedgerGrpID.Value, 0);
        }
        objLedgerColl.Search(RecordStatus.ALL);
        Session.Add(TableConstants.LedgerCollObj, objLedgerColl);
        grdLedgerSummary.DataSource = objLedgerColl;
        grdLedgerSummary.DataBind();

      
                
    }
}