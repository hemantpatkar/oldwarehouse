﻿#region Author
/*
Name    :   Navnath Sonavane
Date    :   04/08/2016
Use     :   This form is used to add Journal Voucher Order details  
*/
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.Services;
using AppUtility;
using Objects;
using AjaxControlToolkit;
#endregion
public partial class T_JournalVoucher : BigSunPage
{
    #region Page_Load
    AppObjects.AppJournalVoucher obj = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = new AppObjects.AppJournalVoucher(intRequestingUserID);
        if (!IsPostBack)
        {
            Session[TableConstants.JournalVoucher] = obj;
            string ID = Convert.ToString(Request.QueryString["JournalVoucherID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int JournalVoucherID = new AppUtility.AppConvert(ID);
                obj = new AppObjects.AppJournalVoucher(intRequestingUserID, JournalVoucherID);
                PopulateData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "2")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage(Journal Voucher Approved Sucessfully','1'" + ");", true);
                }
            }
            else
            {
                DefaultData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
            }
            Session[TableConstants.JournalVoucher] = obj;
            BindData();
        }
    }
    #endregion
    #region Button Click Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppJournalVoucher JournalVoucher = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
        JournalVoucher.JournalVoucherDetailColl.RemoveAll(x => x.Ledger_ID == 0);
        if (JournalVoucher.JournalVoucherDetailColl.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please add atleast one item','2'" + ");", true);
            return;
        }
        Session.Add(TableConstants.JournalVoucher, JournalVoucher);
        decimal DebitAmount = JournalVoucher.JournalVoucherDetailColl.Sum(x => x.DebitAmount);
        decimal CreditAmount = JournalVoucher.JournalVoucherDetailColl.Sum(x => x.CreditAmount);
        if (DebitAmount != CreditAmount)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Debit and Credit amount not matching.','2'" + ");", true);
            return;
        }
        SaveJournalVoucherHeader();
        Session.Remove(TableConstants.JournalVoucher);
        Response.Redirect("T_JournalVoucher.aspx?Msg=1");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("T_JournalVoucherSummary.aspx");
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        AppJournalVoucher JV = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
        JV.Status = new AppConvert((int)RecordStatus.Approve);
        JV.Save();
        Response.Redirect("T_JournalVoucher.aspx?Msg=2&JournalVoucherID="+Convert.ToString(Request.QueryString["JournalVoucherID"]));
    }
    protected void btnAddLineItem_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            AddEmptyRow(1);
            if (grdJournalVoucherDetails.Rows.Count > 0)
            {
                TextBox txtLedger = (TextBox)grdJournalVoucherDetails.Rows[0].FindControl("txtLedger");
                AppJournalVoucher JournalVoucher = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
                if (JournalVoucher == null)
                {
                    JournalVoucher = new AppJournalVoucher(intRequestingUserID);
                }

                txtLedger.Focus();
            }
        }
    }
    #endregion
    #region Functions
    public bool Validation()
    {

        if (grdJournalVoucherDetails.Rows.Count > 0)
        {
            TextBox txtLedger = (TextBox)grdJournalVoucherDetails.Rows[0].FindControl("txtLedger");
            TextBox txtDebit = (TextBox)grdJournalVoucherDetails.Rows[0].FindControl("txtDebit");
            TextBox txtCredit = (TextBox)grdJournalVoucherDetails.Rows[0].FindControl("txtCredit");
            if (txtLedger.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please select ledger first.','2'" + ");", true);
                return false;
            }
            if (txtDebit.Text == txtCredit.Text)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter debit or credit value for selected ledger.','2'" + ");", true);
                return false;
            }

            txtLedger.Focus();
        }
        return true;
    }
    public void DefaultData()
    {
        txtJournalVoucherDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);
        btnApprove.Visible = false;
    }
    public void PopulateData()
    {
        txtJournalVoucherDate.Text = new AppConvert(obj.JournalVoucherDate);
        txtJournalVoucherNo.Text = new AppConvert(obj.JournalVoucherNo);
        txtStatus.Text = obj.StatusName;
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);
        txtHeaderExchangeCurrency.Text = obj.CurrencyType.Name;
        hdfHeaderExchangeCurrencyName.Value = txtHeaderExchangeCurrency.Text;
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(obj.CurrencyType_ID);
        btnSave.Visible = obj.Status != new AppConvert((int)RecordStatus.Created) ? false : true;
        btnApprove.Visible = obj.Status == new AppConvert((int)RecordStatus.Created) ? true : false;
    }
    public void BindData()
    {
        AppJournalVoucher JournalVoucher = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
        if (JournalVoucher == null)
        {
            JournalVoucher = new AppJournalVoucher(intRequestingUserID);
        }
        grdJournalVoucherDetails.DataSource = JournalVoucher.JournalVoucherDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created));
        grdJournalVoucherDetails.DataBind();
        if (JournalVoucher.JournalVoucherDetailColl.Count <= 0)
        {
            AddEmptyRow(1);
        }
        SetValueToGridFooter();
    }
    public void SaveJournalVoucherHeader()
    {
        AppJournalVoucher JV = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
        if (JV.ID == 0)
        {
            JV.JournalVoucherNo = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "JV", System.DateTime.Now,"JV-", 4, ResetCycle.Yearly));
        }
        JV.VoucherType_ID = 19;
        JV.TotalAmount = new AppConvert(txtTotalAmount.Text);
        JV.Company_ID = new AppConvert(LoginCompanyID);
        JV.JournalVoucherDate = new AppConvert(txtJournalVoucherDate.Text);
        JV.CurrencyType_ID = new AppConvert(hdfHeaderExchangeCurrencyID.Value);
        JV.Status = new AppConvert((int)RecordStatus.Created);
        JV.Type = obj.Type;
        JV.ModifiedBy = intRequestingUserID;
        JV.ModifiedOn = System.DateTime.Now;
        JV.Save();

        if (JV.JournalVoucherDetailColl.Count > 0)
        {
            foreach (var item in JV.JournalVoucherDetailColl)
            {
                if (item.CreditAmount != 0)
                {
                    LedgerEntry(LoginCompanyID, 100601, 1006, item.Ledger_ID, item.ID, JV.JournalVoucherDate, item.CreditAmount, 0, JV.JournalVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, JV.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, JV.VoucherType_ID, item.JournalVoucherDesc);
                }
                else
                {
                    LedgerEntry(LoginCompanyID, 100602, 1006, item.Ledger_ID, item.ID, JV.JournalVoucherDate, 0, item.DebitAmount, JV.JournalVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, JV.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, JV.VoucherType_ID, item.JournalVoucherDesc);
                }


            }

        }
    }
    public void SetValueToGridFooter()
    {
        AppJournalVoucher JournalVoucher = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
        Label lblFooterCreditAmount = (Label)grdJournalVoucherDetails.FooterRow.FindControl("lblFooterCreditAmount");
        lblFooterCreditAmount.Text = Convert.ToDecimal(JournalVoucher.JournalVoucherDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created)).Sum(x => x.CreditAmount)).ToString("#,##0.00");

        Label lblFooterDebitAmount = (Label)grdJournalVoucherDetails.FooterRow.FindControl("lblFooterDebitAmount");
        lblFooterDebitAmount.Text = Convert.ToDecimal(JournalVoucher.JournalVoucherDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created)).Sum(x => x.DebitAmount)).ToString("#,##0.00");
        txtTotalAmount.Text = lblFooterDebitAmount.Text;



    }
    public void AddEmptyRow(int NumberOfRows)
    {
        AppJournalVoucher JournalVoucher = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
        if (JournalVoucher == null)
        {
            JournalVoucher = new AppJournalVoucher(intRequestingUserID);
        }
        AppJournalVoucherDetail JournalVoucherItem = new AppJournalVoucherDetail(intRequestingUserID);
        for (int i = 0; i < NumberOfRows; i++)
        {
            if (JournalVoucher.JournalVoucherDetailColl.Count == 0)
            {
                JournalVoucher.JournalVoucherDetailColl.Add(JournalVoucherItem);
            }
            else
            {
                JournalVoucher.JournalVoucherDetailColl.Insert(0, JournalVoucherItem);
            }
            if (JournalVoucher.JournalVoucherDetailColl.Count >= 0)
            {
                decimal DebitAmount = JournalVoucher.JournalVoucherDetailColl.Sum(x => x.DebitAmount);
                decimal CreditAmount = JournalVoucher.JournalVoucherDetailColl.Sum(x => x.CreditAmount);
                if (DebitAmount > CreditAmount)
                {

                    JournalVoucher.JournalVoucherDetailColl[0].CreditAmount = new AppConvert(DebitAmount - CreditAmount);

                }
                else
                {
                    JournalVoucher.JournalVoucherDetailColl[0].DebitAmount = new AppConvert(CreditAmount - DebitAmount);

                }
            }
            Session.Add(TableConstants.JournalVoucher, JournalVoucher);
        }
        BindData();
    }
    #endregion
    #region Text Box Event
    protected void txtLedger_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfLedgerID = (HiddenField)grdJournalVoucherDetails.Rows[RowIndex].FindControl("hdfLedgerID");
        TextBox txtLedger = (TextBox)grdJournalVoucherDetails.Rows[RowIndex].FindControl("txtLedger");
        TextBox txtJournalVoucherDesc = (TextBox)grdJournalVoucherDetails.Rows[RowIndex].FindControl("txtJournalVoucherDesc");
        AppJournalVoucher JournalVoucher = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
        JournalVoucher.JournalVoucherDetailColl[RowIndex].Ledger_ID = new AppUtility.AppConvert(hdfLedgerID.Value);
        JournalVoucher.JournalVoucherDetailColl[RowIndex].ModifiedBy = intRequestingUserID;
        JournalVoucher.JournalVoucherDetailColl[RowIndex].ModifiedOn = System.DateTime.Now;
        JournalVoucher.JournalVoucherDetailColl[RowIndex].Status = new AppConvert((int)RecordStatus.Created);
        JournalVoucher.JournalVoucherDetailColl[RowIndex].Type = new AppConvert((int)DefaultNormalType.Normal);
        Session.Remove(TableConstants.JournalVoucher);
        Session.Add(TableConstants.JournalVoucher, JournalVoucher);
        BindData();
        txtJournalVoucherDesc.Focus();
    }
    protected void txtJournalVoucherDesc_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtJournalVoucherDesc = (TextBox)grdJournalVoucherDetails.Rows[RowIndex].FindControl("txtJournalVoucherDesc");
        TextBox txtDebit = (TextBox)grdJournalVoucherDetails.Rows[RowIndex].FindControl("txtDebit");
        AppJournalVoucher JournalVoucher = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
        JournalVoucher.JournalVoucherDetailColl[RowIndex].JournalVoucherDesc = new AppUtility.AppConvert(txtJournalVoucherDesc.Text);

        Session.Remove(TableConstants.JournalVoucher);
        Session.Add(TableConstants.JournalVoucher, JournalVoucher);
        BindData();
        txtDebit.Focus();
    }
    protected void txtProfitCenter_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtProfitCenter = (TextBox)grdJournalVoucherDetails.Rows[RowIndex].FindControl("txtProfitCenter");
        HiddenField hdfProfitCenterID = (HiddenField)grdJournalVoucherDetails.Rows[RowIndex].FindControl("hdfProfitCenterID");
        AppJournalVoucher JournalVoucher = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
        JournalVoucher.JournalVoucherDetailColl[RowIndex].ProfitCenter_ID = new AppUtility.AppConvert(hdfProfitCenterID);
        Session.Remove(TableConstants.JournalVoucher);
        Session.Add(TableConstants.JournalVoucher, JournalVoucher);
        BindData();
        txtProfitCenter.Focus();
    }
    protected void txtCostCenter_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtCostCenter = (TextBox)grdJournalVoucherDetails.Rows[RowIndex].FindControl("txtCostCenter");
        HiddenField hdfCostCenterID = (HiddenField)grdJournalVoucherDetails.Rows[RowIndex].FindControl("hdfCostCenterID");
        AppJournalVoucher JournalVoucher = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
        JournalVoucher.JournalVoucherDetailColl[RowIndex].CostCenter_ID = new AppUtility.AppConvert(hdfCostCenterID.Value);
        Session.Remove(TableConstants.JournalVoucher);
        Session.Add(TableConstants.JournalVoucher, JournalVoucher);
        BindData();
        txtCostCenter.Focus();
    }
    protected void txtDebit_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtDebit = (TextBox)grdJournalVoucherDetails.Rows[RowIndex].FindControl("txtDebit");
        TextBox txtCredit = (TextBox)grdJournalVoucherDetails.Rows[RowIndex].FindControl("txtCredit");
        AppJournalVoucher JournalVoucher = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
        JournalVoucher.JournalVoucherDetailColl[RowIndex].DebitAmount = new AppUtility.AppConvert(txtDebit.Text);
        JournalVoucher.JournalVoucherDetailColl[RowIndex].CreditAmount = new AppUtility.AppConvert("0.00");

        Session.Remove(TableConstants.JournalVoucher);
        Session.Add(TableConstants.JournalVoucher, JournalVoucher);
        BindData();
        txtCredit.Focus();
    }
    protected void txtCredit_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtCredit = (TextBox)grdJournalVoucherDetails.Rows[RowIndex].FindControl("txtCredit");
        Button btnAddLineItem = (Button)grdJournalVoucherDetails.HeaderRow.FindControl("btnAddLineItem");
        AppJournalVoucher JournalVoucher = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
        JournalVoucher.JournalVoucherDetailColl[RowIndex].CreditAmount = new AppUtility.AppConvert(txtCredit.Text);
        JournalVoucher.JournalVoucherDetailColl[RowIndex].DebitAmount = new AppUtility.AppConvert("0.00");
        Session.Remove(TableConstants.JournalVoucher);
        Session.Add(TableConstants.JournalVoucher, JournalVoucher);
        BindData();
        btnAddLineItem.Focus();
    }
    #endregion
    #region Grid View Events
    protected void grdJournalVoucherDetails_PreRender(object sender, EventArgs e)
    {
        if (grdJournalVoucherDetails.Rows.Count > 0)
        {
            grdJournalVoucherDetails.UseAccessibleHeader = true;
            grdJournalVoucherDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdJournalVoucherDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
            AppJournalVoucher JournalVoucher = (AppJournalVoucher)Session[TableConstants.JournalVoucher];
            JournalVoucher.JournalVoucherDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created)).ToList()[row.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
            Session.Add(TableConstants.JournalVoucher, JournalVoucher);
            BindData();
        }

    }
    #endregion
}