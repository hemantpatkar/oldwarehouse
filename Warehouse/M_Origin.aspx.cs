﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppUtility;
using AppObjects;
public partial class M_Origin : BigSunPage
{
    AppOriginColl obj;
    protected void Page_Load(object sender, EventArgs e)
    {
  
        obj = new AppObjects.AppOriginColl(intRequestingUserID);
        Session[TableConstants.SessionOrigin] = obj;
        if (!IsPostBack)
        {
           BindData();
        }
    }
    #region GridViewEvents
   
    protected void grdOrigin_PreRender(object sender, EventArgs e)
    {
        if (grdOrigin.Rows.Count > 0)
        {
            grdOrigin.UseAccessibleHeader = true;
            grdOrigin.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdOrigin_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if (strCommandName == "INSERT")
        {       
            HiddenField hdfOriginID = grdOrigin.FooterRow.FindControl("hdfOriginID") as HiddenField;
            TextBox txtCode = grdOrigin.FooterRow.FindControl("txtNewCode") as TextBox;
            TextBox txtName = grdOrigin.FooterRow.FindControl("txtNewName") as TextBox;
            CheckBox chkActiveOrigin = grdOrigin.FooterRow.FindControl("chkActiveOrigin") as CheckBox;
            CheckBox chkDefaultOrigin = grdOrigin.FooterRow.FindControl("chkDefaultOrigin") as CheckBox;
            SaveOriginDetails(strCommandID, txtCode.Text, txtName.Text, chkActiveOrigin.Checked, chkDefaultOrigin.Checked);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Successfully Added','1'" + ");", true);
            grdOrigin.EditIndex = -1;
            BindData();
        }
        else if (strCommandName == "CANCELADD")
        {
            grdOrigin.ShowFooter = false;
            BindData();
        }
        else if (strCommandName == "CANCELEDIT")
        {
            grdOrigin.EditIndex = -1;
            BindData();
        }
        else if(strCommandName=="ADDNEW")
        {
            grdOrigin.EditIndex = -1;
            grdOrigin.ShowFooter = true;
            BindData();
        }
    }
    protected void grdOrigin_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdOrigin.ShowFooter = false;
        grdOrigin.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdOrigin_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HiddenField hdfID = grdOrigin.Rows[e.RowIndex].FindControl("hdfID") as HiddenField;
        AppOrigin SingleObj = new AppOrigin(intRequestingUserID);
        SingleObj.ID = new AppConvert(hdfID.Value);
        SingleObj.Status = new AppConvert((int)RecordStatus.Deleted);
        SingleObj.Save();
     
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void grdOrigin_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strCommandID = new AppConvert(e.RowIndex);
        HiddenField hdfOriginID = grdOrigin.Rows[e.RowIndex].FindControl("hdfOriginID") as HiddenField;
        TextBox txtCode = grdOrigin.Rows[e.RowIndex].FindControl("txtCode") as TextBox;
        TextBox txtName = grdOrigin.Rows[e.RowIndex].FindControl("txtName") as TextBox;
        CheckBox chkIsActiveOrigin = grdOrigin.Rows[e.RowIndex].FindControl("chkIsActiveOrigin") as CheckBox;
        CheckBox chkIsDefaultOrigin = grdOrigin.Rows[e.RowIndex].FindControl("chkIsDefaultOrigin") as CheckBox;

        SaveOriginDetails(hdfOriginID.Value, txtCode.Text, txtName.Text , chkIsActiveOrigin.Checked, chkIsDefaultOrigin.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Successfully Updated','1'" + ");", true);
        grdOrigin.EditIndex = -1;
        BindData();
    }
    #endregion
    #region MiscFunctions
    public void SaveOriginDetails(string Id, string Code, string Name, bool IsActive, bool IsDefault)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppOrigin SingleObj = new AppOrigin(intRequestingUserID);      
        SingleObj.ID = new AppConvert(Id);
        SingleObj.Code = new AppConvert(Code);
        SingleObj.Name = new AppConvert(Name);
        SingleObj.Status = (IsActive == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));        
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;

        Session.Add(TableConstants.SessionOrigin, obj);
        SingleObj.Save();
        BindData();
    }
    

    protected void BindData()
    {
        AppObjects.AppOriginColl objColl = new AppOriginColl(intRequestingUserID);
        if (!string.IsNullOrEmpty(txtName.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Origin.Name, AppUtility.Operators.Equals, txtName.Text, 0);
        }
        if (!string.IsNullOrEmpty(txtCode.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Origin.Code, AppUtility.Operators.Equals, txtCode.Text, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
             
            objColl.AddCriteria(AppObjects.Origin.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.SessionOrigin] = objColl;
        if (objColl != null)
        {
            grdOrigin.DataSource = objColl;
            grdOrigin.DataBind();
            if (objColl.Count <= 0)
            {
                AddEmptyRow();
            }
        }
       
       
    }
    public void AddEmptyRow()
    {
        AppOriginColl collOfContacts = new AppOriginColl(intRequestingUserID);
        AppOrigin newContact = new AppOrigin(intRequestingUserID);
        newContact.Code = "Add atleast one Origin";
        collOfContacts.Add(newContact);
        grdOrigin.DataSource = collOfContacts;
        grdOrigin.DataBind();
        grdOrigin.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdOrigin.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdOrigin.ShowFooter = true;
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}