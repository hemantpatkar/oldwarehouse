﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnlogin_Click(object sender, EventArgs e)
    {
        Session[TableConstants.UserIDSession] = null;
        Session[TableConstants.CompanyIdConst] = null;
        Session[TableConstants.DepartmentIDConst] = null;
        Session[TableConstants.DesignationIDConst] = null;
        Session[TableConstants.ActiveModuleID] = null;
        Session[TableConstants.ActiveModuleName] = null;
        string UserName = txtUsername.Text;
        string Password = txtpassword.Text;
        AppCompanyUserColl CUL = new AppCompanyUserColl(1);
        AppCompanyUser objAppCompanyUser = CUL.ValidateUser(UserName, Password);

        if (objAppCompanyUser == null || objAppCompanyUser.ID == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please check user name and password','2'" + ");", true);
        }
        else
        {
            Session[TableConstants.UserIDSession] = objAppCompanyUser.ID;
            Session[TableConstants.CompanyIdConst] = objAppCompanyUser.Company_ID;
            Session[TableConstants.DepartmentIDConst] = objAppCompanyUser.DepartmentType_ID;
            Session[TableConstants.DesignationIDConst] = objAppCompanyUser.DesignationType_ID;
            //Session[TableConstants.ActiveModule] = "2";
            Session[TableConstants.LoginUser] = objAppCompanyUser;
            Response.Redirect("Home.aspx");
        }
        Response.Redirect("Home.aspx");
    }
}