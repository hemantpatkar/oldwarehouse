﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" Culture="en-gb" EnableEventValidation="false" AutoEventWireup="true" Inherits="StockPreInSearch" Codebehind="StockPreInSearch.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" language="javascript">
        function checkedAllSearch() {
            var but = document.getElementById("SelectAll");
            var TargetBaseControl = document.getElementById('<%= this.grdPreInwardSearch.ClientID %>');
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var icount = 0; icount < Inputs.length; ++icount) {
                if (Inputs[icount].type == 'checkbox') {
                    if (Inputs[icount].disabled == false) {
                        if (but.checked == true) {
                            Inputs[icount].checked = true;
                        }
                        else {

                            Inputs[icount].checked = false;
                        }
                    }
                    else {
                        Inputs[icount].checked = false;
                    }
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" DisplayAfter="0" AssociatedUpdatePanelID="grdUpdate"
        runat="server">
        <ProgressTemplate>
            <div class="modal fade in" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-keyboard="false" style="display: block">
                <div class="modal-dialog  modal-sm">
                    <h1 style="text-align: center;">
                        <i class="fa fa-truck fa-magic fa-2x faa-passing animated"></i>
                    </h1>
                </div>
            </div>
            <div class="modal-backdrop fade in"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <div class="row">
        <div class="col-lg-9">
            <div class="heading">
                <i class="fa fa-truck"></i>
                <asp:Label ID="lblPreinwardSearch" Text="Pre Inward Search" runat="server"></asp:Label>
            </div>
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSearch_Click" Text="Search" ValidationGroup="Search" TabIndex="1" />
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnNew" OnClick="btnNew_Click" CssClass="btn btn-primary btn-block btn-sm" runat="server" AccessKey="n" CausesValidation="False" TabIndex="1" Text="New" />
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnExport" CssClass="btn btn-warning btn-block btn-sm" runat="server" OnClick="btnExportToExcel_Click" AccessKey="n" CausesValidation="False" TabIndex="1" Text="Export" />
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblPreInwardNo" Text="Pre Inward No." CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtPreInwardNo" runat="server" CssClass="form-control input-sm" ValidationGroup="Search" TabIndex="1">
            </asp:TextBox>

        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblCustomer" Text="Customer" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtCustomer" runat="server" onblur="ClearVechical()" CssClass="form-control input-sm" ValidationGroup="Search" TabIndex="1">
            </asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblfromdate" Text="From Date" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control input-sm" ValidationGroup="Search" TabIndex="1">
            </asp:TextBox>
            <asp:MaskedEditExtender ID="txtDate_MaskedEditExtender" runat="server" Enabled="true" Mask="99/99/9999" ClearMaskOnLostFocus="true"
                MaskType="Date" TargetControlID="txtfromdate">
            </asp:MaskedEditExtender>
            <asp:CalendarExtender ID="CalendarExtender3" TargetControlID="txtfromdate" Format="dd/MM/yyyy"
                PopupButtonID="txtDate" runat="server">
            </asp:CalendarExtender>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lbltodate" Text="To Date" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txttodate" runat="server" CssClass="form-control input-sm" ValidationGroup="Search"
                TabIndex="1">
            </asp:TextBox>
            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="True" Mask="99/99/9999" ClearMaskOnLostFocus="true"
                MaskType="Date" TargetControlID="txttodate">
            </asp:MaskedEditExtender>
            <asp:CalendarExtender ID="CalendarExtender4" TargetControlID="txttodate" Format="dd/MM/yyyy"
                PopupButtonID="txttodate" runat="server">
            </asp:CalendarExtender>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblStatus" Text="Status" CssClass="bold"></asp:Label>
            <asp:DropDownList ID="ddlStatus" runat="server" TabIndex="1" CssClass="form-control input-sm">
                <asp:ListItem Value="-1">All</asp:ListItem>
                <asp:ListItem Value="1" Selected="True">Un Approved</asp:ListItem>
                <asp:ListItem Value="2">Available</asp:ListItem>
                <asp:ListItem Value="4">Used</asp:ListItem>
            </asp:DropDownList>
            <asp:HiddenField ID="hdfgatereg" runat="server" />
            <asp:HiddenField ID="ShowLastRecords" runat="server" />
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblReceivedDate" Text="Received Date" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtReceivedDate" runat="server" CssClass="form-control input-sm" ValidationGroup="Search"
                TabIndex="1">
            </asp:TextBox>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-lg-12" style="overflow: auto">
            <asp:UpdatePanel ID="grdUpdate" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdPreInwardSearch" runat="server" PageSize="50" AllowPaging="true" OnPageIndexChanging="grdPreInwardSearch_PageIndexChanging"
                        GridLines="None" AutoGenerateColumns="False" PagerStyle-CssClass="pagination-ys" CssClass="table table-hover table-striped text-nowrap table-Full"
                        OnPreRender="grdPreInwardSearch_PreRender">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <input id="SelectAll" onclick="checkedAllSearch();" type="checkbox" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input id="CheckBoxSelect" name="CheckBoxSelect" type="checkbox" tabindex="4" value='<%#Eval("ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pre Inward No.">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkPreInwardNo" runat="server" ToolTip='<%# Eval("PreInwardNo") %>' NavigateUrl='<%# "StockPreInDetail.aspx?ID=" + Eval("ID") %>'
                                        Text='<%# Eval("PreInwardNo") %>' CommandArgument='<%# Eval("ID") %>'></asp:HyperLink><br />
                                    <asp:Label ID="lblChallanNo" runat="server" ToolTip='<%#Eval("GateEntry.GateNo") %>' Text='<%#Eval("GateEntry.GateNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer Info">
                                <ItemTemplate>
                                    <asp:Label ID="lblVehicalNo" runat="server" ToolTip='<%#Eval("Customer_Company.Name") %>'
                                        Text='<%#Eval("Customer_Company.Name") %>'></asp:Label><br />
                                    <asp:Label ID="lblVehicalName" runat="server" Font-Bold="true" ForeColor="Maroon"
                                       Text='<%#Eval("Customer_Company.Parent.Name") %>' ToolTip='<%#Eval("Customer_Company.Parent.Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pre Inward Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblpartyname" runat="server" ToolTip='<%#Eval("PreInwardDate") %>'
                                        Text='<%#Eval("PreInwardDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Received By">
                                <ItemTemplate>
                                    <asp:Label ID="lblDriverName" runat="server" Text='<%# Eval("ReceivedBy_CompanyContact.FirstName")+" "+ Eval("ReceivedBy_CompanyContact.LastName") %>'></asp:Label><br />
                                    <asp:Label ID="lblMobNo" runat="server" ToolTip='<%# Eval("ReceivedDate") %>' ForeColor="#3399ff" Font-Bold="true" Text='<%# Eval("ReceivedDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Modified By">
                                <ItemTemplate>
                                    <asp:Label ID="lblinunits" runat="server" ToolTip='<%# Eval("ModifiedBy") %>' Text='<%# Eval("ModifiedBy") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Modified On">
                                <ItemTemplate>
                                    <asp:Label ID="lbloutunits" runat="server" ToolTip='<%# Eval("ModifiedOn") %>' Text='<%# Eval("ModifiedOn") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Print">
                                <ItemTemplate>
                                    <asp:HyperLink ID="HLReport" runat="server" ToolTip="View Report" Target="_blank" NavigateUrl='<% #"../../Reports/ReportView.aspx?GateRegID=" + Eval("ID") + "&RPName=GatePass"%>'>
                                        <i class="fa fa-print fa-2x"></i>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>

</asp:Content>

