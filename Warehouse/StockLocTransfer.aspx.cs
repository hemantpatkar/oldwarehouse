﻿using AppObjects;
using AppUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class StockLocTransfer : BigSunPage
{

    #region Variables
    public static int lotid = 0;
    public int f = 0;
    int total = 0;
    public int temp;
    public int flag;
    public int old;
    public int alertflag = 0;
    public string UnitCode = "";
    private int oldPTID = 0;
    private DateTime oldINDate;
    List<int> InwardDetail_IDs;
    AppTransferStockLocationDetailColl objAppTransferStockLocationDetailColl;
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        DecideLanguage(Convert.ToString(Session["LanguageCode"]));
        Page.Title = "Location Transfer";
        objAppTransferStockLocationDetailColl = new AppTransferStockLocationDetailColl(intRequestingUserID);
        if (!IsPostBack)
        {
            Session.Add(TableConstants.TransferStockLocationSession, objAppTransferStockLocationDetailColl); 
            txtLotSearch.Focus(); 
        }
    }
    #endregion

    #region Button Event
     
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockLocTransferSearch.aspx");
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        objAppTransferStockLocationDetailColl = (AppTransferStockLocationDetailColl)Session[TableConstants.TransferStockLocationSession];
        int i = 0;
        foreach (var AppTransferStockLocationDetail in objAppTransferStockLocationDetailColl)
        {

            TextBox txtOrderQuantity = (TextBox)gvDescription.Rows[i].FindControl("txtOrderQuantity");
            HiddenField hdfToStorageLocationID = (HiddenField)gvDescription.Rows[i].FindControl("hdfToStorageLocationID");
            AppTransferStockLocationDetail.MovedQuantity = new AppConvert(txtOrderQuantity.Text);
            AppTransferStockLocationDetail.To_StorageLocation_ID = new AppConvert(hdfToStorageLocationID.Value);
            AppTransferStockLocationDetail.ModifiedOn = DateTime.Now;
            AppTransferStockLocationDetail.Status = (int)RecordStatus.Active;
            AppTransferStockLocationDetail.Save();
            i++;


            AppInwardDetail FromLocation = new AppInwardDetail(intRequestingUserID, AppTransferStockLocationDetail.InwardDetail_ID);
            FromLocation.InwardQuantity = FromLocation.InwardQuantity - AppTransferStockLocationDetail.MovedQuantity;
            FromLocation.Save();

            AppInwardDetail ToLocation = new AppInwardDetail(intRequestingUserID, AppTransferStockLocationDetail.InwardDetail_ID);
            ToLocation.ID = 0;
            ToLocation.InwardQuantity = AppTransferStockLocationDetail.MovedQuantity;
            ToLocation.StorageLocation_ID = new AppConvert(hdfToStorageLocationID.Value);
            ToLocation.VoucherType_ID = 43;
            ToLocation.VoucherDetailID = AppTransferStockLocationDetail.ID;
            ToLocation.TransferDate = AppTransferStockLocationDetail.TransferDate;
            ToLocation.Status= (int)RecordStatus.Active;
            ToLocation.Save();



        }

        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Saved Successfully','1'" + ");", true);

    }
    protected void btnNewDetail_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockLocTransfer.aspx");
    }
    protected void btnLotSearch_Click(object sender, EventArgs e)
    {

        SearchOptimised();

        
        if (gvSearch.Rows.Count > 0)
        {
            //CheckBox CheckBoxSelect = (CheckBox)gvSearch.Rows[0].FindControl("CheckBoxSelect");
            //CheckBoxSelect.Focus();
        }
        else
        {
            UsrtxtPartnerGroupCode.Focus();
        }


    }
    protected void btnAddItems_Click(object sender, EventArgs e)
    {
        objAppTransferStockLocationDetailColl = (AppTransferStockLocationDetailColl)Session[TableConstants.TransferStockLocationSession];

        #region To Get IDs of Inward ID from Search Grid

        string[] check = Convert.ToString(Request.Form["CheckBoxSelect"]).Split(',');

        for (int i = 0; i < check.Length; i++)
        {
            AppTransferStockLocationDetail objAppTransferStockLocationDetail = new AppTransferStockLocationDetail(intRequestingUserID);
            int InwardDetailID = Convert.ToInt32(check[i]);
            AppInwardDetail objAppInwardDetail = new AppInwardDetail(intRequestingUserID, InwardDetailID);

            objAppTransferStockLocationDetail.StockQuantity = objAppInwardDetail.InwardQuantity;
            objAppTransferStockLocationDetail.MovedQuantity = 0;
            objAppTransferStockLocationDetail.Item_ID = objAppInwardDetail.Item_ID;
            objAppTransferStockLocationDetail.InwardDetail_ID = objAppInwardDetail.ID;
            objAppTransferStockLocationDetail.VoucherDetailID = 0;
            objAppTransferStockLocationDetail.From_StorageLocation_ID = objAppInwardDetail.StorageLocation_ID;
            objAppTransferStockLocationDetail.To_StorageLocation_ID = 0;
            objAppTransferStockLocationDetail.LotInternal = objAppInwardDetail.LotInternal;
            objAppTransferStockLocationDetail.LotVersion = objAppInwardDetail.LotVersion;
            objAppTransferStockLocationDetail.TransferDate = DateTime.Now;
            objAppTransferStockLocationDetailColl.Add(objAppTransferStockLocationDetail);

            #region datevalidation
            //Int64 id = Convert.ToInt64("0" + hdfInwardDetailID.Value);
            //int date = compare(id);
            //old = Convert.ToInt32("0" + ViewState["oldDate"]);
            //if (old < date)
            //{
            //    txtDateCompare.Text = GenericUtills.ConvertIntToDate(date);
            //    ViewState["oldDate"] = date;
            //}

            #endregion

        }

        #endregion

        Session.Add(TableConstants.TransferStockLocationSession, objAppTransferStockLocationDetailColl);

        gvDescription.DataSource = objAppTransferStockLocationDetailColl;
        gvDescription.DataBind();
         
    }

    #endregion

    #region Function
    private void DecideLanguage(string languageCode)  // Method used to fill Screen Language on lables & Buttons text property
    {
        try
        {
            lblLocTransferTitle.Text = "Location Transfer";
            btnSave.Text = "Save";
            btnDeleteItem.Text = "Delete" + " " + "Item";
            btnNewDetail.Text = "Create New";
            btnLotSearch.Text = "Search";
            btnAddItems.Text = "Add";

        }
        catch (Exception)
        {

        }
    }
    public void SearchOptimised()
    {

        int flag = 1;

        #region FindControls
        //TextBox txtScheme = (TextBox)gvDescription.FooterRow.FindControl("txtScheme");
        //TextBox txtLotNo = (TextBox)gvDescription.FooterRow.FindControl("txtLotNo");
        //TextBox txtItemGroup = (TextBox)gvDescription.FooterRow.FindControl("txtItemGroup");
        //TextBox txtStorageItemName = (TextBox)gvDescription.FooterRow.FindControl("txtStorageItemName");
        //TextBox txtMark = (TextBox)gvDescription.FooterRow.FindControl("txtMark");
        //TextBox txtpackingref = (TextBox)gvDescription.FooterRow.FindControl("txtpackingref");
        //TextBox txtVakkal = (TextBox)gvDescription.FooterRow.FindControl("txtVakkal");
        //TextBox txtReceivedQuantity = (TextBox)gvDescription.FooterRow.FindControl("txtReceivedQuantity");

        #endregion

        #region Get Financial Year
        //int YID = Convert.ToInt32(Session["Finyear"]);
        //DateTime startdate = Convert.ToDateTime(GenericUtills.convertIntToDate(db.mFinancialYears.FirstOrDefault(x => x.FYearID == YID).StartDate));
        //DateTime enddate = Convert.ToDateTime(GenericUtills.convertIntToDate(db.mFinancialYears.FirstOrDefault(x => x.FYearID == YID).EndDate));
        #endregion

        #region declare variables

        int Customergroup = Convert.ToInt32("0" + UsrhdfPartnerGroup.Value);
        int Customer = Convert.ToInt32("0" + hdfCustomerID.Value);
        int LotNoInt = new AppConvert(txtLotSearch.Text.Trim());
        string Vakkal = new AppConvert(txtVakkalSearch.Text.Trim());
        string packingref = new AppConvert(txtpackingrefSearch.Text);
        string Mark = new AppConvert(txtMarkSearch.Text);
        string ItemName = new AppConvert(txtStorageItemName.Text);
        string SchemeName = new AppConvert(txtSchemeSearch.Text);
        string itemgroup = new AppConvert(txtItemGroup.Text);
        decimal InwardQty = new AppConvert(txtReceivedQuantitySearch.Text);

        #endregion

         
        AppStockSearchVWColl objAppStockSearchVWColl = new AppStockSearchVWColl(intRequestingUserID);

        InwardDetail_IDs = (List<int>)Session[TableConstants.InwardDetailIDSession];
        objAppStockSearchVWColl.Search(0, Customergroup, Customer, 0, InwardQty, LotNoInt, Vakkal, Mark, packingref, ItemName, InwardDetail_IDs, false);

        if (flag == 1)
        {
            gvSearch.DataSource = objAppStockSearchVWColl.Where(x => x.NetQuantity > 0).ToList();
            gvSearch.DataBind();

        }

    }
     
    #endregion

    #region GridView

    protected void gvSearch_PreRender(object sender, EventArgs e)
    {
        if (gvSearch.Rows.Count > 0)
        {
            gvSearch.UseAccessibleHeader = true;
            gvSearch.HeaderRow.TableSection = TableRowSection.TableHeader;


        }
    }
    protected void gvSearch_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName == "Sel")
        //{
        //    //new changes for quantity date 04/07/2014

        //    int rowindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
        //    List<string> InwardIDs = new List<string>();
        //    HiddenField hdfInwardDetailID = (HiddenField)gvSearch.Rows[rowindex].FindControl("hdfInwardDetailID");
        //    HiddenField hdfInwardDetailQtyID = (HiddenField)gvSearch.Rows[rowindex].FindControl("hdfInwardDetailQtyID");
        //    //InwardIDs.Add(hdfInwardDetailID.Value + "," + rowindex);
        //    InwardIDs.Add(hdfInwardDetailQtyID.Value + "," + rowindex);

        //    #region datevalidation

        //    //int id = Convert.ToInt32(hdfInwardDetailID.Value);
        //    Int64 id = Convert.ToInt64("0" + hdfInwardDetailQtyID.Value);
        //    int date = compare(id);
        //    old = Convert.ToInt32("0" + ViewState["oldDate"]);
        //    if (old < date)
        //    {
        //        //txtDateCompare.Text = GenericUtills.ConvertIntToDate(date);
        //        ViewState["oldDate"] = date;
        //    }

        //    #endregion

        //    ViewState["InwardIds"] = InwardIDs;
        //    ShowPartyDescription(InwardIDs);
        //    TotalQuantityCount();
        //    btnovrOk.Visible = true;
        //    //btnovrOk.Focus();
        //}

        //if (e.CommandName == "Detail")
        //{
        //    int selectedindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
        //    int InwardNo = Convert.ToInt32(e.CommandArgument);
        //    BindInwardItemDetail(InwardNo);
        //    BindOutwards(InwardNo);
        //    BindMO(InwardNo);
        //    BindLT(InwardNo);
        //    //string sb = GenericUtills.setfocus(mpInwardDetail.BehaviorID, btnCancelDetailPopup.ClientID);
        //    //Page.ClientScript.RegisterStartupScript(Page.GetType(), "Startup", sb.ToString());
        //    //btnCancelDetailPopup.Focus();
        //    //mpInwardDetail.Show();
        //    //GenericUtills.popup_Show("modalInwardDetail", "", this);
        //    focus = 4;
        //    rowsid.Value = "" + selectedindex;
        //}

        //#region outstanding popup

        //if (e.CommandName == "Customer")
        //{
        //    int selindex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
        //    Int32 partnerid = Convert.ToInt32(e.CommandArgument);
        //    //mploverride.Show();
        //    //GenericUtills.popup_Show("modalOverride", "", this);
        //    //Outstanding.Filloutstanding(partnerid);
        //    //LabourOutstanding.Filloutstanding(partnerid);
        //    btnovrOk.Visible = false;
        //    //btnovrCancel.Focus();
        //}

        //#endregion
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSearch.PageIndex = e.NewPageIndex;
        btnLotSearch_Click(sender, e);
    }
    #endregion

}