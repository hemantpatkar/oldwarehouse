﻿#region Author
/// Hemant Patkar
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.Common;
using AppObjects;
using AppUtility;

public partial class StockInDetail : BigSunPage
{

    #region Variables   

    AppInward objAppInward = null;
    #endregion

    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        ViewState["PostBackID"] = Guid.NewGuid().ToString();
        dateval();
        TimeStamp();
        objAppInward = new AppInward(intRequestingUserID);

        #region IsPostBack

        if (!IsPostBack)
        {
            BydefaultclickEnter();
            Session.Add(TableConstants.InwardSession, objAppInward);
            hdfPageType.Value = Request.QueryString["InwardPageType"];
            DecideLanguage(new AppConvert(Session["LanguageCode"]));
            fillDropdownforInward();
            hdfInward.Value = Request.QueryString["ID"];
            if (hdfInward.Value != "")
            {

                if (new AppConvert(Request.QueryString["Msg"]) != "")
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Saved Successfully" + "','1'" + ");", true);
                txtDateCompare.Text = "";             
                PopulateInward(hdfInward.Value);
                history();
                Rangback.Enabled = false;

                #region preinward              
                prin.Attributes.Add("style", "display:none");
                txtGateRegis.Focus();
                #endregion

                prin.Visible = false;
            }
            else
            {
                txtInwardDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                hdfrevised.Value = "";
                ViewState["pre"] = "";
                txtDateCompare.Text = "11/12/2000";
                CompareValidator2.Operator = ValidationCompareOperator.GreaterThanEqual;              
                #region preinward
                lblPreinwardNo.Visible = true;
                btnsearch.Visible = true;
                UsrtxtParNo.Visible = true;
                prin.Attributes.Add("style", "display:;");
                UsrtxtParNo.Focus();
                #endregion

                prin.Visible = true;

            }
        }
        #endregion

        #region Preinward Validation

        if (grdInward.Rows.Count > 0)
        {
            prin.Visible = false;

        }
        else
        {
            prin.Visible = true;
        }

        #endregion

    }

    #endregion

    #region Button Event
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {

            #region Validation

            #region Double Click Validation
            bool val = ValidateInward();
            if (val == false)
            {
                Response.Write("<script LANGUAGE='JavaScript'> window.confirm('Do You Want to Refresh?');document.location='" + ResolveClientUrl("StockInDetail.aspx?InwardID=" + hdfInward.Value + "&InwardPageType=" + hdfPageType.Value) + "';</script>");
                //Duplicate Save validation 
                return;
            }

            #endregion

            #region Item Validation

            if (grdInward.Rows.Count == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Select Item" + "','2'" + ");", true);
                return;
            }

            #endregion

            #region Validation For Scheme and Location 

            for (int i = 0; i < grdInward.Rows.Count; i++)
            {
                #region Finding data from row of grid

                Label lblScheme = (Label)grdInward.Rows[i].FindControl("lblScheme");
                Label lblStorageLocationIDInitial = (Label)grdInward.Rows[i].FindControl("lblStorageLocationIDInitial");

                #endregion

                if (new AppConvert(lblScheme.Text) <= 0 && new AppConvert(lblStorageLocationIDInitial.Text) <= 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please select Scheme And Location " + "','2'" + ");", true);
                    return;
                }
                if (new AppConvert(lblScheme.Text) <= 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please select Scheme" + "','2'" + ");", true);
                    return;
                }
                if (new AppConvert(lblStorageLocationIDInitial.Text) <= 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please select Location" + "','2'" + ");", true);
                    return;
                }
            }

            #endregion

            #region PreInward Validation

            if (hdfInward.Value == "")
            {
                bool val1 = Preval();
                if (val1 == false)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Pre Inward has been alerady used" + "','2'" + ");", true);
                    return;
                }
            }

            #endregion

            #region Code for Validating AppInward Started Date and AppInward Ended Date
            if (txtInwardStartedOn.Text.Trim() != "" && txtInwardEndedOn.Text.Trim() != "" && txtInwardStartedOn.Text.Trim() != "__/__/____" && txtInwardEndedOn.Text.Trim() != "__/__/____")
            {
                DateTime Inwardstartedon, Inwardendedon;
                Inwardstartedon = new AppConvert(txtInwardStartedOn.Text);
                Inwardendedon = new AppConvert(txtInwardEndedOn.Text);
                if (Inwardstartedon > Inwardendedon)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid Inward Date" + "','2'" + ");", true);
                    return;
                }
                #region Code for Validating  Started On and Ended On Hour and Minute
                if (Inwardstartedon == Inwardendedon)
                {
                    int Shh = 0, Smm = 0, Ehh = 0, Emm = 0;
                    Shh = new AppConvert(txtStartedOnHH.Text.Trim());
                    Smm = new AppConvert(txtStartedOnMM.Text.Trim());
                    Ehh = new AppConvert(txtEndedOnHR.Text.Trim());
                    Emm = new AppConvert(txtEndedOnMM.Text.Trim());
                    if (Shh > Ehh)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid start time and end time" + "','2'" + ");", true);
                        return;
                    }

                    if (Shh == Ehh)
                    {
                        if (Smm > Emm)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid start time and end time" + "','2'" + ");", true);
                            return;
                        }
                    }
                }
                #endregion
            }
            #endregion

            #endregion

            #region AppInward Save

            objAppInward = (AppInward)Session[TableConstants.InwardSession];
            if (objAppInward == null)
            {
                objAppInward = new AppInward(intRequestingUserID);
            }
            if (hdfInward.Value != "")
            {
                int inwardID = new AppConvert(hdfInward.Value);
                objAppInward.ID = inwardID;
                objAppInward.InwardNo = lblInwardNoValue.Text;
            }
            else
            {
                objAppInward.InwardNo = "GRN" + new AppConvert(AppUtility.AutoID.GetNextNumber(1, "GRN", System.DateTime.Now, 4, ResetCycle.Yearly));
            }
            objAppInward.ModifiedBy = intRequestingUserID;
            objAppInward.ModifiedOn = DateTime.Now;
            objAppInward.ReceivedBy_CompanyContact_ID = new AppConvert(hdfReceivedBy.Text);
            objAppInward.Remarks = txtRemarks.Text.Trim();
            objAppInward.Status = new AppConvert((int)RecordStatus.Active);
            objAppInward.GateEntry_ID = new AppConvert(hdfGateRegis.Text);
            objAppInward.InwardDate = new AppConvert(txtInwardDate.Text);
            objAppInward.ReceivedDate = new AppConvert(txtReceivedDate.Text);
            objAppInward.Customer_Company_ID = new AppConvert("0" + UsrhdfPartner.Value); ;


            if (hdfPageType.Value != "")
            {
                if (hdfPageType.Value == "1")
                {
                    objAppInward.Billable = 1;
                }
                else
                {
                    objAppInward.Billable = 0;
                }
            }

            if (txtInwardStartedOn.Text.Trim() != "" && txtInwardStartedOn.Text.Trim() != "__/__/____")
            {
                DateTime startedon = new AppConvert(txtInwardStartedOn.Text + " " + txtStartedOnHH.Text + ":" + txtStartedOnMM.Text);
                objAppInward.InwardStartedOn = startedon;
            }

            if (txtInwardEndedOn.Text.Trim() != "" && txtInwardEndedOn.Text.Trim() != "__/__/____")
            {
                DateTime endedon = new AppConvert(txtInwardEndedOn.Text + " " + txtEndedOnHR.Text + ":" + txtEndedOnMM.Text);
                objAppInward.InwardEndedOn = endedon;
            }

            objAppInward.Save();


            #endregion

            #region Code to save InwardDetail,  Bill Projection, 


            #endregion

            #region Redirect
            Response.Redirect("StockInDetail.aspx?ID=" + objAppInward.ID + "&InwardPageType=" + objAppInward.Billable + "&Msg=1");
            #endregion

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Transaction Failed" + "','2'" + ");", true);
        }
    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        #region Stock Transfer on Approving

        // Boolean Status = false;

        {//Transaction begin here
            try
            {
                #region Validation

                #region Double Click Validation
                bool val = ValidateInward();
                if (val == false)
                {
                    Response.Write("<script LANGUAGE='JavaScript'> window.confirm('Do You Want to Refresh?');document.location='" + ResolveClientUrl("StockInDetail.aspx?InwardID=" + hdfInward.Value + "&InwardPageType=" + hdfPageType.Value) + "';</script>");
                    //Duplicate Save validation 
                    return;
                }

                #endregion

                #region Item Validation

                if (grdInward.Rows.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Select Item" + "','2'" + ");", true);
                    return;
                }

                #endregion

                #region Validation For Scheme and Location 

                for (int i = 0; i < grdInward.Rows.Count; i++)
                {
                    #region Finding data from row of grid

                    Label lblScheme = (Label)grdInward.Rows[i].FindControl("lblScheme");
                    Label lblStorageLocationIDInitial = (Label)grdInward.Rows[i].FindControl("lblStorageLocationIDInitial");

                    #endregion

                    if (new AppConvert(lblScheme.Text) <= 0 && new AppConvert(lblStorageLocationIDInitial.Text) <= 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please select Scheme And Location " + "','2'" + ");", true);
                        return;
                    }
                    if (new AppConvert(lblScheme.Text) <= 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please select Scheme" + "','2'" + ");", true);
                        return;
                    }
                    if (new AppConvert(lblStorageLocationIDInitial.Text) <= 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please select Location" + "','2'" + ");", true);
                        return;
                    }
                }

                #endregion
                 
                #region Code for Validating AppInward Started Date and AppInward Ended Date
                if (txtInwardStartedOn.Text.Trim() != "" && txtInwardEndedOn.Text.Trim() != "" && txtInwardStartedOn.Text.Trim() != "__/__/____" && txtInwardEndedOn.Text.Trim() != "__/__/____")
                {
                    DateTime Inwardstartedon, Inwardendedon;
                    Inwardstartedon = new AppConvert(txtInwardStartedOn.Text);
                    Inwardendedon = new AppConvert(txtInwardEndedOn.Text);
                    if (Inwardstartedon > Inwardendedon)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid Inward Date" + "','2'" + ");", true);
                        return;
                    }
                    #region Code for Validating  Started On and Ended On Hour and Minute
                    if (Inwardstartedon == Inwardendedon)
                    {
                        int Shh = 0, Smm = 0, Ehh = 0, Emm = 0;
                        Shh = new AppConvert(txtStartedOnHH.Text.Trim());
                        Smm = new AppConvert(txtStartedOnMM.Text.Trim());
                        Ehh = new AppConvert(txtEndedOnHR.Text.Trim());
                        Emm = new AppConvert(txtEndedOnMM.Text.Trim());
                        if (Shh > Ehh)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid start time and end time" + "','2'" + ");", true);
                            return;
                        }

                        if (Shh == Ehh)
                        {
                            if (Smm > Emm)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid start time and end time" + "','2'" + ");", true);
                                return;
                            }
                        }
                    }
                    #endregion
                }
                #endregion

                #endregion

                #region Code to save AppInward Old No. and Set Status of Approved to 1

                if (!string.IsNullOrEmpty(hdfInward.Value))
                {
                    int InwardID = new AppConvert(hdfInward.Value);
                    objAppInward = new AppInward(intRequestingUserID, InwardID);
                    objAppInward.Status = new AppConvert((int)RecordStatus.Approve);
                    objAppInward.ModifiedBy = intRequestingUserID;
                    objAppInward.ModifiedOn = DateTime.Now;
                    objAppInward.PreInwardDetailColl.ForEach(x => x.Status = new AppConvert((int)RecordStatus.Approve));
                    objAppInward.Save();
                     
                }

                #endregion

                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Approved Successfully " + "','1'" + ");", true);
                Response.Write("<script LANGUAGE='JavaScript' > window.alert('Saved Successful \\n AppInward Number:-" + objAppInward.InwardNo + "');document.location='" + ResolveClientUrl("StockInDetail.aspx?InwardID=" + objAppInward.ID
                    + "&InwardPageType=" + objAppInward.Billable) + "';</script>");
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Transaction Failed" + "','2'" + ");", true);
            }
        }
        #endregion
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockInSearch.aspx?InwardPageType=" + hdfPageType.Value);

    }
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockInDetail.aspx?InwardPageType=" + hdfPageType.Value);
    }
    protected void btnDeleteInwardDetail_Click(object sender, EventArgs e)
    {
        try
        {

            #region Material Order and Location Transfer Validation

            for (int chek = grdInward.Rows.Count - 1; chek >= 0; chek--)
            {

                Label lblInwardDetID = (Label)grdInward.Rows[chek].FindControl("lblInwardDetID");
                Int32 InwardDetail_ID = new AppConvert("0" + lblInwardDetID.Text);

                AppMaterialOrderDetailColl objAppMaterialOrderDetailColl = new AppMaterialOrderDetailColl(intRequestingUserID);
                objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.InwardDetail_ID, Operators.Equals, InwardDetail_ID);
                objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.Status, Operators.Equals, (int)RecordStatus.Active);
                objAppMaterialOrderDetailColl.Search();

                AppTransferStockLocationDetailColl objAppTransferStockLocationDetailColl = new AppTransferStockLocationDetailColl(intRequestingUserID);
                objAppTransferStockLocationDetailColl.AddCriteria(TransferStockLocationDetail.InwardDetail_ID, Operators.Equals, InwardDetail_ID);
                objAppTransferStockLocationDetailColl.AddCriteria(TransferStockLocationDetail.Status, Operators.Equals, (int)RecordStatus.Active);
                objAppTransferStockLocationDetailColl.Search();


                if (objAppMaterialOrderDetailColl.Count() > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Order has been made from this Inward. \n Please Cancel the Order First..!" + objAppMaterialOrderDetailColl.FirstOrDefault().MaterialOrder.MaterialOrderNo + "','2'" + ");", true);
                    return;
                }
                else if (objAppTransferStockLocationDetailColl.Count() > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Location has been transfered from this Inward. \n Please Cancel the transfer First..!" + objAppTransferStockLocationDetailColl.FirstOrDefault().TransferDate + "','2'" + ");", true);
                    return;
                }
            }

            #endregion

            #region AppInward

            objAppInward = (AppInward)Session[TableConstants.InwardSession];
            objAppInward.Delete();

            #endregion

            for (int chk = grdInward.Rows.Count - 1; chk >= 0; chk--)
            {
                Label lblInwardDetID = (Label)grdInward.Rows[chk].FindControl("lblInwardDetID");
                int InwardDetail_ID = new AppConvert("0" + lblInwardDetID.Text);
                DeleteInwardDetail(InwardDetail_ID);
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Deleted Successfully" + "','1'" + ");", true);

            grdInward.DataSource = objAppInward.InwardDetailColl;
            grdInward.DataBind();

            btnDeleteInwardDetail.Visible = false;
            btnApproved.Visible = false;
            btnSave.Visible = false;
            btnCopy.Visible = true;

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + ex.Message + "','2'" + ");", true);
        }

    }
    protected void btnDetailSave_Click(object sender, EventArgs e)
    {

        objAppInward = (AppInward)Session[TableConstants.InwardSession];

        AppInwardDetail objInwardDetail = new AppInwardDetail(intRequestingUserID);
        if (hdfInwdDetailRowIndex.Value.Trim() != "")
        {
            int rowindex = new AppConvert(hdfInwdDetailRowIndex.Value);
            objInwardDetail = objAppInward.InwardDetailColl[rowindex];
        }
        else
        {
            objAppInward.AddNewInwardDetail(objInwardDetail);
        }

        objInwardDetail.ID = new AppConvert(hdfInwardDetail.Value);
        objInwardDetail.VoucherDetailID = new AppConvert(hdfReferenceID.Value);
        objInwardDetail.Item_ID = new AppConvert(UsrhdfItem.Value);
        objInwardDetail.StorageSchemeRate_ID = new AppConvert(hdfStorageSchemeRateID.Value);
        objInwardDetail.InwardQuantity = new AppConvert(txtItemQuantity.Text.Trim());
        objInwardDetail.Origin_ID = new AppConvert(hdfOriginID.Value.Trim());
        objInwardDetail.Brand_ID = new AppConvert(hdfBrandID.Value.Trim());
        objInwardDetail.Variety_ID = new AppConvert(hdfVarietyID.Value.Trim());
        objInwardDetail.Count = new AppConvert(txtcount.Text.Trim());
        objInwardDetail.StorageLocation_ID = new AppConvert(UsrhdfLocation.Value.Trim());
        objInwardDetail.ModifiedBy = intRequestingUserID;
        objInwardDetail.ModifiedOn = DateTime.Now;
        objInwardDetail.Billable = new AppConvert(hdfPageType.Value);
        if (txtLotNoInternal.Text != "")
            objInwardDetail.LotInternal = new AppConvert(txtLotNoInternal.Text);
        else
        {
            AppLotVersion objAppLotVersion = new AppLotVersion(intRequestingUserID, 1);
            objInwardDetail.LotInternal = objAppLotVersion.LotNumber + 1;
            objInwardDetail.LotVersion = new AppConvert(objAppLotVersion.ID);
            objAppLotVersion.LotNumber = objAppLotVersion.LotNumber + 1;
            objAppLotVersion.Save();
        }
        objInwardDetail.LotCustomer = new AppConvert(txtLotNoCustomer.Text.Trim());
        objInwardDetail.ItemMark = new AppConvert(txtItemMark.Text.Trim());
        objInwardDetail.PackingReference = new AppConvert(txtPackingReference.Text.Trim());
        objInwardDetail.Status = new AppConvert((int)RecordStatus.Active);

        Session.Add(TableConstants.InwardSession, objAppInward);
        grdInward.DataSource = objAppInward.InwardDetailColl;
        grdInward.DataBind();

        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Item Added Sucessfully" + "','1'" + ");", true);

        hdfInwardDetail.Value = "";
        UsrtxtItemN.Text = "";
        txtLotNoInternal.Enabled = true;
        hdfApprovedQty.Value = ""; //clearing to get new approved qty
        btnApprovedVisibility();

        txtLotNoInternal.Focus();
        clear();
        pnlAddItem.Attributes.Add("style", "display:none");
    }
    protected void btnAddItem_Click(object sender, EventArgs e)
    {
        pnlAddItem.Attributes.Add("style", "display:");
        clear();
    }
    #endregion

    #region Function
     
    #region TimeStamp

    public void TimeStamp()
    {
        int usertype = new AppConvert(Session["UserTypeId"]);
        if (usertype != 1)
        {
            bottem.Visible = false;
        }
    }
    #endregion

    #region Refresh

    protected bool ValidateInward()
    {
        int InwardID = new AppConvert("0" + hdfInward.Value);
        string strrev = "";
        AppInward CheckAppInward = new AppInward(intRequestingUserID, InwardID);
        if (InwardID != 0)
        {
            strrev = new AppConvert(CheckAppInward.ModifiedOn);
        }
        if (hdfrevised.Value == strrev)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    #endregion

    #region PreinwardValidation

    protected List<Int64> prelist()
    {
        List<Int64> valdetaillist = new List<Int64>();
        List<Int64> valPrelist = new List<Int64>();
        if (hdfInward.Value == "")
        {
            if (grdInward.Rows.Count > 0)
            {
                for (int i = 0; i < grdInward.Rows.Count; i++)
                {
                    Label lblReferenceID = (Label)grdInward.Rows[i].FindControl("lblReferenceID");
                    Label lblReferenceType = (Label)grdInward.Rows[i].FindControl("lblReferenceType");
                    if (lblReferenceType.Text == "10")
                    {
                        valdetaillist.Add(new AppConvert(lblReferenceID.Text));
                    }
                }

                if (valdetaillist.Count > 0)
                {
                    //IQueryable<PreInwardDetail> sou = from pre in db.PreInwardDetails
                    //                                  where valdetaillist.Contains(pre.PreInwardDetailID)
                    //                                  select pre;

                    //foreach (var item in sou)
                    //{
                    //    valPrelist.Add(item.InwardID);
                    //}
                    //valPrelist = valPrelist.Distinct().ToList();
                }
            }
        }
        return valPrelist;
    }

    protected bool Preval()
    {
        AppInward objInward = (AppInward)Session[TableConstants.InwardSession];

        foreach (var Item in objInward.InwardDetailColl)
        {
            if (Item.VoucherType_ID == 39)
            {
                AppPreInwardDetail objAppPreInwardDetail = new AppPreInwardDetail(intRequestingUserID, Item.VoucherDetailID);

                if (objAppPreInwardDetail.Status != (int)RecordStatus.Approve)
                {
                    return false;
                }
            }
        }
        return true;
    }

    #endregion
     
    #region date validation

    public void dateval()
    {
        //FinancialYearType fy = new FinancialYearType();
        //Int32 fyid = new AppConvert(Session["FYearID"]);
        //fy = db.FinancialYearType.FirstOrDefault(a => a.ID == fyid);
        //if (fy != null)
        {
            Rangeval.MinimumValue = DateTime.Now.ToString("dd/MM/yyyy");
            Rangeval.MaximumValue = DateTime.Now.ToString("dd/MM/yyyy");
        }
        Rangback.MinimumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangback.MaximumValue = DateTime.Now.ToString("dd/MM/yyyy");
    }

    #endregion

    private bool validatePostback()
    {
        // verify duplicate postback
        string postbackid = ViewState["PostBackID"] as string;
        bool isValid = Cache[postbackid] == null;
        if (isValid)
            Cache.Insert(postbackid, true, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(10));
        return isValid;
    }

    public void btndelete()
    {
        if (hdfInward.Value != "")
        {
            int id = new AppConvert(hdfInward.Value);
            //if (db.Inward.FirstOrDefault(x => x.ID == id).Status == 1)
            //{
            //    btnDeleteInwardDetail.Visible = true;
            //    btnDeleteInwardLineItem.Visible = true;
            //    btnCopy.Visible = false;
            //}
            //else
            //{
            //    btnDeleteInwardDetail.Visible = false;
            //    btnDeleteInwardLineItem.Visible = true;
            //    btnCopy.Visible = true;
            //}
        }
    }

    public void btnApprovedVisibility()
    {

        /// New Code By Hemant

        if (hdfInward.Value == "")
        {
            btnApproved.Visible = false;
            hdfapproved.Value = "0";
        }
        else
        {
            int id = new AppConvert(hdfInward.Value);
            //if (db.Inward.FirstOrDefault(x => x.ID == id).IsApproved == 1)
            //{
            //    btnApproved.Visible = false;
            //    hdfapproved.Value = "1";
            //    btnNew.Focus();
            //}
            //else
            //{
            //    btnApproved.Visible = true;
            //    hdfapproved.Value = "0";
            //    btnApproved.Focus();

            //}
        }
    }

    #region PrintVisibility

    public void PrintVisibility()
    {
        if (hdfInward.Value != "" && hdfInward.Value != null)
        {
            int id = new AppConvert(hdfInward.Value);
            //if (db.Inward.Any(x => x.ID == id && x.Status == 1 && x.Status == 1))
            //{
            //    btnPrint.Visible = true;
            //}
            //else
            //{
            //    btnPrint.Visible = false;
            //}
        }
        else
        {
            btnPrint.Visible = false;
        }
    }

    #endregion

    public void PopulateInward(string ID) // function used to fill all fileds while updating data(populate)
    {
        //if (db.ApplicationSetting.Any(a => a.ID == 110))
        //{
        //    string str = db.ApplicationSetting.FirstOrDefault(a => a.ID == 110).Value;
        //    btnPrint.NavigateUrl = "../../Reports/ReportView.aspx?InwardID=" + ID + "&RPName=" + str;
        //}
        //else
        //{
        //    btnPrint.NavigateUrl = "../../Reports/ReportView.aspx?InwardID=" + ID + "&RPName=InwardReceipt";
        //}

        int inwardID = new AppConvert(ID);
        objAppInward = new AppInward(intRequestingUserID, inwardID);
        Int32 RefrenceType = objAppInward.VoucherType_ID;

        UsrhdfPartner.Value = new AppConvert(objAppInward.Customer_Company_ID);
        UsrlblPartner.Text = objAppInward.Customer_Company.Code + "--" + objAppInward.Customer_Company.Name;
        UsrlblPartner1.Text = objAppInward.Customer_Company.Code + "--" + objAppInward.Customer_Company.Name;
        UsrtxtPartnerCode.Text = objAppInward.Customer_Company.Code + "--" + objAppInward.Customer_Company.Name;

        UsrtxtPartnerCode_AutoCompleteExtender.ContextKey = new AppConvert("3," + objAppInward.Customer_Company.Parent_ID);
        UsrhdfPartnerGroup.Value = new AppConvert(objAppInward.Customer_Company.Parent_ID);
        UsrlblPartnerGroup.Text = objAppInward.Customer_Company.Parent.Code + "--" + objAppInward.Customer_Company.Parent.Name;
        UsrtxtPartnerGroupCode.Text = UsrlblPartnerGroup.Text;

        lblInwardNoValue.Text = new AppConvert(objAppInward.InwardNo);
        hdfGateRegis.Text = new AppConvert(objAppInward.GateEntry_ID);
        txtVehicalNo.Text = objAppInward.GateEntry.VehicalNo;
        txtGateRegis.Text = objAppInward.GateEntry.GateNo;
        txtVehName.Text = objAppInward.GateEntry.GateNo;
        txtGateRegis_AutoCompleteExtender.ContextKey = objAppInward.GateEntry_ID.ToString();

        hdfrevised.Value = new AppConvert(objAppInward.ModifiedOn);
        txtInwardDate.Text = new AppConvert(objAppInward.InwardDate);

        int ReceivedBy = new AppConvert(objAppInward.ReceivedBy_CompanyContact_ID);
        txtReceivedBy_AutoCompleteExtender.ContextKey = ReceivedBy.ToString();
        if (objAppInward.ReceivedBy_CompanyContact_ID != 0 || objAppInward.ReceivedBy_CompanyContact_ID > 0)
        {
            hdfReceivedBy.Text = new AppConvert(objAppInward.ReceivedBy_CompanyContact_ID);
            txtReceivedBy.Text = objAppInward.ReceivedBy_CompanyContact.FirstName + " " + objAppInward.ReceivedBy_CompanyContact.LastName;
            lblReceived.Text = txtReceivedBy.Text;
        }
        txtRemarks.Text = objAppInward.Remarks;
        int BillingStatus = new AppConvert(objAppInward.Billable); // use tochek whether BillingStatus is 1 or 0
        txtReceivedDate.Text = new AppConvert(objAppInward.ReceivedDate);

        if (objAppInward.InwardStartedOn != null || new AppConvert(objAppInward.InwardStartedOn) != "")
        {
            DateTime EntryOn = new AppConvert(objAppInward.InwardStartedOn);
            txtInwardStartedOn.Text = new AppConvert(EntryOn.ToString("dd/MM/yyyy"));
            txtStartedOnHH.Text = new AppConvert(EntryOn.Hour);
            if (txtStartedOnHH.Text.Length == 1)
            {
                txtStartedOnHH.Text = "0" + txtStartedOnHH.Text;
            }
            txtStartedOnMM.Text = new AppConvert(EntryOn.Minute);
            if (txtStartedOnMM.Text.Length == 1)
            {
                txtStartedOnMM.Text = "0" + txtStartedOnMM.Text;
            }
        }

        if (objAppInward.InwardEndedOn != null || new AppConvert(objAppInward.InwardEndedOn) != "")
        {
            DateTime EntryOn = new AppConvert(objAppInward.InwardEndedOn);
            txtInwardEndedOn.Text = new AppConvert(EntryOn.ToString("dd/MM/yyyy"));
            txtEndedOnHR.Text = new AppConvert(EntryOn.Hour);
            if (txtEndedOnHR.Text.Length == 1)
            {
                txtEndedOnHR.Text = "0" + txtEndedOnHR.Text;
            }
            txtEndedOnMM.Text = new AppConvert(EntryOn.Minute);
        }

        if (objAppInward.Status > 0)
        {
            btnDeleteInwardDetail.Visible = true;
            btnPrint.Visible = true;
            btnDeleteInwardDetail.Visible = true;
        }
        if (objAppInward.Status == 1)
        {
            btnApproved.Visible = true;
        }
        if (objAppInward.Status == 2)
        {
            btnAddItem.Visible = false;

        }

        #region Fill InwardGrid
        grdInward.DataSource = objAppInward.InwardDetailColl;
        grdInward.DataBind();
        #endregion

        Session.Add(TableConstants.InwardSession, objAppInward);

    }

    public void BydefaultclickEnter() // Method used to hit save buton on ENTER key press
    {

        txtGateRegis.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtInwardDate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtReceivedDate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtInwardStartedOn.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtInwardEndedOn.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtInwardStartedOn.Attributes.Add("onkeydown", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtInwardEndedOn.Attributes.Add("onkeydown", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtRemarks.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");

        txtReceivedBy.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtStartedOnHH.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtStartedOnMM.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtEndedOnHR.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtEndedOnMM.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        UsrtxtPartnerCode.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");

        UsrtxtLocation.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtStorageSchemeRate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        UsrtxtYearMonth.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtItemQuantity.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtItemMark.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtLotNoInternal.Attributes.Add("onkeypress", "return  clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtLotNoCustomer.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtPalletNo.Attributes.Add("onkeypress", "return  clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtContainerNo.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");

        txtPackingReference.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtLotNoInternal.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        UsrtxtItemN.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        UsrtxtParNo.Attributes.Add("onkeypress", "return clickButton(event,'" + btnsearch.ClientID + "')");
        txtBrand.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtOrigin.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtVariety.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");
        txtcount.Attributes.Add("onkeypress", "return clickButton(event,'" + btnDetailSave.ClientID + "')");

    }

    private void DecideLanguage(string languageCode)  // Method used to fill Screen Language on lables & Buttons text property
    {
        try
        {
            lblVeh.Text = "Vehicle" + " " + "No";

            lblPartnerCode.Text = "Customer";// +" " + "Code";
            btnCopy.Text = "Copy";
            btnApproved.Text = "Approve";
            btnPrint.ToolTip = "View" + " " + "Report";
            lblInward.Text = "Inward";
            lblcron.Text = "CreatedOn" + " : ";
            lbledon.Text = "EditedOn";
            lbldelon.Text = "DeletedOn" + " : ";
            lblappon.Text = "Approved" + " " + "On" + " : ";
            lblshowshotcut.Text = "Shortcuts";
            lbledit.Text = "Transaction" + " " + "History";
            btnDeleteInwardDetail_ConfirmButtonExtender.ConfirmText = "DeleteMsg";
            colshotcut.CollapsedText = "Show" + " " + "Shortcuts";
            colshotcut.ExpandedText = "Hide" + " " + "Shortcuts";
            lblPackingRefer.Text = "Packing" + " " + "Reference";
            btnSave.Text = "Save";
            btnNew.Text = "Create New";
            lblGateRegisterID.Text = "Gate" + " " + "Register" + " " + "No";
            lblInwardDate.Text = "Inward" + " " + "Date";
            lblReceivedBy.Text = "Received" + " " + "By";
            lblRemarks.Text = "Remarks";
            btnDetailSave.Text = "Add" + " " + "Item";
            lblItem.Text = "Item";
            lblItemMark.Text = "Item" + " " + "Mark";
            lblStorageSchemeRate.Text = "Scheme";
            lblLotNoInternal.Text = "Lot" + " " + "No";
            lblLotNoCustomer.Text = "CustomerLot";
            lblItemQuantity.Text = "Item" + " " + "Quantity";
            lblPalletNo.Text = "Pallet" + " " + "No";
            lblContainerNo.Text = "Container" + " " + "No";
            lblStorageLocationIDInitial.Text = "Storage" + " " + "Location";
            lblReceivedDate.Text = "Received" + " " + "Date";
            lblInwardStartedOn.Text = "Started" + " " + "On";
            lblInwardEndedOn.Text = "Ended" + " " + "On";

            lblStartedOnHH.Text = "HH";
            lblEndedOnHH.Text = "HH";
            lblStartedOnMM.Text = "MM";
            lblEndedonMM.Text = "MM";

            lblExpiryYearMonth.Text = "ExpiryYearMonth";
            lblPreinwardNo.Text = "Pre" + " " + "Inward" + " " + "No";
            btnsearch.Text = "Search";
            btnpopsearch.Text = "Search";
            btnpopcancel.Text = "Cancel";
            lblparty.Text = "Customer";
            lblpredate.Text = "Date";
            btnPreInwardAddItem.Text = "Add";
            lblpophead.Text = "Pre" + " " + "Inward" + " " + "Detail";


            lblorigin.Text = "Origin";
            lblBrand.Text = "Brand";
            lblVariety.Text = "Variety";
            lblcount.Text = "Count";
            lblPartnerGroupCode.Text = "Customer" + " " + "Group";

            if (hdfPageType.Value != "")
            {
                if (hdfPageType.Value == "1")
                {
                    lblInward.Text = "Billing" + " " + "Inward";
                    Page.Title = "Billing" + " " + "Inward";
                }
                if (hdfPageType.Value == "2")
                {
                    lblInward.Text = "No" + " " + "Billing" + " " + "Inward";
                    Page.Title = "No" + " " + "Billing" + " " + "Inward";
                }
            }
        }
        catch (Exception)
        {

        }
    }

    public void clear()
    {
        UsrtxtItemN.Text = "";
        UsrhdfItem.Value = "";
        UsrhdfLocation.Value = "";
        hdfStorageSchemeRateID.Value = "";
        UsrlblItem.Text = "";
        UsrlblLocation.Text = "";
        hdfStorageSchemeRateName.Value = "";
        UsrtxtLocation.Text = "";
        txtStorageSchemeRate.Text = "";
        UsrtxtYearMonth.Text = "";
        txtPackingReference.Text = "";
        hdfInwdDetailRowIndex.Value = "";
        txtItemMark.Text = "";
        txtLotNoInternal.Text = "";
        txtLotNoCustomer.Text = "";
        txtItemQuantity.Text = "";
        txtPalletNo.Text = "";
        txtContainerNo.Text = "";
        txtOrigin.Text = "";
        HideOrigin.Text = "";
        hdfOriginID.Value = "";
        txtBrand.Text = "";
        HideBrand.Text = "";
        hdfBrandID.Value = "";
        txtVariety.Text = "";
        HideVariety.Text = "";
        hdfVarietyID.Value = "";
        txtcount.Text = "";
        UsrhdfPartner.Value = "";
        UsrhdfPartnerGroup.Value = "";
        UsrlblPartnerGroup.Text = "";
        UsrtxtPartnerGroupCode.Text = "";

        UsrtxtPartnerCode_AutoCompleteExtender.ContextKey = "3";

    }

    public void fillDropdownforInward()
    {
        txtGateRegis_AutoCompleteExtender.ContextKey = "0";
        txtReceivedBy_AutoCompleteExtender.ContextKey = "0";
        UsrtxtLocation_AutoCompleteExtender.ContextKey = new AppConvert(Session["UnitID"]);

    }

    public void DeleteInwardDetail(int InwardDetail_ID)
    {
        #region AppInward Details

        AppInwardDetail objAppInwardDetail = new AppInwardDetail(intRequestingUserID, InwardDetail_ID);
        objAppInwardDetail.ModifiedBy = new AppConvert(Session["UserID"]);
        objAppInwardDetail.ModifiedOn = DateTime.Now;
        objAppInwardDetail.Status = (int)RecordStatus.Deleted;
        objAppInwardDetail.Save();

        #endregion
    }

    #region Calculate net Quantity 

    private int CalNetQty(int TypeID, int InQtyID, int DD)
    {
        int Outqty = 0, LTqty = 0, totqty = 0, InitmQty = 0;
        //InitmQty = new AppConvert("0" + db.InwardDetailQuantities.FirstOrDefault(iq => iq.InwardDetail_ID == InQtyID).Quantity);
        //if (TypeID == 1)
        //{
        //    var Outwrd = db.OutwardDetail.Where(x => x.MaterialOrderDetail.InwardDetail_ID == InQtyID && x.Status == 1 && x.Outward.OutwardDate < DD);
        //    var LT = db.TransferStockLocationDetail.Where(l => l.InwardDetail_ID == InQtyID && l.Status == 1 && l.TransferStockLocation.TransferDate < DD);
        //    if (Outwrd.Count() > 0)
        //    {
        //        Outqty = Outwrd.Sum(a => a.StorageItemQuantity);
        //    }
        //    if (LT.Count() > 0)
        //    {
        //        LTqty = LT.Sum(a => a.StorageItemQuantity);
        //    }
        //}
        //else
        //{
        //    var Outwrd = db.OutwardDetail.Where(x => x.MaterialOrderDetail.InwardDetail_ID == InQtyID && x.Status == 1 && x.Outward.OutwardDate <= DD);
        //    var LT = db.TransferStockLocationDetail.Where(l => l.InwardDetail_ID == InQtyID && l.Status == 1 && l.TransferStockLocation.TransferDate <= DD);
        //    if (Outwrd.Count() > 0)
        //    {
        //        Outqty = Outwrd.Sum(a => a.StorageItemQuantity);
        //    }
        //    if (LT.Count() > 0)
        //    {
        //        LTqty = LT.Sum(a => a.StorageItemQuantity);
        //    }
        //}

        //totqty = InitmQty - (Outqty + LTqty);

        return totqty;
    }

    #endregion

    #region Delete Execting Records 

    private void DeleteExectingProjection(Int64 INQtyID, int UserID, DbTransaction Trans)
    {
        //var whpInward = db.BillingDetail.Where(X => X.TransDetID == INQtyID && X.Status == 1 && X.TransType == 1);
        //foreach (BillingDetail itm in whpInward)
        //{
        //    itm.Status = 2;
        //    itm.RecivedBy = UserID;
        //    itm.RecivedOn = DateTime.Now;
        //    db.SubmitChanges();

        //    #region Location Transfer

        //    if (db.TransferStockLocationDetail.Any(c => c.InwardDetail_ID == itm.TransDetID && c.Status == 1))
        //    {
        //        Int64 LTID = db.TransferStockLocationDetail.FirstOrDefault(c => c.InwardDetail_ID == itm.TransDetID && c.Status == 1).TransferStockLocationDetailID;
        //        Int64 NewInID = db.InwardDetailQuantities.FirstOrDefault(idx => idx.ReferenceID == LTID && idx.ReferenceType == 8 && idx.Status == 1).InwardDetailID;
        //        var INdtQty = db.InwardDetailQuantities.Where(idd => idd.InwardDetailID == NewInID && idd.ReferenceType == 8 && idd.Status == 1);

        //        foreach (InwardDetailQuantity iq in INdtQty)
        //        {
        //            #region AppInward
        //            var whpLocationtransferInward = db.BillingDetail.Where(X => X.TransDetID == iq.InwardDetail_ID && X.Status == 1 && X.TransType == 1);
        //            foreach (BillingDetail LTitm in whpLocationtransferInward)
        //            {
        //                LTitm.Status = 2;
        //                LTitm.RecivedBy = UserID;
        //                LTitm.RecivedOn = DateTime.Now;
        //                db.SubmitChanges();
        //            }

        //            #endregion

        //            #region Outward

        //            var whpLocationtransferOutward = db.BillingDetail.Where(X => X.ReferenceID == iq.InwardDetail_ID && X.Status == 1 && X.TransType == 2);
        //            foreach (BillingDetail LToitm in whpLocationtransferOutward)
        //            {
        //                LToitm.Status = 2;
        //                LToitm.RecivedBy = UserID;
        //                LToitm.RecivedOn = DateTime.Now;
        //                db.SubmitChanges();
        //            }
        //            #endregion
        //        }
        //    }

        //    #endregion

        //}

        //var whpOutward = db.BillingDetail.Where(X => X.ReferenceID == INQtyID && X.Status == 1 && X.TransType == 2);
        //foreach (BillingDetail otm in whpOutward)
        //{
        //    otm.Status = 2;
        //    otm.ModifiedBy = UserID;
        //    otm.ModifiedOn = DateTime.Now;
        //    db.SubmitChanges();
        //}
    }

    #endregion

    #region Insert AppInward Outward Data To Projection 

    private void InsertInwardOutwardProjection(Int64 INQtyID, int FromDate, int Todate, int UserID, DbTransaction Trans)
    {
        //var Indetqty = db.InwardDetailQuantities.FirstOrDefault(xy => xy.InwardDetail_ID == INQtyID && xy.Status == 1);
        //if (Indetqty != null)
        //{
        //    int Calrate = BillingGeneric.GetItemRate(Indetqty.InwardDetail.StorageScheme_ID, Indetqty.InwardDetail.Item_ID, Indetqty.InwardDetail.Inward.Customer_Company_ID, Indetqty.InwardDetail.Inward.InwardDate, Indetqty.InwardDetail.Inward.InwardDate, FromDate);
        //    int Actrate = BillingGeneric.GetActualRate(Indetqty.InwardDetail.StorageScheme_ID, Indetqty.InwardDetail.Item_ID, Indetqty.InwardDetail.Inward.InwardDate, FromDate);
        //    if (Calrate > 0)
        //    {
        //        var SISDT = db.StorageSchemeRate.FirstOrDefault(a => a.StorageItemSchemeDetailID == Indetqty.InwardDetail.StorageItemSchemeDetailID);

        //        #region Insert AppInward
        //        int BillInQty = CalNetQty(1, new AppConvert("0" + Indetqty.InwardDetail_ID), FromDate);
        //        int BillBalQty = CalNetQty(2, new AppConvert("0" + Indetqty.InwardDetail_ID), Todate);
        //        BillingGeneric.ProjectionInsertUpdate(1, new AppConvert("0" + Indetqty.InwardDetail.InwardID), new AppConvert("0" + Indetqty.InwardDetail_ID), Indetqty.InwardDetail.Inward.InwardNo, Indetqty.InwardDetail.Inward.InwardDate, Indetqty.InwardDetail.Inward.UnitID, Indetqty.InwardDetail.Inward.Customer_Company_ID, FromDate, Todate, Indetqty.InwardDetail.LotInternal, Indetqty.InwardDetail.LotVersion, Indetqty.InwardDetail.Item_ID, Indetqty.InwardDetail.StorageScheme_ID, 0, Indetqty.StorageLocation_ID, new AppConvert("0" + Indetqty.Quantity), BillInQty, BillBalQty, 1, 0, Actrate, Calrate, new AppConvert("0" + Indetqty.Weight), "NA", UserID, (SISDT.IsTax ?? 0), 1, 0, db);
        //        #endregion

        //        #region Insert Outward
        //        if (Indetqty.InwardDetail.Scheme.SchemeTypeID == 6)
        //        {
        //            var whoutward = db.OutwardDetail.Where(z => z.MaterialOrderDetail.InwardDetail_ID == Indetqty.InwardDetail_ID && z.Outward.OutwardDate >= FromDate && z.Status == 1);
        //            if (whoutward != null)
        //            {
        //                int outbalqty = 0;
        //                foreach (OutwardDetail otitm in whoutward)
        //                {
        //                    int Week = BillingGeneric.GetWeekandadditionalDays(2, SISDT.StorageScheme_ID, Indetqty.InwardDetail.Inward.InwardDate, otitm.Outward.OutwardDate);
        //                    int AddDays = BillingGeneric.GetWeekandadditionalDays(1, SISDT.StorageScheme_ID, Indetqty.InwardDetail.Inward.InwardDate, otitm.Outward.OutwardDate);

        //                    #region Outward qty calculation

        //                    if (outbalqty == 0)
        //                    {
        //                        outbalqty = BillInQty - new AppConvert("0" + otitm.StorageItemQuantity);
        //                    }
        //                    else
        //                    {
        //                        outbalqty = outbalqty - new AppConvert("0" + otitm.StorageItemQuantity);
        //                    }

        //                    #endregion 

        //                    BillingGeneric.ProjectionInsertUpdate(2, new AppConvert("0" + otitm.OutwardID), new AppConvert("0" + otitm.OutwardDetailID), otitm.Outward.OutwardNo, otitm.Outward.OutwardDate, otitm.Outward.UnitID, otitm.Outward.Customer_Company_IDFrom, FromDate, otitm.Outward.OutwardDate, Indetqty.InwardDetail.LotInternal, Indetqty.InwardDetail.LotVersion, Indetqty.InwardDetail.Item_ID, Indetqty.InwardDetail.StorageScheme_ID, new AppConvert("0" + Indetqty.InwardDetail_ID), Indetqty.StorageLocation_ID, new AppConvert("0" + Indetqty.Quantity), new AppConvert("0" + otitm.StorageItemQuantity), outbalqty, Week, AddDays, Actrate, Calrate, new AppConvert("0" + Indetqty.Weight), "NA", UserID, (SISDT.IsTax ?? 0), 1, 0, db);

        //                    #region Update balance Quantity for related inward (Fixed Days Scheme) in Projection

        //                    if (Indetqty.InwardDetail.Scheme.SchemeTypeID == 6)
        //                    {
        //                        if (db.BillingDetail.Any(x => x.TransType == 1 && x.TransDetID == Indetqty.InwardDetail_ID && x.Status == 1 && x.UnitID == otitm.Outward.UnitID))
        //                        {
        //                            var whLTO = db.BillingDetail.Where(y => y.TransType == 1 && y.TransDetID == Indetqty.InwardDetail_ID && y.Status == 1 && y.UnitID == otitm.Outward.UnitID && y.BillDate <= FromDate).OrderByDescending(x => x.BillDate);
        //                            BillingDetail WHPLT = new BillingDetail();
        //                            WHPLT = db.BillingDetail.FirstOrDefault(c => c.ID == whLTO.FirstOrDefault().BillProjectionID);
        //                            WHPLT.BillingQty = WHPLT.BillingQty - new AppConvert("0" + otitm.StorageItemQuantity);
        //                            WHPLT.BalanceQuantity = WHPLT.BalanceQuantity - new AppConvert("0" + otitm.StorageItemQuantity);
        //                            WHPLT.ModifiedBy = UserID;
        //                            WHPLT.ModifiedOn = DateTime.Now;
        //                            db.SubmitChanges();
        //                        }
        //                    }

        //                    #endregion
        //                }
        //            }

        //        }
        //        else
        //        {
        //            var whoutward = db.OutwardDetail.Where(z => z.InwardDetail_ID == Indetqty.InwardDetail_ID && z.Outward.OutwardDate >= FromDate && z.Outward.OutwardDate <= Todate && z.Status == 1);
        //            if (whoutward != null)
        //            {
        //                foreach (OutwardDetail otitm in whoutward)
        //                {
        //                    int Week = BillingGeneric.GetWeekandadditionalDays(2, SISDT.StorageScheme_ID, Indetqty.InwardDetail.Inward.InwardDate, otitm.Outward.OutwardDate);
        //                    int AddDays = BillingGeneric.GetWeekandadditionalDays(1, SISDT.StorageScheme_ID, Indetqty.InwardDetail.Inward.InwardDate, otitm.Outward.OutwardDate);
        //                    int outbalqty = BillInQty - new AppConvert("0" + otitm.StorageItemQuantity);
        //                    BillingGeneric.ProjectionInsertUpdate(2, new AppConvert("0" + otitm.OutwardID), new AppConvert("0" + otitm.OutwardDetailID), otitm.Outward.OutwardNo, otitm.Outward.OutwardDate, otitm.Outward.UnitID, otitm.Outward.Customer_Company_IDFrom, FromDate, otitm.Outward.OutwardDate, Indetqty.InwardDetail.LotInternal, Indetqty.InwardDetail.LotVersion, Indetqty.InwardDetail.Item_ID, Indetqty.InwardDetail.StorageScheme_ID, new AppConvert("0" + Indetqty.InwardDetail_ID), Indetqty.StorageLocation_ID, new AppConvert("0" + Indetqty.Quantity), new AppConvert("0" + otitm.StorageItemQuantity), outbalqty, Week, AddDays, Actrate, Calrate, new AppConvert("0" + Indetqty.Weight), "NA", UserID, (SISDT.IsTax ?? 0), 1, 0, db);

        //                    #region Update balance Quantity for related inward (Fixed Days Scheme) in Projection

        //                    if (Indetqty.InwardDetail.Scheme.SchemeTypeID == 3)
        //                    {
        //                        if (db.BillingDetail.Any(x => x.TransType == 1 && x.TransDetID == Indetqty.InwardDetail_ID && x.Status == 1 && x.UnitID == otitm.Outward.UnitID))
        //                        {
        //                            var whLTO = db.BillingDetail.Where(y => y.TransType == 1 && y.TransDetID == Indetqty.InwardDetail_ID && y.Status == 1 && y.UnitID == otitm.Outward.UnitID && y.BillDate <= FromDate).OrderByDescending(x => x.BillDate);
        //                            BillingDetail WHPLT = new BillingDetail();
        //                            WHPLT = db.BillingDetail.FirstOrDefault(c => c.ID == whLTO.FirstOrDefault().BillProjectionID);
        //                            WHPLT.BillingQty = WHPLT.BillingQty - new AppConvert("0" + otitm.StorageItemQuantity);
        //                            WHPLT.ModifiedBy = UserID;
        //                            WHPLT.ModifiedOn = DateTime.Now;
        //                            db.SubmitChanges();
        //                        }
        //                    }

        //                    #endregion
        //                }
        //            }
        //        }
        //        #endregion

        //        #region Inserst Location Transfer
        //        if (db.TransferStockLocationDetail.Any(c => c.InwardDetail_ID == Indetqty.InwardDetail_ID && c.Status == 1))
        //        {
        //            Int64 LTID = db.TransferStockLocationDetail.FirstOrDefault(c => c.InwardDetail_ID == Indetqty.InwardDetail_ID && c.Status == 1).TransferStockLocationDetailID;
        //            Int64 NewInID = db.InwardDetailQuantities.FirstOrDefault(idx => idx.ReferenceID == LTID && idx.ReferenceType == 8 && idx.Status == 1).InwardDetailID;
        //            var INdtQty = db.InwardDetailQuantities.Where(idd => idd.InwardDetailID == NewInID && idd.ReferenceType == 8 && idd.Status == 1 && idd.TransferDate >= FromDate && idd.TransferDate <= Todate);

        //            foreach (InwardDetailQuantity idq in INdtQty)
        //            {
        //                if (!db.BillingDetail.Any(a => a.TransDetID == idq.InwardDetail_ID && a.Status == 1))
        //                {
        //                    #region Insert AppInward
        //                    int BillInQtyLT = CalNetQty(1, new AppConvert("0" + idq.InwardDetail_ID), FromDate);
        //                    int BillBalQtyLT = CalNetQty(2, new AppConvert("0" + idq.InwardDetail_ID), Todate);
        //                    BillingGeneric.ProjectionInsertUpdate(1, new AppConvert("0" + idq.InwardDetail.InwardID), new AppConvert("0" + idq.InwardDetail_ID), idq.InwardDetail.Inward.InwardNo, idq.InwardDetail.Inward.InwardDate, idq.InwardDetail.Inward.UnitID, idq.InwardDetail.Inward.Customer_Company_ID, FromDate, Todate, idq.InwardDetail.LotInternal, idq.InwardDetail.LotVersion, idq.InwardDetail.Item_ID, idq.InwardDetail.StorageScheme_ID, 0, idq.StorageLocation_ID, new AppConvert("0" + idq.Quantity), BillInQtyLT, BillBalQtyLT, 1, 0, Actrate, Calrate, new AppConvert("0" + idq.Weight), "NA", UserID, (SISDT.IsTax ?? 0), 1, 0, db);
        //                    #endregion

        //                    #region Update Execting Billing Qty in Projection For Location Transfer

        //                    Int32 OLDINID = new AppConvert("0" + db.TransferStockLocationDetail.FirstOrDefault(c => c.TransferStockLocationDetailID == idq.ReferenceID && c.Status == 1).InwardDetail_ID);

        //                    BillingDetail WHP = new BillingDetail();
        //                    var wh = db.BillingDetail.Where(y => y.TransType == 1 && y.TransDetID == OLDINID && y.Status == 1 && y.UnitID == idq.InwardDetail.Inward.UnitID && y.BillDate <= idq.TransferDate).OrderByDescending(x => x.BillDate);
        //                    WHP = db.BillingDetail.FirstOrDefault(a => a.BillProjectionID == wh.FirstOrDefault().BillProjectionID);
        //                    WHP.BillingQty = (WHP.BillingQty ?? 0) - new AppConvert("0" + idq.Quantity);
        //                    WHP.RecivedBy = UserID;
        //                    WHP.RecivedOn = DateTime.Now;
        //                    db.SubmitChanges();
        //                    #endregion

        //                    #region Insert Outward

        //                    if (idq.InwardDetail.Scheme.SchemeTypeID == 6)
        //                    {
        //                        var LToutward = db.OutwardDetail.Where(z => z.MaterialOrderDetail.InwardDetail_ID == idq.InwardDetail_ID && z.Outward.OutwardDate >= FromDate && z.Status == 1);
        //                        if (LToutward != null)
        //                        {
        //                            int outbalqtyLT = 0;
        //                            foreach (OutwardDetail otitmLT in LToutward)
        //                            {
        //                                int WeekLT = BillingGeneric.GetWeekandadditionalDays(2, SISDT.StorageScheme_ID, idq.InwardDetail.Inward.InwardDate, otitmLT.Outward.OutwardDate);
        //                                int AddDaysLT = BillingGeneric.GetWeekandadditionalDays(1, SISDT.StorageScheme_ID, idq.InwardDetail.Inward.InwardDate, otitmLT.Outward.OutwardDate);

        //                                #region OutQty calulation

        //                                if (outbalqtyLT == 0)
        //                                {
        //                                    outbalqtyLT = BillInQtyLT - new AppConvert("0" + otitmLT.StorageItemQuantity);
        //                                }
        //                                else
        //                                {
        //                                    outbalqtyLT = outbalqtyLT - new AppConvert("0" + otitmLT.StorageItemQuantity);
        //                                }

        //                                #endregion  

        //                                BillingGeneric.ProjectionInsertUpdate(2, new AppConvert("0" + otitmLT.OutwardID), new AppConvert("0" + otitmLT.OutwardDetailID), otitmLT.Outward.OutwardNo, otitmLT.Outward.OutwardDate, otitmLT.Outward.UnitID, otitmLT.Outward.Customer_Company_IDFrom, FromDate, otitmLT.Outward.OutwardDate, idq.InwardDetail.LotInternal, idq.InwardDetail.LotVersion, idq.InwardDetail.Item_ID, idq.InwardDetail.StorageScheme_ID, new AppConvert("0" + idq.InwardDetail_ID), idq.StorageLocation_ID, new AppConvert("0" + idq.Quantity), new AppConvert("0" + otitmLT.StorageItemQuantity), outbalqtyLT, WeekLT, AddDaysLT, Actrate, Calrate, new AppConvert("0" + idq.Weight), "NA", UserID, (SISDT.IsTax ?? 0), 1, 0, db);

        //                                #region Update balance Quantity for related inward in Projection

        //                                if (idq.InwardDetail.Scheme.SchemeTypeID == 6)
        //                                {
        //                                    if (db.BillingDetail.Any(x => x.TransType == 1 && x.TransDetID == idq.InwardDetail_ID && x.Status == 1 && x.UnitID == otitmLT.Outward.UnitID))
        //                                    {
        //                                        var whLTO = db.BillingDetail.Where(y => y.TransType == 1 && y.TransDetID == idq.InwardDetail_ID && y.Status == 1 && y.UnitID == otitmLT.Outward.UnitID && y.BillDate <= FromDate).OrderByDescending(x => x.BillDate);
        //                                        BillingDetail WHPLT = new BillingDetail();
        //                                        WHPLT = db.BillingDetail.FirstOrDefault(c => c.BillProjectionID == whLTO.FirstOrDefault().BillProjectionID);
        //                                        WHPLT.BillingQty = WHPLT.BillingQty - new AppConvert("0" + otitmLT.StorageItemQuantity);
        //                                        WHPLT.BalanceQuantity = WHPLT.BalanceQuantity - new AppConvert("0" + otitmLT.StorageItemQuantity);
        //                                        WHPLT.RecivedBy = UserID;
        //                                        WHPLT.RecivedOn = DateTime.Now;
        //                                        db.SubmitChanges();
        //                                    }
        //                                }

        //                                #endregion
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        var LToutward = db.OutwardDetail.Where(z => z.MaterialOrderDetail.InwardDetail_ID == idq.InwardDetail_ID && z.Outward.OutwardDate >= FromDate && z.Outward.OutwardDate <= Todate && z.Status == 1);
        //                        if (LToutward != null)
        //                        {
        //                            foreach (OutwardDetail otitmLT in LToutward)
        //                            {
        //                                int WeekLT = BillingGeneric.GetWeekandadditionalDays(2, SISDT.StorageScheme_ID, idq.InwardDetail.Inward.InwardDate, otitmLT.Outward.OutwardDate);
        //                                int AddDaysLT = BillingGeneric.GetWeekandadditionalDays(1, SISDT.StorageScheme_ID, idq.InwardDetail.Inward.InwardDate, otitmLT.Outward.OutwardDate);
        //                                int outbalqtyLT = BillInQtyLT - new AppConvert("0" + otitmLT.StorageItemQuantity);
        //                                BillingGeneric.ProjectionInsertUpdate(2, new AppConvert("0" + otitmLT.OutwardID), new AppConvert("0" + otitmLT.OutwardDetailID), otitmLT.Outward.OutwardNo, otitmLT.Outward.OutwardDate, otitmLT.Outward.UnitID, otitmLT.Outward.Customer_Company_IDFrom, FromDate, otitmLT.Outward.OutwardDate, idq.InwardDetail.LotInternal, idq.InwardDetail.LotVersion, idq.InwardDetail.Item_ID, idq.InwardDetail.StorageScheme_ID, new AppConvert("0" + idq.InwardDetail_ID), idq.StorageLocation_ID, new AppConvert("0" + idq.Quantity), new AppConvert("0" + otitmLT.StorageItemQuantity), outbalqtyLT, WeekLT, AddDaysLT, Actrate, Calrate, new AppConvert("0" + idq.Weight), "NA", UserID, (SISDT.IsTax ?? 0), 1, 0, db);


        //                                #region Update balance Quantity for related inward (Fixed Days Scheme) in Projection

        //                                if (idq.InwardDetail.Scheme.SchemeTypeID == 3)
        //                                {
        //                                    if (db.BillingDetail.Any(x => x.TransType == 1 && x.TransDetID == idq.InwardDetail_ID && x.Status == 1 && x.UnitID == otitmLT.Outward.UnitID))
        //                                    {
        //                                        var whLTO = db.BillingDetail.Where(y => y.TransType == 1 && y.TransDetID == idq.InwardDetail_ID && y.Status == 1 && y.UnitID == otitmLT.Outward.UnitID && y.BillDate <= FromDate).OrderByDescending(x => x.BillDate);
        //                                        BillingDetail WHPLT = new BillingDetail();
        //                                        WHPLT = db.BillingDetail.FirstOrDefault(c => c.BillProjectionID == whLTO.FirstOrDefault().BillProjectionID);
        //                                        WHPLT.BillingQty = WHPLT.BillingQty - new AppConvert("0" + otitmLT.StorageItemQuantity);
        //                                        WHPLT.ModifiedBy = UserID;
        //                                        WHPLT.ModifiedOn = DateTime.Now;
        //                                        db.SubmitChanges();
        //                                    }
        //                                }

        //                                #endregion

        //                            }
        //                        }

        //                    }


        //                    #endregion
        //                }
        //            }

        //        }
        //        #endregion
        //    }
        //}
    }

    #endregion

    #endregion

    #region Grid Events

    protected void grdInward_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Sel")
        {
            try
            {

                #region FindControls
                objAppInward = (AppInward)Session[TableConstants.InwardSession];
                AppInwardDetail objAppInwardDetail = new AppInwardDetail(intRequestingUserID);
                int row = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
                objAppInwardDetail = objAppInward.InwardDetailColl[row];
                hdfInwdDetailRowIndex.Value = new AppConvert(row);
                #endregion

                int unitID = new AppConvert(Session["UnitID"]);
                clear();

                hdfInwdDetailRowIndex.Value = new AppConvert(row); //use to set hdf value while editing item from gridview

                #region FindControls
                LinkButton lnkSelect = (LinkButton)grdInward.Rows[row].FindControl("lnkSelect");



                #endregion

                //-------populating value in fields

                hdfInwardDetail.Value = new AppConvert(objAppInwardDetail.ID);
                hdflotversion.Value = new AppConvert(objAppInwardDetail.LotVersion);
                hdfversion.Value = new AppConvert(objAppInwardDetail.LotVersion);
                UsrhdfItem.Value = new AppConvert(objAppInwardDetail.Item_ID);
                txtPackingReference.Text = new AppConvert(objAppInwardDetail.PackingReference);

                UsrlblItem.Text = new AppConvert(lnkSelect.Text); //not is use bcoz textbox converted to combobox
                UsrtxtItemN.Text = new AppConvert(lnkSelect.Text); //not is use bcoz textbox converted to combobox
                UsrhdfItem.Value = lnkSelect.CommandArgument;
                UsrlblItem.ToolTip = new AppConvert(lnkSelect.Text);

                if (hdfInwardDetail.Value != "0" && !string.IsNullOrEmpty(hdfInwardDetail.Value))
                {
                    ACE_txtStorageSchemeRate.ContextKey = new AppConvert(objAppInwardDetail.Item_ID) + "," + new AppConvert(objAppInward.InwardDate);
                }
                else
                {
                    ACE_txtStorageSchemeRate.ContextKey = new AppConvert(objAppInwardDetail.Item_ID) + "," + new AppConvert(DateTime.Now.Date);
                }

                txtStorageSchemeRate.Text = new AppConvert(objAppInwardDetail.StorageSchemeRate.StorageScheme.Code);
                hdfStorageSchemeRateID.Value = new AppConvert(objAppInwardDetail.StorageSchemeRate_ID);
                hdfStorageSchemeRateName.Value = txtStorageSchemeRate.Text;

                hdfReferenceID.Value = new AppConvert(objAppInwardDetail.VoucherDetailID);
                hdfReferenceType.Value = new AppConvert(objAppInwardDetail.VoucherType_ID);
                txtItemMark.Text = new AppConvert(objAppInwardDetail.ItemMark);
                txtLotNoInternal.Text = new AppConvert(objAppInwardDetail.LotInternal);
                txtLotNoCustomer.Text = new AppConvert(objAppInwardDetail.LotCustomer);
                txtItemQuantity.Text = new AppConvert(objAppInwardDetail.InwardQuantity);
                hdfItemQuantity.Value = new AppConvert(objAppInwardDetail.InwardQuantity);
                hdfApprovedQty.Value = txtItemQuantity.Text;
                txtPalletNo.Text = new AppConvert(objAppInwardDetail.PalletNo);
                txtContainerNo.Text = new AppConvert(objAppInwardDetail.ContainerNo);
                UsrtxtYearMonth.Text = new AppConvert(objAppInwardDetail.ExpiryDate);
                UsrhdfLocation.Value = new AppConvert(objAppInwardDetail.StorageLocation_ID);
                UsrlblLocation.Text = new AppConvert(objAppInwardDetail.StorageLocation.Name);
                UsrtxtLocation.Text = UsrlblLocation.Text;

                #region Origin Brand Variety

                txtOrigin.Text = new AppConvert(objAppInwardDetail.Origin.Name);
                HideOrigin.Text = txtOrigin.Text;
                hdfOriginID.Value = new AppConvert(objAppInwardDetail.Origin_ID);
                txtOrigin_AutoCompleteExtender.ContextKey = new AppConvert(objAppInwardDetail.Item_ID);

                txtBrand.Text = new AppConvert(objAppInwardDetail.Brand.Name);
                HideBrand.Text = txtBrand.Text;
                hdfBrandID.Value = new AppConvert(objAppInwardDetail.Brand_ID);
                txtBrand_AutoCompleteExtender.ContextKey = new AppConvert(objAppInwardDetail.Item_ID);

                txtVariety.Text = new AppConvert(objAppInwardDetail.Variety.Name);
                HideVariety.Text = txtVariety.Text;
                hdfVarietyID.Value = new AppConvert(objAppInwardDetail.Variety_ID);
                txtVariety_AutoCompleteExtender.ContextKey = new AppConvert(objAppInwardDetail.Item_ID);

                txtcount.Text = new AppConvert(objAppInwardDetail.Count);

                #endregion

                pnlAddItem.Attributes.Add("style", "display:");

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + ex.Message + "','2'" + ");", true);
                throw;
            }
        }

        if (e.CommandName == "Del")
        {
            try
            {
                objAppInward = (AppInward)Session[TableConstants.InwardSession];
                AppInwardDetail objAppInwardDetail = new AppInwardDetail(intRequestingUserID);
                int row = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
                objAppInwardDetail = objAppInward.InwardDetailColl[row];

                if (objAppInwardDetail.ID != 0)
                {
                    #region Validation

                    AppMaterialOrderDetailColl objAppMaterialOrderDetailColl = new AppMaterialOrderDetailColl(intRequestingUserID);
                    objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.InwardDetail_ID, Operators.Equals, objAppInwardDetail.ID);
                    objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.Status, Operators.Equals, (int)RecordStatus.Active);
                    objAppMaterialOrderDetailColl.Search();

                    if (objAppMaterialOrderDetailColl.Count() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Order has been made from this Inward. \n Please Cancel the Order First..!" + "','2'" + ");", true);
                        return;
                    }

                    #endregion

                    //GenericUtills.InsertTransactionLog(121, indid, 4, new AppConvert(Session["UserID"]));
                }

                objAppInward.InwardDetailColl.Remove(objAppInwardDetail);

                grdInward.DataSource = objAppInward.InwardDetailColl;
                grdInward.DataBind();

                Session.Add(TableConstants.InwardSession, objAppInward);
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Item deleted successfully" + "','1'" + ");", true);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + ex.Message + "','2'" + ");", true);
                throw;
            }
        }
    }

    protected void grdPreinward_PreRender(object sender, EventArgs e)
    {
        if (grdPreinward.Rows.Count > 0)
        {
            grdPreinward.UseAccessibleHeader = true;
            grdPreinward.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

    protected void grdedit_OnPreRender(object sender, EventArgs e)
    {
        if (grdedit.Rows.Count > 0)
        {

            grdedit.UseAccessibleHeader = true;
            grdedit.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion

    #region TextBox Events
    protected void txtInwardStartedOn_TextChanged(object sender, EventArgs e)
    {
        if (txtInwardStartedOn.Text.Trim() != "")
        {
            rfvtxtStartedOnHH.Enabled = true;
            metxtStartedOnHH.Enabled = true;
            mevtxtStartedOnHH.Enabled = true;

            rfvtxtStartedOnMM.Enabled = true;
            metxtStartedOnMM.Enabled = true;
            mevtxtStartedOnMM.Enabled = true;
        }
        else
        {
            rfvtxtStartedOnHH.Enabled = false;
            metxtStartedOnHH.Enabled = false;
            mevtxtStartedOnHH.Enabled = false;

            rfvtxtStartedOnMM.Enabled = false;
            metxtStartedOnMM.Enabled = false;
            mevtxtStartedOnMM.Enabled = false;
        }
        txtStartedOnHH.Focus();
    }
    protected void txtInwardEndedOn_TextChanged(object sender, EventArgs e)
    {


        if (txtInwardEndedOn.Text.Trim() != "")
        {
            rfvmetxtEndedOnHR.Enabled = true;
            metxtEndedOnHR.Enabled = true;
            mevtxtEndedOnHR.Enabled = true;

            rfvtxtEndedOnMM.Enabled = true;
            metxtEndedOnMM.Enabled = true;
            mevtxtEndedOnMM.Enabled = true;
        }
        else
        {
            rfvmetxtEndedOnHR.Enabled = false;
            metxtEndedOnHR.Enabled = false;
            mevtxtEndedOnHR.Enabled = false;

            rfvtxtEndedOnMM.Enabled = false;
            metxtEndedOnMM.Enabled = false;
            mevtxtEndedOnMM.Enabled = false;
        }
        txtEndedOnHR.Focus();
    }

    #endregion

    #region Report

    public void ViewReport(string URLPath, EventArgs e)
    {
        StringBuilder strContent = new StringBuilder();
        string lasturl = System.Configuration.ConfigurationManager.AppSettings["ReportServerURL1"].ToString() + URLPath; // Define inside web.config file
        strContent.Append("window.open('" + lasturl + "','',',toolbar= 0,menubar= 0,status=0,location=0,resizable=1,scrollbars=1');");
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "subscribescript", strContent.ToString(), true);
    }

    #endregion

    #region showing history

    public void history()
    {
        //int refid = new AppConvert(hdfInward.Value);
        //IQueryable<TransactionLog> Tlog = from tl in db.TransactionLogs where tl.ReferenceID == refid && tl.ReferenceType == 10 select tl;
        //var cr = from te in Tlog where te.TransactionType == 1 select te;
        //if (cr.Count() != 0)
        //{
        //    int uid = db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 1 && x.ReferenceID == refid && x.ReferenceType == 10).UserID;
        //    lblcruser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == uid).UserFullName;
        //    lblcrdate.Text = new AppConvert(db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 1 && x.ReferenceID == refid && x.ReferenceType == 10).CreateDate);
        //}
        //var edit = (from te in Tlog where te.TransactionType == 2 select te).OrderByDescending(l => l.CreateDate).Take(1);
        //if (edit.Count() != 0)
        //{
        //    int eid = edit.FirstOrDefault().UserID; //db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 2 && x.ReferenceID == refid && x.ReferenceType == 10);
        //    lbleduser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == eid).UserFullName;
        //    DateTime str = edit.FirstOrDefault().CreateDate;
        //    lbleddate.Text = new AppConvert(str);
        //    lnkdetail.Visible = true;
        //}
        //var del = from te in Tlog where te.TransactionType == 4 select te;
        //if (del.Count() != 0)
        //{
        //    int id = db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 4 && x.ReferenceID == refid && x.ReferenceType == 10).UserID;
        //    lbldluser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == id).UserFullName;
        //    lbldldate.Text = new AppConvert(db.TransactionLogs.FirstOrDefault(x => x.TransactionType == 4 && x.ReferenceID == refid && x.ReferenceType == 10).CreateDate);
        //}
        ////var app = from iw in db.Inwards where iw.InwardID == refid select iw;
        //if (db.Inward.Any(a => a.InwardID == refid))
        //{
        //    var appinw = db.Inward.FirstOrDefault(a => a.InwardID == refid);
        //    if (appinw.ApprovedBy != null)
        //    {
        //        lalapuser.Text = db.UserInfoes.FirstOrDefault(a => a.UserID == appinw.ApprovedBy).UserFullName;
        //        lblapdate.Text = appinw.ApprovedOn.ToString();
        //    }
        //}

    }

    protected void btnhistory_Click(object sender, EventArgs e)
    {
        //if (hdfInward.Value != "")
        //{
        //    int refid = new AppConvert(hdfInward.Value);
        //    IQueryable<TransactionLog> Tlog = (from tl in db.TransactionLogs where tl.ReferenceID == refid && tl.ReferenceType == 10 select tl).OrderByDescending(p => p.CreateDate);
        //    var edit = from te in Tlog where te.TransactionType == 2 select te;
        //    if (edit.Count() != 0)
        //    {
        //        grdedit.DataSource = edit.ToList();
        //        grdedit.DataBind();
        //    }
        //}

        //popup_Show("modalhistory", "", this);
    }

    #endregion

    #region preInward

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        if (UsrhdfPre.Value != "" && UsrhdfPre.Value != null)
        {
            AppInward objInward = (AppInward)Session[TableConstants.InwardSession];

            int inwardID = new AppConvert(UsrhdfPre.Value);
            AppPreInward objAppPreInward = new AppPreInward(intRequestingUserID, inwardID);

            UsrhdfPartner.Value = new AppConvert(objAppPreInward.Customer_Company_ID);
            UsrlblPartner.Text = objAppPreInward.Customer_Company.Code + "--" + objAppPreInward.Customer_Company.Code;
            UsrlblPartner1.Text = UsrlblPartner.Text;
            UsrtxtPartnerCode.Text = UsrlblPartner.Text;

            UsrtxtPartnerCode_AutoCompleteExtender.ContextKey = new AppConvert("3," + objAppPreInward.Customer_Company.Parent_ID);
            UsrhdfPartnerGroup.Value = new AppConvert(objAppPreInward.Customer_Company.Parent_ID);
            UsrlblPartnerGroup.Text = objAppPreInward.Customer_Company.Parent.Code + "--" + objAppPreInward.Customer_Company.Parent.Name;
            UsrtxtPartnerGroupCode.Text = UsrlblPartnerGroup.Text;

            hdfGateRegis.Text = new AppConvert(objAppPreInward.GateEntry_ID);
            txtGateRegis.Text = objAppPreInward.GateEntry.GateNo + ":-" + objAppPreInward.GateEntry.VehicalNo;
            txtVehicalNo.Text = objAppPreInward.GateEntry.VehicalNo;
            txtVehName.Text = txtGateRegis.Text;
            txtGateRegis_AutoCompleteExtender.ContextKey = objAppPreInward.GateEntry_ID.ToString();
            txtInwardDate.Text = new AppConvert(objAppPreInward.PreInwardDate);
            txtReceivedBy_AutoCompleteExtender.ContextKey = objAppPreInward.ReceivedBy_CompanyContact_ID.ToString();
            hdfReceivedBy.Text = new AppConvert(objAppPreInward.ReceivedBy_CompanyContact_ID);
            txtReceivedBy.Text = objAppPreInward.ReceivedBy_CompanyContact.FullName;
            lblReceived.Text = txtReceivedBy.Text;
            txtRemarks.Text = objAppPreInward.Remarks;
            txtReceivedDate.Text = new AppConvert(objAppPreInward.ReceivedDate);


            objInward.Customer_Company_ID = objAppPreInward.Customer_Company_ID;
            objInward.GateEntry_ID = objAppPreInward.GateEntry_ID;
            objInward.ReceivedBy_CompanyContact_ID = objAppPreInward.ReceivedBy_CompanyContact_ID;
            objInward.ReceivedDate = objAppPreInward.ReceivedDate;
            objInward.Remarks = objAppPreInward.Remarks;
            objInward.InwardDate = objAppPreInward.PreInwardDate;
            objInward.Company_ID = objAppPreInward.Company_ID;


            if (objAppPreInward.PreInwardDetailColl.Count > 0)
            {
                objInward.InwardDetailColl.AddRange(objAppPreInward.PreInwardDetailColl.
                    Select(x => new AppInwardDetail(intRequestingUserID)
                    {

                        StorageSchemeRate_ID = x.StorageSchemeRate_ID,
                        InwardQuantity = x.Quantity,
                        Item_ID = x.Item_ID,
                        Brand_ID = x.Brand_ID,
                        Origin_ID = x.Origin_ID,
                        Variety_ID = x.Variety_ID,
                        Count = x.Count,
                        VoucherType_ID = 39,
                        VoucherDetailID = x.ID,
                        StorageLocation_ID = x.StorageLocation_ID,
                        LotInternal = x.LotInternal,
                        LotVersion = x.LotVersion,
                        LotCustomer = x.LotCustomer,
                        ItemMark = x.ItemMark,
                        ContainerNo = x.ContainerNo,
                        PalletNo = x.PalletNo,
                        PackingReference = x.PackingReference,
                        Billable = x.Billable,
                        TaxType_ID = x.TaxType_ID,
                        ExpiryDate = x.ExpiryDate,
                        Status = x.Status,
                        Type = x.Type
                    })
                );
            }

            grdInward.DataSource = objInward.InwardDetailColl;
            grdInward.DataBind();

            #region Hide preinward search
            prin.Visible = false;
            txtReceivedBy.Focus();
            #endregion

        }

    }
    protected void Lnkpreinward_Click(object sender, EventArgs e)
    {


        if (!string.IsNullOrEmpty(hdfInward.Value))
        {

            popup_Hide("modalprein", "", this);
        }
        else
        {
            if (!string.IsNullOrEmpty(UsrhdfPartner.Value))
            {
                int id = new AppConvert(UsrhdfPartner.Value);
                hdfParty.Value = UsrhdfPartner.Value;
                //txtparty.Text = db.Company.FirstOrDefault(a => a.ID == id).Code + "--" +
                //                db.Company.FirstOrDefault(a => a.ID == id).Name;
                HideParty.Text = txtparty.Text;
                txtparty.ReadOnly = true;
                btnpopsearch.Focus();
            }
            else
            {
                txtparty.Text = "";
                hdfParty.Value = "";
                HideParty.Text = "";
                txtparty.ReadOnly = false;
                txtparty.Focus();
            }

            txtDate.Text = "";
            grdPreinward.DataSource = null;
            grdPreinward.DataBind();
            btnPreInwardAddItem.Visible = false;

            popup_Show("modalprein", "ContentPlaceHolder1_txtparty", this);
        }

    }
    protected void btnpopsearch_Click(object sender, EventArgs e)
    {
        int Partid = 0;
        DateTime? pdate = null;
        AppPreInwardColl objAppPreInwardColl = new AppPreInwardColl(intRequestingUserID);
        objAppPreInwardColl.AddCriteria(PreInward.Status, Operators.Equals, (int)RecordStatus.Active);

        if (!string.IsNullOrEmpty(hdfParty.Value))
        {
            Partid = new AppConvert(hdfParty.Value);
            objAppPreInwardColl.AddCriteria(PreInward.Customer_Company_ID, Operators.Equals, Partid);

        }
        if (!string.IsNullOrEmpty(txtDate.Text) && txtDate.Text != "__/__/____")
        {
            pdate = new AppConvert(txtDate.Text);
            objAppPreInwardColl.AddCriteria(PreInward.PreInwardDate, Operators.Equals, pdate);
        }

        objAppPreInwardColl.Search();
        objAppPreInwardColl.Sort(AppPreInward.Comparision);
        grdPreinward.DataSource = objAppPreInwardColl;
        grdPreinward.DataBind();

        if (grdPreinward.Rows.Count > 0)
        {
            btnPreInwardAddItem.Visible = true;
        }

        popup_Show("modalprein", "ContentPlaceHolder1_txtparty", this);
    }    
    protected void btnPreInwardAddItem_Click(object sender, EventArgs e)
    {
        AppInward objInward = (AppInward)Session[TableConstants.InwardSession];

        #region To Get IDs of PreInward ID from Search Grid

        for (int i = 0; i < grdPreinward.Rows.Count; i++)
        {
            CheckBox cbSelect = (CheckBox)grdPreinward.Rows[i].FindControl("chkselect");
            if (cbSelect.Checked == true)
            {
                HiddenField hdfPreid = (HiddenField)grdPreinward.Rows[i].FindControl("hdfPreid");
                if (!string.IsNullOrEmpty(hdfPreid.Value))
                {
                    int PreInward_ID = new AppConvert(hdfPreid.Value);
                    AppPreInward objAppPreInward = new AppPreInward(intRequestingUserID, PreInward_ID);

                    if (i == 0)
                    {
                        UsrhdfPartner.Value = new AppConvert(objAppPreInward.Customer_Company_ID);
                        UsrlblPartner.Text = objAppPreInward.Customer_Company.Code + "--" + objAppPreInward.Customer_Company.Code;
                        UsrlblPartner1.Text = UsrlblPartner.Text;
                        UsrtxtPartnerCode.Text = UsrlblPartner.Text;


                        UsrtxtPartnerCode_AutoCompleteExtender.ContextKey = new AppConvert("3," + objAppPreInward.Customer_Company.Parent_ID);
                        UsrhdfPartnerGroup.Value = new AppConvert(objAppPreInward.Customer_Company.Parent_ID);
                        UsrlblPartnerGroup.Text = objAppPreInward.Customer_Company.Parent.Code + "--" + objAppPreInward.Customer_Company.Parent.Name;
                        UsrtxtPartnerGroupCode.Text = UsrlblPartnerGroup.Text;

                        hdfGateRegis.Text = new AppConvert(objAppPreInward.GateEntry_ID);
                        txtGateRegis.Text = objAppPreInward.GateEntry.GateNo + ":-" + objAppPreInward.GateEntry.VehicalNo;
                        txtVehicalNo.Text = objAppPreInward.GateEntry.VehicalNo;
                        txtVehName.Text = txtGateRegis.Text;
                        txtGateRegis_AutoCompleteExtender.ContextKey = objAppPreInward.GateEntry_ID.ToString();
                        txtInwardDate.Text = new AppConvert(objAppPreInward.PreInwardDate);
                        txtReceivedBy_AutoCompleteExtender.ContextKey = objAppPreInward.ReceivedBy_CompanyContact_ID.ToString();
                        hdfReceivedBy.Text = new AppConvert(objAppPreInward.ReceivedBy_CompanyContact_ID);
                        txtReceivedBy.Text = objAppPreInward.ReceivedBy_CompanyContact.FullName;
                        lblReceived.Text = txtReceivedBy.Text;
                        txtRemarks.Text = objAppPreInward.Remarks;
                        txtReceivedDate.Text = new AppConvert(objAppPreInward.ReceivedDate);

                        objInward.Customer_Company_ID = objAppPreInward.Customer_Company_ID;
                        objInward.GateEntry_ID = objAppPreInward.GateEntry_ID;
                        objInward.ReceivedBy_CompanyContact_ID = objAppPreInward.ReceivedBy_CompanyContact_ID;
                        objInward.ReceivedDate = objAppPreInward.ReceivedDate;
                        objInward.Remarks = objAppPreInward.Remarks;
                        objInward.InwardDate = objAppPreInward.PreInwardDate;
                        objInward.Company_ID = objAppPreInward.Company_ID;

                    }

                    if (objAppPreInward.PreInwardDetailColl.Count > 0)
                    {
                        objInward.InwardDetailColl.AddRange(objAppPreInward.PreInwardDetailColl.
                            Select(x => new AppInwardDetail(intRequestingUserID)
                            {
                                StorageSchemeRate_ID = x.StorageSchemeRate_ID,
                                InwardQuantity = x.Quantity,
                                Item_ID = x.Item_ID,
                                Brand_ID = x.Brand_ID,
                                Origin_ID = x.Origin_ID,
                                Variety_ID = x.Variety_ID,
                                Count = x.Count,
                                VoucherType_ID = 39,
                                VoucherDetailID = x.ID,
                                StorageLocation_ID = x.StorageLocation_ID,
                                LotInternal = x.LotInternal,
                                LotVersion = x.LotVersion,
                                LotCustomer = x.LotCustomer,
                                ItemMark = x.ItemMark,
                                ContainerNo = x.ContainerNo,
                                PalletNo = x.PalletNo,
                                PackingReference = x.PackingReference,
                                Billable = x.Billable,
                                TaxType_ID = x.TaxType_ID,
                                ExpiryDate = x.ExpiryDate,
                                Status = x.Status,
                                Type = x.Type
                            })
                        );
                    }
                }
                cbSelect.Checked = false;
            }
        }
        #endregion

        Session.Add(TableConstants.InwardSession, objInward);
        grdInward.DataSource = objInward.InwardDetailColl;
        grdInward.DataBind();

        prin.Visible = false;
        popup_Hide("modalprein", "", this);
    }

    #endregion
   
}
