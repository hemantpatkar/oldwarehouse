﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" Culture="en-gb" AutoEventWireup="true" Inherits="TranshipmentDetail" Codebehind="TranshipmentDetail.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function PartnerGroupSearch(sender, eventArgs) {

            var hdpartID = $get('<%= UsrhdfPartnerGroup.ClientID %>');
            var lblPartname = $get('<%= UsrlblPartnerGroup.ClientID %>');
            var partycode = $get('<%= UsrtxtPartnerGroupCode.ClientID %>');

            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=UsrtxtPartnerCode_AutoCompleteExtender.ClientID %>').set_contextKey("3," + eventArgs.get_value());
            }
        }


        function ClearPartnerGroupSearch() {
            var OrignalProfitCenter = $get('<%=UsrtxtPartnerGroupCode.ClientID %>');
            var HidenProfitCenter = $get('<%= UsrlblPartnerGroup.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=UsrhdfPartnerGroup.ClientID %>').value = "";
                $get('<%=UsrlblPartnerGroup.ClientID %>').value = "";
                $get('<%=UsrtxtPartnerGroupCode.ClientID %>').value = "";
                $find('<%=UsrtxtPartnerCode_AutoCompleteExtender.ClientID %>').set_contextKey("0");
            }
        }


        function PartnerSearch(sender, eventArgs) {
            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());
            var hdpartID = $get('<%= UsrhdfPartner.ClientID %>');
            var lblPartname = $get('<%= UsrlblPartner1.ClientID %>');
            var partycode = $get('<%= UsrtxtPartnerCode.ClientID %>');
            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            lblPartname.defaultvalue = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=UsrtxtPartnerGroupCode_AutoCompleteExtender.ClientID %>').set_contextKey(eventArgs.get_value());
            }

        }

        function ClearPartnerSearch() {
            var OrignalProfitCenter = $get('<%=UsrtxtPartnerCode.ClientID %>');
            var HidenProfitCenter = $get('<%= UsrlblPartner1.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=UsrlblPartner1.ClientID %>').value = "";
                $get('<%=UsrtxtPartnerCode.ClientID %>').value = "";
                $get('<%=UsrhdfPartner.ClientID %>').value = "";
            }
        }


        function GrditemSearch(sender, eventArgs) {
            debugger
            var hiddenID = (sender.valueOf('id')._id).replace("ACEtxtStorageItem", "hdfStorageItemID");
            var hiddenlbl = (sender.valueOf('id')._id).replace("ACEtxtStorageItem", "hdfStorageItemName");
            var txtmsg = (sender.valueOf('id')._id).replace("ACEtxtStorageItem", "txtStorageItem");
            document.getElementById(hiddenID).value = eventArgs.get_value();

            var autocomplete = (sender.valueOf('id')._id).replace("ACEtxtStorageItem", "ACEtxtScheme");
            var originID = (sender.valueOf('id')._id).replace("ACEtxtStorageItem", "txtOrigin_AutoCompleteExtender");
            var BrandID = (sender.valueOf('id')._id).replace("ACEtxtStorageItem", "txtBrand_AutoCompleteExtender");
            var VariteyID = (sender.valueOf('id')._id).replace("ACEtxtStorageItem", "txtVariety_AutoCompleteExtender");
            $find(autocomplete).set_contextKey(eventArgs.get_value() + ',' + $get('<%=txtTranshipmentDate.ClientID %>').value);
            $find(originID).set_contextKey(eventArgs.get_value());
            $find(BrandID).set_contextKey(eventArgs.get_value());
            $find(VariteyID).set_contextKey(eventArgs.get_value());
            if (eventArgs.get_value() == "No Records Found" || eventArgs.get_value() == "") {
                document.getElementById(hiddenlbl).value = "";
                document.getElementById(txtmsg).value = "";
            }
        }


        function GrdiSchemeSearch(sender, eventArgs) {
            var GridRowID = (sender.valueOf('id')._id).replace("ACEtxtScheme", "");
            var hdSchID = (sender.valueOf('id')._id).replace("ACEtxtScheme", "hdfSchemeID");
            var hdSchdetID = (sender.valueOf('id')._id).replace("ACEtxtScheme", "hdfStorageSchemeRate_ID");
            var hiddenlbl = (sender.valueOf('id')._id).replace("ACEtxtScheme", "UsrHideScheme");
            var txtmsg = (sender.valueOf('id')._id).replace("ACEtxtScheme", "txtScheme");
            var detail = eventArgs.get_value().split('~');
            document.getElementById(hdSchID).value = detail[0];
            document.getElementById(hdSchdetID).value = detail[1];
            if (eventArgs.get_value() == "No Records Found" || eventArgs.get_value() == "") {
                document.getElementById(hiddenlbl).value = "";
                document.getElementById(txtmsg).value = "";
            }
        }


        function OriginSearch(sender, eventArgs) {
            var GridRowID = (sender.valueOf('id')._id).replace("txtOrigin_AutoCompleteExtender", "");
            var hdfID = GridRowID + 'hdfOriginID';
            var Orignal = GridRowID + 'txtOrigin';
            var Hiden = GridRowID + 'HideOrigin';
            document.getElementById(hdfID).value = eventArgs.get_value();
            if (hdfID.value != "") {
                document.getElementById(Orignal).value = eventArgs.get_text();
                document.getElementById(Hiden).value = eventArgs.get_text();
            }
            else {

                document.getElementById(Orignal).value = "";
                document.getElementById(Hiden).value = "";
            }
        }


        function BrandSearch(sender, eventArgs) {
            var GridRowID = (sender.valueOf('id')._id).replace("txtBrand_AutoCompleteExtender", "");
            var hdfID = GridRowID + 'hdfBrandID';
            var Orignal = GridRowID + 'txtBrand';
            var Hiden = GridRowID + 'HideBrand';
            document.getElementById(hdfID).value = eventArgs.get_value();
            if (hdfID.value != "") {
                document.getElementById(Orignal).value = eventArgs.get_text();
                document.getElementById(Hiden).value = eventArgs.get_text();
            }
            else {

                document.getElementById(Orignal).value = "";
                document.getElementById(Hiden).value = "";
            }
        }


        function VarietySearch(sender, eventArgs) {
            var GridRowID = (sender.valueOf('id')._id).replace("txtVariety_AutoCompleteExtender", "");
            var hdfID = GridRowID + 'hdfVarietyID';
            var Orignal = GridRowID + 'txtVariety';
            var Hiden = GridRowID + 'HideVariety';
            document.getElementById(hdfID).value = eventArgs.get_value();
            if (hdfID.value != "") {
                document.getElementById(Orignal).value = eventArgs.get_text();
                document.getElementById(Hiden).value = eventArgs.get_text();
            }
            else {

                document.getElementById(Orignal).value = "";
                document.getElementById(Hiden).value = "";
            }
        }



    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <%--Header Section--%>
            <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <i class="fa fa-truck"></i>
                        <asp:Label ID="lblTranshipmentDetailTitle" Text="Transhipment Detail" runat="server"></asp:Label>
                        &nbsp;/&nbsp;
                         <asp:Label ID="lblTranshipmentNo" runat="server" ForeColor="Maroon" CssClass="bold"></asp:Label>
                        <asp:HiddenField ID="hdfTranshipmentDetailID" runat="server" />
                        <asp:HiddenField ID="hdfTranshipmentID" runat="server" />
                        <div class="pull-right">
                            <asp:LinkButton ID="btnDelete" runat="server" ForeColor="Maroon" CausesValidation="False" OnClick="btnDelete_Click" TabIndex="12" Visible="false">
                                  <i class="fa fa-trash"></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnBack" runat="server" CausesValidation="False" OnClick="btnBack_Click"
                                TabIndex="12" Text="">
                                  <i class="fa fa-search"></i>
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row btnrow">
                <div class="col-lg-6">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <asp:LinkButton ID="btnSave" runat="server" OnClick="btnSave_Click" TabIndex="11" Text=""
                                ValidationGroup="Trans" AccessKey="w"></asp:LinkButton><span></span>
                        </li>
                        <li>
                            <asp:LinkButton ID="btnNewDetail" runat="server" Text="" CausesValidation="false" TabIndex="13"
                                OnClick="btnNewDetail_Click" AccessKey="n" />
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <div class="pull-right">
                        <ul class="nav nav-pills nav-wizard">
                            <li class="active"><a href="#" data-toggle="tab">New</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Pending Approval</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Available</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Partially Available</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Done</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <hr />

            <%--Body Section--%>
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-1" style="display: none;">
                            <asp:Label ID="lblTranshipmentDetails" runat="server" Text="Transhipment Detail" CssClass="Header"></asp:Label>
                        </div>
                    </div>
                    <div class="newline"></div>
                    <div class="row">
                        <asp:HiddenField ID="HDID" runat="server" />
                        <div class="col-lg-2">
                            <asp:Label ID="lblGateRegFr" runat="server" Text="Gate Register From" CssClass="bold"></asp:Label>
                            <asp:Label ID="Label6" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:Label ID="lblVehicleNofr" runat="server" CssClass="bold"></asp:Label>
                            <asp:RequiredFieldValidator ID="Requgate" runat="server" ControlToValidate="txtGetRegFr"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Trans"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtGetRegFr" runat="server" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtGetRegFr" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch" ServiceMethod="GateEntrySearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtGetRegFr">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfGetRegFrID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdfGetRegFrName" runat="server" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblGateRegTo" runat="server" Text="Gate Register To" CssClass="bold"></asp:Label>
                            <asp:Label ID="lblgreTo" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:Label ID="lblVehicleNoTo" runat="server" CssClass="bold"></asp:Label>
                            <asp:CompareValidator ID="cmpgatecomp" runat="server" ErrorMessage="Plase Select valid Gate No."
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ControlToCompare="txtGetRegFr" ControlToValidate="txtGetRegTo"
                                Operator="NotEqual" ValidationGroup="Trans">
                            </asp:CompareValidator>
                            <asp:RequiredFieldValidator ID="Requgateto" runat="server" ControlToValidate="txtGetRegTo"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic" ValidationGroup="Trans"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtGetRegTo" runat="server" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtGetRegTo" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch" ServiceMethod="GateEntrySearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtGetRegTo">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfGetRegToID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdfGetRegToName" runat="server" />

                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPartnerGroupCode" runat="server" CssClass="bold" Text="Customer Group"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="UsrtxtPartnerGroupCode" onblur="ClearPartnerGroupSearch()" TabIndex="1" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                            </div>
                            <asp:AutoCompleteExtender ID="UsrtxtPartnerGroupCode_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerGroupSearch"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" ContextKey="4" UseContextKey="true"
                                TargetControlID="UsrtxtPartnerGroupCode">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:HiddenField ID="UsrhdfPartnerGroup" runat="server" />
                                <asp:Label ID="UsrlblPartnerGroup" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPartnerCode" CssClass="bold" runat="server" Text="Customer"></asp:Label>
                            <asp:Label ID="Label4" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="RFVFromdate1" runat="server" ControlToValidate="UsrtxtPartnerCode"
                                Display="Dynamic" ErrorMessage="" CssClass="text-danger" SetFocusOnError="true"
                                ValidationGroup="Trans">
                            </asp:RequiredFieldValidator>
                            <div class="input-group">
                                <asp:TextBox ID="UsrtxtPartnerCode" CssClass="form-control input-sm" TabIndex="1" runat="server" onblur="ClearPartner()"></asp:TextBox>
                                <asp:AutoCompleteExtender ID="UsrtxtPartnerCode_AutoCompleteExtender" runat="server"
                                    CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                    CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                    CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                    FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerSearch"
                                    ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                    UseContextKey="true" ContextKey="3" TargetControlID="UsrtxtPartnerCode">
                                </asp:AutoCompleteExtender>
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div>

                            <div style="display: none">
                                <asp:HiddenField ID="UsrhdfPartner" runat="server" />
                                <asp:TextBox ID="UsrlblPartner1" CssClass="form-control input-sm" runat="server" BorderWidth="0px" Width="200px"></asp:TextBox>
                                <asp:TextBox ID="txtDateCompare" CssClass="form-control input-sm" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblReceivedBy" runat="server" CssClass="bold" Text="Received By"></asp:Label>
                            <asp:TextBox ID="txtReceivedBy" runat="server" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtReceivedBy" runat="server"
                                CompletionInterval="1" CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListItemCssClass="AutoExtenderList"
                                CompletionSetCount="10" ContextKey="0" DelimiterCharacters="" EnableCaching="false"
                                Enabled="True" FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch"
                                ServiceMethod="CompanyUserSearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtReceivedBy">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfReceivedByID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdfReceivedByName" runat="server" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblReceivedDate" runat="server" Text="Received Date" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtReceivedDate" runat="server" MaxLength="50" ValidationGroup="Trans"
                                    CssClass="form-control input-sm">
                                </asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="txtReceivedDate_MaskedEditExtender" runat="server" CultureAMPMPlaceholder=""
                                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                                CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtReceivedDate">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="txtReceivedDate_CalendarExtender" runat="server" Enabled="True"
                                TargetControlID="txtReceivedDate">
                            </asp:CalendarExtender>
                            <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtReceivedDate"
                                ValidationGroup="Trans" Type="Date" MinimumValue="01/01/1900"
                                MaximumValue="01/01/2100" Display="Dynamic">
                            </asp:RangeValidator>
                        </div>
                    </div>
                    <div class="newline"></div>
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label ID="lblTranshipmentStartedOn" runat="server" Text="Unloading Started On" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtTranshipmentStartedOn" ValidationGroup="Trans" runat="server"
                                    CssClass="form-control input-sm">
                                </asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Mask="99/99/9999"
                                MaskType="Date" Enabled="True" TargetControlID="txtTranshipmentStartedOn">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CalendarExtender2" TargetControlID="txtTranshipmentStartedOn"
                                Format="dd/MM/yyyy" PopupButtonID="txtInwardStartedOn" runat="server">
                            </asp:CalendarExtender>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtTranshipmentStartedOn"
                                Type="Date" MinimumValue="01/01/1900" MaximumValue="01/01/2100"
                                ValidationGroup="Trans" Display="Dynamic">
                            </asp:RangeValidator>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblStartedOnHH" runat="server" Text="HH" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtStartedOnHH" ValidationGroup="Trans" MaxLength="2" runat="server"
                                CssClass="form-control input-sm">
                            </asp:TextBox>
                            <asp:MaskedEditExtender ID="metxtStartedOnHH" Mask="99" MaskType="Number" ClearMaskOnLostFocus="true"
                                TargetControlID="txtStartedOnHH" runat="server">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevtxtStartedOnHH" ControlToValidate="txtStartedOnHH"
                                ValidationGroup="Trans" ControlExtender="metxtStartedOnHH" MaximumValue="23"
                                Display="Dynamic" MinimumValue="0" InvalidValueMessage="Please Enter Valid Hour"
                                MaximumValueMessage="Please Enter Valid Hour" InvalidValueBlurredMessage="Please Enter Valid Hour"
                                runat="server" CssClass="text-danger">
                            </asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ValidationGroup="Trans" ID="rfvtxtStartedOnHH" runat="server"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ControlToValidate="txtStartedOnHH" Enabled="False">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblStartedOnMM" runat="server" Text="MM" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtStartedOnMM" ValidationGroup="Trans" runat="server"
                                CssClass="form-control input-sm" MaxLength="2">
                            </asp:TextBox>
                            <asp:MaskedEditExtender ID="metxtStartedOnMM" Mask="99" MaskType="Number" ClearMaskOnLostFocus="true"
                                TargetControlID="txtStartedOnMM" runat="server">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ValidationGroup="Trans" ID="mevtxtStartedOnMM" ControlToValidate="txtStartedOnMM"
                                ControlExtender="metxtStartedOnMM" MaximumValue="59" MinimumValue="0" InvalidValueMessage="Please Enter Valid Minutes"
                                MaximumValueMessage="Please Enter Valid Minutes" InvalidValueBlurredMessage="Please Enter Valid Minutes"
                                Display="Dynamic" runat="server" CssClass="text-danger">
                            </asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ValidationGroup="Trans" ID="rfvtxtStartedOnMM" runat="server"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ControlToValidate="txtStartedOnMM" Enabled="False">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblTranshipmentEndedOn" runat="server" Text="Unloading Ended On" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtTranshipmentEndedOn" ValidationGroup="Trans" runat="server" CssClass="form-control input-sm">
                                </asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99/99/9999"
                                MaskType="Date" Enabled="True" TargetControlID="txtTranshipmentEndedOn">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CalendarExtender3" TargetControlID="txtTranshipmentEndedOn"
                                Format="dd/MM/yyyy" PopupButtonID="txtInwardEndedOn" runat="server">
                            </asp:CalendarExtender>
                            <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtTranshipmentEndedOn"
                                ValidationGroup="Trans" Type="Date" MinimumValue="01/01/1900"
                                MaximumValue="01/01/2100" Display="Dynamic">
                            </asp:RangeValidator>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblEndedOnHH" runat="server" Text="HH" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtEndedOnHR" MaxLength="2" runat="server" CssClass="form-control input-sm"
                                ValidationGroup="Trans">
                            </asp:TextBox>
                            <asp:MaskedEditExtender ID="metxtEndedOnHR" Mask="99" MaskType="Number" ClearMaskOnLostFocus="true"
                                TargetControlID="txtEndedOnHR" runat="server" Enabled="True">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevtxtEndedOnHR" ControlToValidate="txtEndedOnHR" ValidationGroup="Trans"
                                ControlExtender="metxtEndedOnHR" MaximumValue="23" MinimumValue="0" InvalidValueMessage="Please Enter Valid Hour"
                                MaximumValueMessage="Please Enter Valid Hour" InvalidValueBlurredMessage="Please Enter Valid Hour"
                                Display="Dynamic" runat="server" CssClass="text-danger">
                            </asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ValidationGroup="Trans" ID="rfvmetxtEndedOnHR" runat="server"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ControlToValidate="txtEndedOnHR" Enabled="False">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblEndedonMM" runat="server" Text="MM" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtEndedOnMM" runat="server" CssClass="form-control input-sm"
                                MaxLength="2" ValidationGroup="Trans">
                            </asp:TextBox>
                            <asp:MaskedEditExtender ID="metxtEndedOnMM" Mask="99" MaskType="Number" ClearMaskOnLostFocus="true"
                                TargetControlID="txtEndedOnMM" runat="server" Enabled="True">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevtxtEndedOnMM" ControlToValidate="txtEndedOnMM" ValidationGroup="Trans"
                                ControlExtender="metxtEndedOnMM" MaximumValue="59" MinimumValue="0" InvalidValueMessage="Please Enter Valid Minutes"
                                MaximumValueMessage="Please Enter Valid Minutes" InvalidValueBlurredMessage="Please Enter Valid Minutes"
                                Display="Dynamic" runat="server" CssClass="text-danger">
                            </asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ID="rfvtxtEndedOnMM" runat="server" ValidationGroup="Trans"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ControlToValidate="txtEndedOnMM" Enabled="False">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="newline"></div>
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label ID="lblLStartedOn" runat="server" Text="Loading Started On" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtLStartedOn" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="MaskedEditExtender_L" runat="server" Mask="99/99/9999" MaskType="Date" Enabled="True" TargetControlID="txtLStartedOn">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CalendarExtender_L" TargetControlID="txtLStartedOn" Format="dd/MM/yyyy" PopupButtonID="txtLStartedOn" runat="server">
                            </asp:CalendarExtender>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblLStartedOnHH" runat="server" Text="HH" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtLStartedOnHH" runat="server" CssClass="form-control input-sm" MaxLength="2"></asp:TextBox>
                            <asp:MaskedEditExtender ID="meLtxtStartedOnHH" runat="server" ClearMaskOnLostFocus="true" Mask="99" MaskType="Number" TargetControlID="txtLStartedOnHH">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevLtxtStartedOnHH" SetFocusOnError="true" runat="server"
                                ControlExtender="meLtxtStartedOnHH" ControlToValidate="txtLStartedOnHH" CssClass="text-danger"
                                Enabled="false" InvalidValueBlurredMessage="Invalid Hour" InvalidValueMessage="Invalid Hour"
                                MaximumValue="23" MaximumValueMessage="Invalid Hour" MinimumValue="0"
                                ValidationGroup="Trans" Display="Dynamic"></asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ID="rfvLtxtStartedOnHH" runat="server" ControlToValidate="txtLStartedOnHH" CssClass="text-danger" Enabled="false" Display="Dynamic" SetFocusOnError="true" ValidationGroup="Trans"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblLStartedOnMM" runat="server" Text="MM" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtLStartedOnMM" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:MaskedEditExtender ID="meLtxtStartedOnMM" runat="server" ClearMaskOnLostFocus="true" Mask="99" MaskType="Number" TargetControlID="txtLStartedOnMM">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevLtxtStartedOnMM" SetFocusOnError="true" runat="server"
                                ControlExtender="meLtxtStartedOnMM" ControlToValidate="txtLStartedOnMM" CssClass="text-danger"
                                Enabled="false" InvalidValueBlurredMessage="Invalid Minutes" InvalidValueMessage="Invalid Minutes"
                                MaximumValue="59" MaximumValueMessage="Invalid Minutes" MinimumValue="0"
                                ValidationGroup="Trans" Display="Dynamic"></asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ID="rfvLtxtStartedOnMM" SetFocusOnError="true" runat="server" Display="Dynamic" ControlToValidate="txtLStartedOnMM" CssClass="text-danger" Enabled="false" ValidationGroup="Trans"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblLEndedOn" runat="server" Text="Loading Ended On" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtLEndedOn" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99/99/9999"
                                MaskType="Date" Enabled="True" TargetControlID="txtLEndedOn" AutoComplete="False"
                                ClearTextOnInvalid="True" UserDateFormat="DayMonthYear">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtLEndedOn" Format="dd/MM/yyyy" PopupButtonID="txtLEndedOn" runat="server">
                            </asp:CalendarExtender>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblLEndedOnHH" runat="server" Text="HH" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtLEndedOnHR" runat="server" CssClass="form-control input-sm" MaxLength="2"></asp:TextBox>
                            <asp:MaskedEditExtender ID="meLtxtEndedOnHR" runat="server" ClearMaskOnLostFocus="false" Enabled="false" Mask="99" MaskType="Number" TargetControlID="txtLEndedOnHR">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevLtxtEndedOnHR" runat="server" ControlExtender="meLtxtEndedOnHR"
                                ControlToValidate="txtLEndedOnHR" CssClass="text-danger" Enabled="false" InvalidValueBlurredMessage="Invalid Hour"
                                InvalidValueMessage="Invalid Hour" MaximumValue="23" MaximumValueMessage="Invalid Hour"
                                MinimumValue="0" ValidationGroup="Trans" Display="Dynamic"></asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ID="rfvLmetxtEndedOnHR" runat="server" ControlToValidate="txtLEndedOnHR" Display="Dynamic" SetFocusOnError="true" CssClass="text-danger" Enabled="False" ValidationGroup="Trans"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblLEndedonMM" runat="server" Text="MM" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtLEndedOnMM" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:MaskedEditExtender ID="meLtxtEndedOnMM" runat="server" ClearMaskOnLostFocus="false" Enabled="false" Mask="99" MaskType="Number" TargetControlID="txtLEndedOnMM">
                            </asp:MaskedEditExtender>
                            <asp:MaskedEditValidator ID="mevLtxtEndedOnMM" runat="server" SetFocusOnError="true"
                                ControlExtender="meLtxtEndedOnMM" ControlToValidate="txtLEndedOnMM" CssClass="text-danger"
                                Enabled="false" InvalidValueBlurredMessage="Invalid Minutes" InvalidValueMessage="Invalid Minutes"
                                MaximumValue="59" MaximumValueMessage="Invalid Minutes" MinimumValue="0"
                                ValidationGroup="Trans" Display="Dynamic"></asp:MaskedEditValidator>
                            <asp:RequiredFieldValidator ID="rfvLtxtEndedOnMM" runat="server" SetFocusOnError="true" ControlToValidate="txtLEndedOnMM" CssClass="text-danger" Enabled="False" Display="Dynamic" ValidationGroup="Trans"></asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblTranshipmentDate" runat="server" CssClass="bold" Text="Transhipment Date"></asp:Label>
                            <asp:Label ID="Label19" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtTranshipmentDate" runat="server" MaxLength="50" ValidationGroup="Trans" CssClass="form-control input-sm"> </asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="txtTranshipmentDate_MaskedEditExtender" runat="server"
                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtTranshipmentDate">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="txtTranshipmentDate_CalendarExtender" runat="server" Enabled="True"
                                TargetControlID="txtTranshipmentDate">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="txtTranshipmentDate"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ValidationGroup="Trans">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-12">
                            <asp:Label ID="lblRemarks" runat="server" CssClass="bold" Text="Remarks"></asp:Label>
                            <asp:TextBox ID="txtRemarks" runat="server" MaxLength="500" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="newline"></div>
            <div class="row">
                <div class="col-lg-12">
                    <asp:GridView ID="grdItems" runat="server" OnRowDataBound="grdItems_RowDataBound" OnRowCommand="grdItems_RowCommand" AutoGenerateColumns="False" OnPreRender="grdItems_PreRender"
                        CssClass="table table-striped table-hover text-nowrap">
                        <Columns>
                            <asp:TemplateField HeaderText="Item">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtStorageItem" runat="server" Text='<%# Eval("Item.Name") %>' CssClass="form-control input-sm" TabIndex="3"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="ACEtxtStorageItem" runat="server" CompletionInterval="1"
                                        CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                        CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionSetCount="10"
                                        DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                        MinimumPrefixLength="1" OnClientItemSelected="GrditemSearch" ServiceMethod="ItemSearch"
                                        ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                        ContextKey="0" UseContextKey="true" TargetControlID="txtStorageItem">
                                    </asp:AutoCompleteExtender>
                                    <asp:HiddenField ID="hdfStorageItemID" runat="server" Value='<%# Eval("Item_ID")%>' />
                                    <asp:HiddenField ID="hdfStorageItemName" runat="server" Value='<%# Eval("Item.Name") %>'></asp:HiddenField>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtStorageItem"
                                        CssClass="text-danger" Display="Dynamic" SetFocusOnError="true"
                                        ValidationGroup="Trans"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Scheme">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtScheme" runat="server" Text='<%# Eval("StorageSchemeRate.StorageScheme.Name") %>' CssClass="form-control input-sm" TabIndex="3"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="ACEtxtScheme" runat="server" CompletionInterval="1"
                                        CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList"
                                        CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionSetCount="10"
                                        DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                        MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch" ServiceMethod="SchemeSearch"
                                        ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                        ContextKey="0" UseContextKey="true" TargetControlID="txtScheme">
                                    </asp:AutoCompleteExtender>
                                    <div style="display: none">
                                        <asp:HiddenField ID="hdfSchemeID" runat="server" Value='<%# Eval("StorageSchemeRate.StorageScheme.ID") %>' />
                                        <asp:HiddenField ID="hdfStorageSchemeRate_ID" runat="server" Value='<%# Eval("StorageSchemeRate_ID") %>' />
                                        <asp:TextBox ID="UsrHideScheme" runat="server" Text='<%# Eval("StorageSchemeRate.StorageScheme.Name") %>' CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ControlToValidate="txtScheme"
                                        CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                        ValidationGroup="Trans"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Mark">
                                <ItemTemplate>
                                    <asp:TextBox ID="ItemMark" MaxLength="50" runat="server" Text='<%# Eval("ItemMark") %>'
                                        TabIndex="3" ToolTip='<%# Eval("ItemMark") %>' CssClass="form-control input-sm"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pack Ref.">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtPackingReference" runat="server" Text='<%#Eval("PackingReference") %>'
                                        ToolTip='<%#Eval("PackingReference")%>' TabIndex="3" CssClass="form-control input-sm"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty">
                                <ItemTemplate>
                                    <asp:TextBox ID="Quantity" runat="server" Text='<%#Eval("Quantity") %>'
                                        TabIndex="3" ToolTip='<%#Eval("Quantity") %>' MaxLength="5" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="StorageItemIndQuantityTaken_FilteredTextBoxExtender"
                                        runat="server" Enabled="True" FilterType="Numbers,Custom" ValidChars="." TargetControlID="Quantity">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="Quantity"
                                        CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                        ValidationGroup="Trans"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lot No.">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblLotNoInternal" runat="server" MaxLength="20" Text='<%# Eval("LotInternal") %>'
                                        TabIndex="3" ToolTip='<%# Eval("LotInternal") %>' CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqlot" runat="server" ControlToValidate="lblLotNoInternal"
                                        CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                        ValidationGroup="Trans"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vakkal">
                                <ItemTemplate>
                                    <asp:TextBox ID="lblLotNoCustomer" Width="100px" MaxLength="20" Text='<%# Eval("LotCustomer") %>'
                                        TabIndex="3" ToolTip='<%# Eval("LotCustomer") %>' runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdOrigin" Text="Origin" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtOrigin" runat="server" CssClass="form-control" Text='<%# Eval("Origin.Name") %>' TabIndex="3"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="txtOrigin_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                        CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                        DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                        MinimumPrefixLength="1" OnClientItemSelected="OriginSearch" ServiceMethod="OriginSearch"
                                        ServicePath="~/Services/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                        TargetControlID="txtOrigin">
                                    </asp:AutoCompleteExtender>
                                    <div style="display: none">
                                        <asp:HiddenField ID="hdfOriginID" Value='<%# Eval("Origin_ID") %>' runat="server" />
                                        <asp:TextBox ID="HideOrigin" Text='<%# Eval("Origin.Name") %>' runat="server"></asp:TextBox>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdBrand" Text="Brand" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtBrand" runat="server" CssClass="form-control" Text='<%# Eval("Brand.Name") %>' TabIndex="3"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="txtBrand_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                        CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                        DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                        MinimumPrefixLength="1" OnClientItemSelected="BrandSearch" ServiceMethod="BrandSearch"
                                        ServicePath="~/Services/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                        TargetControlID="txtBrand">
                                    </asp:AutoCompleteExtender>
                                    <div style="display: none">
                                        <asp:HiddenField ID="hdfBrandID" Value='<%# Eval("Brand_ID") %>' runat="server" />
                                        <asp:TextBox ID="HideBrand" Text='<%# Eval("Brand.Name") %>' runat="server"></asp:TextBox>
                                    </div>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdVariety" Text="Variety" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtVariety" runat="server" Text='<%# Eval("Variety.Name") %>' CssClass="form-control" TabIndex="3"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="txtVariety_AutoCompleteExtender" runat="server" CompletionInterval="1"
                                        CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                        DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                        MinimumPrefixLength="1" OnClientItemSelected="VarietySearch" ServiceMethod="VarietySearch"
                                        ServicePath="~/Services/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                        TargetControlID="txtVariety">
                                    </asp:AutoCompleteExtender>
                                    <div style="display: none">
                                        <asp:HiddenField ID="hdfVarietyID" Value='<%# Eval("Variety_ID") %>' runat="server" />
                                        <asp:TextBox ID="HideVariety" Text='<%# Eval("Variety.Name") %>' runat="server"></asp:TextBox>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:LinkButton ID="btnAddRow" runat="server" OnClick="btnAddRow_Click" ToolTip="Add Row" CausesValidation="false" Text="Add Row" CssClass="btn btn-xs btn-primary" AccessKey="w"></asp:LinkButton><span></span>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDeleteRow" ForeColor="Maroon" runat="server" CommandName="Del" CommandArgument='<%#Eval("ID")%>' ToolTip="Delete Row" Visible='<%#Convert.ToString(Eval("ID"))=="0"?false:true%>' CausesValidation="false"> <i class="fa fa-close fa-2x"></i></asp:LinkButton><span></span>
                                    <asp:HiddenField ID="hdfTranshipmentID" runat="server" Value='<%#Eval("ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

