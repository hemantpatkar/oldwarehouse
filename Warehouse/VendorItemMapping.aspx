﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="VendorItemMapping" Codebehind="VendorItemMapping.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function AutoCompleteSearchInGrid(sender, eventArgs) {           
            var id = sender._id;
            var autocompleteId = id.split('_');
            var replacetxt = autocompleteId[3].replace('txt', '');
            var txt = $get(autocompleteId[0] + '_' + autocompleteId[1] + '_txt' + replacetxt);
            var txtName = $get(autocompleteId[0] + '_' + autocompleteId[1] + '_hdf' + replacetxt + 'Name');
            var txtID = $get(autocompleteId[0] + '_' + autocompleteId[1] + '_hdf' + replacetxt + 'ID');
            txt.value = eventArgs.get_text();
            txtName.value = eventArgs.get_text();
            txtID.value = eventArgs.get_value();
        }
        function ClearAutocompleteTextBoxInGrid(sender) {
            debugger;
            var id = sender.id;
            var autocompleteId = id.split('_');
            var replacetxt = autocompleteId[2].replace('txt', '');
            var txt = $get(autocompleteId[0] + '_' + autocompleteId[1] + '_txt' + replacetxt);
            var txtName = $get(autocompleteId[0] + '_' + autocompleteId[1] + '_hdf' + replacetxt + 'Name');
            var txtID = $get(autocompleteId[0] + '_' + autocompleteId[1] + '_hdf' + replacetxt + 'ID');
            if (txt.value == "No Records Found" || txt.value != txtName.value) {
                txt.value = "";
                txtName.value = "";
                txtID.value = "";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <h3>Item Mapping</h3>
            <hr />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdItemMapping" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found" OnRowDataBound="grdItemMapping_RowDataBound"
                        OnPreRender="grdItemMapping_PreRender" OnRowCommand="grdItemMapping_RowCommand" OnRowEditing="grdItemMapping_RowEditing" OnRowUpdating="grdItemMapping_RowUpdating"
                        OnRowCancelingEdit="grdItemMapping_RowCancelingEdit"
                        OnRowDeleting="grdItemMapping_RowDeleting"
                        CssClass="table table-striped  dt-responsive table-hover text-nowrap">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="Insert" OnClick="lbtnAddNew_Click"
                                        Text="Add New" ToolTip="Add New" />
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="EditItem"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" CssClass="fa fa-ban fa-2x" ToolTip="Cancel"
                                        Text="" />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CssClass="fa fa-edit fa-2x fa-color-Edit " CommandName="Edit"
                                        Text="" ToolTip="Edit" />
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete " CommandName="Delete" Text="" ToolTip="Delete" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x fa-color-Add" ToolTip="Save"
                                        Text="" ValidationGroup="AddItem" UseSubmitBehavior="False" />
                                    <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban  fa-2x" Text="" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblItem" Text="Item Name" runat="server" CssClass="bold" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblItemType" runat="server" Text='<%# Eval("Item.Name") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtItemType" runat="server" Text='<%# Eval("Item.Name") %>' MaxLength="500" TabIndex="2" ValidationGroup="EditItem" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBoxInGrid(this)"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdfItemTypeName" Value='<%# Eval("Item.Name").ToString() %>' />
                                    <asp:HiddenField runat="server" ID="hdfItemTypeID" Value='<%# Eval("Item_ID") %>' />
                                    <asp:AutoCompleteExtender ID="Ace_txtItemType" runat="server"
                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="ItemSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearchInGrid"
                                        ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtItemType">
                                    </asp:AutoCompleteExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtItemType" CssClass="text-danger"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="EditItem" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewItemType" runat="server" TabIndex="2" onblur="return ClearAutocompleteTextBoxInGrid(this)" PlaceHolder="Select Item Type" AutoPostBack="false" AutoComplete="Off" AutoCompleteType="None" MaxLength="50" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdfNewItemTypeName" />
                                    <asp:HiddenField runat="server" ID="hdfNewItemTypeID" Value="0" />
                                    <asp:AutoCompleteExtender ID="Ace_txtNewItemType" runat="server"
                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="ItemSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearchInGrid"
                                        ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtNewItemType">
                                    </asp:AutoCompleteExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtNewItemType" CssClass="text-danger" ToolTip="Required Field"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="AddItem" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblUOM" Text="Unit Of Measurement" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblUnitOfMeasu" runat="server" Text='<%#Eval("UOMType.Name") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlUOM" runat="server" TabIndex="8" CssClass="form-control input-sm">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RFV_ddlUOM" runat="server" ControlToValidate="ddlUOM"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditItem" InitialValue="0"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlNewUOM" runat="server" TabIndex="8" CssClass="form-control input-sm">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlNewUOM" CssClass="text-danger" ToolTip="Required Field"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="AddItem" SetFocusOnError="true" InitialValue="0"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblTaxType" Text="Tax Type" runat="server" CssClass="" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblTax" runat="server" TabIndex="2" Text='<%#Eval("TaxType.Name") %>' AutoComplete="Off" CssClass=""></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtTaxType" runat="server" TabIndex="4" Text='<%#Eval("TaxType.Name") %>' AutoPostBack="false" ValidationGroup="EditItem" AutoComplete="Off" AutoCompleteType="None" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdfTaxTypeName" Value='<%#Eval("TaxType.Name") %>' />
                                    <asp:HiddenField runat="server" ID="hdfTaxTypeID" Value='<%#Eval("TaxType_ID") %>' />
                                    <asp:AutoCompleteExtender ID="Ace_txtTaxType" runat="server"
                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TaxTypeSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearchInGrid"
                                        ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtTaxType">
                                    </asp:AutoCompleteExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtTaxType" CssClass="text-danger" ToolTip="Required Field"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="EditItem" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewTaxType" runat="server" onblur="return ClearAutocompleteTextBoxInGrid(this)" PlaceHolder="Select Tax Type" AutoComplete="Off" TabIndex="4" ValidationGroup="AddItem" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdfNewTaxTypeName" />
                                    <asp:HiddenField runat="server" ID="hdfNewTaxTypeID" Value="" />
                                    <asp:AutoCompleteExtender ID="Ace_txtNewTaxType" runat="server"
                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TaxTypeSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearchInGrid"
                                        ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtNewTaxType">
                                    </asp:AutoCompleteExtender>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNewTaxType" CssClass="text-danger" ToolTip="Required Field"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="AddItem" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrSupplyRate" Text="Supply Rate" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblItSupplyRate" runat="server" TabIndex="2" Text='<%#Eval("Price").ToString()=="0"?" ":Eval("Price").ToString() %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtSupplyRate" runat="server" TabIndex="5" Text='<%#Eval("Price").ToString() %>' AutoComplete="Off" CssClass="form-control input-sm" onkeypress="return validateFloatKeyPress(this, event)" ValidationGroup="EditItem"></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewSupplyRate" TabIndex="5" runat="server" AutoComplete="Off" CssClass="form-control input-sm" onkeypress="return validateFloatKeyPress(this, event)" ValidationGroup="AddItem"></asp:TextBox>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrPrice" Text="MRP" runat="server" CssClass="bold" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblItPrice" runat="server" Text='<%#Eval("MRP").ToString()=="0"?" ":Eval("MRP").ToString() %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtProductPrice" TabIndex="6" runat="server" onkeypress="return validateFloatKeyPress(this, event)" Text='<%#Eval("MRP").ToString() %>' CssClass="form-control input-sm"></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewProductPrice" TabIndex="6" runat="server" onkeypress="return validateFloatKeyPress(this, event)" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrMOQ" Text="Min Order Qty" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblItMinOrderQty" runat="server" Text='<%#Eval("MinmumOrderQuantity").ToString()=="0"?" ":Eval("MinmumOrderQuantity").ToString() %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtMinOrderQty" TabIndex="7" runat="server" Text='<%#Eval("MinmumOrderQuantity") %>' onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm" ValidationGroup="EditItem" AutoPostBack="false"></asp:TextBox>
                                    <%--<asp:FilteredTextBoxExtender ID="FTB_txtMinOrderQty" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtMinOrderQty">
                                    </asp:FilteredTextBoxExtender>--%>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewMinOrderQty" TabIndex="7" runat="server" onkeypress="return validateFloatKeyPress(this, event)" MaxLength="10" AutoComplete="Off" AutoPostBack="false" CssClass="form-control input-sm" ValidationGroup="AddItem"></asp:TextBox>
                                    <%--<asp:FilteredTextBoxExtender ID="FTB_txtNewMinOrderQty" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtNewMinOrderQty">
                                    </asp:FilteredTextBoxExtender>--%>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrLeadDays" Text="Supply Lead Days" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblItLeadDays" runat="server" Text='<%#Eval("SupplyLeadDays").ToString()=="0"?" ":Eval("SupplyLeadDays").ToString() %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtLeadDays" TabIndex="7" onkeypress="return checkIfNumber(event);" runat="server" Text='<%#Eval("SupplyLeadDays") %>' CssClass="form-control input-sm" ValidationGroup="EditItem"></asp:TextBox>
                                    <%--<asp:FilteredTextBoxExtender ID="FTB_txtLeadDays" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtLeadDays">
                                    </asp:FilteredTextBoxExtender>--%>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewLeadDays" TabIndex="7" runat="server" MaxLength="10" onkeypress="return checkIfNumber(event);" AutoComplete="Off" AutoPostBack="false" CssClass="form-control input-sm" ValidationGroup="AddItem"></asp:TextBox>
                                    <%-- <asp:FilteredTextBoxExtender ID="FTB_txtNewLeadDays" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtNewLeadDays">
                                    </asp:FilteredTextBoxExtender>--%>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrDispatchAdd" Text="Dispatch Address" runat="server" CssClass="bold" ></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblItDispatchAdd" runat="server" Text='<%#Eval("CompanyAddress.Address1") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDispatchAdd" runat="server" TabIndex="8" CssClass="form-control">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="ddlDispatchAdd" CssClass="text-danger" ToolTip="Required Field"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="EditItem" SetFocusOnError="true" InitialValue="0"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlNewDispatchAdd" runat="server" TabIndex="8" CssClass="form-control">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlNewDispatchAdd" CssClass="text-danger" ToolTip="Required Field"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="AddItem" SetFocusOnError="true" InitialValue="0"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrCurrency" Text="Currency" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblItCurrency" runat="server" Text='<%#Eval("CurrencyType.Name") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlCurrency" runat="server" TabIndex="8" CssClass="form-control">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlNewCurrency" runat="server" TabIndex="8" CssClass="form-control">
                                    </asp:DropDownList>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrRateVar" Text="Rate Variance" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblItRateVar" runat="server" Text='<%#Eval("PriceVariance").ToString()=="0"?" ":Eval("PriceVariance") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtRateVariance" TabIndex="7" runat="server" onkeypress="return validateFloatKeyPress(this, event)" Text='<%#Eval("PriceVariance") %>' CssClass="form-control input-sm" ValidationGroup="EditItem"></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewRateVariance" TabIndex="7" runat="server" MaxLength="10" onkeypress="return validateFloatKeyPress(this, event)" AutoComplete="Off" AutoPostBack="false" CssClass="form-control input-sm" ValidationGroup="AddItem"></asp:TextBox>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrQtyVar" Text="Quantity Variance" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblItQtyVar" runat="server" Text='<%#Eval("QtyVariance").ToString()=="0"?" ":Eval("QtyVariance") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtQtyVariance" TabIndex="7" runat="server" Text='<%#Eval("QtyVariance") %>' CssClass="form-control input-sm" ValidationGroup="EditItem" onkeypress="return validateFloatKeyPress(this, event)"></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewQtyVariance" TabIndex="7" runat="server" MaxLength="10" AutoComplete="Off" AutoPostBack="false" CssClass="form-control input-sm" ValidationGroup="AddItem" onkeypress="return validateFloatKeyPress(this, event)"></asp:TextBox>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" runat="server" Text='<%#Eval("Status").ToString()=="1"?"Y":"N" %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkActive" TabIndex="9" CssClass="bold" Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsVendorActive" TabIndex="9" CssClass=" bold" Checked="true" />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdItemMapping" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdItemMapping.ClientID%>');
        $(document).ready(function () {
            //GridUI(GridID, 50);
        })
    </script>
</asp:Content>