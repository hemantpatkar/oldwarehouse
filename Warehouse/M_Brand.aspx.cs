﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppUtility;
using AppObjects;
public partial class M_Brand : BigSunPage
{
    AppBrandColl obj;
    protected void Page_Load(object sender, EventArgs e)
    {
  
        obj = new AppObjects.AppBrandColl(intRequestingUserID);
        Session[TableConstants.SessionBrand] = obj;
        if (!IsPostBack)
        {
           BindData();
        }
    }
    #region GridViewEvents
   
    protected void grdBrand_PreRender(object sender, EventArgs e)
    {
        if (grdBrand.Rows.Count > 0)
        {
            grdBrand.UseAccessibleHeader = true;
            grdBrand.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdBrand_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if (strCommandName == "INSERT")
        {       
            HiddenField hdfBrandID = grdBrand.FooterRow.FindControl("hdfBrandID") as HiddenField;
            TextBox txtCode = grdBrand.FooterRow.FindControl("txtNewCode") as TextBox;
            TextBox txtName = grdBrand.FooterRow.FindControl("txtNewName") as TextBox;
            CheckBox chkActiveBrand = grdBrand.FooterRow.FindControl("chkActiveBrand") as CheckBox;
            CheckBox chkDefaultBrand = grdBrand.FooterRow.FindControl("chkDefaultBrand") as CheckBox;
            SaveBrandDetails(strCommandID, txtCode.Text, txtName.Text, chkActiveBrand.Checked, chkDefaultBrand.Checked);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Successfully Added','1'" + ");", true);
            grdBrand.EditIndex = -1;
            BindData();
        }
        else if (strCommandName == "CANCELADD")
        {
            grdBrand.ShowFooter = false;
            BindData();
        }
        else if (strCommandName == "CANCELEDIT")
        {
            grdBrand.EditIndex = -1;
            BindData();
        }
        else if(strCommandName=="ADDNEW")
        {
            grdBrand.EditIndex = -1;
            grdBrand.ShowFooter = true;
            BindData();
        }
    }
    protected void grdBrand_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdBrand.ShowFooter = false;
        grdBrand.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdBrand_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HiddenField hdfID = grdBrand.Rows[e.RowIndex].FindControl("hdfID") as HiddenField;
        AppBrand SingleObj = new AppBrand(intRequestingUserID);
        SingleObj.ID = new AppConvert(hdfID.Value);
        SingleObj.Status = new AppConvert((int)RecordStatus.Deleted);
        SingleObj.Save();
     
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void grdBrand_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strCommandID = new AppConvert(e.RowIndex);
        HiddenField hdfBrandID = grdBrand.Rows[e.RowIndex].FindControl("hdfBrandID") as HiddenField;
        TextBox txtCode = grdBrand.Rows[e.RowIndex].FindControl("txtCode") as TextBox;
        TextBox txtName = grdBrand.Rows[e.RowIndex].FindControl("txtName") as TextBox;
        CheckBox chkIsActiveBrand = grdBrand.Rows[e.RowIndex].FindControl("chkIsActiveBrand") as CheckBox;
        CheckBox chkIsDefaultBrand = grdBrand.Rows[e.RowIndex].FindControl("chkIsDefaultBrand") as CheckBox;

        SaveBrandDetails(hdfBrandID.Value, txtCode.Text, txtName.Text , chkIsActiveBrand.Checked, chkIsDefaultBrand.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Successfully Updated','1'" + ");", true);
        grdBrand.EditIndex = -1;
        BindData();
    }
    #endregion
    #region MiscFunctions
    public void SaveBrandDetails(string Id, string Code, string Name, bool IsActive, bool IsDefault)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppBrand SingleObj = new AppBrand(intRequestingUserID);      
        SingleObj.ID = new AppConvert(Id);
        SingleObj.Code = new AppConvert(Code);
        SingleObj.Name = new AppConvert(Name);
        SingleObj.Status = (IsActive == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));        
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;

        Session.Add(TableConstants.SessionBrand, obj);
        SingleObj.Save();
        BindData();
    }
    

    protected void BindData()
    {
        AppObjects.AppBrandColl objColl = new AppBrandColl(intRequestingUserID);
        if (!string.IsNullOrEmpty(txtName.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Brand.Name, AppUtility.Operators.Equals, txtName.Text, 0);
        }
        if (!string.IsNullOrEmpty(txtCode.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Brand.Code, AppUtility.Operators.Equals, txtCode.Text, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
             
            objColl.AddCriteria(AppObjects.Brand.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.SessionBrand] = objColl;
        if (objColl != null)
        {
            grdBrand.DataSource = objColl;
            grdBrand.DataBind();
            if (objColl.Count <= 0)
            {
                AddEmptyRow();
            }
        }
       
       
    }
    public void AddEmptyRow()
    {
        AppBrandColl collOfContacts = new AppBrandColl(intRequestingUserID);
        AppBrand newContact = new AppBrand(intRequestingUserID);
        newContact.Code = "Add atleast one Brand";
        collOfContacts.Add(newContact);
        grdBrand.DataSource = collOfContacts;
        grdBrand.DataBind();
        grdBrand.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdBrand.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdBrand.ShowFooter = true;
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}