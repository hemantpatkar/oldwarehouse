﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="CompanyAddressDetails" Codebehind="CompanyAddressDetails.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div class="row">
        <div class="col-lg-12">
            <h3>Address Details</h3>
            <hr />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdCompanyAddress" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found" 
                        OnPreRender="grdCompanyAddress_PreRender" CssClass="table table-striped  table-hover text-nowrap"
                        OnRowCancelingEdit="grdCompanyAddress_RowCancelingEdit" OnRowCommand="grdCompanyAddress_RowCommand"
                        OnRowEditing="grdCompanyAddress_RowEditing" OnRowUpdating="grdCompanyAddress_RowUpdating" OnRowDeleting="grdCompanyAddress_RowDeleting">
                        <Columns>                                                    
                            <asp:TemplateField>                                
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="Insert" OnClick="lbtnAddNew_Click" CommandArgument=""
                                        Text="Add New" ToolTip="Add New" />
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="EditAddress"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" CssClass="fa fa-ban fa-2x" ToolTip="Cancel"
                                        Text="" />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CssClass="fa fa-edit fa-2x fa-color-Edit " CommandName="Edit"
                                        Text="" ToolTip="Edit" />
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete " CommandName="Delete" Text="" ToolTip="Delete" />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x fa-color-Add" ToolTip="Save"
                                        Text="" ValidationGroup="AddAddress" UseSubmitBehavior="False" />
                                    <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban  fa-2x" Text="" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblAddress" Text="Address" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="txtAddress" runat="server" Text='<%# Eval("Address1") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtAddress" runat="server" Text='<%# Eval("Address1") %>' MaxLength="500" TabIndex="2" ValidationGroup="EditAddress" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtAddress" runat="server" ControlToValidate="txtAddress"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ValidationGroup="EditAddress"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewAddress" runat="server" TabIndex="2" AutoComplete="Off" MaxLength="500" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtNewAddress" runat="server" ControlToValidate="txtNewAddress"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ValidationGroup="AddAddress"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblArea" Text="Area" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblAreaText" runat="server" Text='<%#Eval("Address2") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtVendorArea" runat="server" TabIndex="3" Text='<%#Eval("Address2") %>' MaxLength="100" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtVendorArea1" runat="server" ControlToValidate="txtVendorArea"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditAddress"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewVendorArea" runat="server" AutoComplete="Off" MaxLength="100" TabIndex="3" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtVendorArea" runat="server" ControlToValidate="txtNewVendorArea"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddAddress"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblCity" Text="City" runat="server" CssClass=""></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCityText" runat="server" TabIndex="2" Text='<%#Eval("CountryType.City") %>' AutoComplete="Off" CssClass=""></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtCity" runat="server" disabled="disabled" TabIndex="4" Text='<%#Eval("CountryType.City") %>' ValidationGroup="EditAddress" AutoComplete="Off" AutoCompleteType="None" CssClass="form-control input-sm"></asp:TextBox>                                   
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewCity" runat="server" disabled="disabled" AutoComplete="Off" TabIndex="4" ValidationGroup="AddAddress" CssClass="form-control input-sm"></asp:TextBox>                                   
                                     <asp:RequiredFieldValidator ID="RFV_txtNewCity" runat="server" ControlToValidate="txtNewCity"
                                            Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddAddress">                            
                                        </asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblState" Text="State" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblStateText" runat="server" TabIndex="2" Text='<%#Eval("CountryType.State") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtState" runat="server" disabled="disabled" TabIndex="5" Text='<%#Eval("CountryType.State") %>' AutoComplete="Off" CssClass="form-control input-sm" ValidationGroup="EditAddress"></asp:TextBox>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewState" TabIndex="5" disabled="disabled" runat="server" AutoComplete="Off" CssClass="form-control input-sm" ValidationGroup="AddAdress"></asp:TextBox>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblCountry" Text="Country" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCountryText" runat="server" Text='<%#Eval("CountryType.Country") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                  <asp:TextBox ID="txtCountry" TabIndex="6" disabled="disabled" runat="server" Text='<%#Eval("CountryType.Country").ToString() %>' CssClass="form-control input-sm" ValidationGroup="EditAddress"></asp:TextBox>
                                  <asp:HiddenField ID="hdCountryID" runat="server" Value='<%#Eval("CountryType_ID").ToString() %>' />                                      
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewCountry" TabIndex="6" disabled="disabled" runat="server" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdfNewCountryId" Value="" />                                    
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPincode" Text="Pin Code" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPincodeText" runat="server" Text='<%#Eval("ZipCodeType.ZipCode").ToString() %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtVendorPinCode" TabIndex="7" runat="server" Text='<%#Eval("ZipCodeType.ZipCode") %>' OnTextChanged="txtVendorPinCode_TextChanged" AutoPostBack="true"
                                         CssClass="form-control input-sm" ValidationGroup="EditAddress"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtVendorPinCode1" runat="server" ControlToValidate="txtVendorPinCode"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditAddress"></asp:RequiredFieldValidator>
                                    <asp:FilteredTextBoxExtender ID="FTB_txtVendorPinCode" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtVendorPinCode">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:HiddenField runat="server" ID="hdfVendorPincodeID" Value='<%#Eval("ZipCodeType_ID").ToString() %>'    />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewVendorPinCode" TabIndex="7" runat="server" MaxLength="10" AutoComplete="Off" AutoPostBack="true" OnTextChanged="txtNewVendorPinCode_TextChanged" CssClass="form-control input-sm" ValidationGroup="AddAddress"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtVendorPinCode" runat="server" ControlToValidate="txtNewVendorPinCode"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddAddress"></asp:RequiredFieldValidator>
                                    <asp:FilteredTextBoxExtender ID="FTB_txtNewVendorPinCode" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtNewVendorPinCode">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:HiddenField runat="server" ID="hdfNewZipCodeID" Value="" />                                    
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" runat="server" Text='<%#Eval("Status").ToString()=="1"?"Y":"N" %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkActive" TabIndex="9" CssClass="bold" Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsVendorActive" TabIndex="9" CssClass=" bold" Checked="true" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblDefault" Text="Default" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDefaultText" runat="server" Text='<%#Eval("Type").ToString()=="1"?"Y":"N" %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" TabIndex="10"  ID="chkDefault" CssClass="bold" Checked='<%#Eval("Type").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" TabIndex="10" ID="chkIsVendorDefault" CssClass="bold"  />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <footer>
                             <asp:Label ID = "lblEmptyMessage" Text="" runat="server" />
                            </footer>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdCompanyAddress" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdCompanyAddress.ClientID%>');
        $(document).ready(function () {
            debugger;
          //GridUI(GridID, 50);
        })
    </script>
</asp:Content>
