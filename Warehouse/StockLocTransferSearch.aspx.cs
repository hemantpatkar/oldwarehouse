﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using System.IO;
using System.Drawing;

public partial class StockLocTransferSearch : BigSunPage
{
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Location Transfer Search";
        if (!IsPostBack)
        {
        }
    }
    #endregion

    #region Button Event
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockLocTransfer.aspx");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Search();
    }

    protected void Search()
    {

        AppTransferStockLocationDetailColl objAppTransferStockLocationDetailColl = new AppTransferStockLocationDetailColl(intRequestingUserID);

        DateTime fromdate;
        if (txtTransferDate.Text.Trim() != "" && txtTransferDate.Text.Trim() != "__/__/____")
        {
            fromdate = Convert.ToDateTime(txtTransferDate.Text);
            objAppTransferStockLocationDetailColl.AddCriteria(TransferStockLocationDetail.TransferDate, Operators.GreaterOrEqualTo, fromdate);
        }
        if (txtCustomer.Text.Trim() != "")
        {
            objAppTransferStockLocationDetailColl.AddCriteria(TransferStockLocationDetail.Company_ID, Operators.StartWith, txtCustomer.Text);
        }
        if (txtMovedQuantity.Text.Trim() != "")
        {
            objAppTransferStockLocationDetailColl.AddCriteria(TransferStockLocationDetail.MovedQuantity, Operators.Like, txtMovedQuantity.Text);
        }
        if (txtItemName.Text.Trim() != "")
        {
            objAppTransferStockLocationDetailColl.AddCriteria(TransferStockLocationDetail.Item_ID, Operators.Like, txtItemName.Text);
        }
        if (txtLotNo.Text.Trim() != "")
        {
            objAppTransferStockLocationDetailColl.AddCriteria(TransferStockLocationDetail.LotInternal, Operators.Equals, txtLotNo.Text);
        }
        if (ddlStatus.SelectedValue != "-1")
        {
            int status = Convert.ToInt32(ddlStatus.SelectedValue);
            objAppTransferStockLocationDetailColl.AddCriteria(TransferStockLocationDetail.Status, Operators.Equals, status); //
        }

        objAppTransferStockLocationDetailColl.Search();
        //objAppGateEntryColl.Sort(AppPreInward.ComparisionDesc);
        grdStockLocTransferSearch.DataSource = objAppTransferStockLocationDetailColl;
        grdStockLocTransferSearch.DataBind();
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (grdStockLocTransferSearch.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Doesnot contain data" + "','2'" + ");", true);
            return;
        }

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grdStockLocTransferSearch.AllowPaging = false;
            grdStockLocTransferSearch.HeaderRow.BackColor = Color.White;
            foreach (TableCell cell in grdStockLocTransferSearch.HeaderRow.Cells)
            {
                cell.BackColor = grdStockLocTransferSearch.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in grdStockLocTransferSearch.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = grdStockLocTransferSearch.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = grdStockLocTransferSearch.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            grdStockLocTransferSearch.RenderControl(hw);
            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    #endregion

    #region Grid

    protected void grdStockLocTransferSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdStockLocTransferSearch.PageIndex = e.NewPageIndex;
        Search();
    }

    protected void grdStockLocTransferSearch_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Del")
        {
            try
            {
                int RowIndex = ((GridViewRow)((LinkButton)e.CommandSource).NamingContainer).RowIndex;
                int ID = new AppConvert(e.CommandArgument);
                AppTransferStockLocationDetail objAppTransferStockLocationDetail = new AppTransferStockLocationDetail(intRequestingUserID, ID);

                AppInwardDetailColl objAppInwardDetailColl = new AppInwardDetailColl(intRequestingUserID);
                objAppInwardDetailColl.AddCriteria(InwardDetail.Status, Operators.Equals, (int)RecordStatus.Active);
                objAppInwardDetailColl.AddCriteria(InwardDetail.VoucherType_ID, Operators.Equals, 43);
                objAppInwardDetailColl.AddCriteria(InwardDetail.VoucherDetailID, Operators.Equals, ID);               
                objAppInwardDetailColl.Search();

                if (objAppInwardDetailColl.Count > 0)
                {                   
                    AppInwardDetail FromobjAppInwardDetail = new AppInwardDetail(intRequestingUserID, objAppTransferStockLocationDetail.InwardDetail_ID);
                    AppInwardDetail ToobjAppInwardDetail = new AppInwardDetail(intRequestingUserID);
                    ToobjAppInwardDetail = objAppInwardDetailColl.FirstOrDefault();

                    #region Order Validation

                    AppMaterialOrderDetailColl objAppMaterialOrderDetailColl = new AppMaterialOrderDetailColl(intRequestingUserID);
                    objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.InwardDetail_ID, Operators.Equals, ToobjAppInwardDetail.ID);
                    objAppMaterialOrderDetailColl.AddCriteria(MaterialOrderDetail.Status, Operators.Equals, (int)RecordStatus.Active);
                    objAppMaterialOrderDetailColl.Search();

                    if (objAppMaterialOrderDetailColl.Count() > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Order has been made. Please delete the Order first" + "','2'" + ");", true);
                        return;
                    }

                    #endregion

                    objAppTransferStockLocationDetail.Status = (int)RecordStatus.Deleted;
                    objAppTransferStockLocationDetail.ModifiedBy = intRequestingUserID;
                    objAppTransferStockLocationDetail.ModifiedOn = DateTime.Now;
                    objAppTransferStockLocationDetail.Save();

                    ToobjAppInwardDetail.Status = (int)RecordStatus.Deleted;
                    ToobjAppInwardDetail.ModifiedBy = intRequestingUserID;
                    ToobjAppInwardDetail.ModifiedOn = DateTime.Now;
                    ToobjAppInwardDetail.Save();

                    FromobjAppInwardDetail.InwardQuantity = FromobjAppInwardDetail.InwardQuantity + ToobjAppInwardDetail.InwardQuantity;
                    FromobjAppInwardDetail.ModifiedBy = intRequestingUserID;
                    FromobjAppInwardDetail.ModifiedOn = DateTime.Now;
                    FromobjAppInwardDetail.Save();

                    //InsertTransactionLog(21, MODID, 4, intRequestingUserID);
                }

                Search();
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "Deleted Sucessfully" + "','1'" + ");", true);
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Foreign"))
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + "This Detail Is Used For Outward" + "','2'" + ");", true);
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS1", @"ShowAnimatedMessage('" + ex.Message + "','2'" + ");", true);
            }
        }
    }

    #endregion
}