﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="CustomerItemPrice" Codebehind="CustomerItemPrice.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <asp:UpdatePanel runat="server" ID="PnlCustomerItemPrice">
                        <ContentTemplate>
                            <asp:GridView ID="grdCustomerItemPrice" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Both" ShowFooter="true"
                                OnPreRender="grdCustomerItemPrice_PreRender" OnRowCommand="grdCustomerItemPrice_RowCommand" OnRowDeleting="grdCustomerItemPrice_RowDeleting" OnRowUpdating="grdCustomerItemPrice_RowUpdating"
                                OnRowDataBound="grdCustomerItemPrice_RowDataBound" OnRowEditing="grdCustomerItemPrice_RowEditing"
                                CssClass="table table-hover table-striped text-nowrap nowrap">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="15">
                                        <HeaderTemplate>
                                            <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="ADDNEW" CommandArgument=""
                                                Text="Add New" ToolTip="Add New" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnEdit" runat="server" CssClass="fa fa-edit fa-2x fa-color-Edit " CommandName="Edit"
                                                Text="" ToolTip="Edit" />
                                            <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete " CommandName="Delete" Text="" ToolTip="Delete" />
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="EditItem"
                                                Text="" />
                                            <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" CssClass="fa fa-ban fa-2x" ToolTip="Cancel"
                                                Text="" />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x fa-color-Add" ToolTip="Save"
                                                Text="" ValidationGroup="AddItem" UseSubmitBehavior="False" />
                                            <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban fa-2x" Text="" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="150">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderItem" Text="Item" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblITItem" runat="server" CssClass="bold" Text='<%# Eval("Item.Name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtItem" runat="server" ControlToValidate="txtItem" CssClass="text-danger" Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtItem" TabIndex="1" Text='<%# Eval("Item.Name") %>' placeholder="Select Item" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfItemName" Value='<%# Eval("Item.Name") %>' />
                                            <asp:HiddenField runat="server" ID="hdfItemID" Value='<%# Eval("Item_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtItem" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="VendorItemSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtItem">
                                            </asp:AutoCompleteExtender>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtNewItem" runat="server" ControlToValidate="txtNewItem" CssClass="text-danger" Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtNewItem" TabIndex="1" Text='' placeholder="Select Item" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfNewItemName" Value='' />
                                            <asp:HiddenField runat="server" ID="hdfNewItemID" Value='' />
                                            <asp:AutoCompleteExtender ID="ACEtxtNewItem" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="ItemSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtNewItem">
                                            </asp:AutoCompleteExtender>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderUOM" Text="UOM" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblUOM" runat="server" CssClass="bold" Text='<%# Eval("UOMType.Name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:HiddenField runat="server" ID="hdfUOMID" Value='<%# Eval("UOMType_ID") %>' />
                                            <asp:RequiredFieldValidator ID="Rfv_txtUOM" runat="server" ControlToValidate="txtUOM" CssClass="text-danger" Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtUOM" TabIndex="1" Text='<%# Eval("UOMType.Name") %>' AutoPostBack="false" placeholder="Select UOM" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfUOMName" Value='<%# Eval("UOMType.Name") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtUOM" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="UOMTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtUOM">
                                            </asp:AutoCompleteExtender>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:HiddenField runat="server" ID="hdfNewUOMID" Value='' />
                                            <asp:RequiredFieldValidator ID="Rfv_txtNewUOM" runat="server" ControlToValidate="txtNewUOM" CssClass="text-danger" Display="Dynamic" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtNewUOM" TabIndex="1" Text='' AutoPostBack="false" placeholder="Select UOM" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfNewUOMName" Value='' />
                                            <asp:AutoCompleteExtender ID="ACEtxtNewUOM" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="UOMTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtNewUOM">
                                            </asp:AutoCompleteExtender>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderPrice" Text="Price" runat="server" CssClass="bold"></asp:Label>
                                                (  <span><i class="fa fa-inr"></i></span>)
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPrice" runat="server" CssClass="bold" Text='<%# Eval("Price").ToString()=="0"?"":Eval("Price") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtPrice" TabIndex="1" Text='<%# Eval("Price") %>' placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewPrice" TabIndex="1" placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderMRP" Text="MRP" runat="server" CssClass="bold"></asp:Label>
                                                (  <span><i class="fa fa-inr"></i></span>)
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMRP" runat="server" CssClass="bold" Text='<%# Eval("MRP").ToString()=="0"?"":Eval("MRP") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtMRP" TabIndex="1" Text='<%# Eval("MRP") %>' placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewMRP" TabIndex="1" placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblFromDate" Text="From Dt" runat="server" CssClass="bold"></asp:Label>
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblFromDate" runat="server" CssClass="bold" Text='<%# Eval("FromDate").ToString()=="01-01-0001 12:00:00 AM"?"":Eval("FromDate").ToString() %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtFromDate" TabIndex="1" Text='<%# Eval("FromDate") %>' runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:MaskedEditExtender ID="MSE_txtFromDate" runat="server"
                                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFromDate" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="">
                                            </asp:MaskedEditExtender>
                                            <asp:CalendarExtender ID="CE_FromDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtFromDate"
                                                TargetControlID="txtFromDate" Enabled="True">
                                            </asp:CalendarExtender>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewFromDate" TabIndex="1" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:MaskedEditExtender ID="MSE_txtNewFromDate" runat="server"
                                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtNewFromDate" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="">
                                            </asp:MaskedEditExtender>
                                            <asp:CalendarExtender ID="CE_FromDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtNewFromDate"
                                                TargetControlID="txtNewFromDate" Enabled="True">
                                            </asp:CalendarExtender>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblToDate" Text="To Dt" runat="server" CssClass="bold"></asp:Label>
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblHeaderToDate" runat="server" CssClass="bold" Text='<%# Eval("ToDate").ToString()=="01-01-0001 12:00:00 AM"?"":Eval("ToDate").ToString() %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtToDate" TabIndex="1" Text='<%# Eval("ToDate") %>' runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:MaskedEditExtender ID="MSE_txtToDate" runat="server"
                                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtToDate" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="">
                                            </asp:MaskedEditExtender>
                                            <asp:CalendarExtender ID="CE_txtToDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtToDate"
                                                TargetControlID="txtToDate" Enabled="True">
                                            </asp:CalendarExtender>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewToDate" TabIndex="1" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:MaskedEditExtender ID="MSE_txtNewToDate" runat="server"
                                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtNewToDate" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="">
                                            </asp:MaskedEditExtender>
                                            <asp:CalendarExtender ID="CE_txtNewToDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtNewToDate"
                                                TargetControlID="txtNewToDate" Enabled="True">
                                            </asp:CalendarExtender>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderDiscount" Text="Discount" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                             <asp:Label ID="lblDiscount" runat="server" CssClass="bold" Text='<%# Eval("Discount").ToString()=="0"?"":Eval("Discount") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtDiscount" TabIndex="1" Text='<%# Eval("Discount") %>' placeholder="0.00" runat="server" AutoCompleteType="None" AutoComplete="Off"
                                                CssClass="form-control input-sm"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="txtNewDiscount" TabIndex="1" Text='' runat="server" AutoCompleteType="None" AutoComplete="Off"
                                                CssClass="form-control input-sm"></asp:TextBox>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField Visible="true">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderLineItemCompanyAddress" Text="Company Address" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%--<asp:Label ID="lblITAddress" runat="server" CssClass="bold" Text='<%# Eval("CompanyAddress.Address1") %>'></asp:Label>--%>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlDispatchAdd" runat="server" TabIndex="1" CssClass="form-control input-sm">
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="ddlNewDispatchAdd" runat="server" TabIndex="1" CssClass="form-control input-sm">
                                            </asp:DropDownList>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHdrCurrency" Text="Currency" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItCurrency" runat="server" Text='<%#Eval("CurrencyType.Name") %>' CssClass="bold"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlCurrency" runat="server" TabIndex="8" CssClass="form-control">
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:DropDownList ID="ddlNewCurrency" runat="server" TabIndex="8" CssClass="form-control">
                                            </asp:DropDownList>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblActive" runat="server" Text='<%#Eval("Status").ToString()=="1"?"Y":"N" %>' CssClass="bold"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:CheckBox runat="server" ID="chkActive" TabIndex="9" CssClass="bold" Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                                        </EditItemTemplate>
                                        <FooterTemplate>
                                            <asp:CheckBox runat="server" ID="chkIsActive" TabIndex="9" CssClass=" bold" Checked="true" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
