﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="Home" Codebehind="Home.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="dashboard-stats__item bg-orange">
                <i class="fa fa-comments"></i>
                <div class="row">
                    <div class="col-xs-12">
                        <h4>
                            <div class="pull-right" style="font-size: 20px;">Gate Entry</div>
                        </h4>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-9">
                            <span class="pull-right" style="padding-right: 0;">Total </span>
                        </div>
                        <div class="col-xs-3 spnPercent">
                            <span class="pull-right" style="text-overflow: ellipsis; white-space: nowrap;" runat="server" id="spnMonthlyBookedSites">0</span>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-xs-9">
                            <span class="pull-right">Average</span>
                        </div>
                        <div class="col-xs-3 spnPercent">
                            <span class="pull-right" runat="server" id="spnMonthlyBookedPercent"><b>0</b><i class="fa  fa-percent"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="dashboard-stats__item bg-pink">
                <i class="fa fa-calendar"></i>
                <div class="row">
                    <div class="col-xs-12">
                        <h4>
                            <div class="pull-right" style="font-size: 20px;">Inward</div>
                        </h4>
                    </div>

                    <div class="col-xs-12">
                        <div class="col-xs-9">
                            <span class="pull-right" style="padding-right: 0;">Total</span>
                        </div>
                        <div class="col-xs-3 spnPercent">
                            <span class="pull-right" style="text-overflow: ellipsis; white-space: nowrap;" runat="server" id="spnYearlyBooking">0</span>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="col-xs-9">
                            <span class="pull-right">Average</span>
                        </div>
                        <div class="col-xs-3 spnPercent">
                            <span class="pull-right" runat="server" id="spnTotalYearlyPercent"><b>0</b><i class="fa  fa-percent"></i></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="dashboard-stats__item bg-accent">
                <i class="fa fa-pie-chart"></i>
                <div class="row">
                    <div class="col-xs-12">
                        <h4>
                            <div class="pull-right" style="font-size: 20px;">Material Order</div>
                        </h4>
                    </div>


                    <div class="col-xs-12">
                        <div class="col-xs-9">
                            <span class="pull-right" style="padding-right: 0;">Total</span>
                        </div>
                        <div class="col-xs-3 spnPercent">
                            <span class="pull-right" style="text-overflow: ellipsis; white-space: nowrap;" runat="server" id="Span1">0</span>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="col-xs-9">
                            <span class="pull-right">Average</span>
                        </div>
                        <div class="col-xs-3 spnPercent">
                            <span class="pull-right" runat="server" id="Span2"><b>0</b><i class="fa  fa-percent"></i></span>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-3 col-md-6">
            <div class="dashboard-stats__item bg-teal">
                <i class="fa fa-globe"></i>
                <div class="row">
                    <div class="col-xs-12">
                        <h4>
                            <div class="pull-right" style="font-size: 20px;">Outward</div>
                        </h4>
                    </div>

                    <div class="col-xs-12">
                        <div class="col-xs-9">
                            <span class="pull-right" style="padding-right: 0;">Total</span>
                        </div>
                        <div class="col-xs-3 spnPercent">
                            <span class="pull-right" style="text-overflow: ellipsis; white-space: nowrap;" runat="server" id="Span3">0</span>
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="col-xs-9">
                            <span class="pull-right">Average</span>
                        </div>
                        <div class="col-xs-3 spnPercent">
                            <span class="pull-right" runat="server" id="Span4"><b>0</b><i class="fa  fa-percent"></i></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row" style="display: none">


        <div class="col-lg-6">
            <div class="panel">
                <div class="panel-body">
                    <h4><i class="fa fa-line-chart"></i>&nbsp;Company's Performance</h4>
                    <hr />
                    <canvas id="lineChart"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="panel">
                <div class="panel-body">
                    <h4><i class="fa fa-bar-chart"></i>&nbsp;Company's Performance</h4>
                    <hr />
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(function () {


        });

        function LoadBarChart() {

            $.ajax({

                type: "POST",
                url: "Home.aspx/GetBarChart",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    var aData = r.d;
                    var aLabels = aData[0];
                    var InwardSet = aData[1];
                    var OutwardSet = aData[2];
                    var bgColor = aData[3];
                    var hbgColor = aData[3];

                    var ctx = document.getElementById("myChart");
                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: aLabels,
                            datasets: [{
                                label: '# of Revenue',
                                data: InwardSet,
                                backgroundColor: bgColor,
                                borderColor: hbgColor,
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });




                    var ctx = document.getElementById("lineChart");
                    var myChart = new Chart.Line(ctx, {
                        type: 'line',
                        data: {
                            labels: aLabels,
                            datasets: [{
                                label: 'Inward',
                                data: InwardSet,
                                backgroundColor: 'rgba(54, 162, 235, 0.7)'
                            }, {
                                label: 'Outward',
                                data: OutwardSet,
                                backgroundColor: 'rgba(84, 84, 84, 0.6)'
                            }]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });

                }
            })
        }
    </script>
</asp:Content>

