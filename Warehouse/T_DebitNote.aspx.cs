﻿#region Author
/*
Name    :   Navnath Sonavane
Date    :   04/08/2016
Use     :   This form is used to add Debit Voucher Order details  
*/
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.Services;
using AppUtility;
using Objects;
using AjaxControlToolkit;
#endregion
public partial class T_DebitNote : BigSunPage
{
    #region Page_Load
    AppObjects.AppDebitNote obj = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = new AppObjects.AppDebitNote(intRequestingUserID);
        if (!IsPostBack)
        {
            Session[TableConstants.DebitNote] = obj;
            string ID = Convert.ToString(Request.QueryString["DebitNoteID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int DebitNoteID = new AppUtility.AppConvert(ID);
                obj = new AppObjects.AppDebitNote(intRequestingUserID, DebitNoteID);
                PopulateData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "2")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Debit Note Approved Sucessfully','1'" + ");", true);
                }
            }
            else
            {
                DefaultData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
            }
            Session[TableConstants.DebitNote] = obj;
            BindData();
        }
    }
    #endregion
    #region Button Click Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppDebitNote DebitNote = (AppDebitNote)Session[TableConstants.DebitNote];
        DebitNote.DebitNoteDetailColl.RemoveAll(x => x.Ledger_ID == 0);
        if (DebitNote.DebitNoteDetailColl.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please add atleast one item','2'" + ");", true);
            return;
        }
        Session.Add(TableConstants.DebitNote, DebitNote);
        decimal DebitAmount = DebitNote.DebitNoteDetailColl.Sum(x => x.DebitAmount);
        decimal CreditAmount = DebitNote.DebitNoteDetailColl.Sum(x => x.CreditAmount);
        if (DebitAmount != CreditAmount)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Debit and Credit amount not matching.','2'" + ");", true);
            return;
        }
        SaveDebitNoteHeader();
        Session.Remove(TableConstants.DebitNote);
        Response.Redirect("T_DebitNote.aspx?Msg=1");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("T_DebitNoteSummary.aspx");
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        AppDebitNote DN = (AppDebitNote)Session[TableConstants.DebitNote];
        DN.Status = new AppConvert((int)RecordStatus.Approve);
        DN.Save();
        Response.Redirect("T_DebitNote.aspx?Msg=2&DebitNoteID="+Convert.ToString(Request.QueryString["DebitNoteID"]));
    }
    protected void btnAddLineItem_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            AddEmptyRow(1);
            if (grdDebitNoteDetails.Rows.Count > 0)
            {
                TextBox txtLedger = (TextBox)grdDebitNoteDetails.Rows[0].FindControl("txtLedger");
                AppDebitNote DebitNote = (AppDebitNote)Session[TableConstants.DebitNote];
                if (DebitNote == null)
                {
                    DebitNote = new AppDebitNote(intRequestingUserID);
                }

                txtLedger.Focus();
            }
        }
    }
    #endregion
    #region Functions
    public bool Validation()
    {

        if (grdDebitNoteDetails.Rows.Count > 0)
        {
            TextBox txtLedger = (TextBox)grdDebitNoteDetails.Rows[0].FindControl("txtLedger");
            TextBox txtDebit = (TextBox)grdDebitNoteDetails.Rows[0].FindControl("txtDebit");
            TextBox txtCredit = (TextBox)grdDebitNoteDetails.Rows[0].FindControl("txtCredit");
            if (txtLedger.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please select ledger first.','2'" + ");", true);
                return false;
            }
            if (txtDebit.Text == txtCredit.Text)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter debit or credit value for selected ledger.','2'" + ");", true);
                return false;
            }

            txtLedger.Focus();
        }
        return true;
    }
    public void DefaultData()
    {
        txtDebitNoteDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);
        btnApprove.Visible = false;
    }
    public void PopulateData()
    {
        txtDebitNoteDate.Text = new AppConvert(obj.DebitNoteDate);
        txtDebitNoteNo.Text = new AppConvert(obj.DebitNoteNo);
        txtStatus.Text = obj.StatusName;
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);
        txtHeaderExchangeCurrency.Text = obj.CurrencyType.Name;
        hdfHeaderExchangeCurrencyName.Value = txtHeaderExchangeCurrency.Text;
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(obj.CurrencyType_ID);
        btnSave.Visible = obj.Status != new AppConvert((int)RecordStatus.Created) ? false : true;
        btnApprove.Visible = obj.Status == new AppConvert((int)RecordStatus.Created) ? true : false;
    }
    public void BindData()
    {
        AppDebitNote DebitNote = (AppDebitNote)Session[TableConstants.DebitNote];
        if (DebitNote == null)
        {
            DebitNote = new AppDebitNote(intRequestingUserID);
        }
        grdDebitNoteDetails.DataSource = DebitNote.DebitNoteDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created));
        grdDebitNoteDetails.DataBind();
        if (DebitNote.DebitNoteDetailColl.Count <= 0)
        {
            AddEmptyRow(1);
        }
        SetValueToGridFooter();
    }
    public void SaveDebitNoteHeader()
    {
        AppDebitNote DN = (AppDebitNote)Session[TableConstants.DebitNote];
        if (DN.ID == 0)
        {
            DN.DebitNoteNo = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "DN", System.DateTime.Now,"DN-" ,4, ResetCycle.Yearly));
        }
        DN.VoucherType_ID = 19;
        DN.TotalAmount = new AppConvert(txtTotalAmount.Text);
        DN.Company_ID = new AppConvert(LoginCompanyID);
        DN.DebitNoteDate = new AppConvert(txtDebitNoteDate.Text);
        DN.CurrencyType_ID = new AppConvert(hdfHeaderExchangeCurrencyID.Value);
        DN.Status = new AppConvert((int)RecordStatus.Created);
        DN.Type = obj.Type;
        DN.ModifiedBy = intRequestingUserID;
        DN.ModifiedOn = System.DateTime.Now;
        DN.Save();

        if (DN.DebitNoteDetailColl.Count > 0)
        {
            foreach (var item in DN.DebitNoteDetailColl)
            {
                if (item.CreditAmount != 0)
                {
                    LedgerEntry(LoginCompanyID, 101002, 1010, item.Ledger_ID, item.ID, DN.DebitNoteDate, item.CreditAmount, 0, DN.DebitNoteNo, item.ModifiedBy, item.ModifiedOn, item.Status, DN.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, DN.VoucherType_ID, item.DebitNoteDesc);
                }
                else
                {
                    LedgerEntry(LoginCompanyID, 101001, 1010, item.Ledger_ID, item.ID, DN.DebitNoteDate, 0, item.DebitAmount, DN.DebitNoteNo, item.ModifiedBy, item.ModifiedOn, item.Status, DN.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, DN.VoucherType_ID, item.DebitNoteDesc);
                }


            }

        }
    }
    public void SetValueToGridFooter()
    {
        AppDebitNote DebitNote = (AppDebitNote)Session[TableConstants.DebitNote];
        Label lblFooterCreditAmount = (Label)grdDebitNoteDetails.FooterRow.FindControl("lblFooterCreditAmount");
        lblFooterCreditAmount.Text = Convert.ToDecimal(DebitNote.DebitNoteDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created)).Sum(x => x.CreditAmount)).ToString("#,##0.00");

        Label lblFooterDebitAmount = (Label)grdDebitNoteDetails.FooterRow.FindControl("lblFooterDebitAmount");
        lblFooterDebitAmount.Text = Convert.ToDecimal(DebitNote.DebitNoteDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created)).Sum(x => x.DebitAmount)).ToString("#,##0.00");
        txtTotalAmount.Text = lblFooterDebitAmount.Text;



    }
    public void AddEmptyRow(int NumberOfRows)
    {
        AppDebitNote DebitNote = (AppDebitNote)Session[TableConstants.DebitNote];
        if (DebitNote == null)
        {
            DebitNote = new AppDebitNote(intRequestingUserID);
        }
        AppDebitNoteDetail DebitNoteItem = new AppDebitNoteDetail(intRequestingUserID);
        for (int i = 0; i < NumberOfRows; i++)
        {
            if (DebitNote.DebitNoteDetailColl.Count == 0)
            {
                DebitNote.DebitNoteDetailColl.Add(DebitNoteItem);
            }
            else
            {
                DebitNote.DebitNoteDetailColl.Insert(0, DebitNoteItem);
            }
            if (DebitNote.DebitNoteDetailColl.Count >= 0)
            {
                decimal DebitAmount = DebitNote.DebitNoteDetailColl.Sum(x => x.DebitAmount);
                decimal CreditAmount = DebitNote.DebitNoteDetailColl.Sum(x => x.CreditAmount);
                if (DebitAmount > CreditAmount)
                {

                    DebitNote.DebitNoteDetailColl[0].CreditAmount = new AppConvert(DebitAmount - CreditAmount);

                }
                else
                {
                    DebitNote.DebitNoteDetailColl[0].DebitAmount = new AppConvert(CreditAmount - DebitAmount);

                }
            }
            Session.Add(TableConstants.DebitNote, DebitNote);
        }
        BindData();
    }
    #endregion
    #region Text Box Event

    protected void txtLedger_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfLedgerID = (HiddenField)grdDebitNoteDetails.Rows[RowIndex].FindControl("hdfLedgerID");
        TextBox txtLedger = (TextBox)grdDebitNoteDetails.Rows[RowIndex].FindControl("txtLedger");
        TextBox txtDebitNoteDesc = (TextBox)grdDebitNoteDetails.Rows[RowIndex].FindControl("txtDebitNoteDesc");
        AppDebitNote DebitNote = (AppDebitNote)Session[TableConstants.DebitNote];
        DebitNote.DebitNoteDetailColl[RowIndex].Ledger_ID = new AppUtility.AppConvert(hdfLedgerID.Value);
        DebitNote.DebitNoteDetailColl[RowIndex].ModifiedBy = intRequestingUserID;
        DebitNote.DebitNoteDetailColl[RowIndex].ModifiedOn = System.DateTime.Now;
        DebitNote.DebitNoteDetailColl[RowIndex].Status = new AppConvert((int)RecordStatus.Created);
        DebitNote.DebitNoteDetailColl[RowIndex].Type = new AppConvert((int)DefaultNormalType.Normal);
        Session.Remove(TableConstants.DebitNote);
        Session.Add(TableConstants.DebitNote, DebitNote);
        BindData();
        txtDebitNoteDesc.Focus();
    }


    protected void txtDebitNoteDesc_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtDebitNoteDesc = (TextBox)grdDebitNoteDetails.Rows[RowIndex].FindControl("txtDebitNoteDesc");
        TextBox txtDebit = (TextBox)grdDebitNoteDetails.Rows[RowIndex].FindControl("txtDebit");
        AppDebitNote DebitNote = (AppDebitNote)Session[TableConstants.DebitNote];
        DebitNote.DebitNoteDetailColl[RowIndex].DebitNoteDesc = new AppUtility.AppConvert(txtDebitNoteDesc.Text);

        Session.Remove(TableConstants.DebitNote);
        Session.Add(TableConstants.DebitNote, DebitNote);
        BindData();
        txtDebit.Focus();
    }
    protected void txtProfitCenter_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtProfitCenter = (TextBox)grdDebitNoteDetails.Rows[RowIndex].FindControl("txtProfitCenter");
        HiddenField hdfProfitCenterID = (HiddenField)grdDebitNoteDetails.Rows[RowIndex].FindControl("hdfProfitCenterID");
        AppDebitNote DebitNote = (AppDebitNote)Session[TableConstants.DebitNote];
        DebitNote.DebitNoteDetailColl[RowIndex].ProfitCenter_ID = new AppUtility.AppConvert(hdfProfitCenterID);
        Session.Remove(TableConstants.DebitNote);
        Session.Add(TableConstants.DebitNote, DebitNote);
        BindData();
        txtProfitCenter.Focus();
    }

    protected void txtCostCenter_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtCostCenter = (TextBox)grdDebitNoteDetails.Rows[RowIndex].FindControl("txtCostCenter");
        HiddenField hdfCostCenterID = (HiddenField)grdDebitNoteDetails.Rows[RowIndex].FindControl("hdfCostCenterID");
        AppDebitNote DebitNote = (AppDebitNote)Session[TableConstants.DebitNote];
        DebitNote.DebitNoteDetailColl[RowIndex].CostCenter_ID = new AppUtility.AppConvert(hdfCostCenterID.Value);
        Session.Remove(TableConstants.DebitNote);
        Session.Add(TableConstants.DebitNote, DebitNote);
        BindData();
        txtCostCenter.Focus();
    }

    protected void txtDebit_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtDebit = (TextBox)grdDebitNoteDetails.Rows[RowIndex].FindControl("txtDebit");
        TextBox txtCredit = (TextBox)grdDebitNoteDetails.Rows[RowIndex].FindControl("txtCredit");
        AppDebitNote DebitNote = (AppDebitNote)Session[TableConstants.DebitNote];
        DebitNote.DebitNoteDetailColl[RowIndex].DebitAmount = new AppUtility.AppConvert(txtDebit.Text);
        DebitNote.DebitNoteDetailColl[RowIndex].CreditAmount = new AppUtility.AppConvert("0.00");

        Session.Remove(TableConstants.DebitNote);
        Session.Add(TableConstants.DebitNote, DebitNote);
        BindData();
        txtCredit.Focus();
    }

    protected void txtCredit_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtCredit = (TextBox)grdDebitNoteDetails.Rows[RowIndex].FindControl("txtCredit");
        Button btnAddLineItem = (Button)grdDebitNoteDetails.HeaderRow.FindControl("btnAddLineItem");
        AppDebitNote DebitNote = (AppDebitNote)Session[TableConstants.DebitNote];
        DebitNote.DebitNoteDetailColl[RowIndex].CreditAmount = new AppUtility.AppConvert(txtCredit.Text);
        DebitNote.DebitNoteDetailColl[RowIndex].DebitAmount = new AppUtility.AppConvert("0.00");
        Session.Remove(TableConstants.DebitNote);
        Session.Add(TableConstants.DebitNote, DebitNote);
        BindData();
        btnAddLineItem.Focus();
    }



    #endregion
    #region Grid View Events
    protected void grdDebitNoteDetails_PreRender(object sender, EventArgs e)
    {
        if (grdDebitNoteDetails.Rows.Count > 0)
        {
            grdDebitNoteDetails.UseAccessibleHeader = true;
            grdDebitNoteDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdDebitNoteDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
            AppDebitNote DebitNote = (AppDebitNote)Session[TableConstants.DebitNote];
            DebitNote.DebitNoteDetailColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created)).ToList()[row.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
            Session.Add(TableConstants.DebitNote, DebitNote);
            BindData();
        }

    }
    #endregion
}