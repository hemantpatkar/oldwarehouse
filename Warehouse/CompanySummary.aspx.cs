﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to display all Company lists.
*/
#endregion
#region Using
using System;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.UI;
#endregion
public partial class CompanySummary : BigSunPage
{
    AppCompanyColl objCompany;
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }
    #endregion
    #region BindData   
    public void BindData()
    {
        AppCompanyColl objCollection = new AppCompanyColl(intRequestingUserID);
        RecordStatus Status = CommonFunctions.GetStatus(ddlStatus.SelectedValue);
        objCollection.Clear();
        if (!string.IsNullOrEmpty(txtCompanyCode.Text))
        {
            objCollection.AddCriteria(AppObjects.Company.Code, AppUtility.Operators.Like, txtCompanyCode.Text);
        }

        if (!string.IsNullOrEmpty(txtCompanyName.Text))
        {
            objCollection.AddCriteria(AppObjects.Company.Name, AppUtility.Operators.Like, txtCompanyName.Text);
        }

        if (ddlStatus.SelectedValue != "-32768")
        {
            objCollection.AddCriteria(AppObjects.Company.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objCollection.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Company.Type, AppUtility.Operators.Equals, (int)CompanyRoleType.Self, 1);
        objCollection.Search(RecordStatus.ALL);
        Session.Add(TableConstants.ObjCompanySessionColl, objCollection);
        grdCompanySummary.DataSource = objCollection;
        grdCompanySummary.DataBind();
       
        if (objCollection.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Records Found','2'" + ");", true);
        }
    }
    #endregion
    #region GridView Events
    protected void grdCompanySummary_PreRender(object sender, EventArgs e)
    {
        if (grdCompanySummary.Rows.Count > 0)
        {
            grdCompanySummary.UseAccessibleHeader = true;
            grdCompanySummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdCompanySummary_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        objCompany = (AppCompanyColl)Session[TableConstants.ObjCompanySessionColl];
        objCompany[e.RowIndex].Status = new AppUtility.AppConvert((int)RecordStatus.Deleted);
        objCompany[e.RowIndex].Save();
        Session.Add(TableConstants.ObjCompanySessionColl, objCompany);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Company Deleted Successfully','1'" + ");", true);
        BindData();
    }
    #endregion   
    #region Button Click Events
    protected void lnkNewCompany_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.ObjCompanySession);
        Response.Redirect("CompanyDetails.aspx");
    }
    protected void Search_Click(object sender, EventArgs e)
    {
        BindData();
    }
    #endregion
    protected void grdCompanySummary_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string CommandName = e.CommandName;
        int commandArgument = new AppUtility.AppConvert(e.CommandArgument);
        if(CommandName=="APPROVE")
        {
            objCompany = (AppCompanyColl)Session[TableConstants.ObjCompanySessionColl];
            objCompany[commandArgument].Status = new AppUtility.AppConvert((int)CompanyStatus.Approve);
            objCompany[commandArgument].Save();
            Session.Add(TableConstants.ObjCompanySessionColl, objCompany);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Company Successfully Approved','1'" + ");", true);
            BindData();
        }
    }
    protected void grdCompanySummary_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        objCompany = (AppCompanyColl)Session[TableConstants.ObjCompanySessionColl];
        if (objCompany.Count > 0 && objCompany!=null)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (objCompany[e.Row.RowIndex].Status == new AppUtility.AppConvert((int)RecordStatus.Deleted))
                {
                    LinkButton lbtnApprove = (LinkButton)e.Row.FindControl("lbtnApprove");
                    lbtnApprove.Enabled = false;
                    lbtnApprove.ToolTip = "Already Deleted";
                    LinkButton lbtnDelete = (LinkButton)e.Row.FindControl("lbtnDelete");
                    lbtnDelete.Enabled = false;
                    lbtnDelete.ToolTip = "Already Deleted";
                }
                else if (objCompany[e.Row.RowIndex].Status == new AppUtility.AppConvert((int)RecordStatus.Approve))
                {
                    LinkButton lbtnApprove = (LinkButton)e.Row.FindControl("lbtnApprove");
                    lbtnApprove.Enabled = false;
                    lbtnApprove.ToolTip = "Already Approved";
                }
            }
        }
    }
}