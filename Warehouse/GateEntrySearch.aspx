﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" EnableEventValidation="false" Culture="en-gb" AutoEventWireup="true" Inherits="GateEntrySearch" Codebehind="GateEntrySearch.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" DisplayAfter="0" AssociatedUpdatePanelID="grdUpdate"
        runat="server">
        <ProgressTemplate>
            <div class="modal fade in" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-keyboard="false" style="display: block">
                <div class="modal-dialog  modal-sm">
                    <h1 style="text-align: center;">
                        <i class="fa fa-truck fa-magic fa-2x faa-passing animated"></i>
                    </h1>
                </div>
            </div>
            <div class="modal-backdrop fade in"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-lg-9">
            <div class="heading">
                <i class="fa fa-search"></i>
                Gate Entry Search
            </div>
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSearch_Click" Text="Search" ValidationGroup="Search" TabIndex="1" />
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnNew" OnClick="btnNew_Click" CssClass="btn btn-primary btn-block btn-sm" runat="server" AccessKey="n" CausesValidation="False" TabIndex="1" Text="New" />
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnExport" CssClass="btn btn-warning btn-block btn-sm" runat="server" OnClick="btnExportToExcel_Click" AccessKey="n" CausesValidation="False" TabIndex="1" Text="Export" />
        </div>
    </div>
    <hr />
    <div class="newline"></div>
    <div class="row">
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblGateReg" Text="Gate No." CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtsearch" runat="server" CssClass="form-control input-sm" ValidationGroup="Search" TabIndex="1">
            </asp:TextBox>

        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblVehical" Text="Vehicle No." CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtVehical" runat="server" onblur="ClearVechical()" CssClass="form-control input-sm" ValidationGroup="Search" TabIndex="1">
            </asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblfromdate" Text="From Date" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control input-sm" ValidationGroup="Search" TabIndex="1">
            </asp:TextBox>
            <asp:MaskedEditExtender ID="txtDate_MaskedEditExtender" runat="server" Enabled="true" Mask="99/99/9999" ClearMaskOnLostFocus="true"
                MaskType="Date" TargetControlID="txtfromdate">
            </asp:MaskedEditExtender>
            <asp:CalendarExtender ID="CalendarExtender3" TargetControlID="txtfromdate" Format="dd/MM/yyyy"
                PopupButtonID="txtDate" runat="server">
            </asp:CalendarExtender>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lbltodate" Text="To Date" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txttodate" runat="server" CssClass="form-control input-sm" ValidationGroup="Search"
                TabIndex="1">
            </asp:TextBox>
            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="True" Mask="99/99/9999" ClearMaskOnLostFocus="true"
                MaskType="Date" TargetControlID="txttodate">
            </asp:MaskedEditExtender>
            <asp:CalendarExtender ID="CalendarExtender4" TargetControlID="txttodate" Format="dd/MM/yyyy"
                PopupButtonID="txttodate" runat="server">
            </asp:CalendarExtender>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblStatus" Text="Status" CssClass="bold"></asp:Label>
            <asp:DropDownList ID="ddlStatus" runat="server" TabIndex="1" CssClass="form-control input-sm">
                <asp:ListItem Value="-1">All</asp:ListItem>
                <asp:ListItem Value="1" Selected="True">In</asp:ListItem>
                <asp:ListItem Value="2">Out</asp:ListItem>
            </asp:DropDownList>
            <asp:HiddenField ID="hdfgatereg" runat="server" />            
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblDriver" Text="Driver Name" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtDriver" runat="server" CssClass="form-control input-sm" ValidationGroup="Search" TabIndex="1" onblur="ClearDriverNm()">
            </asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblMobile" Text="Mobile No." CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtMobile" runat="server" CssClass="form-control input-sm" ValidationGroup="Search"
                TabIndex="1">
            </asp:TextBox>

        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblChallanNo" Text="Challan No" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtChallanNo" runat="server" CssClass="form-control input-sm" ValidationGroup="Search"
                TabIndex="1">
            </asp:TextBox>

        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblTransporterName" Text="Transporter Name" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtTransporterName" runat="server" CssClass="form-control input-sm" ValidationGroup="Search"
                TabIndex="1">
            </asp:TextBox>

        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-lg-12" style="overflow: auto">
            <asp:UpdatePanel ID="grdUpdate" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdGateRegister" runat="server" GridLines="None"  AllowPaging="True" OnPageIndexChanging="grdGateRegister_PageIndexChanging" 
                        PageSize="50" AutoGenerateColumns="False" PagerStyle-CssClass="pagination-ys" CssClass="table table-hover table-striped text-nowrap table-Full"
                        OnRowCommand="grdGateRegister_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Sr No.">
                                <ItemTemplate>
                                    <asp:Label ID="lblSrno" runat="server" Text="<%# Convert.ToInt32(Container.DataItemIndex)+1%>"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gate No.">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnkGateRegistorNo" runat="server" ToolTip='<%# Eval("GateNo") %>' NavigateUrl='<%# "GateEntryDetail.aspx?ID=" + Eval("ID") %>'
                                        Text='<%# Eval("GateNo") %>' CommandArgument='<%# Eval("ID") %>'></asp:HyperLink><br />
                                    <asp:Label ID="lblChallanNo" runat="server" ToolTip='<%#Eval("ChallanNo") %>' Text='<%#Eval("ChallanNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vehicle Info">
                                <ItemTemplate>
                                    <asp:Label ID="lblVehicalNo" runat="server" ToolTip='<%#Eval("VehicalNo") %>'
                                        Text='<%#Eval("VehicalNo") %>'></asp:Label><br />
                                    <asp:Label ID="lblVehicalName" runat="server" Font-Bold="true" ForeColor="Maroon" ToolTip='<%#Eval("VechicleType.Name") %>'
                                        Text='<%#Eval("VechicleType.Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblpartyname" runat="server" ToolTip='<%#Eval("PartyName") %>'
                                        Text='<%#Eval("PartyName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Driver Info">
                                <ItemTemplate>
                                    <asp:Label ID="lblDriverName" runat="server" ToolTip='<%# Eval("DriverName") %>' Text='<%# Eval("DriverName") %>'></asp:Label><br />
                                    <asp:Label ID="lblMobNo" runat="server" ToolTip='<%# Eval("MobileNo") %>' ForeColor="#3399ff" Font-Bold="true" Text='<%# Eval("MobileNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Transpoter Info">
                                <ItemTemplate>
                                    <asp:Label ID="lblDriverName" runat="server" ToolTip='<%# Eval("TransporterName") %>' Text='<%# Eval("TransporterName") %>'></asp:Label><br />
                                    <asp:Label ID="lblMobNo" runat="server" ToolTip='<%# Eval("LicenseNo") %>' ForeColor="#3399ff" Font-Bold="true" Text='<%# Eval("LicenseNo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="In Units">
                                <ItemTemplate>
                                    <asp:Label ID="lblinunits" runat="server" ToolTip='<%# Eval("InUnit") %>' Text='<%# Eval("InUnit") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Out Units">
                                <ItemTemplate>
                                    <asp:Label ID="lbloutunits" runat="server" ToolTip='<%# Eval("OutUnit") %>' Text='<%# Eval("OutUnit") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Entry On">
                                <ItemTemplate>
                                    <asp:Label ID="lblEntryOn" runat="server" ToolTip='<%# Convert.ToString(Eval("EntryOn"))==null?"": Convert.ToDateTime(Eval("EntryOn")).ToString("dd/MM/yyyy HH : mm") %>' Text='<%# Convert.ToString(Eval("EntryOn"))==null?"": Convert.ToDateTime(Eval("EntryOn")).ToString("dd/MM/yyyy HH : mm") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Exit On">
                                <ItemTemplate>
                                    <asp:Label ID="lblExitOn" runat="server" ToolTip='<%# Convert.ToString(Eval("ExitOn"))=="" ? "" : Convert.ToDateTime(Eval("ExitOn")).ToString("dd/MM/yyyy HH : mm") %>' Text='<%#Convert.ToString(Eval("ExitOn"))==""?"": Convert.ToDateTime(Eval("ExitOn")).ToString("dd/MM/yyyy HH : mm")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Print">
                                <ItemTemplate>
                                    <asp:HyperLink ID="HLReport" runat="server" ToolTip="View Report" Target="_blank" NavigateUrl='<% #"../../Reports/ReportView.aspx?ID=" + Eval("ID") + "&RPName=GatePass"%>'>
                                        <i class="fa fa-print fa-2x"></i>
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>                        
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

