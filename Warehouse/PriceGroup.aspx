﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="PriceGroup" Codebehind="PriceGroup.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function AutoCompleteSearchInGrid(sender, eventArgs) {
            debugger;
            var id = sender._id;
            var autocompleteId = id.split('_');
            var replacetxt = autocompleteId[3].replace('txt', '');
            var txt = $get(autocompleteId[0] + '_' + autocompleteId[1] + '_txt' + replacetxt);
            var txtName = $get(autocompleteId[0] + '_' + autocompleteId[1] + '_hdf' + replacetxt + 'Name');
            var txtID = $get(autocompleteId[0] + '_' + autocompleteId[1] + '_hdf' + replacetxt + 'ID');
            txt.value = eventArgs.get_text();
            txtName.value = eventArgs.get_text();
            txtID.value = eventArgs.get_value();
        }
        function ClearAutocompleteTextBoxInGrid(sender) {
            var id = sender.id;
            var autocompleteId = id.split('_');
            var replacetxt = autocompleteId[2].replace('txt', '');
            var txt = $get(autocompleteId[0] + '_' + autocompleteId[1] + '_txt' + replacetxt);
            var txtName = $get(autocompleteId[0] + '_' + autocompleteId[1] + '_hdf' + replacetxt + 'Name');
            var txtID = $get(autocompleteId[0] + '_' + autocompleteId[1] + '_hdf' + replacetxt + 'ID');
            if (txt.value == "No Records Found" || txt.value != txtName.value) {
                txt.value = "";
                txtName.value = "";
                txtID.value = "";
            }
        }
        function checkIfNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if ((key < 48 || key > 57) || key == 46) {
                return false;
            }
            else return true;
        };
        function validateFloatKeyPress(el, evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57)) {
                return false;
            }
            if (charCode == 46 && el.value.indexOf(".") !== -1) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="pull-right">
                        <br />
                        <asp:LinkButton ID="btnSave" runat="server" ValidationGroup="FinalSave" OnClick="btnSave_Click"><i class="fa fa-check  fa-2x text-success"></i></asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" OnClick="btnCancel_Click"><i class="fa fa-close fa-2x"></i></asp:LinkButton>
                        <%-- <asp:LinkButton ID="btnApprove" runat="server" OnClick="btnApprove_Click"><i class="fa fa-thumb fa-2x"></i></asp:LinkButton>--%>
                    </div>
                    <h3>Price Group
                    </h3>
                    <hr />
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:Label ID="lblPriceGrpCode" runat="server" CssClass="bold" Text="Price Group Code"></asp:Label>
                            <asp:Label ID="Label8" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ControlToValidate="txtPriceGrpCode" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage="Select" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtPriceGrpCode" runat="server" TabIndex="1" MaxLength="50" ValidationGroup="FinalSave" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblPriceGrpName" runat="server" CssClass="bold " Text="Price Group Name"></asp:Label>
                            <asp:Label ID="lblVnameStar" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPriceGrpName" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage=" (Enter Name)" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtPriceGrpName" TabIndex="2" runat="server" MaxLength="100" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="FinalSave"
                                CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="Label3" runat="server" Text="Active/InActive" CssClass="bold"></asp:Label>
                            <div class="material-switch pull-right">
                                <asp:CheckBox runat="server" ID="chkActive" Checked="true" />
                                <label for='<%=chkActive.ClientID %>' class="label-primary"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <asp:UpdatePanel ID="up1" runat="server">
                <ContentTemplate>
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h4>Price Group Items
                                    </h4>
                                    <hr />
                                    <asp:GridView ID="grdPriceGrpItems" runat="server" OnPreRender="grdPriceGrpItems_PreRender" OnRowCancelingEdit="grdPriceGrpItems_RowCancelingEdit"
                                        OnRowCommand="grdPriceGrpItems_RowCommand" OnRowDataBound="grdPriceGrpItems_RowDataBound" OnRowDeleting="grdPriceGrpItems_RowDeleting"
                                        OnRowEditing="grdPriceGrpItems_RowEditing" OnRowUpdating="grdPriceGrpItems_RowUpdating"
                                        AutoGenerateColumns="False" GridLines="Horizontal"
                                        CssClass="table table-striped table-hover table-condensed  text-nowrap">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name">
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="Insert" OnClick="lbtnAddNew_Click"
                                                        Text="Add New" ToolTip="Add New" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnEdit" runat="server" CssClass="fa fa-edit fa-2x fa-color-Edit space" CommandName="EDIT" CommandArgument='<%#Eval("ID") %>'
                                                        Text="" ToolTip="Edit" />
                                                    <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete space" CommandName="DELETE" CommandArgument='<%#Eval("ID") %>' Text="" ToolTip="Delete" />
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="EditItem"
                                                        Text="" />
                                                    <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" CssClass="fa fa-ban fa-2x" ToolTip="Cancel"
                                                        Text="" />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x fa-color-Add" ToolTip="Save"
                                                        Text="" ValidationGroup="AddItem" UseSubmitBehavior="False" />
                                                    <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban  fa-2x" Text="" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblItemType" Text="Item " runat="server"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItemType" runat="server" Text='<%# Eval("Item.Name") %>' TabIndex="2" AutoComplete="Off"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtItemType" runat="server" Text='<%# Eval("Item.Name") %>' MaxLength="500" TabIndex="2" ValidationGroup="EditItem" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBoxInGrid(this)"></asp:TextBox>
                                                    <asp:HiddenField runat="server" ID="hdfItemTypeName" Value='<%# Eval("Item.Name").ToString() %>' />
                                                    <asp:HiddenField runat="server" ID="hdfItemTypeID" Value='<%# Eval("Item_ID") %>' />
                                                    <asp:AutoCompleteExtender ID="Ace_txtItemType" runat="server"
                                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="ItemSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearchInGrid"
                                                        ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtItemType">
                                                    </asp:AutoCompleteExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtItemType" CssClass="text-danger"
                                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="EditItem" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtNewItemType" runat="server" TabIndex="2" PlaceHolder="Select Item Type" AutoPostBack="false" AutoComplete="Off" AutoCompleteType="None" MaxLength="50" CssClass="form-control input-sm"></asp:TextBox>
                                                    <asp:HiddenField runat="server" ID="hdfNewItemTypeName" />
                                                    <asp:HiddenField runat="server" ID="hdfNewItemTypeID" Value="0" />
                                                    <asp:AutoCompleteExtender ID="Ace_txtNewItemType" runat="server"
                                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="ItemSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearchInGrid"
                                                        ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtNewItemType">
                                                    </asp:AutoCompleteExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtNewItemType" CssClass="text-danger" ToolTip="Required Field"
                                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="AddItem" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHdrRate" Text="Rate" runat="server" CssClass="bold" Width="100px"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItRate" runat="server" Text='<%#Eval("Rate").ToString()=="0"?" ":Eval("Rate").ToString() %>' CssClass="bold"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtItemRate" TabIndex="6" runat="server" onkeypress="return validateFloatKeyPress(this, event)" Text='<%#Eval("Rate").ToString() %>' CssClass="form-control input-sm"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtNewItemRate" TabIndex="6" runat="server" onkeypress="return validateFloatKeyPress(this, event)" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHdrMRP" Text="MRP" runat="server" CssClass="bold" Width="100px"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItMRP" runat="server" Text='<%#Eval("MRP").ToString()=="0"?" ":Eval("MRP").ToString() %>' CssClass="bold"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtMRP" TabIndex="6" runat="server" onkeypress="return validateFloatKeyPress(this, event)" Text='<%#Eval("MRP").ToString() %>' CssClass="form-control input-sm"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtNewMRP" TabIndex="6" runat="server" onkeypress="return validateFloatKeyPress(this, event)" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHdrDiscount" Text="Discount" runat="server" CssClass="bold"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItDiscount" runat="server" Text='<%#Eval("Discount").ToString()=="0"?" ":Eval("Discount").ToString() %>' CssClass="bold"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDiscount" TabIndex="7" runat="server" Text='<%#Eval("Discount") %>' onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm" ValidationGroup="EditItem" AutoPostBack="false"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtNewDiscount" TabIndex="7" runat="server" onkeypress="return validateFloatKeyPress(this, event)" MaxLength="10" AutoComplete="Off" AutoPostBack="false" CssClass="form-control input-sm" ValidationGroup="AddItem"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHdrFrmDt" Text="From Date" runat="server" CssClass="bold"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItFrmDt" runat="server" Text='<%#Eval("FromDate").ToString()=="01-01-0001 12:00:00 AM"?" ": Eval("FromDate")%>' CssClass="bold"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtFromDate" TabIndex="7" runat="server" Text='<%#Eval("FromDate") %>' CssClass="form-control input-sm" ValidationGroup="EditItem"></asp:TextBox>
                                                    <asp:MaskedEditExtender ID="txtFrmDate_MaskedEditExtender" runat="server"
                                                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtFromDate">
                                                    </asp:MaskedEditExtender>
                                                    <asp:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtFromDate"
                                                        TargetControlID="txtFromDate">
                                                    </asp:CalendarExtender>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtNewFromDate" TabIndex="7" runat="server" MaxLength="10" AutoComplete="Off" AutoPostBack="false" CssClass="form-control input-sm" ValidationGroup="AddItem"></asp:TextBox>
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender8" runat="server"
                                                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtNewFromDate">
                                                    </asp:MaskedEditExtender>
                                                    <asp:CalendarExtender ID="CalendarExtender6" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtNewFromDate"
                                                        TargetControlID="txtNewFromDate">
                                                    </asp:CalendarExtender>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHdrToDt" Text="To Date" runat="server" CssClass="bold"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItToDt" runat="server" Text='<%#Eval("ToDate").ToString()=="01-01-0001 12:00:00 AM"?" ": Eval("ToDate")%>' CssClass="bold"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtToDate" TabIndex="7" runat="server" Text='<%#Eval("ToDate") %>' CssClass="form-control input-sm" ValidationGroup="EditItem"></asp:TextBox>
                                                    <asp:MaskedEditExtender ID="txtToDate_MaskedEditExtender" runat="server"
                                                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtToDate">
                                                    </asp:MaskedEditExtender>
                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtToDate"
                                                        TargetControlID="txtToDate">
                                                    </asp:CalendarExtender>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:TextBox ID="txtNewToDate" TabIndex="7" runat="server" MaxLength="10" AutoComplete="Off" AutoPostBack="false" CssClass="form-control input-sm" ValidationGroup="AddItem"></asp:TextBox>
                                                    <asp:MaskedEditExtender ID="txtNewToDate_MaskedEditExtender" runat="server"
                                                        Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtNewToDate">
                                                    </asp:MaskedEditExtender>
                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtNewToDate"
                                                        TargetControlID="txtNewToDate">
                                                    </asp:CalendarExtender>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHdrCurrency" Text="Currency" runat="server" CssClass="bold"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblItCurrency" runat="server" Text='<%#Eval("CurrencyType.Name") %>' CssClass="bold"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlCurrency" runat="server" TabIndex="8" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="ddlNewCurrency" runat="server" TabIndex="8" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblUOM" Text="Unit Of Measurement" runat="server" CssClass="bold"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUnitOfMeasu" runat="server" Text='<%#Eval("UOMType.Name") %>' CssClass="bold"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlUOM" runat="server" TabIndex="8" CssClass="form-control input-sm">
                                                    </asp:DropDownList>
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="ddlNewUOM" runat="server" TabIndex="8" CssClass="form-control input-sm">
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblActive" runat="server" Text='<%#Eval("Status").ToString()=="1"?"Y":"N" %>' CssClass="bold"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkActive" TabIndex="9" CssClass="bold" Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                                                </EditItemTemplate>
                                                <FooterTemplate>
                                                    <asp:CheckBox runat="server" ID="chkIsActive" TabIndex="9" CssClass=" bold" Checked="true" />
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdPriceGrpItems" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdPriceGrpItems.ClientID%>');
        $(document).ready(function () {
            GridUI(GridID, 50);
        })
    </script>
</asp:Content>
