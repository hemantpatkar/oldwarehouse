﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="M_Ledger" Codebehind="M_Ledger.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="UI/js/jquery.ztree.core-3.5.js"></script>
    <script src="UI/js/jquery.ztree.exedit-3.5.js"></script>
    <link href="UI/css/zTreeStyle.css" rel="stylesheet" />
    <script type="text/javascript">
        function AutoCompleteSearch(sender, eventArgs) {
            var id = sender._id;
            var autocompleteId = id.split('_');
            var replacetxt = autocompleteId[2].replace('txt', '');
            var txt = $get(autocompleteId[0] + '_txt' + replacetxt);
            var txtName = $get(autocompleteId[0] + '_hdf' + replacetxt + 'Name');
            var txtID = $get(autocompleteId[0] + '_hdf' + replacetxt + 'ID');
            txt.value = eventArgs.get_text();
            txtName.value = eventArgs.get_text();
            txtID.value = eventArgs.get_value();
        }
        function ClearAutocompleteTextBox(sender) {
            var id = sender.id;
            var autocompleteId = id.split('_');
            var replacetxt = autocompleteId[1].replace('txt', '');
            var txt = $get(autocompleteId[0] + '_txt' + replacetxt);
            var txtName = $get(autocompleteId[0] + '_hdf' + replacetxt + 'Name');
            var txtID = $get(autocompleteId[0] + '_hdf' + replacetxt + 'ID');
            if (txt.value == "No Records Found" || txt.value != txtName.value) {
                txt.value = "";
                txtName.value = "";
                txtID.value = "";
            }
        }
        function checkIfNumber(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if ((key < 48 || key > 57) || key == 46) {
                return false;
            }
            else return true;
        }
        var setting = {
            data: {
                keep: {
                    parent: true,
                    leaf: true
                },
                key: {
                    title: "t"
                },
                simpleData: {
                    enable: true
                }
            },
            edit: {
                enable: true,
                showRemoveBtn: false,
                showRenameBtn: false
            },
            view: {
                fontCss: getFont,
                nameIsHTML: true
            },
            callback: {
                beforeClick: beforeClick,
                onClick: onClick,
                onRightClick: OnRightClick,
                beforeDrag: beforeDrag,
                beforeRemove: beforeRemove,
                beforeRename: beforeRename,
                onRemove: onRemove
            }
        };
        function getTime() {
            var now = new Date(),
            h = now.getHours(),
            m = now.getMinutes(),
            s = now.getSeconds();
            return (h + ":" + m + ":" + s);
        }
        function beforeDrag(treeId, treeNodes) {
            return false;
        }
        function beforeRemove(treeId, treeNode) {
            className = (className === "dark" ? "" : "dark");
            showLog("[ " + getTime() + " beforeRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
            return confirm("Confirm delete node '" + treeNode.name + "' it?");
        }
        function onRemove(e, treeId, treeNode) {
            showLog("[ " + getTime() + " onRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
        }
        function beforeRename(treeId, treeNode, newName) {
            if (newName.length == 0) {
                alert("Node name can not be empty.");
                var zTree = $.fn.zTree.getZTreeObj("treeDemo");
                setTimeout(function () { zTree.editName(treeNode) }, 10);
                return false;
            }
            else {
                Rename(treeNode.id, newName);
                return true;
            }
            return true;
        }
        function ajaxGetNodes(treeNode, reloadType) {
            var zTree = $.fn.zTree.getZTreeObj("treeDemo");
            if (reloadType == "refresh") {
                zTree.updateNode(treeNode);
            }
            zTree.reAsyncChildNodes(treeNode, reloadType, true);
        }
        function fontCss(treeNode) {
            var aObj = $("#" + treeNode.tId + "_a");
            aObj.removeClass("copy").removeClass("cut");
            if (treeNode === curSrcNode) {
                if (curType == "copy") {
                    aObj.addClass(curType);
                } else {
                    aObj.addClass(curType);
                }
            }
        }
        var curSrcNode, curType;
        function setCurSrcNode(treeNode) {
            var zTree = $.fn.zTree.getZTreeObj("treeDemo");
            if (curSrcNode) {
                delete curSrcNode.isCur;
                var tmpNode = curSrcNode;
                curSrcNode = null;
                fontCss(tmpNode);
            }
            curSrcNode = treeNode;
            if (!treeNode) return;
            curSrcNode.isCur = true;
            zTree.cancelSelectedNode();
            fontCss(curSrcNode);
        }
        function cut(e) {
            hideRMenu();
            var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			nodes = zTree.getSelectedNodes();
            if (nodes.length == 0) {
                alert("Please select one node at first...");
                return;
            }
            curType = "cut";
            setCurSrcNode(nodes[0]);
        }
        function MoveFolder(FolderID, DestinationID) {
            $.ajax({
                type: "POST",
                url: "M_Ledger.aspx/Move",
                data: "{'FolderID': '" + FolderID + "','DestinationID':'" + DestinationID + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "jsondata",
                async: "true",
                success: function (response) {
                    hdfFoldID = response.d;
                },
                error: function (response) {
                    alert(response.status + ' ' + response.statusText);
                }
            });
        }
        function paste(e) {
            debugger
            hideRMenu();
            if (!curSrcNode) {
                alert("Please select one node to copy or cut at first...");
                return;
            }
            var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			nodes = zTree.getSelectedNodes(),
			targetNode = nodes.length > 0 ? nodes[0] : null;
            if (curSrcNode === targetNode) {
                alert("Can't move, the same source node and destination node.");
                return;
            } else if (curType === "cut" && ((!!targetNode && curSrcNode.parentTId === targetNode.tId) || (!targetNode && !curSrcNode.parentTId))) {
                alert("Can't move, source node is the target node's child.");
                return;
            } else if (curType === "copy") {
                $.ajax({
                    type: "POST",
                    url: "M_Ledger.aspx/Copy",
                    data: "{'FDID': '" + curSrcNode.id + "','PID':'" + targetNode.id + "'}",
                    contentType: "application/json; charset=utf-8",
                    datatype: "jsondata",
                    async: "true",
                    success: function (response) {
                        hdfFoldID = response.d;
                    },
                    error: function (response) {
                        alert(response.status + ' ' + response.statusText);
                    }
                });
                ajaxGetNodes(targetNode, "refresh");
                BindMaintreee();
            } else if (curType === "cut" && curSrcNode.IsFile == 0) {
                debugger
                MoveFolder(curSrcNode.FID, targetNode.FID);
                targetNode = zTree.moveNode(targetNode, curSrcNode, "inner");
                if (!targetNode) {
                    alert("Cutting failure, source node is the target node's parent.");
                }
                targetNode = curSrcNode;
            }
            else if (curType === "cut" && curSrcNode.IsFile == 1) {
                MoveFile(curSrcNode.FID, targetNode.FID);
                targetNode = zTree.moveNode(targetNode, curSrcNode, "inner");
                if (!targetNode) {
                    alert("Cutting failure, source node is the target node's parent.");
                }
                targetNode = curSrcNode;
            }
            setCurSrcNode();
            delete targetNode.isCur;
            zTree.selectNode(targetNode);
        }
        function OnRightClick(event, treeId, treeNode) {
            debugger
            if (!treeNode && event.target.tagName.toLowerCase() != "button" && $(event.target).parents("a").length == 0) {
                zTree.cancelSelectedNode();
                showRMenu("root", event.clientX, event.clientY, zTree.Lock == null ? treeNode.Lock : zTree.Lock, treeNode.IsFile, treeNode.FID);
            }
            else if (treeNode.level == '0') {
                zTree.selectNode(treeNode);
                showRMenu("root", event.clientX, event.clientY, zTree.Lock == null ? treeNode.Lock : zTree.Lock, treeNode.IsFile, treeNode.FID);
            }
            else if (treeNode && !treeNode.noR) {
                zTree.selectNode(treeNode);
                showRMenu("node", event.clientX, event.clientY, zTree.Lock == null ? treeNode.Lock : zTree.Lock, treeNode.IsFile, treeNode.FID);
            }
        }
        function showRMenu(type, x, y, lock, File, id) {
            var hdfFoldIDvalue = $get('<%=hdfFoldID.ClientID %>');
            var ulrg = $get('<%=ulRiMenu.ClientID %>')
            var log = document.getElementById('ctl00_ContentPlaceHolder1_Label1');
            $.ajax({
                type: "POST",
                url: "M_Ledger.aspx/BingMenu",
                data: "{'FDID': '" + id + "','Type':'" + type + "','Lock':'" + lock + "','File':'" + File + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "jsondata",
                async: "true",
                success: function (response) {
                    ulrg.innerHTML = response.d;
                    rMenu = $("#rMenu");
                },
                error: function (response) {
                    alert(response.status + ' ' + response.statusText);
                }
            });
            $("#rMenu ul").show();
            if (1 == 1) {
                $("#m_copy").show();
                $("#m_paste").show();
            }
            rMenu.css({ "top": y + "px", "left": x + "px", "visibility": "visible" });
            $("body").bind("mousedown", onBodyMouseDown);
        }
        function hideRMenu() {
            if (rMenu) rMenu.css({ "visibility": "hidden" });
            $("body").unbind("mousedown", onBodyMouseDown);
        }
        function onBodyMouseDown(event) {
            if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length > 0)) {
                rMenu.css({ "visibility": "hidden" });
            }
        }
        function updateChildpnode(pnode, flag) {
            if (pnode.children != null)
                for (var uj = 0; uj < pnode.children.length; uj++) {
                    if (flag == 1) {
                        zTree.Lock = 1;
                        zTree.setting.view.fontCss["color"] = "blue";
                    }
                    else {
                        zTree.Lock = 0;
                        zTree.setting.view.fontCss["color"] = "red";
                    }
                    zTree.updateNode(pnode.children[uj]);
                    if (pnode.children[uj].children != null) {
                        updateChildpnode(pnode.children[uj]);
                    }
                }
        }
        function getFont(treeId, node) {
            return node.font ? node.font : {};
        }
        var log, className = "dark";
        function beforeClick(treeId, treeNode, clickFlag) {
            className = (className === "dark" ? "" : "dark");
            return (treeNode.click != false);
        }
        function onClick(event, treeId, treeNode, clickFlag) {
        }
        var zTree, rMenu;
        function BindMaintreee() {
            debugger;
            var hdfFoldIDvalue = $get('<%=hdfFoldID.ClientID %>');
            $.ajax({
                type: "POST",
                url: "M_Ledger.aspx/getData",
                data: "{'FDID': '" + hdfFoldIDvalue.value + "'}",
                contentType: "application/json; charset=utf-8",
                datatype: "jsondata",
                async: "true",
                success: function (response) {
                    var msg = eval('(' + response.d + ')');
                    $.fn.zTree.init($("#treeDemo"), setting, msg);
                    zTree = $.fn.zTree.getZTreeObj("treeDemo");
                    rMenu = $("#rMenu");
                },
                error: function (response) {
                    alert(response.status + ' ' + response.statusText);
                }
            });
            $("#m_cut").bind("click", cut);
            $("#m_paste").bind("click", paste);
        }
        $(document).ready(function () {
            BindMaintreee();
        });
        function HideParentLedger() {
           debugger
            var ParentLedger = $get('<%=divParentLedger.ClientID %>');
            var rdbTypeOfLedger = $get("ContentPlaceHolder1_rdbTypeOfLedger_2");
            if (rdbTypeOfLedger.checked === true) {
                ParentLedger.style.visibility = 'hidden';
                ValidatorEnable(document.getElementById('<%= RFV_txtParentLedgerGrp.ClientID %>'), false);
               
            } else {
                ParentLedger.style.visibility = 'visible';
                 ValidatorEnable(document.getElementById('<%= RFV_txtParentLedgerGrp.ClientID %>'),true );
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>--%>
        <div class="col-lg-3">
            <div class="panel">
                <div class="panel-body">
                    <asp:HiddenField ID="hdfFoldID" runat="server" />
                    <asp:HiddenField ID="hdfFolderView" runat="server" />
                    <ul id="treeDemo" class="ztree"></ul>
                </div>
                <div id="rMenu" class="animated bounceIn">
                    <ul id="ulRiMenu" runat="server" class="animated bounceIn">
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="panel">
                <div class="panel-body">
                    <div class="pull-right">
                        <br />
                        <asp:LinkButton ID="btnSave" runat="server" ValidationGroup="save" OnClick="btnSave_Click"><i class="fa fa-check  fa-2x text-success"></i></asp:LinkButton>
                        <asp:HiddenField ID="hdfID" runat="server" Value="" />
                        <asp:LinkButton ID="btnCancel" runat="server" OnClick="btnCancel_Click"><i class="fa fa-close fa-2x"></i></asp:LinkButton>
                        <asp:LinkButton ID="btnApprove" runat="server" OnClick="btnApprove_Click"><i class="fa fa-thumbs-o-up fa-2x"></i></asp:LinkButton>
                    </div>
                    <h3>Ledger Detail</h3>
                    <hr />
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:RadioButtonList runat="server" ID="rdbTypeOfLedger" RepeatLayout="Table" onclick="HideParentLedger()" RepeatDirection="Horizontal" RepeatColumns="3">
                            <asp:ListItem Value="3" Text="General Ledger" Selected="True"> </asp:ListItem>
                            <asp:ListItem Value="2" Text="Ledger Group"> </asp:ListItem>
                            <asp:ListItem Value="1" Text="Control Account"> </asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-lg-3">
                            <asp:Label ID="lblAccountNo" runat="server" CssClass="bold" Text="Account No"></asp:Label>
                            <asp:Label ID="lblAccountNoMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                            <asp:RequiredFieldValidator ID="RFV_txtAccountNo" runat="server" ControlToValidate="txtAccountNo" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage=" (Enter Account No)" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtAccountNo" TabIndex="1" runat="server" MaxLength="50" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                        </div>
                        <div class="col-lg-3" style="display: none">
                            <asp:Label ID="lblLedgerCode" runat="server" CssClass="bold" Text="Ledger Code"></asp:Label>
                            <asp:TextBox ID="txtLedgerCode" TabIndex="1" runat="server" MaxLength="50" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblLedgerName" runat="server" CssClass="bold " Text="Ledger Name"></asp:Label>
                            <asp:Label ID="lblStar" runat="server" CssClass=" bold text-danger" Text=" * ">
                                <asp:RequiredFieldValidator ID="RFV_txtLedgerName" runat="server" ControlToValidate="txtLedgerName" CssClass="text-danger"
                                    Display="Dynamic" ErrorMessage=" (Enter Ledger Name)" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            </asp:Label>
                            <asp:TextBox ID="txtLedgerName" TabIndex="1" runat="server" MaxLength="50" AutoCompleteType="None" autocomplete="Off" ValidationGroup="save"
                                CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblLedgerType" CssClass="bold " runat="server" Text="Ledger Type"> </asp:Label>
                            <asp:Label ID="lblLedgerTypeMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                            <asp:RequiredFieldValidator ID="RFV_ddlLedgerType" runat="server" ControlToValidate="ddlLedgerType" CssClass="text-danger" InitialValue="0"
                                Display="Dynamic" ErrorMessage="" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlLedgerType" TabIndex="1" runat="server" CssClass="form-control input-sm">
                                <asp:ListItem Value="-1">Select Ledger Type</asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div class="col-lg-3">
                            <asp:Label ID="lblAccountType" CssClass="bold " runat="server" Text="Account Type"> </asp:Label>
                            <asp:Label ID="lblAccountTypeMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                            <asp:RequiredFieldValidator ID="RFV_ddlAccountType" runat="server" ControlToValidate="ddlAccountType" CssClass="text-danger" InitialValue="0"
                                Display="Dynamic" ErrorMessage=" (Select P&L Category)" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlAccountType" TabIndex="1" runat="server" CssClass="form-control input-sm">
                                <asp:ListItem Value="-1">Select Account Type</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-3" runat="server" id="divParentLedger">
                            <asp:Label ID="lblParentLedgerGrp" CssClass="bold " runat="server" Text="Parent Ledger Group"> </asp:Label>
                            <div class="input-group">
                                 <asp:RequiredFieldValidator ID="RFV_txtParentLedgerGrp" runat="server" ControlToValidate="txtParentLedgerGrp" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage="" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>

                                <asp:TextBox ID="txtParentLedgerGrp" TabIndex="1" placeholder="Select Parent Ledger Group" runat="server"
                                    AutoCompleteType="None" autocomplete="Off" ValidationGroup="save" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                <asp:HiddenField runat="server" ID="hdfParentLedgerGrpName" />
                                <asp:HiddenField runat="server" ID="hdfParentLedgerGrpID" Value="0" />
                            </div>
                            <asp:AutoCompleteExtender ID="Ace_txtParentLedgerGrp" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx"
                                OnClientItemSelected="AutoCompleteSearch" ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtParentLedgerGrp">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class="col-lg-3">
                            <br />
                            <asp:Label ID="lblActive" runat="server" Text="Active/InActive" CssClass="bold "></asp:Label>
                            <div class="material-switch pull-right">
                                <asp:CheckBox runat="server" ID="chkActive" TabIndex="1" Checked="true" />
                                <label for='<%=chkActive.ClientID %>' class="label-primary"></label>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <asp:Label ID="lblLedgerDesc" runat="server" CssClass="bold" Text="Description"></asp:Label>
                            <asp:TextBox ID="txtDescription" TabIndex="1" runat="server" MaxLength="2000" TextMode="MultiLine" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <%-- </ContentTemplate>
            </asp:UpdatePanel>--%>
    </div>
</asp:Content>
