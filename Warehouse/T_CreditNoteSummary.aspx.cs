﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to display all Company lists.
*/
#endregion
#region Using
using System;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.UI;
using AppUtility;
#endregion
public partial class T_CreditNoteSummary : BigSunPage
{
    #region PageLoad
    AppCreditNoteColl objCN;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtVoucherDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            BindData();
        }
    }
    #endregion
    #region Function   
    protected void BindData()
    {
        AppObjects.AppCreditNoteColl objColl = new AppCreditNoteColl(intRequestingUserID);
        if (txtCreditNoteNo.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.CreditNote.CreditNoteNo, AppUtility.Operators.Equals, txtCreditNoteNo.Text, 0);
        }
        if (txtCreditNoteAmount.Text != "")
        {
            switch (ddlOperators.SelectedValue)
            {
                case "1":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.CreditNote.ExchangeRate, AppUtility.Operators.Equals, txtCreditNoteAmount.Text, 0);
                    break;
                case "2":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.CreditNote.ExchangeRate, AppUtility.Operators.GreaterOrEqualTo, txtCreditNoteAmount.Text, 0);
                    break;
                case "3":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.CreditNote.ExchangeRate, AppUtility.Operators.LessOrEqualTo, txtCreditNoteAmount.Text, 0);
                    break;
                case "4":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.CreditNote.ExchangeRate, AppUtility.Operators.GreaterThan, txtCreditNoteAmount.Text, 0);
                    break;
                case "5":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.CreditNote.ExchangeRate, AppUtility.Operators.LessThan, txtCreditNoteAmount.Text, 0);
                    break;
            }
        }
        if (!string.IsNullOrEmpty(txtVoucherFromDate.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.CreditNote.CreditNoteDate, AppUtility.Operators.Equals,new AppUtility.AppConvert(txtVoucherFromDate.Text), 0);
        }
        if (!string.IsNullOrEmpty(txtVoucherToDate.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.CreditNote.CreditNoteDate, AppUtility.Operators.Equals, new AppUtility.AppConvert(txtVoucherToDate.Text), 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
            objColl.AddCriteria(AppObjects.CreditNote.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.CreditNoteSummary] = objColl;
        grdCreditNoteSummary.DataSource = objColl;
        grdCreditNoteSummary.DataBind();
    }
    public void CancelCreditNote(AppCreditNote ObjCreditNote)
    {
        AppCreditNote ObjDuplicateCreditNote = ObjCreditNote;
        ObjDuplicateCreditNote.ID = 0;
        ObjDuplicateCreditNote.VoucherType_ID = 34; //Credit Voucher Cancel
        ObjDuplicateCreditNote.ModifiedBy = intRequestingUserID;
        ObjDuplicateCreditNote.ModifiedOn = System.DateTime.Now;
        ObjDuplicateCreditNote.Status = new AppConvert((int)RecordStatus.Cancelled);

        if (ObjDuplicateCreditNote.CreditNoteDetailColl.Count > 0)
        {
            int TotalCount = ObjDuplicateCreditNote.CreditNoteDetailColl.Count;
            for (int i = 0; i < TotalCount; i++)
            {
                ObjDuplicateCreditNote.CreditNoteDetailColl[i].ID = 0;
                if (ObjDuplicateCreditNote.CreditNoteDetailColl[i].DebitAmount > 0)
                {
                    ObjDuplicateCreditNote.CreditNoteDetailColl[i].CreditAmount = ObjDuplicateCreditNote.CreditNoteDetailColl[i].DebitAmount;
                    ObjDuplicateCreditNote.CreditNoteDetailColl[i].DebitAmount = 0;
                }
                else
                {
                    ObjDuplicateCreditNote.CreditNoteDetailColl[i].DebitAmount = ObjDuplicateCreditNote.CreditNoteDetailColl[i].CreditAmount;
                    ObjDuplicateCreditNote.CreditNoteDetailColl[i].CreditAmount = 0;
                }
                ObjDuplicateCreditNote.CreditNoteDetailColl[i].ModifiedBy = intRequestingUserID;
                ObjDuplicateCreditNote.CreditNoteDetailColl[i].ModifiedOn = System.DateTime.Now;
                ObjDuplicateCreditNote.CreditNoteDetailColl[i].Status = new AppConvert((int)RecordStatus.Cancelled);
            }
        }
        ObjDuplicateCreditNote.Save();

        #region Add Ledger Entry


        if (ObjDuplicateCreditNote.CreditNoteDetailColl.Count > 0)
        {
            foreach (var item in ObjDuplicateCreditNote.CreditNoteDetailColl)
            {
                if (item.CreditAmount != 0)
                {
                    LedgerEntry(LoginCompanyID, 100802, 1008, item.Ledger_ID, item.ID, ObjDuplicateCreditNote.CreditNoteDate, item.CreditAmount, 0, ObjDuplicateCreditNote.CreditNoteNo, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicateCreditNote.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, ObjDuplicateCreditNote.VoucherType_ID, item.CreditNoteDesc);
                }
                else
                {
                    LedgerEntry(LoginCompanyID, 100801, 1008, item.Ledger_ID, item.ID, ObjDuplicateCreditNote.CreditNoteDate, 0, item.DebitAmount, ObjDuplicateCreditNote.CreditNoteNo, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicateCreditNote.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, ObjDuplicateCreditNote.VoucherType_ID, item.CreditNoteDesc);
                }


            }

        }


        #endregion
    }
    #endregion
    #region GridView Events
    protected void grdCreditNoteSummary_PreRender(object sender, EventArgs e)
    {
        if (grdCreditNoteSummary.Rows.Count > 0)
        {
            grdCreditNoteSummary.UseAccessibleHeader = true;
            grdCreditNoteSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region Button Click Events
    protected void btnNewCreditNote_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.ObjCompanySession);
        Response.Redirect("T_CreditNote.aspx");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (Update(grdCreditNoteSummary, new AppConvert((int)RecordStatus.Deleted), 1008, 1008) == 1)
        {
            BindData();
        }
    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        if (Update(grdCreditNoteSummary, new AppConvert((int)RecordStatus.Approve), 1008, 1008) == 1)
        {
            BindData();
        }
    }
    protected void btnCanceled_Click(object sender, EventArgs e)
    {
        AppObjects.AppCreditNoteColl objColl = new AppCreditNoteColl(intRequestingUserID);
        if (txtCancelCNNo.Text != "" && txtVoucherDate.Text != "")
        {
            objColl.AddCriteria(AppObjects.CreditNote.CreditNoteNo, AppUtility.Operators.Equals, txtCancelCNNo.Text);
            objColl.Search(RecordStatus.ALL);

            if (objColl.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Record not found','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                return;
            }
            else
            {
                AppCreditNote objSingleCN = objColl[0];
                if (objColl[0].Status == new AppUtility.AppConvert((int)RecordStatus.Cancelled))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelCNNo.Text + " Transaction alerady cancelled','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelCNNo.Focus();
                    return;
                }
                else if (objColl[0].Status == new AppUtility.AppConvert((int)RecordStatus.Deleted))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelCNNo.Text + " Transaction is deleted','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelCNNo.Focus();
                    return;
                }
                else if (objColl[0].Status == new AppUtility.AppConvert((int)RecordStatus.Created))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelCNNo.Text + " Transaction is not approved','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelCNNo.Focus();
                    return;
                }
                else if (objColl[0].CreditNoteDate > new AppUtility.AppConvert(Convert.ToDateTime(txtVoucherDate.Text)))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelCNNo.Text + " Cancel voucher date should be greater than or equal to voucher date.','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelCNNo.Focus();
                    return;
                }
                objSingleCN.Status = new AppUtility.AppConvert((int)RecordStatus.Cancelled);

                objSingleCN.Save();

                if (objSingleCN.CreditNoteDetailColl.Count > 0)
                {
                    foreach (var item in objSingleCN.CreditNoteDetailColl)
                    {
                        if (item.CreditAmount != 0)
                        {
                            UpdateLedgerEntryStatus(100802, 1008, item.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                        }
                        else
                        {
                            UpdateLedgerEntryStatus(100801, 1008, item.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                        }
                    }
                }
                CancelCreditNote(objSingleCN);
            }
            Session[TableConstants.CreditNoteSummary] = objColl;
            grdCreditNoteSummary.DataSource = objColl;
            grdCreditNoteSummary.DataBind();
            txtCancelCNNo.Text = "";
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Credit voucher cancelled successfully','1'" + ");", true);


        }
        else
        {
            if (txtCancelCNNo.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Credit voucher no.','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                txtCancelCNNo.Focus();
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter voucher date.','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                txtVoucherDate.Focus();
                return;
            }
        }
    }
    #endregion
}