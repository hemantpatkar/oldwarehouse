﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using System.IO;
using System.Drawing;

public partial class StockPreInSearch : BigSunPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockPreInDetail.aspx");
    }
    protected void Search()
    {
        AppPreInwardColl objAppGateEntryColl = new AppPreInwardColl(intRequestingUserID);

        #region DateValidation
        DateTime fromdate, to;
        if (!string.IsNullOrEmpty(txtfromdate.Text.Trim()) && !string.IsNullOrEmpty(txttodate.Text.Trim()))
        {
            fromdate = new AppConvert(txtfromdate.Text);
            to = new AppConvert(txttodate.Text);
            if (fromdate > to)
            {
                grdPreInwardSearch.DataSource = null;
                grdPreInwardSearch.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please Select Validate Date range..!" + "','2'" + ");", true);
                return;
            }
        }

        #endregion

        #region Search

        if (ddlStatus.SelectedValue == "0")
        {
            if (txtfromdate.Text.Trim() != "" && txttodate.Text.Trim() != "" && txtfromdate.Text.Trim() != "__/__/____" && txttodate.Text.Trim() != "__/__/____")
            {
                fromdate = Convert.ToDateTime(txtfromdate.Text);
                to = Convert.ToDateTime(txttodate.Text + " 23:59:59");
                objAppGateEntryColl.AddCriteria(PreInward.PreInwardDate, Operators.GreaterOrEqualTo, fromdate);
                objAppGateEntryColl.AddCriteria(PreInward.PreInwardDate, Operators.LessOrEqualTo, to);
            }
            else if (txtfromdate.Text.Trim() != "" && txtfromdate.Text.Trim() != "__/__/____" && (txttodate.Text.Trim() == "" || txttodate.Text.Trim() == "__/__/____"))
            {
                fromdate = Convert.ToDateTime(txtfromdate.Text);

                objAppGateEntryColl.AddCriteria(PreInward.PreInwardDate, Operators.GreaterOrEqualTo, fromdate);
            }
            else if (txttodate.Text.Trim() != "" && txttodate.Text.Trim() != "__/__/____" && (txtfromdate.Text.Trim() == "" || txtfromdate.Text.Trim() == "__/__/____"))
            {
                to = Convert.ToDateTime(txttodate.Text + " 23:59:59");

                objAppGateEntryColl.AddCriteria(PreInward.PreInwardDate, Operators.LessOrEqualTo, to);
            }
        }
        else if (ddlStatus.SelectedValue == "1")
        {

        }
        else
        {

        }

        if (txtPreInwardNo.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(PreInward.PreInwardNo, Operators.Like, txtPreInwardNo.Text);
        }

        if (txtCustomer.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(PreInward.Customer_Company_ID, Operators.Like, txtCustomer.Text);
        }

        if (txtReceivedDate.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(PreInward.ReceivedDate, Operators.Like, txtReceivedDate.Text);
        }
        //if (txtChallanNo.Text.Trim() != "")
        //{
        //    objAppGateEntryColl.AddCriteria(PreInward.DriverName, Operators.Like, txtChallanNo.Text);
        //}        
        if (ddlStatus.SelectedValue != "-1")
        {
            int status = Convert.ToInt32(ddlStatus.SelectedValue);
            objAppGateEntryColl.AddCriteria(PreInward.Status, Operators.Equals, status); //
        }

        objAppGateEntryColl.Search();
       
        grdPreInwardSearch.DataSource = objAppGateEntryColl;
        grdPreInwardSearch.DataBind();


        #endregion
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Search();
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (grdPreInwardSearch.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Doesnot contain data" + "','2'" + ");", true);
            return;
        }

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grdPreInwardSearch.AllowPaging = false;
            grdPreInwardSearch.HeaderRow.BackColor = Color.White;
            foreach (TableCell cell in grdPreInwardSearch.HeaderRow.Cells)
            {
                cell.BackColor = grdPreInwardSearch.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in grdPreInwardSearch.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = grdPreInwardSearch.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = grdPreInwardSearch.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            grdPreInwardSearch.RenderControl(hw);
            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void grdPreInwardSearch_PreRender(object sender, EventArgs e)
    {

    }

    protected void grdPreInwardSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdPreInwardSearch.PageIndex = e.NewPageIndex;
        Search();
    }
}