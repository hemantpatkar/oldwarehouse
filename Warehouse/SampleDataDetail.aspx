﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Culture="en-gb" Inherits="SampleDataDetail" Codebehind="SampleDataDetail.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<script runat="server">


</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function checkedAllSearch() {
            var but = document.getElementById("SelectAll");
            var TargetBaseControl = document.getElementById('<%= this.gvSearch.ClientID %>');
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var icount = 0; icount < Inputs.length; ++icount) {
                if (Inputs[icount].type == 'checkbox') {
                    if (Inputs[icount].disabled == false) {
                        if (but.checked == true) {
                            Inputs[icount].checked = true;
                        }
                        else {

                            Inputs[icount].checked = false;
                        }
                    }
                    else {
                        Inputs[icount].checked = false;
                    }
                }
            }
        }

        function PartnerGroupCodeSearch(sender, eventArgs) {

            var hdpartID = $get('<%= hdfPartnerGroupCodeID.ClientID %>');
            var lblPartname = $get('<%= hdfPartnerGroupCodeName.ClientID %>');
            var partycode = $get('<%= txtPartnerGroupCode.ClientID %>');

            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=ACEtxtPartnerCode.ClientID %>').set_contextKey("3," + eventArgs.get_value());
            }
        }


        function ClearPartnerGroupCodeSearch() {
            var OrignalProfitCenter = $get('<%=txtPartnerGroupCode.ClientID %>');
            var HidenProfitCenter = $get('<%= hdfPartnerGroupCodeName.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=hdfPartnerGroupCodeID.ClientID %>').value = "";
                $get('<%=hdfPartnerGroupCodeName.ClientID %>').value = "";
                $get('<%=txtPartnerGroupCode.ClientID %>').value = "";
                $find('<%=ACEtxtPartnerCode.ClientID %>').set_contextKey("0");
            }
        }




        function PartnerCodeSearch(sender, eventArgs) {
            var hdpartID = $get('<%= hdfPartnerCodeID.ClientID %>');
            var lblPartname = $get('<%= hdfPartnerCodeName.ClientID %>');
            var partycode = $get('<%= hdfPartnerCodeID.ClientID %>');

            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
        }


        function ClearPartnerCodeSearch() {
            var OrignalProfitCenter = $get('<%=hdfPartnerCodeID.ClientID %>');
            var HidenProfitCenter = $get('<%= hdfPartnerCodeName.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=hdfPartnerCodeID.ClientID %>').value = "";
                $get('<%=hdfPartnerCodeName.ClientID %>').value = "";
                $get('<%=txtPartnerCode.ClientID %>').value = "";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdateProgress ID="UpdateProgress1" DisplayAfter="0" AssociatedUpdatePanelID="UpdatePanel1"
        runat="server">
        <ProgressTemplate>
            <div class="modal fade in" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-keyboard="false" style="display: block">
                <div class="modal-dialog  modal-sm">
                    <h1 style="text-align: center;">
                        <i class="fa fa-truck fa-magic fa-2x faa-passing animated"></i>
                    </h1>
                </div>
            </div>
            <div class="modal-backdrop fade in"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <%--Header Section--%>
            <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <i class="fa fa-shopping-cart"></i>
                        <asp:Label ID="lblSampleDataTitle" Text="Sample Detail" runat="server"></asp:Label>
                        &nbsp;/&nbsp;
                        <asp:Label ID="lblSampleNo" ForeColor="Maroon" runat="server"></asp:Label>
                        <asp:HiddenField ID="hdfSamplingID" runat="server" />
                        <asp:Label ID="lblMsg" runat="server" CssClass="text-danger"></asp:Label>
                        <div class="pull-right">
                            <asp:LinkButton ID="btnDeleteSample" runat="server" ForeColor="Maroon" CausesValidation="False" OnClick="btnDeleteSample_Click" TabIndex="12" Visible="false">
                                  <i class="fa fa-trash"></i>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnBack" runat="server" CausesValidation="False" OnClick="btnBack_Click"
                                TabIndex="12" Text="">
                                  <i class="fa fa-search"></i>
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row btnrow">
                <div class="col-lg-6">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <asp:LinkButton ID="lnkdetail" runat="server"></asp:LinkButton>

                            <asp:HiddenField ID="hdfrevised" runat="server" />
                            <asp:ConfirmButtonExtender ID="cbeDelet" runat="server" ConfirmText="Are you sure?" TargetControlID="btnDelete">
                            </asp:ConfirmButtonExtender>
                        </li>
                        <li>
                            <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank"
                                Visible="False" TabIndex="12"></asp:HyperLink>
                        </li>
                        <li>
                            <asp:LinkButton ID="btnSave" runat="server" OnClick="btnSave_Click" TabIndex="11" Text=""
                                ValidationGroup="SampleData" AccessKey="w"></asp:LinkButton><span></span>

                        </li>
                        <li>
                            <asp:LinkButton ID="btnNewDetail" runat="server" Text="" CausesValidation="false" TabIndex="13"
                                OnClick="btnNewDetail_Click" AccessKey="n" />
                        </li>
                        <li>

                            <asp:LinkButton ID="btnDelete" runat="server" TabIndex="19" CssClass="btn btn-primary btn-sm"
                                Visible="False" OnClientClick="return confirmalert('Do you want to Delete ?' )" />
                        </li>
                        <li>
                            <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CssClass="btn btn-primary btn-sm"
                                TabIndex="160" OnClick="btnhistory_Click" Visible="false" />
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6">
                    <div class="pull-right">
                        <ul class="nav nav-pills nav-wizard">
                            <li class="active"><a href="#" data-toggle="tab">New</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Pending Approval</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Available</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Partially Available</a><div class="nav-arrow"></div>
                            </li>
                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Done</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-1">
                    <asp:HiddenField ID="hdfSDID" runat="server" />
                    <asp:HiddenField ID="tempPtID" runat="server" />
                </div>

            </div>
            <hr />

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2" runat="server">
                            <asp:Label ID="lblGateRegFr" runat="server" Text="Gate Register From" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtGetRegFr" runat="server" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtGetRegFr" runat="server" CompletionInterval="1"
                                CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch" ServiceMethod="GateEntrySearch"
                                ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                TargetControlID="txtGetRegFr">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfGetRegFrID" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdfGetRegFrName" runat="server" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPartnerGrpFr" runat="server" Text="Customer Group" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtPartnerGroupCode" runat="server" onblur="ClearPartnerGroupCodeSearch()" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtPartnerGroupCode" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="10" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerGroupCodeSearch"
                                ServiceMethod="PartnerGroupSearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" ContextKey="0" UseContextKey="true"
                                TargetControlID="txtPartnerGroupCode">
                            </asp:AutoCompleteExtender>
                            <div style="display: none">
                                <asp:HiddenField ID="hdfPartnerGroupCodeID" runat="server" />
                                <asp:HiddenField ID="hdfPartnerGroupCodeName" runat="server"></asp:HiddenField>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblPartnerFr" runat="server" CssClass="bold" Text="Customer"></asp:Label>
                            <asp:Label ID="Label13" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="Requpartner" runat="server" ControlToValidate="txtPartnerCode"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ValidationGroup="SampleData">
                            </asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtPartnerCode" runat="server" onblur="ClearPartnerCodeSearch()" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="ACEtxtPartnerCode" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="10" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerCodeSearch"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                UseContextKey="true" ContextKey="0" TargetControlID="txtPartnerCode">
                            </asp:AutoCompleteExtender>
                            <asp:HiddenField ID="hdfPartnerCodeID" runat="server" />
                            <asp:HiddenField ID="hdfPartnerCodeName" runat="server"></asp:HiddenField>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblSampleDate" runat="server" Text="Sameple Date" CssClass="bold"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtSampleDate" runat="server" MaxLength="10"
                                    TabIndex="1" CssClass="form-control input-sm"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                            <asp:MaskedEditExtender ID="txtSampleDate_MaskedEditExtender" runat="server"
                                CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat=""
                                CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                CultureTimePlaceholder="" Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtSampleDate">
                            </asp:MaskedEditExtender>
                            <asp:CalendarExtender ID="txtSampleDate_CalendarExtender" runat="server" Enabled="True"
                                TargetControlID="txtSampleDate">
                            </asp:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ControlToValidate="txtSampleDate"
                                CssClass="text-danger" SetFocusOnError="true" Display="Dynamic"
                                ValidationGroup="SampleData">
                            </asp:RequiredFieldValidator>
                        </div>
                        <div class="col-lg-4">
                            <asp:Label ID="lblRemarks" Text="Remarks" runat="server" CssClass="bold"></asp:Label>
                            <asp:TextBox ID="txtRemarks" runat="server" MaxLength="500" CssClass="form-control input-sm" TabIndex="1"></asp:TextBox>
                            <asp:HiddenField ID="hdf" runat="server" />
                        </div>
                    </div>
                </div>
            </div>

            <%--Add Section--%>
            <div class="newline"></div>
            <div class="row">
                <div class="col-lg-12">
                    <asp:GridView ID="grdSamplingDetails" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-striped text-nowrap table-Full" GridLines="None"
                        OnPreRender="grdSamplingDetails_PreRender" OnRowDataBound="grdSamplingDetails_RowDataBound" OnRowCommand="grdSamplingDetails_RowCommand">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnDeleteItem" ForeColor="Maroon" OnClientClick="return confirmalert('Do you want to Delete ?' )" runat="server" CommandName="Del" CommandArgument='<%# Eval("ID") %>' CausesValidation="False" TabIndex="1">
                                                <i class="fa fa-close fa-2x"></i>
                                    </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblSrno" runat="server" Text="<%# Convert.ToInt32(Container.DataItemIndex)+1%>" Visible="true"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lot No">
                                <ItemTemplate>
                                    <asp:Label ID="LotNoInternal" runat="server" Text='<%#Eval("LotInternal") %>' ToolTip='<%#Eval("LotInternal")%>'></asp:Label>
                                    <asp:Label ID="lblLotVersion" runat="server" Text='<%#Eval("LotVersion") %>' ToolTip='<%#Eval("LotVersion")%>'></asp:Label>
                                    <asp:HiddenField ID="hdfSamplingDetailID" runat="server" Value='<%#Eval("ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item">
                                <ItemTemplate>
                                    <asp:Label ID="lblStorageItemName" runat="server" Text='<%#Eval("Item.Name") %>' ToolTip='<%#Eval("Item.Name")%>'></asp:Label>
                                    <asp:HiddenField ID="hdfStorageItemID" runat="server" Value='<%#Eval("Item_ID")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Variety/Count">
                                <ItemTemplate>
                                    <asp:Label ID="lblVariety" Text='<%# Eval("InwardDetail.Variety.Name") %>' ForeColor="#3399ff" ToolTip='<%# Eval("InwardDetail.Variety.Name") %>' runat="server" />
                                    <asp:HiddenField ID="hdfVarietyID" Value='<%# Eval("InwardDetail.Variety_ID") %>' runat="server" />
                                    <br />
                                    <asp:Label ID="lblCount" Text='<%# Eval("InwardDetail.Count") %>' ForeColor="Maroon" ToolTip='<%# Eval("InwardDetail.Count") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Qty Opened">
                                <ItemTemplate>
                                    <asp:TextBox ID="StorageItemIndQuantityOpened" runat="server" Text='<%#Eval("QuantityOpened") %>'
                                        ToolTip='<%#Eval("QuantityOpened") %>' MaxLength="5" TabIndex="2" Width="100px" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="StorageItemIndQuantityOpened_FilteredTextBoxExtender"
                                        runat="server" Enabled="True" FilterType="Numbers,Custom" ValidChars="." TargetControlID="StorageItemIndQuantityOpened">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="RFVStorageItemIndQuantityOpened" runat="server" ControlToValidate="StorageItemIndQuantityOpened" CssClass="text-danger" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Invalid Qty" ValidationGroup="SampleData"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Qty Taken">
                                <ItemTemplate>
                                    <asp:TextBox ID="StorageItemIndQuantityTaken" runat="server" Text='<%#Eval("QuantityTaken") %>'
                                        ToolTip='<%#Eval("QuantityTaken") %>' MaxLength="5" TabIndex="2" Width="100px" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="StorageItemIndQuantityTaken_FilteredTextBoxExtender" runat="server" Enabled="True"
                                        FilterType="Numbers,Custom" ValidChars="." TargetControlID="StorageItemIndQuantityTaken">
                                    </asp:FilteredTextBoxExtender>
                                    <asp:RequiredFieldValidator ID="RFVStorageItemIndQuantityTaken" runat="server" ControlToValidate="StorageItemIndQuantityTaken" CssClass="text-danger" SetFocusOnError="true" Display="Dynamic" ErrorMessage="Invalid Qty" ValidationGroup="SampleData"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UOM">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlStorageItemIndQuantityTakenUOM" runat="server" TabIndex="2" CssClass="form-control input-sm"></asp:DropDownList>
                                    <asp:HiddenField ID="hdfdropdown" runat="server" Value='<%#Eval("QuantityUOM") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>


            <%--Search parameter panel--%>
            <div class="newline"></div>
            <div class="panel" runat="server" id="searchlot" style="display: none">
                <div class="panel-heading btn-primary">
                    <div class="row">
                    </div>

                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">

                            <asp:TextBox ID="UsrtxtPartnerGroupCode" onblur="ClearPartnerGroupSearch()" TabIndex="1" placeholder="Customer Group"
                                runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="UsrtxtPartnerGroupCode_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerGroupSearch"
                                ServiceMethod="PartnerGroupSearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" ContextKey="0" UseContextKey="true"
                                TargetControlID="UsrtxtPartnerGroupCode">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:HiddenField ID="UsrhdfPartnerGroup" runat="server" />
                                <asp:Label ID="UsrlblPartnerGroup" runat="server"></asp:Label>

                                <asp:TextBox ID="txthideMo" runat="server"></asp:TextBox>
                                <asp:HiddenField ID="hdfmono" runat="server" />
                            </div>
                        </div>
                        <div class="col-lg-2">

                            <asp:TextBox ID="UsrtxtPartnerCode" onblur="ClearPartnerSearch()" TabIndex="2" runat="server" placeholder="Customer"
                                AccessKey="p" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="UsrtxtPartnerCode_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerSearch"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                UseContextKey="true" ContextKey="0" TargetControlID="UsrtxtPartnerCode">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:HiddenField ID="UsrhdfPartner" runat="server" />
                                <asp:HiddenField ID="UserhdfPartner1" runat="server" />
                                <asp:Label ID="UsrlblPartner" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtLotSearch" placeholder="Lot No" CssClass="form-control input-sm" MaxLength="10" runat="server"
                                TabIndex="4"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftblot" runat="server" FilterType="Numbers" TargetControlID="txtLotSearch">
                            </asp:FilteredTextBoxExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtReceivedQuantitySearch" TabIndex="4" MaxLength="20" placeholder="Received Quantity" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtItemGroup" TabIndex="4" MaxLength="20" placeholder="Item Group"
                                runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtItemGroup_AutoCompleteExtender" runat="server" EnableCaching="false"
                                TargetControlID="txtItemGroup" CompletionInterval="1" CompletionSetCount="10"
                                UseContextKey="true" ContextKey="0" DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1"
                                ServiceMethod="ItemCategory" FirstRowSelected="false" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" CompletionListElementID="divwidth"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListCssClass="AutoExtender">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtStorageItemName" TabIndex="4" MaxLength="20" placeholder="Item Name"
                                runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtLocSearch_AutoCompleteExtender" runat="server" EnableCaching="false"
                                TargetControlID="txtStorageItemName" CompletionInterval="1" CompletionSetCount="10" ContextKey="0" UseContextKey="true"
                                DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="ItemSearch"
                                FirstRowSelected="false" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListCssClass="AutoExtender">
                            </asp:AutoCompleteExtender>
                        </div>

                        <div class="col-lg-2">
                            <asp:TextBox ID="txtpackingrefSearch" placeholder="Packing Reference" TabIndex="4" MaxLength="20" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtMarkSearch" placeholder="Item Mark" TabIndex="4" MaxLength="20" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtVakkalSearch" placeholder="Vakkal" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtSchemeSearch" placeholder="Scheme" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtBrand" placeholder="Brand" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtOrigin" placeholder="Origin" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <asp:Button ID="btnLotSearch" runat="server" OnClick="btnLotSearch_Click" TabIndex="19" CssClass="btn btn-primary btn-sm btn-block" Text="Search" />
                        </div>
                        <div class="col-lg-2">
                            <asp:Button ID="btnAddItems" runat="server" CausesValidation="False" CssClass="btn btn-primary btn-sm btn-block"
                                OnClick="btnAddItems_Click" TabIndex="19" Text="" />
                        </div>
                        <div class="col-lg-2">

                            <label class="btn btn-primary btn-sm  btn-block" id="divIssue" runat="server">
                                <asp:CheckBox ID="chkZero" runat="server" TabIndex="2" />
                                <asp:Label ID="lblchkZero" runat="server" CssClass="bold" />
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <label class="btn btn-primary btn-sm  btn-block" id="divNewRequirement" runat="server">
                                <asp:CheckBox ID="chkFyear" runat="server" TabIndex="2" />
                                <asp:Label ID="lblchkFyear" runat="server" CssClass="bold" />
                            </label>
                        </div>
                        <div class="col-lg-2">
                            <asp:Button ID="btnDeleteItem" runat="server" OnClick="btnDeleteItem_Click" TabIndex="19" Visible="False"
                                OnClientClick="return confirmalert('Do you want to Delete ?' )" CssClass="btn btn-danger btn-sm" />
                        </div>
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-lg-12" style="overflow: auto">
                            <asp:GridView ID="gvSearch" runat="server" AutoGenerateColumns="False" PagerStyle-CssClass="pagination-ys"
                                AllowPaging="True" CssClass="table table-hover  table-striped text-nowrap" PageSize="30">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <input id="SelectAll" onclick="checkedAllSearch();" type="checkbox" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%--disabled='<%# Convert.ToInt32(Eval("NetQuantity")) > 0 ? false : true  %>'--%>
                                            <input id="CheckBoxSelect" name="CheckBoxSelect" onkeypress="clickButton(event,'<%# ((GridViewRow)Container).FindControl("lnkAdditem").ClientID %>')" type="checkbox" tabindex="4" value='<%#Eval("InwardDetail_ID") %>' />
                                            <div style="display: none;">
                                                <asp:LinkButton ID="lnkAdditem" runat="server" CommandName="Sel" CommandArgument='<%#Eval("InwardDetail_ID") %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblSrNo" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSrno" runat="server" Text="<%# Convert.ToInt32(Container.DataItemIndex)+1%>" Visible="true"></asp:Label>

                                            <asp:HiddenField ID="hdfInwardDetailID" runat="server" Value='<%#Eval("InwardDetail_ID") %>' />
                                            <asp:HiddenField ID="hdfMaterialOrderID" runat="server" Value="" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblLotNo" Text="LotNo" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLotNo" Text='<%#Eval("LotInternal") %>' CommandArgument='<%# Eval("InwardDetail_ID")%>'
                                                CommandName="Detail" ToolTip='<%#Eval("LotInternal")%>' runat="server"></asp:LinkButton>
                                            <asp:LinkButton ID="lblLotVersion" Text='<%#Eval("LotVersion") %>' CommandArgument='<%# Eval("InwardDetail_ID")%>'
                                                CommandName="Detail" ToolTip='<%#Eval("LotVersion")%>' runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblParty" Text="Party Name" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkpartner" Text='<%#Eval("CustomerName") %>' CommandArgument='<%# Eval("Customer_Company_ID")%>'
                                                CommandName="Customer" ToolTip='<%#Eval("CustomerName")%>' runat="server"></asp:LinkButton>
                                            <asp:HiddenField ID="hdfPartnerID" runat="server" Value='<%#Eval("Customer_Company_ID") %>' />
                                            <br />
                                            <asp:Label ID="lblCustomer" Text='<%#Eval("CustomerGroupName")%>' ForeColor="Maroon" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblItem" Text="Item" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItem" runat="server" Text='<%#Eval("ItemName") %>'></asp:Label>
                                            <asp:HiddenField runat="server" ID="hdfStorageItemID" Value='<%#Eval("Item_ID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblMark" Text="Item Mark" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMark" runat="server" Text='<%# Eval("ItemMark") %>'
                                                ToolTip='<%# Eval("ItemMark") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblPackingRef" Text="Packing Ref" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPackingReference" Text='<%# Eval("PackingReference") %>' ToolTip='<%# Eval("PackingReference") %>'
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblVakkal" Text="Vakkal" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVakkal" runat="server" Text='<%#Eval("LotCustomer")  %>'
                                                ToolTip='<%#Eval("LotCustomer") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblPalleteNo" Text="Pallete No" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPallete" runat="server" Text='<%# Eval("PalletNo") %>'
                                                ToolTip='<%#Eval("PalletNo")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblhdCount" Text="Variety/Count" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVariety" Text='<%# Eval("VarietyName") %>' ToolTip='<%# Eval("VarietyName") %>' runat="server" />
                                            <asp:HiddenField ID="hdfVarietyID" Value='<%# Eval("Variety_ID") %>' runat="server" />
                                            <br />
                                            <asp:Label ID="lblCount" Text='<%# Eval("Count") %>' ToolTip='<%# Eval("Count") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblhdBrand" Text="Brand/Origin" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBrand" Text='<%# Eval("BrandName") %>' ToolTip='<%# Eval("BrandName") %>' runat="server" />
                                            <asp:HiddenField ID="hdfBrandID" Value='<%# Eval("Brand_ID") %>' runat="server" />
                                            <br />
                                            <asp:Label ID="lblorigin" Text='<%# Eval("OriginName") %>' ToolTip='<%# Eval("OriginName") %>' runat="server" />
                                            <asp:HiddenField ID="hdfOriginID" Value='<%# Eval("Origin_ID") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblStockQty" Text="Stock Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblStockQuantity" runat="server" Text='<%#Eval("StockQuantity") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblOrderQty" Text="Order Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrderQuantity" runat="server" Text='<%#Eval("OrderQuantity") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblNetQty" Text="Net Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetQuantity" runat="server" Text='<% #Eval("NetQuantity")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblScheme" Text="Scheme/Location" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblScheme" runat="server" Text='<%#Eval("SchemeCode")  %>' ForeColor="#3399ff"></asp:Label>
                                            <asp:HiddenField runat="server" ID="hdfSchemeID" Value='<%#Eval("StorageScheme_ID")  %>' />
                                            <br />
                                            <asp:Label ID="lblLocationNo" runat="server" Text='<%#Eval("LocationName") %>' ForeColor="Maroon"
                                                ToolTip='<%#Eval("LocationName")%>'></asp:Label>
                                            <asp:HiddenField runat="server" ID="hdfStorageLocationID" Value='<%#Eval("StorageLocation_ID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>

            <%--Search Grid on last Scection--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
