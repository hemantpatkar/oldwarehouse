﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="M_ProfitCenter" Codebehind="M_ProfitCenter.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class=" col-lg-12 ">
            <div id="divButtons" class="pull-right">
                <br />
                <div class="btn-group">
                    <div class=" col-lg-3">
                        <asp:TextBox ID="txtProfitCenterName" runat="server" placeholder="Profit Center Name" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                    </div>
                    <div class=" col-lg-3">
                        <asp:TextBox ID="txtProfitCenterCode" runat="server" placeholder="Profit Center Code" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                    </div>
                    <div class=" col-lg-3">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control  input-sm">
                            <asp:ListItem Text="ALL" Value="-32768"></asp:ListItem>
                            <asp:ListItem Text="Approved" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Created" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Deleted" Value="-10"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class=" col-lg-3">
                        <asp:Button runat="server" Text="Search" CssClass="btn btn-sm  btn-primary" ID="btnSearch" OnClick="btnSearch_Click"></asp:Button>
                    </div>

                </div>
            </div>
            <h3>Profit Center Details</h3>
            <hr />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdProfitCenter" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Horizontal"
                        CssClass="table table-hover table-striped text-nowrap nowrap"
                        OnPreRender="grdProfitCenter_PreRender"
                        OnRowCommand="grdProfitCenter_RowCommand" OnRowEditing="grdProfitCenter_RowEditing" OnRowDeleting="grdProfitCenter_RowDeleting"
                        OnRowUpdating="grdProfitCenter_RowUpdating">
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="ADDNEW"
                                        Text="Add New" ToolTip="Add New" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CssClass=" fa fa-edit fa-2x" CommandName="Edit"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnDelete" Visible="false" runat="server" CssClass="fa fa-trash fa-2x" CommandName="Delete"
                                        Text="" ToolTip="Delete" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="EditProfitCenter"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="CANCELEDIT" CssClass="fa fa-ban fa-2x" toot="Cancel"
                                        Text="" />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x" ToolTip="Save" ValidationGroup="AddProfitCenter"
                                        Text="" UseSubmitBehavior="False" />
                                    <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban  fa-2x" Text="" />
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrProfitCenterCode" Text="Code" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblProfitCenterCode" runat="server" TabIndex="1" Text='<%#Eval("ProfitCenterCode") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                      <asp:HiddenField runat="server" ID="hdfID" Value='<%# Eval("ID") %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfProfitCenterID" Value='<%# Eval("ID") %>' />
                                    <asp:TextBox ID="txtProfitCenterCode" runat="server" Text='<%# Eval("ProfitCenterCode") %>' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtProfitCenterCode" runat="server" ControlToValidate="txtProfitCenterCode"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditProfitCenter"></asp:RequiredFieldValidator>

                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewProfitCenterCode" runat="server" Text='' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtNewProfitCenterCode" runat="server" ControlToValidate="txtNewProfitCenterCode"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddProfitCenter"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrProfitCenterName" Text="Name" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblProfitCenterName" runat="server" TabIndex="1" Text='<%#Eval("ProfitCenterName") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtProfitCenterName" runat="server" Text='<%# Eval("ProfitCenterName") %>' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtProfitCenterName" runat="server" ControlToValidate="txtProfitCenterName"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditProfitCenter"></asp:RequiredFieldValidator>

                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewProfitCenterName" runat="server" Text='' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtNewProfitCenterName" runat="server" ControlToValidate="txtNewProfitCenterName"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddProfitCenter"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" runat="server" Text='<%#(Eval("StatusName")) %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsActiveProfitCenter" CssClass="form-control" TabIndex="8" Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkActiveProfitCenter" CssClass="form-control" TabIndex="8" Checked="true" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Default" Text="Default" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDefault" runat="server" Text='<%#(Eval("Type").ToString()=="1"?"Y":"N") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsDefaultProfitCenter" TabIndex="9" Checked='<%#Eval("Type").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkDefaultProfitCenter" TabIndex="9" />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdProfitCenter" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
