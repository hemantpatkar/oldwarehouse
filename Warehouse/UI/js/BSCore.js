﻿function GridUI(GridID, ShowLastRecords) {
    //debugger
    var responsiveHelper_dt_basic = undefined;
    var breakpointDefinition = {
        tablet: 1024,
        phone: 480
    };

    if (ShowLastRecords == 0)
        ShowLastRecords = 10
    if (GridID != null) {
        $('#' + GridID.id).dataTable({
            "dom": "<'dt-toolbar'<'col-lg-6'l><'col-lg-6'f>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-lg-6'i><'col-lg-6'p>>",
            "autoWidth": true,
            "Destroy": true
        });
    }
}

function ShowAnimatedMessage(message, type) {
    $.notify('<strong><i class="fa fa-' + (type == "1" ? 'thumbs-o-up' : 'bell') + ' iconsize-6 animated bounce"></i>&nbsp;' + message + '</strong> ', {
        // settings
        element: 'body',
        type: type == "1" ? "success" : "danger",
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: true,
        placement: {
            from: "top",
            align: "right"
        },
        offset: { x: "14", y: "95" },
        spacing: 10,
        z_index: 999999,
        delay: type == "1" ? 2000 : 10000,
        timer: 2000,
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        icon_type: 'class',
        template: '<div data-notify="container" class="col-lg-3 alert alert-{0}" role="alert">' +
                   '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">x</button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
    });
    return;
}

function clickButton(e, buttonid) {
    var evt = (e || window.event);
    var keyCode = (evt.keyCode || evt.which);
    var bt = document.getElementById(buttonid);
    if (bt) {
        if (keyCode == 13) {
            bt.click();
            return false;
        }
    }
}


// Check if entered key is number
// To use this function just add the attribute onkeypress='return checkIfNumber(event);' in the element

function checkIfNumber(event) {
    var key = window.event ? event.keyCode : event.which;

    if (event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39) {
        return true;
    }
    else if ((key < 48 || key > 57) || key == 46) {
        return false;
    }
    else return true;
};

function validateFloatKeyPress(el, evt) {

    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
&& (charCode < 48 || charCode > 57)) {
        return false;
    }

    if (charCode == 46 && el.value.indexOf(".") !== -1) {
        return false;
    }

    return true;
}

function AutoCompleteSearch(sender, eventArgs) {

    var id = sender._id;
    var ArrayIDs = id.split('_');
    if (isNaN(ArrayIDs[ArrayIDs.length - 1]) == true) {// out side gridview textbox

        var ContentPlaceHolderID = id.replace(ArrayIDs[ArrayIDs.length - 1], '')
        var NameOfTextBox = ArrayIDs[ArrayIDs.length - 1].replace('ACEtxt', '');

        var txt = $get(ContentPlaceHolderID + 'txt' + NameOfTextBox);
        var txtName = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'Name');
        var txtID = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'ID');
        txt.value = eventArgs.get_text();
        txtName.value = eventArgs.get_text();
        txtID.value = eventArgs.get_value();
    }
    else { // in side gridview textbox

        var ContentPlaceHolderID = id.replace("_" + ArrayIDs[ArrayIDs.length - 1], '').replace(ArrayIDs[ArrayIDs.length - 2], '')
        var NameOfTextBox = ArrayIDs[ArrayIDs.length - 2].replace('ACEtxt', '');
        var RowIndex = ArrayIDs[ArrayIDs.length - 1];

        var txt = $get(ContentPlaceHolderID + 'txt' + NameOfTextBox + '_' + RowIndex);
        var txtName = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'Name_' + RowIndex);
        var txtID = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'ID_' + RowIndex);
        txt.value = eventArgs.get_text();
        txtName.value = eventArgs.get_text();
        txtID.value = eventArgs.get_value();
    }
}
function ClearAutocompleteTextBox(sender) {

    var id = sender.id;
    var ArrayIDs = id.split('_');
    if (isNaN(ArrayIDs[ArrayIDs.length - 1]) == true) { // out side gridview textbox

        var ContentPlaceHolderID = id.replace(ArrayIDs[ArrayIDs.length - 1], '')
        var NameOfTextBox = ArrayIDs[ArrayIDs.length - 1].replace('txt', '');

        var txt = $get(ContentPlaceHolderID + 'txt' + NameOfTextBox);
        var txtName = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'Name');
        var txtID = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'ID');
        if (txt.value == "No Records Found" || txt.value != txtName.value) {
            txt.value = "";
            txtName.value = "";
            txtID.value = "";
        }
    }
    else {// In side gridview textbox

        var ContentPlaceHolderID = id.replace("_" + ArrayIDs[ArrayIDs.length - 1], '').replace(ArrayIDs[ArrayIDs.length - 2], '')
        var NameOfTextBox = ArrayIDs[ArrayIDs.length - 2].replace('txt', '');
        var RowIndex = ArrayIDs[ArrayIDs.length - 1];

        var txt = $get(ContentPlaceHolderID + 'txt' + NameOfTextBox + '_' + RowIndex);
        var txtName = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'Name_' + RowIndex);
        var txtID = $get(ContentPlaceHolderID + 'hdf' + NameOfTextBox + 'ID_' + RowIndex);

        if (txt.value == "No Records Found" || txt.value != txtName.value) {
            txt.value = "";
            txtName.value = "";
            txtID.value = "";
        }
    }
}

function SelectAll(sender) {
    var id = sender.id;
    var ArrayIDs = id.split('_');
    var but = document.getElementById(sender.id);
    var GridID = sender.id.split('_')[0] + "_" + sender.id.split('_')[1];
    var TargetBaseControl = document.getElementById(GridID);
    var allInputs = TargetBaseControl.getElementsByTagName("input");
    for (var i = 0, max = allInputs.length; i < max; i++) {
        if (allInputs[i].type === 'checkbox') {
            if (but.checked == true) {
                if (allInputs[i].disabled == false) {
                    allInputs[i].checked = true;
                }
            }
            else {
                allInputs[i].checked = false;
            }
        }

    }
}


function checkClass(no) {
    debugger;
    if (no == 1) {
        $("#divSearch").removeClass("in");
    }
    if (no == 2) {
        $("#divCancel").removeClass("in");
    }
    if (no == 3) {
        $("#divCancel").addClass("in");
    }
}