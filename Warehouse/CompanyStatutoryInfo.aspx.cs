﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to add,edit and delete statutory info of Company
*/
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
#endregion
public partial class CompanyStatutoryInfo : BigSunPage
{
    AppCompany objCompany = null;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        objCompany = (AppCompany)Session[TableConstants.ObjCompanySession];     
        if (!IsPostBack)
        {
            BindData();
        }
    }
    #endregion
    #region grdVendorStatuInfo_PreRender
    protected void grdVendorStatuInfo_PreRender(object sender, EventArgs e)
    {
        if (grdVendorStatuInfo.Rows.Count > 0)
        {
            grdVendorStatuInfo.UseAccessibleHeader = true;
            grdVendorStatuInfo.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region BindData
    protected void BindData()
    {       
        grdVendorStatuInfo.DataSource = objCompany.CompanyStatutoryCollAllList;
        grdVendorStatuInfo.DataBind();
    }
    #endregion
    #region txtValue_TextChanged
    protected void txtValue_TextChanged(object sender, EventArgs e)
    {
        GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent;
        string statutoryId = ((HiddenField)currentRow.FindControl("hdfStatutoryId")).Value;
        string statutoryval = ((TextBox)currentRow.FindControl("txtValue")).Text;             
        AppObjects.AppCompanyStatutory SingleObj = new AppObjects.AppCompanyStatutory(intRequestingUserID);
        SingleObj.Company_ID = objCompany.ID;
        SingleObj.StatutoryType_ID = new AppConvert(statutoryId);
        SingleObj.Value = statutoryval;
        SingleObj.Status = new AppConvert((int)RecordStatus.Active);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;
        objCompany.AddNewCompanyStatutory(SingleObj);
        int index = objCompany.CompanyStatutoryCollAllList.FindIndex(o => o.StatutoryType_ID == SingleObj.StatutoryType_ID);
        if (index >= -1)
            objCompany.CompanyStatutoryCollAllList[index] = SingleObj;
        Session.Add(TableConstants.ObjCompanySession,objCompany);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Record Successfully Added','1'" + ");", true);
        BindData();
    }
    #endregion
}