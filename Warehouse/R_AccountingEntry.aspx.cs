﻿#region Author
/*
Name    :   Navnath Sonavane
Date    :   18/10/2016
Use     :   This form is used to display accounting entry data.
*/
#endregion
#region Using
using System;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using AppObjects;
using System.Web.UI;
using System.Configuration;
using System.Web;
using System.Collections.Generic;
using AppUtility;
#endregion
public partial class R_AccountingEntry : BigSunPage
{
    CommonFunctions CF = new CommonFunctions();
    static string CompanyID = "";
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CompanyID = new AppConvert(Session[TableConstants.CompanyIdConst]);
            DefaultData();
        }
    }
    #endregion
   
    public void DefaultData()
    {
        DateTime now = DateTime.Now;
        txtFromDate.Text = new DateTime(now.Year, now.Month,1).ToString("dd/MM/yyyy");
        txtToDate.Text = now.ToString("dd/MM/yyyy");
       
    }
  
  
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string path = ConfigurationManager.AppSettings["ReportPath"];
        string url = path + CF.GetReportName("10001", CompanyID); // Fetch Path from Database
        ViewReport(url);
        btnSearch.Focus();
    }
    public void ViewReport(string url)
    {

        string
            VoucherType = ddlVocuerType.SelectedValue,
            VoucherNo = txtVoucherNo.Text == "" ? "-1" : txtVoucherNo.Text,
            FromDate = Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd"),
            ToDate = Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd"),
            ReportServer = "";

        ReportServer = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "/") + ConfigurationManager.AppSettings["RootURL"];
       

        ReportViewer1.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerURLEmbedded"].ToString());
        ReportViewer1.ServerReport.ReportPath = url;
        var parameters = new List<ReportParameter>();

        parameters.Add(new ReportParameter("CompanyID", CompanyID));
        parameters.Add(new ReportParameter("VoucherType", VoucherType));
        parameters.Add(new ReportParameter("VoucherNo", VoucherNo));
        parameters.Add(new ReportParameter("FromDate", FromDate));
        parameters.Add(new ReportParameter("ToDate", ToDate));
        parameters.Add(new ReportParameter("ProfitCenterName", "Profit Center"));
        
        ReportViewer1.ServerReport.SetParameters(parameters);
        ReportViewer1.ProcessingMode = ProcessingMode.Remote;
        //Hide toolbar
        ReportViewer1.ShowParameterPrompts = false;
        //Hide Export Bar
        ReportViewer1.ShowPromptAreaButton = false;
        ReportViewer1.ServerReport.Refresh();
    }


}