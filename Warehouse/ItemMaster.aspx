﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="ItemMaster" Codebehind="ItemMaster.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>
        .checkboxHeader {
            width: 20px;
            margin: 0px 0px;
            position: relative;
        }

            .checkboxHeader label {
                display: inline-block;
                max-width: 100%;
                margin-bottom: 0px;
                font-weight: 700;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    
                    <div class="panel">
                        <div class="panel-body">
                            <div class="pull-right">
                                <br />
                                <asp:LinkButton ID="btnSave" TabIndex="1" runat="server" ValidationGroup="save" OnClick="btnSave_Click"><i class="fa fa-check  fa-2x text-success"></i></asp:LinkButton>
                                <asp:HiddenField ID="hdfID" runat="server" Value="" />
                                <asp:LinkButton ID="btnCancel" TabIndex="1" runat="server" OnClick="btnCancel_Click"><i class="fa fa-close fa-2x"></i></asp:LinkButton>
                            </div>
                            <h3>Item Details</h3>
                            <hr />
                            <div class="row">
                                <div class="col-lg-3">
                                    <asp:Label ID="lblItemCode" runat="server" CssClass="bold" Text="Item Code"></asp:Label>
                                    <asp:Label ID="Label1" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtItemCode" CssClass="text-danger"
                                        Display="Dynamic" ErrorMessage=" (Enter Item Code)" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtItemCode" TabIndex="1" runat="server" MaxLength="50" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                                </div>
                                <div class="col-lg-3">
                                    <asp:Label ID="lblItemName" runat="server" CssClass="bold " Text="Item Name"></asp:Label>
                                    <asp:Label ID="lblStar" runat="server" CssClass=" bold text-danger" Text=" * ">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtItemName" CssClass="text-danger"
                                            Display="Dynamic" ErrorMessage=" (Enter Item Name)" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    </asp:Label>
                                    <asp:TextBox ID="txtItemName" TabIndex="1" runat="server" MaxLength="50" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save"
                                        CssClass="form-control input-sm"></asp:TextBox>
                                </div>
                                <div class="col-lg-3">
                                    <asp:Label ID="lblItemCategory" CssClass="bold " runat="server" Text="Item Category"> </asp:Label>
                                    <asp:Label ID="lblitemcate" runat="server" CssClass=" bold text-danger" Text="  "></asp:Label>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtItemCategory" CssClass="text-danger" InitialValue="0" Text=""
                                        Display="Dynamic" ErrorMessage=" (Select Item Category)" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <div class="input-group">
                                        <asp:TextBox ID="txtItemCategory" TabIndex="1" placeholder="Select Item Category" runat="server"
                                            AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div>
                                    <asp:HiddenField runat="server" ID="hdfItemCategoryName" Value="" />
                                    <asp:HiddenField runat="server" ID="hdfItemCategoryID" Value="" />
                                    <asp:AutoCompleteExtender ID="ACEtxtItemCategory" runat="server"
                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CategoryTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                        OnClientItemSelected="AutoCompleteSearch" ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtItemCategory">
                                    </asp:AutoCompleteExtender>
                                </div>
                                <div class="col-lg-1">
                                    <asp:Label ID="lblActive" runat="server" Text="Active/InActive" CssClass="bold "></asp:Label>

                                    <div class="material-switch pull-right">
                                        <asp:CheckBox runat="server" ID="chkActive" TabIndex="1" Checked="true" />
                                        <label for='<%=chkActive.ClientID %>' class="label-primary"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <asp:Label ID="lblItemUnit" CssClass="bold " runat="server" Text="UOM"></asp:Label>
                                    <asp:DropDownList ID="ddlUOM" runat="server" CssClass="form-control input-sm" TabIndex="1">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-3">
                                    <asp:Label ID="lblItemMode" CssClass="bold " runat="server" Text="Item Mode"></asp:Label>
                                    <asp:DropDownList ID="ddlItemMode" OnSelectedIndexChanged="ddlItemMode_SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control input-sm" TabIndex="1">
                                        <asp:ListItem Value="0">Purchase</asp:ListItem>
                                        <asp:ListItem Value="1">PurchaseAndSale</asp:ListItem>
                                        <asp:ListItem Value="2">Sale</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-3">
                                    <asp:Label ID="lblItemType" CssClass="bold " runat="server" Text="Item Type"></asp:Label>
                                    <asp:DropDownList ID="ddlItemType" runat="server" CssClass="form-control input-sm" TabIndex="1">
                                        <asp:ListItem Value="0">Movable</asp:ListItem>
                                        <asp:ListItem Value="1">Marketable</asp:ListItem>
                                        <asp:ListItem Value="2">Manufactured</asp:ListItem>
                                        <asp:ListItem Value="3">Mentioned</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-lg-3">
                                    <asp:Label ID="lblType" CssClass="bold " runat="server" Text="Type"></asp:Label>
                                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control input-sm" TabIndex="1">
                                        <asp:ListItem Value="0">Select Type</asp:ListItem>
                                        <asp:ListItem Value="1">Weight</asp:ListItem>
                                        <asp:ListItem Value="2">Volume</asp:ListItem>
                                        <asp:ListItem Value="3">Length</asp:ListItem>
                                        <asp:ListItem Value="4">Counted</asp:ListItem>
                                        <asp:ListItem Value="5">Serviced</asp:ListItem>
                                        <asp:ListItem Value="6">Non Inventorized</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                         <div id="divPurchase" runat="server">
                                            <div class="col-lg-3" runat="server" id="divPurchaseLedger">
                                                <asp:Label ID="lblPurchaseLedger" Visible="false" CssClass="bold" runat="server" Text="Purchase Ledger"> </asp:Label>
                                                <asp:CheckBox Text="New Ledger" ID="chkIsNewPurchaseLedger" CssClass="checkboxHeader" runat="server" AutoPostBack="true" OnCheckedChanged="chkIsNewPurchaseLedger_CheckedChanged" Checked="true" />
                                                <asp:CheckBox Text="Sales Same As" ID="chkSameAs" CssClass="checkboxHeader" runat="server" AutoPostBack="true" OnCheckedChanged="chkSameAs_CheckedChanged" Checked="true" />
                                                <div class="input-group">
                                                    <asp:RequiredFieldValidator ID="RFV_txtPurchaseLedger" runat="server" ControlToValidate="txtPurchaseLedger" CssClass="text-danger"
                                                        Display="Dynamic" ErrorMessage="" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>

                                                    <asp:TextBox ID="txtPurchaseLedger" TabIndex="1" placeholder="Select Purchase Ledger " runat="server"
                                                        AutoCompleteType="None" autocomplete="Off" ValidationGroup="save" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                    <asp:HiddenField runat="server" ID="hdfPurchaseLedgerName" />
                                                    <asp:HiddenField runat="server" ID="hdfPurchaseLedgerID" Value="0" />
                                                </div>
                                                <asp:AutoCompleteExtender ID="ACEtxtPurchaseLedger" runat="server"
                                                    CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                    CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                    CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                                    FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                    OnClientItemSelected="AutoCompleteSearch" ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtPurchaseLedger">
                                                </asp:AutoCompleteExtender>
                                            </div>
                                            <div id="divPurchaseTax" class="col-lg-3">
                                                <asp:Label ID="lblPurchaseTax" CssClass="bold " runat="server" Text="Purchase Tax"> </asp:Label>
                                                <asp:Label ID="lblPurchaseTaxMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                <asp:RequiredFieldValidator ID="RFV_txtPurchaseTax" runat="server" ControlToValidate="txtPurchaseTax" CssClass="text-danger"
                                                    Display="Dynamic" ErrorMessage=" (Select Tax)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtPurchaseTax" TabIndex="1" placeholder="Select Tax" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                </div>
                                                <asp:HiddenField runat="server" ID="hdfPurchaseTaxName" Value="" />
                                                <asp:HiddenField runat="server" ID="hdfPurchaseTaxID" Value="" />
                                                <asp:AutoCompleteExtender ID="ACEtxtPurchaseTax" runat="server"
                                                    CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                    CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                    CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                    FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TaxTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                    ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtPurchaseTax">
                                                </asp:AutoCompleteExtender>
                                            </div>
                                        </div>
                                         <div id="divSales" runat="server">
                                            <div class="col-lg-3" runat="server" id="divSalesLedger">
                                                <asp:Label ID="lblSalesLedger" CssClass="bold" runat="server" Text="Sales Ledger"> </asp:Label>
                                                <asp:CheckBox Text="New Ledger" ID="chkIsNewSalesLedger" CssClass="checkboxHeader" AutoPostBack="true" OnCheckedChanged="chkIsNewSalesLedger_CheckedChanged" runat="server" Checked="true" />
                                                <div class="input-group">
                                                    <asp:RequiredFieldValidator ID="RFV_txtSalesLedger" runat="server" ControlToValidate="txtSalesLedger" CssClass="text-danger"
                                                        Display="Dynamic" ErrorMessage="" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>

                                                    <asp:TextBox ID="txtSalesLedger" TabIndex="1" placeholder="Select Sales Ledger" runat="server"
                                                        AutoCompleteType="None" autocomplete="Off" ValidationGroup="save" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                    <asp:HiddenField runat="server" ID="hdfSalesLedgerName" />
                                                    <asp:HiddenField runat="server" ID="hdfSalesLedgerID" Value="0" />
                                                </div>
                                                <asp:AutoCompleteExtender ID="ACEtxtSalesLedger" runat="server"
                                                    CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                    CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                    CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                                    FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                    OnClientItemSelected="AutoCompleteSearch" ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtSalesLedger">
                                                </asp:AutoCompleteExtender>
                                            </div>
                                            <div id="divSalesTax" class="col-lg-3">
                                                <asp:Label ID="lblSalesTax" CssClass="bold " runat="server" Text="Sales Tax"> </asp:Label>
                                                <asp:Label ID="lblSalesTaxMsg" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                                                <asp:RequiredFieldValidator ID="RFV_txtSalesTax" runat="server" ControlToValidate="txtSalesTax" CssClass="text-danger"
                                                    Display="Dynamic" ErrorMessage=" (Select Tax)" ValidationGroup="MainSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtSalesTax" TabIndex="1" placeholder="Select Tax" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                </div>
                                                <asp:HiddenField runat="server" ID="hdfSalesTaxName" Value="" />
                                                <asp:HiddenField runat="server" ID="hdfSalesTaxID" Value="" />
                                                <asp:AutoCompleteExtender ID="ACEtxtSalesTax" runat="server"
                                                    CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                    CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                    CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                    FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TaxTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                    ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtSalesTax">
                                                </asp:AutoCompleteExtender>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <asp:Label ID="lblDescription" runat="server" CssClass="bold" Text="Description"></asp:Label>
                                    <asp:TextBox ID="txtItemDescription" TextMode="MultiLine" TabIndex="1" runat="server" MaxLength="500" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                                </div>

                            </div>
                        </div>
                    </div>


















                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
