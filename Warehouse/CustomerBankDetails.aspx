﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="CustomerBankDetails" Codebehind="CustomerBankDetails.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class=" col-lg-12 ">
            <h3>Bank Details</h3>
            <hr />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdBankDetails" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Horizontal"
                        CssClass="table table-hover table-striped text-nowrap nowrap" OnRowDataBound="grdBankDetails_RowDataBound"
                        OnPreRender="grdBankDetails_PreRender"
                        OnRowCommand="grdBankDetails_RowCommand" OnRowEditing="grdBankDetails_RowEditing" OnRowDeleting="grdBankDetails_RowDeleting"
                        OnRowUpdating="grdBankDetails_RowUpdating">
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <HeaderTemplate>
                                    <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="btn btn-primary btn-xs" CommandName="ADDNEW"
                                        Text="Add New" ToolTip="Add New" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnEdit" runat="server" CssClass=" fa fa-edit fa-2x" CommandName="Edit"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x" CommandName="Delete"
                                        Text="" ToolTip="Delete" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update" ValidationGroup="EditBank"
                                        Text="" />
                                    <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="CANCELEDIT" CssClass="fa fa-ban fa-2x" toot="Cancel"
                                        Text="" />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="lbtnSave" runat="server" CommandName="INSERT" CssClass="fa fa-floppy-o fa-2x" ToolTip="Save" ValidationGroup="AddBank"
                                        Text="" UseSubmitBehavior="False" />
                                    <asp:LinkButton ID="lbtnCancelAdd" runat="server" CommandName="CANCELADD" CssClass="fa fa-ban  fa-2x" Text="" />
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblBankType" Text="Bank Type" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblBank" runat="server" CssClass="bold" MaxLength="50" Text='<%#Eval("BankType.Name") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtBankType" runat="server" Text='<%# Eval("BankType.Name") %>' MaxLength="500" TabIndex="1" ValidationGroup="EditBank" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdfBankTypeName" Value='<%# Eval("BankType.Name").ToString() %>' />
                                    <asp:HiddenField runat="server" ID="hdfBankTypeID" Value='<%# Eval("BankType_ID") %>' />
                                    <asp:RequiredFieldValidator ID="RFV_txtBankType" runat="server" ControlToValidate="txtBankType" CssClass="text-danger"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="EditBank" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:AutoCompleteExtender ID="ACEtxtBankType" runat="server"
                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="BankTypeSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                        ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtBankType">
                                    </asp:AutoCompleteExtender>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewBank" runat="server" TabIndex="1" onblur="return ClearAutocompleteTextBox(this)" PlaceHolder="Select Bank" AutoPostBack="false" AutoComplete="Off" AutoCompleteType="None" MaxLength="50" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdfNewBankName" />
                                    <asp:HiddenField runat="server" ID="hdfNewBankID" Value="0" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtNewBank" CssClass="text-danger" ToolTip="Required Field"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="AddBank" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:AutoCompleteExtender ID="ACEtxtNewBank" runat="server"
                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="BankTypeSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                        ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtNewBank">
                                    </asp:AutoCompleteExtender>                                    
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblAccNo" Text="Account No" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblAccuntNo" runat="server" TabIndex="1" Text='<%#Eval("AccountNo") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtAccountNo" runat="server" Text='<%# Eval("AccountNo") %>' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:Label ID="lblAccountNumber" runat="server" CssClass="text-danger">
                                        <asp:RequiredFieldValidator ID="RFV_txtAccountNo" runat="server" ControlToValidate="txtAccountNo"
                                            Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="EditBank"></asp:RequiredFieldValidator>
                                    </asp:Label>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewAccountNo" runat="server" Text='' MaxLength="50" TabIndex="1" AutoComplete="Off" CssClass="input-sm form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_txtNewAccountNo" runat="server" ControlToValidate="txtNewAccountNo"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="" ValidationGroup="AddBank"></asp:RequiredFieldValidator>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblHdrLedger" Text="Ledger" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblLedger" runat="server" CssClass="bold" MaxLength="50" Text='<%#Eval("Ledger.LedgerName") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtLedger" runat="server" Text='<%# Eval("Ledger.LedgerName") %>' MaxLength="500" TabIndex="1" ValidationGroup="EditBank" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdfLedgerName" Value='<%# Eval("Ledger.LedgerName").ToString() %>' />
                                    <asp:HiddenField runat="server" ID="hdfLedgerID" Value='<%# Eval("Ledger_ID") %>' />
                                    <asp:RequiredFieldValidator ID="RFV_txtLedger" runat="server" ControlToValidate="txtLedger" CssClass="text-danger"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="EditBank" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:AutoCompleteExtender ID="ACEtxtLedger" runat="server"
                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                        ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLedger">
                                    </asp:AutoCompleteExtender>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewLedger" runat="server" TabIndex="1" onblur="return ClearAutocompleteTextBox(this)" PlaceHolder="Select Bank" AutoPostBack="false" AutoComplete="Off" AutoCompleteType="None" MaxLength="50" CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:HiddenField runat="server" ID="hdfNewLedgerName" />
                                    <asp:HiddenField runat="server" ID="hdfNewLedgerID" Value="0" />
                                    <asp:RequiredFieldValidator ID="RFV_txtNewLedger" runat="server" ControlToValidate="txtNewLedger" CssClass="text-danger" ToolTip="Required Field"
                                        Display="Dynamic" ErrorMessage="" ValidationGroup="AddBank" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:AutoCompleteExtender ID="ACEtxtNewLedger" runat="server"
                                        CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                        CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                        FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                        ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtNewLedger">
                                    </asp:AutoCompleteExtender>                                    
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblCurrency" Text="Currency" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblCurrency" runat="server" Text='<%#Eval("CurrencyType.Name").ToString() %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlCurrencyType" runat="server" TabIndex="1" CssClass="form-control input-sm" ValidationGroup="EditBank">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddlNewCurrencyType" runat="server" TabIndex="1" CssClass="form-control input-sm" ValidationGroup="AddBank">
                                    </asp:DropDownList>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Active" Text="Active" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblActive" runat="server" Text='<%#(Eval("StatusName")) %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsActiveBank" CssClass="form-control" TabIndex="8" Checked='<%#Eval("Status").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkActiveBank" CssClass="form-control" TabIndex="8" Checked="true" />
                                </FooterTemplate>
                            </asp:TemplateField>
                          <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="Default" Text="Default" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblDefault" runat="server" Text='<%#(Eval("Type").ToString()=="1"?"Y":"N") %>' CssClass="bold"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkIsDefaultBank" TabIndex="9"  Checked='<%#Eval("Type").ToString()=="1"?true:false %>' />
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox runat="server" ID="chkDefaultBank" TabIndex="9"  />
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdBankDetails" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
