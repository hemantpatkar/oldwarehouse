﻿#region Author
/*
Name    :   Pradip BObhate
Date    :   04/08/2016
Use     :   This form is used to add Sale Order details  
*/
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.Services;
using AppUtility;
using Objects;
using AjaxControlToolkit;
#endregion
public partial class T_SaleOrder : BigSunPage
{
    #region Page_Load
    AppObjects.AppSaleOrder obj = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["UserIdConst"] = "1";
        obj = new AppObjects.AppSaleOrder(intRequestingUserID);
        if (!IsPostBack)
        {
            DefaultData();
            //ACEtxtCustomer.ContextKey = new AppConvert(LoginCompanyID);
            ACEtxtCustomer.ContextKey = "3";
            Session[TableConstants.SaleOrderSession] = obj;
            string ID = Convert.ToString(Request.QueryString["SaleOrderID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int SaleOrderID = new AppUtility.AppConvert(ID);
                obj = new AppObjects.AppSaleOrder(intRequestingUserID, SaleOrderID);
                Session[TableConstants.SaleOrderSession] = obj;
                PopulateData();
            }
            else
            {
                DefaultData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
            }
            BindData();
            SetValueToGridFooter(true);
        }
    }
    #endregion
    #region Button Click Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppSaleOrder SaleOrder = (AppSaleOrder)Session[TableConstants.SaleOrderSession];
        SaleOrder.SaleOrderItemColl.RemoveAll(x => x.Item_ID == 0);
        if(SaleOrder.SaleOrderItemColl.Count<=0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please add atleast one item','2'" + ");", true);
            return;
        }
        SaveSaleOrderHeader();
        Session.Remove(TableConstants.SaleOrderSession);
        Response.Redirect("T_SaleOrder.aspx?Msg=1");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.SaleOrderSession);
        Response.Redirect("T_SaleOrderSummary.aspx");
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
    }
    protected void btnAddLineItem_Click(object sender, EventArgs e)
    {
        //BindGridData();
        AddEmptyRow(1);
        if (grdSaleOrderDetails.Rows.Count >= 0)
        {
            TextBox txtItem = (TextBox)grdSaleOrderDetails.Rows[0].FindControl("txtItem");
            AutoCompleteExtender ACEtxtItem = (AutoCompleteExtender)grdSaleOrderDetails.Rows[0].FindControl("ACEtxtItem");
            ACEtxtItem.ContextKey = hdfCustomerID.Value;
            txtItem.Focus();
        }
    }
    #endregion
    #region Functions
    public void DefaultData()
    {
        txtSaleDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtDeliveryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtCompanyAddress.Text = DefaultCompanyAddress;
        hdfCompanyAddressName.Value = txtCompanyAddress.Text;
        hdfCompanyAddressID.Value = new AppConvert(DefaultCompanyAddressID);
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);
       
        ACEtxtDeliveryAddress.ContextKey = new AppConvert(Session[TableConstants.LoginCompanyID]);
        ACEtxtCompanyAddress.ContextKey = new AppConvert(Session[TableConstants.LoginCompanyID]);
    }
    public void BindData()
    {
        AppSaleOrder SaleOrder = (AppSaleOrder)Session[TableConstants.SaleOrderSession];
        if (SaleOrder == null)
        {
            SaleOrder = new AppSaleOrder(intRequestingUserID);
        }
        grdSaleOrderDetails.DataSource = SaleOrder.SaleOrderItemColl.Where(x => x.Status >= 0);
        grdSaleOrderDetails.DataBind();
        if (SaleOrder.SaleOrderItemColl.Count <= 0)
        {
            AddEmptyRow(1);
            AutoCompleteExtender ACEtxtItem = (AutoCompleteExtender)grdSaleOrderDetails.Rows[0].FindControl("ACEtxtItem");
            ACEtxtItem.ContextKey = hdfCustomerID.Value;
        }
    }
    public void SaveSaleOrderHeader()
    {
        AppSaleOrder SO = (AppSaleOrder)Session[TableConstants.SaleOrderSession];
        if (SO.ID == 0)
        {
            SO.SaleOrderNumber = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "SO", System.DateTime.Now,"SO-" ,4, ResetCycle.Yearly));
        }
        SO.Company_ID = new AppConvert(LoginCompanyID);
        SO.CompanyAddress_ID = new AppConvert(hdfCompanyAddressID.Value);
        SO.Delivery_Company_ID = new AppConvert(hdfCustomerID.Value);
        SO.Delivery_CompanyAddress_ID = new AppConvert(hdfDeliveryAddressID.Value);
        SO.SaleOrderDate = new AppConvert(txtSaleDate.Text);
        SO.TermsAndConditions_ID = new AppConvert(hdfTermsAndConditionsID.Value);
        SO.TaxType_ID = new AppConvert(hdfHeaderTaxID.Value);
        SO.CurrencyType_ID = new AppConvert(hdfHeaderExchangeCurrencyID.Value);
        SO.SaleOrderBaseAmount = new AppConvert(txtTotalAmount.Text);
        SO.TaxAmount = new AppConvert(txtHeaderTaxAmount.Text);
        SO.OtherCharges = 0;
        SO.FinalAmount = new AppConvert(txtTotalAmount.Text);
        SO.DiscountValue = new AppConvert(txtDiscount.Text);
        SO.DiscountType = new AppConvert(hdfDiscountType.Value);
        SO.DiscountAmount = SO.CalDiscountAmount;
        SO.RoundAmount = 0;
        SO.PODate = new AppConvert(txtPODate.Text);
        SO.PORefernce = new AppConvert(txtPOReference.Text);
        SO.Status = new AppConvert((int)RecordStatus.Created);
        SO.Type = SO.Type;
        SO.ModifiedBy = intRequestingUserID;
        SO.ModifiedOn = System.DateTime.Now;
        SO.Remark = txtRemark.Text;
        SO.Save();
        Session.Remove(TableConstants.SaleOrderSession);
    }
    public void LineItemCalculation(int RowIndex)
    {
        AppSaleOrder SaleOrder = (AppSaleOrder)Session[TableConstants.SaleOrderSession];
        TextBox txtQuantity = (TextBox)grdSaleOrderDetails.Rows[RowIndex].FindControl("txtQuantity");
        TextBox txtRate = (TextBox)grdSaleOrderDetails.Rows[RowIndex].FindControl("txtRate");
        TextBox txtBasePrice = (TextBox)grdSaleOrderDetails.Rows[RowIndex].FindControl("txtBasePrice");
        TextBox txtNetAmount = (TextBox)grdSaleOrderDetails.Rows[RowIndex].FindControl("txtNetAmount");
        TextBox txtDiscount = (TextBox)grdSaleOrderDetails.Rows[RowIndex].FindControl("txtDiscount");
        HiddenField hdfDiscountType = (HiddenField)grdSaleOrderDetails.Rows[RowIndex].FindControl("hdfDiscountType");
        HiddenField hdfLineItemTaxID = (HiddenField)grdSaleOrderDetails.Rows[RowIndex].FindControl("hdfLineItemTaxID");
        HiddenField hdfUOMID = (HiddenField)grdSaleOrderDetails.Rows[RowIndex].FindControl("hdfUOMID");
        SaleOrder.SaleOrderItemColl[RowIndex].Company_ID = new AppConvert(LoginCompanyID);
        SaleOrder.SaleOrderItemColl[RowIndex].CompanyAddress_ID = new AppConvert(hdfCompanyAddressID.Value);
        SaleOrder.SaleOrderItemColl[RowIndex].Delivery_Company_ID = new AppConvert(hdfCustomerID.Value);
        SaleOrder.SaleOrderItemColl[RowIndex].Delivery_CompanyAddress_ID = new AppConvert(hdfDeliveryAddressID.Value);
        SaleOrder.SaleOrderItemColl[RowIndex].Quantity = new AppUtility.AppConvert(txtQuantity.Text);
        SaleOrder.SaleOrderItemColl[RowIndex].ItemRate = new AppUtility.AppConvert(txtRate.Text);
        SaleOrder.SaleOrderItemColl[RowIndex].DiscountValue = new AppUtility.AppConvert(txtDiscount.Text);
        SaleOrder.SaleOrderItemColl[RowIndex].DiscountAmount = SaleOrder.SaleOrderItemColl[RowIndex].CalDiscountAmount;
        SaleOrder.SaleOrderItemColl[RowIndex].DiscountType = new AppConvert(hdfDiscountType.Value == "0" ? 1 : new AppConvert(hdfDiscountType.Value));
        SaleOrder.SaleOrderItemColl[RowIndex].ItemBaseAmount = SaleOrder.SaleOrderItemColl[RowIndex].PostDiscountPrice;
        SaleOrder.SaleOrderItemColl[RowIndex].UOMType_ID = new AppUtility.AppConvert(hdfUOMID.Value);
        SaleOrder.SaleOrderItemColl[RowIndex].TaxType_ID = new AppUtility.AppConvert(hdfLineItemTaxID.Value);
        if(string.IsNullOrEmpty(hdfLineItemTaxID.Value)==false && hdfLineItemTaxID.Value!="0")
        {
            SaleOrder.SaleOrderItemColl[RowIndex].TaxType.CalculateTax(new AppConvert(SaleOrder.SaleOrderItemColl[RowIndex].ItemBaseAmount), 0, 0);
            SaleOrder.SaleOrderItemColl[RowIndex].TaxAmount = SaleOrder.SaleOrderItemColl[RowIndex].TaxType.TaxValue;
        }
        SaleOrder.SaleOrderItemColl[RowIndex].NetAmount = SaleOrder.SaleOrderItemColl[RowIndex].CalNetAmount;
        SaleOrder.SaleOrderItemColl[RowIndex].FinalAmount = SaleOrder.SaleOrderItemColl[RowIndex].CalFinalAmount;        
        SaleOrder.SaleOrderItemColl[RowIndex].ModifiedBy = intRequestingUserID;
        SaleOrder.SaleOrderItemColl[RowIndex].ModifiedOn = System.DateTime.Now;
        SaleOrder.SaleOrderItemColl[RowIndex].Status = new AppConvert((int)RecordStatus.Created);
        SaleOrder.SaleOrderItemColl[RowIndex].Type = new AppConvert((int)DefaultNormalType.Normal);
        Session.Add(TableConstants.SaleOrderSession, SaleOrder);
        BindData();
        SetValueToGridFooter();
        HeaderCalculation();
    }
    public void SetValueToGridFooter(bool IsPopulateData=false)
    {
        AppSaleOrder SaleOrder = (AppSaleOrder)Session[TableConstants.SaleOrderSession];
        Label lblFooterCurrencyAmount = (Label)grdSaleOrderDetails.FooterRow.FindControl("lblFooterCurrencyAmount");
        lblFooterCurrencyAmount.Text = Convert.ToDecimal(SaleOrder.SaleOrderItemColl.Where(x=>x.Status==0).Sum(x => x.ConvertedCurrencyAmount)).ToString("#,##0.00");
        Label lblFooterNetAmount = (Label)grdSaleOrderDetails.FooterRow.FindControl("lblFooterNetAmount");
        lblFooterNetAmount.Text = Convert.ToDecimal(SaleOrder.SaleOrderItemColl.Where(x => x.Status == 0).Sum(x => x.CalNetAmount)).ToString("#,##0.00");
        Label lblFooterTax = (Label)grdSaleOrderDetails.FooterRow.FindControl("lblFooterTax");
        lblFooterTax.Text = Convert.ToDecimal(SaleOrder.SaleOrderItemColl.Where(x => x.Status == 0).Sum(x => x.TaxAmount)).ToString("#,##0.00");
        Label lblFooterDiscount = (Label)grdSaleOrderDetails.FooterRow.FindControl("lblFooterDiscount");
        lblFooterDiscount.Text = Convert.ToDecimal(SaleOrder.SaleOrderItemColl.Where(x => x.Status == 0).Sum(x => x.DiscountValue)).ToString("#,##0.00");
        Label lblFooterBasePrice = (Label)grdSaleOrderDetails.FooterRow.FindControl("lblFooterBasePrice");
        lblFooterBasePrice.Text = Convert.ToDecimal(SaleOrder.SaleOrderItemColl.Where(x => x.Status == 0).Sum(x => x.BasePrice)).ToString("#,##0.00");
        Label lblFooterQuantity = (Label)grdSaleOrderDetails.FooterRow.FindControl("lblFooterQuantity");
        lblFooterQuantity.Text = Convert.ToDecimal(SaleOrder.SaleOrderItemColl.Where(x => x.Status == 0).Sum(x => x.Quantity)).ToString("#,##0.00");
        if(IsPopulateData==false)
        txtTotalAmount.Text = lblFooterNetAmount.Text;
    }
    public void AddEmptyRow(int NumberOfRows)
    {
        AppSaleOrder SaleOrder = (AppSaleOrder)Session[TableConstants.SaleOrderSession];
        if (SaleOrder == null)
        {
            SaleOrder = new AppSaleOrder(intRequestingUserID);
        }
        AppSaleOrderItem SaleOrderItem = new AppSaleOrderItem(intRequestingUserID);
        for (int i = 0; i < NumberOfRows; i++)
        {
            if (SaleOrder.SaleOrderItemColl.Count == 0)
            {
                SaleOrder.SaleOrderItemColl.Add(SaleOrderItem);
            }
            else
            {
                SaleOrder.SaleOrderItemColl.Insert(0, SaleOrderItem);
            }
            Session.Add(TableConstants.SaleOrderSession, SaleOrder);
        }
        BindData();
    }
    public void DefaultCustomerRecord(int NumberOfRows, int CustomerID)
    {
        AppSaleOrder SaleOrder = (AppSaleOrder)Session[TableConstants.SaleOrderSession];
        if (SaleOrder.SaleOrderItemColl == null)
        {
            SaleOrder = new AppSaleOrder(intRequestingUserID);
        }
        AppSaleOrderItem SaleOrderItem = new AppSaleOrderItem(intRequestingUserID);
        for (int i = 0; i < NumberOfRows; i++)
        {
            SaleOrder.SaleOrderItemColl.Add(SaleOrderItem);
            Session.Add(TableConstants.SaleOrderSession, SaleOrder);
        }
        BindData();
    }
    public void PopulateData()
    {
        AppSaleOrder obj = (AppSaleOrder)Session[TableConstants.SaleOrderSession];
        txtCustomer.Enabled = false;
        txtSaleDate.Text = new AppConvert(obj.SaleOrderDate);
        txtCustomer.Text = new AppConvert(obj.Delivery_Company.Name);
        hdfCustomerName.Value = txtCustomer.Text;
        hdfCustomerID.Value = new AppConvert(obj.Delivery_Company.ID);        
        txtSaleOrderNo.Text = new AppConvert(obj.SaleOrderNumber);
        txtStatus.Text = obj.StatusName;
        txtTermsAndConditions.Text = new AppConvert(obj.TermsAndConditions.Statement);
        hdfTermsAndConditionsName.Value = txtTermsAndConditions.Text;
        hdfTermsAndConditionsID.Value = new AppConvert(obj.TermsAndConditions.ID);
        txtDiscount.Text = new AppConvert(obj.DiscountValue);
        hdfDiscountType.Value = new AppConvert(obj.DiscountType);
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);

       

        txtHeaderExchangeCurrency.Text = obj.CurrencyType.Name;
        hdfHeaderExchangeCurrencyName.Value = txtHeaderExchangeCurrency.Text;
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(obj.CurrencyType_ID);
        txtCompanyAddress.Text = obj.SetCompanyAddress;
        hdfCompanyAddressName.Value = txtCompanyAddress.Text;
        hdfCompanyAddressID.Value = new AppConvert(obj.CompanyAddress_ID);
        txtDeliveryAddress.Text = obj.SetDeliveryAddress;
        hdfDeliveryAddressName.Value = txtDeliveryAddress.Text;
        hdfDeliveryAddressID.Value = new AppConvert(obj.Delivery_CompanyAddress_ID);
        txtDeliveryAddress.Text = obj.CustomerAddress;
        hdfDeliveryAddressName.Value = txtDeliveryAddress.Text;
        hdfDeliveryAddressID.Value = new AppConvert(obj.CustomerAddressID);
        txtRemark.Text = obj.Remark;
        txtPODate.Text = new AppUtility.AppConvert(obj.PODate);
        txtPOReference.Text = obj.PORefernce;
        txtHeaderTax.Text = obj.TaxType.Name;
        hdfHeaderTaxName.Value = txtHeaderTax.Text;
        hdfHeaderTaxID.Value = new AppConvert(obj.TaxType_ID);
        txtHeaderTaxAmount.Text = new AppConvert(obj.TaxAmount);
        txtTotalAmount.Text = new AppConvert(obj.FinalAmount);
    }
    public void HeaderCalculation()
    {
        AppSaleOrder SaleOrder = (AppSaleOrder)Session[TableConstants.SaleOrderSession];
        SaleOrder.DiscountType = new AppConvert(hdfDiscountType.Value);
        SaleOrder.DiscountValue = new AppConvert(txtDiscount.Text);
        SaleOrder.FinalAmount = SaleOrder.SaleOrderItemColl.Sum(x => x.NetAmount);
        txtTotalAmount.Text = new AppConvert(SaleOrder.PostDiscountPrice);
        
        if(string.IsNullOrEmpty(hdfHeaderTaxID.Value)==false && hdfHeaderTaxID.Value!="0")
        {
            SaleOrder.TaxType_ID = new AppConvert(hdfHeaderTaxID.Value);
            SaleOrder.TaxType.CalculateTax(new AppConvert(txtTotalAmount.Text), 0, 0);
            txtHeaderTaxAmount.Text = new AppConvert(SaleOrder.TaxType.TaxValue);
            txtTotalAmount.Text = new AppConvert(SaleOrder.PostDiscountPrice + SaleOrder.TaxType.TaxValue);
        }
        else{
            txtHeaderTaxAmount.Text = "0";
        }
        SaleOrder.AllocateAmount();

        Session[TableConstants.SaleOrderSession] = SaleOrder;

    }
    #endregion
    #region Text Box Event
    protected void txtCustomer_TextChanged(object sender, EventArgs e)
    {
       
        obj.Delivery_Company_ID = new AppConvert(hdfCustomerID.Value);
        hdfDeliveryAddressID.Value = new AppConvert(obj.CustomerAddressID);
        hdfDeliveryAddressName.Value = new AppConvert(obj.CustomerAddress);
        txtDeliveryAddress.Text = new AppConvert(obj.CustomerAddress);
        hdfTermsAndConditionsID.Value = new AppConvert(obj.DefaultTermsAndConditionsID);
        hdfTermsAndConditionsName.Value = new AppConvert(obj.DefaultTermsAndConditionsStatement);
        txtTermsAndConditions.Text = new AppConvert(obj.DefaultTermsAndConditionsStatement);
        obj.GetTopCustomerItem(5);
        Session.Add(TableConstants.SaleOrderSession, obj);
        BindData();
    }
    protected void txtDiscount_TextChanged1(object sender, EventArgs e)
    {
        HeaderCalculation();
    }
    protected void txtHeaderTax_TextChanged(object sender, EventArgs e)
    {
        HeaderCalculation();
    }
    #endregion
    #region Grid View Events
    protected void grdSaleOrderDetails_PreRender(object sender, EventArgs e)
    {
        if (grdSaleOrderDetails.Rows.Count > 0)
        {
            grdSaleOrderDetails.UseAccessibleHeader = true;
            grdSaleOrderDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdSaleOrderDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
            AppSaleOrder SaleOrder = (AppSaleOrder)Session[TableConstants.SaleOrderSession]; 
            if(SaleOrder.ID==0)
            {
                AppSaleOrderItem objSingleSaleOrderItem = SaleOrder.SaleOrderItemColl[row.RowIndex];
                SaleOrder.SaleOrderItemColl.Remove(objSingleSaleOrderItem);
            }
            else
            {
                SaleOrder.SaleOrderItemColl.Where(x => x.Status == 0).ToList()[row.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
            }            
            Session.Add(TableConstants.SaleOrderSession, SaleOrder);
            BindData();
            SetValueToGridFooter();
        }
    }
    protected void txtItem_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfItemID = (HiddenField)grdSaleOrderDetails.Rows[RowIndex].FindControl("hdfItemID");
        HiddenField hdfUOMID = (HiddenField)grdSaleOrderDetails.Rows[RowIndex].FindControl("hdfUOMID");
        TextBox txtQuantity = (TextBox)grdSaleOrderDetails.Rows[RowIndex].FindControl("txtQuantity");
        AppSaleOrder SaleOrder = (AppSaleOrder)Session[TableConstants.SaleOrderSession];
        SaleOrder.SaleOrderItemColl[RowIndex].Item_ID = new AppUtility.AppConvert(hdfItemID.Value);
        SaleOrder.SaleOrderItemColl[RowIndex].UOMType_ID = new AppUtility.AppConvert(hdfUOMID.Value);
        SaleOrder.SaleOrderItemColl[RowIndex].Delivery_Company_ID = new AppConvert(hdfCustomerID.Value);
        Session.Remove(TableConstants.SaleOrderSession);
        Session.Add(TableConstants.SaleOrderSession, SaleOrder);
        BindData();
        SetValueToGridFooter();
        txtQuantity.Focus();
    }
    protected void txtQuantity_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtRate = (TextBox)grdSaleOrderDetails.Rows[RowIndex].FindControl("txtRate");
        LineItemCalculation(RowIndex);
        txtRate.Focus();
    }
    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtDiscount = (TextBox)grdSaleOrderDetails.Rows[RowIndex].FindControl("txtDiscount");
        LineItemCalculation(RowIndex);
        txtDiscount.Focus();
    }
    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtLineItemTax = (TextBox)grdSaleOrderDetails.Rows[RowIndex].FindControl("txtLineItemTax");
        LineItemCalculation(RowIndex);
        txtLineItemTax.Focus();
    }
    protected void txtLineItemTax_TextChanged(object sender, EventArgs e)
    {        
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        Button btnAddLineItem = (Button)grdSaleOrderDetails.HeaderRow.FindControl("btnAddLineItem");
        LineItemCalculation(RowIndex);
        btnAddLineItem.Focus();
    }
    #endregion       
    
}