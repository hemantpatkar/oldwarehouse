﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="User" Codebehind="User.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        function AutoCompleteSearch(sender, eventArgs) {
            debugger;
            var id = sender._id;
            var autocompleteId = id.split('_');
            var replacetxt = autocompleteId[2].replace('txt', '');
            var txt = $get(autocompleteId[0] + '_txt' + replacetxt);
            var txtName = $get(autocompleteId[0] + '_hdf' + replacetxt + 'Name');
            var txtID = $get(autocompleteId[0] + '_hdf' + replacetxt + 'ID');
            txt.value = eventArgs.get_text();
            txtName.value = eventArgs.get_text();
            txtID.value = eventArgs.get_value();
        }

        function ClearAutocompleteTextBox(sender) {
            debugger;
            var id = sender.id;
            var autocompleteId = id.split('_');
            var replacetxt = autocompleteId[1].replace('txt', '');
            var txt = $get(autocompleteId[0] + '_txt' + replacetxt);
            var txtName = $get(autocompleteId[0] + '_hdf' + replacetxt + 'Name');
            var txtID = $get(autocompleteId[0] + '_hdf' + replacetxt + 'ID');
            if (txt.value == "No Records Found" || txt.value != txtName.value) {
                txt.value = "";
                txtName.value = "";
                txtID.value = "";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="PnlUser" runat="server">
        <ContentTemplate>
            <div class=" panel panel-body">
                <div class="row">
                    <div class=" col-lg-2">
                        <asp:Label ID="lblCode" runat="server" Text="Code" CssClas="bold"></asp:Label>
                        <asp:Label ID="Label2" runat="server" CssClass=" bold text-danger" Text="*"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtDesignation" CssClass="text-danger"
                            Display="Dynamic" ErrorMessage=" (Select Designation Name)" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="txtCode" TabIndex="3" placeholder="Enter User Code" runat="server" ValidationGroup="FinalSave" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                    <div class=" col-lg-2">
                        <asp:Label ID="lblUsername" runat="server" Text="Username" CssClas="bold"></asp:Label>
                        <asp:Label ID="Label3" runat="server" CssClass=" bold text-danger" Text="*"></asp:Label>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDesignation" CssClass="text-danger"
                            Display="Dynamic" ErrorMessage=" (Select Designation Name)" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:TextBox ID="txtUsername" TabIndex="3" placeholder="Enter Username" runat="server" ValidationGroup="FinalSave" CssClass="form-control input-sm"></asp:TextBox>
                    </div>
                    <div class=" col-lg-3">
                        <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClas="bold"></asp:Label>
                        <asp:Label ID="lblDepStar" runat="server" CssClass=" bold text-danger" Text="*"></asp:Label>

                        <asp:RequiredFieldValidator ID="Rfv_txtDepartment" runat="server" ControlToValidate="txtDepartment" CssClass="text-danger"
                            Display="Dynamic" ErrorMessage=" (Select Department Name)" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>

                        <div class="input-group">
                            <asp:TextBox ID="txtDepartment" TabIndex="3" placeholder="Select Department" runat="server" AutoPostBack="true" OnTextChanged="txtDepartment_TextChanged"
                                AutoCompleteType="None" AutoComplete="Off" ValidationGroup="FinalSave" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <asp:HiddenField runat="server" ID="hdfDepartmentName" />
                            <asp:HiddenField runat="server" ID="hdfDepartmentID" Value="0" />
                        </div>

                        <asp:AutoCompleteExtender ID="Ace_txtDepartment" runat="server"
                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="DepartmentSearch" ServicePath="~/Service/AutoComplete.asmx"
                            OnClientItemSelected="AutoCompleteSearch" ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtDepartment">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class=" col-lg-3">
                        <asp:Label ID="lblDesignation" runat="server" Text="Designation" CssClas="bold"></asp:Label>
                        <asp:Label ID="lblDesStar" runat="server" CssClass=" bold text-danger" Text="*"></asp:Label>

                        <asp:RequiredFieldValidator ID="Rfv_txtDesignation" runat="server" ControlToValidate="txtDesignation" CssClass="text-danger"
                            Display="Dynamic" ErrorMessage=" (Select Designation Name)" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>

                        <div class="input-group">
                            <asp:TextBox ID="txtDesignation" TabIndex="3" placeholder="Select Designation" runat="server" AutoPostBack="true"
                                AutoCompleteType="None" AutoComplete="Off" ValidationGroup="FinalSave" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <asp:HiddenField runat="server" ID="hdfDesignationName" />
                            <asp:HiddenField runat="server" ID="hdfDesignationID" Value="0" />
                        </div>

                        <asp:AutoCompleteExtender ID="Ace_txtDesignation" runat="server"
                            CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                            CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" UseContextKey="true"
                            CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                            FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="DesignationSearch" ServicePath="~/Service/AutoComplete.asmx"
                            OnClientItemSelected="AutoCompleteSearch" ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtDesignation">
                        </asp:AutoCompleteExtender>
                    </div>
                    <div class="col-lg-2">
                        <asp:Label ID="lblPassword" runat="server" Text="Password" CssClas="bold"></asp:Label>
                        <asp:Label ID="Label1" runat="server" CssClass=" bold text-danger" Text="*"></asp:Label>

                        <asp:RequiredFieldValidator ID="Rfv_txtPassword" runat="server" ControlToValidate="txtDesignation" CssClass="text-danger"
                            Display="Dynamic" ErrorMessage=" (Enter Password)" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>

                        <asp:TextBox ID="txtPassword" TabIndex="3" placeholder="Enter Password" runat="server" ValidationGroup="FinalSave" CssClass="form-control input-sm"
                            TextMode="Password"></asp:TextBox>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="pnlUserDetails" runat="server">
        <ContentTemplate>
            <div class=" panel panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <asp:TabContainer runat="server" CssClass="Bigtab" ActiveTabIndex="0" ID="tabContact" Visible="true">
                            <asp:TabPanel runat="server" HeaderText="Contacts" TabIndex="11">
                                <ContentTemplate>
                                    <br />
                                    <div class="col-lg-12" id="divContact">
                                        <div class="col-lg-3">
                                            <asp:Label ID="lblFirstName" runat="server" Text="First Name" CssClas="bold"></asp:Label>
                                            <asp:TextBox ID="txtFirstName" TabIndex="3" placeholder="Enter First Name" runat="server" AutoPostBack="true"
                                                ValidationGroup="FinalSave" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>

                                        <div class="col-lg-3">
                                            <asp:Label ID="lblLastName" runat="server" Text="Last Name" CssClas="bold"></asp:Label>
                                            <asp:TextBox ID="txtLastName" TabIndex="3" placeholder="Enter Last Name" runat="server" AutoPostBack="true"
                                                ValidationGroup="FinalSave" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>

                                        <div class="col-lg-3">
                                            <asp:Label ID="lblContactNo1" runat="server" Text="Contact No.1" CssClas="bold"></asp:Label>
                                            <asp:TextBox ID="txtContactNo1" TabIndex="3" placeholder="Enter Contact No.1" runat="server" AutoPostBack="true"
                                                ValidationGroup="FinalSave" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>

                                        <div class="col-lg-3">
                                            <asp:Label ID="lblContactNo2" runat="server" Text="Contact No.2" CssClas="bold"></asp:Label>
                                            <asp:TextBox ID="txtContactNo2" TabIndex="3" placeholder="Enter Contact No.2" runat="server" AutoPostBack="true"
                                                ValidationGroup="FinalSave" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:TabPanel>
                            <asp:TabPanel runat="server" HeaderText="Address" TabIndex="15" ID="tabAddress">
                                <ContentTemplate>
                                    <br />
                                    <div class="col-lg-12" id="divAddress">
                                        <div class="col-lg-2">
                                            <asp:Label ID="lblCountry" runat="server" Text="Country" CssClas="bold"></asp:Label>
                                            <asp:TextBox ID="txtCountry" TabIndex="3" placeholder="Enter Country" runat="server" AutoPostBack="true"
                                                ValidationGroup="FinalSave" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>

                                        <div class="col-lg-2">
                                            <asp:Label ID="lblZipCode" runat="server" Text="Zip Code" CssClas="bold"></asp:Label>
                                            <asp:TextBox ID="txtZipCode" TabIndex="3" placeholder="Enter Zip Code" runat="server" AutoPostBack="true"
                                                ValidationGroup="FinalSave" CssClass="form-control input-sm"></asp:TextBox>
                                        </div>

                                        <div class="col-lg-4">
                                            <asp:Label ID="lblAddressNo1" runat="server" Text="Address No.1" CssClas="bold"></asp:Label>
                                            <asp:TextBox ID="txtAddressNo1" TabIndex="3" placeholder="Enter Address No.1" runat="server" AutoPostBack="true"
                                                ValidationGroup="FinalSave" CssClass="form-control input-sm" TextMode="MultiLine"></asp:TextBox>
                                        </div>

                                        <div class="col-lg-4">
                                            <asp:Label ID="lblAddressNo2" runat="server" Text="Address No.2" CssClas="bold"></asp:Label>
                                            <asp:TextBox ID="txtAddressNo2" TabIndex="3" placeholder="Enter Address No.2" runat="server" AutoPostBack="true"
                                                ValidationGroup="FinalSave" CssClass="form-control input-sm" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:TabPanel>
                        </asp:TabContainer>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

