﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppUtility;
using AppObjects;
public partial class M_TDS : BigSunPage
{
    AppTDSColl obj;
    protected void Page_Load(object sender, EventArgs e)
    {

        obj = new AppObjects.AppTDSColl(intRequestingUserID);
        Session[TableConstants.SessionTDS] = obj;
        if (!IsPostBack)
        {
            BindData();
        }
    }
    #region GridViewEvents

    protected void grdTDS_PreRender(object sender, EventArgs e)
    {
        if (grdTDS.Rows.Count > 0)
        {
            grdTDS.UseAccessibleHeader = true;
            grdTDS.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdTDS_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if (strCommandName == "INSERT")
        {
            TextBox txtTDSName = grdTDS.FooterRow.FindControl("txtNewTDSName") as TextBox;
            TextBox txtTDSCode = grdTDS.FooterRow.FindControl("txtNewCode") as TextBox;
            HiddenField hdfLedgerID = grdTDS.FooterRow.FindControl("hdfNewLedgerID") as HiddenField;
            CheckBox chkIsActiveTDS = grdTDS.FooterRow.FindControl("chkActiveTDS") as CheckBox;
            CheckBox chkIsDefaultTDS = grdTDS.FooterRow.FindControl("chkDefaultTDS") as CheckBox;
            SaveTDSDetails("0", txtTDSName.Text, txtTDSCode.Text, hdfLedgerID.Value, chkIsActiveTDS.Checked, chkIsDefaultTDS.Checked);

            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('TDS Successfully Added','1'" + ");", true);
            grdTDS.EditIndex = -1;
            BindData();
        }
        if (strCommandName == "TDSRate")
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
            int rowindex = row.RowIndex;
            Label lblTDSName = grdTDS.Rows[rowindex].FindControl("lblTDSName") as Label;
            txtTDS.Text = lblTDSName.Text;
            txtTdsRate.Text = "0.00";
            txtEffectiveDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            hdfTDSID.Value = new AppConvert(e.CommandArgument.ToString());
            BindTDSRate(new AppConvert(e.CommandArgument.ToString()));
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "ShowPopupn();", true);
        }
        else if (strCommandName == "CANCELADD")
        {
            grdTDS.ShowFooter = false;
            BindData();
        }
        else if (strCommandName == "CANCELEDIT")
        {
            grdTDS.EditIndex = -1;
            BindData();
        }
        else if (strCommandName == "ADDNEW")
        {
            grdTDS.EditIndex = -1;
            grdTDS.ShowFooter = true;
            BindData();
        }
    }
    protected void grdTDS_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdTDS.ShowFooter = false;
        grdTDS.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdTDS_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HiddenField hdfID = grdTDS.Rows[e.RowIndex].FindControl("hdfID") as HiddenField;
        AppTDS SingleObj = new AppTDS(intRequestingUserID);
        SingleObj.ID = new AppConvert(hdfID.Value);
        SingleObj.Status = new AppConvert((int)RecordStatus.Deleted);
        SingleObj.Save();

        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('TDS Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void grdTDS_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        HiddenField hdfTDSID = grdTDS.Rows[e.RowIndex].FindControl("hdfTDSID") as HiddenField;
        TextBox txtTDSName = grdTDS.Rows[e.RowIndex].FindControl("txtTDSName") as TextBox;
        TextBox txtTDSCode = grdTDS.Rows[e.RowIndex].FindControl("txtCode") as TextBox;
        HiddenField hdfLedgerID = grdTDS.Rows[e.RowIndex].FindControl("hdfLedgerID") as HiddenField;
        CheckBox chkIsActiveTDS = grdTDS.Rows[e.RowIndex].FindControl("chkIsActiveTDS") as CheckBox;
        CheckBox chkIsDefaultTDS = grdTDS.Rows[e.RowIndex].FindControl("chkIsDefaultTDS") as CheckBox;
        SaveTDSDetails(hdfTDSID.Value, txtTDSName.Text, txtTDSCode.Text, hdfLedgerID.Value, chkIsActiveTDS.Checked, chkIsDefaultTDS.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('TDS Successfully Updated','1'" + ");", true);
        grdTDS.EditIndex = -1;
        BindData();
    }
    #endregion
    #region MiscFunctions
    public void SaveTDSDetails(string Id, string TDSName, string Code, string Ledger_ID, bool IsActive, bool IsDefault)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppTDS SingleObj = new AppTDS(intRequestingUserID);
        SingleObj.ID = new AppConvert(Id);
        SingleObj.Name = new AppConvert(TDSName);
        SingleObj.Code = new AppConvert(Code);
        SingleObj.Ledger_ID = new AppConvert(Ledger_ID);
        SingleObj.Status = (IsActive == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        if (IsDefault == true)
        {
            obj.SetDefaultTDS();
        }
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;
        Session.Add(TableConstants.SessionTDS, obj);
        SingleObj.Save();
        BindData();
    }
    public void SaveTDSRate(string Id, string TDS_ID, string Rate, string EffectiveFrom, bool IsActive, bool IsDefault)
    {
 
        AppTDSRate SingleObj = new AppTDSRate(intRequestingUserID);
        SingleObj.ID = new AppConvert(Id);
        SingleObj.TDS_ID = new AppConvert(TDS_ID);
        SingleObj.Rate = new AppConvert(Rate);
        SingleObj.EffectiveFrom = new AppConvert(EffectiveFrom);
        SingleObj.Status = (new AppConvert((int)RecordStatus.Active));
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;
        Session.Add(TableConstants.SessionTDSRate, obj);
        SingleObj.Save();
        BindData();
    }


    protected void BindData()
    {
        AppObjects.AppTDSColl objColl = new AppTDSColl(intRequestingUserID);
        if (!string.IsNullOrEmpty(txtTDSName.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.TDS.Name, AppUtility.Operators.Equals, txtTDSName.Text, 0);
        }

        if (ddlStatus.SelectedValue != "-32768")
        {

            objColl.AddCriteria(AppObjects.TDS.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.SessionTDS] = objColl;
        if (objColl != null)
        {
            grdTDS.DataSource = objColl;
            grdTDS.DataBind();
            if (objColl.Count <= 0)
            {
                AddEmptyRow();
            }
        }


    }

    protected void BindTDSRate(int TDSID)
    {
        AppObjects.AppTDSRateColl objColl = new AppTDSRateColl(intRequestingUserID);
        objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.TDSRate.TDS_ID, AppUtility.Operators.Equals, TDSID, 0);
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.SessionTDSRate] = objColl;
        grdTdsRate.DataSource = objColl;
        grdTdsRate.DataBind();
    }

    protected bool ChackExists(int TDSID,string EffectiveDate)
    {
        AppObjects.AppTDSRateColl objColl = new AppTDSRateColl(intRequestingUserID);
        objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.TDSRate.TDS_ID, AppUtility.Operators.Equals, TDSID, 0);
        objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.TDSRate.EffectiveFrom, AppUtility.Operators.Equals, EffectiveDate, 0);
        objColl.Search(RecordStatus.ALL);
        if(objColl.Count>0)
        {
            return true;
        }
        else
        {
            return false;
        }
       
    }
    public void AddEmptyRow()
    {
        AppTDSColl collOfContacts = new AppTDSColl(intRequestingUserID);
        AppTDS newContact = new AppTDS(intRequestingUserID);
        newContact.Name = "Add atleast one TDS";
        collOfContacts.Add(newContact);
        grdTDS.DataSource = collOfContacts;
        grdTDS.DataBind();
        grdTDS.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdTDS.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdTDS.ShowFooter = true;
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }

    protected void btnSaveTdsRate_Click(object sender, EventArgs e)
    {
        if(ChackExists(new AppConvert(hdfTDSID.Value), txtEffectiveDate.Text)==true)
        {

            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('TDS rate already allocated for the selected effective date','1'" + ");", true);
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "ShowPopupn();", true);
            return;
        }
        SaveTDSRate("0", hdfTDSID.Value, txtTdsRate.Text, txtEffectiveDate.Text, true, false);
        BindTDSRate(new AppConvert(hdfTDSID.Value));
        Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "ShowPopupn();", true);
    }

    protected void grdTdsRate_PreRender(object sender, EventArgs e)
    {
        if (grdTdsRate.Rows.Count > 0)
        {
            grdTdsRate.UseAccessibleHeader = true;
            grdTdsRate.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
}