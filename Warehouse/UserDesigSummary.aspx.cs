﻿#region Author
/*
Name    :   Ronak Nair
Date    :   25/07/2016
Use     :   This form is used to display all Designation list.
*/
#endregion

using System;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;

public partial class UserDesigSummary : BigSunPage
{
    #region Global Objects
    AppDesignationTypeColl objDesignationTypeColl;
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        objDesignationTypeColl = (AppDesignationTypeColl)Session[TableConstants.DesignationData];         
        if (!IsPostBack)
        {
            CommonFunctions.BindDropDown(intRequestingUserID, Components.DepartmentType, ddlDepartment, "Name", "ID");
            CommonFunctions.BindDropDown(intRequestingUserID, Components.DesignationType, ddlReportDesign, "Name", "ID");            
            BindData();
            ShowHidePanel(2);
        }
    }
    #endregion

    #region Grid
    public void BindData()
    {
        AppDesignationTypeColl objDesignationColl = new AppDesignationTypeColl(intRequestingUserID);
        objDesignationColl.Search();
        grdDesignations.DataSource = objDesignationColl;
        grdDesignations.DataBind();
        Session.Add(TableConstants.DesignationData, objDesignationColl);
    }
    protected void grdDesignations_PreRender(object sender, EventArgs e)
    {
        if (grdDesignations.Rows.Count > 0)
        {
            grdDesignations.UseAccessibleHeader = true;
            grdDesignations.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdDesignations_RowEditing(object sender, GridViewEditEventArgs e)
    {
        SetDataForEdit(e.NewEditIndex);
    }
    protected void grdDesignations_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        objDesignationTypeColl[e.RowIndex].Status = -2;
        objDesignationTypeColl[e.RowIndex].Save();
    }
    #endregion

    #region Button Event
    protected void btnSaveDesignation_Click(object sender, EventArgs e)
    {
        AppDesignationType objDesignationType = new AppDesignationType(intRequestingUserID);
        string strCommandID = hdfDesigId.Value;
        int intCommandID = new AppConvert(strCommandID);
        if (String.IsNullOrEmpty(strCommandID) == false)
        {
            objDesignationType = objDesignationTypeColl[intCommandID];
        }
        //objDesignationType. = txtDesigCode.Text;
        objDesignationType.Parent_ID = new AppConvert(ddlReportDesign.SelectedValue);
        objDesignationType.DepartmentType_ID = new AppConvert(ddlDepartment.SelectedValue);
        objDesignationType.Name = txtDesigName.Text;
        objDesignationType.Status = new AppConvert(chkActive.Checked);
        objDesignationType.Save();
        ClearFields();
        BindData();
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        ClearFields();
        ShowHidePanel(1);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ShowHidePanel(2);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        ClearFields();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        AppDesignationTypeColl newObj = new AppDesignationTypeColl(intRequestingUserID);
        newObj.Search();
        grdDesignations.DataSource = newObj;
        grdDesignations.DataBind();
    }
    #endregion

    #region Function
    public void SetDataForEdit(int Index)
    {
        AppDesignationTypeColl obj = (AppDesignationTypeColl)Session[TableConstants.DesignationData];
        AppObjects.AppDesignationType objEdit = null;
        if (Index >= 0)
        {
            objEdit = obj[Index];
        }
        //txtDeptCode.Text = objEdit.c;
        txtDesigName.Text = objEdit.Name;
        ddlReportDesign.SelectedValue = new AppConvert(objEdit.Parent_ID);
        chkActive.Checked = new AppUtility.AppConvert(objEdit.Status);
        hdfDesigId.Value = new AppConvert(Index);
        ShowHidePanel(1);
    }
    public void ShowHidePanel(int HID)
    {

        btnCancel.Visible = false;
        btnReset.Visible = false;
        btnSaveDesignation.Visible = false;
        btnSearch.Visible = false;
        pnlDeptEntry.Visible = false;
        pnlSearch.Visible = false;
        if (grdDesignations.Rows.Count <= 0)
        {
            pnlDeptEntry.Visible = true;
            btnCancel.Visible = true;
            btnReset.Visible = true;
            btnSaveDesignation.Visible = true;
        }
        else
        {
            if (HID == 1)
            {
                pnlDeptEntry.Visible = true;
                btnCancel.Visible = true;
                btnReset.Visible = true;
                btnSaveDesignation.Visible = true;
            }
            if (HID == 2)
            {
                pnlSearch.Visible = true;
                btnSearch.Visible = true;
            }
        }
    }
    public void ClearFields()
    {
        txtDesigCode.Text = "";
        txtDesigName.Text = "";
        chkActive.Checked = true;
        hdfDesigId.Value = "";
    }
    #endregion   
}