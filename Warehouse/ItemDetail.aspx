﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="ItemMaster" Codebehind="ItemDetail.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <h4 class="text-center">
            <b>Item Entry</b>
        </h4>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="panel-title">
                        Item Details
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:Label ID="lblItemCode" runat="server" CssClass="bold" Text="Item Code"></asp:Label>
                            <asp:TextBox ID="txtItemCode" TabIndex="1" runat="server" MaxLength="30" CssClass="form-control" Enabled="true"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblItemName" runat="server" CssClass="bold " Text="Item Name"></asp:Label>
                            <asp:Label ID="lblStar" runat="server" CssClass=" bold text-danger ErrorMessage" Text=" * ">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtItemName" CssClass="text-danger"
                                    Display="Dynamic" ErrorMessage=" (Enter Item Name)" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            </asp:Label>
                            <asp:TextBox ID="txtItemName" TabIndex="2" runat="server" MaxLength="100" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save"
                                CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblItemGroup" CssClass="bold " runat="server" Text="Item Group">             
                            </asp:Label>
                            <asp:Label ID="lblitemgrp" runat="server" CssClass=" bold text-danger ErrorMessage" Text=" * "></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtItemGroup" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage=" (Select Item Group)" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:AutoCompleteExtender ID="txtItemGrp_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="SearchResources" ServicePath=""
                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtItemGroup">
                            </asp:AutoCompleteExtender>
                            <div class="input-group">
                                <asp:TextBox ID="txtItemGroup" TabIndex="3" placeholder="Select Item Group" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save"
                                    CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>                            
                            <asp:HiddenField runat="server"  ID="hdfItemGroupName"/>
                            <asp:HiddenField runat="server" ID="hdfItemGroupPkNo" Value="0" />                                
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblTax" CssClass="bold " runat="server" Text="Tax">             
                            </asp:Label>
                            <asp:Label ID="lblTaxMsg" runat="server" CssClass=" bold text-danger ErrorMessage" Text=" * "></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtItemGroup" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage=" (Select Tax)" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="SearchResources" ServicePath=""
                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtTax">
                            </asp:AutoCompleteExtender>
                            <div class="input-group">
                                <asp:TextBox ID="txtTax" TabIndex="4" placeholder="Select Tax" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save"
                                    CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>                                                       
                            <asp:HiddenField runat="server" ID="hdfTaxeNames"  Value=""/>
                            <asp:HiddenField runat="server" ID="hdfTaxPkNo"  Value="0"/>                            
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-lg-2">
                            <asp:Label ID="lblPurchasePrice" runat="server" CssClass="bold " Text="Purchase Price"></asp:Label>
                            <asp:Label ID="lblPrice" runat="server" CssClass=" bold text-danger ErrorMessage" Text=" * "></asp:Label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtPurchasePrice" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage=" (Enter Purchase Price )" ValidationGroup="save" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <div class="input-group">
                                <asp:TextBox ID="txtPurchasePrice" TabIndex="5" runat="server" MaxLength="10" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save"
                                    CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-inr"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblSalesPrice" runat="server" CssClass="bold " Text="Sales Price"></asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtSalesPrice" TabIndex="10" runat="server" MaxLength="10" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save"
                                    CssClass="form-control"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-inr"></i></span>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblItemUnit" CssClass="bold " runat="server" Text="UOM"></asp:Label>
                            <asp:DropDownList ID="ddlUOM" runat="server" CssClass="form-control">
                                <asp:ListItem Text="--Select--"></asp:ListItem>                                
                            </asp:DropDownList>
                        </div>
                    </div>
                    <p></p>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 ">
                            <asp:Label ID="lblServiceItem" runat="server" Text="Service Item" CssClass="bold "></asp:Label>
                            <div class="material-switch pull-right">
                                <asp:CheckBox runat="server" ID="chkServiceItem" />
                                <label for='<%=chkServiceItem.ClientID %>' class="label-primary"></label>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 ">
                            <asp:Label ID="lblAsset" runat="server" Text="Asset" CssClass="bold "></asp:Label>
                            <div class="material-switch pull-right">
                                <asp:CheckBox runat="server" ID="chkAsset" />
                                <label for='<%=chkAsset.ClientID %>' class="label-primary"></label>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 ">
                            <asp:Label ID="lblInventorize" runat="server" Text="Inventorize" CssClass="bold "></asp:Label>
                            <div class="material-switch pull-right">
                                <asp:CheckBox runat="server" ID="chkInventorize" />
                                <label for='<%=chkInventorize.ClientID %>' class="label-primary"></label>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 ">
                            <asp:Label ID="lblOutSource" runat="server" Text="OutSource" CssClass="bold "></asp:Label>
                            <div class="material-switch pull-right">
                                <asp:CheckBox runat="server" ID="chkOutSource" />
                                <label for='<%=chkOutSource.ClientID %>' class="label-primary"></label>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 ">
                            <asp:Label ID="lblActive" runat="server" Text="Active" CssClass="bold "></asp:Label>
                            <div class="material-switch pull-right">
                                <asp:CheckBox runat="server" ID="chkActive" />
                                <label for='<%=chkActive.ClientID %>' class="label-primary"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class=" col-lg-12 col-md-12 col-sm-12">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:TabContainer runat="server" ID="tsItemDetails" CssClass="Bigtab" ActiveTabIndex="0">
                        <asp:TabPanel runat="server" HeaderText="Item Ledger" ID="tpItemLedger" >
                            <ContentTemplate>
                                <div class="row">
                                    <iframe runat="server" src="ItemLedger.aspx?Master=Simple" id="ifrmSearch" style="height: 400px;" class="col-lg-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>
                        <asp:TabPanel runat="server" HeaderText="Basic Info">
                            <ContentTemplate>
                                <div class="row">
                                    <iframe runat="server" src="ItemBasicInfo.aspx?Master=Simple" id="IfrmSample" style="height: 400px;" class=" col-lg-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>
                        <asp:TabPanel runat="server" HeaderText="Attachments">
                            <ContentTemplate>
                                <div class="row">
                                    <iframe runat="server" src="ItemAttachments.aspx?Master=Simple" id="IframeInfo" style="height: 400px;" class=" col-lg-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>
                    </asp:TabContainer>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <!-- Div for footer  -->
    <div class="navbar navbar-fixed-bottom navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#divFooter">
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="divFooter" class="navbar-collapse collapse">
            <div class="container">
                <ul class="nav navbar-nav navbar-right" style="padding-top: 0.7%;">
                    <li>                      
                        <div class="col-sm-1">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary btn-sm " />
                        </div>
                    </li>
                    <li>                        
                        <div class="col-sm-1">
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-default btn-sm " />
                        </div>
                    </li>
                    <li>                        
                        <div class="col-sm-1">
                            <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-success btn-sm " />
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end footer -->
</asp:Content>
