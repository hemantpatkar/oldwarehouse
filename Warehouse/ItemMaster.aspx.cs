﻿#region Author
/*Name    :   Pradip Bobhate
Date      :   28/07/2016*/
#endregion
#region Using
using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
#endregion
public partial class ItemMaster : BigSunPage
{
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CommonFunctions.BindDropDown(intRequestingUserID, Components.UOMType, ddlUOM, "Name", "ID");
            string ID = Convert.ToString(Request.QueryString["ItemId"]);

            if (!string.IsNullOrEmpty(ID))
            {
                int ItemId = new AppConvert(ID);
                AppItem objItem = new AppItem(intRequestingUserID, ItemId);
                txtItemCode.Text = objItem.Code;
                txtItemName.Text = objItem.Name;
                txtItemCategory.Text = objItem.CategoryType.Code + " - " + objItem.CategoryType.Name;
                hdfItemCategoryName.Value = objItem.CategoryType.Code + " - " + objItem.CategoryType.Name;
                hdfItemCategoryID.Value = new AppConvert(objItem.CategoryType_ID);

                hdfSalesLedgerID.Value = new AppConvert(objItem.Sales_Ledger_ID);
                hdfSalesLedgerName.Value = new AppConvert(objItem.Sales_Ledger.LedgerName);
                txtSalesLedger.Text = hdfSalesLedgerName.Value;


                hdfSalesTaxID.Value = new AppConvert(objItem.Sales_TaxType_ID);
                hdfSalesTaxName.Value = new AppConvert(objItem.Sales_TaxType.Name);
                txtSalesTax.Text = hdfSalesTaxName.Value;

                hdfPurchaseLedgerID.Value = new AppConvert(objItem.Purchase_Ledger_ID);
                hdfPurchaseLedgerName.Value = new AppConvert(objItem.Purchase_Ledger.LedgerName);
                txtPurchaseLedger.Text = hdfPurchaseLedgerName.Value;


                hdfPurchaseTaxID.Value = new AppConvert(objItem.Purchase_TaxType_ID);
                hdfPurchaseTaxName.Value = new AppConvert(objItem.Purchase_TaxType.Name);
                txtPurchaseTax.Text = hdfPurchaseTaxName.Value;


                ddlUOM.SelectedValue = new AppConvert(objItem.UOMType_ID);
                ddlItemMode.SelectedValue = new AppConvert(objItem.ItemMode);
                ddlItemType.SelectedValue = new AppConvert(objItem.ItemType);
                ddlType.SelectedValue = new AppConvert(objItem.Type);
                txtItemDescription.Text = objItem.Description;
                ddlType.SelectedValue = new AppConvert(objItem.Type);
                chkActive.Checked = new AppConvert(objItem.Status);
                hdfID.Value = new AppConvert(objItem.ID);
                LedgerType(objItem.ID);
            }
            else
            {
                ItemMode();
                LedgerType();
            }
            txtItemCode.Focus();
        }
    }
    #endregion
    #region Function
    public int CreateLedger(int Parent_ID)
    {
        AppLedgerColl objLedgerColl = (AppLedgerColl)HttpContext.Current.Session[TableConstants.LedgerTreeSessionObj];
        AppLedger objLedger = new AppLedger(intRequestingUserID);
        objLedger.ID = 0;
        objLedger.LedgerName = txtItemName.Text;
        objLedger.AccountNo = "1000000001";
        objLedger.LedgerCode = txtItemCode.Text;
        objLedger.Parent_ID = new AppConvert(Parent_ID);
        //objLedger.AccountType_ID = new AppConvert(objLedgerColl.FirstOrDefault(x=> x.Parent_ID== Parent_ID).AccountType_ID);
        objLedger.AccountType_ID = AppLedgerColl.GetAccountType(Parent_ID);

        objLedger.LedgerType_ID = 7;
        objLedger.Status = (chkActive.Checked == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        objLedger.LedgerFlag = new AppConvert((int)LedgerFlag.GeneralLedger);
        if (objLedgerColl != null)
        {
            objLedger.SortOrder = new AppConvert(objLedgerColl.FindLastIndex(o => o.Parent_ID == objLedger.Parent_ID));
        }
        objLedger.ModifiedBy = intRequestingUserID;
        objLedger.ModifiedOn = System.DateTime.Now;
        objLedger.Save();
        return objLedger.ID;
    }
    public void ItemMode(int ID = 0)
    {
        if (ID != 0)
        {
            chkIsNewPurchaseLedger.Visible = false;
            chkIsNewPurchaseLedger.Checked = false;
            chkIsNewSalesLedger.Checked = false;
            chkIsNewSalesLedger.Visible = false;
            chkSameAs.Visible = false;
            chkSameAs.Checked = false;

            lblPurchaseLedger.Text = "Purchase Ledger";
            lblPurchaseLedger.Visible = true;
            lblSalesLedger.Text = "Sales Ledger";
            lblSalesLedger.Visible = true;
        }
        switch (ddlItemMode.SelectedValue)
        {
            case "0":
                divPurchase.Visible = true;
                divSales.Visible = false;
                chkSameAs.Visible = false;
                break;
            case "1":
                divPurchase.Visible = true;
                divSales.Visible = true;
                divSalesLedger.Visible = chkSameAs.Checked == true ? false : true;
                chkSameAs.Visible = ID == 0 ? true : false;
                break;
            case "2":
                divPurchase.Visible = false;
                divSales.Visible = true;
                divSalesLedger.Visible = true;
                chkSameAs.Visible = false;
                break;
            default:
                divPurchase.Visible = true;
                divSales.Visible = false;
                chkSameAs.Visible = false;
                break;
        }
    }
    public void LedgerType(int ID=0)
    {


        ItemMode(ID);
        if (ddlItemMode.SelectedValue == "0")
        {
            divPurchase.Visible = true;
        }
        if (chkIsNewSalesLedger.Checked)
        {

            lblSalesLedger.Text = "Sales Parent Ledger";
            ACEtxtSalesLedger.ContextKey = "," + (new AppUtility.AppConvert((int)LedgerFlag.ControlAccount) + "~" + new AppUtility.AppConvert((int)LedgerFlag.LedgerGroup) + "~");
        }
        else
        {
            lblSalesLedger.Text = "Sales Ledger";
            ACEtxtSalesLedger.ContextKey = "7," + ("~" + "~" + new AppUtility.AppConvert((int)LedgerFlag.GeneralLedger));
        }

        if (chkIsNewPurchaseLedger.Checked) 
        {

            lblPurchaseLedger.Text = "Purchase Parent Ledger";
            ACEtxtPurchaseLedger.ContextKey = "," + (new AppUtility.AppConvert((int)LedgerFlag.ControlAccount) + "~" + new AppUtility.AppConvert((int)LedgerFlag.LedgerGroup) + "~");
        }
        else
        {
            lblPurchaseLedger.Text = "Purchase Ledger";
            ACEtxtPurchaseLedger.ContextKey = "7," + ("~" + "~" + new AppUtility.AppConvert((int)LedgerFlag.GeneralLedger));
        }
    }
    #endregion
    #region Button Click Events     
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppItem objItem = new AppItem(intRequestingUserID);
        string strCommandID = hdfID.Value;
        if (String.IsNullOrEmpty(strCommandID) == false)
        {
            objItem.ID = new AppConvert(strCommandID);
        }
        objItem.CategoryType_ID = new AppConvert(hdfItemCategoryID.Value);
        objItem.Name = txtItemName.Text;
        objItem.Code = txtItemCode.Text;
        objItem.UOMType_ID = new AppConvert(ddlUOM.SelectedValue);
        objItem.Description = txtItemDescription.Text;
        objItem.ItemMode = new AppConvert(ddlItemMode.SelectedValue);
        objItem.ItemType = new AppConvert(ddlItemType.SelectedValue);
        objItem.Status = new AppConvert(chkActive.Checked);
        objItem.Type = new AppConvert(ddlType.SelectedValue);
        if (objItem.ItemMode == 0 || objItem.ItemMode == 1)
        {
            if (chkIsNewPurchaseLedger.Checked)
            {
                objItem.Purchase_Ledger_ID = CreateLedger(new AppConvert(hdfPurchaseLedgerID.Value));
            }
            else
            {
                objItem.Purchase_Ledger_ID = new AppConvert(hdfPurchaseLedgerID.Value);
                objItem.Purchase_Ledger.DeAttach(Ledger.ID);
            }
        }
        else
        {
            objItem.Purchase_Ledger_ID = 0;

        }
       
        if (chkSameAs.Checked == false)
        {
            if (objItem.ItemMode == 1 || objItem.ItemMode == 2)
            {
                if (chkIsNewSalesLedger.Checked)
                {
                    objItem.Sales_Ledger_ID = CreateLedger(new AppConvert(hdfSalesLedgerID.Value));
                }
                else
                {
                    objItem.Sales_Ledger_ID = new AppConvert(hdfSalesLedgerID.Value);
                    objItem.Sales_Ledger.DeAttach(Ledger.ID);
                }
            }
            else
            {
                objItem.Sales_Ledger_ID = 0;

            }
        }
        else
        {
            objItem.Sales_Ledger_ID = objItem.Purchase_Ledger_ID;
        }
        objItem.Purchase_TaxType_ID = new AppConvert(hdfPurchaseTaxID.Value);
        objItem.Sales_TaxType_ID = new AppConvert(hdfSalesTaxID.Value);
        objItem.Save();
        Response.Redirect("ItemMaster.aspx?ItemId=" + objItem.ID);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ItemSummary.aspx");
    }
    #endregion

    protected void chkIsNewPurchaseLedger_CheckedChanged(object sender, EventArgs e)
    {
        LedgerType();
    }

    protected void chkIsNewSalesLedger_CheckedChanged(object sender, EventArgs e)
    {
        LedgerType();
    }

    protected void ddlItemMode_SelectedIndexChanged(object sender, EventArgs e)
    {
        ItemMode();
    }

    protected void chkSameAs_CheckedChanged(object sender, EventArgs e)
    {
        ItemMode();
    }
}