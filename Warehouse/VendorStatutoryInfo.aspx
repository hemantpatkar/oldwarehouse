﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="VendorStatutoryInfo" Codebehind="VendorStatutoryInfo.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
             <h3>Statutory Info</h3>
            <hr />
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdVendorStatuInfo" runat="server" AutoGenerateColumns="False"
                        OnPreRender="grdVendorStatuInfo_PreRender"
                        CssClass="table table-hover table-striped  text-nowrap nowrap">
                        <Columns>                        
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblType" Text="Type" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%-- <asp:Label ID="lblTypeText" runat="server" Text='<%# Eval("Name") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>--%>
                                    <asp:Label ID="txtType" runat="server" Text='<%# Eval("StatutoryType.Name") %>' TabIndex="2" CssClass="bold"></asp:Label>
                                    <asp:HiddenField runat="server" ID="hdfStatutoryId" Value='<%# Eval("StatutoryType.ID") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblValue" Text="Value" runat="server" CssClass="bold"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%--                            <asp:Label ID="lblValueText" runat="server" Text='<%# Eval("ExpressionServer") %>'  CssClass="bold"></asp:Label>--%>
                                    <asp:TextBox ID="txtValue" runat="server" AutoPostBack="true" OnTextChanged="txtValue_TextChanged" Text='<%# Eval("Value") %>' CssClass="form-control input-sm"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RFV_itxtValue" runat="server" ControlToValidate="txtValue"
                                        Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*" ValidationGroup="StatutoryValue"></asp:RequiredFieldValidator>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="grdVendorStatuInfo" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdVendorStatuInfo.ClientID%>');
        $(document).ready(function () {
            //GridUI(GridID, 50);
        })
    </script>
</asp:Content>
