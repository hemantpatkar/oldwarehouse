﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class PriceGroup : BigSunPage
{
    AppPriceGroup objPriceGroup = null;
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {   
        objPriceGroup = new AppPriceGroup(intRequestingUserID);
        if (!IsPostBack)
        {   
            Session.Add(TableConstants.PriceGroupConst,objPriceGroup);
            string ID = Convert.ToString(Request.QueryString["PriceGroupID"]);
            if(string.IsNullOrEmpty(ID)==false)
            {
                int VendorID = new AppUtility.AppConvert(ID);
                AppPriceGroup objCompany = new AppPriceGroup(VendorID);
                txtPriceGrpCode.Text = objCompany.Code;
                txtPriceGrpName.Text = objCompany.Name;                
                Session.Add(TableConstants.PriceGroupConst, objCompany);
            }
            BindData();
        }
    }   
    #endregion
    #region Miscellenous Functions
    public void BindData()
    {
        objPriceGroup = (AppPriceGroup)Session[TableConstants.PriceGroupConst];
        if (objPriceGroup != null)
        {
            grdPriceGrpItems.DataSource = objPriceGroup.PriceGroupItemColl;
            grdPriceGrpItems.DataBind();
        }
        if (objPriceGroup.PriceGroupItemColl.Count <= 0)
        {
            AddEmptyRow();
        }
    }  
    public void AddEmptyRow()
    {
        AppPriceGroupItemColl PriceGroupItemCollection = new AppPriceGroupItemColl(intRequestingUserID);
        AppPriceGroupItem newPriceGroupItem = new AppPriceGroupItem(intRequestingUserID);
        newPriceGroupItem.Item.Name = "";
        newPriceGroupItem.UOMType.Name = "";
        PriceGroupItemCollection.Add(newPriceGroupItem);
        grdPriceGrpItems.DataSource = PriceGroupItemCollection;
        grdPriceGrpItems.DataBind();
        int columncount = grdPriceGrpItems.Rows[0].Cells.Count;
        grdPriceGrpItems.Rows[0].Cells.Clear();
        grdPriceGrpItems.Rows[0].Cells.Add(new TableCell());
        grdPriceGrpItems.Rows[0].Cells[0].ColumnSpan = columncount;
        grdPriceGrpItems.Rows[0].Cells[0].Text = "Add atleast one Price Group Item";
        //grdPriceGrpItems.Rows[0].FindControl("lbtnEdit").Visible = false;
        //grdPriceGrpItems.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdPriceGrpItems.ShowFooter = true;
    }  
    #endregion
    #region GridView Events
    protected void grdPriceGrpItems_PreRender(object sender, EventArgs e)
    {
        if (grdPriceGrpItems.Rows.Count > 0)
        {
            grdPriceGrpItems.UseAccessibleHeader = true;
            grdPriceGrpItems.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdPriceGrpItems_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grdPriceGrpItems.EditIndex = -1;
    }
    protected void grdPriceGrpItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if(strCommandName=="INSERT")
        {            
            HiddenField hdfNewItemTypeID = grdPriceGrpItems.FooterRow.FindControl("hdfNewItemTypeID") as HiddenField;
            TextBox txtNewItemRate = grdPriceGrpItems.FooterRow.FindControl("txtNewItemRate") as TextBox;
            TextBox txtNewMRP = grdPriceGrpItems.FooterRow.FindControl("txtNewMRP") as TextBox;
            TextBox txtNewDiscount = grdPriceGrpItems.FooterRow.FindControl("txtNewDiscount") as TextBox;
            TextBox txtNewFromDate = grdPriceGrpItems.FooterRow.FindControl("txtNewFromDate") as TextBox;
            TextBox txtNewToDate = grdPriceGrpItems.FooterRow.FindControl("txtNewToDate") as TextBox;            
            DropDownList ddlNewCurrency = grdPriceGrpItems.FooterRow.FindControl("ddlNewCurrency") as DropDownList;
            DropDownList ddlNewUOM = grdPriceGrpItems.FooterRow.FindControl("ddlNewUOM") as DropDownList;            
            CheckBox chkIsActive = grdPriceGrpItems.FooterRow.FindControl("chkIsActive") as CheckBox;
            SavePriceGroupItemsDetails(strCommandID, hdfNewItemTypeID.Value, txtNewItemRate.Text, txtNewMRP.Text, txtNewDiscount.Text, txtNewFromDate.Text, 
                txtNewToDate.Text, ddlNewCurrency.SelectedValue,ddlNewUOM.SelectedValue, chkIsActive.Checked);
        }
    }
    protected void grdPriceGrpItems_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Footer:               
                DropDownList ddlNewCurrency = (DropDownList)e.Row.FindControl("ddlNewCurrency");
                CommonFunctions.BindCurrencyTypes(ddlNewCurrency, "Name", "ID");
                DropDownList ddlNewUOM = (DropDownList)e.Row.FindControl("ddlNewUOM");                
                CommonFunctions.BindDropDown(intRequestingUserID, Components.UOMType, ddlNewUOM, "Name", "ID");
                break;
            case DataControlRowType.DataRow:               
                DropDownList ddlCurrency = (DropDownList)e.Row.FindControl("ddlCurrency");
                if (ddlCurrency != null)
                {
                    CommonFunctions.BindCurrencyTypes(ddlCurrency, "Name", "ID");
                    ddlCurrency.SelectedValue = new AppConvert(objPriceGroup.PriceGroupItemColl[e.Row.RowIndex].CurrencyType_ID);
                }
                DropDownList ddlUOM = (DropDownList)e.Row.FindControl("ddlUOM");
                if (ddlUOM != null)
                {                    
                    CommonFunctions.BindDropDown(intRequestingUserID, Components.UOMType, ddlUOM, "Name", "ID");
                    ddlUOM.SelectedValue = new AppConvert(objPriceGroup.PriceGroupItemColl[e.Row.RowIndex].UOMType_ID);
                }
                break;
            default:
                break;
        }
    }
    protected void grdPriceGrpItems_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        objPriceGroup = (AppPriceGroup)Session[TableConstants.PriceGroupConst];
        objPriceGroup.PriceGroupItemColl[e.RowIndex].Status = new AppConvert(RecordStatus.Deleted);
        Session.Add(TableConstants.PriceGroupConst, objPriceGroup);            
    }
    protected void grdPriceGrpItems_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdPriceGrpItems.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdPriceGrpItems_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strCommandID = new AppConvert(e.RowIndex);
        HiddenField hdfItemTypeID = grdPriceGrpItems.Rows[e.RowIndex].FindControl("hdfItemTypeID") as HiddenField;
        TextBox txtItemRate = grdPriceGrpItems.Rows[e.RowIndex].FindControl("txtItemRate") as TextBox;
        TextBox txtMRP = grdPriceGrpItems.Rows[e.RowIndex].FindControl("txtMRP") as TextBox;
        TextBox txtDiscount = grdPriceGrpItems.Rows[e.RowIndex].FindControl("txtDiscount") as TextBox;
        TextBox txtFromDate = grdPriceGrpItems.Rows[e.RowIndex].FindControl("txtFromDate") as TextBox;
        TextBox txtToDate = grdPriceGrpItems.Rows[e.RowIndex].FindControl("txtToDate") as TextBox;
        DropDownList ddlCurrency = grdPriceGrpItems.Rows[e.RowIndex].FindControl("ddlCurrency") as DropDownList;
        DropDownList ddlUOM = grdPriceGrpItems.Rows[e.RowIndex].FindControl("ddlUOM") as DropDownList;
        CheckBox chkActive = grdPriceGrpItems.Rows[e.RowIndex].FindControl("chkActive") as CheckBox;
        SavePriceGroupItemsDetails(strCommandID, hdfItemTypeID.Value, txtItemRate.Text, txtMRP.Text, txtDiscount.Text, txtFromDate.Text, txtToDate.Text, 
            ddlCurrency.SelectedValue, ddlUOM.SelectedValue, chkActive.Checked);               
        grdPriceGrpItems.EditIndex = -1;
    }
    #endregion
    #region SavePriceGroupItemsDetails
    public void SavePriceGroupItemsDetails(string Id, string ItemTypeID, string ItemRate, string MRP, string Discount, string FromDate,
        string ToDate, string Currency, string UOM, bool IsActive)
    {
        objPriceGroup = (AppPriceGroup)Session[TableConstants.PriceGroupConst];
        string strCommandID = Id;
        int intCommandID = new AppUtility.AppConvert(strCommandID);
        AppPriceGroupItem SingleObj = new AppPriceGroupItem(intRequestingUserID);
        if (String.IsNullOrEmpty(strCommandID) == false)
        {
            SingleObj = objPriceGroup.PriceGroupItemColl[intCommandID];
        }         
        SingleObj.PriceGroup_ID = objPriceGroup.ID;
        SingleObj.Item_ID = new AppConvert(ItemTypeID);
        SingleObj.Rate = new AppConvert(ItemRate);
        SingleObj.MRP = new AppUtility.AppConvert(MRP);
        SingleObj.Discount = new AppUtility.AppConvert(Discount);
        SingleObj.FromDate = new AppUtility.AppConvert(FromDate);
        SingleObj.ToDate = new AppUtility.AppConvert(ToDate);
        SingleObj.CurrencyType_ID = new AppUtility.AppConvert(Currency);
        SingleObj.UOMType_ID = new AppUtility.AppConvert(UOM);
        SingleObj.Status = new AppUtility.AppConvert(IsActive);
        //SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;
        objPriceGroup.AddNewPriceGroupItem(SingleObj);
        //add new object in collection.           
        Session.Add(TableConstants.PriceGroupConst, objPriceGroup);
        BindData();
        grdPriceGrpItems.ShowFooter = false;
    }
    #endregion
    #region Button Click Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppPriceGroup objVendor = (AppPriceGroup)Session[TableConstants.PriceGroupConst];
        if (objVendor == null)
        {
            objVendor = new AppPriceGroup(intRequestingUserID);
        }
        objVendor.Code = txtPriceGrpCode.Text;
        objVendor.Name = txtPriceGrpName.Text;
        objVendor.Status = new AppConvert(chkActive.Checked);        
        objVendor.Save();
        Session.Remove(TableConstants.PriceGroupConst);
        Response.Redirect("PriceGroupSummary.aspx");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.PriceGroupConst);
        Response.Redirect("PriceGroupSummary.aspx");
    }   
    protected void lbtnAddNew_Click(object sender, EventArgs e)
    {
        grdPriceGrpItems.ShowFooter = true;
        BindData();
    }
    #endregion
}