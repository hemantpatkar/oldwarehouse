﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data.Common;
using System.Drawing;
using AppObjects;
using AppUtility;
using System.IO;

public partial class StockInSearch : BigSunPage
{
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    #endregion

    #region Button Event
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockInDetail.aspx");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Search();
    }

    protected void Search()
    {
        AppInwardColl objAppGateEntryColl = new AppInwardColl(intRequestingUserID);

        #region DateValidation
        DateTime fromdate, to;
        if (!string.IsNullOrEmpty(txtfromdate.Text.Trim()) && !string.IsNullOrEmpty(txttodate.Text.Trim()))
        {
            fromdate = new AppConvert(txtfromdate.Text);
            to = new AppConvert(txttodate.Text);
            if (fromdate > to)
            {
                grdInwardSearch.DataSource = null;
                grdInwardSearch.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please Select Validate Date range..!" + "','2'" + ");", true);
                return;
            }
        }

        #endregion

        #region Code By Hemant On 24-12-2012

        if (ddlStatus.SelectedValue == "0")
        {
            if (txtfromdate.Text.Trim() != "" && txttodate.Text.Trim() != "" && txtfromdate.Text.Trim() != "__/__/____" && txttodate.Text.Trim() != "__/__/____")
            {
                fromdate = Convert.ToDateTime(txtfromdate.Text);
                to = Convert.ToDateTime(txttodate.Text + " 23:59:59");
                objAppGateEntryColl.AddCriteria(Inward.InwardDate, Operators.GreaterOrEqualTo, fromdate);
                objAppGateEntryColl.AddCriteria(Inward.InwardDate, Operators.LessOrEqualTo, to);
            }
            else if (txtfromdate.Text.Trim() != "" && txtfromdate.Text.Trim() != "__/__/____" && (txttodate.Text.Trim() == "" || txttodate.Text.Trim() == "__/__/____"))
            {
                fromdate = Convert.ToDateTime(txtfromdate.Text);

                objAppGateEntryColl.AddCriteria(Inward.InwardDate, Operators.GreaterOrEqualTo, fromdate);
            }
            else if (txttodate.Text.Trim() != "" && txttodate.Text.Trim() != "__/__/____" && (txtfromdate.Text.Trim() == "" || txtfromdate.Text.Trim() == "__/__/____"))
            {
                to = Convert.ToDateTime(txttodate.Text + " 23:59:59");

                objAppGateEntryColl.AddCriteria(Inward.InwardDate, Operators.LessOrEqualTo, to);
            }
        }
        else if (ddlStatus.SelectedValue == "1")
        {

        }
        else
        {

        }

        if (txtPreInwardNo.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(Inward.InwardNo, Operators.Like, txtPreInwardNo.Text);
        }

        if (txtCustomer.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(Inward.Customer_Company_ID, Operators.Like, txtCustomer.Text);
        }

        if (txtReceivedDate.Text.Trim() != "")
        {
            objAppGateEntryColl.AddCriteria(Inward.ReceivedDate, Operators.Like, txtReceivedDate.Text);
        }
        //if (txtChallanNo.Text.Trim() != "")
        //{
        //    objAppGateEntryColl.AddCriteria(PreInward.DriverName, Operators.Like, txtChallanNo.Text);
        //}        
        if (ddlStatus.SelectedValue != "-1")
        {
            int status = Convert.ToInt32(ddlStatus.SelectedValue);
            objAppGateEntryColl.AddCriteria(Inward.Status, Operators.Equals, status); //
        }

        objAppGateEntryColl.Search();
        //objAppGateEntryColl.Sort(AppPreInward.ComparisionDesc);
        grdInwardSearch.DataSource = objAppGateEntryColl;
        grdInwardSearch.DataBind();


        #endregion
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (grdInwardSearch.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Doesnot contain data" + "','2'" + ");", true);
            return;
        }

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grdInwardSearch.AllowPaging = false;
            grdInwardSearch.HeaderRow.BackColor = Color.White;
            foreach (TableCell cell in grdInwardSearch.HeaderRow.Cells)
            {
                cell.BackColor = grdInwardSearch.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in grdInwardSearch.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = grdInwardSearch.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = grdInwardSearch.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            grdInwardSearch.RenderControl(hw);
            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    #endregion

    #region Grid
     
    protected void grdInwardSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdInwardSearch.PageIndex = e.NewPageIndex;
        Search();
    }
    #endregion


}