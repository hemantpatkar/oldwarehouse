﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using AppUtility;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class R_ReportPrint : BigSunPage
{
    static string CompanyID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CommonFunctions CF = new CommonFunctions();
            string path = ConfigurationManager.AppSettings["ReportPath"];
            CompanyID = new AppConvert(Session[TableConstants.CompanyIdConst]);
            string url = path + CF.GetReportName("10002", CompanyID); // Fetch Path from Database
            string ReportOutput = CF.GetReportOutput("10002", CompanyID); // Fetch Path from Database
            ViewReport(url, ReportOutput);
        }
    }


    protected void PrintReport(string ReportName, List<ReportParameter> parameters, ReportViewer reportView)
    {
        HttpContext context = HttpContext.Current;
        if (!context.Response.Buffer)
            return;
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string extension;
        // Set the report server URL and report path
        string _reportServerUrl = ConfigurationManager.AppSettings["ReportServerUrl"];
        string _reportPath = ConfigurationManager.AppSettings["ReportPath"];

        // Set the processing mode for the ReportViewer to Remote
        reportView.ProcessingMode = ProcessingMode.Remote;
        ServerReport serverReport = reportView.ServerReport;
        serverReport.ReportServerUrl = new Uri(_reportServerUrl);
        serverReport.ReportPath = _reportPath + ReportName;
        reportView.ServerReport.SetParameters(parameters);
        reportView.ExportContentDisposition = ContentDisposition.AlwaysInline;

        byte[] pdfContent = reportView.ServerReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
        context.Response.Buffer = true;
        context.Response.ContentType = "application/pdf";
        context.Response.AddHeader("Content-disposition", string.Format("inline; filename={0}.{1}", ReportName, extension));
        context.Response.BinaryWrite(pdfContent);
        context.Response.Flush();
        context.Response.End();
    }

    public void ViewReport(string url, string ReportOutput = "NONE")
    {
        HttpContext context = HttpContext.Current;
        Warning[] warnings;
        string[] streamids;
        string mimeType, encoding, extension, ReportName = "";
        string ID = "3";
        ReportViewer1.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerURLEmbedded"].ToString());
        ReportViewer1.ServerReport.ReportPath = url;
        var parameters = new List<ReportParameter>();
        parameters.Add(new ReportParameter("ID", ID));
        parameters.Add(new ReportParameter("CompanyID", CompanyID));
        ReportViewer1.ServerReport.SetParameters(parameters);
        ReportViewer1.ProcessingMode = ProcessingMode.Remote;
        //Hide toolbar
        ReportViewer1.ShowParameterPrompts = false;
        //Hide Export Bar
        ReportViewer1.ShowPromptAreaButton = false;

        if (ReportOutput.ToUpper() == "NONE")
        {
            ReportViewer1.ServerReport.Refresh();
        }
        else
        {
            ReportViewer1.ExportContentDisposition = ContentDisposition.AlwaysInline;
            byte[] reportContent = ReportViewer1.ServerReport.Render(ReportOutput, null, out mimeType, out encoding, out extension, out streamids, out warnings);
            context.Response.Buffer = true;
            context.Response.ContentType = "application/" + ReportOutput;
            context.Response.AddHeader("Content-disposition", string.Format("inline; filename={0}.{1}", ReportName, extension));//Passed customized report name here
            context.Response.BinaryWrite(reportContent);
            context.Response.Flush();
            context.Response.End();
        }

    }
}