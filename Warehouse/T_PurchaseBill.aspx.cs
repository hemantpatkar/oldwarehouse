﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class T_PurchaseBill : BigSunPage
{
    #region PageLoad
    AppPurchaseGRN PurchaseGRN = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        Ce_PurchaseBillDate.SelectedDate = DateTime.Now;
        if (!IsPostBack)
        {
            ACEtxtVendor.ContextKey = "2" + ",0";
            PurchaseGRN = new AppPurchaseGRN(intRequestingUserID);
            PurchaseGRN.BillType = new AppConvert(Request.QueryString["BillType"]);
            string ID = Convert.ToString(Request.QueryString["PurchaseBillID"]);
            divPurchaseOrder.Visible = PurchaseGRN.BillType == 1 ? false : true;
            if (!string.IsNullOrEmpty(ID))
            {
                int PurchaseBillID = new AppUtility.AppConvert(ID);
                PurchaseGRN = new AppObjects.AppPurchaseGRN(intRequestingUserID, PurchaseBillID);
                PopulateData();
            }
            else
            {
                DefaultData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
            }
            Session.Add(TableConstants.PurchaseGRN, PurchaseGRN);
            BindData();
            SetValueToGridFooter();
        }
    } 
    #endregion
    #region Text Box Event
    protected void txtItem_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        HiddenField hdfItemID = (HiddenField)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("hdfItemID");
        TextBox txtQuantity = (TextBox)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("txtQuantity");
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].Item_ID = new AppUtility.AppConvert(hdfItemID.Value);
        LineItemCalculation(RowIndex);
        txtQuantity.Focus();
    }
    protected void txtQuantity_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtRate = (TextBox)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("txtRate");
        LineItemCalculation(RowIndex);
        txtRate.Focus();
    }
    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtDiscount = (TextBox)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("txtDiscount");
        LineItemCalculation(RowIndex);
        txtDiscount.Focus();
    }
    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtLineItemTax = (TextBox)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("txtLineItemTax");
        LineItemCalculation(RowIndex);
        txtLineItemTax.Focus();
    }
    protected void txtLineItemTax_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        Button btnAddLineItem = (Button)grdPurchaseBillItemDetails.HeaderRow.FindControl("btnAddLineItem");
        LineItemCalculation(RowIndex);
        btnAddLineItem.Focus();
    }
    protected void btnAddLineItem_Click(object sender, EventArgs e)
    {
        AddEmptyRow(1);
        if (grdPurchaseBillItemDetails.Rows.Count >= 0)
        {
            TextBox txtItem = (TextBox)grdPurchaseBillItemDetails.Rows[0].FindControl("txtItem");
            txtItem.Focus();
        }
    }
    protected void txtPurchaseOrder_TextChanged(object sender, EventArgs e)
    {
        PurchaseGRN = (AppPurchaseGRN)HttpContext.Current.Session[TableConstants.PurchaseGRN];
        PurchaseGRN.PurchaseOrder_ID = new AppConvert(hdfPurchaseOrderID.Value);
        PurchaseGRN.PurchaseOrder = new AppPurchaseOrder(intRequestingUserID, PurchaseGRN.PurchaseOrder_ID);
        PurchaseGRN.BillType = new AppConvert(Request.QueryString["BillType"]);
        txtTotalAmount.Text = new AppConvert(PurchaseGRN.PurchaseOrder.FinalAmount);
        txtTermsAndConditions.Text = new AppConvert(PurchaseGRN.PurchaseOrder.TermsAndConditions.Statement);
        hdfTermsAndConditionsName.Value = new AppConvert(PurchaseGRN.PurchaseOrder.TermsAndConditions.Statement);
        hdfTermsAndConditionsID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.TermsAndConditions.ID);
        txtDiscount.Text = new AppConvert(PurchaseGRN.PurchaseOrder.DiscountValue);
        hdfDiscountType.Value = new AppConvert(PurchaseGRN.PurchaseOrder.DiscountType == 0 ? 1 : PurchaseGRN.PurchaseOrder.DiscountType);
        txtHeaderExchangeCurrency.Text = PurchaseGRN.PurchaseOrder.CurrencyType.Name;
        hdfHeaderExchangeCurrencyName.Value = new AppConvert(PurchaseGRN.PurchaseOrder.CurrencyType.Name);
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.CurrencyType_ID);
        txtDeliveryAddress.Text = PurchaseGRN.GetDeliveryAddress;
        hdfDeliveryAddressName.Value = new AppConvert(PurchaseGRN.GetDeliveryAddress);
        hdfDeliveryAddressID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.Delivery_CompanyAddress_ID);
        txtCompanyAddress.Text = PurchaseGRN.SetCompanyAddress;
        hdfCompanyAddressID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.CompanyAddress_ID);
        txtRemark.Text = new AppConvert(PurchaseGRN.PurchaseOrder.Remark);
        hdfCompanyAddressName.Value = PurchaseGRN.SetCompanyAddress;
        txtHeaderTax.Text = new AppConvert(PurchaseGRN.SetTaxType);
        hdfHeaderTaxName.Value = new AppConvert(PurchaseGRN.SetTaxType);
        hdfHeaderTaxID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.TaxType_ID);
        txtHeaderTaxAmount.Text = new AppConvert(PurchaseGRN.PurchaseOrder.TaxAmount);
        hdfVendorID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.Vendor_Company_ID);
        txtVendorAddress.Text = PurchaseGRN.SetVendorAddress;
        hdfVendorAddressName.Value = new AppConvert(PurchaseGRN.SetVendorAddress);
        hdfVendorAddressID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.Vendor_CompanyAddress_ID);
        PurchaseGRN.PurchaseGRNItemColl.Clear();
        PurchaseGRN.GetAllPurchaseOrderDetails();
        grdPurchaseBillItemDetails.DataSource = PurchaseGRN.PurchaseGRNItemColl;
        grdPurchaseBillItemDetails.DataBind();
        SetValueToGridFooter();
    }
    protected void txtDiscount_TextChanged1(object sender, EventArgs e)
    {
        HeaderCalculation();
    }
    protected void txtHeaderTax_TextChanged(object sender, EventArgs e)
    {
        HeaderCalculation();
    }
    protected void txtVendor_TextChanged(object sender, EventArgs e)
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        ACEtxtVendorAddress.ContextKey = hdfVendorID.Value;
        ACEtxtPurchaseOrder.ContextKey = hdfVendorID.Value;
        PurchaseGRN.Vendor_Company_ID = new AppConvert(hdfVendorID.Value);
        txtTermsAndConditions.Text = PurchaseGRN.DefaultTermsAndConditionsStatement;
        hdfTermsAndConditionsName.Value = PurchaseGRN.DefaultTermsAndConditionsStatement;
        hdfTermsAndConditionsID.Value = new AppConvert(PurchaseGRN.DefaultTermsAndConditionsID);
        txtVendorAddress.Text = PurchaseGRN.VendorAddress;
        hdfVendorAddressName.Value = txtVendorAddress.Text;
        txtHeaderExchangeCurrency.Text = new AppConvert(PurchaseGRN.VendorCurrencyName);
        hdfHeaderExchangeCurrencyName.Value = txtHeaderExchangeCurrency.Text;
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(PurchaseGRN.VendorCurrencyID);
        hdfVendorAddressID.Value = new AppConvert(PurchaseGRN.VendorAddressID);
        Session.Add(TableConstants.PurchaseOrder, PurchaseGRN);
        BindData();
        if(PurchaseGRN.BillType==2)
        {
            txtPurchaseOrder.Focus();
        }
        else
        {
            txtVendor.Focus();
        }

    }
    #endregion
    #region Function
    public void LineItemCalculation(int RowIndex)
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        TextBox txtQuantity = (TextBox)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("txtQuantity");
        TextBox txtRate = (TextBox)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("txtRate");
        TextBox txtBasePrice = (TextBox)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("txtBasePrice");
        TextBox txtNetAmount = (TextBox)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("txtNetAmount");
        TextBox txtDiscount = (TextBox)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("txtDiscount");
        HiddenField hdfDiscountType = (HiddenField)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("hdfDiscountType");
        HiddenField hdfLineItemTaxID = (HiddenField)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("hdfLineItemTaxID");
        HiddenField hdfUOMID = (HiddenField)grdPurchaseBillItemDetails.Rows[RowIndex].FindControl("hdfUOMID");
        decimal Quantity = new AppUtility.AppConvert(txtQuantity.Text);
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].Quantity = Quantity;
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].ItemRate = new AppUtility.AppConvert(txtRate.Text);
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].DiscountValue = new AppUtility.AppConvert(txtDiscount.Text);
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].DiscountAmount = PurchaseGRN.PurchaseGRNItemColl[RowIndex].CalDiscountAmount;
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].DiscountType = new AppConvert(hdfDiscountType.Value == "0" ? 1 : new AppConvert(hdfDiscountType.Value));
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].ItemBaseAmount = PurchaseGRN.PurchaseGRNItemColl[RowIndex].PostDiscountPrice;
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].UOMType_ID = new AppUtility.AppConvert(hdfUOMID.Value);
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].TaxType_ID = new AppUtility.AppConvert(hdfLineItemTaxID.Value);
        if (string.IsNullOrEmpty(hdfLineItemTaxID.Value) == false && hdfLineItemTaxID.Value != "0")
        {
            PurchaseGRN.PurchaseGRNItemColl[RowIndex].TaxType.CalculateTax(new AppConvert(PurchaseGRN.PurchaseGRNItemColl[RowIndex].ItemBaseAmount), 0, 0);
            PurchaseGRN.PurchaseGRNItemColl[RowIndex].TaxAmount = PurchaseGRN.PurchaseGRNItemColl[RowIndex].TaxType.TaxValue;
        }
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].NetAmount = PurchaseGRN.PurchaseGRNItemColl[RowIndex].CalNetAmount;
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].FinalAmount = PurchaseGRN.PurchaseGRNItemColl[RowIndex].CalFinalAmount;
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].ModifiedBy = intRequestingUserID;
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].ModifiedOn = System.DateTime.Now;
        Session.Add(TableConstants.PurchaseGRN, PurchaseGRN);
        BindData();
        SetValueToGridFooter();
        HeaderCalculation();
    }
    public void SetValueToGridFooter()
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        Label lblFooterNetAmount = (Label)grdPurchaseBillItemDetails.FooterRow.FindControl("lblFooterNetAmount");
        lblFooterNetAmount.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.NetAmount)).ToString("#,##0.00");
        Label lblFooterTax = (Label)grdPurchaseBillItemDetails.FooterRow.FindControl("lblFooterTax");
        lblFooterTax.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.TaxAmount)).ToString("#,##0.00");
        Label lblFooterDiscount = (Label)grdPurchaseBillItemDetails.FooterRow.FindControl("lblFooterDiscount");
        lblFooterDiscount.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.DiscountValue)).ToString("#,##0.00");
        Label lblFooterBasePrice = (Label)grdPurchaseBillItemDetails.FooterRow.FindControl("lblFooterBasePrice");
        lblFooterBasePrice.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.BasePrice)).ToString("#,##0.00");
        Label lblFooterQuantity = (Label)grdPurchaseBillItemDetails.FooterRow.FindControl("lblFooterQuantity");
        lblFooterQuantity.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.Quantity)).ToString("#,##0.00");
        HeaderCalculation();
    }
    public void HeaderCalculation()
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        PurchaseGRN.DiscountType = new AppConvert(hdfDiscountType.Value);
        PurchaseGRN.DiscountValue = new AppConvert(txtDiscount.Text);
        PurchaseGRN.FinalAmount = PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.NetAmount);
        txtTotalAmount.Text = new AppConvert(PurchaseGRN.PostDiscountPrice);

        if (string.IsNullOrEmpty(hdfHeaderTaxID.Value) == false && hdfHeaderTaxID.Value != "0")
        {
            PurchaseGRN.TaxType_ID = new AppConvert(hdfHeaderTaxID.Value);
            PurchaseGRN.TaxType.CalculateTax(new AppConvert(txtTotalAmount.Text), 0, 0);
            txtHeaderTaxAmount.Text = new AppConvert(PurchaseGRN.TaxType.TaxValue);
            txtTotalAmount.Text = new AppConvert(PurchaseGRN.PostDiscountPrice + PurchaseGRN.TaxType.TaxValue);

        }
        else {
            txtHeaderTaxAmount.Text = "0";
        }
        PurchaseGRN.AllocateAmount();
        Session[TableConstants.PurchaseGRN] = PurchaseGRN;
    }
    public void PopulateData()
    {
        txtPurchaseOrder.Enabled = false;

        txtPurchaseBillDate.Text = new AppConvert(PurchaseGRN.InvoiceDate);
        PurchaseGRN.BillType = PurchaseGRN.VoucherType_ID == 8 ? 1 : 2;
        txtPurchaseOrder.Text = PurchaseGRN.PurchaseOrder.PurchaseOrderNumber;
        hdfPurchaseOrderID.Value = new AppConvert(PurchaseGRN.PurchaseOrder_ID);
        hdfPurchaseOrderName.Value = PurchaseGRN.PurchaseOrder.PurchaseOrderNumber;
        txtPurchaseBillNo.Text = PurchaseGRN.InvoiceNumber;
        txtStatus.Text = PurchaseGRN.StatusName;
        txtVendor.Text = new AppConvert(PurchaseGRN.Vendor_Company.Name);
        hdfVendorName.Value = txtVendor.Text;
        hdfVendorID.Value = new AppConvert(PurchaseGRN.Vendor_Company_ID);
        txtTotalAmount.Text = new AppConvert(PurchaseGRN.InvoiceBaseAmount);
        txtTermsAndConditions.Text = new AppConvert(PurchaseGRN.TermsAndConditions.Statement);
        hdfTermsAndConditionsName.Value = txtTermsAndConditions.Text;
        hdfTermsAndConditionsID.Value = new AppConvert(PurchaseGRN.TermsAndConditions.ID);
        txtDiscount.Text = new AppConvert(PurchaseGRN.DiscountValue);
        hdfDiscountType.Value = new AppConvert(PurchaseGRN.DiscountType);
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrency);
        txtHeaderExchangeCurrency.Text = PurchaseGRN.CurrencyType.Name;
        hdfHeaderExchangeCurrencyName.Value = txtHeaderExchangeCurrency.Text;
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(PurchaseGRN.CurrencyType_ID);
        txtCompanyAddress.Text = PurchaseGRN.SetCompanyAddress;
        hdfCompanyAddressName.Value = txtCompanyAddress.Text;
        hdfCompanyAddressID.Value = new AppConvert(PurchaseGRN.CompanyAddress_ID);
        txtVendorAddress.Text = PurchaseGRN.SetVendorAddress;
        hdfVendorAddressName.Value = txtVendorAddress.Text;
        hdfVendorAddressID.Value = new AppConvert(PurchaseGRN.Vendor_CompanyAddress_ID);
        txtDeliveryAddress.Text = PurchaseGRN.GetDeliveryAddress;
        hdfDeliveryAddressName.Value = txtDeliveryAddress.Text;
        hdfDeliveryAddressID.Value = new AppConvert(PurchaseGRN.Delivery_CompanyAddress_ID);
        txtRemark.Text = PurchaseGRN.BillDescription;
        txtHeaderTax.Text = PurchaseGRN.TaxType.Name;
        hdfHeaderTaxName.Value = txtHeaderTax.Text;
        hdfHeaderTaxID.Value = new AppConvert(PurchaseGRN.TaxType_ID);
        txtHeaderTaxAmount.Text = new AppConvert(PurchaseGRN.TaxAmount);

    }
    public void DefaultData()
    {
        txtPurchaseBillDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtCompanyAddress.Text = base.DefaultCompanyAddress;
        hdfCompanyAddressName.Value = txtCompanyAddress.Text;
        hdfCompanyAddressID.Value = new AppConvert(this.DefaultCompanyAddressID);
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrency);
        txtDeliveryAddress.Text = this.DefaultDeliveryAddress;
        hdfDeliveryAddressName.Value = txtDeliveryAddress.Text;
        hdfDeliveryAddressID.Value = new AppConvert(this.DefaultDeliveryAddressID);
        ACEtxtDeliveryAddress.ContextKey = new AppConvert(Session[TableConstants.LoginCompanyID]);
        ACEtxtCompanyAddress.ContextKey = new AppConvert(Session[TableConstants.LoginCompanyID]);
    }
    public void SavePurchaseBillHeader()
    {
        AppPurchaseGRN PB = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        if (PB.ID == 0)
        {
            PB.InvoiceNumber = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "PB", System.DateTime.Now,"PB-", 4, ResetCycle.Yearly));
        }
        PB.VoucherType_ID = PB.BillType == 1 ? 8 : 6;
        PB.Company_ID = new AppConvert(LoginCompanyID);
        PB.CompanyAddress_ID = new AppConvert(hdfCompanyAddressID.Value);
        PB.Vendor_Company_ID = new AppConvert(hdfVendorID.Value);
        PB.Vendor_CompanyAddress_ID = new AppConvert(hdfVendorAddressID.Value);
        PB.InvoiceDate = new AppConvert(txtPurchaseBillDate.Text);
        PB.PurchaseOrder_ID = new AppConvert(hdfPurchaseOrderID.Value);
        PB.TermsAndConditions_ID = new AppConvert(hdfTermsAndConditionsID.Value);
        PB.TaxType_ID = new AppConvert(hdfHeaderTaxID.Value);
        PB.CurrencyType_ID = new AppConvert(hdfHeaderExchangeCurrencyID.Value);
        PB.Delivery_CompanyAddress_ID = new AppConvert(hdfDeliveryAddressID.Value);
        PB.InvoiceBaseAmount = new AppConvert(txtTotalAmount.Text);
        PB.TaxAmount = new AppConvert(txtHeaderTaxAmount.Text);
        PB.BillDescription = txtRemark.Text;
        PB.OtherCharges = 0;
        PB.FinalAmount = new AppConvert(txtTotalAmount.Text);
        PB.DiscountValue = new AppConvert(txtDiscount.Text);
        PB.DiscountType = new AppConvert(hdfDiscountType.Value);
        PB.DiscountAmount = PB.CalDiscountAmount;
        PB.RoundAmount = 0;
        PB.Status = new AppConvert((int)RecordStatus.Created);
        PB.Type = 0;
        PB.ModifiedBy = intRequestingUserID;
        PB.ModifiedOn = System.DateTime.Now;
        PB.Save();
    }
    public void BindData()
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        if (PurchaseGRN == null)
        {
            PurchaseGRN = new AppPurchaseGRN(intRequestingUserID);
        }
        grdPurchaseBillItemDetails.DataSource = PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created));
        grdPurchaseBillItemDetails.DataBind();
        if (PurchaseGRN.PurchaseGRNItemColl.Count <= 0)
        {
            AddEmptyRow(1);
        }
    }
    public void AddEmptyRow(int NumberOfRows)
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        if (PurchaseGRN == null)
        {
            PurchaseGRN = new AppPurchaseGRN(intRequestingUserID);
        }
        AppPurchaseGRNItem PurchaseGRNItem = new AppPurchaseGRNItem(intRequestingUserID);
        for (int i = 0; i < NumberOfRows; i++)
        {
            if (PurchaseGRN.PurchaseGRNItemColl.Count == 0)
            {
                PurchaseGRN.PurchaseGRNItemColl.Add(PurchaseGRNItem);
            }
            else
            {
                PurchaseGRN.PurchaseGRNItemColl.Insert(0, PurchaseGRNItem);
            }
            Session.Add(TableConstants.PurchaseGRN, PurchaseGRN);
        }
        BindData();
    }
    #endregion
    #region Grid Event
    protected void grdPurchaseBillItemDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
            AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
            PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).ToList()[row.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
            Session.Add(TableConstants.PurchaseGRN, PurchaseGRN);
            BindData();
            SetValueToGridFooter();
        }
    }
    protected void grdPurchaseBillItemDetails_PreRender(object sender, EventArgs e)
    {
        if (grdPurchaseBillItemDetails.Rows.Count > 0)
        {
            grdPurchaseBillItemDetails.UseAccessibleHeader = true;
            grdPurchaseBillItemDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region Button Event
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        PurchaseGRN.PurchaseGRNItemColl.RemoveAll(x => x.Item_ID == 0);
        if (PurchaseGRN.PurchaseGRNItemColl.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please add atleast one item','2'" + ");", true);
            return;
        }
        if (txtDiscount.Text != "0" && txtDiscount.Text != "" && DiscountLedgerID == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please create discount ledger','2'" + ");", true);
            return;
        }
        if (SaleBill.RoundAmount != 0 && RoundingOFFLedgerID == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please create RoundOFF ledger','2'" + ");", true);
            return;
        }
        Session.Add(TableConstants.PurchaseGRN, PurchaseGRN);
        SavePurchaseBillHeader();

        #region Add Ledger Entries
        LedgerEntry(LoginCompanyID, 100101, 1001, PurchaseGRN.Vendor_Company.Vendor_Ledger_ID, PurchaseGRN.ID, PurchaseGRN.InvoiceDate, PurchaseGRN.FinalAmount, 0, PurchaseGRN.InvoiceNumber, PurchaseGRN.ModifiedBy, PurchaseGRN.ModifiedOn,0, PurchaseGRN.Status, PurchaseGRN.Type,0,0, PurchaseGRN.VoucherType_ID,PurchaseGRN.BillDescription);
        if (PurchaseGRN.TaxType.TaxTypeConfigurationColl.Count > 0 && PurchaseGRN.TaxType_ID > 0)
        {
            List<TaxDetails> objTaxDetails = PurchaseGRN.TaxType.CalculateTax(PurchaseGRN.InvoiceBaseAmount - PurchaseGRN.TaxAmount, 0, 0);
            foreach (TaxDetails item in objTaxDetails)
            {
                if (item.Ledger_ID != 0)
                {
                    UpdateLedgerEntryStatus(100102, 1001, PurchaseGRN.ID, new AppUtility.AppConvert((int)RecordStatus.Deleted));
                    LedgerEntry(LoginCompanyID, 100102, 1001, item.Ledger_ID, PurchaseGRN.ID, PurchaseGRN.InvoiceDate, 0, item.TaxValue, PurchaseGRN.InvoiceNumber, PurchaseGRN.ModifiedBy, PurchaseGRN.ModifiedOn, 0, PurchaseGRN.Status, PurchaseGRN.Type,0, 0, PurchaseGRN.VoucherType_ID, PurchaseGRN.BillDescription);
                }
            }
        }
        LedgerEntry(LoginCompanyID, 100103, 1001, DiscountLedgerID, PurchaseGRN.ID, PurchaseGRN.InvoiceDate, PurchaseGRN.DiscountAmount, 0, PurchaseGRN.InvoiceNumber, PurchaseGRN.ModifiedBy, PurchaseGRN.ModifiedOn, 0, PurchaseGRN.Status, PurchaseGRN.Type, 0, 0, PurchaseGRN.VoucherType_ID, PurchaseGRN.BillDescription);
        LedgerEntry(LoginCompanyID, 100104, 1001, RoundingOFFLedgerID, PurchaseGRN.ID, PurchaseGRN.InvoiceDate, 0, PurchaseGRN.RoundAmount, PurchaseGRN.InvoiceNumber, PurchaseGRN.ModifiedBy, PurchaseGRN.ModifiedOn, 0, PurchaseGRN.Status, PurchaseGRN.Type, 0, 0, PurchaseGRN.VoucherType_ID, PurchaseGRN.BillDescription);
        if (PurchaseGRN.PurchaseGRNItemColl.Count > 0)
        {
            foreach (var item in PurchaseGRN.PurchaseGRNItemColl)
            {   // Item Ledger
                if (item.ItemBaseAmount != 0) { LedgerEntry(LoginCompanyID, 100201, 1002, item.Item.Sales_Ledger_ID, item.ID, PurchaseGRN.InvoiceDate, 0, item.ItemBaseAmount, PurchaseGRN.InvoiceNumber, item.ModifiedBy, item.ModifiedOn, item.Status, PurchaseGRN.Status, item.Type, 0, 0, PurchaseGRN.VoucherType_ID, item.ItemDescription); }
                // tax Ledger
                if (item.TaxAmount != 0)
                {
                    if (item.TaxType.TaxTypeConfigurationColl.Count > 0 && item.TaxType_ID > 0)
                    {
                        List<TaxDetails> objTaxDetails = item.TaxType.CalculateTax(item.ItemBaseAmount, 0, 0);
                        foreach (TaxDetails td in objTaxDetails)
                        {
                            if (td.Ledger_ID != 0)
                            {
                                UpdateLedgerEntryStatus(100202, 1002, item.ID, new AppUtility.AppConvert((int)RecordStatus.Deleted));
                                LedgerEntry(LoginCompanyID, 100202, 1002, td.Ledger_ID, item.ID, PurchaseGRN.InvoiceDate, 0, td.TaxValue, PurchaseGRN.InvoiceNumber, PurchaseGRN.ModifiedBy, PurchaseGRN.ModifiedOn, item.Status, PurchaseGRN.Status, PurchaseGRN.Type, 0, 0, PurchaseGRN.VoucherType_ID, item.ItemDescription);
                            }
                        }
                    }
                }
            }
        }
        #endregion
        Session.Remove(TableConstants.PurchaseGRN);
        Response.Redirect("T_PurchaseBill.aspx?Msg=1&BillType="+new AppConvert(Request.QueryString["BillType"]));
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("T_PurchaseBillSummary.aspx");
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {

    }
    #endregion
}