﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;

public partial class CustomerItemPrice : BigSunPage
{
    AppCustomer obj;
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = (AppCustomer)Session[TableConstants.CustomerSessionObj];
        if (!IsPostBack)
        {
            BindData();
        }
    }
    
    #region Bind Data  
    private void BindData()
    {
        if (obj != null)
        {
            
            grdCustomerItemPrice.DataSource = obj.CustomerItemPriceColl;
            grdCustomerItemPrice.DataBind();
            if (obj.CustomerItemPriceColl.Count <= 0)
            {
                AddEmptyRow();
            }            
        }
    }
    #endregion
    
    #region AddEmptyRow
    public void AddEmptyRow()
    {
        AppCustomerItemPriceColl collOfContacts = new AppCustomerItemPriceColl(intRequestingUserID);
        AppCustomerItemPrice newContact = new AppCustomerItemPrice(intRequestingUserID);
        newContact.Item.Name = "Add atleast one item";
        collOfContacts.Add(newContact);
        grdCustomerItemPrice.DataSource = collOfContacts;
        grdCustomerItemPrice.DataBind();
        grdCustomerItemPrice.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdCustomerItemPrice.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdCustomerItemPrice.ShowFooter = true;
    }
    #endregion
    
    protected void grdCustomerItemPrice_PreRender(object sender, EventArgs e)
    {
        if (grdCustomerItemPrice.Rows.Count > 0)
        {
            grdCustomerItemPrice.UseAccessibleHeader = true;
            grdCustomerItemPrice.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

    protected void grdCustomerItemPrice_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string CommandName = e.CommandName;
        string CommandID = "";
        switch (CommandName)
        {
            case "INSERT":
                HiddenField hdfNewItemID = grdCustomerItemPrice.FooterRow.FindControl("hdfNewItemID") as HiddenField;
                HiddenField hdfNewUOMID = grdCustomerItemPrice.FooterRow.FindControl("hdfNewUOMID") as HiddenField;
                TextBox txtNewPrice = grdCustomerItemPrice.FooterRow.FindControl("txtNewPrice") as TextBox;
                TextBox txtNewMRP = grdCustomerItemPrice.FooterRow.FindControl("txtNewMRP") as TextBox;
                TextBox txtNewFromDate = grdCustomerItemPrice.FooterRow.FindControl("txtNewFromDate") as TextBox;
                TextBox txtNewToDate = grdCustomerItemPrice.FooterRow.FindControl("txtNewToDate") as TextBox;
                TextBox txtNewDiscount = grdCustomerItemPrice.FooterRow.FindControl("txtNewDiscount") as TextBox;
                DropDownList ddlNewDispatchAdd = grdCustomerItemPrice.FooterRow.FindControl("ddlNewDispatchAdd") as DropDownList;
                DropDownList ddlNewCurrency = grdCustomerItemPrice.FooterRow.FindControl("ddlNewCurrency") as DropDownList;                
                CheckBox IsActive = grdCustomerItemPrice.FooterRow.FindControl("chkIsActive") as CheckBox;
                SaveItemDetails(CommandID, 
                                hdfNewItemID.Value, 
                                hdfNewUOMID.Value,
                                txtNewPrice.Text,
                                txtNewMRP.Text,
                                txtNewFromDate.Text,
                                txtNewToDate.Text,
                                txtNewDiscount.Text,
                                ddlNewDispatchAdd,
                                ddlNewCurrency,
                                IsActive.Checked);
                BindData();
                break;
            case "ADDNEW":
                AddEmptyRow();
                BindData();
                break;
        }
    }

    protected void grdCustomerItemPrice_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        obj = (AppCustomer)Session[TableConstants.CustomerSessionObj];
        obj.CustomerItemPriceColl[e.RowIndex].Status = (int)RecordStatus.Deleted;
        Session[TableConstants.CustomerSessionObj] = obj;
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Item Successfully Deleted','1'" + ");", true);
        BindData();
    }

    protected void grdCustomerItemPrice_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string Index = new AppConvert(e.RowIndex);
        HiddenField hdfItemID = (HiddenField)grdCustomerItemPrice.Rows[e.RowIndex].FindControl("hdfItemID");
        HiddenField hdfUOMID = (HiddenField)grdCustomerItemPrice.Rows[e.RowIndex].FindControl("hdfUOMID");
        TextBox txtPrice = (TextBox)grdCustomerItemPrice.Rows[e.RowIndex].FindControl("txtPrice");
        TextBox txtMRP = (TextBox)grdCustomerItemPrice.Rows[e.RowIndex].FindControl("txtMRP");
        TextBox txtFromDate = (TextBox)grdCustomerItemPrice.Rows[e.RowIndex].FindControl("txtFromDate");
        TextBox txtToDate = (TextBox)grdCustomerItemPrice.Rows[e.RowIndex].FindControl("txtToDate");
        TextBox txtDiscount = (TextBox)grdCustomerItemPrice.Rows[e.RowIndex].FindControl("txtDiscount");
        DropDownList ddlDispatchAdd = (DropDownList)grdCustomerItemPrice.Rows[e.RowIndex].FindControl("ddlDispatchAdd");
        DropDownList ddlCurrency = (DropDownList)grdCustomerItemPrice.Rows[e.RowIndex].FindControl("ddlCurrency");
        CheckBox chkActive = grdCustomerItemPrice.Rows[e.RowIndex].FindControl("chkActive") as CheckBox;
        SaveItemDetails(Index, 
                        hdfItemID.Value,
                        hdfUOMID.Value,
                        txtPrice.Text,
                        txtMRP.Text,
                        txtFromDate.Text,
                        txtToDate.Text,
                        txtDiscount.Text,
                        ddlDispatchAdd,
                        ddlCurrency,
                        chkActive.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Item Successfully Updated','1'" + ");", true);
        grdCustomerItemPrice.EditIndex = -1;
        BindData();
    }

    protected void grdCustomerItemPrice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Footer:
                DropDownList ddlAddresss = (DropDownList)e.Row.FindControl("ddlNewDispatchAdd");
                BindAddress("Address1", "ID", ddlAddresss);
                DropDownList ddlNewCurrency = (DropDownList)e.Row.FindControl("ddlNewCurrency");
                CommonFunctions.BindDropDown(intRequestingUserID, Components.CurrencyType, ddlNewCurrency, "Name", "ID");               
                break;
            case DataControlRowType.DataRow:                
                DropDownList ddlCustomerAddresss = (DropDownList)e.Row.FindControl("ddlDispatchAdd");
                if (ddlCustomerAddresss != null)
                {
                    BindAddress("Address1", "ID", ddlCustomerAddresss);
                    ddlCustomerAddresss.SelectedIndex = e.Row.RowIndex; //new AppConvert(obj.CompanyContactColl[e.Row.RowIndex].CompanyAddress_ID);
                }
                break;
            default:
                break;
        }
    }

    #region BindAddress
    public void BindAddress(string DataTextField, string DataValueField, DropDownList ddlAddressID)
    {
        ddlAddressID.DataSource = obj.CompanyAddressColl;
        ddlAddressID.DataTextField = DataTextField;
        ddlAddressID.DataValueField = DataValueField;
        ddlAddressID.DataBind();
    }
    #endregion
    public void SaveItemDetails(string Id, string ItemID, string UOMID,string Price,string MRP, string FromDate, string ToDate,
      string Discount,DropDownList DispatchAdd, DropDownList Currency,  bool Active)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppCustomerItemPrice SingleObj = new AppCustomerItemPrice(intRequestingUserID);
        if (String.IsNullOrEmpty(Id) == false)
        {
            SingleObj = obj.CustomerItemPriceColl[intCommandID];
        }
        else
        {
            obj.AddNewCustomerItemPrice(SingleObj);
        }
        SingleObj.Company_ID = obj.ID;
        SingleObj.Item_ID = new AppConvert(ItemID);
        SingleObj.UOMType_ID = new AppConvert(UOMID);
        SingleObj.CompanyAddress = obj.CompanyAddressColl[DispatchAdd.SelectedIndex];
        SingleObj.Price = new AppConvert(Price);
        SingleObj.MRP = new AppConvert(MRP);
        SingleObj.Discount = new AppConvert(Discount);
        SingleObj.FromDate = new AppConvert(FromDate);
        SingleObj.ToDate = new AppConvert(ToDate);
        SingleObj.CurrencyType_ID = new AppConvert(Currency.SelectedValue);        
        SingleObj.Status = (Active == true ? new AppUtility.AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));       
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifedOn = System.DateTime.Now;
        Session.Add(TableConstants.CustomerSessionObj, obj);
        BindData();
        grdCustomerItemPrice.EditIndex = -1;
    }

    protected void grdCustomerItemPrice_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdCustomerItemPrice.ShowFooter = false;
        grdCustomerItemPrice.EditIndex = e.NewEditIndex;
        BindData();
    }
}