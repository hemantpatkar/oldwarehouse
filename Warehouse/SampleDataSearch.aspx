﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Culture="en-gb" Inherits="SampleDataSearch" Codebehind="SampleDataSearch.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-9">
            <div class="heading">
                <i class="fa fa-truck"></i>
                <asp:Label ID="lblTitleLocationTransferSearch" Text="Sample Search" runat="server"></asp:Label>
            </div>
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSearch_Click" Text="Search" ValidationGroup="Search" />
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnNew" OnClick="btnNew_Click" CssClass="btn btn-primary btn-block btn-sm" runat="server" AccessKey="n" CausesValidation="False" Text="New" />
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnExport" CssClass="btn btn-warning btn-block btn-sm" runat="server" OnClick="btnExportToExcel_Click" AccessKey="n" CausesValidation="False" Text="Export" />
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblSampleNo" Text="Sample No" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtSampleNo" runat="server" CssClass="form-control input-sm" ValidationGroup="Search"></asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblSampleDate" Text="Transhipment Date" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtSampleDate" runat="server"></asp:TextBox>
            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder=""
                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtSampleDate">
            </asp:MaskedEditExtender>
            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtSampleDate">
            </asp:CalendarExtender>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblCustomer" Text="Customer" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtCustomer" runat="server" CssClass="form-control input-sm" ValidationGroup="Search">
            </asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblReceivedBy" Text="Received By" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtReceivedBy" runat="server" CssClass="form-control input-sm" ValidationGroup="Search"></asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblStatus" Text="Status" CssClass="bold"></asp:Label>
            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control input-sm">
                <asp:ListItem Value="-1" Selected="True">All</asp:ListItem>
                <asp:ListItem Value="1">Active</asp:ListItem>
                <asp:ListItem Value="2">Deleted</asp:ListItem>
            </asp:DropDownList>
            <asp:HiddenField ID="hdfgatereg" runat="server" />
            <asp:HiddenField ID="ShowLastRecords" runat="server" />
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-lg-12" style="overflow: auto">
            <asp:GridView ID="grdTranshipment" runat="server" AllowPaging="true" PagerStyle-CssClass="pagination-ys" AutoGenerateColumns="false" CssClass="table table-hover table-striped text-nowrap table-Full" OnPreRender="grdTranshipment_PreRender">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkNo" runat="server" CssClass="fa fa-edit fa-2x" NavigateUrl='<%#"SampleDataDetail.aspx?SampleID="+ Eval("ID")%>' CausesValidation="False" Target="_blank"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sample No">
                        <ItemTemplate>
                            <asp:Label ID="lblgrdSampleNo" runat="server" Text='<%# Eval("SampleNo") %>' ToolTip='<%# Eval("SampleNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sample Date">
                        <ItemTemplate>
                            <asp:Label ID="lblSampleDate" Text='<%#  Convert.ToDateTime(Eval("SampleDate")).ToShortDateString() %>' ToolTip='<%# Eval("SampleDate")%>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Customer Info">
                        <ItemTemplate>
                            <asp:Label ID="lblVehicalNo" runat="server" ToolTip='<%#Eval("Customer_Company.Name") %>'
                                Text='<%#Eval("Customer_Company.Name") %>'></asp:Label><br />
                            <asp:Label ID="lblVehicalName" runat="server" Font-Bold="true" ForeColor="Maroon"
                                Text='<%#Eval("Customer_Company.Parent.Name") %>' ToolTip='<%#Eval("Customer_Company.Parent.Name") %>' ></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remark">
                        <ItemTemplate>
                            <asp:Label ID="lblRemark" Text='<%#  Eval("Remarks") %>' ToolTip='<%# Eval("Remarks")%>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Modified By">
                        <ItemTemplate>
                            <asp:Label ID="lblModifiedBy" Text='<%# Eval("ModifiedBy") %>' ToolTip='<%# Eval("ModifiedBy") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Modified On">
                        <ItemTemplate>
                            <asp:Label ID="lblModifiedOn" Text='<%# Convert.ToDateTime(Eval("ModifiedOn")).ToShortDateString() %>' ToolTip='<%# Convert.ToDateTime(Eval("ModifiedOn")).ToString("dd/MM/yyyy") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Print">
                        <ItemTemplate>
                            <asp:HyperLink ID="HLReport" runat="server" ToolTip="View Report" Target="_blank" NavigateUrl='<% #"../../Reports/ReportView.aspx?GateRegID=" + Eval("ID") + "&RPName=GatePass"%>'>
                                        <i class="fa fa-print fa-2x"></i>
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
