﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class T_CustomerReceiptDetails : BigSunPage
{
    #region PageLoad
    AppObjects.AppReceiptVoucher objReceiptVoucher = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        objReceiptVoucher = new AppReceiptVoucher(intRequestingUserID);
        if (!IsPostBack)
        {
            ACEtxtCustomer.ContextKey = new AppConvert((int)AppObjects.CompanyRoleType.Customer);
            Session[TableConstants.CustomerReceiptDetails] = objReceiptVoucher;
            Ce_ReceiptDate.SelectedDate = DateTime.Today;
            BindToDropdown();
            string ID = Convert.ToString(Request.QueryString["ReceiptVoucherID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int ReceiptVoucherID = new AppUtility.AppConvert(ID);
                objReceiptVoucher = new AppObjects.AppReceiptVoucher(intRequestingUserID, ReceiptVoucherID);
                Session[TableConstants.CustomerReceiptDetails] = objReceiptVoucher;
                PopulateData();
            }
            else
            {
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
                Session[TableConstants.CustomerReceiptDetails] = objReceiptVoucher;
            }
            BindData();
        }
    }
    #endregion
    #region Text Events
    protected void txtCustomerName_TextChanged(object sender, EventArgs e)
    {
        objReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.CustomerReceiptDetails];
        int CustomerCompanyID = new AppConvert(hdfCustomerID.Value);
        objReceiptVoucher.Customer_Company_ID = CustomerCompanyID;
        BindCustomerDetails(CustomerCompanyID);
        BindData();
    }
    protected void txtCollectedAmount_TextChanged(object sender, EventArgs e)
    {
        AppReceiptVoucher objReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.CustomerReceiptDetails];
        txtCollectedAmount.Text = Convert.ToDecimal(txtCollectedAmount.Text).ToString("#,##0.00");
        objReceiptVoucher.DummyBalanceAmount = new AppConvert(txtCollectedAmount.Text);
        txtCollectedAmount.Focus();
        Session.Add(TableConstants.CustomerReceiptDetails, objReceiptVoucher);
        CalculateCollectedBillAmount();
    }
    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {
        AppReceiptVoucher objPV = (AppReceiptVoucher)Session[TableConstants.CustomerReceiptDetails];
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        Label lblBillAmount = (Label)grdSaleBillDetails.Rows[RowIndex].FindControl("lblBillAmount");
        Label lblBalanceAmount = (Label)grdSaleBillDetails.Rows[RowIndex].FindControl("lblBalanceAmount");
        TextBox txtDiscount = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtDiscount");
        decimal BalanceAmount = new AppConvert(lblBalanceAmount.Text);
        decimal DiscountAmount = new AppConvert(txtDiscount.Text);
        if (BalanceAmount < 0 || (DiscountAmount > BalanceAmount))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Discount Amount should be greater than balance amount','2'" + ");", true);
            txtDiscount.Text = "0.00";
            return;
        }
        else
        {
            BalanceAmount = BalanceAmount - DiscountAmount;
        }
        lblBalanceAmount.Text = new AppConvert(BalanceAmount);
        objPV.ReceiptVoucherDetailColl[RowIndex].DiscountAmount = DiscountAmount;
        objPV.ReceiptVoucherDetailColl[RowIndex].Discount_Ledger_ID = CommonFunctions.DiscountLedgerID;
        Session[TableConstants.CustomerReceiptDetails] = objPV;
        calculateFinalAmount(RowIndex);
    }
    protected void txtTDSAmount_TextChanged(object sender, EventArgs e)
    {
        AppReceiptVoucher objPV = (AppReceiptVoucher)Session[TableConstants.CustomerReceiptDetails];
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        Label lblBillAmount = (Label)grdSaleBillDetails.Rows[RowIndex].FindControl("lblBillAmount");
        Label lblBalanceAmount = (Label)grdSaleBillDetails.Rows[RowIndex].FindControl("lblBalanceAmount");
        TextBox txtTDSAmount = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtTDSAmount");
        decimal BalanceAmount = new AppConvert(lblBalanceAmount.Text);
        decimal DiscountAmount = new AppConvert(txtTDSAmount.Text);
        if (BalanceAmount < 0 || (DiscountAmount > BalanceAmount))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Discount Amount should be greater than balance amount','2'" + ");", true);
            txtTDSAmount.Text = "0.00";
            return;
        }
        else
        {
            BalanceAmount = BalanceAmount - DiscountAmount;
        }
        lblBalanceAmount.Text = new AppConvert(BalanceAmount);
        objPV.ReceiptVoucherDetailColl[RowIndex].TDSAmount = DiscountAmount;
        Session[TableConstants.CustomerReceiptDetails] = objPV;
        calculateFinalAmount(RowIndex);
    }
    protected void txtSetOffAmt_TextChanged(object sender, EventArgs e)
    {
        decimal AdvanceAmount = new AppConvert(txtAdvanceAmount.Text);
        decimal SetOffAmount = new AppConvert(txtSetOffAmt.Text);
        if (SetOffAmount > 0 && AdvanceAmount > 0)
        {
            AdvanceAmount = AdvanceAmount - SetOffAmount;
        }
        txtAdvanceAmount.Text = new AppConvert(AdvanceAmount);
    }
    protected void txtReceiptAmt_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        CalculateNetAmount(RowIndex);
    }
    #endregion
    #region Functions
    public void BindCustomerDetails(int CustomerCompanyID)
    {
        AppCompany objCustomerDetails = new AppCompany(intRequestingUserID, CustomerCompanyID);
        CommonFunctions.BindDropDown(intRequestingUserID, Components.CompanyBank, ddlCustomerAccountNo, "AccountNo", "ID", "", false, CustomerCompanyID);
        if (ddlCustomerAccountNo.Items.Count > 0) { ddlCustomerAccountNo.SelectedValue = new AppConvert(objCustomerDetails.DefaultCompanyBank.ID); }
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCustomerBankBranch, "Name", "ID", "", true, CustomerCompanyID);
        if (ddlCustomerBankBranch.Items.Count > 0) { ddlCustomerBankBranch.SelectedValue = new AppConvert(objCustomerDetails.DefaultCompanyBank.BankType_ID); }
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCustomerBank, "BankBranch", "ID", "", true, CustomerCompanyID);
        if (ddlCustomerBank.Items.Count > 0) { ddlCustomerBank.SelectedValue = new AppConvert(objCustomerDetails.DefaultCompanyBank.BankType_ID); }
        txtPayee.Text = objCustomerDetails.Name;
    }
    public void CalculateCollectedBillAmount()
    {
        AppReceiptVoucher objReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.CustomerReceiptDetails];
        decimal CollectedAmount = new AppConvert(txtCollectedAmount.Text);
        decimal BalanceCollAmount = CollectedAmount;
        decimal TotalReceipt = 0;
        foreach (AppReceiptVoucherDetail item in objReceiptVoucher.ReceiptVoucherDetailColl)
        {
            if (BalanceCollAmount <= 0)
            {
                break;
            }
            if (item.IsReceiptChecked == true)
            {
                TotalReceipt += item.Amount;
            }
            else
            {
            }
        }
        txtTotalReceipt.Text = new AppConvert(TotalReceipt);
        txtAdvanceAmount.Text = new AppConvert(CollectedAmount - TotalReceipt);
        txtTotalSettledAmount.Text = new AppConvert(txtTotalReceipt.Text);
    }
    protected void BindData()
    {
        objReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.CustomerReceiptDetails];
        if (txtNoOfDueDays.Text != "")
        {
            //objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleOrder.SaleOrderNumber, AppUtility.Operators.Equals, txtNoOfDueDays.Text, 0);
        }
        if (hdfCustomerID.Value != "")
        {
            objReceiptVoucher.Customer_Company_ID = new AppConvert(hdfCustomerID.Value);
        }
        objReceiptVoucher.GetAllReceiptOrderDetails();
        grdSaleBillDetails.DataSource = objReceiptVoucher.ReceiptVoucherDetailColl;
        grdSaleBillDetails.DataBind();
        divBillDetails.Visible = true;
        Session.Add(TableConstants.CustomerReceiptDetails, objReceiptVoucher);
    }
    public void calculateFinalAmount(Int32 rowindex)
    {
        Label lblBillAmt = (Label)grdSaleBillDetails.Rows[rowindex].FindControl("lblBillAmount");
        Label lblPaidAmt = (Label)grdSaleBillDetails.Rows[rowindex].FindControl("lblReceiptAmount");
        Label lblTotalReceiptAmount = (Label)grdSaleBillDetails.Rows[rowindex].FindControl("lblTotalReceiptAmount");
        TextBox txtDiscount = (TextBox)grdSaleBillDetails.Rows[rowindex].FindControl("txtDiscount");
        TextBox txtTdsAmount = (TextBox)grdSaleBillDetails.Rows[rowindex].FindControl("txtTDSAmount");
        Label txtBalance = (Label)grdSaleBillDetails.Rows[rowindex].FindControl("lblBalanceAmount");
        TextBox txtAmount = (TextBox)grdSaleBillDetails.Rows[rowindex].FindControl("txtReceiptAmt");
        decimal BillAmt = Convert.ToDecimal("0" + lblBillAmt.Text.Trim());
        decimal PaidAmt = Convert.ToDecimal("0" + lblPaidAmt.Text.Trim());
        decimal DiscountAmt = Convert.ToDecimal("0" + txtDiscount.Text.Trim());
        decimal TdsAmt = Convert.ToDecimal("0" + txtTdsAmount.Text.Trim());
        decimal TotalReciept = new AppConvert(lblTotalReceiptAmount.Text);
        decimal BalanceAmt = Convert.ToDecimal(txtBalance.Text.Trim() == "" ? "0" : txtBalance.Text.Trim());
        txtBalance.Text = Convert.ToString(BillAmt - TotalReciept - PaidAmt - DiscountAmt - TdsAmt);
    }
    public void BindToDropdown()
    {
        CommonFunctions.BindDropDown(intRequestingUserID, Components.PaymentType, ddlPaymentType, "PaymentTypeName", "ID");
        CommonFunctions.BindDropDown(intRequestingUserID, Components.CompanyBank, ddlCompanyAccountNo, "AccountNo", "ID", "", false, LoginCompanyID);
        ddlCompanyAccountNo.SelectedValue = new AppConvert(DefaultCompanyBank.ID);
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCompanyBankBranch, "BankBranch", "ID", "", true, LoginCompanyID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(DefaultCompanyBank.BankType_ID);
        CommonFunctions.BindCompanyBanks(intRequestingUserID, ddlCompanyBank, "Name", "ID", "", true, LoginCompanyID);
        ddlCompanyBank.SelectedValue = new AppConvert(DefaultCompanyBank.BankType_ID);
        SetBankDetails();
    }
    public void SetBankDetails()
    {
        int PaymentType = new AppConvert(ddlPaymentType.SelectedValue);
        switch (PaymentType)
        {
            case 1:
                divBankDetails.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            case 2:
                divBankDetails.Visible = true;
                divChequeNo.Visible = true;
                divChequeDate.Visible = true;
                break;
            case 3:
                divBankDetails.Visible = true;
                divChequeNo.Visible = true;
                divChequeDate.Visible = true;
                break;
            case 4:
                divBankDetails.Visible = true;
                divChequeNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            case 5:
                divBankDetails.Visible = true;
                divChequeNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
            default:
                divBankDetails.Visible = false;
                divChequeNo.Visible = false;
                divChequeDate.Visible = false;
                txtChequeNo.Text = "";
                txtChequeDate.Text = "";
                break;
        }
    }
    public void CalculateNetAmount(int RowIndex)
    {
        AppReceiptVoucher objPV = (AppReceiptVoucher)Session[TableConstants.CustomerReceiptDetails];
        TextBox txtReceiptAmt = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtReceiptAmt");
        Label lblBillAmount = (Label)grdSaleBillDetails.Rows[RowIndex].FindControl("lblBillAmount");
        Label lblTotalReceiptAmount = (Label)grdSaleBillDetails.Rows[RowIndex].FindControl("lblTotalReceiptAmount");        
        Label lblReceiptAmount = (Label)grdSaleBillDetails.Rows[RowIndex].FindControl("lblReceiptAmount");
        Label lblBalanceAmount = (Label)grdSaleBillDetails.Rows[RowIndex].FindControl("lblBalanceAmount");
        decimal CurrentBillReceiptAmount = 0;
        decimal AlreadyCurrentBillReceipt = 0;
        decimal BillAmount = 0;
        decimal TotalReceiptAmount = 0;
        decimal BalanceAmount = 0;
        decimal CollectedBillAmount = new AppConvert(txtCollectedAmount.Text);
        if(CollectedBillAmount<=0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Enter Collected Bill Amount','2'" + ");", true);
            txtReceiptAmt.Text = "0.00";
            return;
        }
        CurrentBillReceiptAmount = new AppConvert(txtReceiptAmt.Text);
        AlreadyCurrentBillReceipt = objPV.ReceiptVoucherDetailColl[RowIndex].Amount;
        BillAmount = objPV.ReceiptVoucherDetailColl[RowIndex].BillAmount;
        TotalReceiptAmount = objPV.ReceiptVoucherDetailColl[RowIndex].ReceiptAmount;
        BalanceAmount = objPV.ReceiptVoucherDetailColl[RowIndex].BalanceAmount;
        decimal DummyBalanceCollAmount = objPV.DummyBalanceAmount;
        if (CurrentBillReceiptAmount > 0)
        {
            if (CurrentBillReceiptAmount > BalanceAmount)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Amount should not be greater than Balance Amount','2'" + ");", true);
                CurrentBillReceiptAmount = 0;
            }
            else
            {
                TotalReceiptAmount = (TotalReceiptAmount) + CurrentBillReceiptAmount;                
                BalanceAmount = BillAmount - TotalReceiptAmount;
                if(DummyBalanceCollAmount>0)
                {
                    if (AlreadyCurrentBillReceipt > CurrentBillReceiptAmount)
                    {
                        DummyBalanceCollAmount = AlreadyCurrentBillReceipt - CurrentBillReceiptAmount;
                    }
                    else
                    {
                        DummyBalanceCollAmount = CurrentBillReceiptAmount - AlreadyCurrentBillReceipt;
                    }
                }
            }
            txtReceiptAmt.Enabled = true;
        }
        else
        {
            if (CurrentBillReceiptAmount > 0)
            {
                DummyBalanceCollAmount += CurrentBillReceiptAmount;
                CurrentBillReceiptAmount = 0;
                txtReceiptAmt.Enabled = false;          
            }
        }
       // NetReceivableAmount = (NetReceivableAmount - AlreadyCurrentBillReceipt) + CurrentBillReceiptAmount;
              //lblTotalReceiptAmount.Text = new AppConvert(TotalReceiptAmount);
        lblReceiptAmount.Text = new AppConvert(CurrentBillReceiptAmount);
        lblBalanceAmount.Text = new AppConvert(BalanceAmount);
        txtReceiptAmt.Text = new AppConvert(CurrentBillReceiptAmount);
        objPV.ReceiptVoucherDetailColl[RowIndex].Amount = new AppConvert(CurrentBillReceiptAmount);
        objPV.ReceiptVoucherDetailColl[RowIndex].Ledger_ID = objPV.Customer_Company.Customer_Ledger_ID;
        objPV.ReceiptVoucherDetailColl[RowIndex].Advance = 0;
        objPV.ReceiptVoucherDetailColl[RowIndex].ModifiedOn = DateTime.Now;
        objPV.ReceiptVoucherDetailColl[RowIndex].ModifiedBy = intRequestingUserID;
        objPV.ReceiptVoucherDetailColl[RowIndex].Status = new AppConvert((int)RecordStatus.Created);
        objPV.ReceiptVoucherDetailColl[RowIndex].Type = 0;
        objPV.DummyBalanceAmount = DummyBalanceCollAmount;
        Session[TableConstants.CustomerReceiptDetails] = objPV;
        calculateFinalAmount(RowIndex);
        CalculateCollectedBillAmount();
    }
    public void PopulateData()
    {
        AppReceiptVoucher objReceiptVoucher = (AppReceiptVoucher)Session[TableConstants.CustomerReceiptDetails];
        txtReceiptDate.Text = new AppConvert(objReceiptVoucher.ReceiptVoucherDate);
        ddlPaymentType.SelectedValue = new AppConvert(objReceiptVoucher.PaymentType_ID);
        ddlCompanyAccountNo.SelectedValue = new AppConvert(objReceiptVoucher.From_CompanyBank_ID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objReceiptVoucher.From_CompanyBank.BankType.Name);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objReceiptVoucher.From_CompanyBank.BankType.BankBranch);
        if (objReceiptVoucher.PaymentType_ID > 1)
        {
            BindCustomerDetails(objReceiptVoucher.Customer_Company_ID);
            txtChequeNo.Text = new AppConvert(objReceiptVoucher.ChequeNo);
            txtChequeDate.Text = new AppConvert(objReceiptVoucher.ChequeDate);
            divChequeNo.Visible = true;
            divChequeDate.Visible = true;
            ddlCustomerAccountNo.SelectedValue = new AppConvert(objReceiptVoucher.To_CompanyBank_ID);
            ddlCustomerBankBranch.SelectedValue = new AppConvert(objReceiptVoucher.To_CompanyBank.BankType.BankBranch);
            ddlCustomerBank.SelectedValue = new AppConvert(objReceiptVoucher.To_CompanyBank.BankType.Name);
            divBankDetails.Visible = true;
        }
        else
        {
            divBankDetails.Visible = false;
        }
        txtCollectedAmount.Text = new AppConvert(objReceiptVoucher.CollectionAmount);
        txtCustomer.Text = new AppConvert(objReceiptVoucher.Customer_Company.Name);
        hdfCustomerName.Value = new AppConvert(objReceiptVoucher.Customer_Company.Name);
        hdfCustomerID.Value = new AppConvert(objReceiptVoucher.Customer_Company_ID);        
        txtPayee.Text = new AppConvert(objReceiptVoucher.Customer_Company.Name);
        txtLedger.Text = new AppConvert(objReceiptVoucher.Ledger.LedgerName);
        hdfLedgerName.Value = new AppConvert(objReceiptVoucher.Ledger.LedgerName);
        hdfLedgerID.Value = new AppConvert(objReceiptVoucher.Ledger_ID);
        txtVoucherNo.Text = new AppConvert(objReceiptVoucher.ReceiptVoucherNo);
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(objReceiptVoucher.CurrencyExchID);
        //hdfHeaderExchangeCurrencyName.Value = new AppConvert(objReceiptVoucher.cu);
        txtHeaderExchangeCurrency.Text = "";
        txtCurrency.Text = new AppConvert(objReceiptVoucher.CurrencyType.Name);
        hdfCurrencyID.Value = new AppConvert(objReceiptVoucher.CurrencyType_ID);
        hdfCurrencyName.Value = new AppConvert(objReceiptVoucher.CurrencyType.Name);
        txtReceiptDate.Text = new AppConvert(objReceiptVoucher.ReceiptVoucherDate);
        objReceiptVoucher.GetAllReceiptOrderDetails();
        grdSaleBillDetails.DataSource = objReceiptVoucher.ReceiptVoucherDetailColl;
        grdSaleBillDetails.DataBind();
        Session[TableConstants.CustomerReceiptDetails] = objReceiptVoucher;
        CalculateCollectedBillAmount();


    }
    #endregion
    #region Dropdown Events
    protected void ddlPaymentType_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetBankDetails();
        int Receipt_Type = new AppConvert(ddlPaymentType.SelectedValue);
        if (Receipt_Type > 1)
        {
            AppCompanyBank ObjCompanyBank = new AppCompanyBank(intRequestingUserID, Receipt_Type);
            txtLedger.Text = ObjCompanyBank.Ledger.LedgerName;
            hdfLedgerName.Value = ObjCompanyBank.Ledger.LedgerName;
            hdfLedgerID.Value = new AppConvert(ObjCompanyBank.Ledger_ID);
        }
        else
        {
            txtLedger.Text = "";
            hdfLedgerName.Value = "";
            hdfLedgerID.Value = "";
        }
    }
    protected void ddlCompanyAccountNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Company_Bank_Id = new AppConvert(ddlCompanyAccountNo.SelectedValue);
        AppCompanyBank objCOmpanyBank = new AppCompanyBank(intRequestingUserID, Company_Bank_Id);
        ddlCompanyBank.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
    }
    protected void ddlCustomerAccountNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        int Company_Bank_Id = new AppConvert(ddlCustomerAccountNo.SelectedValue);
        AppCompanyBank objCOmpanyBank = new AppCompanyBank(intRequestingUserID, Company_Bank_Id);
        ddlCompanyBank.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
        ddlCompanyBankBranch.SelectedValue = new AppConvert(objCOmpanyBank.BankType_ID);
    }
    #endregion
    #region Button Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppReceiptVoucher RV = (AppReceiptVoucher)Session[TableConstants.CustomerReceiptDetails];
        string Message = "Record successfully Saved";
        if (RV.ID == 0)
        {
            RV.ReceiptVoucherNo = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "CR", System.DateTime.Now,"CR-", 4, ResetCycle.Yearly));
        }
        else
        {
            Message = "Record successfully Updated";
            RV.IsRecordModified = true;
        }
        int Receipt_Type = new AppConvert(ddlPaymentType.SelectedValue);
        if ((Receipt_Type == 2 || Receipt_Type == 3))
        {
            if (string.IsNullOrEmpty(txtChequeNo.Text) == true)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter cheque no','2'" + ");", true);
                return;
            }
            if (string.IsNullOrEmpty(txtChequeDate.Text) == true)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Cheque Date','2'" + ");", true);
                return;
            }
        }
        RV.Company_ID = new AppConvert(LoginCompanyID);
        RV.VoucherType_ID = 17;   //Receipt Voucher
        RV.Customer_Company_ID = new AppConvert(hdfCustomerID.Value);
        RV.ReceiptVoucherDate = new AppConvert(txtReceiptDate.Text);
        RV.PaymentType_ID = new AppConvert(ddlPaymentType.SelectedValue);
        RV.Amount = new AppConvert(txtTotalReceipt.Text);
        RV.Description = txtDescription.Text;        
        RV.Ledger_ID = new AppConvert(hdfLedgerID.Value);// RV.Customer_Company.Customer_Ledger_ID; 
        RV.ChequeNo = new AppConvert(txtChequeNo.Text);
        RV.ChequeDate = new AppConvert(txtChequeDate.Text);
        RV.CurrencyType_ID = new AppConvert(hdfCurrencyID.Value);
        RV.CurrencyExchID = new AppConvert(hdfHeaderExchangeCurrencyID.Value);
        RV.ExchangeRate = new AppConvert(txtHeaderExchangeCurrencyRate.Text);
        RV.From_CompanyBank_ID = new AppConvert(ddlCompanyAccountNo.SelectedValue);
        RV.To_CompanyBank_ID = new AppConvert(ddlCustomerAccountNo.SelectedValue);
        RV.Advance = 0;
        RV.CollectionAmount = new AppConvert(txtCollectedAmount.Text);
        RV.PayerName = txtPayee.Text;
        RV.ModifiedBy = intRequestingUserID;
        RV.ModifiedOn = System.DateTime.Now;
        RV.Status = new AppConvert((int)RecordStatus.Created);
        RV.Type = 0;
        RV.ReceiptVoucherDetailColl.RemoveAll(x => x.IsReceiptChecked == false);
        RV.Save();
        RV.AddNewReceiptAllocationDetails();
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + Message + "','1'" + ");", true);
        #region Add LedgerEntries
        LedgerEntry(LoginCompanyID, 101301, 1013, RV.Ledger_ID, RV.ID, RV.ReceiptVoucherDate,0, RV.Amount, RV.ReceiptVoucherNo, RV.ModifiedBy, RV.ModifiedOn, 0,RV.Status, RV.Type,
                              0, 0, RV.VoucherType_ID, RV.Description, "", RV.PaymentType_ID);
        if (RV.ReceiptVoucherDetailColl.Count > 0)
        {
            foreach (var item in RV.ReceiptVoucherDetailColl)
            {
                if (item.Amount != 0)
                {
                    LedgerEntry(LoginCompanyID, 101401, 1014, item.Ledger_ID, item.ID, RV.ReceiptVoucherDate, item.Amount ,0, RV.ReceiptVoucherNo, item.ModifiedBy, item.ModifiedOn, item.Status, RV.Status, item.Type,
                                 item.ProfitCenter_ID, item.CostCenter_ID, RV.VoucherType_ID, item.ReceiptVoucherDesc, "", RV.PaymentType_ID);
                }
            }
        }
        #endregion
        Session.Remove(TableConstants.CustomerReceiptDetails);
        //Response.Redirect("T_CustomerReceiptSummary.aspx");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.CustomerReceiptDetails);
        Response.Redirect("T_CustomerReceiptSummary.aspx");
    }
    #endregion
    #region GridView Events
    protected void grdSaleBillDetails_PreRender(object sender, EventArgs e)
    {
        if (grdSaleBillDetails.Rows.Count > 0)
        {
            grdSaleBillDetails.UseAccessibleHeader = true;
            grdSaleBillDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region CheckBox Events
    protected void ChkSelect_CheckedChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        AppReceiptVoucher objPV = (AppReceiptVoucher)Session[TableConstants.CustomerReceiptDetails];
        CheckBox ChkSelect = (CheckBox)grdSaleBillDetails.Rows[RowIndex].FindControl("ChkSelect");
        TextBox txtReceiptAmt = (TextBox)grdSaleBillDetails.Rows[RowIndex].FindControl("txtReceiptAmt");
        Label lblBillAmount = (Label)grdSaleBillDetails.Rows[RowIndex].FindControl("lblBillAmount");
        Label lblTotalReceiptAmount = (Label)grdSaleBillDetails.Rows[RowIndex].FindControl("lblTotalReceiptAmount");
        Label lblReceiptAmount = (Label)grdSaleBillDetails.Rows[RowIndex].FindControl("lblReceiptAmount");
        Label lblBalanceAmount = (Label)grdSaleBillDetails.Rows[RowIndex].FindControl("lblBalanceAmount");
        decimal NetReceivableAmount = 0;
        decimal CurrentBillReceiptAmount = 0;
        decimal AlreadyCurrentBillReceipt = 0;
        decimal BillAmount = 0;
        decimal TotalReceiptAmount = 0;
        decimal BalanceAmount = 0;
        bool IsChecked = false;
        decimal CollectedAmount = new AppConvert(txtCollectedAmount.Text);
        if(CollectedAmount<=0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Enter Collected Bill Amount','2'" + ");", true);
            txtReceiptAmt.Text = "0.00";
            ChkSelect.Checked = false;
            return;
        }
        decimal DummyBalanceCollAmount = objPV.DummyBalanceAmount;
        if (CollectedAmount == DummyBalanceCollAmount)
        {
            DummyBalanceCollAmount = CollectedAmount;
        }
        CurrentBillReceiptAmount = new AppConvert(txtReceiptAmt.Text);
        AlreadyCurrentBillReceipt = objPV.ReceiptVoucherDetailColl[RowIndex].Amount;
        BillAmount = objPV.ReceiptVoucherDetailColl[RowIndex].BillAmount;
        TotalReceiptAmount = objPV.ReceiptVoucherDetailColl[RowIndex].ReceiptAmount;
        BalanceAmount = objPV.ReceiptVoucherDetailColl[RowIndex].BalanceAmount;
        if (ChkSelect.Checked == true)
        {
            txtReceiptAmt.Enabled = true;
            if(CurrentBillReceiptAmount<=0)
            {
                CurrentBillReceiptAmount = new AppConvert(lblBalanceAmount.Text);
            }
            if (CurrentBillReceiptAmount > DummyBalanceCollAmount)
            {
                BalanceAmount = CurrentBillReceiptAmount - DummyBalanceCollAmount;
                CurrentBillReceiptAmount = DummyBalanceCollAmount;
                DummyBalanceCollAmount = 0;
            }
            else
            {
                DummyBalanceCollAmount = DummyBalanceCollAmount - CurrentBillReceiptAmount;
                BalanceAmount = 0;
            }            
            IsChecked = true;
        }
        else
        {
            if (CurrentBillReceiptAmount > 0)
            {
                DummyBalanceCollAmount += CurrentBillReceiptAmount;
                CurrentBillReceiptAmount = 0;
                txtReceiptAmt.Enabled = false;
                IsChecked = false;
            }
        }
        BalanceAmount = BillAmount - (TotalReceiptAmount + CurrentBillReceiptAmount);
        lblReceiptAmount.Text = new AppConvert(CurrentBillReceiptAmount);
        txtReceiptAmt.Text = new AppConvert(CurrentBillReceiptAmount);
        NetReceivableAmount = (NetReceivableAmount - AlreadyCurrentBillReceipt) + CurrentBillReceiptAmount;
        objPV.ReceiptVoucherDetailColl[RowIndex].Amount = new AppConvert(CurrentBillReceiptAmount);
        objPV.ReceiptVoucherDetailColl[RowIndex].Ledger_ID = objPV.Customer_Company.Customer_Ledger_ID;
        objPV.ReceiptVoucherDetailColl[RowIndex].Advance = 0;
        objPV.ReceiptVoucherDetailColl[RowIndex].ModifiedOn = DateTime.Now;
        objPV.ReceiptVoucherDetailColl[RowIndex].ModifiedBy = intRequestingUserID;
        objPV.ReceiptVoucherDetailColl[RowIndex].Status = new AppConvert((int)RecordStatus.Created);
        objPV.ReceiptVoucherDetailColl[RowIndex].Type = 0;
        objPV.ReceiptVoucherDetailColl[RowIndex].IsReceiptChecked = IsChecked;
        objPV.DummyBalanceAmount = DummyBalanceCollAmount;
        Session[TableConstants.CustomerReceiptDetails] = objPV;      
        calculateFinalAmount(RowIndex);
        CalculateCollectedBillAmount();
    }
    #endregion     
}