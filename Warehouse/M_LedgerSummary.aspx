﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="M_LedgerSummary" Codebehind="M_LedgerSummary.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function AutoCompleteSearch(sender, eventArgs) {
            var id = sender._id;
            var autocompleteId = id.split('_');
            var replacetxt = autocompleteId[2].replace('txt', '');
            var txt = $get(autocompleteId[0] + '_txt' + replacetxt);
            var txtName = $get(autocompleteId[0] + '_hdf' + replacetxt + 'Name');
            var txtID = $get(autocompleteId[0] + '_hdf' + replacetxt + 'ID');
            txt.value = eventArgs.get_text();
            txtName.value = eventArgs.get_text();
            txtID.value = eventArgs.get_value();
        }
        function ClearAutocompleteTextBox(sender) {
            var id = sender.id;
            var autocompleteId = id.split('_');
            var replacetxt = autocompleteId[1].replace('txt', '');
            var txt = $get(autocompleteId[0] + '_txt' + replacetxt);
            var txtName = $get(autocompleteId[0] + '_hdf' + replacetxt + 'Name');
            var txtID = $get(autocompleteId[0] + '_hdf' + replacetxt + 'ID');
            if (txt.value == "No Records Found" || txt.value != txtName.value) {
                txt.value = "";
                txtName.value = "";
                txtID.value = "";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <h3>Ledger Summary</h3>
                    <hr />
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:Label ID="lblAccountNo" runat="server" CssClass="bold" Text="Account No"></asp:Label>
                            <asp:TextBox ID="txtAccountNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-3" style="display: none">
                            <asp:Label ID="lblLedgerCode" runat="server" CssClass="bold" Text="Ledger Code"></asp:Label>
                            <asp:TextBox ID="txtLedgerCode" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblLedgerName" runat="server" CssClass="bold" Text="Ledger Name"></asp:Label>
                            <asp:TextBox ID="txtLedgerName" TabIndex="1" runat="server" AutoCompleteType="None" AutoComplete="Off"
                                CssClass=" form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblLedgerType" CssClass="bold " runat="server" Text="Ledger Type"> </asp:Label>


                            <asp:DropDownList ID="ddlLedgerType" TabIndex="1" runat="server" CssClass="form-control input-sm">
                                <asp:ListItem Value="-1">Select Ledger Type</asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <div class="col-lg-3">
                            <asp:Label ID="lblAccountType" CssClass="bold " runat="server" Text="Account Type"> </asp:Label>
                            <asp:DropDownList ID="ddlAccountType" TabIndex="1" runat="server" CssClass="form-control input-sm">
                                <asp:ListItem Value="-1">Select Account Type</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:Label ID="lblParentLedgerGrp" CssClass="bold " runat="server" Text="Parent Ledger Group"> </asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtParentLedgerGrp" TabIndex="3" placeholder="Select Parent Ledger Group" runat="server"
                                    AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                <asp:HiddenField runat="server" ID="hdfParentLedgerGrpName" />
                                <asp:HiddenField runat="server" ID="hdfParentLedgerGrpID" Value="0" />
                            </div>
                            <asp:AutoCompleteExtender ID="Ace_txtParentLedgerGrp" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                OnClientItemSelected="AutoCompleteSearch" ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtParentLedgerGrp">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:Label ID="lblTypeOfLedger" Text="Type of ledger" runat="server" CssClass="bold"></asp:Label>
                            <asp:DropDownList ID="ddlTypeofLedger" runat="server" CssClass="form-control input-sm" >
                                <asp:ListItem Value="0">ALL</asp:ListItem>
                                <asp:ListItem Value="1">Control Account</asp:ListItem>
                                <asp:ListItem Value="2">Ledger Group</asp:ListItem>
                                <asp:ListItem Value="3">General Ledger</asp:ListItem>
                            </asp:DropDownList>
                            
                         
                        </div>

                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="btn-group pull-right">
                                    <asp:LinkButton runat="server" Text="Search" CssClass="btn  btn-sm btn-default" ID="lbtnSearch" OnClick="lbtnSearch_Click">                                    
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="lbtnNewLedger" Text="New Ledger" CssClass="btn btn-sm btn-default" OnClick="lbtnNewLedger_Click">                                 
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-body">
                <asp:GridView ID="grdLedgerSummary" runat="server" AutoGenerateColumns="False" GridLines="Horizontal"
                    OnPreRender="grdLedgerSummary_PreRender" OnRowDeleting="grdLedgerSummary_RowDeleting"
                    CssClass="table table-striped table-hover text-nowrap dt-responsive nowrap">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblSelect" Text="Delete" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete" CommandName="DELETE" CommandArgument='<%#Eval("ID") %>' Text="" ToolTip="Delete" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="hdrlblLedgerFlag" Text="Type" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblLedgerFlag" runat="server" Text='<%# Convert.ToString(Eval("LedgerFlag"))=="1"?"CA":Convert.ToString(Eval("LedgerFlag"))=="2"?"LG":"GL" %>' CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <HeaderTemplate>
                                <asp:Label ID="hdrLedgerCode" Text="Ledger Code" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink TabIndex="1" CausesValidation="false" ID="hlLedgerCode" runat="server"
                                    Text='<%# Eval("LedgerCode")%>' ToolTip="History" NavigateUrl='<% #"M_LedgerEntry.aspx?LedgerId=" + Eval("ID")%>' Target="_blank"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="hdrlblAccountNo" Text="Account No" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink TabIndex="1" CausesValidation="false" ID="hlLedgerCode" runat="server"
                                    Text='<%# Eval("AccountNo")%>' ToolTip="History" NavigateUrl='<% #"M_Ledger.aspx?LedgerId=" + Eval("ID")%>' Target="_blank"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="HdrLedgerName" Text="Name" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblLedgerName" runat="server" Text='<%# Eval("LedgerName") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="hdrlblAccountType" Text="Acount Type" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAccountType" runat="server" Text='<%#Eval("AccountType.AccountTypeName") %>' CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="hdrlblLedgerType" Text="Ledger Type" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblLedgerType" runat="server" Text='<%#Eval("LedgerType.LedgerTypeName") %>' CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="hdrParentLedger" Text="Parent Ledger" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblParentLedger" runat="server" Text='<%#Eval("Parent.LedgerName") %>' CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
