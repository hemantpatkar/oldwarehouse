﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class T_PurchaseGRN : BigSunPage
{
    #region PageLoad
    AppPurchaseGRN PurchaseGRN = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        Ce_PurchaseGRNDate.SelectedDate = DateTime.Now;
        if (!IsPostBack)
        {
            PurchaseGRN = new AppPurchaseGRN(intRequestingUserID);
            string ID = Convert.ToString(Request.QueryString["PurchaseGRNID"]);
            if (!string.IsNullOrEmpty(ID))
            {
                int PurchaseGRNID = new AppUtility.AppConvert(ID);
                PurchaseGRN = new AppObjects.AppPurchaseGRN(intRequestingUserID, PurchaseGRNID);
                PopulateData();
            }
            else
            {
                DefaultData();
                if (Convert.ToString(Request.QueryString["Msg"]) == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
                }
            }
            Session.Add(TableConstants.PurchaseGRN, PurchaseGRN);
            BindData();
            SetValueToGridFooter();
        }
    }
    #endregion
    #region TextBox Events
    protected void txtPurchaseOrder_TextChanged(object sender, EventArgs e)
    {
        PurchaseGRN = (AppPurchaseGRN)HttpContext.Current.Session[TableConstants.PurchaseGRN];
        PurchaseGRN.PurchaseOrder_ID = new AppConvert(hdfPurchaseOrderID.Value);
        PurchaseGRN.PurchaseOrder = new AppPurchaseOrder(intRequestingUserID, PurchaseGRN.PurchaseOrder_ID);
        txtPurchaseOrderDate.Text = new AppConvert(PurchaseGRN.PurchaseOrder.PurchaseOrderDate);
        txtTotalAmount.Text = new AppConvert(PurchaseGRN.PurchaseOrder.FinalAmount);
        txtTermsAndConditions.Text = new AppConvert(PurchaseGRN.PurchaseOrder.TermsAndConditions.Statement);
        hdfTermsAndConditionsName.Value = new AppConvert(PurchaseGRN.PurchaseOrder.TermsAndConditions.Statement);
        hdfTermsAndConditionsID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.TermsAndConditions.ID);
        txtDiscount.Text = new AppConvert(PurchaseGRN.PurchaseOrder.DiscountValue);
        hdfDiscountType.Value = new AppConvert(PurchaseGRN.PurchaseOrder.DiscountType == 0 ? 1 : PurchaseGRN.PurchaseOrder.DiscountType);
        txtHeaderExchangeCurrency.Text = PurchaseGRN.PurchaseOrder.CurrencyType.Name;
        hdfHeaderExchangeCurrencyName.Value = new AppConvert(PurchaseGRN.PurchaseOrder.CurrencyType.Name);
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.CurrencyType_ID);
        txtDeliveryAddress.Text = PurchaseGRN.GetDeliveryAddress;
        hdfDeliveryAddressName.Value = new AppConvert(PurchaseGRN.GetDeliveryAddress);
        hdfDeliveryAddressID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.Delivery_CompanyAddress_ID);
        txtCompanyAddress.Text = PurchaseGRN.SetCompanyAddress;
        hdfCompanyAddressID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.CompanyAddress_ID);
        hdfCompanyAddressName.Value = PurchaseGRN.SetCompanyAddress;
        txtHeaderTax.Text = new AppConvert(PurchaseGRN.SetTaxType);
        hdfHeaderTaxName.Value = new AppConvert(PurchaseGRN.SetTaxType);
        hdfHeaderTaxID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.TaxType_ID);
        txtHeaderTaxAmount.Text = new AppConvert(PurchaseGRN.PurchaseOrder.TaxAmount);
        hdfVendorID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.Vendor_Company_ID);
        txtVendorAddress.Text = PurchaseGRN.SetVendorAddress;
        hdfVendorAddressName.Value = new AppConvert(PurchaseGRN.SetVendorAddress);
        hdfVendorAddressID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.Vendor_CompanyAddress_ID);
        PurchaseGRN.PurchaseGRNItemColl.Clear();
        PurchaseGRN.GetAllPurchaseOrderDetails();
        grdPurchaseGRNItemDetails.DataSource = PurchaseGRN.PurchaseGRNItemColl;
        grdPurchaseGRNItemDetails.DataBind();
        SetValueToGridFooter();
    }
    protected void txtReceivedQuantity_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtReceivedQuantity = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtReceivedQuantity");
        TextBox txtAcceptedQuantity = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtAcceptedQuantity");
        LineItemCalculation(RowIndex);
        txtAcceptedQuantity.Focus();
    }
    protected void txtAcceptedQuantity_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtAcceptedQuantity = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtAcceptedQuantity");
        TextBox txtRate = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtRate");
        LineItemCalculation(RowIndex);
        txtRate.Focus();
    }
    protected void txtRate_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtRate = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtRate");
        TextBox txtDiscount = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtDiscount");
        LineItemCalculation(RowIndex);
        txtDiscount.Focus();
    }
    protected void txtLineItemTax_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        LineItemCalculation(RowIndex);
    }
    protected void txtItem_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtRate = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtDiscount");
        TextBox txtLineItemTax = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtLineItemTax");
        LineItemCalculation(RowIndex);
        txtLineItemTax.Focus();
    }
    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtRate = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtDiscount");
        TextBox txtLineItemTax = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtLineItemTax");
        LineItemCalculation(RowIndex);
        txtLineItemTax.Focus();
    }
    protected void txtRejectedQuantity_TextChanged(object sender, EventArgs e)
    {
        int RowIndex = ((GridViewRow)((sender as Control)).NamingContainer).RowIndex;
        TextBox txtRejectedQuantity = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtRejectedQuantity");
        TextBox txtRate = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtRate");
        LineItemCalculation(RowIndex);
        txtRate.Focus();
    }
    #endregion
    #region Function
    public void BindData()
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        if (PurchaseGRN == null)
        {
            PurchaseGRN = new AppPurchaseGRN(intRequestingUserID);
        }
        grdPurchaseGRNItemDetails.DataSource = PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == new AppConvert((int)RecordStatus.Created));
        grdPurchaseGRNItemDetails.DataBind();
        if (PurchaseGRN.PurchaseGRNItemColl.Count <= 0)
        {
            AddEmptyRow(1);
        }
    }
    public void AddEmptyRow(int NumberOfRows)
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        if (PurchaseGRN == null)
        {
            PurchaseGRN = new AppPurchaseGRN(intRequestingUserID);
        }
        AppPurchaseGRNItem PurchaseGRNItem = new AppPurchaseGRNItem(intRequestingUserID);
        for (int i = 0; i < NumberOfRows; i++)
        {
            if (PurchaseGRN.PurchaseGRNItemColl.Count == 0)
            {
                PurchaseGRN.PurchaseGRNItemColl.Add(PurchaseGRNItem);
            }
            else
            {
                PurchaseGRN.PurchaseGRNItemColl.Insert(0, PurchaseGRNItem);
            }
            Session.Add(TableConstants.PurchaseGRN, PurchaseGRN);
        }
        BindData();
    }
    public void PopulateData()
    {
        txtPurchaseOrder.Enabled = false;
        txtPurchaseOrderDate.Text = new AppConvert(PurchaseGRN.PurchaseOrder.PurchaseOrderDate);
        txtPurchaseGRNDate.Text = new AppConvert(PurchaseGRN.GRNDate);
        txtPurchaseOrder.Text = PurchaseGRN.PurchaseOrder.PurchaseOrderNumber;
        hdfPurchaseOrderID.Value = new AppConvert(PurchaseGRN.PurchaseOrder_ID);
        hdfPurchaseOrderName.Value = PurchaseGRN.PurchaseOrder.PurchaseOrderNumber;
        txtPurchaseGRNNo.Text = PurchaseGRN.GRNNumber;
        txtStatus.Text = PurchaseGRN.StatusName;
        hdfVendorID.Value = new AppConvert(PurchaseGRN.Vendor_Company_ID);
        txtTotalAmount.Text = new AppConvert(PurchaseGRN.InvoiceBaseAmount);
        txtTermsAndConditions.Text = new AppConvert(PurchaseGRN.TermsAndConditions.Statement);
        hdfTermsAndConditionsName.Value = txtTermsAndConditions.Text;
        hdfTermsAndConditionsID.Value = new AppConvert(PurchaseGRN.TermsAndConditions.ID);
        txtDiscount.Text = new AppConvert(PurchaseGRN.DiscountValue);
        hdfDiscountType.Value = new AppConvert(PurchaseGRN.DiscountType);
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);
        txtHeaderExchangeCurrency.Text = PurchaseGRN.CurrencyType.Name;
        hdfHeaderExchangeCurrencyName.Value = txtHeaderExchangeCurrency.Text;
        hdfHeaderExchangeCurrencyID.Value = new AppConvert(PurchaseGRN.CurrencyType_ID);
        txtCompanyAddress.Text = PurchaseGRN.SetCompanyAddress;
        hdfCompanyAddressName.Value = txtCompanyAddress.Text;
        hdfCompanyAddressID.Value = new AppConvert(PurchaseGRN.CompanyAddress_ID);
        txtVendorAddress.Text = PurchaseGRN.SetVendorAddress;
        hdfVendorAddressName.Value = txtVendorAddress.Text;
        hdfVendorAddressID.Value = new AppConvert(PurchaseGRN.Vendor_CompanyAddress_ID);
        txtDeliveryAddress.Text = PurchaseGRN.GetDeliveryAddress;
        hdfDeliveryAddressName.Value = txtDeliveryAddress.Text;
        hdfDeliveryAddressID.Value = new AppConvert(PurchaseGRN.PurchaseOrder.Delivery_CompanyAddress_ID);
        //txtRemark.Text = PurchaseGRN.Remark;        
        txtHeaderTax.Text = PurchaseGRN.TaxType.Name;
        hdfHeaderTaxName.Value = txtHeaderTax.Text;
        hdfHeaderTaxID.Value = new AppConvert(PurchaseGRN.TaxType_ID);
        txtHeaderTaxAmount.Text = new AppConvert(PurchaseGRN.TaxAmount);
        txtDCDate.Text = new AppConvert(PurchaseGRN.DCDate);
        txtDeliveryChallanNo.Text = PurchaseGRN.DCReference;
    }
    public void DefaultData()
    {
        txtPurchaseGRNDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtCompanyAddress.Text = base.DefaultCompanyAddress;
        hdfCompanyAddressName.Value = txtCompanyAddress.Text;
        hdfCompanyAddressID.Value = new AppConvert(this.DefaultCompanyAddressID);
        txtHeaderBaseCurrency.Text = this.DefaultBaseCurrency;
        hdfHeaderBaseCurrencyID.Value = new AppConvert(this.DefaultBaseCurrencyID);
        txtDeliveryAddress.Text = this.DefaultDeliveryAddress;
        hdfDeliveryAddressName.Value = txtDeliveryAddress.Text;
        hdfDeliveryAddressID.Value = new AppConvert(this.DefaultDeliveryAddressID);
        ACEtxtDeliveryAddress.ContextKey = new AppConvert(Session[TableConstants.LoginCompanyID]);
        ACEtxtCompanyAddress.ContextKey = new AppConvert(Session[TableConstants.LoginCompanyID]);
    }
    public void LineItemCalculation(int RowIndex)
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        TextBox txtQuantity = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtQuantity");
        TextBox txtReceivedQuantity = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtReceivedQuantity");
        TextBox txtAcceptedQuantity = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtAcceptedQuantity");
        TextBox txtRejectedQuantity = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtRejectedQuantity");
        TextBox txtRate = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtRate");
        TextBox txtBasePrice = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtBasePrice");
        TextBox txtNetAmount = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtNetAmount");
        TextBox txtDiscount = (TextBox)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("txtDiscount");
        HiddenField hdfDiscountType = (HiddenField)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("hdfDiscountType");
        HiddenField hdfLineItemTaxID = (HiddenField)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("hdfLineItemTaxID");
        HiddenField hdfUOMID = (HiddenField)grdPurchaseGRNItemDetails.Rows[RowIndex].FindControl("hdfUOMID");
        decimal Quantity = new AppUtility.AppConvert(txtQuantity.Text);
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].Quantity = Quantity;
        decimal ReceivedQty = new AppConvert(txtReceivedQuantity.Text);
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].ReceivedQuantity = ReceivedQty;
        if (ReceivedQty > Quantity)
        {
            PurchaseGRN.PurchaseGRNItemColl[RowIndex].ReceivedQuantity = Quantity;
        }
        decimal AcceptedQty = new AppUtility.AppConvert(txtAcceptedQuantity.Text);
        decimal RejectedQty = new AppUtility.AppConvert(txtRejectedQuantity.Text);
        if (AcceptedQty > ReceivedQty)
        {
            PurchaseGRN.PurchaseGRNItemColl[RowIndex].AcceptedQuantity = ReceivedQty;
        }
        else
        {
            PurchaseGRN.PurchaseGRNItemColl[RowIndex].AcceptedQuantity = AcceptedQty;
        }
        AcceptedQty = PurchaseGRN.PurchaseGRNItemColl[RowIndex].AcceptedQuantity;
        if (AcceptedQty <= 0)
        {
            PurchaseGRN.PurchaseGRNItemColl[RowIndex].RejectQuantity = 0;
        }
        else
        {
            PurchaseGRN.PurchaseGRNItemColl[RowIndex].RejectQuantity = Quantity - AcceptedQty;
        }
        RejectedQty = PurchaseGRN.PurchaseGRNItemColl[RowIndex].RejectQuantity;
        decimal SumOfQty = ReceivedQty - (AcceptedQty + RejectedQty);
        if (SumOfQty > Quantity)
        {
            PurchaseGRN.PurchaseGRNItemColl[RowIndex].ReceivedQuantity = 0;
            PurchaseGRN.PurchaseGRNItemColl[RowIndex].AcceptedQuantity = 0;
            PurchaseGRN.PurchaseGRNItemColl[RowIndex].RejectQuantity = 0;
        }
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].ItemRate = new AppUtility.AppConvert(txtRate.Text);
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].DiscountValue = new AppUtility.AppConvert(txtDiscount.Text);
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].DiscountAmount = PurchaseGRN.PurchaseGRNItemColl[RowIndex].CalDiscountAmount;
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].DiscountType = new AppConvert(hdfDiscountType.Value == "0" ? 1 : new AppConvert(hdfDiscountType.Value));
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].TaxType_ID = new AppUtility.AppConvert(hdfLineItemTaxID.Value);
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].UOMType_ID = new AppUtility.AppConvert(hdfUOMID.Value);
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].ModifiedBy = intRequestingUserID;
        PurchaseGRN.PurchaseGRNItemColl[RowIndex].ModifiedOn = System.DateTime.Now;
        Session.Add(TableConstants.PurchaseGRN, PurchaseGRN);
        BindData();
        SetValueToGridFooter();
        HeaderCalculation();
    }
    public void SetValueToGridFooter()
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        Label lblFooterNetAmount = (Label)grdPurchaseGRNItemDetails.FooterRow.FindControl("lblFooterNetAmount");
        lblFooterNetAmount.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.NetAmount)).ToString("#,##0.00");
        Label lblFooterTax = (Label)grdPurchaseGRNItemDetails.FooterRow.FindControl("lblFooterTax");
        lblFooterTax.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.TaxAmount)).ToString("#,##0.00");
        Label lblFooterDiscount = (Label)grdPurchaseGRNItemDetails.FooterRow.FindControl("lblFooterDiscount");
        lblFooterDiscount.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.DiscountValue)).ToString("#,##0.00");
        Label lblFooterBasePrice = (Label)grdPurchaseGRNItemDetails.FooterRow.FindControl("lblFooterBasePrice");
        lblFooterBasePrice.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.BasePrice)).ToString("#,##0.00");
        Label lblFooterQuantity = (Label)grdPurchaseGRNItemDetails.FooterRow.FindControl("lblFooterQuantity");
        lblFooterQuantity.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.Quantity)).ToString("#,##0.00");
        Label lblFooterReceivedQuantity = (Label)grdPurchaseGRNItemDetails.FooterRow.FindControl("lblFooterReceivedQuantity");
        lblFooterReceivedQuantity.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.ReceivedQuantity)).ToString("#,##0.00");
        Label lblFooterAcceptedQuantity = (Label)grdPurchaseGRNItemDetails.FooterRow.FindControl("lblFooterAcceptedQuantity");
        lblFooterAcceptedQuantity.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.AcceptedQuantity)).ToString("#,##0.00");
        Label lblFooterRejectedQuantity = (Label)grdPurchaseGRNItemDetails.FooterRow.FindControl("lblFooterRejectedQuantity");
        lblFooterRejectedQuantity.Text = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.RejectQuantity)).ToString("#,##0.00");
        HeaderCalculation();
    }
    public void HeaderCalculation()
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        PurchaseGRN.DiscountType = new AppConvert(hdfDiscountType.Value);
        PurchaseGRN.DiscountValue = new AppConvert(txtDiscount.Text);
        PurchaseGRN.FinalAmount = Convert.ToDecimal(PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).Sum(x => x.NetAmount).ToString("#,##0.00"));
        txtTotalAmount.Text = new AppConvert(PurchaseGRN.PostDiscountPrice);
        PurchaseGRN.TaxType_ID = new AppConvert(hdfHeaderTaxID.Value);
        PurchaseGRN.TaxType.CalculateTax(new AppConvert(txtTotalAmount.Text), 0, 0);
        txtHeaderTaxAmount.Text = new AppConvert(PurchaseGRN.TaxType.TaxValue);
    }
    public void SavePurchaseGRNHeader()
    {
        AppPurchaseGRN PO = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        if (PO.ID == 0)
        {
            PO.GRNNumber = new AppConvert(AppUtility.AutoID.GetNextNumber(1, "GRN", System.DateTime.Now, "GRN-", 4, ResetCycle.Yearly));
        }
        PO.Company_ID = new AppConvert(LoginCompanyID);
        PO.CompanyAddress_ID = new AppConvert(hdfCompanyAddressID.Value);
        PO.Vendor_Company_ID = new AppConvert(hdfVendorID.Value);
        PO.Vendor_CompanyAddress_ID = new AppConvert(hdfVendorAddressID.Value);
        PO.DCReference = new AppConvert(txtDeliveryChallanNo.Text);
        PO.DCDate = new AppConvert(txtDCDate.Text);
        PO.GRNDate = new AppConvert(txtPurchaseGRNDate.Text);
        PO.PurchaseOrder_ID = new AppConvert(hdfPurchaseOrderID.Value);
        PO.TermsAndConditions_ID = new AppConvert(hdfTermsAndConditionsID.Value);
        PO.TaxType_ID = new AppConvert(hdfHeaderTaxID.Value);
        PO.CurrencyType_ID = new AppConvert(hdfHeaderExchangeCurrencyID.Value);
        PO.InvoiceBaseAmount = new AppConvert(txtTotalAmount.Text);
        PO.TaxAmount = new AppConvert(txtHeaderTaxAmount.Text);
        PO.OtherCharges = 0;
        PO.FinalAmount = PO.HeaderFinalAmount;
        PO.DiscountValue = new AppConvert(txtDiscount.Text);
        PO.DiscountType = new AppConvert(hdfDiscountType.Value);
        PO.DiscountAmount = PO.CalDiscountAmount;
        PO.RoundAmount = 0;
        PO.Status = new AppConvert((int)RecordStatus.Created);
        PO.Type = 0;
        PO.ModifiedBy = intRequestingUserID;
        PO.ModifiedOn = System.DateTime.Now;
        PO.Save();
    }
    #endregion
    #region GridView Events
    protected void grdPurchaseGRNItemDetails_PreRender(object sender, EventArgs e)
    {
        if (grdPurchaseGRNItemDetails.Rows.Count > 0)
        {
            grdPurchaseGRNItemDetails.UseAccessibleHeader = true;
            grdPurchaseGRNItemDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdPurchaseGRNItemDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
            AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
            PurchaseGRN.PurchaseGRNItemColl.Where(x => x.Status == 0).ToList()[row.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
            Session.Add(TableConstants.PurchaseGRN, PurchaseGRN);
            BindData();
            SetValueToGridFooter();
        }
    }
    #endregion
    #region Button Events
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppPurchaseGRN PurchaseGRN = (AppPurchaseGRN)Session[TableConstants.PurchaseGRN];
        PurchaseGRN.PurchaseGRNItemColl.RemoveAll(x => x.Item_ID == 0);
        if (PurchaseGRN.PurchaseGRNItemColl.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please add atleast one item','2'" + ");", true);
            return;
        }
        Session.Add(TableConstants.PurchaseGRN, PurchaseGRN);
        SavePurchaseGRNHeader();

        #region Add Ledger Entries
        LedgerEntry(LoginCompanyID, 100101, 1001, PurchaseGRN.Company.DefaultCompanyBank.Ledger_ID, PurchaseGRN.ID, PurchaseGRN.GRNDate, PurchaseGRN.InvoiceBaseAmount, 0, PurchaseGRN.GRNNumber, PurchaseGRN.ModifiedBy, PurchaseGRN.ModifiedOn, 0, PurchaseGRN.Status, PurchaseGRN.Type);

        if (PurchaseGRN.TaxType.TaxTypeConfigurationColl.Count > 0 && PurchaseGRN.TaxType_ID > 0)
        {
            List<TaxDetails> objTaxDetails = PurchaseGRN.TaxType.CalculateTax(PurchaseGRN.InvoiceBaseAmount, 0, 0);
            foreach (TaxDetails item in objTaxDetails)
            {
                if (item.Ledger_ID != 0)
                {
                    UpdateLedgerEntryStatus(101502, 1015, PurchaseGRN.ID, new AppUtility.AppConvert((int)RecordStatus.Deleted));
                    LedgerEntry(LoginCompanyID, 100102, 1001, item.Ledger_ID, PurchaseGRN.ID, PurchaseGRN.GRNDate, 0, item.TaxValue, PurchaseGRN.GRNNumber, PurchaseGRN.ModifiedBy, PurchaseGRN.ModifiedOn, 0, PurchaseGRN.Status, PurchaseGRN.Type);
                }
            }
        }


        // TO update ledger entries of Discount Type  - Pending Work
        LedgerEntry(LoginCompanyID, 100103, 1001, 0, PurchaseGRN.ID, PurchaseGRN.GRNDate, PurchaseGRN.DiscountAmount, 0, PurchaseGRN.GRNNumber, PurchaseGRN.ModifiedBy, PurchaseGRN.ModifiedOn, 0, PurchaseGRN.Status, PurchaseGRN.Type);
        // TO update ledger entries of Round Amount  - Pending Work
        LedgerEntry(LoginCompanyID, 100104, 1001, PurchaseGRN.Vendor_Company.Vendor_Ledger_ID, PurchaseGRN.ID, PurchaseGRN.GRNDate, PurchaseGRN.RoundAmount, 0, PurchaseGRN.GRNNumber, PurchaseGRN.ModifiedBy, PurchaseGRN.ModifiedOn, 0, PurchaseGRN.Status, PurchaseGRN.Type);

        if (PurchaseGRN.PurchaseGRNItemColl.Count > 0)
        {
            foreach (var item in PurchaseGRN.PurchaseGRNItemColl)
            {
                if (item.ItemBaseAmount != 0) { LedgerEntry(LoginCompanyID, 100201, 1002, item.Item.Purchase_Ledger_ID, PurchaseGRN.ID, PurchaseGRN.GRNDate, 0, item.ItemBaseAmount, PurchaseGRN.GRNNumber, item.ModifiedBy, item.ModifiedOn, item.Status, PurchaseGRN.Status, item.Type); }

                if (item.TaxAmount != 0)
                {
                    if (item.TaxType.TaxTypeConfigurationColl.Count > 0 && item.TaxType_ID > 0)
                    {
                        List<TaxDetails> objTaxDetails = item.TaxType.CalculateTax(item.NetAmount, 0, 0);
                        foreach (TaxDetails td in objTaxDetails)
                        {
                            if (td.Ledger_ID != 0)
                            {
                                UpdateLedgerEntryStatus(101602, 1016, item.ID, new AppUtility.AppConvert((int)RecordStatus.Deleted));
                                LedgerEntry(LoginCompanyID, 100202, 1002, td.Ledger_ID, item.ID, PurchaseGRN.GRNDate, 0, td.TaxValue, PurchaseGRN.GRNNumber, item.ModifiedBy, item.ModifiedOn, item.Status, PurchaseGRN.Status, item.Type);
                            }
                        }
                    }
                }
            }

        }
        #endregion
        Session.Remove(TableConstants.PurchaseGRN);
        Response.Redirect("T_PurchaseGRN.aspx?Msg=1");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("T_PurchaseGRNSummary.aspx");
    }
    protected void btnAddLineItem_Click(object sender, EventArgs e)
    {
    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {

    } 
    #endregion
}