﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="VendorSummary" Codebehind="VendorSummary.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <h3>Vendor Summary </h3>
                    <hr />
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:Label ID="lblVendorCode" runat="server" CssClass="bold " Text="Vendor Code"></asp:Label>
                            <asp:TextBox ID="txtVendorCode" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblVendorName" runat="server" CssClass="bold " Text="Vendor Name"></asp:Label>
                            <asp:TextBox ID="txtVendorName" TabIndex="1" runat="server" AutoCompleteType="None" AutoComplete="Off"
                                CssClass=" form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblStatus" CssClass="bold " runat="server" Text="Status"></asp:Label>
                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control  input-sm">                              
                                <asp:ListItem Text="ALL" Value="-32768"></asp:ListItem>
                                <asp:ListItem Text="Approved" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                 <asp:ListItem Text="Created" Value="0"></asp:ListItem>
                                <asp:ListItem Text="InActive" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Deleted" Value="-10"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">                        
                            <div class="col-lg-2">
                                <asp:Button runat="server" Text="Search" CssClass="btn btn-sm btn-block btn-primary" ID="Search" OnClick="Search_Click">                                    
                                </asp:Button>
                            </div>
                            <div class="col-lg-2">
                                <asp:Button runat="server" ID="lnkNewVendor" Text="New Vendor" CssClass="btn  btn-sm btn-block btn-primary" OnClick="lnkNewVendor_Click"> 
                                </asp:Button>
                            </div>                            
                        </div>                     
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-body">
                <asp:GridView ID="grdVendorSummary" runat="server"  AutoGenerateColumns="False" Width="100%" 
                    OnPreRender="grdVendorSummary_PreRender" GridLines="Horizontal" OnRowDeleting="grdVendorSummary_RowDeleting"
                    CssClass="table table-striped table-hover  text-nowrap  dt-responsive nowrap" OnRowCommand="grdVendorSummary_RowCommand">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblSelect" Text="" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnApprove" runat="server" CssClass="fa fa-thumbs-o-up fa-2x " CommandName="APPROVE" CommandArgument="<%# Container.DataItemIndex %>" ToolTip="Approve" ></asp:LinkButton>
                                 <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete " CommandName="DELETE" Text="" ToolTip="Delete" ></asp:LinkButton>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblVendorCode" Text="Vendor Code" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink TabIndex="1" CausesValidation="false" ID="lbtnVendorCode" runat="server"
                                    Text='<%# Eval("Code")%>' ToolTip="History" NavigateUrl='<% #"VendorDetails.aspx?VendorID=" + Eval("ID")%>' Target="_blank"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrVendorName" Text="Name" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblVendorName" runat="server" Text='<%# Eval("Name") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrPan" Text="Area" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblPanNo" runat="server" Text='<%#Eval("DefaultAddress") %>' CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrCity" Text="City" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCityText" runat="server" TabIndex="2" Text='<%#Eval("DefaultCity") %>' AutoComplete="Off" CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrApproved" Text="Active" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblApproved" runat="server" Text='<%#Eval("StatusName").ToString() %>' CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdVendorSummary.ClientID%>');
        $(document).ready(function () {
            GridUI(GridID, 50);
        })    
    </script>
</asp:Content>
