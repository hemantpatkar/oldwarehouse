﻿#region Author
/*
Name    :   Pradip U Bobhate
Date    :   27/07/2016
Use     :   This form is used to save department type in database.
*/
#endregion

#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
#endregion
public partial class DepartmentType : BigSunPage
{
    AppDepartmentTypeColl objDepartmentTypeColl;
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        objDepartmentTypeColl = (AppDepartmentTypeColl)Session[TableConstants.DepartmentData];
        if (!IsPostBack)
        {            
            BindData();           
        }
        HidePanel(2);        
    }
    #endregion
    
    #region BindData
    public void BindData()
    {
        AppDepartmentTypeColl objDepartmentColl = new AppDepartmentTypeColl(intRequestingUserID);
        objDepartmentColl.Search();
        grdDepartments.DataSource = objDepartmentColl;
        grdDepartments.DataBind();
        Session.Add(TableConstants.DepartmentData, objDepartmentColl);
    }
    #endregion  
        
    #region SetDataForEdit
    public void SetDataForEdit(int Index)
    {
        AppDepartmentTypeColl obj = (AppDepartmentTypeColl)Session[TableConstants.DepartmentData];
        AppObjects.AppDepartmentType objEdit = null;
        if (Index >= 0)
        {
            objEdit = obj[Index];
        }
        txtDeptCode.Text = objEdit.Code;
        txtDeptName.Text = objEdit.Name;
        chkActive.Checked = new AppUtility.AppConvert(objEdit.Status);
        hdfID.Value = new AppConvert(Index);
    }
    #endregion  
    
    #region HidePanel
    public void HidePanel(int Condi=0)
    {
        //bool blnStatus = (grdDepartments.Rows.Count > 0) && (Condi > 0);
        //pnlDeptEntry.Visible = blnStatus;
        //pnlSearch.Visible = !blnStatus;
        //btnSearch.Visible = false;
        //btnSaveDept.Visible = true;
        //btnResetCancel.Visible = true;
        //btnCancel.Visible = true;


        if (grdDepartments.Rows.Count <= 0)
        {
            pnlDeptEntry.Visible = true;
            pnlSearch.Visible = false;
            btnSearch.Visible = false;
            btnSaveDept.Visible = true;
            btnResetCancel.Visible = true;
            btnCancel.Visible = true;
        }
        else
        {
            if (Condi == 1)
            {
                pnlDeptEntry.Visible = true;
                pnlSearch.Visible = false;
                btnSearch.Visible = false;
                btnSaveDept.Visible = true;
                btnResetCancel.Visible = true;
                btnCancel.Visible = true;
            }
            else if (Condi == 2)
            {
                pnlDeptEntry.Visible = false;
                pnlSearch.Visible = true;
                btnSearch.Visible = true;
                btnSaveDept.Visible = false;
                btnResetCancel.Visible = false;
                btnCancel.Visible = false;
            }
        }        
    }
    #endregion

    #region GridView Events  
    protected void grdDepartments_PreRender(object sender, EventArgs e)
    {
        if (grdDepartments.Rows.Count > 0)
        {
            grdDepartments.UseAccessibleHeader = true;
            grdDepartments.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }   
    protected void grdDepartments_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        objDepartmentTypeColl[e.RowIndex].Status = -2;
        objDepartmentTypeColl[e.RowIndex].Save();
    }  
    protected void grdDepartments_RowEditing(object sender, GridViewEditEventArgs e)
    {
        SetDataForEdit(e.NewEditIndex);
        HidePanel(1);

    }
    #endregion

    #region Button Click Events
    protected void btnResetCancel_Click(object sender, EventArgs e)
    {
        HidePanel(2);
    }
    protected void lbtnAddNew_Click(object sender, EventArgs e)
    {
        HidePanel(1);
        //ClearFields(1);
    }
    public void ClearFields(int param)
    {
        if (param == 1)
        {
            txtDeptCode.Text = "";
            txtDeptName.Text = "";
            chkActive.Checked = true;
            hdfID.Value = "";
            pnlDeptEntry.Visible = false;
            pnlSearch.Visible = true;
        }
        else if (param == 2)
        {
            txtCode.Text = "";
            txtName.Text = "";
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        AppDepartmentTypeColl newObj = new AppDepartmentTypeColl(intRequestingUserID);
        newObj.Search();
        grdDepartments.DataSource = newObj;
        grdDepartments.DataBind();

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ClearFields(2);
    }
    protected void btnSaveDept_Click(object sender, EventArgs e)
    {
        objDepartmentTypeColl = (AppDepartmentTypeColl)Session[TableConstants.DepartmentData];
        AppDepartmentType objDepartmentType = new AppDepartmentType(intRequestingUserID);
        string strCommandID = hdfID.Value;
        int intCommandID = new AppConvert(strCommandID);
        if (String.IsNullOrEmpty(strCommandID) == false)
        {
            objDepartmentType = objDepartmentTypeColl[intCommandID];

        }
        objDepartmentType.Code = txtDeptCode.Text;
        objDepartmentType.Name = txtDeptName.Text;
        objDepartmentType.Status = new AppConvert(chkActive.Checked);
        objDepartmentType.Save();
        ClearFields(1);
        BindData();
        HidePanel(2);
    }
    #endregion
}