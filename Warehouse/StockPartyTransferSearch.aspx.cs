﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using System.IO;
using System.Drawing;

public partial class StockPartyTransferSearch : BigSunPage
{
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Party Transfer Search";
        if (!IsPostBack)
        {
        }
    }
    #endregion

    #region Button Event
    protected void btnNew_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockPartyTransfer.aspx");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Search();
    }

    protected void Search()
    {

        AppTransferStockCustomerColl objAppTransferStockCustomerColl = new AppTransferStockCustomerColl(intRequestingUserID);

        DateTime fromdate;
        if (txtTransferDate.Text.Trim() != "" && txtTransferDate.Text.Trim() != "__/__/____")
        {
            fromdate = Convert.ToDateTime(txtTransferDate.Text);
            objAppTransferStockCustomerColl.AddCriteria(TransferStockCustomer.TransferDate, Operators.GreaterOrEqualTo, fromdate);
        }
        if (txtCustomer.Text.Trim() != "")
        {
            objAppTransferStockCustomerColl.AddCriteria(TransferStockCustomer.Company_ID, Operators.StartWith, txtCustomer.Text);
        }
        if (txtTransferNo.Text.Trim() != "")
        {
            objAppTransferStockCustomerColl.AddCriteria(TransferStockCustomer.TransferNo, Operators.Equals, txtTransferNo.Text);
        }
        if (ddlStatus.SelectedValue != "-1")
        {
            int status = Convert.ToInt32(ddlStatus.SelectedValue);
            objAppTransferStockCustomerColl.AddCriteria(TransferStockCustomer.Status, Operators.Equals, status); //
        }

        objAppTransferStockCustomerColl.Search();
        //objAppGateEntryColl.Sort(AppPreInward.ComparisionDesc);
        grdStockPartyTransferSearch.DataSource = objAppTransferStockCustomerColl;
        grdStockPartyTransferSearch.DataBind();
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (grdStockPartyTransferSearch.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Doesnot contain data" + "','2'" + ");", true);
            return;
        }

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            grdStockPartyTransferSearch.AllowPaging = false;
            grdStockPartyTransferSearch.HeaderRow.BackColor = Color.White;
            foreach (TableCell cell in grdStockPartyTransferSearch.HeaderRow.Cells)
            {
                cell.BackColor = grdStockPartyTransferSearch.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in grdStockPartyTransferSearch.Rows)
            {
                row.BackColor = Color.White;
                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = grdStockPartyTransferSearch.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = grdStockPartyTransferSearch.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            grdStockPartyTransferSearch.RenderControl(hw);
            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    #endregion

    #region Grid
    protected void grdStockPartyTransferSearch_PreRender(object sender, EventArgs e)
    {

    }
    #endregion

    protected void grdStockPartyTransferSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdStockPartyTransferSearch.PageIndex = e.NewPageIndex;
        Search();
    }
}