﻿using AppObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppUtility;

public partial class GateEntryDetail : BigSunPage
{

    #region Page Load

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            dateval();
            DecideLanguage();
            txtCustomer.Focus();
            if (Request.QueryString["ID"] != "" && Request.QueryString["ID"] != null)
            {
                int ID = Convert.ToInt32("0" + Request.QueryString["ID"]);
                FillGateEntryDetail(ID);
            }
        }
    }

    #endregion

    #region vehicle Type

 

    private void DecideLanguage()
    {
        // Method used to fill Screen Language on labels & Buttons text property

        Page.Title = "Gate" + " " + "Entry";
        lblPartyName.Text = "Customer";
        lblGateRegisterDetail.Text = "Gate" + " " + "Entry";
        btnSave.Text = "Save";
        btnNewDet.Text = "Create New";
        lblVehicleID.Text = "Vehicle" + " " + "Type";
        lblVehicleNo.Text = "Vehicle" + " " + "No";
        lblDriverName.Text = "Driver" + " " + "Name";
        lblMobileNo.Text = "Mobile" + " " + "No";
        lblLicenseNo.Text = "License" + " " + "No";
        lblEntryOn.Text = "In";
        lblExitOn.Text = "Out";
        lblEntryHH.Text = "HH";
        lblExitHH.Text = "HH";
        lblEntryMM.Text = "MM";
        lblExitMM.Text = "MM";
        lblnounit.Text = "In Unit";
        lbloutunit.Text = "Out Unit";
        lblTransporterName.Text = "Transporter" + " " + "Name";
        lblChallanNopop.Text = "Challan" + " " + "No.";

        #region Vehicle Type
        btnSaveVehicleType.Text = "Save";
        btnBackVehicleType.Text = "Back";
        btnNewVehicleType.Text = "New";
        lblTypeName.Text = "Vehicle" + " " + "Type" + " " + "Name";
        lblVehicleTypeCode.Text = "Vehicle" + " " + "Type" + " " + "Code";
        lblVehicleTypeName.Text = "Vehicle" + " " + "Type";
        #endregion

    }

    public void dateval()
    {
        Rangeval.MinimumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangeval.MaximumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangback.MinimumValue = DateTime.Now.ToString("dd/MM/yyyy");
        Rangback.MaximumValue = DateTime.Now.ToString("dd/MM/yyyy");
        cmptodate.ValueToCompare = DateTime.Now.ToString("dd/MM/yyyy");
        txtEntryDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtEntryHour.Text = new AppConvert(DateTime.Now.Hour);
        txtEntryMinute.Text = new AppConvert(DateTime.Now.Minute);
        txtunit.Text = new AppConvert(0);
        txtoutunit.Text = new AppConvert(0);
    }

    public void clearVehicalType()  // method use to clear data while opening new detail page
    {
        txtVehicleTypeCode.Text = "";
        txtVehicleTypeName.Text = "";
        txtCapacity.Text = "";
        txtStandardTAT.Text = "";
        txtVehicleTypeCode.Focus();
    }

    protected void btnSaveVehicleType_Click(object sender, EventArgs e)
    {
        AppVechicleType VT = new AppVechicleType(intRequestingUserID);
        AppVechicleTypeColl objVehicleTypeColl = new AppVechicleTypeColl(intRequestingUserID);
        objVehicleTypeColl.AddCriteria(AppObjects.VechicleType.Code, Operators.Equals, txtVehicleTypeCode.Text.Trim());
        objVehicleTypeColl.Search();

        #region Validation

        if (objVehicleTypeColl.Count > 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Entry already exists..!" + "','2'" + ");", true);
            return;
        }

        #endregion

        VT.ModifiedBy = intRequestingUserID;
        VT.ModifiedOn = DateTime.Now;
        VT.Capacity = txtCapacity.Text;
        VT.StandardTAT = new AppConvert(txtStandardTAT.Text);
        VT.Code = txtVehicleTypeCode.Text;
        VT.Name = txtVehicleTypeName.Text;
        VT.Save();

        txtVehical.Text = txtVehicleTypeName.Text;
        hdfVehicalName.Value = txtVehical.Text;
        hdfVehicalID.Value = new AppConvert(VT.ID);
     

        div_Vehicletype.Attributes.Add("style", "display:none");
        clearVehicalType();
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Saved..!" + "','1'" + ");", true);
        txtVehical.Focus();
    }

    protected void lnkAddVehicalType_Click(object sender, EventArgs e)
    {
        div_Vehicletype.Attributes.Add("style", "display:");
        txtVehicleTypeCode.Focus();
    }

    protected void btnBackVehicleType_Click(object sender, EventArgs e)
    {
        txtVehicleNo.Focus();
        div_Vehicletype.Attributes.Add("style", "display:none");
        txtVehical.Focus();
    }

    protected void btnNewVehicleType_Click(object sender, EventArgs e)
    {
        clearVehicalType();
        div_Vehicletype.Attributes.Add("style", "display:");

    }

    protected void btnNewDet_Click(object sender, EventArgs e)
    {
        Response.Redirect("GateEntryDetail.aspx");
    }

    #endregion

    #region GateEntry
    public void FillGateEntryDetail(int ID)
    {
        if (ID <= 0) return;

        lnkAddVehicalType.Visible = false;
        AppGateEntry gr = new AppGateEntry(intRequestingUserID, ID);
        txtGateRegNo.Text = gr.GateNo;
        txtCustomer.Text = new AppConvert(gr.PartyName);
        hdfVehicalID.Value = new AppConvert(gr.VechicleType_ID);
        hdfStatus.Value = new AppConvert(gr.Status);
        txtVehical.Text = gr.VechicleType.Name;
        txtVehical.ToolTip = txtVehical.Text;

        txtDriverName.Text = new AppConvert(gr.DriverName);
        txtDriverName.ToolTip = txtDriverName.Text;
        txtMobileNo.Text = new AppConvert(gr.MobileNo);
        txtMobileNo.ToolTip = txtMobileNo.Text;
        txtLicenseNo.Text = new AppConvert(gr.LicenseNo);
        txtLicenseNo.ToolTip = txtLicenseNo.Text;
        txtTransporterName.Text = new AppConvert(gr.TransporterName);
        txtTransporterName.ToolTip = txtTransporterName.Text;
        txtVehicleNo.Text = new AppConvert(gr.VehicalNo);
        txtVehicleNo.ToolTip = txtVehicleNo.Text;

        txtunit.Text = new AppConvert(gr.InUnit);
        txtoutunit.Text = new AppConvert(gr.OutUnit);
        txtChallanNopop.Text = gr.ChallanNo;
        hdfGRID.Value = new AppConvert(gr.ID);

        if (new AppConvert(gr.EntryOn) != "" && gr.EntryOn != null)
        {
            DateTime EntryOn = Convert.ToDateTime(gr.EntryOn);
            txtEntryDate.Text = new AppConvert(EntryOn.ToString("dd/MM/yyyy"));
            txtEntryDate.ToolTip = new AppConvert(EntryOn.ToString("dd/MM/yyyy"));
            txtEntryHour.Text = new AppConvert(EntryOn.Hour);
            txtEntryHour.ToolTip = new AppConvert(EntryOn.Hour);
            txtEntryMinute.Text = new AppConvert(EntryOn.Minute);
            txtEntryMinute.ToolTip = new AppConvert(EntryOn.Minute);
        }
        if (new AppConvert(gr.ExitOn) != "" && gr.ExitOn != null)
        {
            DateTime ExitOn = Convert.ToDateTime(gr.ExitOn);
            txtExitDate.Text = new AppConvert(ExitOn.ToString("dd/MM/yyyy"));
            txtExitDate.ToolTip = new AppConvert(ExitOn.ToString("dd/MM/yyyy"));
            txtExitHour.Text = new AppConvert(ExitOn.Hour);
            txtExitHour.ToolTip = new AppConvert(ExitOn.Hour);
            txtExitMinutes.Text = new AppConvert(ExitOn.Minute);
            txtExitMinutes.ToolTip = new AppConvert(ExitOn.Minute);
        }
        
    }

    public void BydefaultclickEnter()
    {
        txtCustomer.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtGateRegNo.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtVehical.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtDriverName.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtMobileNo.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtEntryDate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtEntryHour.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtEntryMinute.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtExitDate.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtExitHour.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtExitMinutes.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtLicenseNo.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtLicenseNo.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtunit.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
        txtoutunit.Attributes.Add("onkeypress", "return clickButton(event,'" + btnSave.ClientID + "')");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        #region vehicle validation

        if (string.IsNullOrEmpty(hdfVehicalID.Value))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Please Select Vehicle No..!" + "','2'" + ");", true);
            return;
        }

        #endregion

        #region Out Time Validation

        if (txtExitDate.Text.Trim() != "" && txtExitDate.Text != "__/__/____")
        {
            int etdate = 0;
            int exdate = new AppConvert(txtExitDate.Text);
            int EHH = 0;
            int XHH = 0;
            int EMM = 0;
            int XMM = 0;

            if (txtEntryDate.Text.Trim() != "" && txtEntryDate.Text != "__/__/____")
            {
                etdate = new AppConvert(txtEntryDate.Text);
            }

            if (exdate == etdate)
            {
                if (txtEntryHour.Text.Trim() != "" && txtEntryHour.Text != "__")
                {
                    EHH = Convert.ToInt32(txtEntryHour.Text);
                }
                if (txtExitHour.Text.Trim() != "" && txtExitHour.Text != "__")
                {
                    XHH = Convert.ToInt32(txtExitHour.Text);
                }
                if (XHH < EHH)
                {

                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid Out Hour" + "','1'" + ");", true);


                    return;
                }

                if (txtEntryMinute.Text.Trim() != "" && txtEntryMinute.Text != "__")
                {
                    EMM = Convert.ToInt32(txtEntryMinute.Text);
                }
                if (txtExitMinutes.Text.Trim() != "" && txtExitMinutes.Text != "__")
                {
                    XMM = Convert.ToInt32(txtExitMinutes.Text);
                }

                if (XHH == EHH)
                {
                    if (XMM < EMM)
                    {

                        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Invalid Out Minutes" + "','1'" + ");", true);

                        return;
                    }
                }
            }
        }

        #endregion


        {//Transaction begin here
            try
            {
                Int32 ID = Convert.ToInt32((hdfGRID.Value == "" ? "0" : hdfGRID.Value));

                AppGateEntry greg = new AppGateEntry(intRequestingUserID);
                DateTime EntryOn;

                #region Duplicate Vehicle Validation

                if (hdfGRID.Value == "")
                {
                    int vid = Convert.ToInt32("0" + hdfVehicalID.Value);
                    if (vid > 0)
                    {
                        AppGateEntryColl objAppGateEntryColl = new AppGateEntryColl(intRequestingUserID);
                        objAppGateEntryColl.AddCriteria(GateEntry.Status, Operators.Equals, 1);
                        objAppGateEntryColl.AddCriteria(GateEntry.VechicleType_ID, Operators.Equals, vid);
                        objAppGateEntryColl.AddCriteria(GateEntry.VehicalNo, Operators.Equals, txtVehicleNo.Text);
                        objAppGateEntryColl.Search();

                        if (objAppGateEntryColl.Count > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('This vehical is already in. \n  First make an out entry','1'" + ");", true);

                            return;
                        }
                    }
                }

                #endregion

                if (txtEntryDate.Text.Trim() != "" && txtEntryDate.Text != "__/__/____")
                {
                    DateTime date = DateTime.Now;
                    string strEntryOn = txtEntryDate.Text + " " + txtEntryHour.Text + ":" + txtEntryMinute.Text;
                    EntryOn = Convert.ToDateTime(strEntryOn);
                }
                else
                    EntryOn = DateTime.Now;

                if (hdfGRID.Value != "")
                {
                    greg.ID = ID;
                    greg.GateNo = new AppConvert(txtGateRegNo.Text);
                }
                else
                {
                    greg.GateNo = "GE" + new AppConvert(AppUtility.AutoID.GetNextNumber(1, "GE", System.DateTime.Now, 4, ResetCycle.Yearly));
                }
                greg.Company_ID = LoginCompanyID;
                greg.ModifiedOn = DateTime.Now;
                greg.ModifiedBy = intRequestingUserID;
                greg.VechicleType_ID = Convert.ToInt32(hdfVehicalID.Value);
                greg.DriverName = new AppConvert(txtDriverName.Text);
                greg.MobileNo = new AppConvert(txtMobileNo.Text);
                greg.LicenseNo = new AppConvert(txtLicenseNo.Text);
                greg.EntryOn = EntryOn;
                greg.TransporterName = txtTransporterName.Text;
                greg.VehicalNo = txtVehicleNo.Text;
                greg.PartyName = txtCustomer.Text;
                greg.InUnit = new AppConvert(txtunit.Text);
                greg.OutUnit = new AppConvert(txtoutunit.Text);
                greg.ChallanNo = txtChallanNopop.Text;

                if (txtExitDate.Text.Trim() != "" && txtExitDate.Text != "__/__/____")
                {
                    string strExitOn = new AppConvert(txtExitDate.Text + " " + txtExitHour.Text + ":" + txtExitMinutes.Text);
                    greg.ExitOn = Convert.ToDateTime(strExitOn);
                    greg.Status = 4;

                }
                else
                {
                    greg.Status = 1;
                }

                greg.Save();

                FillGateEntryDetail(Convert.ToInt32(greg.ID));

                hdfGRID.Value = new AppConvert(greg.ID); //assigning new created id to hidden field
                if (hdfGRID.Value != "")
                {
                    btnPrint.NavigateUrl = "../../Reports/ReportView.aspx?GateRegID=" + hdfGRID.Value + "&RPName=GatePass";
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Saved" + "','1'" + ");", true);
            }
            catch (Exception)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + "Transaction Failed" + "','2'" + ");", true);
                return;
            }

        }
    }

    protected void txtEntryDate_TextChanged(object sender, EventArgs e)
    {
        if (txtEntryDate.Text.Trim() != "" && txtEntryDate.Text != "__/__/____")
        {
            rfvEntryHour.Enabled = true;
            meEntryHour.Enabled = true;
            mevEntryHour.Enabled = true;

            rfvEntryMinute.Enabled = true;
            meEntryMinute.Enabled = true;
            mevEntryMinute.Enabled = true;

            txtEntryHour.Focus();
        }
        else
        {
            rfvEntryHour.Enabled = false;
            meEntryHour.Enabled = false;
            mevEntryHour.Enabled = false;

            rfvEntryMinute.Enabled = false;
            meEntryMinute.Enabled = false;
            mevEntryMinute.Enabled = false;

            txtEntryDate.Focus();
        }


        txtCustomer.Focus();
    }

    protected void txtExitDate_TextChanged(object sender, EventArgs e)
    {
        if (txtExitDate.Text.Trim() != "" && txtExitDate.Text != "__/__/____")
        {
            rfvExitHour.Enabled = true;
            meExitHour.Enabled = true;
            MEVExitHour.Enabled = true;

            cmpDate.Enabled = true;
            cmptodate.Enabled = true;

            rfvExitMinutes.Enabled = true;
            MEExitMinues.Enabled = true;
            MEVExitMinutes.Enabled = true;

            reqoutunit.Enabled = true;
            int etdate = 0;
            int exdate = new AppConvert(txtExitDate.Text);
            if (txtEntryDate.Text.Trim() != "" && txtEntryDate.Text != "__/__/____")
            {
                etdate = new AppConvert(txtEntryDate.Text);
            }
            txtExitHour.Focus();
        }
        else
        {
            rfvExitHour.Enabled = false;
            meExitHour.Enabled = false;
            MEVExitHour.Enabled = false;

            rfvExitMinutes.Enabled = false;
            MEExitMinues.Enabled = false;
            MEVExitMinutes.Enabled = false;

            reqoutunit.Enabled = false;
            cmpDate.Enabled = false;
            cmptodate.Enabled = false;
            txtExitDate.Focus();
        }


    }

    protected void lnkSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("GateEntrySearch.aspx");
    }
    #endregion

}