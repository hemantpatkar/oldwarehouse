﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to display all Company lists.
*/
#endregion
#region Using
using System;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.UI;
using AppUtility;
#endregion
public partial class T_ContraSummary : BigSunPage
{
    #region PageLoad
    AppContraColl objDN;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtVoucherDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            BindData();
        }
    }
    #endregion
    #region Function   
    protected void BindData()
    {
        AppObjects.AppContraColl objColl = new AppContraColl(intRequestingUserID);
        if (txtContraNo.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Contra.ContraNo, AppUtility.Operators.Equals, txtContraNo.Text, 0);
        }
        if (txtContraAmount.Text != "")
        {
            switch (ddlOperators.SelectedValue)
            {
                case "1":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Contra.ExchangeRate, AppUtility.Operators.Equals, txtContraAmount.Text, 0);
                    break;
                case "2":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Contra.ExchangeRate, AppUtility.Operators.GreaterOrEqualTo, txtContraAmount.Text, 0);
                    break;
                case "3":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Contra.ExchangeRate, AppUtility.Operators.LessOrEqualTo, txtContraAmount.Text, 0);
                    break;
                case "4":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Contra.ExchangeRate, AppUtility.Operators.GreaterThan, txtContraAmount.Text, 0);
                    break;
                case "5":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Contra.ExchangeRate, AppUtility.Operators.LessThan, txtContraAmount.Text, 0);
                    break;
            }
        }
        if (!string.IsNullOrEmpty(txtVoucherFromDate.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Contra.ContraDate, AppUtility.Operators.Equals,new AppUtility.AppConvert(txtVoucherFromDate.Text), 0);
        }
        if (!string.IsNullOrEmpty(txtVoucherToDate.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.Contra.ContraDate, AppUtility.Operators.Equals, new AppUtility.AppConvert(txtVoucherToDate.Text), 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
            objColl.AddCriteria(AppObjects.Contra.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.ContraSummary] = objColl;
        grdContraSummary.DataSource = objColl;
        grdContraSummary.DataBind();
    }
    public void CancelContra(AppContra ObjContra)
    {
        AppContra ObjDuplicateContra = ObjContra;
        ObjDuplicateContra.ID = 0;
        ObjDuplicateContra.VoucherType_ID = 36; //Debit Voucher Cancel
        ObjDuplicateContra.Amount = ObjDuplicateContra.Amount;
        ObjDuplicateContra.ModifiedBy = intRequestingUserID;
        ObjDuplicateContra.ModifiedOn = System.DateTime.Now;
        ObjDuplicateContra.Status = new AppConvert((int)RecordStatus.Cancelled);
        ObjDuplicateContra.Save();

        #region Add Ledger Entry
        LedgerEntry(LoginCompanyID, 101201, 1012, ObjDuplicateContra.To_Ledger_ID, ObjDuplicateContra.ID, ObjDuplicateContra.ContraDate, ObjDuplicateContra.Amount, 0, ObjDuplicateContra.ContraNo, ObjDuplicateContra.ModifiedBy, ObjDuplicateContra.ModifiedOn, ObjDuplicateContra.Status, ObjDuplicateContra.Status, ObjDuplicateContra.Type,
                        ObjDuplicateContra.ProfitCenter_ID, ObjDuplicateContra.CostCenter_ID, ObjDuplicateContra.VoucherType_ID, ObjDuplicateContra.Description);

        LedgerEntry(LoginCompanyID, 101202, 1012, ObjDuplicateContra.From_Ledger_ID, ObjDuplicateContra.ID, ObjDuplicateContra.ContraDate, 0, ObjDuplicateContra.Amount, ObjDuplicateContra.ContraNo, ObjDuplicateContra.ModifiedBy, ObjDuplicateContra.ModifiedOn, ObjDuplicateContra.Status, ObjDuplicateContra.Status, ObjDuplicateContra.Type,
                                 ObjDuplicateContra.ProfitCenter_ID, ObjDuplicateContra.CostCenter_ID, ObjDuplicateContra.VoucherType_ID, ObjDuplicateContra.Description);
        #endregion
    }
    #endregion
    #region GridView Events
    protected void grdContraSummary_PreRender(object sender, EventArgs e)
    {
        if (grdContraSummary.Rows.Count > 0)
        {
            grdContraSummary.UseAccessibleHeader = true;
            grdContraSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region Button Click Events
    protected void btnNewContra_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.ObjCompanySession);
        Response.Redirect("T_Contra.aspx");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (Update(grdContraSummary, new AppConvert((int)RecordStatus.Deleted), 1012, 1012) == 1)
        {
            BindData();
        }
    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        if (Update(grdContraSummary, new AppConvert((int)RecordStatus.Approve), 1012, 1012) == 1)
        {
            BindData();
        }
    }
    protected void btnCanceled_Click(object sender, EventArgs e)
    {
        AppObjects.AppContraColl objColl = new AppContraColl(intRequestingUserID);
        if (txtCancelDNNo.Text != "" && txtVoucherDate.Text != "")
        {
            objColl.AddCriteria(AppObjects.Contra.ContraNo, AppUtility.Operators.Equals, txtCancelDNNo.Text);
            objColl.Search(RecordStatus.ALL);

            if (objColl.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Record not found','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                return;
            }
            else
            {
                AppContra objSingleDN = objColl[0];
                if (objColl[0].Status == new AppUtility.AppConvert((int)RecordStatus.Cancelled))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelDNNo.Text + " Transaction alerady cancelled','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelDNNo.Focus();
                    return;
                }
                else if (objColl[0].Status == new AppUtility.AppConvert((int)RecordStatus.Deleted))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelDNNo.Text + " Transaction is deleted','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelDNNo.Focus();
                    return;
                }
                else if (objColl[0].Status == new AppUtility.AppConvert((int)RecordStatus.Created))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelDNNo.Text + " Transaction is not approved','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelDNNo.Focus();
                    return;
                }
                else if (objColl[0].ContraDate > new AppUtility.AppConvert(Convert.ToDateTime(txtVoucherDate.Text)))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelDNNo.Text + " Cancel voucher date should be greater than or equal to voucher date.','2'" + ");", true);
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                    txtCancelDNNo.Focus();
                    return;
                }
                objSingleDN.Status = new AppUtility.AppConvert((int)RecordStatus.Cancelled);
                objSingleDN.Save();

                UpdateLedgerEntryStatus(101201, 1012, objSingleDN.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                UpdateLedgerEntryStatus(101202, 1012, objSingleDN.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));


                CancelContra(objSingleDN);
            }
            Session[TableConstants.ContraSummary] = objColl;
            grdContraSummary.DataSource = objColl;
            grdContraSummary.DataBind();
            txtCancelDNNo.Text = "";
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Debit voucher cancelled successfully','1'" + ");", true);


        }
        else
        {
            if (txtCancelDNNo.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Debit voucher no.','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                txtCancelDNNo.Focus();
                return;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter voucher date.','2'" + ");", true);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
                txtVoucherDate.Focus();
                return;
            }
        }
    }
    #endregion
}