﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Culture="en-gb" Inherits="StockLocTransfer" Codebehind="StockLocTransfer.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function checkedAllSearch() {
            var but = document.getElementById("SelectAll");
            var TargetBaseControl = document.getElementById('<%= this.gvSearch.ClientID %>');
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var icount = 0; icount < Inputs.length; ++icount) {
                if (Inputs[icount].type == 'checkbox') {
                    if (Inputs[icount].disabled == false) {
                        if (but.checked == true) {
                            Inputs[icount].checked = true;
                        }
                        else {

                            Inputs[icount].checked = false;
                        }
                    }
                    else {
                        Inputs[icount].checked = false;
                    }
                }
            }
        }



        function PartnerGroupSearch(sender, eventArgs) {

            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());
            var hdpartID = $get('<%= UsrhdfPartnerGroup.ClientID %>');
            var lblPartname = $get('<%= UsrlblPartnerGroup.ClientID %>');
            var partycode = $get('<%= UsrtxtPartnerGroupCode.ClientID %>');


            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=UsrtxtPartnerCode_AutoCompleteExtender.ClientID %>').set_contextKey(eventArgs.get_value());
            }
        }


        function ClearPartnerGroupSearch() {
            var OrignalProfitCenter = $get('<%=UsrtxtPartnerGroupCode.ClientID %>');
            var HidenProfitCenter = $get('<%= UsrlblPartnerGroup.ClientID %>');
            if ((OrignalProfitCenter.value != HidenProfitCenter.innerText) || OrignalProfitCenter.value == "No Records Found") {
                $get('<%=UsrhdfPartnerGroup.ClientID %>').value = "";
                $get('<%=UsrlblPartnerGroup.ClientID %>').value = "";
                $get('<%=UsrtxtPartnerGroupCode.ClientID %>').value = "";
            }
        }

        function PartnerSearch(sender, eventArgs) {
            //alert(" Key : " + eventArgs.get_text() + "  Value :  " + eventArgs.get_value());
            var hdpartID = $get('<%= hdfCustomerID.ClientID %>');
            var lblPartname = $get('<%= hdfCustomerName.ClientID %>');
            var partycode = $get('<%= UsrtxtPartnerCode.ClientID %>');
            partycode.value = eventArgs.get_text();
            hdpartID.value = eventArgs.get_value();
            lblPartname.innerHTML = eventArgs.get_text();
            lblPartname.defaultvalue = eventArgs.get_text();
            if (hdpartID.value != "") {
                $find('<%=UsrtxtPartnerGroupCode_AutoCompleteExtender.ClientID %>').set_contextKey(eventArgs.get_value());
            }

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
        <ProgressTemplate>
            <div class="modal fade in" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-keyboard="false" style="display: block">
                <div class="modal-dialog  modal-sm">
                    <h1 style="text-align: center;">
                        <i class="fa fa-truck fa-magic fa-2x faa-passing animated"></i>
                    </h1>
                </div>
            </div>
            <div class="modal-backdrop fade in"></div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <%--Header Section--%>
            <div class="row">
                <div class="col-lg-12">
                    <div class="heading">
                        <i class="fa fa-location-arrow"></i>
                        <asp:Label ID="lblLocTransferTitle" runat="server"></asp:Label>
                        &nbsp;/&nbsp;
                   <asp:Label ID="txtMONo" ForeColor="Maroon" runat="server"></asp:Label>
                        <div class="pull-right">
                            <asp:LinkButton ID="btnBack" runat="server" CausesValidation="False" OnClick="btnBack_Click"
                                TabIndex="12" Text="">
                                  <i class="fa fa-search"></i>
                            </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row btnrow">
                <div class="col-lg-6">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <asp:LinkButton ID="btnSave" runat="server" OnClick="btnSave_Click" TabIndex="11" Text=""
                                ValidationGroup="MaterialOrder" AccessKey="w"></asp:LinkButton><span></span>

                        </li>
                        <li>
                            <asp:LinkButton ID="btnNewDetail" runat="server" Text="" CausesValidation="false" TabIndex="13"
                                OnClick="btnNewDetail_Click" AccessKey="n" />
                        </li>

                    </ul>
                </div>
                <div class="col-lg-6">
                    <div class="pull-right">
                        <ul class="nav nav-pills nav-wizard">
                            <li class="active"><a href="#" data-toggle="tab">New</a><div class="nav-arrow"></div>
                            </li>

                            <li>
                                <div class="nav-wedge"></div>
                                <a href="#" data-toggle="tab">Done</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <hr />


            <%--Description--%>

            <div class="row">
                <div class="col-lg-12" style="overflow: auto; max-height: 300px;">
                    <asp:GridView ID="gvDescription" ShowHeaderWhenEmpty="true" ShowHeader="true" runat="server" AutoGenerateColumns="False"
                        CssClass="table table-striped table-hover text-nowrap">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:Label ID="lblSrno" runat="server" Text="<%# Convert.ToInt32(Container.DataItemIndex)+1%>" Visible="true"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblParty" Text="Customer" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblparty" runat="server" ForeColor="#3399ff" Text='<%#Eval("InwardDetail.Inward.Customer_Company.Name")  %>' ToolTip='<%#Eval("InwardDetail.Inward.Customer_Company.Name") %>'></asp:Label>
                                    <br />
                                    <asp:Label ID="Label1" runat="server" ForeColor="Maroon" Text='<%#Eval("InwardDetail.Inward.Customer_Company.Parent.Name")  %>' ToolTip='<%#Eval("InwardDetail.Inward.Customer_Company.Parent.Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblLotNo" Text="LotNo" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblLotNo" runat="server" Text='<%#Eval("LotInternal") %>'
                                        ToolTip='<%#Eval("LotInternal")%>'></asp:Label>
                                    <asp:Label ID="lblLotVersion" runat="server" Text='<%#Eval("LotVersion") %>' ToolTip='<%#Eval("LotVersion")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblScheme" Text="Scheme/Location" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblScheme" runat="server" ForeColor="#3399ff" Text='<%#Eval("InwardDetail.StorageSchemeRate.StorageScheme.Code")  %>' ToolTip='<%#Eval("InwardDetail.StorageSchemeRate.StorageScheme.Code") %>'></asp:Label>
                                    <br />
                                    <asp:Label ID="lblLocationNo" runat="server" ForeColor="Maroon" Text='<%#Eval("InwardDetail.StorageLocation.Name") %>' ToolTip='<%#Eval("InwardDetail.StorageLocation.Name")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblLocation" Text="Location" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtToStorageLocation" runat="server" onblur="return ClearAutocompleteTextBox(this)" Width="100px" CssClass="form-control input-sm" Text='<%# Eval("To_StorageLocation.Name") %>' TabIndex="0"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="ACEtxtToStorageLocation" runat="server" CompletionInterval="1"
                                        CompletionListCssClass="AutoExtender" CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                        CompletionListItemCssClass="AutoExtenderList" CompletionSetCount="10" ContextKey="0"
                                        DelimiterCharacters="" EnableCaching="false" Enabled="True" FirstRowSelected="true"
                                        MinimumPrefixLength="1" OnClientItemSelected="AutoCompleteSearch" ServiceMethod="StorageLocationSearch"
                                        ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                        TargetControlID="txtToStorageLocation">
                                    </asp:AutoCompleteExtender>
                                    <div style="display: none">
                                        <asp:TextBox ID="hdfToStorageLocationName" Text='<%# Eval("To_StorageLocation.Name") %>' runat="server" />
                                        <asp:HiddenField ID="hdfToStorageLocationID" Value='<%# Eval("To_StorageLocation_ID") %>' runat="server" />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblOrderQty" Text="Move Qty" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox runat="server" ID="txtOrderQuantity" CssClass="form-control input-sm" Text='<%#Eval("StockQuantity") %>'
                                        ValidationGroup="MaterialOrder" CausesValidation="True" MaxLength="7" TabIndex="3" />
                                    <asp:CompareValidator ID="cmpOrderQuantity" runat="server" ControlToValidate="txtOrderQuantity"
                                        ErrorMessage="Invalid" ControlToCompare="txtNetQuan" ValidationGroup="MaterialOrder" CssClass="text-danger"
                                        Type="Double" Operator="LessThanEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                    <asp:CompareValidator ID="cmpLessThan1" runat="server" ValidationGroup="MaterialOrder"
                                        ErrorMessage="" ControlToValidate="txtOrderQuantity" ValueToCompare="0" Type="Double" CssClass="text-danger"
                                        Operator="NotEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                    <asp:CompareValidator ID="cmpgrthan" runat="server" ValidationGroup="MaterialOrder"
                                        ErrorMessage="" ControlToValidate="txtOrderQuantity" ValueToCompare="0" Type="Double" CssClass="text-danger"
                                        Operator="GreaterThanEqual" Display="Dynamic" SetFocusOnError="True"></asp:CompareValidator>
                                    <asp:RequiredFieldValidator ID="rfvOrderQuant" runat="server" Display="Dynamic" ControlToValidate="txtOrderQuantity"
                                        ValidationGroup="MaterialOrder" SetFocusOnError="true" ErrorMessage=""></asp:RequiredFieldValidator>
                                    <div style="height: 0px; width: 0px;">
                                        <asp:TextBox ID="txtNetQuan" runat="server" Text='<%#Eval("StockQuantity") %>' Height="0px"
                                            Width="0px" BorderColor="White" BackColor="White" BorderStyle="None" CausesValidation="True"
                                            BorderWidth="0px" ReadOnly="True"></asp:TextBox>
                                    </div>
                                    <asp:FilteredTextBoxExtender ID="ftbOrderQuantity" runat="server" FilterType="Numbers,Custom" ValidChars="."
                                        TargetControlID="txtOrderQuantity">
                                    </asp:FilteredTextBoxExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblStockQuantity" Text="Net Qty" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblStockQuantity" runat="server" Text='<%#Eval("StockQuantity") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblItemCode" Text="Item Name" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblStorageItemName" runat="server" Text='<%#Eval("Item.Name") %>'
                                        ToolTip='<%#Eval("Item.Name")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPackRef" Text="Packing Ref" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPackingReference" Text='<%# Eval("InwardDetail.PackingReference") %>' ToolTip='<%# Eval("InwardDetail.PackingReference") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblMark" Text="Item Mark" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblMark" runat="server" Text='<%# Eval("InwardDetail.ItemMark") %>' ToolTip='<%# Eval("InwardDetail.ItemMark") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblVakkal" Text="Vakkal" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblVakkal" runat="server" Text='<%#Eval("InwardDetail.LotCustomer")  %>' ToolTip='<%#Eval("InwardDetail.LotCustomer") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblPalletNo" Text="Pallete No/Container No" runat="server"></asp:Label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblPallete" runat="server" ForeColor="#3399ff" Text='<%# Eval("InwardDetail.PalletNo") %>' ToolTip='<%#Eval("InwardDetail.PalletNo")%>'></asp:Label><br />
                                    <asp:Label ID="lblContainerNo" runat="server" ForeColor="Maroon" Text='<%# Eval("InwardDetail.ContainerNo") %>' ToolTip='<%#Eval("InwardDetail.ContainerNo")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdVariety" Text="Variety/Count" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblVariety" ForeColor="#3399ff" Text='<%# Eval("InwardDetail.Variety.Name") %>' ToolTip='<%# Eval("InwardDetail.Variety.Name") %>' runat="server" />

                                    <br />
                                    <asp:Label ID="lblCount" ForeColor="Maroon" Text='<%# Eval("InwardDetail.Count") %>' ToolTip='<%# Eval("InwardDetail.Count") %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblhdBrand" Text="Brand/Origin" runat="server" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblBrand" ForeColor="#3399ff" Text='<%# Eval("InwardDetail.Brand.Name") %>' ToolTip='<%# Eval("InwardDetail.Brand.Name") %>' runat="server" />

                                    <br />
                                    <asp:Label ID="lblorigin" ForeColor="Maroon" Text='<%# Eval("InwardDetail.Origin.Name") %>' ToolTip='<%# Eval("InwardDetail.Origin.Name") %>' runat="server" />

                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                    </asp:GridView>
                </div>
            </div>

            <%--Search parameter panel--%>
            <div class="panel">
                <div class="panel-heading btn-primary">
                    <div class="row">
                    </div>

                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-2">

                            <asp:TextBox ID="UsrtxtPartnerGroupCode" onblur="ClearPartnerGroupSearch()" TabIndex="1" placeholder="Customer Group"
                                runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="UsrtxtPartnerGroupCode_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerGroupSearch"
                                ServiceMethod="PartnerGroupSearch" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" ContextKey="0" UseContextKey="true"
                                TargetControlID="UsrtxtPartnerGroupCode">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:HiddenField ID="UsrhdfPartnerGroup" runat="server" />
                                <asp:Label ID="UsrlblPartnerGroup" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="col-lg-2">

                            <asp:TextBox ID="UsrtxtPartnerCode" onblur="ClearPartnerSearch()" TabIndex="2" runat="server" placeholder="Customer"
                                AccessKey="p" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="UsrtxtPartnerCode_AutoCompleteExtender" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionSetCount="15" DelimiterCharacters="" EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" OnClientItemSelected="PartnerSearch"
                                ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                UseContextKey="true" ContextKey="0" TargetControlID="UsrtxtPartnerCode">
                            </asp:AutoCompleteExtender>
                            <div style="display: none;">
                                <asp:HiddenField ID="hdfCustomerName" runat="server" />
                                <asp:HiddenField ID="hdfCustomerID" runat="server" />
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtLotSearch" placeholder="Lot No" CssClass="form-control input-sm" MaxLength="10" runat="server"
                                TabIndex="4"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="ftblot" runat="server" FilterType="Numbers" TargetControlID="txtLotSearch">
                            </asp:FilteredTextBoxExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtReceivedQuantitySearch" TabIndex="4" MaxLength="20" placeholder="Received Quantity" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtItemGroup" TabIndex="4" MaxLength="20" placeholder="Item Group"
                                runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtItemGroup_AutoCompleteExtender" runat="server" EnableCaching="false"
                                TargetControlID="txtItemGroup" CompletionInterval="1" CompletionSetCount="10"
                                UseContextKey="true" ContextKey="0" DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1"
                                ServiceMethod="ItemCategory" FirstRowSelected="false" ServicePath="~/Service/AutoComplete.asmx"
                                ShowOnlyCurrentWordInCompletionListItem="true" CompletionListElementID="divwidth"
                                CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListItemCssClass="AutoExtenderList"
                                CompletionListCssClass="AutoExtender">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtStorageItemName" TabIndex="4" MaxLength="20" placeholder="Item Name"
                                runat="server" CssClass="form-control input-sm"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="txtLocSearch_AutoCompleteExtender" runat="server" EnableCaching="false"
                                TargetControlID="txtStorageItemName" CompletionInterval="1" CompletionSetCount="10" ContextKey="0" UseContextKey="true"
                                DelimiterCharacters="" Enabled="True" MinimumPrefixLength="1" ServiceMethod="ItemSearch"
                                FirstRowSelected="false" ServicePath="~/Service/AutoComplete.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                CompletionListElementID="divwidth" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListCssClass="AutoExtender">
                            </asp:AutoCompleteExtender>
                        </div>
                    </div>
                    <div class="newline"></div>
                    <div class="row">
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtpackingrefSearch" placeholder="Packing Reference" TabIndex="4" MaxLength="20" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtMarkSearch" placeholder="Item Mark" TabIndex="4" MaxLength="20" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtVakkalSearch" placeholder="Vakkal" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtSchemeSearch" placeholder="Scheme" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtBrand" placeholder="Brand" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-2">
                            <asp:TextBox ID="txtOrigin" placeholder="Origin" TabIndex="4" MaxLength="10" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                    </div>
                     <div class="newline"></div>
                    <div class="row">
                        <div class="col-lg-6">
                            <asp:Button ID="btnLotSearch" runat="server" OnClick="btnLotSearch_Click" TabIndex="19" CssClass="btn btn-primary btn-sm btn-block" Text="Search" />
                        </div>
                        <div class="col-lg-6">
                            <asp:Button ID="btnAddItems" runat="server" CausesValidation="False" CssClass="btn btn-primary btn-sm btn-block"
                                OnClick="btnAddItems_Click" TabIndex="19" Text="" />
                        </div>

                        <div class="col-lg-2">
                            <asp:Button ID="btnDeleteItem" runat="server" TabIndex="19" Visible="False"
                                OnClientClick="return confirmalert('Do you want to Delete ?' )" CssClass="btn btn-danger btn-sm" />
                        </div>
                    </div>
                    <hr />
                    <%--Search Grid on last Scection--%>
                    <div class="row">
                        <div class="col-lg-12" style="overflow: auto">
                            <asp:GridView ID="gvSearch" runat="server" AutoGenerateColumns="False" PagerStyle-CssClass="pagination-ys"
                                OnRowCommand="gvSearch_RowCommand" AllowPaging="True" OnPageIndexChanging="gvSearch_PageIndexChanging"
                                CssClass="table table-hover  table-striped text-nowrap" OnPreRender="gvSearch_PreRender" PageSize="30">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <input id="SelectAll" onclick="checkedAllSearch();" type="checkbox" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <input id="CheckBoxSelect" name="CheckBoxSelect" type="checkbox" tabindex="4" value='<%#Eval("InwardDetail_ID") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblSrNo" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSrno" runat="server" Text="<%# Convert.ToInt32(Container.DataItemIndex)+1%>" Visible="true"></asp:Label>
                                            <asp:HiddenField ID="hdfInwardDetailID" runat="server" Value='<%#Eval("InwardDetail_ID") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblLotNo" Text="LotNo" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lblLotNo" Text='<%#Eval("LotInternal") %>' CommandArgument='<%# Eval("InwardDetail_ID")%>'
                                                CommandName="Detail" ToolTip='<%#Eval("LotInternal")%>' runat="server"></asp:LinkButton>
                                            <asp:LinkButton ID="lblLotVersion" Text='<%#Eval("LotVersion") %>' CommandArgument='<%# Eval("InwardDetail_ID")%>'
                                                CommandName="Detail" ToolTip='<%#Eval("LotVersion")%>' runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblParty" Text="Party Name" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkpartner" ForeColor="#3399ff" Text='<%#Eval("CustomerName") %>' CommandArgument='<%# Eval("Customer_Company_ID")%>'
                                                CommandName="Customer" ToolTip='<%#Eval("CustomerName")%>' runat="server"></asp:LinkButton>
                                            <asp:HiddenField ID="hdfPartnerID" runat="server" Value='<%#Eval("Customer_Company_ID") %>' />
                                            <br />
                                            <asp:Label ID="lblCustomer" Text='<%#Eval("CustomerGroupName")%>' ForeColor="Maroon" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblItem" Text="Item" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblItem" runat="server" Text='<%#Eval("ItemName") %>'></asp:Label>
                                            <asp:HiddenField runat="server" ID="hdfStorageItemID" Value='<%#Eval("Item_ID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblMark" Text="Item Mark" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMark" runat="server" Text='<%# Eval("ItemMark") %>'
                                                ToolTip='<%# Eval("ItemMark") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblPackingRef" Text="Packing Ref" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPackingReference" Text='<%# Eval("PackingReference") %>' ToolTip='<%# Eval("PackingReference") %>'
                                                runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblVakkal" Text="Vakkal" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVakkal" runat="server" Text='<%#Eval("LotCustomer")  %>'
                                                ToolTip='<%#Eval("LotCustomer") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblOrderQty" Text="Order Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblOrderQuantity" runat="server" Text='<%#Eval("OrderQuantity") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblNetQty" Text="Net Qty" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblNetQuantity" runat="server" Text='<% #Eval("NetQuantity")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblScheme" Text="Scheme/Location" runat="server"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblScheme" runat="server" Text='<%#Eval("SchemeCode")  %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="lblLocationNo" runat="server" Text='<%#Eval("LocationName") %>' ForeColor="Maroon"
                                                ToolTip='<%#Eval("LocationName")%>'></asp:Label>
                                            <div style="height: 0px; width: 0px; visibility: hidden;">
                                                <asp:LinkButton ID="lnkAdditem" runat="server" CommandName="Sel" CommandArgument='<%#Eval("InwardDetail_ID") %>' />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblhdCount" Text="Variety/Count" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblVariety" Text='<%# Eval("VarietyName") %>' ToolTip='<%# Eval("VarietyName") %>' runat="server" />

                                            <br />
                                            <asp:Label ID="lblCount" Text='<%# Eval("Count") %>' ToolTip='<%# Eval("Count") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblhdBrand" Text="Brand/Origin" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblBrand" ForeColor="#3399ff" Text='<%# Eval("BrandName") %>' ToolTip='<%# Eval("BrandName") %>' runat="server" />

                                            <br />
                                            <asp:Label ID="lblorigin" ForeColor="Maroon" Text='<%# Eval("OriginName") %>' ToolTip='<%# Eval("OriginName") %>' runat="server" />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>



        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

