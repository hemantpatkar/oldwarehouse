﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
using System.Data;
using System.Web.UI;
public partial class VendorAddressDetails : BigSunPage
{
    AppCompany obj;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        obj = (AppCompany)Session[TableConstants.VendorSessionObj];
        if (!IsPostBack)
        {
            BindData();
        }
    }
    #endregion    
    #region Gridview Events
    protected void grdVendorAddress_PreRender(object sender, EventArgs e)
    {
        if (grdVendorAddress.Rows.Count > 0)
        {
            grdVendorAddress.UseAccessibleHeader = true;
            grdVendorAddress.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdVendorAddress_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grdVendorAddress.EditIndex = -1;        
        BindData();
    }
    protected void grdVendorAddress_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = "";
        if (strCommandName == "INSERT")
        {
            TextBox Address1 = grdVendorAddress.FooterRow.FindControl("txtNewAddress") as TextBox;
            TextBox Area = grdVendorAddress.FooterRow.FindControl("txtNewVendorArea") as TextBox;
            HiddenField hdfCountryID = grdVendorAddress.FooterRow.FindControl("hdfNewCountryId") as HiddenField;
            HiddenField hdfZipCodeID = grdVendorAddress.FooterRow.FindControl("hdfNewZipCodeID") as HiddenField;
            CheckBox IsActiveAddress = grdVendorAddress.FooterRow.FindControl("chkIsVendorActive") as CheckBox;
            CheckBox IsDefaultAddress = grdVendorAddress.FooterRow.FindControl("chkIsVendorDefault") as CheckBox;
            SaveAddress(strCommandID, Address1.Text, Area.Text, hdfCountryID.Value, hdfZipCodeID.Value, IsActiveAddress.Checked, IsDefaultAddress.Checked);
        }
        else if(strCommandName== "CANCELADD")
        {
            grdVendorAddress.ShowFooter = false;
            BindData();
        }
    }
    protected void grdVendorAddress_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdVendorAddress.ShowFooter = false;
        grdVendorAddress.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdVendorAddress_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string Index = new AppConvert(e.RowIndex);        
        TextBox Address1 = grdVendorAddress.Rows[e.RowIndex].FindControl("txtAddress") as TextBox;
        TextBox Area = grdVendorAddress.Rows[e.RowIndex].FindControl("txtVendorArea") as TextBox;
        HiddenField hdfCountryID = grdVendorAddress.Rows[e.RowIndex].FindControl("hdCountryID") as HiddenField;
        HiddenField hdfZipCodeID = grdVendorAddress.Rows[e.RowIndex].FindControl("hdfVendorPincodeID") as HiddenField;
        CheckBox IsActiveAddress = grdVendorAddress.Rows[e.RowIndex].FindControl("chkActive") as CheckBox;
        CheckBox IsDefaultAddress = grdVendorAddress.Rows[e.RowIndex].FindControl("chkDefault") as CheckBox;
        SaveAddress(Index, Address1.Text, Area.Text, hdfCountryID.Value, hdfZipCodeID.Value, IsActiveAddress.Checked, IsDefaultAddress.Checked);
        grdVendorAddress.ShowFooter = false;
        grdVendorAddress.EditIndex = -1;
    }
    protected void grdVendorAddress_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        obj = (AppCompany)Session[TableConstants.VendorSessionObj];
        obj.CompanyAddressColl[e.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
        Session.Add(TableConstants.VendorSessionObj,obj);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Address Successfully Deleted','1'" + ");", true);
        BindData();
    }
    #endregion
    #region Button Click Events
    protected void lbtnAddNew_Click(object sender, EventArgs e)
    {
        BindData(true);
        grdVendorAddress.ShowFooter = true;
    }
    #endregion
    #region BindData    
    private void BindData(bool AddNew = false)
    {
        AppCompanyAddressColl appCompanyAddressColl = obj.CompanyAddressColl;
        grdVendorAddress.DataSource = appCompanyAddressColl;
        grdVendorAddress.DataBind();
        if (appCompanyAddressColl.Count <= 0)
        {
            AddEmptyRow();                      
        }               
    }
    #endregion
    #region Save Address
    public void SaveAddress(string Id, string Address1, string Area, string CountryID, 
        string ZipCodeID, bool IsActive, bool IsDefault)
    {
        string Message = "";
        int intCommandID = new AppUtility.AppConvert(Id);
        AppCompanyAddress SingleObj = new AppCompanyAddress(intRequestingUserID);
        if (String.IsNullOrEmpty(Id) ==false)
        {           
            SingleObj = obj.CompanyAddressColl[intCommandID];
            Message = "Address Successfully Updated";
        } 
        else
        {
            obj.AddNewCompanyAddress(SingleObj);
            Message = "Address Successfully Address";
        }                        
        SingleObj.Company_ID = obj.ID;
        SingleObj.Address1 = Address1;
        SingleObj.Address2 = Area;
        SingleObj.ZipCodeType_ID = new AppUtility.AppConvert(ZipCodeID);
        SingleObj.CountryType_ID = new AppUtility.AppConvert(CountryID);
        SingleObj.Status = (IsActive == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;
        if (IsDefault == true)
        {
            obj.SetDefaultAddress();
        }
        Session.Add(TableConstants.VendorSessionObj, obj);
        BindData();
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + Message + "','1'" + ");", true);
        grdVendorAddress.EditIndex = -1; 
    }
    #endregion
    #region Textbox Changed Events
    protected void txtVendorPinCode_TextChanged(object sender, EventArgs e)
    {
        GridViewRow gvr = (GridViewRow)((TextBox)sender).Parent.Parent;
        int RowIndex = new AppConvert(gvr.RowIndex);
        TextBox City = (TextBox)(grdVendorAddress.Rows[RowIndex].FindControl("txtCity"));
        TextBox State = (TextBox)(grdVendorAddress.Rows[RowIndex].FindControl("txtState"));
        TextBox Country = (TextBox)(grdVendorAddress.Rows[RowIndex].FindControl("txtCountry"));
        HiddenField CountryID = (HiddenField)(grdVendorAddress.Rows[RowIndex].FindControl("hdCountryID"));
        HiddenField PincodeId = (HiddenField)(grdVendorAddress.Rows[RowIndex].FindControl("hdfVendorPincodeID"));
        TextBox Pincode = (TextBox)(grdVendorAddress.Rows[RowIndex].FindControl("txtVendorPinCode"));
        string PinCode = Pincode.Text;
        AppZipCodeTypeColl appZCTColl = new AppZipCodeTypeColl(intRequestingUserID);
        appZCTColl.Search(PinCode);
        if (appZCTColl.Count > 0)
        {
            AppZipCodeType objZip = appZCTColl[0];
            City.Text = objZip.CountryType.City;
            State.Text = objZip.CountryType.State;
            Country.Text = objZip.CountryType.Country;
            CountryID.Value = new AppUtility.AppConvert(objZip.CountryType.ID);
            PincodeId.Value = new AppUtility.AppConvert(objZip.CountryType.ID);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Pincode Found','2'" + ");", true);
            City.Text = "";
            State.Text = "";
            Country.Text = "";
            CountryID.Value = "";
            PincodeId.Value = "";
            Pincode.Focus();
        }        
    }
    protected void txtNewVendorPinCode_TextChanged(object sender, EventArgs e)
    {
        GridViewRow gvr = (GridViewRow)((TextBox)sender).Parent.Parent;        
        TextBox City = ((TextBox)(grdVendorAddress.FooterRow.FindControl("txtNewCity")));
        TextBox State = ((TextBox)(grdVendorAddress.FooterRow.FindControl("txtNewState")));
        TextBox Country = ((TextBox)(grdVendorAddress.FooterRow.FindControl("txtNewCountry")));
        HiddenField CountryID = ((HiddenField)(grdVendorAddress.FooterRow.FindControl("hdfNewCountryId")));
        HiddenField ZipCodeID = ((HiddenField)(grdVendorAddress.FooterRow.FindControl("hdfNewZipCodeID")));
        TextBox txtPinCode =  ((TextBox)(grdVendorAddress.FooterRow.FindControl("txtNewVendorPinCode")));
        string PinCode = txtPinCode.Text;
        AppZipCodeTypeColl appZCTColl = new AppZipCodeTypeColl(intRequestingUserID);
        appZCTColl.Search(PinCode);
        if (appZCTColl.Count > 0)
        {
            AppZipCodeType objZip = appZCTColl[0];
            City.Text = objZip.CountryType.City;
            State.Text = objZip.CountryType.State;
            Country.Text = objZip.CountryType.Country;
            CountryID.Value = new AppUtility.AppConvert(objZip.CountryType.ID);
            ZipCodeID.Value = new AppUtility.AppConvert(objZip.CountryType.ID);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Pincode Found','2'" + ");", true);
            City.Text = "";
            State.Text = "";
            Country.Text = "";
            CountryID.Value = "";
            ZipCodeID.Value = "";
            txtPinCode.Focus();
        }
    }
    #endregion    
    #region AddEmptyRow
    public void AddEmptyRow()
    {
        AppCompanyAddressColl CollOfAddress = new AppCompanyAddressColl(intRequestingUserID);           
        AppCompanyAddress newCompany = new AppCompanyAddress(intRequestingUserID);
        newCompany.Address1 = "Add atleast one Address";
        newCompany.Address2 = "";        
        newCompany.CountryType.City = "";
        newCompany.CountryType.State = "";
        newCompany.CountryType.Country = "";
        newCompany.ZipCodeType.ZipCode= "";        
        CollOfAddress.Add(newCompany);
        grdVendorAddress.DataSource = CollOfAddress;
        grdVendorAddress.DataBind();
        grdVendorAddress.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdVendorAddress.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdVendorAddress.ShowFooter = true;
    }
    #endregion
}