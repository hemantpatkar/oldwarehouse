﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="CompanyDetails" Codebehind="CompanyDetails.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="pull-right">
                        <br />
                        <asp:LinkButton ID="btnSave" runat="server" ValidationGroup="FinalSave" OnClick="btnSave_Click"><i class="fa fa-check  fa-2x text-success"></i></asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" OnClick="btnCancel_Click"><i class="fa fa-close fa-2x"></i></asp:LinkButton>
                        <asp:LinkButton ID="btnApprove" runat="server" OnClick="btnApprove_Click"><i class="fa fa-thumbs-o-up fa-2x"></i></asp:LinkButton>
                    </div>
                    <h3>Company Details
                    </h3>
                    <hr />
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:Label ID="lblCompanyCode" runat="server" CssClass="bold" Text="Company Code"></asp:Label>
                            <asp:Label ID="Label8" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="RFV_CompanyCode" runat="server" ControlToValidate="txtCompanyCode" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage="Select" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtCompanyCode" runat="server" TabIndex="1" MaxLength="50" ValidationGroup="FinalSave" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblCompanyName" runat="server" CssClass="bold " Text="Company Name"></asp:Label>
                            <asp:Label ID="lblVnameStar" runat="server" CssClass=" bold text-danger" Text=" * "></asp:Label>
                            <asp:RequiredFieldValidator ID="RFV_CompanyName" runat="server" ControlToValidate="txtCompanyName" CssClass="text-danger"
                                Display="Dynamic" ErrorMessage=" (Enter Company Name)" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:TextBox ID="txtCompanyName" TabIndex="1" runat="server" MaxLength="100" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="FinalSave"
                                CssClass="form-control input-sm"></asp:TextBox>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblparentCompany" CssClass="bold " runat="server" Text="Parent Company"> </asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtParentCompany" TabIndex="1" placeholder="Select Parent Company" runat="server"
                                    AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>
                            <asp:HiddenField runat="server" ID="hdfParentCompanyName" Value="" />
                            <asp:HiddenField runat="server" ID="hdfParentCompanyID" Value="" />
                            <asp:AutoCompleteExtender ID="ACEtxtParentCompany" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="1" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                OnClientItemSelected="AutoCompleteSearch" ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtParentCompany">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblCurrency" CssClass="bold " runat="server" Text="Currency"></asp:Label>
                            <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="form-control input-sm" TabIndex="1">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:Label ID="lblOrgnizationsType" CssClass="bold " runat="server" Text="Organization Type"></asp:Label>
                            <asp:Label ID="lblOrgType" runat="server" CssClass="text-danger" Text="*"></asp:Label>
                            <asp:RequiredFieldValidator ID="RFV_OrgnizationsType" runat="server" ControlToValidate="ddlOrganizationType" CssClass="text-danger" InitialValue="0"
                                Display="Dynamic" ErrorMessage="" ValidationGroup="FinalSave" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:DropDownList ID="ddlOrganizationType" runat="server" CssClass="form-control input-sm" TabIndex="1">
                            </asp:DropDownList>
                        </div>
                        <div class="col-lg-3">
                            <asp:Label runat="server" ID="lblFile" Text="File Upload" CssClass="bold" />
                            <asp:FileUpload runat="server" CssClass="file-upload" ID="FtpCompanyLogo" ToolTip="Company Logo" />
                        </div>
                        <div class="col-lg-3">
                            <asp:Label ID="lblTermsandConditions" CssClass="bold " runat="server" Text="Terms & Conditions"> </asp:Label>
                            <div class="input-group">
                                <asp:TextBox ID="txtTermsAndConditions" TabIndex="1" placeholder="Select Terms And Conditions" runat="server"
                                    AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>
                            <asp:HiddenField runat="server" ID="hdfTermsAndConditionsName" Value="" />
                            <asp:HiddenField runat="server" ID="hdfTermsAndConditionsID" Value="" />
                            <asp:AutoCompleteExtender ID="ACEtxtTermsAndConditions" runat="server"
                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TermsAndConditionsSearch" ServicePath="~/Service/AutoComplete.asmx"
                                OnClientItemSelected="AutoCompleteSearch" ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtTermsAndConditions">
                            </asp:AutoCompleteExtender>
                        </div>
                        <div class="col-lg-1">
                            <asp:Label ID="lblActive" runat="server" Text="Active/InActive" CssClass="bold "></asp:Label>
                            <div class="material-switch pull-right">
                                <asp:CheckBox runat="server" ID="chkActive" TabIndex="1" Checked="true" />
                                <label for='<%=chkActive.ClientID %>' class="label-primary"></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:Label ID="lblDescription" runat="server" CssClass="bold" Text="Description"></asp:Label>
                            <asp:TextBox ID="txtDescription" runat="server" TabIndex="1" MaxLength="50" ValidationGroup="FinalSave" CssClass="form-control input-sm" Enabled="true"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class=" col-lg-12">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:TabContainer runat="server" CssClass="Bigtab" ActiveTabIndex="0" ID="tabMain" OnClientActiveTabChanged="ActiveTabChanged">
                        <asp:TabPanel runat="server" HeaderText="Address" TabIndex="11" ID="tblAddress">
                            <ContentTemplate>
                                <div class="row" id="divAddress">
                                    <iframe runat="server" src="CompanyAddressDetails.aspx?Master=Simple" id="ifrmSearch" style="height: 600px;" class="col-lg-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>
                        <asp:TabPanel runat="server" HeaderText="Contacts" TabIndex="25" ID="tblContacts">
                            <ContentTemplate>
                                <div class="row" id="divContact">
                                    <iframe runat="server" src="CompanyContactDetails.aspx?Master=Simple" id="IfrmSample" style="height: 600px;" class="col-lg-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>
                        <asp:TabPanel runat="server" HeaderText="Statutory Info" TabIndex="25" ID="tblStatutory">
                            <ContentTemplate>
                                <div class="row" id="divStatutoryInfo">
                                    <iframe runat="server" src="CompanyStatutoryInfo.aspx?Master=Simple" id="IframeInfoStatuInfo" style="height: 600px;" class=" col-lg-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>
                        <asp:TabPanel runat="server" HeaderText="Bank Details" TabIndex="25" ID="tblItemMap">
                            <ContentTemplate>
                                <div class="row" id="divBankDetails">
                                    <iframe runat="server" src="CompanyBankDetails.aspx?Master=Simple" id="iframeItemMap" style="height: 600px;" class=" col-lg-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>
                        <asp:TabPanel runat="server" HeaderText="Users" TabIndex="25" ID="TabPanel1">
                            <ContentTemplate>
                                <div class="row" id="divCompanyUsers">
                                    <iframe runat="server" src="CompanyUser.aspx?Master=Simple" id="iframe1" style="height: 600px;" class=" col-lg-12 iframe"></iframe>
                                </div>
                            </ContentTemplate>
                        </asp:TabPanel>
                    </asp:TabContainer>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    <script type="text/javascript">
        function ActiveTabChanged(sender, e) {
            var activeTab = sender.get_activeTabIndex();
            if (activeTab == 1) {
                $("#divContact").html("<iframe  src='CompanyContactDetails.aspx?Master=Simple'  id='frmSample' style='height: 600px;' class='col-lg-12 iframe'></iframe>");
            }
            if (activeTab == 4) {
                $("#divCompanyUsers").html("<iframe  src='CompanyUser.aspx?Master=Simple'  id='frmItemSample' style='height: 600px;' class='col-lg-12 iframe'></iframe>");
            }
        }
    </script>
</asp:Content>
