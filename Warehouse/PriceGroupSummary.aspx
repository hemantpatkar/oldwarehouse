﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="PriceGroupSummary" Codebehind="PriceGroupSummary.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <h3>Price Group Summary </h3>
                    <hr />
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:Label ID="lblPriceGrpCode" runat="server" CssClass="bold " Text="Code"></asp:Label>
                            <asp:TextBox ID="txtPriceGrpCode" runat="server" CssClass=" form-control input-sm"></asp:TextBox>
                        </div>

                        <div class="col-lg-3">
                            <asp:Label ID="lblPriceGrpName" runat="server" CssClass="bold " Text="Name"></asp:Label>
                            <asp:TextBox ID="txtPriceGrpName" TabIndex="1" runat="server" AutoCompleteType="None" AutoComplete="Off"
                                CssClass=" form-control input-sm"></asp:TextBox>
                        </div>                        
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">                        
                            <div class="col-lg-2">
                                <asp:Button runat="server" Text="Search" CssClass="btn btn-sm btn-block btn-primary" ID="Search" OnClick="Search_Click">                                    
                                </asp:Button>
                            </div>
                            <div class="col-lg-2">
                                <asp:Button runat="server" ID="lnkNewPriceGrp" Text="New Price Group" CssClass="btn  btn-sm btn-block btn-primary" OnClick="lnkNewPriceGrp_Click" > 
                                </asp:Button>
                            </div>                            
                        </div>                     
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-body">
                <asp:GridView ID="grdPriceGrpSummary" runat="server"  AutoGenerateColumns="False" Width="100%" 
                    OnPreRender="grdPriceGrpSummary_PreRender" GridLines="Horizontal" OnRowDeleting="grdPriceGrpSummary_RowDeleting"
                    CssClass="table table-striped table-hover  text-nowrap  dt-responsive nowrap">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblSelect" Text="Delete" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete" CommandName="DELETE" CommandArgument='<%#Eval("ID") %>' Text="" ToolTip="Delete" />
                            </ItemTemplate>                           
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblCode" Text="Code" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink TabIndex="1" CausesValidation="false" ID="lbtnCode" runat="server"
                                    Text='<%# Eval("Code")%>'  NavigateUrl='<% #"PriceGroup.aspx?PriceGroupID=" + Eval("ID")%>' Target="_blank"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label ID="lblHdrName" Text="Name" runat="server" CssClass="bold"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                       
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var GridID = $get('<%=grdPriceGrpSummary.ClientID%>');
        $(document).ready(function () {
            GridUI(GridID, 50);
        })     

    </script>
</asp:Content>

