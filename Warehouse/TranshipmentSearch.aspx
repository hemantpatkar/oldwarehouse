﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" Culture="en-gb" AutoEventWireup="true" Inherits="TranshipmentSearch" Codebehind="TranshipmentSearch.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-lg-9">
            <div class="heading">
                <i class="fa fa-truck"></i>
                <asp:Label ID="lblTitleLocationTransferSearch" Text="Transhipment Search" runat="server"></asp:Label>
            </div>
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnSearch" CssClass="btn btn-primary btn-block btn-sm" runat="server" OnClick="btnSearch_Click" Text="Search" ValidationGroup="Search" />
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnNew" OnClick="btnNew_Click" CssClass="btn btn-primary btn-block btn-sm" runat="server" AccessKey="n" CausesValidation="False" Text="New" />
        </div>
        <div class="col-lg-1">
            <asp:LinkButton ID="btnExport" CssClass="btn btn-warning btn-block btn-sm" runat="server" OnClick="btnExportToExcel_Click" AccessKey="n" CausesValidation="False" Text="Export" />
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblTranshipmentNo" Text="Transhipment No" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtTranshipmentNo" runat="server" CssClass="form-control input-sm" ValidationGroup="Search"></asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblTransferDate" Text="Transhipment Date" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtTransferDate" runat="server"></asp:TextBox>
            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" CultureAMPMPlaceholder=""
                CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder=""
                CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder=""
                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtTransferDate">
            </asp:MaskedEditExtender>
            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" TargetControlID="txtTransferDate">
            </asp:CalendarExtender>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblCustomer" Text="Customer" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtCustomer" runat="server" CssClass="form-control input-sm" ValidationGroup="Search">
            </asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblReceivedBy" Text="Received By" CssClass="bold"></asp:Label>
            <asp:TextBox ID="txtReceivedBy" runat="server" CssClass="form-control input-sm" ValidationGroup="Search"></asp:TextBox>
        </div>
        <div class="col-lg-2">
            <asp:Label runat="server" ID="lblStatus" Text="Status" CssClass="bold"></asp:Label>
            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control input-sm">
                <asp:ListItem Value="-1" Selected="True">All</asp:ListItem>
                <asp:ListItem Value="1">Un Approved</asp:ListItem>
                <asp:ListItem Value="2">Approved</asp:ListItem>
                <asp:ListItem Value="-10">Deleted</asp:ListItem>
            </asp:DropDownList>
            <asp:HiddenField ID="hdfgatereg" runat="server" />
            <asp:HiddenField ID="ShowLastRecords" runat="server" />
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-lg-12" style="overflow: auto">
            <asp:GridView ID="grdTranshipment" runat="server" AllowPaging="true" PagerStyle-CssClass="pagination-ys" AutoGenerateColumns="false" CssClass="table table-hover table-striped text-nowrap table-Full" OnPreRender="grdTranshipment_PreRender">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HyperLink ID="lnkNo" runat="server" CssClass="fa fa-edit fa-2x" NavigateUrl='<%#"TranshipmentDetail.aspx?TranshipmentID="+ Eval("ID")%>' CausesValidation="False" Target="_blank"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Transhipment No">
                        <ItemTemplate>
                            <asp:Label ID="lblTranshipmentNo" runat="server" Text='<%# Eval("TranshipmentNo") %>' ToolTip='<%# Eval("TranshipmentNo") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Transhipment Date">
                        <ItemTemplate>
                            <asp:Label ID="lblTranshipmentDate" Text='<%#  Convert.ToDateTime(Eval("TranshipmentDate")).ToShortDateString() %>' ToolTip='<%# Eval("TranshipmentDate")%>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="GateEntry From">
                        <ItemTemplate>
                            <asp:Label ID="lblFrom_GateEntry" runat="server" Text='<%#Eval("From_GateEntry.GateNo")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="GateEntry To">
                        <ItemTemplate>
                            <asp:Label ID="lblGateNoto" Text='<%#Eval("To_GateEntry.GateNo") %>' ToolTip='<%#Eval("To_GateEntry.GateNo") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Modified By">
                        <ItemTemplate>
                            <asp:Label ID="lblModifiedBy" Text='<%# Eval("ModifiedBy") %>' ToolTip='<%# Eval("ModifiedBy") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Modified On">
                        <ItemTemplate>
                            <asp:Label ID="lblModifiedOn" Text='<%# Convert.ToDateTime(Eval("ModifiedOn")).ToShortDateString() %>' ToolTip='<%# Convert.ToDateTime(Eval("ModifiedOn")).ToString("dd/MM/yyyy") %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Print">
                        <ItemTemplate>
                            <asp:HyperLink ID="HLReport" runat="server" ToolTip="View Report" Target="_blank" NavigateUrl='<% #"../../Reports/ReportView.aspx?GateRegID=" + Eval("ID") + "&RPName=GatePass"%>'>
                                        <i class="fa fa-print fa-2x"></i>
                            </asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>

</asp:Content>

