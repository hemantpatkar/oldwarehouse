﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="T_ReceiptVoucherDetails" Codebehind="T_ReceiptVoucherDetails.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
    function switchToTab(sender,args)
    {
        debugger;
        var tabContainer = $find("<%= tabMain.ClientID %>");
        var tab = $find("<%= tabMain.ClientID %>").get_activeTabIndex();
        switch (tab)
        {
            case 0:
                if (Page_ClientValidate('BasicInfo') == false)
                {
                    $find("<%= tabMain.ClientID %>").set_activeTabIndex(0);
                    $find("<%= tblBasicInfo.ClientID %>").set_activeTabIndex(0);
                }
                else if (Page_ClientValidate('AddiInfo') == false)
                {
                    $find("<%= tabMain.ClientID %>").set_activeTabIndex(1);
                    $find("<%= tblAddiInfo.ClientID %>").set_activeTabIndex(0);
                }
            break;
            case 1:
            if (Page_ClientValidate('BasicInfo') == false)
            {
                $find("<%= tabMain.ClientID %>").set_activeTabIndex(0);
            }
            else if (Page_ClientValidate('BasicInfo') == false)
            {
                $find("<%= tabMain.ClientID %>").set_activeTabIndex(1);
                $find("<%= tblBasicInfo.ClientID %>").set_activeTabIndex(0);
            }
        }
}
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="mainDiv">
        <asp:UpdatePanel ID="up_ReceiptVoucher" runat="server">
            <ContentTemplate>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body">
                                <div id="divButtons" class="pull-right">

                                     <div class="btn-group">
                                        <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="btn btn-sm  btn-primary" OnClientClick="switchToTab();" ValidationGroup="BasicInfo, AddiInfo" OnClick="btnSave_Click"></asp:Button>
                                        <asp:Button runat="server" ID="btnApprove" Visible="false" Text="Approve" CssClass="btn  btn-sm  btn-primary" OnClick="btnApprove_Click1"></asp:Button>
                                        <asp:Button runat="server" ID="btnCancel" CausesValidation="false" Text="Back" CssClass="btn  btn-sm  btn-primary" OnClick="btnCancel_Click"></asp:Button>
                                    </div>
                                     
                                    
                                </div>
                                <h3>Receipt Voucher Details </h3>
                                <hr />
                                <div class="row">
                                    <div class=" col-lg-12">
                                        <asp:TabContainer runat="server" CssClass="Bigtab" ActiveTabIndex="0" ID="tabMain">
                                            <asp:TabPanel runat="server" HeaderText="Basic Info" TabIndex="0" ID="tblBasicInfo">
                                                <ContentTemplate>
                                                    <div class="row" runat="server" id="divBasicInfo">
                                                        <div id="divPaymentDate" class="col-lg-2">
                                                            <asp:Label ID="lblPaymentDate" runat="server" Text="Receipt Date" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblMsgPaymentDt" runat="server" Text="*" CssClass="bold text-danger"></asp:Label>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtPaymentDate" TabIndex="1" runat="server" placeholder="Payment Date" CssClass="form-control input-sm" ValidationGroup="BasicInfo"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RFV_txtPaymentDate" runat="server" ErrorMessage="Select Date" ValidationGroup="BasicInfo" ControlToValidate="txtPaymentDate"></asp:RequiredFieldValidator>
                                                            <asp:MaskedEditExtender ID="Mee_txtPaymentDate" runat="server"
                                                                Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtPaymentDate">
                                                            </asp:MaskedEditExtender>
                                                            <asp:CalendarExtender ID="Ce_PaymentDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtPaymentDate"
                                                                TargetControlID="txtPaymentDate">
                                                            </asp:CalendarExtender>
                                                        </div>

                                                        <div id="divPaymentType" class="col-lg-2">
                                                            <asp:Label ID="lblPaymentType" CssClass="bold " runat="server" Text="Receipt Mode"></asp:Label>
                                                            <asp:Label ID="lblMsgPaymentType" runat="server" Text="*" CssClass="bold text-danger"></asp:Label>
                                                            <asp:DropDownList ID="ddlPaymentType" AutoPostBack="true" runat="server" CssClass="form-control input-sm" TabIndex="0" OnSelectedIndexChanged="ddlPaymentType_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RFV_ddlPaymentType" runat="server" ErrorMessage="Select Payment Type" ValidationGroup="BasicInfo" ControlToValidate="ddlPaymentType" InitialValue="0"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div id="divPaymentFor" class="col-lg-2">
                                                            <asp:Label ID="lblPaymentFor" runat="server" Text="Payment For" CssClass="bold"></asp:Label>
                                                            <asp:DropDownList ID="ddlPaymentFor" AutoPostBack="true" runat="server" CssClass="form-control input-sm" TabIndex="0" OnSelectedIndexChanged="ddlPaymentFor_SelectedIndexChanged">
                                                                <asp:ListItem Text="Customer" Value="1" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="Tds" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="Refund" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="Other" Value="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div id="divCustomerName" runat="server" class="col-lg-4">
                                                            <asp:Label ID="lblCustomerName" runat="server" Text="Customer Name" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblMsgCustomerName" runat="server" Text="*" CssClass="bold text-danger"></asp:Label>
                                                            <asp:RequiredFieldValidator ID="RFV_txtCustomer" runat="server" ErrorMessage="Select Customer" ValidationGroup="BasicInfo" ControlToValidate="txtCustomer"></asp:RequiredFieldValidator>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtCustomer" TabIndex="1" AutoPostBack="true" placeholder="Select Customer Name" runat="server" AutoCompleteType="None" AutoComplete="Off" onblur="return ClearAutocompleteTextBox(this)" OnTextChanged="txtCustomerName_TextChanged"
                                                                    CssClass="form-control input-sm"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfCustomerName" Value="" />
                                                            <asp:HiddenField runat="server" ID="hdfCustomerID" Value="" />
                                                            <asp:AutoCompleteExtender ID="ACEtxtCustomer" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="2" UseContextKey="true"
                                                                CompletionSetCount="100" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CompanySearch" ServicePath="~/Service/AutoComplete.asmx"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtCustomer">
                                                            </asp:AutoCompleteExtender>
                                                        </div>

                                                        <div id="divNetPayable" class="col-lg-2">
                                                            <asp:Label ID="lblNetPayable" runat="server" Text="Net Payable" CssClass="bold"></asp:Label>
                                                            <asp:TextBox ID="txtNetPayable" Enabled="false" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                            <asp:TabPanel runat="server" HeaderText="Additional Info" TabIndex="0" ID="tblAddiInfo">
                                                <ContentTemplate>
                                                    <div class="row" id="divPayeeNameDetails">
                                                        <div id="divPayeeName" class="col-lg-3">
                                                            <asp:Label ID="lblPayeeName" runat="server" Text="Payee Name" CssClass="bold"></asp:Label>
                                                            <asp:TextBox ID="txtPayee" TabIndex="0" placeholder="Enter Payee Name" runat="server" AutoCompleteType="None" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <asp:Label ID="lblLedger" runat="server" Text="Ledger" CssClass="bold"></asp:Label>
                                                            <div class="input-group">
                                                                <asp:RequiredFieldValidator ID="RFV_txtLedger" Enabled="true" runat="server" ControlToValidate="txtLedger" CssClass="text-danger"
                                                                    Display="Dynamic" ValidationGroup="AddiInfo" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                                <asp:TextBox ID="txtLedger" placeholder="Select Ledger" runat="server" MaxLength="500" AutoPostBack="false" TabIndex="2" AutoComplete="Off" CssClass="form-control input-sm" onblur="return ClearAutocompleteTextBox(this)"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfLedgerName" />
                                                            <asp:HiddenField runat="server" ID="hdfLedgerID" />
                                                            <asp:AutoCompleteExtender ID="ACEtxtLedger" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight"  
                                                                ContextKey=""
                                                                UseContextKey="true"
                                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx" OnClientItemSelected="AutoCompleteSearch"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLedger">
                                                            </asp:AutoCompleteExtender>
                                                        </div>
                                                        <div id="divDescription" class="col-lg-6">
                                                            <asp:Label ID="lblDescription" runat="server" Text="Description" CssClass="bold"></asp:Label>
                                                            <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="row" id="divVoucherDetails">
                                                        <div id="divVoucherNo" class="col-lg-3">
                                                            <asp:Label ID="lblVoucherNo" runat="server" Text="Voucher No" CssClass="bold"></asp:Label>
                                                            <asp:TextBox ID="txtVoucherNo" TabIndex="0" Enabled="false" runat="server" AutoCompleteType="None" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <asp:RequiredFieldValidator ID="Rfv_txtCurrency" runat="server" ControlToValidate="txtCurrency" CssClass="text-danger"
                                                                Display="Dynamic" ValidationGroup="AddiInfo" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                            <asp:Label ID="lblCurrency" CssClass="bold " runat="server" Text="Currency"> </asp:Label>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtCurrency" Text='<%# Eval("CurrencyType.Name") %>' TabIndex="1" placeholder="Select Transaction Currency" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfCurrencyName" Value='<%# Eval("CurrencyType.Name") %>' />
                                                            <asp:HiddenField runat="server" ID="hdfCurrencyID" Value='<%# Eval("CurrencyType_ID") %>' />
                                                            <asp:AutoCompleteExtender ID="ACEtxtCurrency" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CurrencyTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtCurrency">
                                                            </asp:AutoCompleteExtender>

                                                        </div>

                                                        <div id="divExchangeCurrency" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderExchangeCurrency" CssClass="bold " runat="server" Text="Transaction Currency"> </asp:Label>
                                                            <asp:Label ID="lblHeaderExchangeCurrencyMsg" runat="server" CssClass=" bold text-danger" Text=" "></asp:Label>
                                                            <asp:RequiredFieldValidator ID="Rfv_txtHeaderExchangeCurrency" Enabled="false" runat="server" ControlToValidate="txtHeaderExchangeCurrency" CssClass="text-danger"
                                                                Display="Dynamic" ErrorMessage=" (Select Currency)" ValidationGroup="AddiInfo" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtHeaderExchangeCurrency" TabIndex="1" placeholder="Select Transaction Currency" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                            </div>
                                                            <asp:HiddenField runat="server" ID="hdfHeaderExchangeCurrencyName" Value="" />
                                                            <asp:HiddenField runat="server" ID="hdfHeaderExchangeCurrencyID" Value="" />
                                                            <asp:AutoCompleteExtender ID="ACEtxtHeaderExchangeCurrency" runat="server"
                                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CurrencyTypeSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtHeaderExchangeCurrency">
                                                            </asp:AutoCompleteExtender>
                                                        </div>
                                                        <div id="divExchangeCurrencyRate" class="col-lg-3">
                                                            <asp:Label ID="lblHeaderExchangeCurrencyRate" Text="Exchange Currency Rate" runat="server" CssClass="bold"></asp:Label>
                                                            <asp:Label ID="lblHeaderExchangeCurrencyRateMsg" runat="server" CssClass="text-danger" Text=""></asp:Label>
                                                            <div class="input-group">
                                                                <asp:TextBox ID="txtHeaderExchangeCurrencyRate" placeholder="0.00" runat="server" AutoPostBack="True" CssClass="form-control amt input-sm"
                                                                    onkeypress="return validateFloatKeyPress(this, event)" TabIndex="1" ReadOnly="True"></asp:TextBox>
                                                                <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </ContentTemplate>
                                            </asp:TabPanel>
                                        </asp:TabContainer>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="divBankDetails" runat="server" visible="false">
                    <div class="col-lg-12">
                        <div class="panel">
                            <div class="panel-body" style="padding-top: 10px;">
                                <div class="row" id="CompanyBankDetails">
                                    <div id="divCompanyAccountNo" class="col-lg-3">
                                        <asp:Label ID="lblCompanyAccountNo" CssClass="bold " runat="server" Text="Company Account Number"></asp:Label>
                                        <asp:DropDownList ID="ddlCompanyAccountNo" AutoPostBack="true" runat="server" CssClass="form-control input-sm" TabIndex="0" OnSelectedIndexChanged="ddlCompanyAccountNo_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divCompanyBankBranch" class="col-lg-3">
                                        <asp:Label ID="lblCompanyBankBranch" CssClass="bold " runat="server" Text="Company Bank Branch"></asp:Label>
                                        <asp:DropDownList ID="ddlCompanyBankBranch" Enabled="false" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divCompanyBank" class="col-lg-2">
                                        <asp:Label ID="lblCompanyBank" CssClass="bold " runat="server" Text="Company Bank"></asp:Label>
                                        <asp:DropDownList ID="ddlCompanyBank" Enabled="false" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divChequeNo" class="col-lg-2" runat="server">
                                        <asp:Label ID="lblChequeNo" runat="server" Text="Cheque / DD No" CssClass="bold"></asp:Label>
                                        <asp:TextBox ID="txtChequeNo" runat="server" CssClass="form-control input-sm"></asp:TextBox>
                                    </div>
                                    <div id="divChequeDate" class="col-lg-2" runat="server">
                                        <asp:Label ID="lblChequeDate" runat="server" Text="Cheque Date" CssClass="bold"></asp:Label>
                                        <div class="input-group">
                                            <asp:TextBox ID="txtChequeDate" TabIndex="1" runat="server" placeholder="Cheque Date" CssClass="form-control input-sm"></asp:TextBox>
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        </div>
                                        <asp:MaskedEditExtender ID="MEE_ChequeDate" runat="server"
                                            Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtChequeDate">
                                        </asp:MaskedEditExtender>
                                        <asp:CalendarExtender ID="CE_ChequeDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtChequeDate"
                                            TargetControlID="txtChequeDate">
                                        </asp:CalendarExtender>
                                    </div>
                                </div>
                                <div class="row hidden" id="CustomerBankDetails">
                                    <div id="divCustomerAccountNo" class="col-lg-3">
                                        <asp:Label ID="lblCustomerAccountNo" CssClass="bold " runat="server" Text="Customer Account Number"></asp:Label>
                                        <asp:DropDownList ID="ddlCustomerAccountNo" AutoPostBack="true" OnSelectedIndexChanged="ddlCustomerAccountNo_SelectedIndexChanged" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divCustomerBankBranch" class="col-lg-3">
                                        <asp:Label ID="lblCustomerBankBranch" CssClass="bold " runat="server" Text="Customer Bank Branch"></asp:Label>
                                        <asp:DropDownList ID="ddlCustomerBankBranch" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                    <div id="divCustomerBank" class="col-lg-2">
                                        <asp:Label ID="lblCustomerBank" CssClass="bold " runat="server" Text="Customer Bank"></asp:Label>
                                        <asp:DropDownList ID="ddlCustomerBank" runat="server" CssClass="form-control input-sm" TabIndex="0">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="row" id="divBillDetails" runat="server">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body">
                    <asp:UpdatePanel runat="server" ID="ReceiptVoucher">
                        <ContentTemplate>
                            <asp:GridView ID="grdReceiptVoucherDetails" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="true" GridLines="Both" ShowFooter="true"
                                CssClass="table table-hover table-striped text-nowrap nowrap" OnRowCommand="grdReceiptVoucherDetails_RowCommand" OnPreRender="grdReceiptVoucherDetails_PreRender">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="250">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderLedger" Text="Ledger" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtLedger" runat="server" ControlToValidate="txtLedger" CssClass="text-danger" Display="Dynamic" ValidationGroup="BasicInfo" SetFocusOnError="true"></asp:RequiredFieldValidator>

                                            <div class="input-group">
                                                <asp:TextBox ID="txtLedger" Enabled='<%#Convert.ToBoolean(((ddlPaymentFor.SelectedValue=="1" || ddlPaymentFor.SelectedValue=="3") ?0:1))%>' TabIndex="1" Text='<%# Eval("Ledger.LedgerName") %>' AutoPostBack="true" OnTextChanged="txtLedger_TextChanged" placeholder="Select Ledger" runat="server" AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>

                                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                            </div>


                                            <asp:HiddenField runat="server" ID="hdfLedgerName" Value='<%# Eval("Ledger.LedgerName") %>' />
                                            <asp:HiddenField runat="server" ID="hdfLedgerID" Value='<%# Eval("Ledger_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtLedger" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey='<%#Convert.ToString(((ddlPaymentFor.SelectedValue=="1" || ddlPaymentFor.SelectedValue=="3") ?"1": ddlPaymentFor.SelectedValue=="2"? "6":"")+(",~" + "~" + new AppUtility.AppConvert((int)AppObjects.LedgerFlag.GeneralLedger)))%>' UseContextKey="true"
                                                CompletionSetCount="100" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="LedgerSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtLedger">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="300">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderDescription" Text="Description" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtReceiptVoucherDesc" TabIndex="1" Text='<%# Eval("ReceiptVoucherDesc") %>' AutoPostBack="true" OnTextChanged="txtReceiptVoucherDesc_TextChanged" placeholder="Enter Description" runat="server" AutoCompleteType="None" AutoComplete="Off" CssClass="form-control input-sm"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="150">
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderAmount" Text="Amount" runat="server" CssClass="bold"></asp:Label>
                                                ( <span><i class="fa fa-inr"></i></span>)
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAmount" TabIndex="1" OnTextChanged="txtAmount_TextChanged" AutoPostBack="true" Text='<%# Eval("Amount") %>' placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterAmount" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="200" Visible="true">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderTDS" Text="TDS" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtTDS" runat="server" ControlToValidate="txtTDS" CssClass="text-danger" Display="Dynamic" ValidationGroup="BasicInfo" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtTDS" TabIndex="1" Text='<%# Eval("TDS.Name") %>' AutoPostBack="true" OnTextChanged="txtTDS_TextChanged" placeholder="Select TDS" runat="server"
                                                AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfTDSName" Value='<%# Eval("TDS.Name") %>' />
                                            <asp:HiddenField runat="server" ID="hdfTDSID" Value='<%# Eval("TDS_ID") %>' />
                                            <asp:HiddenField runat="server" ID="hdfTDSRate" Value='<%# Eval("TDSRate") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtTDS" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="TDSSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtTDS">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="150">
                                        <HeaderTemplate>
                                            <div class="input-group">
                                                <asp:Label ID="lblHeaderTDSAmount" Text="TDS Amount" runat="server" CssClass="bold"></asp:Label>
                                                ( <span><i class="fa fa-inr"></i></span>)
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtTDSAmount" TabIndex="1" OnTextChanged="txtTDSAmount_TextChanged" AutoPostBack="true" Text='<%# Eval("TDSAmount") %>' placeholder="0.00" runat="server" AutoCompleteType="None"
                                                AutoComplete="Off" ValidationGroup="save" onkeypress="return validateFloatKeyPress(this, event)" CssClass="form-control input-sm"></asp:TextBox>

                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFooterTDSAmount" Text="0.00" runat="server" CssClass="form-control input-sm"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="200" Visible="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderProfitCenter" Text="Profit Center" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtProfitCenter" runat="server" ControlToValidate="txtProfitCenter" CssClass="text-danger" Display="Dynamic" ValidationGroup="BasicInfo" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtProfitCenter" TabIndex="1" Text='<%# Eval("ProfitCenter.ProfitCenterName") %>' AutoPostBack="true" OnTextChanged="txtProfitCenter_TextChanged" placeholder="Select Profit Center" runat="server"
                                                AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfProfitCenterName" Value='<%# Eval("ProfitCenter.ProfitCenterName") %>' />
                                            <asp:HiddenField runat="server" ID="hdfProfitCenterID" Value='<%# Eval("ProfitCenter_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtProfitCenter" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="ProfitCenterSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtProfitCenter">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="200" Visible="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCostCenter" Text="Cost Center" runat="server" CssClass="bold"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:RequiredFieldValidator ID="Rfv_txtCostCenter" runat="server" ControlToValidate="txtCostCenter" CssClass="text-danger" Display="Dynamic" ValidationGroup="BasicInfo" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtCostCenter" TabIndex="1" Text='<%# Eval("CostCenter.CostCenterName") %>' AutoPostBack="true" OnTextChanged="txtCostCenter_TextChanged" placeholder="Select Cost Center" runat="server"
                                                AutoCompleteType="None" AutoComplete="Off" ValidationGroup="save" onblur="return ClearAutocompleteTextBox(this)" CssClass="form-control input-sm"></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdfCostCenterName" Value='<%# Eval("CostCenter.CostCenterName") %>' />
                                            <asp:HiddenField runat="server" ID="hdfCostCenterID" Value='<%# Eval("CostCenter_ID") %>' />
                                            <asp:AutoCompleteExtender ID="ACEtxtCostCenter" runat="server"
                                                CompletionInterval="1" CompletionListElementID="divwidth" CompletionListCssClass="AutoExtender"
                                                CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" ContextKey="" UseContextKey="true"
                                                CompletionSetCount="10" DelimiterCharacters="," EnableCaching="false" Enabled="True" OnClientItemSelected="AutoCompleteSearch"
                                                FirstRowSelected="true" MinimumPrefixLength="1" ServiceMethod="CostCenterSearch" ServicePath="~/Service/AutoComplete.asmx"
                                                ShowOnlyCurrentWordInCompletionListItem="true" TargetControlID="txtCostCenter">
                                            </asp:AutoCompleteExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="15">
                                        <HeaderTemplate>
                                            <asp:Button ID="btnAddLineItem" TabIndex="1" Text="Add Item" AccessKey="A" runat="server" OnClick="btnAddLineItem_Click" CssClass="fa fa -button"></asp:Button>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnRemoveLineItem" CommandName="Remove" TabIndex="1" runat="server" Text="Remove"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="grdReceiptVoucherDetails" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
