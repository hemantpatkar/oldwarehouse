﻿#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
#endregion

public partial class ItemSummary :BigSunPage
{
    AppItemColl objItemColl;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        objItemColl = new AppItemColl(intRequestingUserID);
        if (!IsPostBack)
        {
            objItemColl = (AppItemColl)Session[TableConstants.ItemsCollection];
            BindData();
        }
    }
    #endregion

    #region Gridview Events
    protected void grdItemSummary_PreRender(object sender, EventArgs e)
    {
        if (grdItemSummary.Rows.Count > 0)
        {
            grdItemSummary.UseAccessibleHeader = true;
            grdItemSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdItemSummary_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        objItemColl = (AppItemColl)Session[TableConstants.ItemsCollection];
        objItemColl[e.RowIndex].Status = -10;
        objItemColl[e.RowIndex].Save();
    }
    #endregion

    #region Button Click Events
    protected void lbtnNewItem_Click(object sender, EventArgs e)
    {
        Response.Redirect("ItemMaster.aspx");
    }
    protected void lbtnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    #endregion

    #region BindData   

    public void BindData()
    {
        AppItemColl objCollection = new AppItemColl(intRequestingUserID);
        RecordStatus Status = CommonFunctions.GetStatus(ddlActiveStatus.SelectedValue);
        objCollection.Clear();
        if (!string.IsNullOrEmpty(txtItemCode.Text))
        {
            objCollection.AddCriteria(AppObjects.Item.Code, AppUtility.Operators.Like, txtItemCode.Text);
        }

        if (!string.IsNullOrEmpty(txtItemName.Text))
        {
            objCollection.AddCriteria(AppObjects.Item.Name, AppUtility.Operators.Like, txtItemName.Text);
        }

        if (ddlActiveStatus.SelectedValue != "-32768")
        {
            objCollection.AddCriteria(AppObjects.Item.Status, AppUtility.Operators.Equals, ddlActiveStatus.SelectedValue);
        }
        
        objCollection.Search(RecordStatus.ALL);
        grdItemSummary.DataSource = objCollection;
        grdItemSummary.DataBind();
        Session.Add(TableConstants.ItemsCollection, objCollection);
        if (objCollection.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Records Found','2'" + ");", true);
        }
    }
    #endregion
}