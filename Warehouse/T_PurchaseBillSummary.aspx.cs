﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
public partial class T_PurchaseBillSummary : BigSunPage
{
    #region PageLoad  
    AppPurchaseGRNColl objPO;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }
    #endregion
    #region Function   
    protected void BindData()
    {
        AppObjects.AppPurchaseGRNColl objColl = new AppPurchaseGRNColl(intRequestingUserID);
        if (txtPurchaeGRNNo.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseGRN.InvoiceNumber, AppUtility.Operators.Equals, txtPurchaeGRNNo.Text, 0);
        }
        if (txtPurchaseGRNAmount.Text != "")
        {
            switch (ddlOperators.SelectedValue)
            {
                case "1":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseGRN.InvoiceBaseAmount, AppUtility.Operators.Equals, txtPurchaseGRNAmount.Text, 0);
                    break;
                case "2":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseGRN.InvoiceBaseAmount, AppUtility.Operators.GreaterOrEqualTo, txtPurchaseGRNAmount.Text, 0);
                    break;
                case "3":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseGRN.InvoiceBaseAmount, AppUtility.Operators.LessOrEqualTo, txtPurchaseGRNAmount.Text, 0);
                    break;
                case "4":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseGRN.InvoiceBaseAmount, AppUtility.Operators.GreaterThan, txtPurchaseGRNAmount.Text, 0);
                    break;
                case "5":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseGRN.InvoiceBaseAmount, AppUtility.Operators.LessThan, txtPurchaseGRNAmount.Text, 0);
                    break;
            }
        }
        if (hdfVendorID.Value != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseGRN.Vendor_Company_ID, AppUtility.Operators.Equals, hdfVendorID.Value, 0);
        }
        if (txtVoucherDate.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseGRN.InvoiceDate, AppUtility.Operators.Equals, txtVoucherDate.Text, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
            objColl.AddCriteria(AppObjects.PurchaseGRN.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.PurchaseGRNSummary] = objColl;
        grdPurchaseGRNSummary.DataSource = objColl;
        grdPurchaseGRNSummary.DataBind();
        if (grdPurchaseGRNSummary.Rows.Count <= 0)
        {
            pnlDisplaySummary.Visible = false;
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Records Found','2'" + ");", true);
        }
        else
        {
            pnlDisplaySummary.Visible = true;
        }
    }
    public void CancelPurchaseGRN(AppPurchaseGRN ObjPurchaseGRN)
    {
        AppPurchaseGRN ObjDuplicatePurchaseGRN = ObjPurchaseGRN;
        ObjDuplicatePurchaseGRN.ID = 0;
        ObjDuplicatePurchaseGRN.VoucherType_ID = 33; //Purchase Bill Cancel
        ObjDuplicatePurchaseGRN.ModifiedBy = intRequestingUserID;
        ObjDuplicatePurchaseGRN.ModifiedOn = System.DateTime.Now;
        ObjDuplicatePurchaseGRN.Status = new AppConvert((int)RecordStatus.Cancelled);
        if (ObjDuplicatePurchaseGRN.PurchaseGRNItemColl.Count > 0)
        {
            int TotalCount = ObjDuplicatePurchaseGRN.PurchaseGRNItemColl.Count;
            for (int i = 0; i < TotalCount; i++)
            {
                ObjDuplicatePurchaseGRN.PurchaseGRNItemColl[i].ID = 0;
                ObjDuplicatePurchaseGRN.PurchaseGRNItemColl[i].ModifiedBy = intRequestingUserID;
                ObjDuplicatePurchaseGRN.PurchaseGRNItemColl[i].ModifiedOn = System.DateTime.Now;
            }
        }
        ObjDuplicatePurchaseGRN.Save();
        #region Add Ledger Entries
        LedgerEntry(LoginCompanyID, 100101, 1001, ObjDuplicatePurchaseGRN.Vendor_Company.Vendor_Ledger_ID, ObjDuplicatePurchaseGRN.ID, ObjDuplicatePurchaseGRN.InvoiceDate, 0, ObjDuplicatePurchaseGRN.FinalAmount, ObjDuplicatePurchaseGRN.InvoiceNumber, ObjDuplicatePurchaseGRN.ModifiedBy, ObjDuplicatePurchaseGRN.ModifiedOn, 0, ObjDuplicatePurchaseGRN.Status, ObjDuplicatePurchaseGRN.Type, 0, 0, ObjDuplicatePurchaseGRN.VoucherType_ID, ObjDuplicatePurchaseGRN.BillDescription);
        if (ObjDuplicatePurchaseGRN.TaxType.TaxTypeConfigurationColl.Count > 0 && ObjDuplicatePurchaseGRN.TaxType_ID > 0)
        {
            List<TaxDetails> objTaxDetails = ObjDuplicatePurchaseGRN.TaxType.CalculateTax(ObjDuplicatePurchaseGRN.InvoiceBaseAmount - ObjDuplicatePurchaseGRN.TaxAmount, 0, 0);
            foreach (TaxDetails item in objTaxDetails)
            {
                if (item.Ledger_ID != 0)
                {
                    LedgerEntry(LoginCompanyID, 100102, 1001, item.Ledger_ID, ObjDuplicatePurchaseGRN.ID, ObjDuplicatePurchaseGRN.InvoiceDate, item.TaxValue, 0, ObjDuplicatePurchaseGRN.InvoiceNumber, ObjDuplicatePurchaseGRN.ModifiedBy, ObjDuplicatePurchaseGRN.ModifiedOn, 0, ObjDuplicatePurchaseGRN.Status, ObjDuplicatePurchaseGRN.Type, 0, 0, ObjDuplicatePurchaseGRN.VoucherType_ID, ObjDuplicatePurchaseGRN.BillDescription);
                }
            }
        }
        LedgerEntry(LoginCompanyID, 100103, 1001, DiscountLedgerID, ObjDuplicatePurchaseGRN.ID, ObjDuplicatePurchaseGRN.InvoiceDate, 0, ObjDuplicatePurchaseGRN.DiscountAmount, ObjDuplicatePurchaseGRN.InvoiceNumber, ObjDuplicatePurchaseGRN.ModifiedBy, ObjDuplicatePurchaseGRN.ModifiedOn, 0, ObjDuplicatePurchaseGRN.Status, ObjDuplicatePurchaseGRN.Type, 0, 0, ObjDuplicatePurchaseGRN.VoucherType_ID, ObjDuplicatePurchaseGRN.BillDescription);
        LedgerEntry(LoginCompanyID, 100104, 1001, RoundingOFFLedgerID, ObjDuplicatePurchaseGRN.ID, ObjDuplicatePurchaseGRN.InvoiceDate, 0, ObjDuplicatePurchaseGRN.RoundAmount, ObjDuplicatePurchaseGRN.InvoiceNumber, ObjDuplicatePurchaseGRN.ModifiedBy, ObjDuplicatePurchaseGRN.ModifiedOn, 0, ObjDuplicatePurchaseGRN.Status, ObjDuplicatePurchaseGRN.Type, 0, 0, ObjDuplicatePurchaseGRN.VoucherType_ID, ObjDuplicatePurchaseGRN.BillDescription);
        if (ObjDuplicatePurchaseGRN.PurchaseGRNItemColl.Count > 0)
        {
            foreach (var item in ObjDuplicatePurchaseGRN.PurchaseGRNItemColl)
            {

                LedgerEntry(LoginCompanyID, 100201, 1002, item.Item.Sales_Ledger_ID, item.ID, ObjDuplicatePurchaseGRN.InvoiceDate, item.ItemBaseAmount, 0, ObjDuplicatePurchaseGRN.InvoiceNumber, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicatePurchaseGRN.Status, item.Type, 0, 0, ObjDuplicatePurchaseGRN.VoucherType_ID, item.ItemDescription);
                if (item.TaxAmount != 0)
                {
                    if (item.TaxType.TaxTypeConfigurationColl.Count > 0 && item.TaxType_ID > 0)
                    {
                        List<TaxDetails> objTaxDetails = item.TaxType.CalculateTax(item.ItemBaseAmount, 0, 0);
                        foreach (TaxDetails td in objTaxDetails)
                        {
                            if (td.Ledger_ID != 0)
                            {
                                LedgerEntry(LoginCompanyID, 100202, 1002, td.Ledger_ID, item.ID, ObjDuplicatePurchaseGRN.InvoiceDate, td.TaxValue, 0, ObjDuplicatePurchaseGRN.InvoiceNumber, ObjDuplicatePurchaseGRN.ModifiedBy, ObjDuplicatePurchaseGRN.ModifiedOn, item.Status, ObjDuplicatePurchaseGRN.Status, ObjDuplicatePurchaseGRN.Type, 0, 0, ObjDuplicatePurchaseGRN.VoucherType_ID, item.ItemDescription);
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
    #endregion
    #region GridView Events
    protected void grdPurchaseGRNSummary_PreRender(object sender, EventArgs e)
    {
        if (grdPurchaseGRNSummary.Rows.Count > 0)
        {
            grdPurchaseGRNSummary.UseAccessibleHeader = true;
            grdPurchaseGRNSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion
    #region Button Click Events
    protected void btnNewPurchaseGRN_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.ObjCompanySession);
        Response.Redirect("T_PurchaseBill.aspx?BillType=1");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
        if (grdPurchaseGRNSummary.Rows.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('No Records Found','2'" + ");", true);
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (Update(grdPurchaseGRNSummary, new AppConvert((int)RecordStatus.Deleted), 1001, 1002) == 1)
        {
            BindData();
        }
    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        if (Update(grdPurchaseGRNSummary, new AppConvert((int)RecordStatus.Approve), 1001, 1002) == 1)
        {
            BindData();
        }

    }
    protected void btnCanceled_Click(object sender, EventArgs e)
    {
        AppObjects.AppPurchaseGRNColl objColl = new AppPurchaseGRNColl(intRequestingUserID);
        if (txtCancelNo.Text != "" && txtVoucherDate.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.PurchaseGRN.InvoiceNumber, AppUtility.Operators.Equals, txtCancelNo.Text, 0);
            objColl.Search(RecordStatus.AllActive);
            if (objColl.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Record not found','2'" + ");", true);
                return;
            }
            else
            {
                AppPurchaseGRN objSinglePG = objColl[0];
                if (objSinglePG.Status == new AppConvert((int)RecordStatus.Cancelled))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelNo.Text + " Transaction alerady cancelled','2'" + ");", true);
                    return;
                }
                if (objSinglePG.Status == new AppConvert((int)RecordStatus.Deleted))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelNo.Text + " Transaction is deleted','2'" + ");", true);
                    return;
                }
                if (objSinglePG.Status == new AppConvert((int)RecordStatus.Created))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelNo.Text + " Transaction is not approved','2'" + ");", true);
                    return;
                }
                objSinglePG.Status = new AppConvert((int)RecordStatus.Cancelled);

                objSinglePG.Save();

                #region Update Ledger Entries
                UpdateLedgerEntryStatus(0, 1001, objSinglePG.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                var PurchaseItem = objSinglePG.PurchaseGRNItemColl.ToList();
                foreach (var item in PurchaseItem)
                {
                    UpdateLedgerEntryStatus(0, 1002, item.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                }
                #endregion
                CancelPurchaseGRN(objSinglePG);
            }
            BindData();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
        }
        else
        {
            if (string.IsNullOrEmpty(txtCancelNo.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Voucher No.','1'" + ");", true);
                txtCancelNo.Focus();
            }
            if (string.IsNullOrEmpty(txtVoucherDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Voucher Date.','1'" + ");", true);
                txtVoucherDate.Focus();
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
            return;
        }
    }
    #endregion
}