﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to display all Company lists.
*/
#endregion
#region Using
using System;
using System.Web.UI.WebControls;
using AppObjects;
using System.Web.UI;
using AppUtility;
using System.Collections.Generic;
#endregion
public partial class T_SaleBillSummary : BigSunPage
{
    #region PageLoad
    AppSaleBillColl objPO;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindData();
        }
    }
    #endregion
    #region Functions   
    protected void BindData()
    {
        AppObjects.AppSaleBillColl objColl = new AppSaleBillColl(intRequestingUserID);
        if (txtPurchaeOrderNo.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleBill.SaleBillNumber, AppUtility.Operators.Equals, txtPurchaeOrderNo.Text, 0);
        }
        if (txtSaleBillAmount.Text != "")
        {
            switch (ddlOperators.SelectedValue)
            {
                case "1":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleBill.BaseAmount, AppUtility.Operators.Equals, txtSaleBillAmount.Text, 0);
                    break;
                case "2":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleBill.BaseAmount, AppUtility.Operators.GreaterOrEqualTo, txtSaleBillAmount.Text, 0);
                    break;
                case "3":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleBill.BaseAmount, AppUtility.Operators.LessOrEqualTo, txtSaleBillAmount.Text, 0);
                    break;
                case "4":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleBill.BaseAmount, AppUtility.Operators.GreaterThan, txtSaleBillAmount.Text, 0);
                    break;
                case "5":
                    objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleBill.BaseAmount, AppUtility.Operators.LessThan, txtSaleBillAmount.Text, 0);
                    break;
            }
        }
        if (hdfCustomerID.Value != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleBill.Customer_Company_ID, AppUtility.Operators.Equals, hdfCustomerID.Value, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleBill.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue, 0);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.SaleBillSummary] = objColl;
        grdSaleBillSummary.DataSource = objColl;
        grdSaleBillSummary.DataBind();
    }
    public void CancelSaleBill(AppSaleBill ObjSaleBill)
    {
        AppSaleBill ObjDuplicateSaleBill = ObjSaleBill;
        ObjDuplicateSaleBill.ID = 0;
        ObjDuplicateSaleBill.VoucherType_ID = 33; //Purchase Bill Cancel
        ObjDuplicateSaleBill.ModifiedBy = intRequestingUserID;
        ObjDuplicateSaleBill.ModifiedOn = System.DateTime.Now;
        ObjDuplicateSaleBill.Status = new AppConvert((int)RecordStatus.Cancelled);
        if (ObjDuplicateSaleBill.SaleBillItemColl.Count > 0)
        {
            int TotalCount = ObjDuplicateSaleBill.SaleBillItemColl.Count;
            for (int i = 0; i < TotalCount; i++)
            {
                ObjDuplicateSaleBill.SaleBillItemColl[i].ID = 0;
                ObjDuplicateSaleBill.SaleBillItemColl[i].ModifiedBy = intRequestingUserID;
                ObjDuplicateSaleBill.SaleBillItemColl[i].ModifiedOn = System.DateTime.Now;

            }
        }
        ObjDuplicateSaleBill.Save();

        #region Add Ledger Entries
        LedgerEntry(LoginCompanyID, 101501, 1015, ObjDuplicateSaleBill.Customer_Company.Customer_Ledger_ID, ObjDuplicateSaleBill.ID, ObjDuplicateSaleBill.SaleBillDate, ObjDuplicateSaleBill.BaseAmount, 0, ObjDuplicateSaleBill.SaleBillNumber, ObjDuplicateSaleBill.ModifiedBy, ObjDuplicateSaleBill.ModifiedOn, 0, ObjDuplicateSaleBill.Status, ObjDuplicateSaleBill.Type, 0, 0, ObjDuplicateSaleBill.VoucherType_ID, ObjDuplicateSaleBill.Remark);
        if (ObjDuplicateSaleBill.TaxType.TaxTypeConfigurationColl.Count > 0 && ObjDuplicateSaleBill.TaxType_ID > 0)
        {
            List<TaxDetails> objTaxDetails = ObjDuplicateSaleBill.TaxType.CalculateTax(ObjDuplicateSaleBill.BaseAmount - ObjDuplicateSaleBill.TaxAmount, 0, 0);
            foreach (TaxDetails item in objTaxDetails)
            {
                if (item.Ledger_ID != 0)
                {
                    UpdateLedgerEntryStatus(101502, 1015, ObjDuplicateSaleBill.ID, new AppUtility.AppConvert((int)RecordStatus.Deleted));
                    LedgerEntry(LoginCompanyID, 101502, 1015, item.Ledger_ID, ObjDuplicateSaleBill.ID, ObjDuplicateSaleBill.SaleBillDate, 0, item.TaxValue, ObjDuplicateSaleBill.SaleBillNumber, ObjDuplicateSaleBill.ModifiedBy, ObjDuplicateSaleBill.ModifiedOn, 0, ObjDuplicateSaleBill.Status, ObjDuplicateSaleBill.Type, 0, 0, ObjDuplicateSaleBill.VoucherType_ID, ObjDuplicateSaleBill.Remark);
                }
            }
        }
        // To update Discount Ledger Type ID - Pending Work
        LedgerEntry(LoginCompanyID, 101503, 1015, 0, ObjDuplicateSaleBill.ID, ObjDuplicateSaleBill.SaleBillDate, ObjDuplicateSaleBill.DiscountAmount, 0, ObjDuplicateSaleBill.SaleBillNumber, ObjDuplicateSaleBill.ModifiedBy, ObjDuplicateSaleBill.ModifiedOn, 0, ObjDuplicateSaleBill.Status, ObjDuplicateSaleBill.Type, 0, 0, ObjDuplicateSaleBill.VoucherType_ID, ObjDuplicateSaleBill.Remark);
        //Other charges // To update RoundUp Ledger Type ID - Pending Work
        LedgerEntry(LoginCompanyID, 101504, 1015, 0, ObjDuplicateSaleBill.ID, ObjDuplicateSaleBill.SaleBillDate, ObjDuplicateSaleBill.RoundAmount, 0, ObjDuplicateSaleBill.SaleBillNumber, ObjDuplicateSaleBill.ModifiedBy, ObjDuplicateSaleBill.ModifiedOn, 0, ObjDuplicateSaleBill.Status, ObjDuplicateSaleBill.Type, 0, 0, ObjDuplicateSaleBill.VoucherType_ID, ObjDuplicateSaleBill.Remark);
        if (ObjDuplicateSaleBill.SaleBillItemColl.Count > 0)
        {
            foreach (var item in ObjDuplicateSaleBill.SaleBillItemColl)
            {                           // Item Ledger
                if (item.BaseAmount != 0) { LedgerEntry(LoginCompanyID, 101601, 1016, item.Item.Sales_Ledger_ID, item.ID, ObjDuplicateSaleBill.SaleBillDate, 0, item.BaseAmount, ObjDuplicateSaleBill.SaleBillNumber, item.ModifiedBy, item.ModifiedOn, item.Status, ObjDuplicateSaleBill.Status, 0, 0, ObjDuplicateSaleBill.VoucherType_ID); }
                // tax Ledger
                if (item.TaxAmount != 0)
                {
                    if (item.TaxType.TaxTypeConfigurationColl.Count > 0 && item.TaxType_ID > 0)
                    {
                        List<TaxDetails> objTaxDetails = item.TaxType.CalculateTax(item.BaseAmount, 0, 0);
                        foreach (TaxDetails td in objTaxDetails)
                        {
                            if (td.Ledger_ID != 0)
                            {
                                UpdateLedgerEntryStatus(101602, 1016, item.ID, new AppUtility.AppConvert((int)RecordStatus.Deleted));
                                LedgerEntry(LoginCompanyID, 101602, 1016, td.Ledger_ID, item.ID, ObjDuplicateSaleBill.SaleBillDate, 0, td.TaxValue, ObjDuplicateSaleBill.SaleBillNumber, ObjDuplicateSaleBill.ModifiedBy, ObjDuplicateSaleBill.ModifiedOn, item.Status, ObjDuplicateSaleBill.Status, 0, 0, ObjDuplicateSaleBill.VoucherType_ID);
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
    #endregion
    #region GridView Events
    protected void grdSaleBillSummary_PreRender(object sender, EventArgs e)
    {
        if (grdSaleBillSummary.Rows.Count > 0)
        {
            grdSaleBillSummary.UseAccessibleHeader = true;
            grdSaleBillSummary.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    #endregion   
    #region Button Click Events
    protected void btnNewSaleBill_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.ObjCompanySession);
        Response.Redirect("T_SaleBill.aspx?BillType=1");
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (Update(grdSaleBillSummary, new AppConvert((int)RecordStatus.Deleted), 1015, 1016) == 1)
        {
            BindData();
        }
    }
    protected void btnApproved_Click(object sender, EventArgs e)
    {
        if (Update(grdSaleBillSummary, new AppConvert((int)RecordStatus.Approve), 1015, 1016) == 1)
        {
            BindData();
        }
    }
    protected void btnCanceled_Click(object sender, EventArgs e)
    {
        AppObjects.AppSaleBillColl objColl = new AppSaleBillColl(intRequestingUserID);
        if (txtCancelNo.Text != "" && txtVoucherDate.Text != "")
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.SaleBill.SaleBillNumber, AppUtility.Operators.Equals, txtCancelNo.Text, 0);
            objColl.Search(RecordStatus.AllActive);
            if (objColl.Count <= 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Record not found','2'" + ");", true);
                return;
            }
            else
            {
                AppSaleBill objSingleSB = objColl[0];
                if (objSingleSB.Status == new AppConvert((int)RecordStatus.Cancelled))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelNo.Text + " Transaction alerady cancelled','2'" + ");", true);
                    return;
                }
                if (objSingleSB.Status == new AppConvert((int)RecordStatus.Deleted))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelNo.Text + " Transaction is deleted','2'" + ");", true);
                    return;
                }
                if (objSingleSB.Status == new AppConvert((int)RecordStatus.Created))
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtCancelNo.Text + " Transaction is not approved','2'" + ");", true);
                    return;
                }
                objSingleSB.Status = new AppConvert((int)RecordStatus.Cancelled);

                objSingleSB.Save();

                #region Update Ledger Entries
                UpdateLedgerEntryStatus(0, 1015, objSingleSB.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                var SalesItem = objSingleSB.SaleBillItemColl;
                foreach (var item in SalesItem)
                {
                    UpdateLedgerEntryStatus(0, 1016, item.ID, new AppUtility.AppConvert((int)RecordStatus.Cancelled));
                }
                #endregion

                CancelSaleBill(objSingleSB);
            }
            BindData();
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
        }
        else
        {
            if (string.IsNullOrEmpty(txtCancelNo.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Voucher No.','1'" + ");", true);
                txtCancelNo.Focus();
            }
            if (string.IsNullOrEmpty(txtVoucherDate.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Please enter Voucher Date.','1'" + ");", true);
                txtVoucherDate.Focus();
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", "checkClass(3);", true);
            return;
        }
    }
    #endregion
}