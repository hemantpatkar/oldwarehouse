﻿#region Author
/*
Name    :   Pradip Bobhate
Date    :   25/07/2016
Use     :   This form is used to add details of vendors
*/
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppObjects;
using AppUtility;
#endregion
public partial class VendorDetails : BigSunPage
{
    AppObjects.AppCompany obj = null;
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {       
        obj = new AppObjects.AppCompany(this.intRequestingUserID);
        if (!IsPostBack)
        {    
            Session.Add(TableConstants.VendorSessionObj, obj);            
            CommonFunctions.BindDropDown(this.intRequestingUserID, Components.OrganizationType, ddlOrganizationType, "Name", "ID","",false);
            CommonFunctions.BindDropDown(this.intRequestingUserID, Components.CurrencyType, ddlCurrency, "Name", "ID", "", false);
            string ID = Convert.ToString(Request.QueryString["VendorID"]);
            if (Convert.ToString(Request.QueryString["Msg"]) == "1")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Data Save Successfully','1'" + ");", true);
            }
            if (!string.IsNullOrEmpty(ID))
            {
                int VendorID = new AppUtility.AppConvert(ID);
                AppObjects.AppCompany objCompany = new AppObjects.AppCompany(intRequestingUserID,VendorID);
                txtVendorCode.Text = objCompany.Code;
                txtVendorName.Text = objCompany.Name;
                ddlOrganizationType.SelectedValue = new AppUtility.AppConvert(objCompany.OrganizationType_ID);
                ddlCurrency.SelectedValue = new AppUtility.AppConvert(objCompany.CurrencyType_ID);
                txtDescription.Text = objCompany.Description;
                hdfTermsAndConditionsID.Value = new AppConvert(objCompany.TermsAndConditions_ID);
                hdfTermsAndConditionsName.Value = new AppConvert(objCompany.TermsAndConditions.Statement);
                txtTermsAndConditions.Text = new AppConvert(objCompany.TermsAndConditions.Statement);
                Session.Add(TableConstants.VendorSessionObj, objCompany);
                btnApprove.Visible = true;
                hdfVendorLedgerID.Value = new AppConvert(objCompany.Vendor_Ledger_ID);
                txtVendorLedger.Text = objCompany.Vendor_Ledger.LedgerName;
                hdfVendorLedgerName.Value = txtVendorLedger.Text;
                chkIsNewVendorLedger.Visible = false;
                chkIsNewVendorLedger.Checked = false;
                LedgerType(VendorID);
            }  
            else
            {
                LedgerType(0);
                btnApprove.Visible = false;
            }          
        }
    }
    #endregion    
    #region btnSave_Click
    protected void btnSave_Click(object sender, EventArgs e)
    {
        AppObjects.AppCompany objVendor = (AppObjects.AppCompany)Session[TableConstants.VendorSessionObj];
        string Message = "Vendor Updated Successfully";
        if (objVendor == null)
        {
            objVendor = new AppObjects.AppCompany(this.intRequestingUserID);
            Message = "New Vendor Created";
        }        
        objVendor.Code = txtVendorCode.Text;
        objVendor.Name = txtVendorName.Text;
        objVendor.Type = new AppConvert((int)CompanyRoleType.Vendor);
        objVendor.CurrencyType_ID = new AppConvert(ddlCurrency.SelectedValue);
        objVendor.OrganizationType_ID = new AppConvert(ddlOrganizationType.SelectedValue);
        objVendor.Status= (chkActive.Checked == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        objVendor.Description = txtDescription.Text;
        objVendor.ModifiedBy = intRequestingUserID;
        objVendor.ModifiedOn = System.DateTime.Now;
        objVendor.TermsAndConditions_ID = new AppConvert(hdfTermsAndConditionsID.Value);
        if (chkIsNewVendorLedger.Checked)
        {
            objVendor.Vendor_Ledger_ID = CreateLedger(new AppConvert(hdfVendorLedgerID.Value));
        }
        else
        {
            objVendor.Vendor_Ledger_ID = (new AppConvert(hdfVendorLedgerID.Value));
        }

        objVendor.Save();
        Session.Remove(TableConstants.VendorSessionObj);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + Message + "','1'" + ");", true);
        AppCompany objNewCompany = new AppCompany(intRequestingUserID);
        Session.Add(TableConstants.VendorSessionObj, objNewCompany);
        Response.Redirect("VendorDetails.aspx?Msg=1&VendorID="+ objVendor.ID);
    }
    #endregion
    #region btnCancel_Click
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.VendorSessionObj);
        Response.Redirect("VendorSummary.aspx");
    }
    #endregion
    #region btnApprove_Click
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        obj = (AppObjects.AppCompany)Session[TableConstants.VendorSessionObj];
        obj.Status =  new AppConvert((int)RecordStatus.Approve);
        obj.Save();
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Vendor Successfully Approved','1'" + ");", true);
    }
    #endregion

    public int CreateLedger(int Parent_ID)
    {
        AppLedgerColl objLedgerColl = (AppLedgerColl)HttpContext.Current.Session[TableConstants.LedgerTreeSessionObj];
        AppLedger objLedger = new AppLedger(intRequestingUserID);
        objLedger.ID = 0;
        objLedger.LedgerName = txtVendorName.Text;
        objLedger.AccountNo = "1000000003";
        objLedger.LedgerCode = txtVendorCode.Text;
        objLedger.Parent_ID = new AppConvert(Parent_ID);
        objLedger.AccountType_ID = AppLedgerColl.GetAccountType(Parent_ID);
        objLedger.LedgerType_ID = 1;
        objLedger.Status = (chkActive.Checked == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        objLedger.LedgerFlag = new AppConvert((int)LedgerFlag.GeneralLedger);
        if (objLedgerColl != null)
        {
            objLedger.SortOrder = new AppConvert(objLedgerColl.FindLastIndex(o => o.Parent_ID == objLedger.Parent_ID));
        }
        objLedger.ModifiedBy = intRequestingUserID;
        objLedger.ModifiedOn = System.DateTime.Now;
        objLedger.Save();
        return objLedger.ID;
    }
    public void LedgerType(int ID = 0)
    {

        if (chkIsNewVendorLedger.Checked)
        {

            lblVendorLedger.Text = "Parent Ledger";
            ACEtxtVendorLedger.ContextKey = "," + (new AppUtility.AppConvert((int)LedgerFlag.ControlAccount) + "~" + new AppUtility.AppConvert((int)LedgerFlag.LedgerGroup) + "~");
        }
        else
        {
            lblVendorLedger.Text = "Vendor Ledger";
            ACEtxtVendorLedger.ContextKey = "1," + ("~" + "~" + new AppUtility.AppConvert((int)LedgerFlag.GeneralLedger));
        }
    }

    
    protected void chkIsNewVendorLedger_CheckedChanged(object sender, EventArgs e)
    {
        LedgerType();
    }
}