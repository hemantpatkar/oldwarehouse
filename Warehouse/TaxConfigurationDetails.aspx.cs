﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using AppObjects;
using AppUtility;
using System.Text.RegularExpressions;
public partial class TaxConfigurationDetails : BigSunPage
{
    AppTaxType ObjTaxType = null;
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        ObjTaxType = new AppTaxType(intRequestingUserID);
        if (!IsPostBack)
        {
            Session.Add(TableConstants.TaxSessionObj, ObjTaxType);
            string ID = Convert.ToString(Request.QueryString["TaxID"]);
            if (string.IsNullOrEmpty(ID) == false)
            {
                int TaxID = new AppUtility.AppConvert(ID);
                AppTaxType objBindTaxType = new AppTaxType(intRequestingUserID, TaxID);
                txtTaxCode.Text = objBindTaxType.Code;
                txtTaxName.Text = objBindTaxType.Name;
                chkTaxType.Checked = new AppConvert(objBindTaxType.Type);
                txtFromDate.Text = new AppConvert(objBindTaxType.FromDate);
                txtToDate.Text = new AppConvert(objBindTaxType.ToDate);
                Session.Add(TableConstants.TaxSessionObj, objBindTaxType);                
                string WrongExpressionValue = objBindTaxType.ValidateExpression(100, 5000, 1500);
                if (WrongExpressionValue == "")
                {
                    objBindTaxType.CalculateTax(100, 5000, 1500);
                }
            }
            BindData();
            txtTaxCode.Focus();
        }
    }
    #endregion
    #region TextBox Changed Event
    protected void txtBasePrice_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtBasePrice.Text))
        {
            ObjTaxType = (AppTaxType)Session[TableConstants.TaxSessionObj];
            ObjTaxType.CalculateTax(Convert.ToDecimal(txtBasePrice.Text), 5000, 1500);
            grdTaxConfigDetails.DataSource = ObjTaxType.TaxTypeConfigurationColl.Where(x => x.Status == 0);
            grdTaxConfigDetails.DataBind();
        }
    }
    protected void txtLedger_TextChanged(object sender, EventArgs e)
    {
        ObjTaxType = (AppTaxType)Session[TableConstants.TaxSessionObj];
        GridViewRow gvr = (GridViewRow)((TextBox)sender).Parent.Parent;
        int RowIndex = new AppConvert(gvr.RowIndex);
        string ID = new AppConvert(RowIndex);
        HiddenField hdfLedgerID = (HiddenField)grdTaxConfigDetails.Rows[RowIndex].FindControl("hdfLedgerID");
        ObjTaxType.TaxTypeConfigurationColl[RowIndex].Ledger_ID = new AppConvert(hdfLedgerID.Value);
        Session[TableConstants.TaxSessionObj] = ObjTaxType;
    }
    protected void ddlRoundType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ObjTaxType = (AppTaxType)Session[TableConstants.TaxSessionObj];
        GridViewRow gvr = (GridViewRow)((DropDownList)sender).Parent.Parent;
        int RowIndex = new AppConvert(gvr.RowIndex);
        string ID = new AppConvert(RowIndex);
        DropDownList ddlRoundType = (DropDownList)grdTaxConfigDetails.Rows[RowIndex].FindControl("ddlRoundType");
        ObjTaxType.TaxTypeConfigurationColl[RowIndex].RoundType = new AppConvert(ddlRoundType.SelectedValue);
        Session[TableConstants.TaxSessionObj] = ObjTaxType;
    }
    protected void txtSetOfPercent_TextChanged(object sender, EventArgs e)
    {
        ObjTaxType = (AppTaxType)Session[TableConstants.TaxSessionObj];
        GridViewRow gvr = (GridViewRow)((TextBox)sender).Parent.Parent;
        int RowIndex = new AppConvert(gvr.RowIndex);
        string ID = new AppConvert(RowIndex);
        TextBox txtSetOfPercent = (TextBox)grdTaxConfigDetails.Rows[RowIndex].FindControl("txtSetOfPercent");
        ObjTaxType.TaxTypeConfigurationColl[RowIndex].SetOffPercentage = new AppConvert(txtSetOfPercent.Text);
        Session[TableConstants.TaxSessionObj] = ObjTaxType;
    }
    protected void txtTaxType_TextChanged(object sender, EventArgs e)
    {
        ObjTaxType = (AppTaxType)Session[TableConstants.TaxSessionObj];
        GridViewRow gvr = (GridViewRow)((TextBox)sender).Parent.Parent;
        int RowIndex = new AppConvert(gvr.RowIndex);
        string ID = new AppConvert(RowIndex);
        TextBox txtTaxType = (TextBox)grdTaxConfigDetails.Rows[RowIndex].FindControl("txtTaxType");
        TextBox txtExpression = (TextBox)grdTaxConfigDetails.Rows[RowIndex].FindControl("txtExpression");
        TextBox txtLedger = (TextBox)grdTaxConfigDetails.Rows[RowIndex].FindControl("txtLedger");
        RequiredFieldValidator Rfv_txtLedger = (RequiredFieldValidator)grdTaxConfigDetails.Rows[RowIndex].FindControl("Rfv_txtLedger");
        if (txtTaxType.Text.ToUpper() == "FINALVALUE")
        {
            txtLedger.Enabled = false;
            Rfv_txtLedger.Enabled = false;
        }
        string OldCode = "(" + ObjTaxType.TaxTypeConfigurationColl[RowIndex].Code + ")";
        if (ObjTaxType.TaxTypeConfigurationColl.Any(x => x.Code == txtTaxType.Text && x.Status==0))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + txtTaxType.Text + " This code is already exists','1'" + ");", true);
            txtTaxType.Text = "";
            txtTaxType.Focus();
            return;
        }
        ObjTaxType.TaxTypeConfigurationColl[RowIndex].Code = txtTaxType.Text;
        ObjTaxType.ReplaceText(OldCode, "(" + txtTaxType.Text + ")");
        Session[TableConstants.TaxSessionObj] = ObjTaxType;
        if (!string.IsNullOrEmpty(txtExpression.Text))
        {
            string WrongExpressionValue = ObjTaxType.ValidateExpression(100, 5000, 1500);
            if (WrongExpressionValue == "")
            {
                ObjTaxType.CalculateTax(100, 5000, 1500);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + WrongExpressionValue + " Please enter valid Expression','1'" + ");", true);
                txtExpression.Focus();
                return;
            }
        }
        grdTaxConfigDetails.DataSource = ObjTaxType.TaxTypeConfigurationColl.Where(x => x.Status == 0);
        grdTaxConfigDetails.DataBind();
    }
    protected void txtExpression_TextChanged(object sender, EventArgs e)
    {
        GridViewRow gvr = (GridViewRow)((TextBox)sender).Parent.Parent;
        int RowIndex = new AppConvert(gvr.RowIndex);
        string ID = new AppConvert(RowIndex);
        TextBox Code = (TextBox)grdTaxConfigDetails.Rows[RowIndex].FindControl("txtTaxType");
        TextBox txtExpression = (TextBox)grdTaxConfigDetails.Rows[RowIndex].FindControl("txtExpression");
        DropDownList RoundType = (DropDownList)grdTaxConfigDetails.Rows[RowIndex].FindControl("ddlRoundType");
        TextBox SetOffPercentage = (TextBox)grdTaxConfigDetails.Rows[RowIndex].FindControl("txtSetOfPercent");
        AddTaxConfigurationCollection(ID, RoundType.SelectedValue, txtExpression.Text, SetOffPercentage.Text, "", Code.Text);
        ObjTaxType = (AppTaxType)Session[TableConstants.TaxSessionObj];
        string WrongExpressionValue = ObjTaxType.ValidateExpression(100, 5000, 1500);
        if (WrongExpressionValue == "")
        {
            ObjTaxType.CalculateTax(100, 5000, 1500);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('" + WrongExpressionValue + " Please enter valid Expression','1'" + ");", true);
            txtExpression.Focus();
            return;
        }
        grdTaxConfigDetails.DataSource = ObjTaxType.TaxTypeConfigurationColl.Where(x => x.Status == 0);
        grdTaxConfigDetails.DataBind();
    }
    #endregion
    #region Button Click Events
    protected void lbtnCancel_Click(object sender, EventArgs e)
    {
        Session.Remove(TableConstants.TaxSessionObj);
        Response.Redirect("M_TaxSummary.aspx");
    }
    protected void lbtnSave_Click(object sender, EventArgs e)
    {
        ObjTaxType = (AppTaxType)Session[TableConstants.TaxSessionObj];
        if (!ObjTaxType.TaxTypeConfigurationColl.Any(x => x.Code == "FinalValue" && x.Status==0))
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('""FinalValue"" code is mandatory for final calculation','1'" + ");", true);
            return;
        }
        if (ObjTaxType == null)
        {
            ObjTaxType = new AppTaxType(intRequestingUserID);
        }
        int TaxConfigCount = ObjTaxType.TaxTypeConfigurationColl.Count;
        if (TaxConfigCount > 0)
        {
            AppTaxTypeConfiguration objTaxConfig = ObjTaxType.TaxTypeConfigurationColl[TaxConfigCount - 1];
            if (string.IsNullOrEmpty(objTaxConfig.Code) == true || string.IsNullOrEmpty(objTaxConfig.Expression) == true)
            {
                ObjTaxType.TaxTypeConfigurationColl.Remove(objTaxConfig);
            }
        }
        ObjTaxType.Code = txtTaxCode.Text;
        ObjTaxType.Name = txtTaxName.Text;
        ObjTaxType.FromDate = new AppConvert(txtFromDate.Text);
        ObjTaxType.ToDate = new AppConvert(txtToDate.Text);
        ObjTaxType.ModifiedBy = intRequestingUserID;
        ObjTaxType.ModifiedOn = System.DateTime.Now;
        ObjTaxType.Save();
        Session.Remove(TableConstants.TaxSessionObj);
        Response.Redirect("M_TaxSummary.aspx");
        //BindData();
    }
    protected void lbtnAddNew_Click(object sender, EventArgs e)
    {
        AddEmptyRow();
    }
    #endregion
    #region Gridview Events
    protected void grdTaxConfigDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        ObjTaxType = (AppTaxType)Session[TableConstants.TaxSessionObj];
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList ddlRoundType = (DropDownList)e.Row.FindControl("ddlRoundType");
            if (ddlRoundType != null)
            {
                if (ObjTaxType.TaxTypeConfigurationColl.Count > 0)
                {
                    ddlRoundType.SelectedValue = new AppConvert(ObjTaxType.TaxTypeConfigurationColl[e.Row.RowIndex].RoundType);
                }
            }
        }
    }
    protected void grdTaxConfigDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DELETE")
        {
            GridViewRow row = (GridViewRow)((LinkButton)e.CommandSource).Parent.Parent;
            AppTaxType taxConfigColl = (AppTaxType)Session[TableConstants.TaxSessionObj];
            taxConfigColl.TaxTypeConfigurationColl.Where(x => x.Status == 0).ToList()[row.RowIndex].Status = new AppConvert((int)RecordStatus.Deleted);
            Session.Add(TableConstants.TaxSessionObj, taxConfigColl);
            BindData();
        }
    }
    protected void grdTaxConfigDetails_PreRender(object sender, EventArgs e)
    {
        if (grdTaxConfigDetails.Rows.Count > 0)
        {
            grdTaxConfigDetails.UseAccessibleHeader = true;
            grdTaxConfigDetails.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdTaxConfigDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        ObjTaxType = (AppTaxType)Session[TableConstants.TaxSessionObj];
        ObjTaxType.TaxTypeConfigurationColl[e.RowIndex].Status = -10;
        Session[TableConstants.TaxSessionObj] = ObjTaxType;
    }
    #endregion
    #region Functions
    public void BindData()
    {
        AppTaxType taxCollection = (AppTaxType)Session[TableConstants.TaxSessionObj];
        if (taxCollection != null)
        {
            grdTaxConfigDetails.DataSource = taxCollection.TaxTypeConfigurationColl.Where(x => x.Status == 0);
            grdTaxConfigDetails.DataBind();
        }
        if (taxCollection.TaxTypeConfigurationColl.Count <= 0)
        {
            AddEmptyRow();
        }
    }
    public void AddEmptyRow()
    {
        AppTaxType taxConfigColl = (AppTaxType)Session[TableConstants.TaxSessionObj];
        AppTaxTypeConfiguration newTaxColl = new AppTaxTypeConfiguration(intRequestingUserID);
        taxConfigColl.TaxTypeConfigurationColl.Add(newTaxColl);
        grdTaxConfigDetails.DataSource = taxConfigColl.TaxTypeConfigurationColl.Where(x => x.Status == 0);
        grdTaxConfigDetails.DataBind();
        Session.Add(TableConstants.TaxSessionObj, taxConfigColl);
    }
    public void AddTaxConfigurationCollection(string Id, string RoundType, string Expression, string Percentage, string LedgerID, string Code)
    {
        ObjTaxType = (AppTaxType)Session[TableConstants.TaxSessionObj];
        string strCommandID = Id;
        int intCommandID = new AppUtility.AppConvert(strCommandID);
        AppTaxTypeConfiguration SingleObj = new AppTaxTypeConfiguration(intRequestingUserID);
        if (String.IsNullOrEmpty(strCommandID) == false)
        {
            SingleObj = ObjTaxType.TaxTypeConfigurationColl[intCommandID];
        }
        SingleObj.TaxType_ID = ObjTaxType.ID;
        SingleObj.RoundType = new AppConvert(RoundType);
        SingleObj.Expression = new AppConvert(Expression);
        SingleObj.SetOffPercentage = new AppUtility.AppConvert(Percentage);
        SingleObj.Ledger_ID = new AppUtility.AppConvert(LedgerID);
        SingleObj.Code = new AppUtility.AppConvert(Code);
        SingleObj.Status = new AppUtility.AppConvert((int)RecordStatus.Created);
        SingleObj.Type = new AppUtility.AppConvert(0);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;
        Session.Add(TableConstants.TaxSessionObj, ObjTaxType);
    }
    #endregion
}
