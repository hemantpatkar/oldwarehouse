﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AppUtility;
using AppObjects;
public partial class M_CostCenter : BigSunPage
{
    AppCostCenterColl obj;
    protected void Page_Load(object sender, EventArgs e)
    {
  
        obj = new AppObjects.AppCostCenterColl(intRequestingUserID);
        Session[TableConstants.SessionCostCenter] = obj;
        if (!IsPostBack)
        {
           BindData();
        }
    }
    #region GridViewEvents
   
    protected void grdCostCenter_PreRender(object sender, EventArgs e)
    {
        if (grdCostCenter.Rows.Count > 0)
        {
            grdCostCenter.UseAccessibleHeader = true;
            grdCostCenter.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }
    protected void grdCostCenter_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string strCommandName = e.CommandName;
        string strCommandID = e.CommandArgument.ToString();
        if (strCommandName == "INSERT")
        {       
            HiddenField hdfCostCenterID = grdCostCenter.FooterRow.FindControl("hdfCostCenterID") as HiddenField;
            TextBox txtCostCenterCode = grdCostCenter.FooterRow.FindControl("txtNewCostCenterCode") as TextBox;
            TextBox txtCostCenterName = grdCostCenter.FooterRow.FindControl("txtNewCostCenterName") as TextBox;
            CheckBox chkActiveCostCenter = grdCostCenter.FooterRow.FindControl("chkActiveCostCenter") as CheckBox;
            CheckBox chkDefaultCostCenter = grdCostCenter.FooterRow.FindControl("chkDefaultCostCenter") as CheckBox;
            SaveCostCenterDetails(strCommandID, txtCostCenterCode.Text, txtCostCenterName.Text, chkActiveCostCenter.Checked, chkDefaultCostCenter.Checked);
            ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Cost Center Successfully Added','1'" + ");", true);
            grdCostCenter.EditIndex = -1;
            BindData();
        }
        else if (strCommandName == "CANCELADD")
        {
            grdCostCenter.ShowFooter = false;
            BindData();
        }
        else if (strCommandName == "CANCELEDIT")
        {
            grdCostCenter.EditIndex = -1;
            BindData();
        }
        else if(strCommandName=="ADDNEW")
        {
            grdCostCenter.EditIndex = -1;
            grdCostCenter.ShowFooter = true;
            BindData();
        }
    }
    protected void grdCostCenter_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grdCostCenter.ShowFooter = false;
        grdCostCenter.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void grdCostCenter_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        HiddenField hdfID = grdCostCenter.Rows[e.RowIndex].FindControl("hdfID") as HiddenField;
        AppCostCenter SingleObj = new AppCostCenter(intRequestingUserID);
        SingleObj.ID = new AppConvert(hdfID.Value);
        SingleObj.Status = new AppConvert((int)RecordStatus.Deleted);
        SingleObj.Save();
     
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Cost Center Successfully Deleted','1'" + ");", true);
        BindData();
    }
    protected void grdCostCenter_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string strCommandID = new AppConvert(e.RowIndex);
        HiddenField hdfCostCenterID = grdCostCenter.Rows[e.RowIndex].FindControl("hdfCostCenterID") as HiddenField;
        TextBox txtCostCenterCode = grdCostCenter.Rows[e.RowIndex].FindControl("txtCostCenterCode") as TextBox;
        TextBox txtCostCenterName = grdCostCenter.Rows[e.RowIndex].FindControl("txtCostCenterName") as TextBox;
        CheckBox chkIsActiveCostCenter = grdCostCenter.Rows[e.RowIndex].FindControl("chkIsActiveCostCenter") as CheckBox;
        CheckBox chkIsDefaultCostCenter = grdCostCenter.Rows[e.RowIndex].FindControl("chkIsDefaultCostCenter") as CheckBox;

        SaveCostCenterDetails(hdfCostCenterID.Value, txtCostCenterCode.Text, txtCostCenterName.Text , chkIsActiveCostCenter.Checked, chkIsDefaultCostCenter.Checked);
        ScriptManager.RegisterStartupScript(this, GetType(), "saveJS", @"ShowAnimatedMessage('Cost Center Successfully Updated','1'" + ");", true);
        grdCostCenter.EditIndex = -1;
        BindData();
    }
    #endregion
    #region MiscFunctions
    public void SaveCostCenterDetails(string Id, string CostCenterCode, string CostCenterName, bool IsActive, bool IsDefault)
    {
        int intCommandID = new AppUtility.AppConvert(Id);
        AppCostCenter SingleObj = new AppCostCenter(intRequestingUserID);      
        SingleObj.ID = new AppConvert(Id);
        SingleObj.CostCenterCode = new AppConvert(CostCenterCode);
        SingleObj.CostCenterName = new AppConvert(CostCenterName);
        SingleObj.Status = (IsActive == true ? new AppConvert((int)RecordStatus.Active) : new AppConvert((int)RecordStatus.InActive));
        if (IsDefault == true)
        {
           
            obj.SetDefaultCostCenter();
        }
        SingleObj.Type = new AppUtility.AppConvert(IsDefault);
        SingleObj.ModifiedBy = intRequestingUserID;
        SingleObj.ModifiedOn = System.DateTime.Now;

        Session.Add(TableConstants.SessionCostCenter, obj);
        SingleObj.Save();
        BindData();
    }
    

    protected void BindData()
    {
        AppObjects.AppCostCenterColl objColl = new AppCostCenterColl(intRequestingUserID);
        if (!string.IsNullOrEmpty(txtCostCenterName.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.CostCenter.CostCenterName, AppUtility.Operators.Equals, txtCostCenterName.Text, 0);
        }
        if (!string.IsNullOrEmpty(txtCostCenterCode.Text))
        {
            objColl.AddCriteria(AppUtility.CollOperator.AND, AppObjects.CostCenter.CostCenterCode, AppUtility.Operators.Equals, txtCostCenterCode.Text, 0);
        }
        if (ddlStatus.SelectedValue != "-32768")
        {
             
            objColl.AddCriteria(AppObjects.CostCenter.Status, AppUtility.Operators.Equals, ddlStatus.SelectedValue);
        }
        objColl.Search(RecordStatus.ALL);
        Session[TableConstants.SessionCostCenter] = objColl;
        if (objColl != null)
        {
            grdCostCenter.DataSource = objColl;
            grdCostCenter.DataBind();
            if (objColl.Count <= 0)
            {
                AddEmptyRow();
            }
            
        }
       
 
    }
    public void AddEmptyRow()
    {
        AppCostCenterColl collOfContacts = new AppCostCenterColl(intRequestingUserID);
        AppCostCenter newContact = new AppCostCenter(intRequestingUserID);
        newContact.CostCenterCode = "Add atleast one Cost Center";
        collOfContacts.Add(newContact);
        grdCostCenter.DataSource = collOfContacts;
        grdCostCenter.DataBind();
        grdCostCenter.Rows[0].FindControl("lbtnEdit").Visible = false;
        grdCostCenter.Rows[0].FindControl("lbtnDelete").Visible = false;
        grdCostCenter.ShowFooter = true;
    }
    #endregion

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }
}