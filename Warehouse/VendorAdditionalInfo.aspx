﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" Inherits="VendorAdditionalInfo" Codebehind="VendorAdditionalInfo.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="row">
            <div class="col-lg-12" >  
                
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>                       
                <asp:GridView ID="grdAdditionalInfo" runat="server" AutoGenerateColumns="False" 
                OnPreRender="grdAdditionalInfo_PreRender" 
                CssClass="table table-hover table-striped  text-nowrap   nowrap">
                <Columns>                    
                    <asp:TemplateField>
                        <HeaderTemplate>
                        <asp:Label ID="lblEdit" Text="Edit" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" CssClass="fa fa-floppy-o fa-2x" ToolTip="Update"
                                Text="" />
                            <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" CssClass="fa fa-ban fa-2x" ToolTip="Cancel"
                                Text="" />
                        </EditItemTemplate>
                        <ItemTemplate>
                             <asp:LinkButton ID="lbtnAddNew" runat="server" CssClass="fa fa-file-o fa-2x" CommandName="Insert" OnClick="lbtnAddNew_Click" 
                                Text="" ToolTip="Add" />
                            <asp:LinkButton ID="lbtnEdit" runat="server" CssClass="fa fa-edit fa-2x fa-color-Edit" CommandName="Edit"
                                Text="" Tooltip="Edit" />
                            <asp:LinkButton ID="lbtnDelete" runat="server" CssClass="fa fa-trash fa-2x fa-color-Delete" CommandName="Delete"
                                Text="" Tooltip="Delete"  />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:LinkButton ID="lbtnSave" runat="server" CommandName="Insert" CssClass="fa fa-floppy-o fa-2x fa-color-Add" ToolTip="Save"
                                Text="" UseSubmitBehavior="False" />
                            <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel"  CssClass="fa fa-ban fa-2x"  Text="" OnClick="lbtnCancel_Click" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblType" Text="Type" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblTypeText" runat="server" Text='<%# Eval("Type") %>' TabIndex="2" AutoComplete="Off" CssClass="bold"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtType" runat="server" Text='<%# Eval("Type") %>' TabIndex="2" AutoComplete="Off" CssClass=" form-control"></asp:TextBox>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNewType" runat="server" AutoComplete="Off" CssClass="  form-control"></asp:TextBox>                            
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblValue" Text="Value" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblValueText" runat="server" Text='<%#Eval("TypeNo") %>' CssClass="bold"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="itxtValue" runat="server" Text='<%#Eval("TypeNo") %>' CssClass=" form-control"></asp:TextBox>                        
                            <asp:RequiredFieldValidator ID="RFV_itxtValue" runat="server" ControlToValidate="itxtValue"
                                Display="Dynamic" CssClass="text-danger" SetFocusOnError="true" ErrorMessage="*" ValidationGroup="val"></asp:RequiredFieldValidator>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="itxtNewValue" runat="server" AutoComplete="Off" CssClass=" form-control"></asp:TextBox>                            
                        </FooterTemplate>
                    </asp:TemplateField>
               
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:Label ID="lblDate" Text="Date" runat="server" CssClass="bold"></asp:Label>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDateText" runat="server" Text='<%#Eval("Date") %>' CssClass="bold"></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                                <asp:TextBox ID="txtDate" runat="server" Text='<%#Eval("Date") %>' CssClass=" form-control"></asp:TextBox>
                               <asp:MaskedEditExtender ID="txtDate_MaskedEditExtender" runat="server"
                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtDate">
                               </asp:MaskedEditExtender>
                               <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtDateID"
                                    TargetControlID="txtDate">
                               </asp:CalendarExtender>                                                        
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNewDate" runat="server" AutoComplete="Off" CssClass=" form-control"></asp:TextBox>  
                            <asp:MaskedEditExtender ID="txtNewDate_MaskedEditExtender" runat="server"
                                    Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtNewDate">
                               </asp:MaskedEditExtender>
                               <asp:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtNewDateID"
                                    TargetControlID="txtNewDate">
                               </asp:CalendarExtender>                         
                        </FooterTemplate>
                    </asp:TemplateField>                
                </Columns>
            </asp:GridView>
                     </ContentTemplate>
                 <Triggers>
                    <asp:AsyncPostBackTrigger ControlID = "grdAdditionalInfo" />
                </Triggers>
             </asp:UpdatePanel>
            </div>
        </div>
    
    <script type="text/javascript">
        var GridID = $get('<%=grdAdditionalInfo.ClientID%>');
        $(document).ready(function () {
            GridUI(GridID, 50);
        })
     
    </script>
</asp:Content>

